var gulp = require('gulp');
var gutil = require('gulp-util');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin');
var rebaseUrls = require('gulp-css-rebase-urls');
var ngAnnotate = require('gulp-ng-annotate');
var cleanCSS = require('gulp-clean-css');
var ngHtml2Js = require("gulp-ng-html2js")
var plumber = require('gulp-plumber');
var concatCss = require('gulp-concat-css');
var runSeq = require('run-sequence');
var replace = require('gulp-replace');
var jslint = require('gulp-jslint');
var eslint = require('gulp-eslint');

//Delete the build directory
gulp.task('clean', function() {
  return gulp.src('build')
 .pipe(clean());
});

//minify all plugin css
gulp.task('minify_common_style', function() {
    gulp.src([
		"css/font-awesome-4.7.0/css/font-awesome.min.css",
        "css/bootstrap.min.css"
		
    ])
    .pipe(plumber())
    .pipe(concatCss('common.css'))
  //  .pipe(replace('../node_modules/font-awesome/fonts/', '../fonts/font-awesome/fonts/'))
    .pipe(gulp.dest('build/css/'))
    .pipe(cleanCSS())
    .pipe(rename('common.min.css'))
    .pipe(gulp.dest('build/css/'));
});

// minify style_home css
gulp.task('minify_main_style', function() {
    gulp.src([
        "css/app.css",
		"css/register.css"
    ])
    .pipe(plumber())
    .pipe(concatCss('main.css'))
    .pipe(gulp.dest('build/css/'))
    .pipe(cleanCSS())
    .pipe(rename('main.min.css'))
    .pipe(gulp.dest('build/css/'));
});



// Move fonts to build directory
gulp.task('fonts', function () {
     gulp.src('fonts/**/*')
     .pipe(plumber())
     .pipe(gulp.dest('build/fonts'));
});

//optimize and move images
gulp.task('images', function () {
    gulp.src('images/**/*')
    .pipe(plumber())
    //.pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
    .pipe(gulp.dest('build/images'));
});

//concatenate and minify bower components
gulp.task('minify_lib', function(){
    gulp.src([
        "js/jquery-3.3.1.min.js",
		"js/popper.min.js",
		"js/bootstrap.min.js",
		"js/webfont.js"
    ])
    .pipe(plumber())
    .pipe(concat('libs.js'))
    .pipe(ngAnnotate())
    .pipe(gulp.dest('build/js'))
    .pipe(uglify({mangle: false, exportAll: true}))
    .pipe(rename('libs.min.js'))
    .pipe(gulp.dest('build/js'))
    .on('error', gutil.log);
});

//concatenate and minify app components
gulp.task('minify_app', function() {
    gulp.src([
        "js/ssam.js",
        "js/FormValidation.js"
    ])
    .pipe(plumber())
    .pipe(concat('app.js'))
    .pipe(ngAnnotate())
    .pipe(gulp.dest('build/js'))
    .pipe(uglify({mangle: false, exportAll: true}))
    .pipe(rename('app.min.js'))
    .pipe(gulp.dest('build/js'))
    .on('error', gutil.log);
});

//Grouped task on basis of nature
gulp.task('style', function() {
    runSeq(
		 'minify_common_style',
         'minify_main_style'
    );
});
gulp.task('script', function() {
    runSeq(
        'minify_lib',
        'minify_app'
    );
});
gulp.task('assets', function() {
    runSeq(
		'fonts',
        'images'
    );
});

//make a master gulp task to run all the above tasks
gulp.task('build', function() {
    runSeq(
		'style',
    	'script',
    	'assets',
        'js-lintin'
    );
});

// Master task for build with cleaning folders
gulp.task('clean-build', ['clean'], function() {
    runSeq(
    	'style',
    	'script',
    	'assets',
        'js-lintin'
    );
});


gulp.task('js-lintin', function() {
	return gulp.src(["js/*.js"])
		.pipe(eslint('eslintConfig.json'))
		.pipe(eslint.format());
});

gulp.task('watch', function() {
  gulp.watch("js/*.js", ['js-lintin']);
});

gulp.task('default', ['js-lintin','watch']);
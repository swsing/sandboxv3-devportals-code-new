$( document ).ready(function() {
  //$('.main-container').hide();
 // $( "#loadingDiv" ).show(); 
 
  WebFont.load({
          custom: {
              families: ['FontAwesome','OpenSans-Bold','OpenSans-Italic','OpenSans-Light','OpenSans-Regular','OpenSans-Semibold']
          },
          active: function(familyName, fvd) {
                   
                  $('.main-container').show();
                  // fadeOut complete. Remove the loading div
                     
                  $( "#loadingDiv" ).hide(); //makes page more lightweight   

                  if($('.portals-alert:visible').length >0 ){
                    $('.portals-alert:visible').attr('tabindex','0').focus();
                    setTimeout(function(){
                        $('.portals-alert:visible').removeAttr('tabindex').blur();
                    },3000);
                  }
          }
      });
     
});

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Developer Hub</title>
    <link href="$settings.cdnBaseURL/ssam/images/favicon.ico" rel="shortcut icon">
    <link href="$settings.cdnBaseURL/ssam/css/common.min.css" rel="stylesheet">
		<link href="$settings.cdnBaseURL/ssam/css/main.min.css" rel="stylesheet">
	<noscript>
						<style>#loadingDiv{ display:none; }</style>
            Please enable Javascript on your browser to use this application.
  </noscript>
  </head>
  <body class="bg">
	<div class="main-container no-display-disabled-cookie-js">
	<div>
		#parse("_nav.vm")
	</div >
	<div class="container container-margin cookie-error">
        <div class="row">
            <div class="container">
                    <div class="alert portals-alert portals-alert-danger col-xs-12 col-sm-12 col-md-12">
                            <p>
                                <span class="alert-prefix">Error:</span>
                                <span class="alert-suffix text-link bank-link">Please enable cookies on your browser to use this application. For more information see our <a href="https://www.bankofireland.com/legal/privacy-statement">Cookie and Privacy policy.</a></span>
                            </p>
                    </div>
            </div>
        </div>
    </div>
    <div class="container container-margin no-display-disabled-cookie-js">
			<div class="row">
		    <div class="col-md-6 offset-md-3">
					<h2 class="form-header">
                    Resend activation link
          </h2>
					<div class="form-container">
					<span id="errorAlerts">
						#if($error)
						<div class="alert portals-alert portals-alert-danger col-xs-12 col-sm-12 col-md-12" >
                            <p>
                                <span class="alert-prefix">Error:</span>
                                <span class="alert-suffix text-link bank-link" id="formErrorMsg">$!error</span>
                            </p>
            </div>
						#end
					</span>
					<div class="signup-banner">Please provide your email address for activation</div>
					<form id="resendForm" action="resendRegistrationLink" method="POST" autocomplete="false" onkeydown="validateFormRequired(this.id,'keydown')">
					  <input type="hidden" name="$_csrf.parameterName" value="$_csrf.token" />
						<input type="hidden" id="formErrorCode" value="$error">
					  <div class="form-group">
						<span class="required-sign" aria-hidden="true">*</span>
						<label for="emailId" class="devportal-input-label"><span class="required-sign sr-only">*&nbsp;</span>Email</label>
						<div class="row">
						  <div class="col-md-12">
							<input id="emailId" type="text" name="emailId" value="$!emailId" placeholder="Example@boi.com" maxlength="50" onblur="validateInput(this, 'email', true, true)" onfocus="focusInput(this)" onkeydown="checkKeyDown(this)" autocomplete="off">
							<span aria-hidden="true" class="err-content">Please enter a valid email address.</span>
						  </div>
						</div>
					  </div>

					  <input type="hidden" id="status" value="$status">
						<input type="hidden" id="devportalHost" name="devportalHost" value="$settings.devportalHost">

					  <div class="row">
					  <div class="col-md-12">
						    <button id="submitButton" type="submit" class="btn btn-submit float-right" disabled>
									Submit
							</button>
							<button id="cancelButton" type="button" class="btn btn-cancel float-left btn-resend-cancel" onkeydown="GoHome('keydown')" onmousedown="(event.which==1)?GoHome():false" >Cancel</button>
							
						</div>
						
					</div>
					</form>
				</div>
			
			</div>
		</div>
	</div>

<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-12">

						<ul class="footer-links">
							<li>
								<a href="$settings.bankCookiePrivacyPolicyURL" onmousedown="(event.which==1)?window.open(this.href,'_blank'):false;" onclick="window.open(this.href,'_blank');return false;">
									<span  title="Link will open in a new window.">Cookie &amp; Privacy policy</span>
									<span class="sr-only" aria-label=" Link will open in a new window"></span>
								</a>
								<span class="pipe">|</span>
							</li>
							<li>
								<a href="$settings.bankTermsOfServiceURL" onmousedown="(event.which==1)?window.open(this.href,'_blank'):false;" onclick="window.open(this.href,'_blank');return false;">
									<span  title="Link will open in a new window.">Terms of use</span>
									<span class="sr-only" aria-label=" Link will open in a new window"></span>
								</a>
								<span class="pipe">|</span>
							</li>
							<li>
								<a href="$settings.bankHelpURL" onmousedown="(event.which==1)?window.open(this.href,'_blank'):false;" onclick="window.open(this.href,'_blank');return false;">
									<span  title="Link will open in a new window.">Help</span>
									<span class="sr-only" aria-label=" Link will open in a new window"></span>
								</a>
							</li>
            </ul>

					</div>
				</div>

				<p class="regulatory-statement">Bank of Ireland is regulated by the Central Bank of Ireland. Bank of Ireland (UK) plc is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority.</p>

			</div>
</footer>
</div>

<div class="modal fade" id="RegistrationSuccess" tabindex="-1" role="dialog" aria-labelledby="RegistrationSuccessLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="RegistrationSuccessLabel">Resend activation link</h3>
     </div>
      <div class="modal-body text-center">
        <p>Your request has been submitted successfully.</p>
        <p class="modal-body-para">An activation link has been sent to your registered email address, follow the instructions to complete registration.</p>
			
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-submit btn-block" onclick="GoHome()">OK</button>
      </div>
    </div>
  </div>
</div>
<div style="" id="loadingDiv"><div class="loader"></div></div>

   <script integrity="$settings.cdnLibsHashCode" crossorigin="anonymous" src="$settings.cdnBaseURL/ssam/js/libs.min.js"></script>
   <script integrity="$settings.cdnAppHashCode" crossorigin="anonymous" src="$settings.cdnBaseURL/ssam/js/app.min.js"></script>
    ## reCAPTCHA script
    #if ($settings.isRecaptchaEnabled())
      <script src="$settings.cdnBaseURL/ssam/js/api.js"></script>
    #end

		<script>
		//In JS Enabled Case
				$(".main-container.no-display-disabled-cookie-js").removeClass("no-display-disabled-cookie-js");
		//In Cookie Disabled/Enabled Case
				if (!window.navigator.cookieEnabled) {
            $(".container.container-margin.cookie-error").removeClass("cookie-error");
        }else{
					$(".container.container-margin.no-display-disabled-cookie-js").removeClass("no-display-disabled-cookie-js");
				}
		</script>

    <script type="text/javascript">
      $(document).ready(function() {

				$("#RegistrationSuccess").modal({
						show: false,
						backdrop: 'static',
						keyboard: false
				});

        if($("#status").val() == "SUCCESS"){
            $("#RegistrationSuccess").modal("show");
            $(".main-container").attr("aria-hidden", "true");
        } else{
        }
				
				$('[data-toggle="tooltip"]').tooltip();
				$('.skip-main').on('click',function(event){
									event.preventDefault();
									$('#emailId').focus();
							});
				});

				var formErrorMsg="";
				if($("#formErrorCode").val() == "SSAM_ACTVN_EMAIL_NOT_FOUND")
					formErrorMsg = "Email not registered. Please try again with a registered email.";
				else if($("#formErrorCode").val() == "SSAM_ACTVN_ALREADY_ACTIVATED")
					formErrorMsg = "Account has already been activated. You can attempt to login.";
				else
					formErrorMsg = "Unable to process your request. Please try again later.";
				
				if(formErrorMsg !== "")
					$("#formErrorMsg").html(formErrorMsg);

				function GoHome(eventButton){
					if (eventButton === "keydown") {
            if (window.event.keyCode !== 13) {
                return;
            }
        	}
					event.preventDefault();
					window.location.href = $("#devportalHost").val();
				}
    </script>
  </body>
</html>

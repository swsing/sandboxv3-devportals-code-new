package com.capgemini.portal.ssam.util;

import static com.capgemini.portal.ssam.util.Constants.ALGORITHM;
import static com.capgemini.portal.ssam.util.Constants.SHA256;
import static javax.crypto.Cipher.DECRYPT_MODE;
import static javax.crypto.Cipher.ENCRYPT_MODE;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public final class AESEncDecUtil {
	/**
	 * Private constructor as only static members present
	 */
	private AESEncDecUtil() {
		// NO OP
	}

	public static final AESEncDecUtil getInstance() {
		return new AESEncDecUtil();
	}

	protected static final byte[] SECRETKEY = new byte[] { '$', 'a', 'a', 'm', 'D', 'e', 'v', 'P', '0', 'r', 't', 'a',
			'l', '#', 'V', '2' };

	/**
	 * Encrypt a string with AES algorithm.
	 *
	 * @param data
	 *            The string to encrypt
	 * @throws NoSuchAlgorithmException
	 *             Throws exception if any error
	 * @throws NoSuchPaddingException
	 *             Throws exception if any error
	 * @throws InvalidKeyException
	 *             Throws exception if any error
	 * @throws BadPaddingException
	 *             Throws exception if any error
	 * @throws IllegalBlockSizeException
	 *             Throws exception if any error
	 * @return java.lang.String the encrypted string
	 */
	public static String encrypt(String data) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHM);
		c.init(ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(data.getBytes());
		return Base64.getUrlEncoder().encodeToString(encVal);
	}

	/**
	 * Decrypt a string with AES algorithm.
	 *
	 * @param encryptedData
	 *            The encrypted string
	 * @throws NoSuchAlgorithmException
	 *             Throws exception if any error
	 * @throws NoSuchPaddingException
	 *             Throws exception if any error
	 * @throws InvalidKeyException
	 *             Throws exception if any error
	 * @throws BadPaddingException
	 *             Throws exception if any error
	 * @throws IllegalBlockSizeException
	 *             Throws exception if any error
	 * @return java.lang.String The decrypted string
	 */
	public static String decrypt(String encryptedData) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHM);
		c.init(DECRYPT_MODE, key);
		byte[] decodedValue = Base64.getUrlDecoder().decode(encryptedData);
		byte[] decryptedValue = c.doFinal(decodedValue);
		return new String(decryptedValue);
	}

	/**
	 * Generate a new encryption key.
	 * 
	 * @return The Key object
	 * @throws NoSuchAlgorithmException
	 *             Throws exception if any error
	 */
	private static Key generateKey() throws NoSuchAlgorithmException {
		MessageDigest sha = MessageDigest.getInstance(SHA256);
		byte[] key = sha.digest(SECRETKEY);
		key = Arrays.copyOf(key, 16); // use only first 128 bit
		return new SecretKeySpec(key, ALGORITHM);
	}
}
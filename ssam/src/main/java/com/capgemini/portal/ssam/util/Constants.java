package com.capgemini.portal.ssam.util;

public final class Constants {
	public static final String ALGORITHM = "AES";
	public static final String AMPERSAND = "&";
	public static final String COMMA = ",";
	public static final String ENCODING = "UTF-8";
	public static final String EQUAL_SIGN = "=";
	public static final String SHA256 = "SHA-256";
	public static final String SLASH = "/";
	public static final String EMAIL_TEMPLATE_FILE_NAME = "emailMessage.html";
	public static final String CLASS_NAME = "com.unboundid.webapp.ssam.SSAMController";
	public static final String RECAPTCHA_PARAM_NAME = "g-recaptcha-response";
	public static final String TOKEN_ID = "ssam";
	public static final String CLIENT_SESSION_ID = "SSAM";
	public static final String ACCOUNT_DISABLED_ATTRIBUTE = "ds-pwp-account-disabled";
	public static final String CSRF_ATTRIBUTE = "_csrf";
	public static final String EMPTY_STRING = "";
	public static final String ERROR_ATTRIBUTE = "error";
	public static final String REGISTERED_ATTRIBUTE = "isRegistered";
	public static final String STATUS_ATTRIBUTE = "status";
	public static final String SUCCESS_ATTRIBUTE = "success";
	public static final String RESULT_ATTRIBUTE = "result";
	public static final String RECIPIENTID_ATTRIBUTE = "recipientID";
	public static final String RECOVERYDN_ATTRIBUTE = "recoveryDN";
	public static final String USERNAME_ATTRIBUTE = "username";
	public static final String USERDN_ATTRIBUTE = "userDN";
	public static final String USE_RECAPTCHA_ATTRIBUTE = "useRecaptcha";
	public static final String REQUIREMENTS_BLOCK = "passwordRequirements";
	public static final String DELETION_SUCCESS_VIEW = "deletion-success";
	public static final String LOGIN_VIEW = "login";
	public static final String REGISTER_VIEW = "register";
	public static final String REGISTRATION_SUCCESS_VIEW = "registration-success";
	public static final String REGISTRATION_VERIFY_VIEW = "registration-verify";
	public static final String RESEND_REGISTRATION_VIEW = "resend-link";
	public static final String USER_PROFILE_VIEW = "user";
	public static final String RECOVER_VERIFY_VIEW = "recover-password-verify";
	public static final String RECOVER_SUCCESS_VIEW = "recover-password-success";
	public static final String VERIFY_ERROR_MESSAGE = "SSAM_ACTVN_LINK_EXPIRED";//"Failed to verify your link because of missing or invalid information or link has expired. You can request another link to be sent.";
	public static final String X_XSS_PROTECTION_HEADER = "X-XSS-Protection";
	public static final String X_XSS_PROTECTION_VALUE = "1; mode=block";
	public static final String CACHE_CONTROL_HEADER = "Cache-Control";
	public static final String CACHE_CONTROL_VALUE = "no-cache,no-store,must-revalidate";
	public static final String PRAGMA_HEADER = "Pragma";
	public static final String PRAGMA_VALUE = "no-cache";
	public static final String X_FRAME_OPTIONS_HEADER = "X-Frame-Options";
	public static final String X_FRAME_OPTIONS_VALUE = "DENY";
	public static final String X_CONTENT_TYPE_OPTIONS_HEADER = "X-Content-Type-Options";
	public static final String X_CONTENT_TYPE_OPTIONS_VALUE = "nosniff";
	public static final String INSTANCE_ROOT = "INSTANCE_ROOT";
	public static final String CONFIG_FILE_PATH = "spring.config.location";
	public static final String APP_PROPS_RELATIVE_PATH = "webapps/ssam-config/application.properties";
	public static final String SSAM_INSTALL_CMD = "install";
	public static final String LDAP_EXCEPTION_MSG = "There is LDAP exception occurred at ";
	public static final String TECHNICAL_EXCEPTION_MSG = "There is technical error occurred at ";
	
	/**
	 * Private constructor as only static members present
	 */
	private Constants() {
		// Private constructor
	}
	
	public static final Constants getInstance() {
		return new Constants();
	}
}

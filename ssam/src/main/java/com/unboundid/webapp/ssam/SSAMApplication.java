/*
 * Copyright 2015-2016 UnboundID Corp.
 *
 * All Rights Reserved.
 */
package com.unboundid.webapp.ssam;

import java.io.File;
import java.io.InputStream;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.unboundid.ldap.sdk.LDAPConnectionPool;
import com.unboundid.util.json.LDAPConnectionDetailsJSONSpecification;
import static com.capgemini.portal.ssam.util.Constants.*;

/**
 * The is the main Self-Service Account Manager Spring Boot application class
 * that defines the application configuration.
 */
@SpringBootApplication
public class SSAMApplication extends SpringBootServletInitializer implements ServletContextListener {
	private static final Logger LOG = LoggerFactory.getLogger(SSAMApplication.class);
	@Autowired
	private SSAMSettings settings;

	protected static final String[] UNAUTHENTICATED_PATHS = { "/register", "/validateLink", "/verifyRegistrationCode",
			"/resendRegistrationCode", "/resendRegistrationLink", "/resendRegistrationLink/**",
			"/resendRegistrationVerifyLink", "/resendRegistrationVerifyLink/**",
			"/initPassword", "/recoverPassword", "/resetPassword", "/verifyLinkRegistrationCode","/assets/**", "/fonts/**", "/css/**","/images/**","/js/**" };

	/**
	 * The default constructor is used to set the {@code spring.config.location}
	 * system property when running within a PingData server. The SSAM installer
	 * writes out a customized {@code application.properties} file under the
	 * server's {@code webapps/ssam-config} directory, which is what SSAM should
	 * use, if it exists.
	 */
	public SSAMApplication() {
		// If the application is running within a PingData server, an
		// INSTANCE_ROOT environment variable will be set, so set the
		// "spring.config.location" system property to the location of that
		// file.
		String instanceRoot = System.getenv(INSTANCE_ROOT).replace(SLASH, File.separator);
		if (instanceRoot != null) {
			String relativePath = APP_PROPS_RELATIVE_PATH.replace(SLASH, File.separator);
			File applicationProperties = new File(instanceRoot, relativePath);
			if (applicationProperties.exists()) {
				System.setProperty(CONFIG_FILE_PATH, applicationProperties.getAbsolutePath());
			}
		}
	}

	/** Runs the stand-alone SSAM application. 
	 * @param args List of arguments
	 */
	public static void main(String[] args) {
		LOG.info("START: main() method");
		if (args.length > 0 && SSAM_INSTALL_CMD.equalsIgnoreCase(args[0])) {
			SSAMInstaller.main(args);
		} else {
			new SpringApplicationBuilder(SSAMApplication.class).run(args);
		}
		LOG.info("END: main() method");
	}

	/** {@inheritDoc} */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		// Register the Spring Boot application so that the web application is
		// configured appropriately when launched by the servlet container. This
		// is
		// needed when creating a Spring Boot deployable war.
		return application.sources(SSAMApplication.class);
	}

	/**
	 * Returns an LDAP connection pool bean that can be wired into other
	 * components, and will be closed when destroyed.
	 *
	 * @return The connection pool is returned
	 *
	 * @throws Exception
	 *             Thrown if there is a problem creating the pool
	 */
	@Bean(destroyMethod = "close")
	public LDAPConnectionPool ldapConnectionPool() throws Exception {
		try (InputStream is = settings.getLdapConnectionDetailsResource().getInputStream()) {
			LDAPConnectionDetailsJSONSpecification connectionDetails = LDAPConnectionDetailsJSONSpecification
					.fromInputStream(is);
			return connectionDetails.createConnectionPool(settings.getNumConnections(), settings.getNumConnections());
		}
	}

	/**
	 * The following empty interface implementation methods are needed for
	 * ServletContext initialization in some web application servers.
	 */

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		// Empty implementation
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		// Empty implementation
	}
}

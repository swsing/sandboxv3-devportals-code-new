package com.unboundid.webapp.ssam;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountIdentificationMappingAdapter;
import com.capgemini.psd2.aspect.PSD2Aspect;
import com.capgemini.psd2.aspect.PSD2AspectUtils;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;

@Configuration
@ComponentScan(basePackages="com.capgemini",
excludeFilters=@ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE,value= {PSD2Aspect.class,PSD2AspectUtils.class,AccountIdentificationMappingAdapter.class,AccountMappingAdapter.class,OBPSD2ExceptionUtility.class}))
public class SSAMConfiguration {
	
	/*@Bean
	public PortalFilter portalFilter(){
		return new PortalFilter();
	}*/
	
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}

	
}

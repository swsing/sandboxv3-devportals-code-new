/*
 * Copyright 2015-2016 UnboundID Corp.
 *
 * All Rights Reserved.
 */
package com.unboundid.webapp.ssam;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * Contains the settings for the Self-Service Account Manager application that
 * can be supplied via properties files, system properties, etc.
 */
/**
 * @author asing248
 *
 */
@Component
@ConfigurationProperties
public class SSAMSettings {
	private Resource ldapConnectionDetailsResource;
	private int numConnections;
	private String baseDN;
	private String[] objectClasses;
	private String namingAttribute;
	private String recoverPasswordSearchFilter;
	private String version;
	private String messageSubject;
	private String fullTextBeforeToken;
	private String fullTextAfterToken;
	private String compactTextBeforeToken;
	private String compactTextAfterToken;
	private String recaptchaSiteKey;
	private String recaptchaSecretKey;
	private String searchBindFilter;
	private int numberOfInputs;
	private boolean allowSelfServiceDelete;
	private String pingFederateLogoutURL;
	private String pingAccessLogoutURL;
	private String homeURL;
	private String federationHost;
	private String devportalHost;
	private String cdnBaseURL;
	private String bankHelpURL;
	private String bankTermsOfServiceURL;
	private String userSearchFilter;
	private String acctLockedFilter;
	private String bankFaqURL;
	private String bankMulePortalURL;
	private String bankCookiePrivacyPolicyURL; 
	private String cdnLibsHashCode;
	private String cdnAppHashCode;
	private String devportalOverviewURL;
	

	/**
	 * Returns the LDAP Connection Details resource, a Spring resource. There
	 * are multiple ways of specifying a resource path. The default path is
	 * "classpath:/ssam-ldap-connection-details.json," but can vary from a file
	 * path (file:///foo/ssam-ldap-connection-details.json) to a URL
	 * (http://example.com/ssam-ldap-connection-details.json).
	 * @return Resource LDAP connection resource
	 */
	public Resource getLdapConnectionDetailsResource() {
		return ldapConnectionDetailsResource;
	}

	/** Sets the LDAP Connection Details resource, a Spring resource. 
	 * @param ldapConnectionDetailsResource LDAP connection resource
	 */
	public void setLdapConnectionDetailsResource(Resource ldapConnectionDetailsResource) {
		this.ldapConnectionDetailsResource = ldapConnectionDetailsResource;
	}

	/** Returns the number of LDAP connections to use in the connection pool. 
	 * @return int Number of connections
	 */
	public int getNumConnections() {
		return numConnections;
	}

	/** Sets the number of LDAP connections to use in the connection pool.
	 * @param numConnections Number of connections
	 */
	public void setNumConnections(int numConnections) {
		this.numConnections = numConnections;
	}

	/** Returns the base DN under which user entries are located. 
	 * @return String Base DN string representation
	 */
	public String getBaseDN() {
		return baseDN;
	}

	/** Sets the base DN under which user entries are located. 
	 * @param baseDN String representation of baseDN entry
	 */
	public void setBaseDN(String baseDN) {
		this.baseDN = baseDN;
	}

	/** Returns the object classes to use when creating user entries.
	 * @return String[] Array of object classes
	 */
	public String[] getObjectClasses() {
		return objectClasses;
	}

	/** Sets the object class to use when creating user entries.
	 * @param objectClasses Array of object classes
	 */
	public void setObjectClasses(String[] objectClasses) {
		this.objectClasses = objectClasses;
	}

	/** Returns the RDN attribute of user entries. 
	 * @return String Naming attribute of user entries
	 */
	public String getNamingAttribute() {
		return namingAttribute;
	}

	/**
	 * Sets the RDN attribute of user entries.
	 * @param namingAttribute Naming attribute of user entries
	 */
	public void setNamingAttribute(String namingAttribute) {
		this.namingAttribute = namingAttribute;
	}

	/** Returns the version number. 
	 * @return String SSAM version {{placeholder}}
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the version number.
	 * @param version SSAM version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/** Returns the registration code subject message. 
	 * @return String subject message of registration code/link
	 */
	public String getMessageSubject() {
		return messageSubject;
	}

	/**
	 * Sets the registration code subject message.
	 * @param messageSubject Subject message
	 */
	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}

	/** Returns the full registration code preceding text. 
	 * @return String Text before the token value
	 */
	public String getFullTextBeforeToken() {
		return fullTextBeforeToken;
	}

	/**
	 * Sets the full registration code preceding text.
	 * @param fullTextBeforeToken Preceding text (before token value)
	 */
	public void setFullTextBeforeToken(String fullTextBeforeToken) {
		this.fullTextBeforeToken = fullTextBeforeToken;
	}

	/** Returns the full registration code succeeding text. 
	 * @return String Text after the token value
	 */
	public String getFullTextAfterToken() {
		return fullTextAfterToken;
	}

	/**
	 * Sets the full registration code succeeding text.
	 * @param fullTextAfterToken Succeeding text (after token value)
	 */
	public void setFullTextAfterToken(String fullTextAfterToken) {
		this.fullTextAfterToken = fullTextAfterToken;
	}

	/** Returns the compact registration code preceding text. 
	 * @return String Text before the token value
	 */
	public String getCompactTextBeforeToken() {
		return compactTextBeforeToken;
	}

	/**
	 * Sets the compact registration code preceding text.
	 * @param compactTextBeforeToken Preceding text (before token value)
	 */
	public void setCompactTextBeforeToken(String compactTextBeforeToken) {
		this.compactTextBeforeToken = compactTextBeforeToken;
	}

	/** Returns the compact registration code succeeding text. 
	 * @return String Text after the token value
	 */
	public String getCompactTextAfterToken() {
		return compactTextAfterToken;
	}

	/**
	 * Sets the compact registration code succeeding text.
	 * @param compactTextAfterToken Succeeding text (after token value)
	 */
	public void setCompactTextAfterToken(String compactTextAfterToken) {
		this.compactTextAfterToken = compactTextAfterToken;
	}

	/** Returns the reCAPTCHA site key. 
	 * @return String reCAPTCHA site key
	 */
	public String getRecaptchaSiteKey() {
		return recaptchaSiteKey;
	}

	/**
	 * Sets the reCAPTCHA site key.
	 * @param recaptchaSiteKey reCAPTCHA site key
	 */
	public void setRecaptchaSiteKey(String recaptchaSiteKey) {
		this.recaptchaSiteKey = recaptchaSiteKey;
	}

	/** Returns the reCAPTCHA secret key. 
	 * @return String reCAPTCHA secret key
	 */
	public String getRecaptchaSecretKey() {
		return recaptchaSecretKey;
	}

	/**
	 * Sets the reCAPTCHA secret key.
	 * @param recaptchaSecretKey reCAPTCHA secret key
	 */
	public void setRecaptchaSecretKey(String recaptchaSecretKey) {
		this.recaptchaSecretKey = recaptchaSecretKey;
	}

	/** Returns whether to allow self-service delete. 
	 * @return boolean If Self service delete action is allowed or not
	 */
	public boolean isSelfServiceDeleteEnabled() {
		return allowSelfServiceDelete;
	}

	/**
	 * Sets whether to allow self-service delete.
	 * @param allowSelfServiceDelete Set self service delete allow action
	 */
	public void setAllowSelfServiceDelete(boolean allowSelfServiceDelete) {
		this.allowSelfServiceDelete = allowSelfServiceDelete;
	}

	/** Checks if reCAPTCHA is available. 
	 * @return boolean Test whether reCAPTACHA is enabled
	 */
	public boolean isRecaptchaEnabled() {
		return StringUtils.isNotEmpty(recaptchaSecretKey) && StringUtils.isNotEmpty(recaptchaSiteKey);
	}

	/**
	 * Returns the LDAP search filter used when the password recovery flow
	 * searches for the account to recover. The value provided in the UI will be
	 * substituted for occurrences of "$0".
	 * @return String LDAP search filter for password recovery flow
	 */
	public String getRecoverPasswordSearchFilter() {
		return recoverPasswordSearchFilter;
	}

	/**
	 * Sets the LDAP search filter used when the password recovery flow searches
	 * for the account to recover. The value provided in the UI will be
	 * substituted for occurrences of "$0".
	 * @param recoverPasswordSearchFilter LDAP search filter for password recovery flow
	 */
	public void setRecoverPasswordSearchFilter(String recoverPasswordSearchFilter) {
		this.recoverPasswordSearchFilter = recoverPasswordSearchFilter;
	}

	/** Checks whether the registration code uses a single input. 
	 * @return boolean If single input is enabled or not
	 */
	public boolean isSingleInputEnabled() {
		return numberOfInputs <= 1;
	}

	/** Gets the number of inputs desired for the registration code. 
	 * @return int Number of Inputs required for user registration
	 */
	public int getNumberOfInputs() {
		return numberOfInputs;
	}

	/**
	 * Sets the number of inputs desired for the registration code.
	 * @param numberOfInputs Set number of inputs required for registration
	 */
	public void setNumberOfInputs(int numberOfInputs) {
		this.numberOfInputs = numberOfInputs;
	}

	/**
	 * Returns the LDAP search filter used during the authentication process.
	 * @return String LDAP Search bind filter for authentication
	 */
	public String getSearchBindFilter() {
		return searchBindFilter;
	}

	/**
	 * Sets the LDAP search filter used during the authentication process.
	 * 
	 * @param searchBindFilter Set LDAP Search bind filter for authentication
	 */
	public void setSearchBindFilter(String searchBindFilter) {
		this.searchBindFilter = searchBindFilter;
	}

	/** Returns the URL used for the PingFederate logout process. 
	 * @return String PingFederate SLO URL
	 */
	public String getPingFederateLogoutURL() {
		return pingFederateLogoutURL;
	}

	/**
	 * Sets the URL used for the PingFederate logout process.
	 * @param pingFederateLogoutURL PingFederate SLO URL
	 */
	public void setPingFederateLogoutURL(String pingFederateLogoutURL) {
		this.pingFederateLogoutURL = pingFederateLogoutURL;
	}

	/** Returns the URL used for the PingAccess logout process. 
	 * @return String PingAccess SLO URL
	 */
	public String getPingAccessLogoutURL() {
		return pingAccessLogoutURL;
	}

	/**
	 * Sets the URL used for the PingAccess logout process.
	 * @param pingAccessLogoutURL PingAccess SLO URL
	 */
	public void setPingAccessLogoutURL(String pingAccessLogoutURL) {
		this.pingAccessLogoutURL = pingAccessLogoutURL;
	}

	/** Returns the URL for the home navigation menu item. 
	 * @return String Home URL menu item if enabled
	 */
	public String getHomeURL() {
		return homeURL;
	}

	/**
	 * Sets the URL for the home navigation menu item.
	 * @param homeURL Home URL navigation menu item
	 */
	public void setHomeURL(String homeURL) {
		this.homeURL = homeURL;
	}

	/** Returns the DNS entry for the Ping Directory and PF server. 
	 * @return String DNS entry for the Ping Directory and PF host
	 */
	public String getFederationHost() {
		return federationHost;
	}

	/**
	 * Sets the DNS for the Ping Directory and PF server for email link placeholder.
	 * @param federationHost DNS entry for the Ping Directory and PF host
	 */
	public void setFederationHost(String federationHost) {
		this.federationHost = federationHost;
	}

	/**
	 * @return the devportalHost
	 */
	public String getDevportalHost() {
		return devportalHost;
	}

	/**
	 * @param devportalHost the devportalHost to set
	 */
	public void setDevportalHost(String devportalHost) {
		this.devportalHost = devportalHost;
	}
	
	/**
	 * @return the cdnBaseURL
	 */
	public String getCdnBaseURL() {
		return cdnBaseURL;
	}

	/**
	 * @param cdnBaseURL the cdnBaseURL to set
	 */
	public void setCdnBaseURL(String cdnBaseURL) {
		this.cdnBaseURL = cdnBaseURL;
	}

	/**
	 * @return the bankHelpURL
	 */
	public String getBankHelpURL() {
		return bankHelpURL;
	}

	/**
	 * @param bankHelpURL the bankHelpURL to set
	 */
	public void setBankHelpURL(String bankHelpURL) {
		this.bankHelpURL = bankHelpURL;
	}
	
	/**
	 * @return the bankTermsOfServiceURL
	 */
	public String getBankTermsOfServiceURL() {
		return bankTermsOfServiceURL;
	}

	/**
	 * @param bankTermsOfServiceURL the bankTermsOfServiceURL to set
	 */
	public void setBankTermsOfServiceURL(String bankTermsOfServiceURL) {
		this.bankTermsOfServiceURL = bankTermsOfServiceURL;
	}

	/**
	 * Returns the LDAP search filter used for searching the user.
	 * @return String LDAP Search filter for user search
	 */
	public String getUserSearchFilter() {
		return userSearchFilter;
	}

	/**
	 * Sets the LDAP search filter used for checking if the user is already activated.
	 * 
	 * @param acctLockedFilter Set LDAP Search filter for checking if the user is already activated
	 */
	public void setAcctLockedFilter(String acctLockedFilter) {
		this.acctLockedFilter = acctLockedFilter;
	}

	/**
	 * Returns the LDAP search filter used for checking if the user is already activated
	 * 
	 * @return String LDAP Search filter for checking if the user is already activated
	 */
	public String getAcctLockedFilter() {
		return acctLockedFilter;
	}

	/**
	 * Sets the LDAP search filter used for searching the user.
	 * 
	 * @param userSearchFilter Set LDAP Search filter for user search
	 */
	public void setUserSearchFilter(String userSearchFilter) {
		this.userSearchFilter = userSearchFilter;
	}

	
	/**
	 * @return the bankFaqURL
	 */
	public String getBankFaqURL() {
		return bankFaqURL;
	}

	/**
	 * @param bankFaqURL the bankFaqURL to set
	 */
	public void setBankFaqURL(String bankFaqURL) {
		this.bankFaqURL = bankFaqURL;
	}

	/**
	 * @return the bankMulePortalURL
	 */
	public String getBankMulePortalURL() {
		return bankMulePortalURL;
	}

	/**
	 * @param bankMulePortalURL the bankMulePortalURL to set
	 */
	public void setBankMulePortalURL(String bankMulePortalURL) {
		this.bankMulePortalURL = bankMulePortalURL;
	}

	
	/**
	 * @return the bankCookiePrivacyPolicyURL
	 */
	public String getBankCookiePrivacyPolicyURL() {
		return bankCookiePrivacyPolicyURL;
	}

	/**
	 * @param bankCookiePrivacyPolicyURL the bankCookiePrivacyPolicyURL to set
	 */
	public void setBankCookiePrivacyPolicyURL(String bankCookiePrivacyPolicyURL) {
		this.bankCookiePrivacyPolicyURL = bankCookiePrivacyPolicyURL;
	}
	
	

	/**
	 * @return the cdnLibsHashCode
	 */
	public String getCdnLibsHashCode() {
		return cdnLibsHashCode;
	}

	/**
	 * @param cdnLibsHashCode the cdnLibsHashCode to set
	 */
	public void setCdnLibsHashCode(String cdnLibsHashCode) {
		this.cdnLibsHashCode = cdnLibsHashCode;
	}

	/**
	 * @return the cdnAppHashCode
	 */
	public String getCdnAppHashCode() {
		return cdnAppHashCode;
	}

	/**
	 * @param cdnAppHashCode the cdnAppHashCode to set
	 */
	public void setCdnAppHashCode(String cdnAppHashCode) {
		this.cdnAppHashCode = cdnAppHashCode;
	}
	
	

	/**
	 * @return the devportalOverviewURL
	 */
	public String getDevportalOverviewURL() {
		return devportalOverviewURL;
	}

	/**
	 * @param devportalOverviewURL the devportalOverviewURL to set
	 */
	public void setDevportalOverviewURL(String devportalOverviewURL) {
		this.devportalOverviewURL = devportalOverviewURL;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}

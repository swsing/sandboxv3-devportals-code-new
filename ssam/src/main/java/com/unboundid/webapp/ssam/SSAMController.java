/*
 * Copyright 2015-2016 UnboundID Corp.
 *
 * All Rights Reserved.
 */
package com.unboundid.webapp.ssam;

import static com.capgemini.portal.ssam.util.Constants.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalSSAMLoggerUtilities;
import com.capgemini.portal.ssam.util.AESEncDecUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.Control;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.ExtendedResult;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPConnectionPool;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPResult;
import com.unboundid.ldap.sdk.LDAPSearchException;
import com.unboundid.ldap.sdk.Modification;
import com.unboundid.ldap.sdk.ModificationType;
import com.unboundid.ldap.sdk.ModifyRequest;
import com.unboundid.ldap.sdk.RDN;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldap.sdk.extensions.PasswordModifyExtendedRequest;
import com.unboundid.ldap.sdk.extensions.PasswordModifyExtendedResult;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldap.sdk.unboundidds.controls.IntermediateClientRequestControl;
import com.unboundid.ldap.sdk.unboundidds.controls.PasswordPolicyErrorType;
import com.unboundid.ldap.sdk.unboundidds.controls.PasswordPolicyRequestControl;
import com.unboundid.ldap.sdk.unboundidds.controls.PasswordPolicyResponseControl;
import com.unboundid.ldap.sdk.unboundidds.extensions.ConsumeSingleUseTokenExtendedRequest;
import com.unboundid.ldap.sdk.unboundidds.extensions.DeliverPasswordResetTokenExtendedRequest;
import com.unboundid.ldap.sdk.unboundidds.extensions.DeliverSingleUseTokenExtendedRequest;
import com.unboundid.ldap.sdk.unboundidds.extensions.DeliverSingleUseTokenExtendedResult;
import com.unboundid.ldap.sdk.unboundidds.extensions.GetPasswordQualityRequirementsExtendedRequest;
import com.unboundid.ldap.sdk.unboundidds.extensions.GetPasswordQualityRequirementsExtendedResult;
import com.unboundid.ldap.sdk.unboundidds.extensions.PasswordQualityRequirement;

/**
 * This is the main Spring MVC controller for the Self-Service Account Manager.
 */
@Controller
public class SSAMController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SSAMController.class);

	@Autowired
	private SSAMSettings settings;

	@Autowired
	private PortalSSAMLoggerUtilities portalSSAMLoggerUtilities;
	
	@Autowired
	private LDAPConnectionPool pool;

	@Autowired
	private Environment environment;

	private Schema schema;

	private DN baseDN;

	@Autowired
	private PortalAspectUtils portalAspectUtils;
	

	/**
	 * Get the schema.
	 * 
	 * @throws LDAPException
	 *             Throws exception when error occurs
	 */
	@PostConstruct
	public void initializeSchema() throws LDAPException {
		schema = pool.getSchema();
	}

	/**
	 * Get the base DN as a DN.
	 * 
	 * @throws LDAPException
	 *             Throws exception when error occurs
	 */
	@PostConstruct
	public void initializeBaseDN() throws LDAPException {
		baseDN = new DN(settings.getBaseDN());
	}

	/**
	 * Makes the HTTP request available in the model.
	 * 
	 * @param request
	 *            The HTTP request object
	 * @return HttpServletRequest The request object available in model
	 */
	@ModelAttribute("request")
	public HttpServletRequest request(HttpServletRequest request) {
		return request;
	}

	/**
	 * Makes the CSRF token in the specified request available in the model.
	 * This can be referenced within templates that need to perform POST
	 * requests.
	 *
	 * @param request
	 *            The HTTP request that contains the CSRF token attribute
	 *
	 * @return Returns the CSRF token to be put in the model
	 */
	@ModelAttribute(CSRF_ATTRIBUTE)
	public CsrfToken csrfToken(HttpServletRequest request) {
		return (CsrfToken) request.getAttribute(CsrfToken.class.getName());
	}

	/**
	 * Makes the SSAM Settings (and its attributes) available
	 * 
	 * @return SSAMSettings The settings object for SSAM
	 */
	@ModelAttribute("settings")
	public SSAMSettings settings() {
		return settings;
	}

	/**
	 * Makes whether or not the Ping authentication profile is active in the
	 * model.
	 * 
	 * @return boolean Whether Ping Auth profile is active or not
	 */
	@ModelAttribute("pingActive")
	public boolean pingActive() {
		return environment.acceptsProfiles("ping-authentication");
	}

	/**
	 * This is an exception handler for web application exceptions, which
	 * returns a response entity created via
	 * {@link WebApplicationException#toResponseEntity()}.
	 *
	 * @param e
	 *            The exception
	 *
	 * @return The response entity is returned with the exception's status code
	 *         and error message
	 */
	@ExceptionHandler(WebApplicationException.class)
	public ResponseEntity<String> handleWebApplicationException(WebApplicationException e) {
		return e.toResponseEntity();
	}

	/**
	 * Simply return the "login" view.
	 * 
	 * @param request
	 *            The HTTP request object
	 * @return String Returns the "login" view
	 */
	@RequestMapping("/login")
	public String login(HttpServletRequest request,HttpServletResponse response) {
		// Get the session, creating it if necessary, to make sure that the
		// session
		// cookie gets set on the browser. If the user goes directly to the
		// "login"
		// page and doesn't have a session cookie, CSRF validation will fail
		// because
		// the token that is posted in the login form cannot be correlated with
		// the
		// token in the session.
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("login", "/login", RequestMethod.GET.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute);
		request.getSession(true);
		portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
		populateResponseHeader(response);
		return LOGIN_VIEW;
	}

	/**
	 * Handles user view requests by getting the currently authenticated user's
	 * entry, populating the model with it, and returning the "user" view.
	 *
	 * @param model
	 *            The model
	 *
	 * @return The "user" view is returned upon success. Otherwise, the "error"
	 *         view is returned.
	 */
	@RequestMapping({ "/", "/user" })
	public String getUser(Model model,HttpServletRequest request ,HttpServletResponse response) {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("getUser", "/user", RequestMethod.GET.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute);
		// search for the user and put the user entry and attributes into the
		// model
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		try {
			Entry entry = getUserEntry();
			populateUserModel(username, entry, model);
			// insert password requirements to use in the user view
			model.addAttribute(REQUIREMENTS_BLOCK, getPasswordRequirements(entry.getDN()));
			portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
			populateResponseHeader(response);
			return USER_PROFILE_VIEW;
		} catch (LDAPException e) {
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			// if we can't get the entry, just display the error page
			model.addAttribute(ERROR_ATTRIBUTE, (String)LDAP_EXCEPTION_MSG.concat(loggerAttribute.getOperationName()));
			// portalAspectUtils.exception(CLASS_NAME, "user", e.getMessage();
			populateResponseHeader(response);
			return ERROR_ATTRIBUTE;
		}
	}

	/**
	 * Handles user update requests when the user form is submitted by modifying
	 * the currently authenticated user's entry with the values in the form.
	 *
	 * @param parameters
	 *            The user form parameters
	 * @param model
	 *            The model
	 *
	 * @return Returns the "user" view upon success or if there was an error
	 *         modifying the user, in which case the error will be displayed.
	 *         Otherwise, the "error" view is returned.
	 */
	@RequestMapping(value = "/user", method = RequestMethod.POST, consumes = APPLICATION_FORM_URLENCODED_VALUE)
	public String updateUser(@RequestParam Map<String, String> parameters, Model model,HttpServletRequest req,HttpServletResponse response) {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("updateUser", "/user", RequestMethod.POST.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute, parameters);
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		Entry targetEntry = null;
		try {
			// get the currently authenticated user's entry and make a copy of
			// it with
			// the provided changes
			Entry sourceEntry = getUserEntry();
			model.addAttribute(REQUIREMENTS_BLOCK, getPasswordRequirements(sourceEntry.getDN()));
			targetEntry = sourceEntry.duplicate();
			for (Map.Entry<String, String> e : parameters.entrySet()) {
				// only handle attributes defined in the schema
				String attribute = e.getKey();
				if (schema.getAttributeType(attribute) != null) {
					// either remove the value from the entry or update it
					String value = e.getValue().trim();
					if (EMPTY_STRING.equals(value)) {
						targetEntry.removeAttribute(attribute);
					} else {
						targetEntry.setAttribute(attribute, value);
					}
				}
			}

			// get the modifications required to update the entry and apply them
			List<Modification> mods = Entry.diff(sourceEntry, targetEntry, true);
			if (!mods.isEmpty()) {
				ModifyRequest request = new ModifyRequest(sourceEntry.getDN(), mods);
				request.addControl(getIntermediateClientRequestControl());
				pool.modify(sourceEntry.getDN(), mods);
			}
			populateUserModel(username, targetEntry, model);
			model.addAttribute(SUCCESS_ATTRIBUTE, "User changes were successfully saved.");
			portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
		} catch (LDAPException e) {
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			// if we couldn't even get the entry, something bad happened, so
			// return
			// the error view
			model.addAttribute(ERROR_ATTRIBUTE,  (String)LDAP_EXCEPTION_MSG.concat(loggerAttribute.getOperationName()));
			if (targetEntry == null) {
				model.addAttribute(USERNAME_ATTRIBUTE, username);
				return ERROR_ATTRIBUTE;
			}

			// there was some sort of error encountered, probably when trying to
			// modify the entry, so populate the model with everything needed to
			// render the "user" view
			Map<String, String> modelParameters = new HashMap<>(parameters);
			modelParameters.remove(CSRF_ATTRIBUTE);
			model.addAllAttributes(modelParameters);
			populateUserModel(username, targetEntry, model);
			
		}
		portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
		populateResponseHeader(response);
		return USER_PROFILE_VIEW;
	}

	/**
	 * Handles password update AJAX requests, changing the currently
	 * authenticated user's password to the specified value.
	 *
	 * @param currentPassword
	 *            The user's current password
	 * @param password
	 *            The new password for the user
	 *
	 * @return Returns a 200 status code and empty response upon success, or an
	 *         error status code and error message if there is an error
	 */
	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST, consumes = APPLICATION_FORM_URLENCODED_VALUE, produces = TEXT_PLAIN_VALUE)
	public ResponseEntity<String> updatePassword(@RequestParam("currentPassword") String currentPassword,
			@RequestParam("password") String password,HttpServletRequest req,HttpServletResponse response) {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("updatePassword", "/updatePassword", RequestMethod.POST.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute, currentPassword, password);
		Control[] controls = { getIntermediateClientRequestControl(), new PasswordPolicyRequestControl() };
		PasswordModifyExtendedRequest request = new PasswordModifyExtendedRequest(null, currentPassword, password,
				controls);
		try {
			PasswordModifyExtendedResult extendedResult = (PasswordModifyExtendedResult) pool
					.processExtendedOperation(request);
			ResultCode resultCode = extendedResult.getResultCode();
			if (resultCode == ResultCode.SUCCESS) {
				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
				return new ResponseEntity<>(HttpStatus.OK);
			} else if (resultCode == ResultCode.INVALID_CREDENTIALS) {
				String additionalInfo = EMPTY_STRING;
				if (extendedResult.hasResponseControl(PasswordPolicyResponseControl.PASSWORD_POLICY_RESPONSE_OID)) {
					additionalInfo += "Reason: ";
					Control[] responseControls = extendedResult.getResponseControls();
					String separator = EMPTY_STRING;
					for (final Control control : responseControls) {
						if (control.getOID().equals(PasswordPolicyResponseControl.PASSWORD_POLICY_RESPONSE_OID)) {
							final PasswordPolicyResponseControl responseControl = (PasswordPolicyResponseControl) control;
							additionalInfo += String.format("%s%s", separator,
									getPasswordPolicyErrorTypeMessage(responseControl.getErrorType()));
							separator = ", ";
						}
					}
				} else {
					additionalInfo = (extendedResult.getDiagnosticMessage() == null)
							? "Please verify that your old password is correct."
							: extendedResult.getDiagnosticMessage();
				}
				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
				// This will be returned if the "current password" is incorrect.
				populateResponseHeader(response);
				return new ResponseEntity<>("Your password could not be updated. " + additionalInfo,
						HttpStatus.BAD_REQUEST);
			} else {
				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
				populateResponseHeader(response);
				return new ResponseEntity<>(resultCode + " - " + extendedResult.getDiagnosticMessage(),
						HttpStatus.BAD_REQUEST);
			}

		} catch (LDAPException e) {
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			populateResponseHeader(response);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Handles self-service delete requests by hard-deleting the current user.
	 * 
	 * @param session
	 *            The session to be invalidated upon a successful deletion
	 * @param model
	 *            The model view representation
	 * @return String Returns the "deletion-success" view upon success, and the
	 *         "user" view otherwise
	 */
	@RequestMapping(value = "/deleteUser", method = RequestMethod.POST, consumes = APPLICATION_FORM_URLENCODED_VALUE)
	public String deleteUser(HttpSession session, Model model,HttpServletRequest request,HttpServletResponse response) {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("deleteUser", "/deleteUser", RequestMethod.POST.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		try {
			// The current code will fail if there are child entries under the
			// current
			// user. In order to successfully delete those accounts, a subtree
			// delete
			// request control may be used to handle the situation.
			// At the moment, there is no need for a subtree delete request
			// control.

			// request deletion of currently authenticated user
			Object userDetails = authentication.getPrincipal();

			if (userDetails instanceof LDAPUser && !StringUtils.isEmpty(((LDAPUser) userDetails).getDN())) {
				pool.delete(((LDAPUser) userDetails).getDN());
			} else {
				pool.delete(getUserEntry().getDN());
			}
			// deletion successful if this is reached
			if (!pingActive()) {
				// invalidate the session to mimic LDAP logout
				session.invalidate();
			}
			portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
			populateResponseHeader(response);
			return DELETION_SUCCESS_VIEW;
		} catch (LDAPException e) {
			model.addAttribute(ERROR_ATTRIBUTE, "There was an error deleting the account.");
			try {
				populateUserModel(authentication.getName(), getUserEntry(), model);
			} catch (LDAPException le) {
				portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), le);
			}
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			populateResponseHeader(response);
			return USER_PROFILE_VIEW;
		}
	}

	/**
	 * Handles registration view requests.
	 *
	 * @param request
	 *            The HTTP request
	 * @param model
	 *            The model to be populated with a list of Password Quality
	 *            Requirements
	 * @return Returns the "register" view
	 */
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(HttpServletRequest request, Model model,HttpServletResponse response) {
		// Get the session, creating it if necessary, to make sure that the
		// session
		// cookie gets set on the browser. If the user goes to the "register"
		// page
		// and doesn't have a session cookie, CSRF validation will fail because
		// the
		// token that is posted in the registration form cannot be correlated
		// with
		// the token in the session.
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("register", "/register", RequestMethod.GET.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute);
		request.getSession(true);

		// insert password requirements to use in the register view
		model.addAttribute(REQUIREMENTS_BLOCK, getPasswordRequirements(null));
		portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
		populateResponseHeader(response);
		return REGISTER_VIEW;
	}

	/**
	 * Handles user registration AJAX requests by adding a user entry using the
	 * attributes in the provided parameters and sending a one-time password to
	 * the user. The user's account will be disabled until after a successful
	 * consumption of the registration code provided through a single use token.
	 *
	 * @param parameters
	 *            The parameters that contain the user attributes and reCAPTCHA
	 *            (optional) code to validate
	 * @param request
	 *            The HTTP request
	 * @param session
	 *            The HTTP session, which will be populated with a "userDN"
	 *            attribute
	 * @param model
	 *            The HTTP model, which can be populated with error messages and
	 *            will be populated with the DeliverRegistrationCodeResult
	 *            "result" that contains the delivery mechanism and recipient ID
	 *
	 * @return The "register" view is returned if there is an error with the
	 *         reCAPTCHA or creating the user and the "registration-verify" view
	 *         is returned otherwise
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = APPLICATION_FORM_URLENCODED_VALUE)
	public String createUser(@RequestParam Map<String, String> parameters, HttpServletRequest request,
			HttpSession session, Model model,HttpServletResponse response) {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("createUser", "/register", RequestMethod.POST.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute, parameters);
		try {
			

			// verify reCAPTCHA
			verifyRecaptcha(parameters);

			// construct a list of attributes for the user based on the form
			// parameters
			List<Attribute> attributes = new ArrayList<>();
			for (String objectClass : settings.getObjectClasses()) {
				attributes.add(new Attribute("objectClass", objectClass.trim()));
			}
			attributes.add(new Attribute(ACCOUNT_DISABLED_ATTRIBUTE, "true"));
			String namingAttributeName = settings.getNamingAttribute();
			String namingAttributeValue = null;
			for (Map.Entry<String, String> e : parameters.entrySet()) {
				// only handle attributes that are defined in the schema
				String name = e.getKey();
				if (schema.getAttributeType(name) != null) {
					String value = e.getValue().trim();
					if (!value.isEmpty()) {
						attributes.add(new Attribute(name, value));
					}
					// take note of the naming attribute value for constructing
					// the DN
					if (name.equals(namingAttributeName)) {
						namingAttributeValue = value;
					}
				}
			}

			// make sure that the naming attribute was found
			if (namingAttributeValue == null) {
				model.addAttribute(ERROR_ATTRIBUTE,
						"A naming attribute was not provided for '" + namingAttributeName + "'");
				model.addAttribute(REQUIREMENTS_BLOCK, getPasswordRequirements(null));
				populateRegistrationModel(parameters, model);
				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
				populateResponseHeader(response);
				return REGISTER_VIEW;
			}

			String userSearchFilter = settings.getUserSearchFilter().replace("$0", namingAttributeValue);

			SearchResultEntry searchEntry = pool.searchForEntry(settings.getBaseDN(), SearchScope.SUB,
					userSearchFilter);
			if (searchEntry != null) {
				portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute),
						new LDAPSearchException(ResultCode.UNWILLING_TO_PERFORM, "SSAM_REG_EMAIL_EXISTS"));
				throw new LDAPSearchException(ResultCode.UNWILLING_TO_PERFORM, "SSAM_REG_EMAIL_EXISTS");
			} else {
				// create and add the user entry
				DN dn = new DN(new RDN(namingAttributeName, namingAttributeValue), baseDN);
				Entry entry = new Entry(dn, attributes);
				LDAPResult result = pool.add(entry);
				ResultCode resultCode = result.getResultCode();
				if (resultCode != ResultCode.SUCCESS) {
					model.addAttribute(ERROR_ATTRIBUTE, "SSAM_GENERIC_ERROR");
					model.addAttribute(REQUIREMENTS_BLOCK, getPasswordRequirements(null));
					populateRegistrationModel(parameters, model);
					portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
					populateResponseHeader(response);
					return REGISTER_VIEW;
				}

				// send a single use token with a registration code
				DeliverRegistrationCodeResult codeResult = deliverRegistrationCode(entry.getDN());

				// put the DN in the session & recipientID in model
				session.setAttribute(USERDN_ATTRIBUTE, dn.toString());
				final String email = dn.toString().split(COMMA, 2)[0].split(EQUAL_SIGN, 2)[1];
				model.addAttribute(RECIPIENTID_ATTRIBUTE, email);
				// put the code result in the session and model
				session.setAttribute(RESULT_ATTRIBUTE, codeResult);
				model.addAttribute(RESULT_ATTRIBUTE, codeResult);
				model.addAttribute(REGISTERED_ATTRIBUTE, "true");

				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
				populateResponseHeader(response);
				return REGISTER_VIEW;// SEP25
			}
		} catch (LDAPException e) {
			if (e instanceof LDAPSearchException && "SSAM_REG_EMAIL_EXISTS".equalsIgnoreCase(e.getMessage())) {
				model.addAttribute(ERROR_ATTRIBUTE, e.getMessage());
			} else {
				model.addAttribute(ERROR_ATTRIBUTE,
						(String) LDAP_EXCEPTION_MSG.concat(loggerAttribute.getOperationName()));
			}
			model.addAttribute(REQUIREMENTS_BLOCK, getPasswordRequirements(null));
			populateRegistrationModel(parameters, model);
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			populateResponseHeader(response);
			return REGISTER_VIEW;
		} catch (WebApplicationException e) {
			// add an accessible error to the model
			model.addAttribute(ERROR_ATTRIBUTE,
					"SSAM_GENERIC_ERROR");
			model.addAttribute(REQUIREMENTS_BLOCK, getPasswordRequirements(null));
			populateRegistrationModel(parameters, model);
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			populateResponseHeader(response);
			return REGISTER_VIEW;
		}
	}

	/**
	 * Handles "verify registration code" requests by consuming the
	 * single-use-token, and then enabling the account defined by the "userDN"
	 * session attribute
	 *
	 * @param parameters
	 *            The registration code to use when consuming the token
	 * @param session
	 *            The HTTP session, which is expected to contain a "userDN"
	 *            attribute
	 * @param model
	 *            The HTTP model, which is updated with errors resulting from
	 *            the consumption process of the single-use-token and/or the
	 *            delivery mechanism and recipient ID of a new single-use-token
	 *
	 * @return Returns the "registration-verify" view if there is an error with
	 *         consuming the single-use-token, and "registration-success"
	 *         otherwise
	 */
	@RequestMapping(value = "/verifyRegistrationCode", method = RequestMethod.POST, consumes = APPLICATION_FORM_URLENCODED_VALUE)
	public String verifyRegistrationCode(@RequestParam Map<String, String> parameters, HttpSession session,
			Model model,HttpServletRequest request,HttpServletResponse response) {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("verifyRegistrationCode", "/verifyRegistrationCode", RequestMethod.POST.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute, parameters);
		String email = null;
		try {
			String code = parameters.get("code");
			String userDN = (String) session.getAttribute(USERDN_ATTRIBUTE);
			email = userDN.split(COMMA, 2)[0].split(EQUAL_SIGN, 2)[1];

			ExtendedResult result = pool
					.processExtendedOperation(new ConsumeSingleUseTokenExtendedRequest(userDN, TOKEN_ID, code));

			if (result.getResultCode() != ResultCode.SUCCESS) {
				// add an error attribute to display an error
				model.addAttribute(ERROR_ATTRIBUTE, VERIFY_ERROR_MESSAGE);
				// add the result attribute and recipient ID
				model.addAttribute(RESULT_ATTRIBUTE, session.getAttribute(RESULT_ATTRIBUTE));
				model.addAttribute(RECIPIENTID_ATTRIBUTE, email);

				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
				populateResponseHeader(response);
				return REGISTRATION_VERIFY_VIEW;
			}

			// enable the account after verifying the Single-Use-Token
			pool.modify(
					new ModifyRequest(userDN, new Modification(ModificationType.DELETE, ACCOUNT_DISABLED_ATTRIBUTE)));

			// clean up the session
			session.removeAttribute(RESULT_ATTRIBUTE);
			session.removeAttribute(RECIPIENTID_ATTRIBUTE);

			portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
			populateResponseHeader(response);
			return REGISTRATION_SUCCESS_VIEW;
		} catch (LDAPException e) {
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			// add an error attribute to display an error
			model.addAttribute(ERROR_ATTRIBUTE, VERIFY_ERROR_MESSAGE);
			model.addAttribute(RESULT_ATTRIBUTE, session.getAttribute(RESULT_ATTRIBUTE));
			model.addAttribute(RECIPIENTID_ATTRIBUTE, email);
			portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
			populateResponseHeader(response);
			return REGISTRATION_VERIFY_VIEW;
		}
	}

	/**
	 * Handles "verify link registration code" requests by consuming the
	 * single-use-token, and then enabling the account defined by the
	 * constructed "userDN" attribute
	 *
	 * @param parameters
	 *            The registration code to use when consuming the token
	 * @param request
	 *            The HTTP request object
	 * @param session
	 *            The HTTP session, which is expected to contain a "userDN"
	 *            attribute
	 * @param model
	 *            The HTTP model, which is updated with errors resulting from
	 *            the consumption process of the single-use-token and/or the
	 *            delivery mechanism and recipient ID of a new single-use-token
	 *
	 * @return Returns the "registration-verify" view if there is an error with
	 *         consuming the single-use-token, and "registration-success"
	 *         otherwise
	 */
	@RequestMapping(value = "/verifyLinkRegistrationCode", method = RequestMethod.GET)
	public String verifyLinkRegistrationCode(@RequestParam Map<String, String> parameters, HttpServletRequest request,
			HttpSession session, Model model,HttpServletResponse response) {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("verifyLinkRegistrationCode", "/verifyLinkRegistrationCode", RequestMethod.GET.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute, parameters);
		String email = null;
		try {
			String code = request.getParameter("code");
			email = AESEncDecUtil.decrypt(code).split(AMPERSAND, 2)[0].split(EQUAL_SIGN, 2)[1];
			final String userDN = new StringBuilder().append(settings.getNamingAttribute()).append(EQUAL_SIGN)
					.append(email).append(COMMA).append(settings.getBaseDN()).toString();

			// Set userDN and recipientID for verification page
			session.setAttribute(USERDN_ATTRIBUTE, userDN);

			ExtendedResult result = pool
					.processExtendedOperation(new ConsumeSingleUseTokenExtendedRequest(userDN, TOKEN_ID, code));

			if (result.getResultCode() != ResultCode.SUCCESS) {
				// add an error attribute to display an error
				model.addAttribute(ERROR_ATTRIBUTE, VERIFY_ERROR_MESSAGE);
				// add the result attribute and recipient ID
				session.setAttribute(RESULT_ATTRIBUTE, result);
				model.addAttribute(RESULT_ATTRIBUTE, result);
				session.setAttribute(RECIPIENTID_ATTRIBUTE, email);
				model.addAttribute(RECIPIENTID_ATTRIBUTE, email);
				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
				populateResponseHeader(response);
				return REGISTRATION_VERIFY_VIEW;
			}

			// enable the account after verifying the Single-Use-Token
			pool.modify(
					new ModifyRequest(userDN, new Modification(ModificationType.DELETE, ACCOUNT_DISABLED_ATTRIBUTE)));

			// clean up the session
			session.removeAttribute(RESULT_ATTRIBUTE);
			session.removeAttribute(RECIPIENTID_ATTRIBUTE);

			portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
			populateResponseHeader(response);
			return REGISTRATION_SUCCESS_VIEW;
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException
				| IllegalBlockSizeException | RuntimeException | LDAPException e) {
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			// add an error attribute to display an error
			model.addAttribute(ERROR_ATTRIBUTE, VERIFY_ERROR_MESSAGE);
			// add the result attribute
			model.addAttribute(RESULT_ATTRIBUTE, session.getAttribute(RESULT_ATTRIBUTE));
            populateResponseHeader(response);
			return REGISTRATION_VERIFY_VIEW;
		}
	}

	/**
	 * Handles "resend registration code" AJAX requests by sending a single use
	 * token containing a registration code to the user defined by the "userDN"
	 * session attribute.
	 *
	 * @param session
	 *            The HTTP session, which is expected to contain a "userDN"
	 *            attribute
	 *
	 * @return Returns the "deliver registration code" result, which includes
	 *         the delivery mechanism and recipient ID
	 *
	 * @throws WebApplicationException
	 *             Thrown if there is a problem delivering the registration code
	 *             that is handled by "handleWebApplicationException"
	 */
	@ResponseBody
	@RequestMapping(value = "/resendRegistrationCode", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
	public DeliverRegistrationCodeResult resendRegistrationCode(HttpSession session,HttpServletRequest request,HttpServletResponse response) throws WebApplicationException {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("resendRegistrationCode", "/resendRegistrationCode", RequestMethod.POST.toString());
		
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute);
		populateResponseHeader(response);
		return deliverRegistrationCode((String) session.getAttribute(USERDN_ATTRIBUTE));
	}

	/**
	 * Handles resend-link view requests.
	 *
	 * @param request
	 *            The HTTP request
	 * @param model
	 *            The model to be populated with "status" attribute
	 * @return Returns the "resend-link" view
	 */
	@RequestMapping(value = "/resendRegistrationLink", method = RequestMethod.GET)
	public String resendRegistrationLink(HttpServletRequest request, Model model,HttpServletResponse response) {
		// Get the session, creating it if necessary, to make sure that the
		// session
		// cookie gets set on the browser. If the user goes to the
		// "resendRegistrationLink"
		// page
		// and doesn't have a session cookie, CSRF validation will fail because
		// the
		// token that is posted in the resend form cannot be correlated
		// with
		// the token in the session.
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("resendRegistrationLink", "/resendRegistrationLink", RequestMethod.GET.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute);
		request.getSession(true);

		// insert "status" attribute to use in the resend-link view
		model.addAttribute(STATUS_ATTRIBUTE, "REQUEST");
		portalAspectUtils.exitPayloadlogger(CLASS_NAME,portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
		populateResponseHeader(response);
		return RESEND_REGISTRATION_VIEW;
	}

	/**
	 * Handles "resend registration link" AJAX requests by sending a single use
	 * token containing a registration link to the user defined by the "userDN"
	 * session attribute.
	 *
	 * @param emailId
	 *            The placeholder for email address of recipient
	 * @param session
	 *            The HTTP session
	 * @param request
	 *            The HTTP request
	 * @param model
	 *            The model to be populated with "status" attribute
	 * @return Returns the result, which includes the delivery mechanism and
	 *         recipient ID
	 *
	 * @throws WebApplicationException
	 *             Thrown if there is a problem delivering the registration code
	 *             that is handled by "handleWebApplicationException"
	 */
	@RequestMapping(value = "/resendRegistrationLink", method = RequestMethod.POST)
	public String resendRegistrationLink(@RequestParam("emailId") String emailId, HttpServletRequest request,
			HttpSession session, Model model,HttpServletResponse response) throws WebApplicationException {

		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("resendRegistrationLink", "/resendRegistrationLink", RequestMethod.POST.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute, emailId);

		DeliverRegistrationCodeResult result = null;

		try {
			final String userDN = new StringBuilder().append(settings.getNamingAttribute()).append(EQUAL_SIGN)
					.append(emailId).append(COMMA).append(settings.getBaseDN()).toString();

			// search for the user
			String filter = settings.getUserSearchFilter().replace("$0", emailId);

			SearchResultEntry entry = pool.searchForEntry(settings.getBaseDN(), SearchScope.SUB, filter);
			if (entry == null) {
				// the user couldn't be found, but we don't want to alert the
				// user in order to prevent phishing
				portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute),
						new WebApplicationException(HttpStatus.NOT_FOUND, "SSAM_ACTVN_EMAIL_NOT_FOUND"));
				throw new WebApplicationException(HttpStatus.NOT_FOUND, "SSAM_ACTVN_EMAIL_NOT_FOUND");
			}

			String acctLockedFilter = settings.getAcctLockedFilter().replace("$0", emailId);

			entry = pool.searchForEntry(settings.getBaseDN(), SearchScope.SUB, acctLockedFilter);
			if (entry == null) {
				portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute),
						new LDAPSearchException(ResultCode.UNWILLING_TO_PERFORM, "SSAM_ACTVN_ALREADY_ACTIVATED"));
				throw new LDAPSearchException(ResultCode.UNWILLING_TO_PERFORM, "SSAM_ACTVN_ALREADY_ACTIVATED");
			} else {
				result = deliverRegistrationCode(userDN);
			}
			portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
		} catch (WebApplicationException | LDAPSearchException e) {
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			if ("SSAM_ACTVN_EMAIL_NOT_FOUND".equalsIgnoreCase(e.getMessage()) || "SSAM_ACTVN_ALREADY_ACTIVATED".equalsIgnoreCase(e.getMessage()) ) {
				model.addAttribute(ERROR_ATTRIBUTE, e.getMessage());
			} else {
				model.addAttribute(ERROR_ATTRIBUTE,
						(String) LDAP_EXCEPTION_MSG.concat(loggerAttribute.getOperationName()));
			}
			model.addAttribute(STATUS_ATTRIBUTE, "ERROR");			
			populateResponseHeader(response);
			return RESEND_REGISTRATION_VIEW;
		}

		if (result != null) {
			model.addAttribute(STATUS_ATTRIBUTE, "SUCCESS");
		}
		portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
		populateResponseHeader(response);
		return RESEND_REGISTRATION_VIEW;
	}

	/**
	 * Handles "resend registration code" AJAX requests by sending a single use
	 * token containing a registration code to the user defined by the "userDN"
	 * session attribute.
	 *
	 * @param emailId
	 *            The placeholder for email address of recipient
	 * @param session
	 *            The HTTP session
	 * @param request
	 *            The HTTP request
	 * @param model
	 *            The model to be populated with "status" attribute
	 * @return Returns the result, which includes the delivery mechanism and
	 *         recipient ID
	 *
	 * @throws WebApplicationException
	 *             Thrown if there is a problem delivering the registration code
	 *             that is handled by "handleWebApplicationException"
	 */
	@RequestMapping(value = "/resendRegistrationVerifyLink", method = RequestMethod.POST)
	public String resendRegistrationVerifyLink(@RequestParam("emailId") String emailId, HttpServletRequest request,
			HttpSession session, Model model,HttpServletResponse response) throws WebApplicationException {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("resendRegistrationVerifyLink", "/resendRegistrationVerifyLink", RequestMethod.POST.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute);

		DeliverRegistrationCodeResult result = null;

		try {
			final String userDN = new StringBuilder().append(settings.getNamingAttribute()).append(EQUAL_SIGN)
					.append(emailId).append(COMMA).append(settings.getBaseDN()).toString();

			// search for the user
			String filter = settings.getUserSearchFilter().replace("$0", emailId);

			SearchResultEntry entry = pool.searchForEntry(settings.getBaseDN(), SearchScope.SUB, filter);
			if (entry == null) {
				// the user couldn't be found, but we don't want to alert the
				// user in order to prevent phishing
				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
				portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute),
						new WebApplicationException(HttpStatus.NOT_FOUND, "SSAM_ACTVN_EMAIL_NOT_FOUND"));
				throw new WebApplicationException(HttpStatus.NOT_FOUND, "SSAM_ACTVN_EMAIL_NOT_FOUND");
			}

			String acctLockedFilter = settings.getAcctLockedFilter().replace("$0", emailId);

			entry = pool.searchForEntry(settings.getBaseDN(), SearchScope.SUB, acctLockedFilter);
			if (entry == null) {
				portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute),
						new LDAPSearchException(ResultCode.UNWILLING_TO_PERFORM, "SSAM_ACTVN_ALREADY_ACTIVATED"));
				throw new LDAPSearchException(ResultCode.UNWILLING_TO_PERFORM, "SSAM_ACTVN_ALREADY_ACTIVATED");
			} else {
				result = deliverRegistrationCode(userDN);
			}
		} catch (WebApplicationException | LDAPSearchException e) {

			if ("SSAM_ACTVN_EMAIL_NOT_FOUND".equalsIgnoreCase(e.getMessage()) || "SSAM_ACTVN_ALREADY_ACTIVATED".equalsIgnoreCase(e.getMessage()) ) {
				model.addAttribute(ERROR_ATTRIBUTE, e.getMessage());
			} else {
				model.addAttribute(ERROR_ATTRIBUTE, "Failed to send activation link : " +  (String)LDAP_EXCEPTION_MSG.concat(loggerAttribute.getOperationName()));
			}
			
			model.addAttribute(STATUS_ATTRIBUTE, "ERROR");
			model.addAttribute(RECIPIENTID_ATTRIBUTE, emailId);
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute),e);
			populateResponseHeader(response);
			return REGISTRATION_VERIFY_VIEW;
		}

		if (result != null) {
			model.addAttribute(STATUS_ATTRIBUTE, "SUCCESS");
		}
		portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
		populateResponseHeader(response);
		return RESEND_REGISTRATION_VIEW;
	}

	/**
	 * Simply return the "recover-password" view.
	 * 
	 * @param request
	 *            The HTTP request object
	 * @return String The recover password view
	 */
	@RequestMapping(value = "/recoverPassword", method = RequestMethod.GET)
	public String recoverPassword(HttpServletRequest request,HttpServletResponse response) {
		// Get the session, creating it if necessary, to make sure that the
		// session
		// cookie gets set on the browser. If the user goes to the "recover"
		// page
		// and doesn't have a session cookie, CSRF validation will fail because
		// the
		// token that is posted in the recovery form cannot be correlated with
		// the
		// token in the session. Also, clean up the session if things were put
		// into
		// it by a failed recovery attempt.
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("recoverPassword", "/recoverPassword", RequestMethod.GET.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute);
		HttpSession session = request.getSession(true);
		session.removeAttribute(RECOVERYDN_ATTRIBUTE);
		session.removeAttribute(USE_RECAPTCHA_ATTRIBUTE);
		portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
		populateResponseHeader(response);
		return "recover-password";
	}

	/**
	 * Handles the initial portion of the password recovery flow by searching
	 * for the specified user using the configured recover password search
	 * filter, and delivering a password reset token to the user using a
	 * delivery mechanism chosen by the server. Any errors are swallowed and not
	 * presented to the user in order to prevent phishing.
	 *
	 * @param parameters
	 *            The form parameters, which are expected to contain
	 *            "identifier" and reCAPTCHA (optional) code to validate
	 * @param session
	 *            The session, which will be populated with a "recoveryDN"
	 *            attribute
	 * @param model
	 *            The model, which will be populated with an "identifier"
	 *            attribute if there is a problem moving on to
	 *            "recover-password-verify," and a "recaptchaError" attribute if
	 *            a reCAPTCHA (optional) alert needs to be shown
	 *
	 * @return The "recover-password" view is returned if there is an error with
	 *         the reCAPTCHA response and "recover-password-verify" view is
	 *         returned otherwise
	 *
	 * @throws WebApplicationException
	 *             Thrown if there is a problem with the reCAPTCHA (optional)
	 *             response
	 */
	@RequestMapping(value = "/recoverPassword", method = RequestMethod.POST, consumes = APPLICATION_FORM_URLENCODED_VALUE)
	public String deliverPasswordResetToken(@RequestParam Map<String, String> parameters, HttpSession session,
			Model model,HttpServletRequest request,HttpServletResponse response) throws WebApplicationException {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("deliverPasswordResetToken", "/recoverPassword", RequestMethod.POST.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute, parameters);
		String identifier = parameters.get("identifier");
		String filter = settings.getRecoverPasswordSearchFilter().replace("$0", identifier);
		// insert password requirements to use in the recover-password-verify
		// view
		model.addAttribute(REQUIREMENTS_BLOCK, getPasswordRequirements(null));
		try {
			// verify reCAPTCHA
			verifyRecaptcha(parameters);

			// search for the user
			SearchResultEntry entry = pool.searchForEntry(settings.getBaseDN(), SearchScope.SUB, filter);
			if (entry == null) {
				// the user couldn't be found, but we don't want to alert the
				// user in
				// order to prevent phishing
				//LOG.info("User search using the following filter did not return any " + "entries:  {}", filter);
				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
				populateResponseHeader(response);
				return RECOVER_VERIFY_VIEW;
			}

			// deliver a password reset token
			String dn = entry.getDN();
			DeliverPasswordResetTokenExtendedRequest deliverPasswordResetTokenRequest = new DeliverPasswordResetTokenExtendedRequest(
					dn, "Password Change Code", "Password Change Code: ", null, "Password Change Code: ", null, null,
					new Control[0]);
			ExtendedResult result = pool.processExtendedOperation(deliverPasswordResetTokenRequest);
			if (result.getResultCode() != ResultCode.SUCCESS) {
				// just log an error since we want to prevent phishing
				/*LOG.error(
						"Encountered error while processing "
								+ "DeliverPasswordResetTokenExtendedRequest for user with " + "DN '{}':  {}",
						dn, result);*/
				
				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), "Encountered error while processing "
						+ "DeliverPasswordResetTokenExtendedRequest for user with " + "DN '{}':  {}");
			}

			// put the DN in the session
			session.setAttribute(RECOVERYDN_ATTRIBUTE, dn);
			// set personalized password requirements to use in the
			// recover-password-verify view
			model.addAttribute(REQUIREMENTS_BLOCK, getPasswordRequirements(dn));
		} catch (LDAPException e) {
			// just log an error since we want to prevent phishing
			if (e.getResultCode() == ResultCode.SIZE_LIMIT_EXCEEDED)
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
		} catch (WebApplicationException e) {
			// add an accessible error to display alert
			// if any other web application exceptions occur, we do not want to
			// let
			// the user know to prevent phishing
			model.addAttribute("recaptchaError", true);
			// update the form to keep the UID when page refreshes
			model.addAttribute("identifier", identifier);
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			populateResponseHeader(response);
			return "recover-password";
		}
		portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
		populateResponseHeader(response);
		return RECOVER_VERIFY_VIEW;
	}

	/**
	 * Completes the password recovery flow by resetting the password using the
	 * password reset token.
	 *
	 * @param parameters
	 *            The form parameters, which are expected to contain "code" and
	 *            "password" and reCAPTCHA (optional) code to validate
	 * @param session
	 *            The session, which is expected to be populated with a
	 *            "recoveryDN" attribute and a "useRecaptcha" attribute if a
	 *            reCAPTCHA (optional) needs to be shown
	 * @param model
	 *            The model, which will be populated with an "error" attribute
	 *            if there is a problem, a "recaptchaErrorAlert" attribute if a
	 *            reCAPTCHA alert needs to be shown, and a "useRecaptcha"
	 *            attribute if reCAPTCHA (optional) needs to be shown
	 *
	 * @return The "recover-password-success" view is returned upon success. If
	 *         the a password reset token wasn't previously sent to the user
	 *         successfully, or there is a problem resetting the password, the
	 *         "recover-password-verify" view is return with an error in the
	 *         model.
	 *
	 * @throws WebApplicationException
	 *             Thrown if there is a problem with the reCAPTCHA (optional)
	 *             response
	 */
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST, consumes = APPLICATION_FORM_URLENCODED_VALUE)
	public String resetPassword(@RequestParam Map<String, String> parameters, HttpSession session, Model model,HttpServletRequest request,HttpServletResponse response)
			throws WebApplicationException {
		PortalLoggerAttribute loggerAttribute = portalSSAMLoggerUtilities.populateLoggerData("resetPassword", "/resetPassword", RequestMethod.POST.toString());
		portalAspectUtils.enterPayloadlogger(CLASS_NAME, loggerAttribute, parameters);
		boolean useRecaptcha = session.getAttribute(USE_RECAPTCHA_ATTRIBUTE) != null;
		// checks if the session needs a reCAPTCHA
		model.addAttribute(USE_RECAPTCHA_ATTRIBUTE, useRecaptcha);

		// if there is no recovery DN in the session, the user didn't have a
		// token
		// successfully sent to them, so go back to the "verify" page with an
		// error
		String userDN = (String) session.getAttribute(RECOVERYDN_ATTRIBUTE);
		model.addAttribute(REQUIREMENTS_BLOCK, getPasswordRequirements(null));
		if (userDN == null) {
			model.addAttribute(ERROR_ATTRIBUTE,
					"A password reset token was not " + "successfully delivered to the user.");
			portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), RECOVER_VERIFY_VIEW);
			populateResponseHeader(response);
			return RECOVER_VERIFY_VIEW;
		}

		try {
			// if the session uses a reCAPTCHA, verify the reCAPTCHA
			if (useRecaptcha) {
				verifyRecaptcha(parameters);
			}

			String code = parameters.get("code");
			String newPassword = parameters.get("password");
			ExtendedResult result = pool
					.processExtendedOperation(new PasswordModifyExtendedRequest(userDN, code, newPassword));
			if (result.getResultCode() != ResultCode.SUCCESS) {
				model.addAttribute(ERROR_ATTRIBUTE, result.getResultString());
				session.setAttribute(USE_RECAPTCHA_ATTRIBUTE, true);
				model.addAttribute(USE_RECAPTCHA_ATTRIBUTE, true);
				portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
				return RECOVER_VERIFY_VIEW;
			}
			session.removeAttribute(RECOVERYDN_ATTRIBUTE);
			session.removeAttribute(USE_RECAPTCHA_ATTRIBUTE);
			portalAspectUtils.exitPayloadlogger(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute));
			populateResponseHeader(response);
			return RECOVER_SUCCESS_VIEW;
		} catch (LDAPException e) {
			// there was some problem resetting the password, so go back to the
			// "verify" page with an error
			model.addAttribute(ERROR_ATTRIBUTE,  (String)LDAP_EXCEPTION_MSG.concat(loggerAttribute.getOperationName()));

			// adds reCAPTCHA for the session to prevent brute forcing the
			// verification code
			session.setAttribute(USE_RECAPTCHA_ATTRIBUTE, true);
			// adds an accessible model attribute for the reCAPTCHA
			// needed to display reCAPTCHA right after the first invalid code
			model.addAttribute(USE_RECAPTCHA_ATTRIBUTE, true);
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			populateResponseHeader(response);
			return RECOVER_VERIFY_VIEW;
		} catch (WebApplicationException e) {
			// display the error alert
			model.addAttribute(ERROR_ATTRIBUTE,  (String)TECHNICAL_EXCEPTION_MSG.concat(loggerAttribute.getOperationName()));
			portalAspectUtils.exceptionPayload(CLASS_NAME, portalSSAMLoggerUtilities.populateTimeStamp(loggerAttribute), e);
			populateResponseHeader(response);
			return RECOVER_VERIFY_VIEW;
		}
	}

	/**
	 * Handles the retrieval of password requirements from the Directory Server
	 * 
	 * @param dn
	 *            The dn for the entry for which to retrieve the password
	 *            requirements from. May be empty or null. If null, retrieve the
	 *            default password requirements from the server
	 * 
	 * @return Returns a List&lt;PasswordQualityRequirement&gt; of current
	 *         Password Quality Requirements in the DS Password Policy
	 */
	public List<PasswordQualityRequirement> getPasswordRequirements(String dn) {
		List<PasswordQualityRequirement> pqRequirements = new ArrayList<>();
		try {
			GetPasswordQualityRequirementsExtendedRequest extendedRequest;
			if (StringUtils.isEmpty(dn)) {
				extendedRequest = GetPasswordQualityRequirementsExtendedRequest
						.createAddWithDefaultPasswordPolicyRequest();
			} else {
				extendedRequest = GetPasswordQualityRequirementsExtendedRequest
						.createSelfChangeForSpecifiedUserRequest(dn);
			}
			GetPasswordQualityRequirementsExtendedResult result = (GetPasswordQualityRequirementsExtendedResult) pool
					.processExtendedOperation(extendedRequest);
			pqRequirements = result.getPasswordRequirements();
		} catch (LDAPException e) {
			//LOG.error("Failed to retrieve password requirements.", e.getMessage());
		}
		return pqRequirements;
	}

	/**
	 * Searches for and returns the entry for the currently authenticated user.
	 *
	 * @return The user entry is returned
	 *
	 * @throws LDAPException
	 *             Thrown if there is a problem getting the entry, or if the
	 *             entry returned is null
	 */
	private Entry getUserEntry() throws LDAPException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object userDetails = authentication.getPrincipal();
		SearchRequest request;

		if (userDetails instanceof LDAPUser) {
			request = new SearchRequest(((LDAPUser) userDetails).getDN(), SearchScope.BASE, "(objectClass=*)");
		} else {
			request = new SearchRequest(settings.getBaseDN(), SearchScope.SUB,
					Filter.createEqualityFilter(settings.getNamingAttribute(), authentication.getName()));
		}
		request.addControl(getIntermediateClientRequestControl());
		Entry entry = pool.searchForEntry(request);

		if (entry == null) {
			throw new LDAPException(ResultCode.NO_SUCH_OBJECT, "Entry search returned null.");
		}
		return entry;
	}

	/**
	 * Populates the provided model with the specified user name and user
	 * entry/attributes.
	 */
	private void populateUserModel(String username, Entry entry, Model model) {
		model.addAttribute(USERNAME_ATTRIBUTE, username);
		for (Attribute attribute : entry.getAttributes()) {
			model.addAttribute(attribute.getName(), attribute.getValue());
		}
		model.addAttribute("entry", entry);
	}

	/** Populates the provided model with the specified parameters. */
	private void populateRegistrationModel(Map<String, String> parameters, Model model) {
		for (Map.Entry<String, String> parameter : parameters.entrySet()) {
			// handle all parameters except the password
			String name = parameter.getKey();
			if (!name.equals("userPassword") && !name.equals(CSRF_ATTRIBUTE)) {
				String value = parameter.getValue().trim();
				if (!value.isEmpty()) {
						//String jsEncodedValue = Encode.forJavaScript(value);
						//String htmlEncodedValue = Encode.forHtml(jsEncodedValue);
						//model.addAttribute(name, htmlEncodedValue);
						model.addAttribute(name, value);
				}
			}
		}
	}

	/**
	 * Constructs and sends a "deliver registration code" extended request for
	 * the user with the provided DN. The server will choose the delivery
	 * mechanism to use, which is governed by the order of delivery mechanisms
	 * registered with the single-use-token extended operation handler and the
	 * optional "ds-auth-preferred-otp-delivery-mechanism" attribute in the
	 * user's entry.
	 *
	 * @param userDN
	 *            The DN of the user to use when sending the request
	 *
	 * @return Returns the "deliver registration code" result, which includes
	 *         the delivery mechanism and recipient ID
	 *
	 * @throws WebApplicationException
	 *             Thrown if there is an error delivering the registration code
	 */
	private DeliverRegistrationCodeResult deliverRegistrationCode(String userDN) throws WebApplicationException {
		try {
			DeliverSingleUseTokenExtendedRequest deliverSingleUseTokenRequest = new DeliverSingleUseTokenExtendedRequest(
					userDN, TOKEN_ID, null, settings.getMessageSubject(), settings.getFullTextBeforeToken(),
					settings.getFullTextAfterToken(), settings.getCompactTextBeforeToken(),
					settings.getCompactTextAfterToken(), null, true, true, true, true);
			DeliverSingleUseTokenExtendedResult result = (DeliverSingleUseTokenExtendedResult) pool
					.processExtendedOperation(deliverSingleUseTokenRequest);
			ResultCode resultCode = result.getResultCode();

			if (resultCode == ResultCode.SUCCESS) {
				return new DeliverRegistrationCodeResult(result);
			} else {
				throw new WebApplicationException(HttpStatus.BAD_REQUEST,
						resultCode + " - " + result.getDiagnosticMessage());
			}
		} catch (LDAPException e) {
		//	LOG.error("Could not deliver registration link for user with userDN '" + userDN + "'", e.getMessage());
			throw new WebApplicationException(HttpStatus.INTERNAL_SERVER_ERROR,  (String)LDAP_EXCEPTION_MSG.concat("deliverRegistrationCode."));
		}
	}

	/**
	 * Verify POSTed reCAPTCHA.
	 *
	 * @param parameters
	 *            The POSTed params that include the reCAPTCHA code to validate
	 *
	 * @throws WebApplicationException
	 *             Thrown if there is an unknown internal server error or if
	 *             there is a problem with the reCAPTCHA (optional) response
	 */
	private void verifyRecaptcha(Map<String, String> parameters) throws WebApplicationException {
		// return if reCAPTCHA is not enabled
		if (!settings.isRecaptchaEnabled()) {
			return;
		}

		// retrieve the reCAPTCHA widget response
		String value = parameters.get(RECAPTCHA_PARAM_NAME);

		// call the service
		MultiValueMap<String, String> args = new LinkedMultiValueMap<>();
		args.add("secret", settings.getRecaptchaSecretKey());
		args.add("response", value);
		RestTemplate restTemplate = new RestTemplate();
		try {
			ReCaptchaResponse response = restTemplate.postForObject("https://www.google.com/recaptcha/api/siteverify",
					args, ReCaptchaResponse.class);

			// was the response invalid?
			if (!response.success) {
				throw new WebApplicationException(HttpStatus.FORBIDDEN, response.displayErrorMessages());
			}
		} catch (Exception ex) {
			// unknown error
			//LOG.error("Error occurred after attempt to get a response during " + "CAPTCHA: " + ex.getMessage());
			throw new WebApplicationException(HttpStatus.INTERNAL_SERVER_ERROR,  (String)LDAP_EXCEPTION_MSG.concat("verifyRecaptcha."));
		}
	}

	/**
	 * Returns an intermediate client request control using the authzId of the
	 * currently authenticated user.
	 *
	 * @return an IntermediateClientRequestControl using the authorizationID of
	 *         the currently authenticated user
	 */
	private IntermediateClientRequestControl getIntermediateClientRequestControl() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object userDetails = authentication.getDetails();
		String authzId = userDetails instanceof LDAPUser ? ((LDAPUser) userDetails).getAuthzID()
				: "u:" + authentication.getName();
		return new IntermediateClientRequestControl(null, null, null, authzId, CLIENT_SESSION_ID, null, null);
	}

	/**
	 * Returns a display message for a password policy error type.
	 *
	 * @param errorType
	 *            password policy error type message to request.
	 * @return String with display message for requested password policy error
	 *         type.
	 */
	private String getPasswordPolicyErrorTypeMessage(PasswordPolicyErrorType errorType) {
		switch (errorType) {
		case PASSWORD_EXPIRED:
			return "password is expired";

		case ACCOUNT_LOCKED:
			return "account is locked or disabled";

		case CHANGE_AFTER_RESET:
			return "password must be changed before any other operation " + "will be allowed";

		case PASSWORD_MOD_NOT_ALLOWED:
			return "password changes are not allowed";

		case MUST_SUPPLY_OLD_PASSWORD:
			return "must provide the current password when attempting " + "to set a new one";

		case INSUFFICIENT_PASSWORD_QUALITY:
			return "proposed password is too weak to be acceptable";

		case PASSWORD_TOO_SHORT:
			return "proposed password is too short";

		case PASSWORD_TOO_YOUNG:
			return "password cannot be changed because the previous " + "password change was too recent";

		case PASSWORD_IN_HISTORY:
			return "proposed password cannot be the same as the current "
					+ "password or any password in the password history";

		default:
			//LOG.warn("Missing display message for password policy errorType '" + errorType.toString() + "'");
			return errorType.toString();
		}
	}

	/**
	 * Simple model for encapsulating response from the reCAPTCHA service.
	 */
	@JsonIgnoreProperties(ignoreUnknown = true)
	private static class ReCaptchaResponse implements Serializable {
		private static final long serialVersionUID = 8505145164400678294L;
		private static final HashMap<String, String> codesMap = getMap();

		/**
		 * Constructs a user friendly code HashMap.
		 */
		private static HashMap<String, String> getMap() {
			HashMap<String, String> codes = new HashMap<>();

			codes.put("missing-input-secret", "secret parameter is missing.");
			codes.put("invalid-input-secret", "secret parameter is invalid or malformed.");
			codes.put("missing-input-response", "response parameter is missing.");
			codes.put("invalid-input-response", "response parameter is invalid or malformed.");

			return codes;
		}

		/**
		 * Whether or not the request was successful.
		 */
		public boolean success;

		/**
		 * Error codes associated with the response, if any.
		 */
		@JsonProperty("error-codes")
		public String[] errorCodes;

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unused")
		protected void toString(final StringBuilder buffer) {
			buffer.append("ReCaptchaResponse(success='");
			buffer.append(success);
			buffer.append("', errorCodes=[");
			buffer.append(Arrays.toString(errorCodes));
			buffer.append(']');
			buffer.append("')");
		}

		/**
		 * For displaying user friendly error codes/alerts.
		 */
		public String displayErrorMessages() {
			String errorMessage = EMPTY_STRING;

			for (String s : errorCodes) {
				errorMessage += "The reCAPTCHA " + codesMap.get(s) + " ";
			}

			return errorMessage;
		}
	
		}

	private void populateResponseHeader(HttpServletResponse response) {
	    response.setHeader(X_XSS_PROTECTION_HEADER, X_XSS_PROTECTION_VALUE);
	    response.setHeader(CACHE_CONTROL_HEADER,CACHE_CONTROL_VALUE);
	    response.setHeader(PRAGMA_HEADER,PRAGMA_VALUE);
	    response.setHeader(X_FRAME_OPTIONS_HEADER,X_FRAME_OPTIONS_VALUE);
	    response.setHeader(X_CONTENT_TYPE_OPTIONS_HEADER, X_CONTENT_TYPE_OPTIONS_VALUE);
	}
	
}

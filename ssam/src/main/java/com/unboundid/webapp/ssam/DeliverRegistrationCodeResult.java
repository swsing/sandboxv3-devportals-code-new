/*
 * Copyright 2015-2016 UnboundID Corp.
 *
 * All Rights Reserved.
 */
package com.unboundid.webapp.ssam;

import java.io.Serializable;

import com.unboundid.ldap.sdk.unboundidds.extensions.DeliverOneTimePasswordExtendedResult;
import com.unboundid.ldap.sdk.unboundidds.extensions.DeliverSingleUseTokenExtendedResult;
import com.unboundid.util.NotMutable;

/**
 * Encapsulates information about the result of a specified extended operation.
 */
@NotMutable
public class DeliverRegistrationCodeResult implements Serializable {
	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = -2456095260354039576L;

	private String deliveryMechanism;
	private String recipientID;

	/**
	 * Creates a new instance using the specified extended operation result.
	 * @param result Single Use Token operation result
	 * @see DeliverSingleUseTokenExtendedResult 
	 */
	public DeliverRegistrationCodeResult(DeliverSingleUseTokenExtendedResult result) {
		this.deliveryMechanism = result.getDeliveryMechanism();
		this.recipientID = result.getRecipientID();
	}

	/**
	 * Creates a new instance using the specified extended operation result.
	 * @param result OTP operation result
	 * @see DeliverOneTimePasswordExtendedResult
	 */
	public DeliverRegistrationCodeResult(DeliverOneTimePasswordExtendedResult result) {
		this.deliveryMechanism = result.getDeliveryMechanism();
		this.recipientID = result.getRecipientID();
	}

	/**
	 * Returns the delivery mechanism that was used to deliver the registration
	 * code.
	 * 
	 * @return String Delivery mechanism used to deliver registration code
	 */
	public String getDeliveryMechanism() {
		return deliveryMechanism;
	}

	/**
	 * Returns the recipient ID that the registration code was delivered to
	 * (e.g. phone number or email address).
	 * @return String Recipient ID like email address for Email Delivery 
	 */
	public String getRecipientID() {
		return recipientID;
	}
}

#!/bin/bash
#
# Copyright 2017 Capgemini.
#
# Executes the Self-Service Account Manager application installer with re-deploy feature.  Run the
# script without any arguments to display the help, and refer to the
# documentation for additional information.
#

function die()
{
  echo $1
  exit 1
}

# Go into the script directory and export the SCRIPT_DIR, which is required by
# the installer.
cd `dirname $0`
export SCRIPT_DIR=`pwd -P`

# Make sure the war file is in the same directory as the script, and make sure
# that java is in the PATH.
[ -f "${SCRIPT_DIR}/setup.sh" ] || die "The setup.sh file must exist in ${SCRIPT_DIR}."

# Run the installer or display the help if no arguments were provided.
if [ $# -eq 0 ]; then
  export INSTANCE_ROOT=/d/Ping/PingDirectory1
  export GIT_REPO=/d/projects/portals
  $INSTANCE_ROOT/bin/ldapdelete -D "cn=dirAdmin" -w admin@123 "cn=SSAM User,dc=bank,dc=com"
  $INSTANCE_ROOT/bin/dsconfig set-password-policy-prop --policy-name "Default Password Policy" --reset password-validator
  $INSTANCE_ROOT/bin/dsconfig set-extended-operation-handler-prop --handler-name "Password Reset Token" --set "password-generator:One-Time Password Generator" --set default-token-delivery-mechanism:Email
  $INSTANCE_ROOT/bin/dsconfig set-extended-operation-handler-prop --handler-name Single-Use-Token --set "password-generator:One-Time Password Generator" --set default-otp-delivery-mechanism:Email
  $INSTANCE_ROOT/bin/dsconfig delete-password-generator --generator-name EncOTP
  $INSTANCE_ROOT/bin/dsconfig delete-otp-delivery-mechanism --mechanism-name EnhHTML
  $INSTANCE_ROOT/bin/dsconfig set-global-configuration-prop --set maximum-shutdown-time:1m
  $INSTANCE_ROOT/bat/stop-ds.bat
  rm -rf $INSTANCE_ROOT/webapps/ssam-config/
  rm -rf $INSTANCE_ROOT/webapps/ssam.war
  rm -rf $INSTANCE_ROOT/tmp/SSAM/
  rm -rf $INSTANCE_ROOT/tmp/manage-extension/ssam-extension/
  rm -rf $INSTANCE_ROOT/extensions/com.capgemini*
  rm -rf "${SCRIPT_DIR}"
  cd $GIT_REPO/ssam-extension/
  # Undefine INSTANCE_ROOT for unit tests
  export INSTANCE_ROOT=
  mvn clean install
  # Redefine INSTANCE_ROOT
  export INSTANCE_ROOT=/d/Ping/PingDirectory1
  $INSTANCE_ROOT/bin/manage-extension --install target/ssam-extension.zip --no-prompt
  cd $GIT_REPO/ssam/
  mvn clean install
  cd target
  unzip ssam-image.zip
  $INSTANCE_ROOT/bat/start-ds.bat
  "${SCRIPT_DIR}/setup.sh" --serverRoot $INSTANCE_ROOT --ldapPort 636 --bindDN "cn=dirAdmin" --bindPassword admin@123 --useSSL --trustStorePath $INSTANCE_ROOT/config/truststore --peopleBaseDN ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com --smtpServerHostname email-smtp.us-west-2.amazonaws.com --smtpSenderEmailAddress "no-reply@capgeminiwebservices.com" --smtpServerPort 587 --useSMTPAuth --useSMTPStartTLS --smtpServerUsername AKIAJDMCM6CZKVXIB52A --smtpServerPassword AvVxfWAc4n6RO8XyZVR03c0JZMpkKPD4Dp5FIfia60Cx --hostDNS https://localhost:8443 --devportalHostDNS https://localhost:8444 --bankHelpURL https://www.bankofireland.com/api/developer/contact/?developer=developer --bankTermsOfServiceURL https://www.bankofireland.com/legal/terms-and-conditions/ --cdnBaseURL https://d21fhsvwv2t3le.cloudfront.net --bankFaqURL https://www.bankofireland.com/api/developer/help --bankCookiePrivacyPolicyURL https://www.bankofireland.com/legal/privacy-statement/ --bankMulePortalURL https://eu1.anypoint.mulesoft.com/exchange/portals/openbanking/?view=list --cdnLibsHashCode Pt7zcHNRE1Ey65h4Zo3tGxatnJ29hfXN6vlRMOLXvak= --cdnAppHashCode uk9WBdsGh0cey0pnzihvSUoGCc2ZoGiWFm9lb7EdLc= --devportalOverviewURL https://developer.abpiboitest.com
fi

#!/bin/bash
#
# Copyright 2015-2016 UnboundID Corp.
#
# Executes the Self-Service Account Manager application installer.  Run the
# script without any arguments to display the help, and refer to the
# documentation for additional information.
#
export INSTANCE_ROOT=/d/Users/aaitavad/PSD2/Software/PingDirectory
export GIT_REPO=/d/PSD2/Workspace_PSD2_portals_KMS/psd2-portals-cma2
$INSTANCE_ROOT/bat/stop-ds.bat
cd $GIT_REPO/ssam/
mvn clean install
cd target
unzip ssam-image.zip
$INSTANCE_ROOT/bat/start-ds.bat
cd ssam
./setup.sh --serverRoot $INSTANCE_ROOT --ldapPort 636 --bindDN "cn=dirAdmin" --bindPassword admin@123 --useSSL --trustStorePath $INSTANCE_ROOT/config/truststore --peopleBaseDN ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com --smtpServerHostname email-smtp.us-west-2.amazonaws.com --smtpSenderEmailAddress "no-reply@capgeminiwebservices.com" --smtpServerPort 587 --useSMTPAuth --useSMTPStartTLS --smtpServerUsername AKIAJDMCM6CZKVXIB52A --smtpServerPassword AvVxfWAc4n6RO8XyZVR03c0JZMpkKPD4Dp5FIfia60Cx --hostDNS https://localhost:8443 --devportalHostDNS https://localhost:8444 --bankHelpURL https://www.bankofireland.com/thirdpartyandeveloperhub/help --ssamUserPassword P@ssw0rd123 --bankTermsOfServiceURL https://www.bankofireland.com/legal/terms-and-conditions/ --cdnBaseURL https://d22cosexs2nm1j.cloudfront.net/v3/sandbox --bankFaqURL https://www.bankofireland.com/api/developer/help/ --bankCookiePrivacyPolicyURL https://www.bankofireland.com/legal/privacy-statement/ --bankMulePortalURL https://eu1.anypoint.mulesoft.com/exchange/portals/openbanking/?view=list --cdnLibsHashCode sha256-uIWxUXUk12qjdwgGT7flviVjI8yYHuiW0J9lzkXIa5I= --cdnAppHashCode sha256-QDQj4FUy1FhrvFGiqPhFS/ZbwHhqJIemXdlOKQccN6M= --devportalOverviewURL https://localhost:8444
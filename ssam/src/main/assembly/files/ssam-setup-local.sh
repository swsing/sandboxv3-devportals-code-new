#!/bin/bash
#
# Copyright 2015-2016 UnboundID Corp.
#
# Executes the Self-Service Account Manager application installer.Run the
# script without any arguments to display the help, and refer to the
# documentation for additional information.
#
export INSTANCE_ROOT=/d/Users/aaitavad/PSD2/Software/PingDirectory
export GIT_REPO=/d/PSD2/Workspace_PSD2_portals_KMS/psd2-portals-cma2
$INSTANCE_ROOT/bat/start-ds.bat
$INSTANCE_ROOT/bin/ldapdelete -D "cn=dirAdmin" -w admin@123 "cn=SSAM User,dc=bank,dc=com"
$INSTANCE_ROOT/bin/dsconfig set-password-policy-prop --policy-name "Default Password Policy" --reset password-validator
$INSTANCE_ROOT/bin/dsconfig set-extended-operation-handler-prop --handler-name "Password Reset Token" --set "password-generator:One-Time Password Generator" --set default-token-delivery-mechanism:Email
$INSTANCE_ROOT/bin/dsconfig set-extended-operation-handler-prop --handler-name Single-Use-Token --set "password-generator:One-Time Password Generator" --set default-otp-delivery-mechanism:Email
$INSTANCE_ROOT/bin/dsconfig delete-password-generator --generator-name EncOTP
$INSTANCE_ROOT/bin/dsconfig delete-otp-delivery-mechanism --mechanism-name EnhHTML
$INSTANCE_ROOT/bin/dsconfig set-global-configuration-prop --set maximum-shutdown-time:1m
$INSTANCE_ROOT/bat/stop-ds.bat
rm -rf $INSTANCE_ROOT/webapps/ssam-config/
rm -rf $INSTANCE_ROOT/webapps/ssam.war
rm -rf $INSTANCE_ROOT/tmp/SSAM/
rm -rf $INSTANCE_ROOT/tmp/manage-extension/ssam-extension/
rm -rf $INSTANCE_ROOT/extensions/com.capgemini*
cd $GIT_REPO/ssam-extension/
# Undefine INSTANCE_ROOT for unit tests
export INSTANCE_ROOT=
mvn clean install
# Redefine INSTANCE_ROOT
export INSTANCE_ROOT=/d/Users/aaitavad/PSD2/Software/PingDirectory
$INSTANCE_ROOT/bin/manage-extension --install target/ssam-extension.zip --no-prompt
cd $GIT_REPO/ssam/
mvn clean install
cd target
unzip ssam-image.zip
cd ssam
$INSTANCE_ROOT/bat/start-ds.bat
./setup.sh --serverRoot $INSTANCE_ROOT --ldapPort 636 --bindDN "cn=dirAdmin" --bindPassword admin@123 --useSSL --trustStorePath $INSTANCE_ROOT/config/truststore --peopleBaseDN ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com --smtpServerHostname email-smtp.us-west-2.amazonaws.com --smtpSenderEmailAddress "no-reply@capgeminiwebservices.com" --smtpServerPort 587 --useSMTPAuth --useSMTPStartTLS --smtpServerUsername AKIAJDMCM6CZKVXIB52A --smtpServerPassword AvVxfWAc4n6RO8XyZVR03c0JZMpkKPD4Dp5FIfia60Cx --hostDNS https://localhost:8443 --devportalHostDNS https://localhost:8444 --bankHelpURL https://www.bankofireland.com/thirdpartyandeveloperhub/help --ssamUserPassword P@ssw0rd123 --bankTermsOfServiceURL https://www.bankofireland.com/legal/terms-and-conditions/ --cdnBaseURL https://d22cosexs2nm1j.cloudfront.net/v3/sandbox --bankFaqURL https://www.bankofireland.com/api/developer/help/ --bankCookiePrivacyPolicyURL https://www.bankofireland.com/legal/privacy-statement/ --bankMulePortalURL https://eu1.anypoint.mulesoft.com/exchange/portals/openbanking/?view=list --cdnLibsHashCode sha256-uIWxUXUk12qjdwgGT7flviVjI8yYHuiW0J9lzkXIa5I= --cdnAppHashCode sha256-QDQj4FUy1FhrvFGiqPhFS/ZbwHhqJIemXdlOKQccN6M= --devportalOverviewURL https://localhost:8444
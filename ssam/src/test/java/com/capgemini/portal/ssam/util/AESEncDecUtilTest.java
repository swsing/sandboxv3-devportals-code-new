package com.capgemini.portal.ssam.util;


import static org.junit.Assert.assertEquals;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.portal.ssam.util.AESEncDecUtil;

public class AESEncDecUtilTest {
	static final Logger LOG = LoggerFactory.getLogger(AESEncDecUtilTest.class);
	private AESEncDecUtil testUtil = null;
	private static String encryptedText = null;
	private static String decryptedText = null;
	private static final String DATA = "email=sales@capgemini.com&code=187212";
	private static final String ENCDATA = "WaHwxXr3HbvJjyQrm6gnvZBlZTKM7_jRMLyVNkQNU74kTW6QD0yCTAAZ0shQScHe";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// Empty implementation
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		// Empty implementation
	}

	@Before
	public void setUp() throws Exception {
		LOG.debug("START: setUp() method");
		testUtil = AESEncDecUtil.getInstance();
		LOG.debug("END: setUp() method");
	}

	@After
	public void tearDown() throws Exception {
		// Empty implementation
	}

	@SuppressWarnings("static-access")
	@Test
	public void testEncrypt() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {
		LOG.debug("START: testEncrypt() method");
		encryptedText = testUtil.encrypt(DATA);
		assertEquals(ENCDATA, encryptedText);
		LOG.debug("END: testEncrypt() method");
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void testDecrypt() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {
		LOG.debug("START: testDecrypt() method");
		decryptedText = testUtil.decrypt(ENCDATA);
		assertEquals(DATA, decryptedText);
		LOG.debug("END: testDecrypt() method");
	}

}
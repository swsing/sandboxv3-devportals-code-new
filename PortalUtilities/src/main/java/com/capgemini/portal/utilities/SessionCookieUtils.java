/*******************************************************************************

 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

package com.capgemini.portal.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.capgemini.portal.config.SessionCookieConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.model.AccessJwtToken;
import com.capgemini.portal.model.JwtToken;
import com.capgemini.portal.model.PTCInfo;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

/**
 * SessionCookieUtils.java is a Utility class used for Session Cookie related
 * operations.
 * 
 */

@Component
public final class SessionCookieUtils {

	private static SessionCookieConfig staticsessionCookieConfig;
	
	@Autowired
	private SessionCookieConfig sessionCookieConfig;
	
	@PostConstruct
	public void init(){
		staticsessionCookieConfig=sessionCookieConfig;
	}
	private SessionCookieUtils() {

	}

	/**
	 * This method generates a Session Cookie JWT token and encrypts it with JWE
	 * 
	 * @param tokenIssuer
	 *            Token issuer from yaml
	 * @param tokenExpirationTime
	 *            Token Expiration Time from yaml
	 * @param tokenSigningKey
	 *            Token Signin Key
	 * @param ptcInfo
	 *            {@link PTCInfo}
	 * @param jweDecryptionKey
	 *            JWE decryption key
	 * @return {@link AccessJwtToken}
	 */
	public static AccessJwtToken createSessionJwtToken(PTCInfo ptcInfo) {
		DateTime currentTime = new DateTime();

		// build JWT token
		JWTClaimsSet jwtClaims = new JWTClaimsSet.Builder().issuer(staticsessionCookieConfig.getTokenIssuer())
				.claim(PortalConstants.PTC_NAME_KEY, ptcInfo.getPtcName())
				.claim(PortalConstants.TPP_ASSOCIATIONS_LIST, ptcInfo.getOrganizationIds())
				.claim(PortalConstants.PTC_ID_KEY, ptcInfo.getPtcId())
				.claim(PortalConstants.CORELATION_ID, new Date().getTime())
				.claim(PortalConstants.STATE, PortalConstants.FALSE)
				.expirationTime(currentTime.plusSeconds(staticsessionCookieConfig.getSessiontimeout()).toDate())
				.notBeforeTime(currentTime.toDate()).issueTime(currentTime.toDate()).jwtID(UUID.randomUUID().toString())
				.build();

		JWEObject jweObject = null;
		String jweString = null;
		// This block sign and encrypts Session Cookie JWT Token.
		try {
			// Sign JWT token with signing key
			JWSSigner signer = new MACSigner(staticsessionCookieConfig.getTokenSigningKey());
			SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS512), jwtClaims);
			signedJWT.sign(signer);
			// signedJWT.getParsedString();

			// Encrpty JWT token
			JWEHeader header = new JWEHeader(JWEAlgorithm.DIR, EncryptionMethod.A128GCM);
			byte[] key = SessionCookieUtils.getJweDecryptionKey(staticsessionCookieConfig.getJweDecryptionKey());
			Payload payload = new Payload(signedJWT);
			jweObject = new JWEObject(header, payload);
			jweObject.encrypt(new DirectEncrypter(key));
			jweString = jweObject.serialize();

		} catch (KeyLengthException e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.TPP_ERROR_GENRIC);
		} catch (JOSEException e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.TPP_ERROR_GENRIC);
		} catch (Exception e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.TPP_ERROR_GENRIC);
		}

		return new AccessJwtToken(jweString);
	}

	/**
	 * This method creates Session Cookie with empty value
	 * 
	 * @param expiry
	 *            Expiry time for Session
	 * @return {@link Cookie}
	 */
	public static Cookie createNewSessionCookie() {

		Cookie cookie = new Cookie(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE, null);
		cookie.setSecure(Boolean.TRUE);
		cookie.setHttpOnly(Boolean.TRUE);
		return cookie;

	}

	/**
	 * This method finds Session Cookie for TPP Portal
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return {@link Cookie}
	 */
	public static Cookie getTPPPortalCookie(HttpServletRequest request) {

		Cookie[] cookies = request.getCookies();
		Cookie tppPortalSessionCoockie = null;
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equalsIgnoreCase(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE)) {
					tppPortalSessionCoockie = cookie;
					break;
				}
			}
		}
		return tppPortalSessionCoockie;

	}

	/**
	 * This method decrypts token stored in Session Cookie and returns PTCInfo
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param portalRequestHeaderAttributes
	 * @param tokenSigningKey
	 * @return {@link PTCInfo}
	 * 
	 */
	public static PTCInfo getPTCInfo(HttpServletRequest request) {
		PTCInfo ptcInfo = null;
		Cookie cookie = getTPPPortalCookie(request);
		if (cookie != null) {
			ptcInfo = new PTCInfo();
			String jweTokenPayload = cookie.getValue();
			// decrypt ,verify and consume jwt.
			try {
				// Parse JWE Token Payload to get JWE Object
				JWEObject jweObject = JWEObject.parse(jweTokenPayload);

				// Decrypt JWEOject with JWE Decryption key and get JWT Token
				byte[] key = SessionCookieUtils.getJweDecryptionKey(staticsessionCookieConfig.getJweDecryptionKey());
				jweObject.decrypt(new DirectDecrypter(key));

				SignedJWT signedJWT = jweObject.getPayload().toSignedJWT();

				// verify JWT with siging key
				signedJWT.verify(new MACVerifier(staticsessionCookieConfig.getTokenSigningKey()));

				// check token validity
				checkJWTValidity(signedJWT.getJWTClaimsSet());

				// consuming JWT after verification
				Map<String, Object> map = signedJWT.getJWTClaimsSet().getClaims();
				if (map.get(PortalConstants.PTC_NAME_KEY) != null) {
					ptcInfo.setPtcName(map.get(PortalConstants.PTC_NAME_KEY).toString());
				}

				if (map.get(PortalConstants.PTC_ID_KEY) != null) {
					ptcInfo.setPtcId(map.get(PortalConstants.PTC_ID_KEY).toString());
				}

				if (null != map.get(PortalConstants.TPP_ASSOCIATIONS_LIST)
						&& !CollectionUtils.isEmpty((List<String>) map.get(PortalConstants.TPP_ASSOCIATIONS_LIST))) {
					List<String> tppOrgList = (List<String>) map.get(PortalConstants.TPP_ASSOCIATIONS_LIST);
							ptcInfo.setOrganizationIds(tppOrgList);
					}
			} catch (Exception e) {
				throw PortalException.populatePortalException(PortalErrorCodeEnum.SSSEION_EXPIRED_OR_INVALID);
			}
		}

		return ptcInfo;
	}

	/**
	 * Check expiry of JWT
	 * 
	 * @param claims
	 */
	public static void checkJWTValidity(JWTClaimsSet claims) {

		if (claims != null) {
			Date now = null;
			SimpleDateFormat sdf;
			// token MUST NOT be accepted on or after any specified exp time:
			Date exp = claims.getExpirationTime();
			if (exp != null) {

				now = new Date();

				if (now.equals(exp) || now.after(exp)) {
					sdf = new SimpleDateFormat();
					String expVal = sdf.format(exp);
					String nowVal = sdf.format(now);

					String msg = "JWT expired at " + expVal + ". Current time: " + nowVal;
					throw PortalException.populatePortalException(msg, PortalErrorCodeEnum.SSSEION_EXPIRED_OR_INVALID);
				}
			}

			// token MUST NOT be accepted before any specified nbf time:
			Date nbf = claims.getNotBeforeTime();
			if (nbf != null) {

				if (now == null) {
					now = new Date();
				}

				if (now.before(nbf)) {
					sdf = new SimpleDateFormat();
					String nbfVal = sdf.format(nbf);
					String nowVal = sdf.format(now);

					String msg = "JWT must not be accepted before " + nbfVal + ". Current time: " + nowVal;
					PortalException.populatePortalException(msg, PortalErrorCodeEnum.SSSEION_EXPIRED_OR_INVALID);
				}
			}
		}
	}
	/**
	 * This method updates empty Session Cookie with JWT Token encrypted with
	 * JWE
	 * 
	 * @param ptcInfo
	 * @param httpServletRequest
	 * @param expiry
	 * @param tokenSigningKey
	 * @param tokenIssuer
	 * @param jweDecyptionKey
	 * @return {@link Cookie}
	 */
	
	public static Cookie modifyTPPPortalCookie(PTCInfo ptcInfo, HttpServletRequest httpServletRequest) {

		// Find if Session Cookie is available otherwise create new
		Cookie tppPortalSessionCookie = getTPPPortalCookie(httpServletRequest);
		if (tppPortalSessionCookie == null) {
			tppPortalSessionCookie = createNewSessionCookie();
		}

		// Creates JWT Token and encrypts with JWE
		JwtToken sessionAccessToken = createSessionJwtToken(ptcInfo);
		String sessionTokenStr = sessionAccessToken.getToken();
		tppPortalSessionCookie.setValue(sessionTokenStr);
		tppPortalSessionCookie.setHttpOnly(true);
		tppPortalSessionCookie.setSecure(true);
		return tppPortalSessionCookie;
	}

	/**
	 * Find Cookie based on given name
	 * 
	 * @param httpServletRequest
	 * @param tppportalTempCookie
	 * @return
	 */
	public static Cookie findCookie(HttpServletRequest httpServletRequest, String tppportalTempCookie) {
		Cookie[] cookies = httpServletRequest.getCookies();
		Cookie tppPortalSessionCoockie = null;
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equalsIgnoreCase(tppportalTempCookie)) {
					tppPortalSessionCoockie = cookie;
					break;
				}
			}
		}
		return tppPortalSessionCoockie;
	}

	/**
	 * Converts JweDecryption key to byte array
	 * 
	 * @param jweDecryptionKey
	 * @return
	 */
	public static byte[] getJweDecryptionKey(String jweDecryptionKey) {

		return jweDecryptionKey.getBytes();
	}

}
/*******************************************************************************

 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

package com.capgemini.portal.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * SessionCookieConfig.java holds config for Session Cookie
 * 
 */

@Component
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("app")
public class SessionCookieConfig {

	private List<String> protectedEndPoints;

	private Integer sessiontimeout;

	private String tokenSigningKey;

	private String tokenIssuer;

	private String jweDecryptionKey;

	public List<String> getProtectedEndPoints() {
		return protectedEndPoints;
	}

	public void setProtectedEndPoints(List<String> protectedEndPoints) {
		this.protectedEndPoints = protectedEndPoints;
	}

	public String getTokenSigningKey() {
		return tokenSigningKey;
	}

	public void setTokenSigningKey(String tokenSigningKey) {
		this.tokenSigningKey = tokenSigningKey;
	}

	public String getTokenIssuer() {
		return tokenIssuer;
	}

	public void setTokenIssuer(String tokenIssuer) {
		this.tokenIssuer = tokenIssuer;
	}

	public Integer getSessiontimeout() {
		return sessiontimeout;
	}

	public void setSessiontimeout(Integer sessiontimeout) {
		this.sessiontimeout = sessiontimeout;
	}

	public String getJweDecryptionKey() {
		return jweDecryptionKey;
	}

	public void setJweDecryptionKey(String jweDecryptionKey) {
		this.jweDecryptionKey = jweDecryptionKey;
	}

}

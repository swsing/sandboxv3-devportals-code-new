/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.exception;

import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;

/**
 * The Class PortalException.
 */
public class PortalException extends PSD2Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new adapter exception.
	 *
	 * @param message
	 *            the message
	 * @param errorInfo
	 *            the error info
	 */
	public PortalException(String message, ErrorInfo errorInfo) {
		super(message, errorInfo);
	}

	/**
	 * Populate PSD 2 exception.
	 *
	 * @param detailErrorMessage
	 *            the detail error message
	 * @param errorCodeEnum
	 *            the error code enum
	 * @return the adapter exception
	 */
	public static PortalException populatePortalException(String detailErrorMessage, ErrorCodeEnum errorCodeEnum) {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailErrorMessage);
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());
		return new PortalException(detailErrorMessage, errorInfo);
	}

	/**
	 * Populate PSD 2 exception.
	 *
	 * @param errorCodeEnum
	 *            the error code enum
	 * @return the adapter exception
	 */
	public static PortalException populatePortalException(ErrorCodeEnum errorCodeEnum) {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(errorCodeEnum.getDetailErrorMessage());
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());
		return new PortalException(errorCodeEnum.getErrorMessage(), errorInfo);
	}

	public static PortalException populatePortalException(String detailErrorMessage,
			PortalErrorCodeEnum errorCodeEnum) {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailErrorMessage);
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());
		return new PortalException(detailErrorMessage, errorInfo);
	}

	/**
	 * Populate PSD 2 exception.
	 *
	 * @param errorCodeEnum
	 *            the error code enum
	 * @return the adapter exception
	 */
	public static PortalException populatePortalException(PortalErrorCodeEnum errorCodeEnum) {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(errorCodeEnum.getDetailErrorMessage());
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());
		return new PortalException(errorCodeEnum.getErrorMessage(), errorInfo);
	}

}

package com.capgemini.portal.enums;

import com.capgemini.portal.constants.PortalConstants;

public enum PortalErrorCodeEnum {

	/** The technical error. */
	TECHNICAL_ERROR("1100", PortalConstants.TECHINICAL_ERROR, PortalConstants.TECHINICAL_ERROR, "500"),

	SERVICE_UNAVAILABLE("1101", "Service Unavailable", "Service Unavailable", "503"),

	/** The authentication failure error. */
	AUTHENTICATION_FAILURE_ERROR("1102", "You are not authorized to use this service", "You are not authorized to use this service", "401"),

	SAML_USER_NULL("1103", PortalConstants.SAML_USER_NULL, PortalConstants.SAML_USER_NULL, "500"),

	/** Fetch User Profile Details **/
	INVALID_USER_ID("1104", PortalConstants.INVALID_USER_ID, PortalConstants.INVALID_USER_ID, "400"),

	INVALID_USER_DETAILS("1105", PortalConstants.INVALID_USER_DETAILS, PortalConstants.INVALID_USER_DETAILS, "400"),

	LDAP_ATTRIBUTE_NOT_FOUND("1106", PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,
			PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST, "400"),

	/** Update User Profile **/
	UPDATE_PROFILE_OPERATION_NOT_SUPPORTED("1107","LDAP: error code 21 - When attempting to modify entry User profile information.",
			"LDAP: error code 21 - When attempting to modify entry User profile information.", "400"),

	/** Change Password **/	
	ERROR_CHANGING_PASSWORD("1108", PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,
			PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST, "400"),

	OPERATION_NOT_SUPPORTED("1109", "The provided new password cannot be the same as the current password.",
			"The provided new password cannot be the same as the current password.", "400"),

	ERROR_SENDIND_MAIL("1110", PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,
			PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST, "400"),

	ERROR_SENDIND_CHANGE_PWD_MAIL("1111", PortalConstants.USER_ID_NULL,
			PortalConstants.USER_ID_NULL, "400"),

	INVALID_CURRENT_PASSWORD("1112", "Incorrect current password.", "Incorrect current password.", "400"),

	ERROR_GENRIC("1113", PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST, PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,
			"500"),

	/** Download TestData **/
	ERROR_DOWNLOADING_TESTDATA("1114", PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,
			PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST, "400"),

	/** Valid Session **/	
	USER_SESSION_EXPIRED("1115", PortalConstants.SESSION_EXPIRED, PortalConstants.SESSION_EXPIRED, "401"),

	APP_USER_SESSION_EXPIRED("1116", PortalConstants.APP_SESSION_EXPIRED, PortalConstants.APP_SESSION_EXPIRED, "401"),



	/** TPP Portal Error Codes **/

	TPP_ERROR_GENRIC("1200", PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,
			PortalConstants.UNABLE_TO_PROCESS_YOUR_REQUEST, "500"),

	NO_PTC_DATA_AVAILABLE("1201", "No PTC Data Found", "No PTC Data Found", "400"),

	NO_TPP_DATA_AVAILABLE("1202", PortalConstants.NO_TPP_DATA_FOUND, PortalConstants.NO_TPP_DATA_FOUND, "400"),

	NO_CLIENT_APP_DATA_FOUND("1203", "No Client Data Found", "No Client Data Found", "400"),

	SSSEION_EXPIRED_OR_INVALID("1204", "Session has been invalidated now", "Session has been invalidated now", "400"),

	TIER_MORE_THAN_ONE("1205", PortalConstants.INVALID_TIER, PortalConstants.INVALID_TIER, "500"),

	NO_TPP_APPLICATION_AVAILABLE("1206", "No Application is registered for this TPP",
			"No Application is registered for this TPP", "500"),

	EXPIRED_SSA("1207", PortalConstants.SSA_EXPIRED, PortalConstants.SSA_EXPIRED, "400"),

	INVALID_SSA("1208", PortalConstants.INVALID_SSA, PortalConstants.INVALID_SSA, "400"),

	SSA_NOT_OF_SELECTED_ORG("1209", PortalConstants.SSA_NOT_OF_SELECTED_ORG, PortalConstants.SSA_NOT_OF_SELECTED_ORG,
			"400"),

	TPP_ORG_NOT_ACTIVE("1210", PortalConstants.TPP_ORG_NOT_ACTIVE, PortalConstants.TPP_ORG_NOT_ACTIVE, "400"),

	MULE_LOGIN_FAILURE("1211", PortalConstants.MULE_LOGIN_FAILURE, PortalConstants.MULE_LOGIN_FAILURE, "500"),

	MULE_API_REGISTERATION_CALL_FAILURE("1212", PortalConstants.MULE_API_REGISTERATION_CALL_FAILURE,
			PortalConstants.MULE_API_REGISTERATION_CALL_FAILURE, "500"),

	MULE_FETCH_LIST_OF_ALL_ORG_APIS_FAILURE("1213", PortalConstants.MULE_FETCH_LIST_OF_ALL_ORG_APIS_FAILURE,
			PortalConstants.MULE_FETCH_LIST_OF_ALL_ORG_APIS_FAILURE, "500"),

	MULE_FETCH_LIST_OF_TIERS_FOR_API_VERSIONS_FAILURE("1214",
			PortalConstants.MULE_FETCH_LIST_OF_TIERS_FOR_API_VERSIONS_FAILURE,
			PortalConstants.MULE_FETCH_LIST_OF_TIERS_FOR_API_VERSIONS_FAILURE, "500"),

	MULE_CREATE_APP_CONTRACT_FAILURE("1215", PortalConstants.MULE_CREATE_APP_CONTRACT_FAILURE,
			PortalConstants.MULE_CREATE_APP_CONTRACT_FAILURE, "500"),

	MULE_API_REGISTERATION_DELETE_CALL_FAILURE("1216", PortalConstants.MULE_API_REGISTERATION_CALL_DELETE_FAILURE,
			PortalConstants.MULE_API_REGISTERATION_CALL_DELETE_FAILURE, "500"),

	PF_REGISTER_API_CALL_FAILURE("1217", PortalConstants.PF_REGISTER_API_CALL_FAILURE,
			PortalConstants.PF_REGISTER_API_CALL_FAILURE, "500"),

	PD_TPP_ORG_UPDATE_OR_INSERT_FAILURE("1218", PortalConstants.PD_TPP_ORG_UPDATE_OR_INSERT_FAILURE,
			PortalConstants.PD_TPP_ORG_UPDATE_OR_INSERT_FAILURE, "500"),

	PD_TPP_APP_ASSOCIATION_FAILURE("1219", PortalConstants.PD_TPP_APP_ASSOCIATION_FAILURE,
			PortalConstants.PD_TPP_APP_ASSOCIATION_FAILURE, "500"),

	PD_CLIENT_APP_MAPPING_FAILURE("1220", PortalConstants.PD_CLIENT_APP_MAPPING_FAILURE,
			PortalConstants.PD_CLIENT_APP_MAPPING_FAILURE, "500"),

	PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE("1221", PortalConstants.PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE,
			PortalConstants.PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE, "500"),

	PD_TPP_APP_ASSOCIATION_DELETE_FAILURE("1222", PortalConstants.PD_TPP_APP_ASSOCIATION_DELETE_FAILURE,
			PortalConstants.PD_TPP_APP_ASSOCIATION_DELETE_FAILURE, "500"),

	PF_REGISTER_API_CALL_DELETE_FAILURE("1223", PortalConstants.PF_REGISTER_API_CALL_DELETE_FAILURE,
			PortalConstants.PF_REGISTER_API_CALL_DELETE_FAILURE, "500"),

	LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP("1224", PortalConstants.LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP,
			PortalConstants.LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP, "400"),

	PING_DIRECTORY_CALL_FAILED("1227", PortalConstants.PING_DIRECTORY_CALL_FAILED,
			PortalConstants.PING_DIRECTORY_CALL_FAILED, "500"),

	TPP_USER_SESSION_EXPIRED("1225", PortalConstants.SESSION_EXPIRED, PortalConstants.SESSION_EXPIRED, "401"),

	TPP_APP_USER_SESSION_EXPIRED("1226", PortalConstants.APP_SESSION_EXPIRED, PortalConstants.APP_SESSION_EXPIRED,
			"401"),

	LOGIN_ERROR("1228", PortalConstants.LOGIN_ERROR, PortalConstants.LOGIN_ERROR, "500"),

	NO_TPP_NAME_PROVIDED("1229", "No TPP name provided in the request", "No TPP name provided in the request", "400"),

	MULE_ORGANISATION_APIS_NOT_AVAILABLE("1230", PortalConstants.MULE_ORGANISATION_APIS_NOT_AVAILABLE,
			PortalConstants.MULE_ORGANISATION_APIS_NOT_AVAILABLE, "500"),

	PD_IS_TPP_EXIST_CALL_FAILED("1231", PortalConstants.PD_IS_TPP_EXIST_CALL_FAILED,
			PortalConstants.PD_IS_TPP_EXIST_CALL_FAILED, "500"),

	TPP_TECHNICAL_ERROR("1232", PortalConstants.TECHINICAL_ERROR, PortalConstants.TECHINICAL_ERROR, "500"),

	UNABLE_TO_PARSE("1233", PortalConstants.UNABLE_TO_PARSE, PortalConstants.UNABLE_TO_PARSE, "500"),

	TPP_CLIENTID_INVALID("1234", "TppId or ClientId is not valid in request",
			"TppId or ClientId is not valid in request", "400"),

	NO_TPP_CLIENTID_FOUND("1235", "TppId or ClientId is not found in request",
			"TppId or ClientId is not found in request", "400"),

	MULE_RESPONSE_NOT_FOUND("1236", "Response not found from Mule Api", "Response not found from Mule Api", "500"),

	TPP_BLOCKED("1237", "Unauthorised access.Tpp is blocked", "Unauthorised access.Tpp is blocked", "400"),

	ALGORITHM_NOT_SUPPORTED("1238", "Unsupported Algorithm", "Unsupported Algorithm", "500"),

	TOKEN_TYPE_INVALID("1239", "Token is of Invalid Type", "Token is of Invalid Type", "500"),

	CONTRACT_API_FLAG_INVALID("1240", PortalConstants.CONTRACT_API_FLAG_INVALID,PortalConstants.CONTRACT_API_FLAG_INVALID, "500"),

	INVALID_SSA_ISSUER("1241","Issuer of the SSA token is invalid","Issuer of the SSA token is invalid", "400"),

	INVALID_SSA_SIGNATURE("1242","Signature of the SSA token is invalid","Signature of the SSA token is invalid", "400"),

	NO_BLOCK_ACTION_PROVIDED("301", "No Block Action is provided", "No Block Action is provided", "400"),

	USER_ID_DOES_NOT_EXIST("302", "Invalid Request : User not found for User Id provided", "Invalid Request : User not found for User Id provided", "400"),

	USER_ALREADY_DEACTIVATED("303", "The action cannot be performed because User is already in requested status" , "The action cannot be performed because User is already in requested status", "400"),

	USER_ALREADY_ACTIVATED("304", "The action cannot be performed because User is already in requested status", "The action cannot be performed because User is already in requested status", "400"),

	LDAP_ATTRIBUTE_NOT_FOUND_IN_DEVELOPER_PORTAL("305", "The ldap attribute not present in the developer Portal", "The ldap attribute not present in the developer Portal", "400"),

	NO_DESCRIPTIVE_MESSAGE_PROVIDED("306","Invalid Request : No Descriptive message  is provided","Invalid Request : No Descriptive message  is provided","400"),

	NO_SUCH_DEVELOPER_USER_RECORD("307","The specified User does not exist","The specified User does not exist", "400"),

	VALIDATION_ERROR("308", "Validation error found with the provided input", "Validation Error found in the parameter", "400"),
	
	NO_EMAIL_ID_PROVIDED("309","No Valid email-id provided","No Valid email-id provided","400"),
	
	INVALID_EMAIL_ID("310","Invalid E-mail Id provided","Invalid E-mail Id provided ","400");
	

	/** The error code. */
	private String errorCode;

	/** The error message. */
	private String errorMessage;

	/** The detail error message. */
	private String detailErrorMessage;

	/** The status code. */
	private String statusCode;

	/**
	 * Instantiates a new error code enum.
	 *
	 * @param errorCode
	 *            the error code
	 * @param errorMesssage
	 *            the error messsage
	 * @param detailErrorMessage
	 *            the detail error message
	 * @param statusCode
	 *            the status code
	 */
	PortalErrorCodeEnum(String errorCode, String errorMesssage, String detailErrorMessage, String statusCode) {
		this.errorCode = errorCode;
		this.errorMessage = errorMesssage;
		this.detailErrorMessage = detailErrorMessage;
		this.statusCode = statusCode;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Gets the detail error message.
	 *
	 * @return the detail error message
	 */
	public String getDetailErrorMessage() {
		return detailErrorMessage;
	}

	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public String getStatusCode() {
		return statusCode;
	}
}
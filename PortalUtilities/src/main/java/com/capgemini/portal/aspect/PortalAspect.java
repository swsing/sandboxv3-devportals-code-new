package com.capgemini.portal.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.response.filteration.ResponseFilter;
import com.capgemini.psd2.utilities.JSONUtilities;

/**
 * The Class PortalAspect.
 */
@Component
@Aspect
public class PortalAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(PortalAspect.class);

	@Autowired
	private PortalLoggerUtils loggerUtils;

	@Autowired
	private PortalLoggerAttribute loggerAttribute;

	@Value("${app.payloadLog:#{false}}")
	private boolean payloadLog;

	@Value("${app.maskPayloadLog:#{false}}")
	private boolean maskPayloadLog;

	@Value("${app.maskPayload:#{false}}")
	private boolean maskPayload;

	@Autowired
	private DataMask dataMask;

	@Autowired
	private ResponseFilter responseFilterUtility;

	@Around("(execution(* com.capgemini.portal.helper.*.* (..)) || execution(* com.capgemini.portal.mvc.*.validateSession(..)) || execution(* com.capgemini.tppportal.controllers.*.validateSession(..)) || execution(* com.capgemini.portal.service.*.* (..))  || execution(* com.capgemini.portal.mvc.*.downloadTestDataFile(..)))  || execution(* com.capgemini.tppportal.controllers.*.decodeTppTokenFromFile(..)) || execution(* com.capgemini.tppportal.controllers.*.createApplication(..)) || (execution(* com.capgemini.tppportal.services.*.* (..)) && !execution(* com.capgemini.tppportal.services.*.registerOrUpdateTPPOrg(..))) || execution(* com.capgemini.portal.developer..service..*.* (..)))() || execution(* com.capgemini.portal.developer.block.ldap.adapter.impl(..))")
	public Object arroundLoggerAdviceService(ProceedingJoinPoint proceedingJoinPoint) {
		loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
		LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
				proceedingJoinPoint.getSignature().getName(), loggerAttribute);
		try {
			Object result = proceedingJoinPoint.proceed();
			loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute);
			return result;
		} catch (PSD2Exception e) {
			loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
			LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getErrorInfo());

			if (LOGGER.isDebugEnabled()) {
				LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}

			if (e instanceof PSD2Exception) {
				ErrorInfo errorInfo = ((PSD2Exception) e).getErrorInfo();
				errorInfo.setErrorMessageAsNull();
			}
			throw e;
		} catch (Throwable e) {
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(),
					ErrorCodeEnum.TECHNICAL_ERROR);
			loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
			LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, psd2Exception.getErrorInfo());
			if (LOGGER.isDebugEnabled()) {
				LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}

			ErrorInfo errorInfo = psd2Exception.getErrorInfo();
			errorInfo.setErrorMessageAsNull();
			throw psd2Exception;
		}
	}

	@Around("(execution(* com.capgemini.portal.mvc.*.* (..)) && !execution(* com.capgemini.portal.mvc.*.validateSession(..)) "
			+ "&& !execution(* com.capgemini.portal.mvc.*.downloadTestDataFile(..)) || execution(* com.capgemini.tppportal.services.*.registerOrUpdateTPPOrg(..)) || "
			+ "(execution(* com.capgemini.tppportal.controllers.*.*(..))  && !(execution(* com.capgemini.tppportal.controllers.*.decodeTppTokenFromFile(..)) || execution(* com.capgemini.tppportal.controllers.*.createApplication(..)) "
			+ "|| execution(* com.capgemini.tppportal.controllers.*.validateSession(..)) || execution(* com.capgemini.tppportal.controllers.*.redirectUrl(..)) )))"
			+ "|| execution(* com.capgemini.portal.developer..controller..* (..)))()")
	public Object arroundLoggerAdviceClient(ProceedingJoinPoint proceedingJoinPoint) {
		loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
		if (maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\",\"Arguments\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute,
					JSONUtilities.getJSONOutPutFromObject(dataMask.maskRequestLog(proceedingJoinPoint.getArgs(),
							proceedingJoinPoint.getSignature().getName())));

		} else if (!maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\",\"Arguments\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute,
					JSONUtilities.getJSONOutPutFromObject(dataMask.maskMRequestLog(proceedingJoinPoint.getArgs(),
							proceedingJoinPoint.getSignature().getName())));

		} else {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute);

		}
		try {

			Object result = proceedingJoinPoint.proceed();
			loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
			result = responseFilterUtility.filterResponse(result, proceedingJoinPoint.getSignature().getName());

			if (maskPayload)
				dataMask.maskResponse(result, proceedingJoinPoint.getSignature().getName());
			else
				dataMask.maskMResponse(result, proceedingJoinPoint.getSignature().getName());

			if (maskPayloadLog && payloadLog) {
				LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute,
						JSONUtilities.getJSONOutPutFromObject(
								dataMask.maskResponseLog(result, proceedingJoinPoint.getSignature().getName())));
			} else if (!maskPayloadLog && payloadLog) {
				LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute,
						JSONUtilities.getJSONOutPutFromObject(
								dataMask.maskMResponseLog(result, proceedingJoinPoint.getSignature().getName())));
			} else {
				LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute);
			}
			return result;
		} catch (PSD2Exception e) {
			loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
			LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getErrorInfo());

			if (LOGGER.isDebugEnabled()) {
				LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}

			if (e instanceof PSD2Exception) {
				ErrorInfo errorInfo = ((PSD2Exception) e).getErrorInfo();
				errorInfo.setErrorMessageAsNull();
			}
			throw e;
		} catch (Throwable e) {
			loggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(),
					ErrorCodeEnum.TECHNICAL_ERROR);

			LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, psd2Exception.getErrorInfo());
			if (LOGGER.isDebugEnabled()) {
				LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}

			ErrorInfo errorInfo = psd2Exception.getErrorInfo();
			errorInfo.setErrorMessageAsNull();
			throw psd2Exception;
		}
	}

}
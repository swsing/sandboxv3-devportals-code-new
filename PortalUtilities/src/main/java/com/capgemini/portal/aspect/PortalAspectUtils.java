package com.capgemini.portal.aspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.utilities.JSONUtilities;

/**
 * The Class PortalAspect.
 */
@Component
public class PortalAspectUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(PortalAspectUtils.class);

	@Value("${app.payloadLog:#{false}}")
	private boolean payloadLog;

	@Value("${app.maskPayloadLog:#{false}}")
	private boolean maskPayloadLog;

	@Value("${app.maskPayload:#{false}}")
	private boolean maskPayload;

	@Autowired
	private DataMask dataMask;

	public void enterLogger(String className, PortalLoggerAttribute portalLoggerAttribute, Object... params)
			throws Throwable {
		LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\"}", className, portalLoggerAttribute.getOperationName(),
				portalLoggerAttribute);
	}

	public void exitLogger(String className, PortalLoggerAttribute portalLoggerAttribute, String methodType)
			throws Throwable {
		LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\"}", className, portalLoggerAttribute.getOperationName(),
				portalLoggerAttribute);
	}

	public void enterPayloadlogger(String className, PortalLoggerAttribute portalLoggerAttribute, Object... params) {
		if (maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\",\"Arguments\":{}}", className,
					portalLoggerAttribute.getOperationName(), portalLoggerAttribute,
					JSONUtilities.getJSONOutPutFromObject(
							dataMask.maskRequestLog(params, portalLoggerAttribute.getOperationName())));

		} else if (!maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\",\"Arguments\":{}}", className,
					portalLoggerAttribute.getOperationName(), portalLoggerAttribute,
					JSONUtilities.getJSONOutPutFromObject(
							dataMask.maskRequestLog(params, portalLoggerAttribute.getOperationName())));

		} else {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\"}", className, portalLoggerAttribute.getOperationName(),
					portalLoggerAttribute);

		}
	}

	public Object exitPayloadlogger(String className, PortalLoggerAttribute portalLoggerAttribute, Object result) {

		if (maskPayload)
			dataMask.maskResponse(result, portalLoggerAttribute.getOperationName());
		else
			dataMask.maskMResponse(result, portalLoggerAttribute.getOperationName());

		if (maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}", className,
					portalLoggerAttribute.getOperationName(), portalLoggerAttribute,
					JSONUtilities.getJSONOutPutFromObject(
							dataMask.maskResponseLog(result, portalLoggerAttribute.getOperationName())));
		} else if (!maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}", className,
					portalLoggerAttribute.getOperationName(), portalLoggerAttribute,
					JSONUtilities.getJSONOutPutFromObject(
							dataMask.maskMResponseLog(result, portalLoggerAttribute.getOperationName())));
		} else {
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\"}", className, portalLoggerAttribute.getOperationName(),
					portalLoggerAttribute);
		}
		return result;
	}

	public void exitPayloadlogger(String className, PortalLoggerAttribute portalLoggerAttribute) {
		LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}", className, portalLoggerAttribute.getOperationName(),
				portalLoggerAttribute, null);
	}

	public void exceptionPayload(String className, PortalLoggerAttribute portalLoggerAttribute, Exception e) {
		LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{}}", className,
				portalLoggerAttribute.getOperationName(), portalLoggerAttribute,
				JSONUtilities.getJSONOutPutFromObject(e));

		if (LOGGER.isDebugEnabled()) {
			LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}", className,
					portalLoggerAttribute.getOperationName(), portalLoggerAttribute, e.getStackTrace());
		}

		if (e instanceof PSD2Exception) {
			ErrorInfo errorInfo = ((PSD2Exception) e).getErrorInfo();
			errorInfo.setErrorMessage(null);
		}
	}
}

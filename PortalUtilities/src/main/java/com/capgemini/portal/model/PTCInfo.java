/*******************************************************************************

 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

package com.capgemini.portal.model;

import java.util.List;

/**
 * PTCInfo.java is a user info class stored in JWT token used in Session Cookie.
 * 
 */

public class PTCInfo {

	private String ptcId;

	private String ptcName;

	private List<String> organizationIds;

	public String getPtcId() {
		return ptcId;
	}

	public void setPtcId(String ptcId) {
		this.ptcId = ptcId;
	}

	public String getPtcName() {
		return ptcName;
	}

	public void setPtcName(String ptcName) {
		this.ptcName = ptcName;
	}

	public List<String> getOrganizationIds() {
		return organizationIds;
	}

	public void setOrganizationIds(List<String> organizationIds) {
		this.organizationIds = organizationIds;
	}

}

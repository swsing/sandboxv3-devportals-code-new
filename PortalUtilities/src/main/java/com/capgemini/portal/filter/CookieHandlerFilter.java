/*******************************************************************************

 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.portal.config.SessionCookieConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.portal.utilities.SessionCookieUtils;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.crypto.DirectDecrypter;

/**
 * CookieHandlerFilter.java is serving as a Session Management Cookie Filter. *
 * 
 */
public class CookieHandlerFilter extends OncePerRequestFilter {

	@Autowired
	private SessionCookieConfig sessionCookieConfig;

	@Autowired
	private PortalLoggerUtils loggerUtils;

	@Value("${app.cookieFilterpatterns}")
	private String cookieFilterpatterns;

	private static final Logger LOG = LoggerFactory.getLogger(CookieHandlerFilter.class);

	private static final String ABSOLUTE_METHOD_NAME = "com.capgemini.portal.filter.CookieHandlerFilter.doFilterInternal()";

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		try {
			LOG.info("{\"Enter\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"{}\"}", ABSOLUTE_METHOD_NAME,
					request.getRemoteAddr(), request.getRemoteHost(),
					loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME));

			// Find if Session Cookie is available
			Cookie tppPortalSessionCookie = SessionCookieUtils.getTPPPortalCookie(request);

			/*
			 * No check for cookies to URLs that are skipped otherwise parse
			 * cookie, in case of any issue throw Session Expired Exception
			 */

			if (!sessionCookieConfig.getProtectedEndPoints().contains(
					request.getServletPath().substring(request.getServletPath().lastIndexOf(PortalConstants.SLASH)))) {

				if (tppPortalSessionCookie != null
						&& !NullCheckUtils.isNullOrEmpty(tppPortalSessionCookie.getValue())) {

					// Get encrypted JWE token from cookie and parse it to JWE
					// Object
					String jweTokenPayload = tppPortalSessionCookie.getValue();
					JWEObject jweObject = JWEObject.parse(jweTokenPayload);

					// Decrypt JWE Object with JWE Decryption key and get JWT
					// token out of it
					byte[] keyBytes = SessionCookieUtils.getJweDecryptionKey(sessionCookieConfig.getJweDecryptionKey());
					jweObject.decrypt(new DirectDecrypter(keyBytes));

					// check JWT token for its validity
					SessionCookieUtils.checkJWTValidity(jweObject.getPayload().toSignedJWT().getJWTClaimsSet());

				} else {
					throw PortalException.populatePortalException(PortalErrorCodeEnum.SSSEION_EXPIRED_OR_INVALID);
				}

			}
			doFilter(request, response, filterChain);
			LOG.info("{\"Exit\":\"{}\",\"{}\"}", ABSOLUTE_METHOD_NAME,
					loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME));

		} catch (PortalException e) {
			invokeLog(e);
			// forward to login page in case of back button.
			if (request.getRequestURI().contains(PortalConstants.HOME_URL)) {
				request.getRequestDispatcher("/").forward(request, response);
			}
			request.setAttribute(RequestDispatcher.ERROR_EXCEPTION, e);
			request.getRequestDispatcher(PortalConstants.ERROR_URL).forward(request, response);
		} catch (Exception e) {
			invokeLog(e);
			request.setAttribute(RequestDispatcher.ERROR_EXCEPTION, e);
			request.getRequestDispatcher(PortalConstants.ERROR_URL).forward(request, response);
		}

	}

	/**
	 * Below requests will not be filtered in doFilterInternal chain.
	 */
	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.addAll(Arrays.asList(cookieFilterpatterns.split(",")));
		AntPathMatcher matcher = new AntPathMatcher();
		return excludeUrlPatterns.stream().anyMatch(p -> matcher.match(p, request.getServletPath()));
	}

	/**
	 * Log the exceptions for doFilterInternal.
	 * 
	 * @param e
	 */
	private void invokeLog(Exception e) {
		if (e instanceof PortalException)
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}", ABSOLUTE_METHOD_NAME,
					loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME),
					((PSD2Exception) e).getErrorInfo());
		else
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}", ABSOLUTE_METHOD_NAME,
					loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME), e.getMessage());

		LOG.debug("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}", ABSOLUTE_METHOD_NAME,
				loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME), e.getStackTrace(), e);
	}

}

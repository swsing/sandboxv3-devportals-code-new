/*******************************************************************************

 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.portal.config.SessionCookieConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.portal.utilities.SessionCookieUtils;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.ValidationUtility;

/**
 * The Class PortalFilter.
 */
@EnableConfigurationProperties
@ConfigurationProperties("app")
public class PortalFilter extends OncePerRequestFilter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PortalFilter.class);

	private static final String ABSOLUTE_METHOD_NAME = "com.capgemini.portal.logger.PortalFilter.doFilterInternal()";

	/** The req header attribute. */
	@Autowired
	private PortalRequestHeaderAttributes reqHeaderAttribute;

	@Autowired
	private SessionCookieConfig sessionCookieConfig;

	/** The logger utils. */
	@Autowired
	private PortalLoggerUtils loggerUtils;

	@Value("${app.patterns}")
	private List<String> patterns = new ArrayList<>();

	/**
	 * This filter will intercept all incoming requests and set headers. It is
	 * used for Logging purpose.
	 */
	@Override
	public void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse servletResponse,
			FilterChain filterChain) throws ServletException, IOException {

		try {
			populateSecurityHeaders(servletResponse);
			reqHeaderAttribute.setPath(httpServletRequest.getRequestURI());
			reqHeaderAttribute.setMethodType(httpServletRequest.getMethod());
			PTCInfo ptcInfo = SessionCookieUtils.getPTCInfo(httpServletRequest);
			setPortalRequestHeaderAttributes(ptcInfo, httpServletRequest);
			setCorrelationId(httpServletRequest, servletResponse);
			
			filterChain.doFilter(httpServletRequest, servletResponse);
			LOG.info("{\"Exit\":\"{}\",\"{}\"}", ABSOLUTE_METHOD_NAME,
					loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME));

		} catch (PSD2Exception e) {
			invokeLog(e, servletResponse);
		} catch (Exception e) {
			PSD2Exception psd2Exception = null;
			if (e.getCause() instanceof PSD2Exception) {
				psd2Exception = (PSD2Exception) e.getCause();
			} else {
				psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);

			}
			invokeLog(psd2Exception, servletResponse);
			LOG.debug("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}", ABSOLUTE_METHOD_NAME,
					loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME), e.getStackTrace(), e);
		}
	}

	/**
	 * Below requests will not be filtered in doFilterInternal chain.
	 */
	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.addAll(patterns);
		excludeUrlPatterns.add("/build/**");
		excludeUrlPatterns.add("/assets/**");
		excludeUrlPatterns.add("/errors");
		excludeUrlPatterns.add("/**/logout");
		excludeUrlPatterns.add("/");
		AntPathMatcher matcher = new AntPathMatcher();
		return excludeUrlPatterns.stream().anyMatch(p -> matcher.match(p, request.getServletPath()));
	}

	/**
	 * Log the exceptions for doFilterInternal.
	 * 
	 * @param e
	 * @param servletResponse
	 * @throws IOException
	 */
	private void invokeLog(PSD2Exception e, HttpServletResponse servletResponse) throws IOException {
		LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}", ABSOLUTE_METHOD_NAME,
				loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME), e.getErrorInfo());
		LOG.debug("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}", ABSOLUTE_METHOD_NAME,
				loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME), e.getStackTrace(), e);

		servletResponse.setContentType("application/json");
		servletResponse.setStatus(Integer.parseInt(e.getErrorInfo().getStatusCode()));
		servletResponse.getWriter().write(JSONUtilities.getJSONOutPutFromObject(e.getErrorInfo()));
	}

	/**
	 * Set header attributes required for logging.
	 * 
	 * @param ptcInfo
	 * @param httpServletRequest
	 */
	private void setPortalRequestHeaderAttributes(PTCInfo ptcInfo, HttpServletRequest httpServletRequest) {
		if (ptcInfo != null) {
			reqHeaderAttribute.setPtcId(ptcInfo.getPtcId());
			if (httpServletRequest.getRequestURI().equals(PortalConstants.HOME_URL)) {
				reqHeaderAttribute.setTppList(ptcInfo.getOrganizationIds());
			}
		}
		if (httpServletRequest.getParameter("tppOrgId") != null) {
			reqHeaderAttribute.setTppId(httpServletRequest.getParameter("tppOrgId"));
		}
	}

	/**
	 * Generate correlation id at login and set same correlation id till logout.
	 * 
	 * @param httpServletRequest
	 * @param servletResponse
	 */
	private void setCorrelationId(HttpServletRequest httpServletRequest, HttpServletResponse servletResponse) {
		String currentCorrId = null;
		if (httpServletRequest.getParameter(PortalConstants.CORRELATION_ID) != null) {
			currentCorrId = httpServletRequest.getParameter(PortalConstants.CORRELATION_ID);
		}

		if (currentCorrId == null) {
			currentCorrId = httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID);
		}
		if (NullCheckUtils.isNullOrEmpty(currentCorrId)) {
			currentCorrId = GenerateUniqueIdUtilities.getUniqueId().toString();
			httpServletRequest.setAttribute(PSD2Constants.CORRELATION_ID, currentCorrId);
			LOG.info("{\"Enter\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"{}\"}", ABSOLUTE_METHOD_NAME,
					httpServletRequest.getRemoteAddr(), httpServletRequest.getRemoteHost(),
					loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME));
		} else {
			LOG.info("{\"Enter\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"{}\"}", ABSOLUTE_METHOD_NAME,
					httpServletRequest.getRemoteAddr(), httpServletRequest.getRemoteHost(),
					loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME));
		}
		ValidationUtility.isValidUUID(currentCorrId);
		reqHeaderAttribute.setCorrelationId(currentCorrId);
		servletResponse.addHeader(PSD2Constants.CORRELATION_ID, currentCorrId);
	}
	
	
	/**
	 * This method is used to populate the headers in response
	 * @param response
	 */
	private void populateSecurityHeaders(HttpServletResponse response) {
	    response.setHeader(PortalConstants.X_XSS_PROTECTION_HEADER,PortalConstants.X_XSS_PROTECTION_VALUE);
	   response.setHeader(PortalConstants.CACHE_CONTROL_HEADER,PortalConstants.CACHE_CONTROL_VALUE);
	    response.setHeader(PortalConstants.PRAGMA_HEADER,PortalConstants.PRAGMA_VALUE);
	    response.setHeader(PortalConstants.X_FRAME_OPTIONS_HEADER,PortalConstants.X_FRAME_OPTIONS_VALUE);
	    response.setHeader(PortalConstants.X_CONTENT_TYPE_OPTIONS_HEADER,PortalConstants.X_CONTENT_TYPE_OPTIONS_VALUE);
		response.setHeader(PortalConstants.STRICT_TRANSPORT_SECURITY, PortalConstants.STRICT_TRANSPORT_SECURITY_VALUE);
	}

}

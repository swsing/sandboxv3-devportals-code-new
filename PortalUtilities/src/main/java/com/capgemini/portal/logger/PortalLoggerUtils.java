/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.utilities.DateUtilites;

/**
 * The Class PortalLoggerUtils.
 */
@Component
public class PortalLoggerUtils {

	/** The req header attribute. */
	@Autowired
	private PortalRequestHeaderAttributes portalRequestHeaderAttributes;

	/** The logger attribute. */
	@Autowired
	private PortalLoggerAttribute portalLoggerAttribute;

	/** The api id. */
	@Value("${spring.application.name}")
	private String applicationId;

	/**
	 * Populate logger data.
	 *
	 * @return the logger attribute
	 */
	public PortalLoggerAttribute populateLoggerData(String methodName) {
		portalLoggerAttribute.setApplicationId(applicationId);
		if (portalRequestHeaderAttributes != null) {
			portalLoggerAttribute.setCorrelationId(portalRequestHeaderAttributes.getCorrelationId());
			portalLoggerAttribute.setRequestId(portalRequestHeaderAttributes.getRequestId());
			portalLoggerAttribute.setPath(portalRequestHeaderAttributes.getPath());
			portalLoggerAttribute.setMethodType(portalRequestHeaderAttributes.getMethodType());
			portalLoggerAttribute.setPtcId(portalRequestHeaderAttributes.getPtcId());
			portalLoggerAttribute.setTppId(portalRequestHeaderAttributes.getTppId());
			portalLoggerAttribute.setRoles(portalRequestHeaderAttributes.getRoles());
			portalLoggerAttribute.setTppList(portalRequestHeaderAttributes.getTppList());
		}
		portalLoggerAttribute.setMessage(null);
		portalLoggerAttribute.setOperationName(methodName);
		portalLoggerAttribute.setTimeStamp(DateUtilites.generateCurrentTimeStamp());
		return portalLoggerAttribute;
	}

}

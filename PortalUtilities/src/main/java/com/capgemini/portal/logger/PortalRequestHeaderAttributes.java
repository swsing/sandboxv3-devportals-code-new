/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.logger;

import java.util.List;
import java.util.UUID;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;

/**
 * The Class RequestHeaderAttributes.
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PortalRequestHeaderAttributes {

	/** The correlation id. */
	private String correlationId;

	/** The request id. */
	private UUID requestId = GenerateUniqueIdUtilities.getUniqueId();

	/** The self url. */
	private String path;

	private String methodType;

	private String ptcId;

	private String tppId;

	private String roles;

	private List<String> tppList;

	public String getPtcId() {
		return ptcId;
	}

	public void setPtcId(String ptcId) {
		this.ptcId = ptcId;
	}

	/**
	 * Gets the correlation id.
	 *
	 * @return the correlation id
	 */
	public String getCorrelationId() {
		return correlationId;
	}

	/**
	 * Sets the correlation id.
	 *
	 * @param correlationId
	 *            the new correlation id
	 */
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	/**
	 * Gets the request id.
	 *
	 * @return the request id
	 */
	public UUID getRequestId() {
		return requestId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getMethodType() {
		return methodType;
	}

	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	public void setRequestId(UUID requestId) {
		this.requestId = requestId;
	}

	public String getTppId() {
		return tppId;
	}

	public void setTppId(String tppId) {
		this.tppId = tppId;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public List<String> getTppList() {
		return tppList;
	}

	public void setTppList(List<String> tppList) {
		this.tppList = tppList;
	}

}

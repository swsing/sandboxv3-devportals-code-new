/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.logger;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.utilities.DateUtilites;

/**
 * The Class PortalLoggerUtils.
 */
@Component
public class PortalSSAMLoggerUtilities {

	/** The api id. */
	@Value("${spring.application.name}")
	private String applicationId;

	/**
	 * Populate logger data.
	 *
	 * @return the logger attribute
	 */
	public PortalLoggerAttribute populateLoggerData(String methodName, String path, String methodType) {
		PortalLoggerAttribute portalLoggerAttribute = new PortalLoggerAttribute();
		portalLoggerAttribute.setApplicationId(applicationId);
		portalLoggerAttribute.setRequestId(UUID.randomUUID());
		portalLoggerAttribute.setCorrelationId(UUID.randomUUID().toString());
		portalLoggerAttribute.setPath(path);
		portalLoggerAttribute.setMethodType(methodType);
		portalLoggerAttribute.setMessage(null);
		portalLoggerAttribute.setOperationName(methodName);
		portalLoggerAttribute.setTimeStamp(DateUtilites.generateCurrentTimeStamp());
		portalLoggerAttribute.setPtcId(null);
		return portalLoggerAttribute;
	}

	public PortalLoggerAttribute populateTimeStamp(PortalLoggerAttribute loggerAttribute) {
		loggerAttribute.setTimeStamp(DateUtilites.generateCurrentTimeStamp());
		return loggerAttribute;
	}

}

/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.logger;

import java.util.List;
import java.util.UUID;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * The Class PortalLoggerAttribute.
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PortalLoggerAttribute {

	/** The request id. */
	private UUID requestId;

	/** The correlation id. */
	private String correlationId;

	/** The api id. */
	private String applicationId;

	/** The operation name. */
	private String operationName;

	/** message */
	private String message;

	/** path */
	private String path;

	/** The time stamp */
	private String timeStamp;

	private String methodType;

	private String ptcId;

	private String tppId;

	private String roles;

	private List<String> tppList;

	public String getPtcId() {
		return ptcId;
	}

	public void setPtcId(String ptcId) {
		this.ptcId = ptcId;
	}

	public UUID getRequestId() {
		return requestId;
	}

	public void setRequestId(UUID requestId) {
		this.requestId = requestId;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMethodType() {
		return methodType;
	}

	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("timestamp\":\"").append(timeStamp);
		builder.append("\",\"requestId\":\"").append(requestId);
		builder.append("\",\"correlationId\":\"").append(correlationId);
		builder.append("\",\"applicationId\":\"").append(applicationId);

		builder.append("\",\"operationName\":\"").append(operationName);
		builder.append("\",\"message\":\"").append(message);

		builder.append("\",\"path\":\"").append(path);
		builder.append("\",\"methodType\":\"").append(methodType);

		if (ptcId != null) {
			builder.append("\",\"ptcId\":\"").append(ptcId);
		}
		if (tppId != null) {
			builder.append("\",\"tppId\":\"").append(tppId);
		}
		if (roles != null) {
			builder.append("\",\"roles\":\"").append(roles);
		}
		if (tppList != null && !tppList.isEmpty()) {
			builder.append("\",\"tppList\":\"").append(tppList);
		}
		return builder.toString();

	}

	public String getTppId() {
		return tppId;
	}

	public void setTppId(String tppId) {
		this.tppId = tppId;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public List<String> getTppList() {
		return tppList;
	}

	public void setTppList(List<String> tppList) {
		this.tppList = tppList;
	}

}

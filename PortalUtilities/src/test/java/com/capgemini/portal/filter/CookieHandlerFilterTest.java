package com.capgemini.portal.filter;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.portal.config.SessionCookieConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;

@RunWith(SpringJUnit4ClassRunner.class)
public class CookieHandlerFilterTest {

	@InjectMocks
	private CookieHandlerFilter cookieHandlerFilter;
	 
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain filterChain;
	@Mock
	private PortalLoggerUtils loggerUtils;
	@Mock
	private PortalLoggerAttribute loggerAttribute;
	@Mock
	private SessionCookieConfig sessionCookieConfig;
	@Value("${app.cookieFilterpatterns}") 
	private String cookieFilterpatterns;

    
	
	 @Before
		public void setUp() throws Exception {
			MockitoAnnotations.initMocks(this);
			when(loggerUtils.populateLoggerData(anyString())).thenReturn(loggerAttribute);
			cookieHandlerFilter = new CookieHandlerFilter();
		}
			
	 	@SuppressWarnings("unchecked")
		@Test(expected = PortalException.class)
		public void testException() throws ServletException, IOException {
			
	 		List<String> list = new ArrayList<>();
	 		list.add("/");
	 		ReflectionTestUtils.setField(cookieHandlerFilter, "loggerUtils", loggerUtils);
	 		ReflectionTestUtils.setField(cookieHandlerFilter, "cookieFilterpatterns", cookieFilterpatterns);
			when(sessionCookieConfig.getTokenSigningKey()).thenReturn("0123456789abcdef");
			when(sessionCookieConfig.getSessiontimeout()).thenReturn(1800);
			
			when(sessionCookieConfig.getProtectedEndPoints()).thenReturn(list);
			when(request.getServletPath()).thenReturn("/");
			when(request.getRequestDispatcher(anyString())).thenThrow(PortalException.class);
			cookieHandlerFilter.doFilter(request, response, filterChain);
		}
	 	
	 	@SuppressWarnings("unchecked")
		@Test(expected = PortalException.class)
		public void testPortalException() throws ServletException, IOException {
	 		List<String> list = new ArrayList<>();
	 		list.add("/");
	 		
	 		ReflectionTestUtils.setField(cookieHandlerFilter, "loggerUtils", loggerUtils);
	 		ReflectionTestUtils.setField(cookieHandlerFilter, "cookieFilterpatterns", cookieFilterpatterns);
			when(sessionCookieConfig.getTokenSigningKey()).thenReturn("0123456789abcdef");
			when(sessionCookieConfig.getSessiontimeout()).thenReturn(1800);
			when(sessionCookieConfig.getProtectedEndPoints()).thenReturn(list);
			ReflectionTestUtils.setField(cookieHandlerFilter, "sessionCookieConfig", sessionCookieConfig);
			when(request.getServletPath()).thenReturn("/filter");
			when(request.getRequestURI()).thenReturn("https://google.com");
			when(request.getRequestDispatcher(anyString())).thenThrow(PortalException.class);
			cookieHandlerFilter.doFilter(request, response, filterChain);
		}
	 	
	 	@Test(expected = PortalException.class)
		public void testCookieException() throws ServletException, IOException {
	 		List<String> list = new ArrayList<>();
	 		list.add("/");
	 		ReflectionTestUtils.setField(cookieHandlerFilter, "loggerUtils", loggerUtils);
	 		ReflectionTestUtils.setField(cookieHandlerFilter, "cookieFilterpatterns", cookieFilterpatterns);
			when(sessionCookieConfig.getTokenSigningKey()).thenReturn("0123456789abcdef");
			when(sessionCookieConfig.getSessiontimeout()).thenReturn(1800);
			when(sessionCookieConfig.getProtectedEndPoints()).thenReturn(list);
			
			String tokenSigningKey = "tokenSigningKey";
			
			Cookie cookie = new Cookie(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE, "21edwjfhcjsafasdgjsadjf");
			
			cookie.setMaxAge(1800);
			Cookie[] cookies = new Cookie[2];
			cookies[0] = cookie;
			when(request.getCookies()).thenReturn(cookies);
			
			
			ReflectionTestUtils.setField(cookieHandlerFilter, "sessionCookieConfig", sessionCookieConfig);
			when(request.getServletPath()).thenReturn("/filter");
			when(request.getRequestURI()).thenReturn("https://google.com");
			when(request.getRequestDispatcher(anyString())).thenThrow(PortalException.class);
			cookieHandlerFilter.doFilter(request, response, filterChain);
		}
	 			
}

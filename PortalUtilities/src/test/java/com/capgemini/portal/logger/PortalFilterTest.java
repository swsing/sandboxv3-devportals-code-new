/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.logger;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.AntPathMatcher;

import com.capgemini.portal.config.SessionCookieConfig;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;

/**
 * The Class PSD2FilterTest.
 */
public class PortalFilterTest {
	
	/** The http servlet request. */
	@Mock
	private HttpServletRequest httpServletRequest;
	
	/** The request header attributes. */
	@Mock
	private PortalRequestHeaderAttributes requestHeaderAttributes;
	
	/** The http servlet response. */
	@Mock
	private HttpServletResponse httpServletResponse;
	
	/** The filter chain. */
	@Mock
	private FilterChain filterChain;
	
	/** The logger utils. */
	@Mock
	private PortalLoggerUtils loggerUtils;
	
	@Mock
	private SessionCookieConfig sessionCookieConfig;
	
	
	
	/** The psd 2 filter. */
	@InjectMocks
	private PortalFilter psd2Filter = new PortalFilter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
		
	/**
	 * Test.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Test 
	public void test() throws IOException, ServletException{
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		ReflectionTestUtils.setField(psd2Filter, "sessionCookieConfig", sessionCookieConfig);
		when(sessionCookieConfig.getTokenSigningKey())
				.thenReturn("2K4M5N7Q8R9TBUCVEXFYG2J3K4N6P7Q9SATBUDWEXFZH2J3M5N6P8R9SAUqwerty");
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
	}
	
	/**
	 * Testdo filter internal exceptions.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Test
	public void testdoFilterInternalExceptions() throws IOException, ServletException {
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS)).thenReturn("10.1.1.2");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_LAST_LOGGED_TIME)).thenReturn("10-10-2017T10:10");
		Mockito.when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
		
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
		
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
				
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
	}
	
	/**
	 * Testdo filter internal exceptions 2.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Test
	public void testdoFilterInternalExceptions2() throws IOException, ServletException {
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS)).thenReturn("10.1.1.2");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_LAST_LOGGED_TIME)).thenReturn("10-10-2017T10:10");
		Mockito.when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
				
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);	
		assertNotNull(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID));
		
	}	
	
	@Test
	public void testdoFilterInternalCurrentCorrIdNull() throws IOException, ServletException {
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn(null);
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS)).thenReturn("10.1.1.2");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_LAST_LOGGED_TIME)).thenReturn("10-10-2017T10:10");
		Mockito.when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
				
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);	
		
	}	
	
	@Test
	public void testdoFilterInternalPSD2Exception() throws IOException, ServletException {
		ErrorInfo errorInfo = new ErrorInfo("110","Technical Error.","500");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenThrow(new PSD2Exception("Technical Error.", errorInfo));
		Mockito.when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
			
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);	
		
	}	
	
	@Test
	public void testdoFilterInternalException() throws IOException, ServletException {
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenThrow(new RuntimeException("Technical Error."));
		Mockito.when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
			
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);	
		
	}	
	
	@Test
	public void testdoFilterInternalExceptionsBearerToken() throws IOException, ServletException {
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_IP_ADDRESS)).thenReturn("10.1.1.2");
		Mockito.when(httpServletRequest.getHeader(PSD2Constants.CUSTOMER_LAST_LOGGED_TIME)).thenReturn("10-10-2017T10:10");
		Mockito.when(httpServletResponse.getWriter()).thenReturn(new PrintWriter("servletResponse"));
		
		
		psd2Filter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
		
	}
	
//	@Test
//	public void testshouldNotFilter() throws IOException,ServletException {
//		 AntPathMatcher antPathMatcher = new AntPathMatcher();
//	  String pattern= "/restart,/health,/css/**,/fonts/**,/js/**,/localization/**,/img/** ";
//	  
//	  boolean result= antPathMatcher.match(pattern, httpServletRequest.getServletPath());
//	  Assert.assertTrue(result);
//		
//	}
	
	@Test
	public void testshouldNotFilter() throws IOException,ServletException {
		 AntPathMatcher antPathMatcher = new AntPathMatcher();
		 String[] pattern= {"/restart","/health","/css/**"} ;
		 List<String> patterns = Arrays.asList(pattern);
	  ReflectionTestUtils.setField(psd2Filter, "patterns", patterns);
	  when(httpServletRequest.getServletPath()).thenReturn("/restart");
	 // boolean result= antPathMatcher.match(pattern, httpServletRequest.getServletPath());
	 // Assert.assertTrue(result);
	//	ReflectionTestUtils.set(psd2Filter, shouldNotFilter(), "/restart,/health,/css/**,/fonts/**,/js/**,/localization/**,/img/**");
		try {
			Method privateStringMethod = PortalFilter.class.
			        getDeclaredMethod("shouldNotFilter",HttpServletRequest.class);
			privateStringMethod.setAccessible(true);
			privateStringMethod.invoke(psd2Filter, httpServletRequest);
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		httpServletRequest = null;
		requestHeaderAttributes = null;
		httpServletResponse = null;
		filterChain = null;
		loggerUtils = null;
	}
}

/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.capgemini.portal.logger.PortalLoggerAttribute;

/**
 * The Class LoggerAttributeTest.
 */
public class PortalLoggerAttributeTest {
	
	/** The logger attribute. */
	private PortalLoggerAttribute loggerAttribute = null;
	
	/** The uuid. */
	UUID uuid = UUID.randomUUID();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		loggerAttribute = new PortalLoggerAttribute();
		loggerAttribute.setApplicationId("AccountService");
		loggerAttribute.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		loggerAttribute.setMessage("Enter {}: Executing loggerAttributetest()");
		loggerAttribute.setRequestId(uuid);
		loggerAttribute.setOperationName("POST");
		loggerAttribute.setMethodType("POST");
		loggerAttribute.setPath("/test");
		loggerAttribute.setTimeStamp("10-10-2017T10:10");
		loggerAttribute.setPtcId("something@capgemini.com");
		loggerAttribute.setTppId("Q5wqFjpnTAeCtDc1Qx");
		loggerAttribute.setRoles("AISP");
		
		List<String> tppList = new ArrayList<>();
		
		loggerAttribute.setTppList(tppList);
	}

	/**
	 * Logger attributetest.
	 */
	@Test
	public void loggerAttributetest() {
		assertEquals("AccountService", loggerAttribute.getApplicationId());
		assertEquals("ba4f73f8-9a60-425b-aed8-2a7ef2509fea", loggerAttribute.getCorrelationId());
		assertEquals("Enter {}: Executing loggerAttributetest()", loggerAttribute.getMessage());
		assertEquals(uuid, loggerAttribute.getRequestId());
		assertEquals("10-10-2017T10:10", loggerAttribute.getTimeStamp());
		assertEquals("/test", loggerAttribute.getPath());
		assertEquals("POST", loggerAttribute.getMethodType());
		assertEquals("POST", loggerAttribute.getOperationName());
		assertEquals("something@capgemini.com", loggerAttribute.getPtcId());
		assertEquals("Q5wqFjpnTAeCtDc1Qx",loggerAttribute.getTppId());
		assertEquals("AISP",loggerAttribute.getRoles());
		List<String> tppList = new ArrayList<>();
		assertEquals(tppList,loggerAttribute.getTppList());
	}
	
	/**
	 * Test to string.
	 */
	@Test
	public void testToString(){
		assertNotNull(loggerAttribute.toString());
	}
	
	/**
	 * Test equals.
	 */
	@Test
	public void testEquals(){
		assertTrue(loggerAttribute.equals(loggerAttribute));
	}
	
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		loggerAttribute = null;
	}

}

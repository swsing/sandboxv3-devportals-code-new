/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

/**
 * The Class RequestHeaderAttributesTest.
 */
public class PortalRequestHeaderAttributesTest {

	/** The request correlation. */
	private PortalRequestHeaderAttributes requestCorrelation = null;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		requestCorrelation = new PortalRequestHeaderAttributes();
		requestCorrelation.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		requestCorrelation.setPtcId("ba4f73f8-9a60-425b-aed8-2a7ef2509fca");
		requestCorrelation.setTppId("Q5wqFjpnTAeCtDc1Qx");
		requestCorrelation.setRoles("AISP");
		List<String> tppList = new ArrayList<>();
		requestCorrelation.setTppList(tppList);
	}

	/**
	 * Test to string.
	 */
	@Test
	public void testToString() {
		assertNotNull(requestCorrelation.toString());
	}

	/**
	 * Test equals.
	 */
	@Test
	public void testEquals() {
		assertTrue(requestCorrelation.equals(requestCorrelation));
	}

	/**
	 * Test request header attribute.
	 */
	@Test
	public void testRequestHeaderAttribute() {
	
		
		requestCorrelation.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		requestCorrelation.setPtcId("ba4f73f8-9a60-425b-aed8-2a7ef2509fca");
		requestCorrelation.setRequestId(UUID.randomUUID());
		requestCorrelation.setPath("/test");
		requestCorrelation.setMethodType("POST");
		requestCorrelation.setRoles("AISP");

		assertEquals("ba4f73f8-9a60-425b-aed8-2a7ef2509fea", requestCorrelation.getCorrelationId());
		assertEquals("ba4f73f8-9a60-425b-aed8-2a7ef2509fca", requestCorrelation.getPtcId());
		assertEquals("/test",requestCorrelation.getPath());
		assertEquals("AISP",requestCorrelation.getRoles());
		assertEquals("Q5wqFjpnTAeCtDc1Qx",requestCorrelation.getTppId());
	     List<String> tppList= new ArrayList<>();
		assertEquals(tppList,requestCorrelation.getTppList());
		assertEquals("POST",requestCorrelation.getMethodType());
		assertTrue(null != requestCorrelation.getRequestId());

	}

}

/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.logger;

import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * The Class LoggerUtilsTest.
 */
public class PortalLoggerUtilsTest {
	
	/** The request header attributes. */
	@Mock
	PortalRequestHeaderAttributes requestHeaderAttributes;
		
	/** The logger attribute. */
	@Mock
	private PortalLoggerAttribute loggerAttribute;
	
	/** The logger utils. */
	@InjectMocks
	private PortalLoggerUtils loggerUtils = new PortalLoggerUtils();
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Test populate logger data.
	 */
	@Test
	public void testPopulateLoggerData() {
		Mockito.when(requestHeaderAttributes.getCorrelationId()).thenReturn("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Mockito.when(requestHeaderAttributes.getMethodType()).thenReturn("POST");
		Mockito.when(requestHeaderAttributes.getPath()).thenReturn("/test");
		UUID uuid = UUID.randomUUID();
		Mockito.when(requestHeaderAttributes.getRequestId()).thenReturn(uuid);
		loggerAttribute = loggerUtils.populateLoggerData("doFilterInternal()");
	}
	
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		requestHeaderAttributes = null;
		loggerAttribute = null;
	}

}

package com.capgemini.portal.enums;



import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.portal.enums.PortalErrorCodeEnum;




@RunWith(SpringJUnit4ClassRunner.class)
public class PortalErrorCodeEnumTest {

	
	private PortalErrorCodeEnum portalErrorCodeEnum;
	

	@Before
	public void setUp() throws Exception {
		portalErrorCodeEnum = portalErrorCodeEnum.TECHNICAL_ERROR;
	}

	/*@Test
	public void test(){
		assertEquals("1100", portalErrorCodeEnum.getErrorCode()); 
		assertEquals("Technical Error.", portalErrorCodeEnum.getErrorMessage()); 
		assertEquals("Technical Error.",portalErrorCodeEnum.getDetailErrorMessage());
		assertEquals("500", portalErrorCodeEnum.getStatusCode()); 
		
	}*/
	
	@Test
	public void test(){
		assertEquals("1100", portalErrorCodeEnum.getErrorCode()); 
		assertEquals("Technical Error.", portalErrorCodeEnum.getErrorMessage()); 
		assertEquals("Technical Error.",portalErrorCodeEnum.getDetailErrorMessage());
		assertEquals("500", portalErrorCodeEnum.getStatusCode()); 
		
	}
}
			


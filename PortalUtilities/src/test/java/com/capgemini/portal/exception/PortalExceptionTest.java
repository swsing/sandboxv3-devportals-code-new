package com.capgemini.portal.exception;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;

@RunWith(SpringJUnit4ClassRunner.class)
public class PortalExceptionTest {

	@InjectMocks
	PortalException portalException;
	
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expected=PortalException.class)
	public void populatePortalExceptionTest() throws Exception {
	throw PortalException.populatePortalException("test", ErrorCodeEnum.CONNECTION_ERROR);
	}
	
	@Test(expected=PortalException.class)
	public void populatePortalExceptionTest1() throws Exception {
	throw PortalException.populatePortalException(ErrorCodeEnum.CONNECTION_ERROR);
	}
	
	@Test(expected=PortalException.class)
	public void populatePortalExceptionExpTest() throws Exception{
		throw PortalException.populatePortalException("test", PortalErrorCodeEnum.AUTHENTICATION_FAILURE_ERROR);
	}
	@After
	public void tearDown() throws Exception {
		portalException = null;

	}
}

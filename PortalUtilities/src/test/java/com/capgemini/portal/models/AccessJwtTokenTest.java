package com.capgemini.portal.models;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.capgemini.portal.model.AccessJwtToken;

public class AccessJwtTokenTest {

	@InjectMocks
	private AccessJwtToken jwtToken;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testConstructorWithTokenArg (){
		jwtToken = new AccessJwtToken("token");
		assertEquals("token",jwtToken.getToken());
	}
	
}

package com.capgemini.portal.models;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.portal.model.PTCInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class PTCInfoTests {

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void PtcInfoFieldsTests() {
		PTCInfo ptcInfo = new PTCInfo();
		ptcInfo.setPtcId("suhas.gajakosh@capgemini.com");
		ptcInfo.setPtcName("Suhas");
		List<String> organizationIds = new ArrayList<>();
		organizationIds.add("org1");
		ptcInfo.setOrganizationIds(organizationIds);
		//ptcInfo.setCorrelationId("123506");
		
		ptcInfo.getPtcName().equals("Suhas");
		ptcInfo.getPtcId().equals("suhas.gajakosh@capgemini.com");
		ptcInfo.getOrganizationIds().get(0).equals("org1");
		//ptcInfo.getCorrelationId().equals("123506");
	}
}

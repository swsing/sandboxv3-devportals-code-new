package com.capgemini.portal.utilities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.portal.model.JwtToken;
import com.capgemini.portal.model.PTCInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class SessionCookieUtilsTest {
	
	@InjectMocks
	private SessionCookieUtils sessionCookieUtils;
	
	@Mock
	private HttpServletRequest httpServletRequest;
	
	@Mock
	private PortalRequestHeaderAttributes portalRequestHeaderAttributes;
	
	private PTCInfo user;
	
	private Cookie cookie;

	 
	@Before
	public void setUp() {
		user = new PTCInfo();
		user.setPtcId("21242");
		user.setPtcName("user");
		List<String> organizationIds = new ArrayList<>();
		organizationIds.add("Qsdjsjfjd");
		user.setOrganizationIds(organizationIds );
		cookie = new Cookie(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE, "21edwjfhcjsafasdgjsadjf");
	}

	@SuppressWarnings("static-access")
	@Test
	public void createNewSessionCookieTest() {
		Cookie cookie = sessionCookieUtils.createNewSessionCookie();
		assertTrue(cookie.getName().equals(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE));
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void getTPPPortalCookiesTest() {
		Cookie cookie = sessionCookieUtils.createNewSessionCookie();
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(httpServletRequest.getCookies()).thenReturn(cookies);
		Cookie cookie1 = sessionCookieUtils.getTPPPortalCookie(httpServletRequest);
		assertTrue(cookie.getName().equals(cookie1.getName()));
		
		cookies = new Cookie[2];
		cookie = new Cookie("Dummy", null);
		cookies[0] = cookie;
		Cookie cookie2 = new Cookie("Dummy2", null);
		cookies[1] = cookie2;
		when(httpServletRequest.getCookies()).thenReturn(cookies);
		cookie1 = sessionCookieUtils.getTPPPortalCookie(httpServletRequest);
		assertNull(cookie1);
		
	}
			
	@SuppressWarnings("static-access")
	@Test
	@Ignore
	public void modifyTPPPortalCookieTest() {
		
		when(httpServletRequest.getContextPath()).thenReturn("/");
		Cookie cookie1 = sessionCookieUtils.modifyTPPPortalCookie(user, httpServletRequest);
		assertTrue(cookie1.getName().equals(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE));
		assertNotNull(cookie1.getValue());
		
	}
	
	@SuppressWarnings("static-access")
	@Test(expected = Exception.class)
	public void getPTCInfoExceptionTest() {
		String tokenSigningKey = "tokenSigningKey";
    	JwtToken sessionAccessToken = SessionCookieUtils.createSessionJwtToken(user);
		String sessionTokenStr = sessionAccessToken.getToken();
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(1800);
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(httpServletRequest.getCookies()).thenReturn(cookies);
		PTCInfo ptcInfo = sessionCookieUtils.getPTCInfo(httpServletRequest);
		assertNotNull(ptcInfo);
	}
	
	@SuppressWarnings("static-access")
	@Test
	@Ignore
	public void getPTCInfoTest() {
		String tokenSigningKey = "tokenSigningKey";
		JwtToken sessionAccessToken = SessionCookieUtils.createSessionJwtToken(user);
		String sessionTokenStr = sessionAccessToken.getToken();
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(1800);
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(httpServletRequest.getCookies()).thenReturn(cookies);
		PTCInfo ptcInfo = sessionCookieUtils.getPTCInfo(httpServletRequest);
		assertNotNull(ptcInfo);
	}
	
	@SuppressWarnings("static-access")
	@Test
	@Ignore
	public void getPTCInfoTestBranch() {
		String tokenSigningKey = "tokenSigningKey";
		JwtToken sessionAccessToken = SessionCookieUtils.createSessionJwtToken(user);
		String sessionTokenStr = sessionAccessToken.getToken();
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(1800);
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(httpServletRequest.getCookies()).thenReturn(cookies);
		PTCInfo ptcInfo = sessionCookieUtils.getPTCInfo(httpServletRequest);
		assertNotNull(ptcInfo);
	}

	@SuppressWarnings("static-access")
	@Test
	public void getPTCInfoTestNoCookieFound() {
		String tokenSigningKey = "tokenSigningKey";
		Cookie cookie = new Cookie("OtherCookie", "21edwjfhcjsafasdgjsadjf");
		Cookie cookie1 = new Cookie("OtherCookie1", "21edwjfhcjsafasdgjsadjf");
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		cookies[1] = cookie1;
		when(httpServletRequest.getCookies()).thenReturn(cookies);
		PTCInfo ptcInfo = sessionCookieUtils.getPTCInfo(httpServletRequest);
		assertNull(ptcInfo);
	}
	
	@Test
	@SuppressWarnings("static-access")
	public void findCookieTest() {
		Cookie cookie = new Cookie("TPPPORTAL_TEMP_COOKIE", null);
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(httpServletRequest.getCookies()).thenReturn(cookies);
		Cookie cookie1 = sessionCookieUtils.findCookie(httpServletRequest,"TPPPORTAL_TEMP_COOKIE");
		assertTrue(cookie.getName().equals(cookie1.getName()));
		
		cookies = new Cookie[2];
		cookie = new Cookie("Dummy", null);
		cookies[0] = cookie;
		Cookie cookie2 = new Cookie("Dummy2", null);
		cookies[1] = cookie2;
		when(httpServletRequest.getCookies()).thenReturn(cookies);
		cookie1 = sessionCookieUtils.findCookie(httpServletRequest,"Dummy");
		
	}
}

/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.aspect;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.response.filteration.ResponseFilter;


/**
 * The Class PSD2AspectTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PSD2AspectUtilsTest {
	
	/** The data mask. */
	@Mock
	private DataMask dataMask;
	
	/** The logger attribute. */
	@Mock
	private PortalLoggerAttribute loggerAttribute;

	/** The logger utils. */
	@Mock
	private PortalLoggerUtils loggerUtils;

	
	/** The response filter utility. */
	@Mock
	private ResponseFilter responseFilterUtility;
	
	/** The proceeding join point. */
	@Mock
	private ProceedingJoinPoint proceedingJoinPoint;
	
	@Mock
	private JoinPoint joinPoint;
		
	/** The signature. */
	@Mock
	private MethodSignature signature;
	
	/** The aspect. */
	@InjectMocks
	private PortalAspectUtils aspect = new PortalAspectUtils();
	
	/** The req header attribute. */
	@Mock
	private PortalRequestHeaderAttributes reqHeaderAttribute;
	
	/**
	 * Before.
	 */
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(loggerAttribute);
		when(proceedingJoinPoint.getSignature()).thenReturn(signature);
		when(signature.getName()).thenReturn("retrieveAccountBalance");
		when(signature.getDeclaringTypeName()).thenReturn("retrieveAccountBalance");
		when(proceedingJoinPoint.getArgs()).thenReturn(new Object[1]);
		when(joinPoint.getSignature()).thenReturn(signature);
		when(signature.getDeclaringType()).thenReturn(String.class);
	}

	/**
	 * Arround logger advice controller test.
	 */
	
	
	@Test
	public void enterLoggerTest() throws Throwable {
		ReflectionTestUtils.setField(aspect, "payloadLog", true);
		aspect.enterLogger(null, loggerAttribute, null);
	}
	
	@Test
	public void exitLoggerTest() throws Throwable {
		ReflectionTestUtils.setField(aspect, "payloadLog", true);
		aspect.exitLogger(null, loggerAttribute, null);
	}
	
	/**
	 * Arround logger advice controller masking test.
	 */
	@Test
	public void enterPayloadloggerTest() {
		ReflectionTestUtils.setField(aspect, "payloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayload", true);
		aspect.enterPayloadlogger(null, loggerAttribute, null);
		
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", false);
		aspect.enterPayloadlogger(null, loggerAttribute, null);
	}
	
	@Test
	public void enterPayloadlogger1Test() {
		ReflectionTestUtils.setField(aspect, "payloadLog", false);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", false);
		aspect.enterPayloadlogger(null, loggerAttribute, null);
		
		ReflectionTestUtils.setField(aspect, "payloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", false);
		aspect.enterPayloadlogger(null, loggerAttribute, null);
	}
	
	@Test
	public void exitPayloadloggerTest() {
		ReflectionTestUtils.setField(aspect, "payloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayload", true);
		aspect.exitPayloadlogger(null, loggerAttribute, null);
		
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", false);
		aspect.exitPayloadlogger(null, loggerAttribute, null);
	}
	
	@Test
	public void exitPayloadlogger3Test() {
		ReflectionTestUtils.setField(aspect, "payloadLog", false);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", false);
		aspect.exitPayloadlogger(null, loggerAttribute, null);
		
		ReflectionTestUtils.setField(aspect, "payloadLog", true);
		ReflectionTestUtils.setField(aspect, "maskPayloadLog", false);
		aspect.exitPayloadlogger(null, loggerAttribute, null);
	}
	
	@Test
	public void exitPayloadlogger2Test() {		
		
		aspect.exitPayloadlogger(null, loggerAttribute);
	}
	
	@Test
	public void exitPayloadlogger1Test() {
		ReflectionTestUtils.setField(aspect, "payloadLog", true);
		aspect.exitPayloadlogger(null, loggerAttribute, null);
	}
	
	@Test
	public void exceptionPayloadloggerTest() {
		ErrorInfo info = new ErrorInfo();
		info.setErrorCode("400");
		PSD2Exception e = new PSD2Exception("error", info);
		aspect.exceptionPayload(null, loggerAttribute, e);
				
	}
	
	/**
	 * Arround logger advice service impl test.
	 */
	
	
	
}

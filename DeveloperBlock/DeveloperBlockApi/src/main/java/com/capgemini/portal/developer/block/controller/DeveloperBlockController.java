/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.portal.developer.block.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.portal.developer.block.helper.DeveloperBlockHelperImpl;
import com.capgemini.portal.developer.block.service.DeveloperBlockService;
import com.capgemini.portal.developer.block.utilities.DeveloperBlockDataValidator;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.psd2.internal.apis.domain.ApiResponse;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;

@RestController
public class DeveloperBlockController {

	@Autowired
	private DeveloperBlockService apisBlockService;
	
	@Autowired
	private DeveloperBlockHelperImpl developerBlockHelperImpl;
	
	@Autowired
	DeveloperBlockDataValidator developerBlockDataValidator;
	
	@RequestMapping(value = "/developer", method = RequestMethod.PUT)
	public ApiResponse developerBlockApi(@RequestBody DeveloperBlockInput inputBody) {
		developerBlockDataValidator.validateData(inputBody);
		return apisBlockService.updateUserStatus(inputBody);
	}

	@RequestMapping(value = "/developer", method = RequestMethod.GET)
	public ApiResponse developerBlockStatusApi(@RequestParam String emailId) {
		
		if(emailId==null)
		{
			throw PortalException.populatePortalException(PortalErrorCodeEnum.NO_EMAIL_ID_PROVIDED);
		}
		developerBlockHelperImpl.validateEmail(emailId); 
				
		return apisBlockService.userStatusApi(emailId);
	}
}

/**
 * 
 */
package com.capgemini.portal.developer.block.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${auth.ldap.enabled}")
	private boolean authenticationEnabled;

	@Value("${auth.ldap.url}")
	private String ldapUrls;

	@Value("${auth.ldap.username}")
	private String ldapSecurityPrincipal;

	@Value("${ldap.read.password}")
	private String ldapPrincipalPassword;

	@Value("${auth.ldap.iuser.searchbase}")
	private String userSearchBase;

	@Value("${auth.ldap.iuser.searchfilter}")
	private String userSearchFilter;

	@Override
	protected void configure(HttpSecurity http) {
		HttpSessionSecurityContextRepository securityContextRepository = new HttpSessionSecurityContextRepository();
		securityContextRepository.setSpringSecurityContextKey("SPRING_SECURITY_CONTEXT_SAML");

		try {
			http.securityContext().securityContextRepository(securityContextRepository);
			if (authenticationEnabled) {

				http.csrf().disable().requestMatchers().antMatchers("/developer/**").and().authorizeRequests()
				.antMatchers("/developer/**").authenticated().and().httpBasic().and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
			} else {
				http.csrf().disable().authorizeRequests().antMatchers("/").permitAll();
			}
		} catch (Exception e) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
		}
	}





	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		try {
			auth.ldapAuthentication().contextSource().url(ldapUrls).managerDn(ldapSecurityPrincipal)
			.managerPassword(ldapPrincipalPassword).and().userSearchBase(userSearchBase)
			.userSearchFilter(userSearchFilter);
		} catch (Exception e) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
		}
	}


	@Override
	public void init(WebSecurity web) throws Exception {
		super.init(web);
	}


	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/deactivateUserApi");
	}

}

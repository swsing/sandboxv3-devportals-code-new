package com.capgemini.portal.developer.block.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;

@Component
public class DeveloperBlockHelperImpl {

	@Value("${app.regex}")
	private String emailIdRegex;

	public void validateEmail(String emailId) {
		if(!emailId.matches(emailIdRegex)) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.INVALID_EMAIL_ID);
		}
	}
	
}

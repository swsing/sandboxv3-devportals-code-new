package com.capgemini.portal.developer.block;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import com.capgemini.portal.developer.block.routing.adapter.impl.DeveloperBlockRoutingAdapter;
import com.capgemini.portal.logger.PortalFilter;
import com.capgemini.psd2.aisp.adapter.DeveloperBlockAdapter;

@EnableEurekaClient
@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2.internal.apis,com.capgemini.psd2.mask,com.capgemini.psd2.logger,"
		+ "com.capgemini.psd2.validator,com.capgemini.psd2.response,com.capgemini.psd2.aisp.adapter,com.capgemini.portal"})
public class DeveloperBlockApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeveloperBlockApplication.class, args);
	}

	@Bean(name = "developerBlockRoutingAdapter")
	public DeveloperBlockAdapter getDeactivateUserAdapter() {
		return new DeveloperBlockRoutingAdapter();
	}
	
	@Bean
	public PortalFilter portalFilter(){
		return new PortalFilter();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
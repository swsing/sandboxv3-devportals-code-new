package com.capgemini.portal.developer.block.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.portal.developer.block.helper.DeveloperBlockHelperImpl;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DeveloperBlockDataValidator {
	
	@Autowired
	private DeveloperBlockHelperImpl blockHelperImpl;

	public void validateData(DeveloperBlockInput body) {
	
		if (NullCheckUtils.isNullOrEmpty(body)) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.VALIDATION_ERROR);
		}

		if (NullCheckUtils.isNullOrEmpty(body.getEmailId())) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.USER_ID_DOES_NOT_EXIST);
		}
		
		if (NullCheckUtils.isNullOrEmpty(body.getDescription())) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.NO_DESCRIPTIVE_MESSAGE_PROVIDED);
		}

		if (NullCheckUtils.isNullOrEmpty(body.getAction())) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.NO_BLOCK_ACTION_PROVIDED);
		}
		
		blockHelperImpl.validateEmail(body.getEmailId());

	}
}

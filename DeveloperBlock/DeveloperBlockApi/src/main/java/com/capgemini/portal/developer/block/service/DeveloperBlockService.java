package com.capgemini.portal.developer.block.service;

import com.capgemini.psd2.internal.apis.domain.ApiResponse;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;

public interface DeveloperBlockService {

	public ApiResponse updateUserStatus(DeveloperBlockInput inputBody);

	public ApiResponse userStatusApi(String emailId);

}

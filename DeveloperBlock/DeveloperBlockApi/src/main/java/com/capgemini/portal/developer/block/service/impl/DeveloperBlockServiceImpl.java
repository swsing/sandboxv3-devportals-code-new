package com.capgemini.portal.developer.block.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.capgemini.portal.developer.block.service.DeveloperBlockService;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.psd2.aisp.adapter.DeveloperBlockAdapter;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockStatus;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.ApiResponse;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;

@Service
public class DeveloperBlockServiceImpl implements DeveloperBlockService {

	@Autowired
	@Qualifier("developerBlockRoutingAdapter")
	private DeveloperBlockAdapter directoryService;

	private ApiResponse apiResponse = new ApiResponse();

	@Override
	public ApiResponse updateUserStatus(DeveloperBlockInput inputBody) {
		DeveloperBlockStatus accountDetails = directoryService.fetchAccountDetails(inputBody.getEmailId());

		if (accountDetails.getEmailId()==null||accountDetails.isAccountBlockedStatus()==null) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.NO_SUCH_DEVELOPER_USER_RECORD);
		}

		if ((accountDetails.isAccountBlockedStatus()== ActionEnum.BLOCK) &&( inputBody.getAction() ==  ActionEnum.BLOCK )) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.USER_ALREADY_DEACTIVATED);
		}
		
		if ((accountDetails.isAccountBlockedStatus()== ActionEnum.UNBLOCK) &&( inputBody.getAction() ==  ActionEnum.UNBLOCK )) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.USER_ALREADY_ACTIVATED);
		}

		return directoryService.updateUserStatus(inputBody, accountDetails);
	}

	@Override
	public ApiResponse userStatusApi(String emailId) {
		DeveloperBlockStatus accountDetails = directoryService.fetchAccountDetails(emailId);

		if (accountDetails.getEmailId()==null||accountDetails.isAccountBlockedStatus()==null) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.USER_ID_DOES_NOT_EXIST);
		}

		apiResponse.setStatusCode(HttpStatus.OK.toString());
		apiResponse.setGetStatus(accountDetails.isAccountBlockedStatus());
		return apiResponse;
	}

}

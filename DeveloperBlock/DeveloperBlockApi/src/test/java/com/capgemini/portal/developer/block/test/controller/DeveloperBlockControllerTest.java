package com.capgemini.portal.developer.block.test.controller;


import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.portal.developer.block.controller.DeveloperBlockController;
import com.capgemini.portal.developer.block.helper.DeveloperBlockHelperImpl;
import com.capgemini.portal.developer.block.service.DeveloperBlockService;
import com.capgemini.portal.developer.block.utilities.DeveloperBlockDataValidator;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;

public class DeveloperBlockControllerTest {

	@Mock
	private DeveloperBlockService deactivateUserService;
	
	@Mock
	private DeveloperBlockHelperImpl blockHelperImpl;
	
	@Mock
	private DeveloperBlockDataValidator developerBlockDataValidator;
	

	@InjectMocks
	DeveloperBlockController obj = new DeveloperBlockController();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/*@Test
	public void testupdateUserStatus() {
		DeveloperBlockInput inputBody = new DeveloperBlockInput();
		inputBody.setAction(ActionEnum.BLOCK);
		inputBody.setDescription("Dummy Description.");
		String emailId = "abhijeet.aitavade@capgemini.com";
		inputBody.setEmailId(emailId);
		when(developerBlockDataValidator.validateData(inputBody)).thenReturn();
		obj.developerBlockApi(inputBody);
	}*/
	
	/*@Test(expected = PortalException.class)
	public void testupdateUserStatusException() {
		DeveloperBlockInput inputBody = new DeveloperBlockInput();
		inputBody.setAction(ActionEnum.BLOCK);
		inputBody.setDescription("Dummy Description.");
		inputBody.setEmailId(null);
		obj.developerBlockApi(inputBody);
	}*/
	
	@Test
	public void userStatusApi() {
		DeveloperBlockInput inputBody = new DeveloperBlockInput();
		
		inputBody.setAction(ActionEnum.BLOCK);
		inputBody.setDescription("Dummy Description.");
		String emailId = "abhijeet.aitavade@capgemini.com";
		inputBody.setEmailId(emailId);
		obj.developerBlockStatusApi("abhijeet.aitavade@capgemini.com");
	}
	
	@Test(expected = PortalException.class)
	public void userStatusApiException() {
		DeveloperBlockInput inputBody = new DeveloperBlockInput();
		obj.developerBlockStatusApi(null);
	}
}

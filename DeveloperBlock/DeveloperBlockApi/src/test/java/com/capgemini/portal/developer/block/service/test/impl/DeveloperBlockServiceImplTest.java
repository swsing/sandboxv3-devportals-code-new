package com.capgemini.portal.developer.block.service.test.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.portal.developer.block.service.impl.DeveloperBlockServiceImpl;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.psd2.aisp.adapter.DeveloperBlockAdapter;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockStatus;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;

public class DeveloperBlockServiceImplTest {

	@Mock
	private DeveloperBlockAdapter developerBlockAdapter;
	
	@Mock
	DeveloperBlockStatus detail;
	
	@Mock
	private DeveloperBlockInput developerBlockInput;
	
	@InjectMocks
	private DeveloperBlockServiceImpl obj;
	
	
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testupdateUserStatus(){
		DeveloperBlockStatus accountStatus = new DeveloperBlockStatus();
		accountStatus.setAccountBlockedStatus(ActionEnum.BLOCK);
		accountStatus.setEmailId("abhijeet.aitavade@capgemini.com");
		when(developerBlockAdapter.fetchAccountDetails(any(String.class))).thenReturn(accountStatus);
		developerBlockInput.setAction(ActionEnum.UNBLOCK);
		developerBlockInput.setDescription("Dummy Description");
		developerBlockInput.setEmailId("abhijeet.aitavade@capgemini.com");
		obj.updateUserStatus(developerBlockInput);
	}
	
	
	
	@Test
	public void testuserStatusApi(){
		DeveloperBlockStatus accountStatus = new DeveloperBlockStatus();
		accountStatus.setAccountBlockedStatus(ActionEnum.BLOCK);
		accountStatus.setEmailId("abhijeet.aitavade@capgemini.com");
		when(developerBlockAdapter.fetchAccountDetails(any(String.class))).thenReturn(accountStatus);
		developerBlockInput.setAction(ActionEnum.BLOCK);
		developerBlockInput.setDescription("Dummy Description");
		developerBlockInput.setEmailId("abhijeet.aitavade@capgemini.com");
		obj.userStatusApi("abhijeet.aitavade@capgemini.com");
	}
	
	@Test(expected = PortalException.class)
	public void testuserStatusApiException(){
		DeveloperBlockStatus accountStatus = new DeveloperBlockStatus();
		accountStatus.setAccountBlockedStatus(ActionEnum.BLOCK);
		accountStatus.setEmailId(null);
		when(developerBlockAdapter.fetchAccountDetails(any(String.class))).thenReturn(accountStatus);
		developerBlockInput.setAction(ActionEnum.BLOCK);
		developerBlockInput.setDescription("Dummy Description");
		developerBlockInput.setEmailId(null);
		obj.userStatusApi(null);
	}
	
	@Test
	public void testuserStatusApi1(){
		DeveloperBlockStatus accountStatus = new DeveloperBlockStatus();
		accountStatus.setAccountBlockedStatus(ActionEnum.BLOCK);
		accountStatus.setEmailId("");
		when(developerBlockAdapter.fetchAccountDetails(any(String.class))).thenReturn(accountStatus);
		developerBlockInput.setAction(ActionEnum.BLOCK);
		developerBlockInput.setDescription("Dummy Description");
		developerBlockInput.setEmailId(null);
		obj.userStatusApi(null);
	}
	
	@Test(expected = PortalException.class)
	public void testuserStatusApi2(){
		DeveloperBlockStatus accountStatus = new DeveloperBlockStatus();
		accountStatus.setAccountBlockedStatus(ActionEnum.BLOCK);
		accountStatus.setEmailId(null);
		when(developerBlockAdapter.fetchAccountDetails(any(String.class))).thenReturn(accountStatus);
		developerBlockInput.setAction(ActionEnum.BLOCK);
		developerBlockInput.setDescription("Dummy Description");
		developerBlockInput.setEmailId("");
		obj.userStatusApi("");
	}
	@Test(expected = PortalException.class)
	public void testuserStatusApi3(){
		DeveloperBlockStatus accountStatus = new DeveloperBlockStatus();
		accountStatus.setAccountBlockedStatus(null);
		accountStatus.setEmailId("");
		when(developerBlockAdapter.fetchAccountDetails(any(String.class))).thenReturn(accountStatus);
		obj.userStatusApi("");
	}
	@Test(expected = PortalException.class)
	public void testupdateUserStatus1(){
		when(detail.isAccountBlockedStatus()).thenReturn(ActionEnum.BLOCK);
		when(detail.getEmailId()).thenReturn("Dummy id");
		when(developerBlockAdapter.fetchAccountDetails(any(String.class))).thenReturn(detail);
		when(developerBlockInput.getAction()).thenReturn(ActionEnum.BLOCK);
		obj.updateUserStatus(developerBlockInput);
	}
	@Test(expected = PortalException.class)
	public void testupdateUserStatus2(){
		when(detail.isAccountBlockedStatus()).thenReturn(ActionEnum.UNBLOCK);
		when(detail.getEmailId()).thenReturn("Dummy id");
		when(developerBlockAdapter.fetchAccountDetails(any(String.class))).thenReturn(detail);
		when(developerBlockInput.getAction()).thenReturn(ActionEnum.UNBLOCK);
		obj.updateUserStatus(developerBlockInput);
	}
	@Test(expected = PortalException.class)
	public void testupdateUserStatus3(){
		when(detail.isAccountBlockedStatus()).thenReturn(ActionEnum.UNBLOCK);
		when(detail.getEmailId()).thenReturn(null);
		when(developerBlockAdapter.fetchAccountDetails(any(String.class))).thenReturn(detail);
		when(developerBlockInput.getAction()).thenReturn(ActionEnum.UNBLOCK);
		obj.updateUserStatus(developerBlockInput);
	}
}

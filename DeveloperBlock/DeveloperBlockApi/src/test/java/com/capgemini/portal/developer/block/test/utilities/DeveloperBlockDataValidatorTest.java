package com.capgemini.portal.developer.block.test.utilities;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.portal.developer.block.helper.DeveloperBlockHelperImpl;
import com.capgemini.portal.developer.block.utilities.DeveloperBlockDataValidator;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;

public class DeveloperBlockDataValidatorTest {
	
	@InjectMocks
	DeveloperBlockDataValidator developerBlockDataValidator;
	
	@Mock
	private DeveloperBlockHelperImpl blockHelperImpl;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	

	@Test
	public void testValidateData() {
		DeveloperBlockInput obj = new DeveloperBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription("dummy desc");
		String emailId = "example@capgemini.com";
		obj.setEmailId(emailId);
		developerBlockDataValidator.validateData(obj);
	}

	@Test(expected = PortalException.class)
	public void testValidateData1() {
		DeveloperBlockInput obj = new DeveloperBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription("dummy desc");
		String tppId = "123456";
		obj.setEmailId(tppId);
		developerBlockDataValidator.validateData(null);
	}

	@Test(expected = PortalException.class)
	public void testValidateData2() {
		DeveloperBlockInput obj = new DeveloperBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription("dummy desc");
		developerBlockDataValidator.validateData(obj);
	}

	@Test(expected = Exception.class)
	public void testValidateData3() {
		DeveloperBlockInput obj = new DeveloperBlockInput();
		obj.setAction(null);
		obj.setDescription("dummy desc");
		String tppId = "123456";
		obj.setEmailId(tppId);
		developerBlockDataValidator.validateData(obj);
	}

	@Test
	public void testValidateData4() {
		DeveloperBlockInput obj = new DeveloperBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription("deactivate User");
		String emailId = "example@capgemini.com";
		obj.setEmailId(emailId);
		developerBlockDataValidator.validateData(obj);
	}
	
	@Test(expected= PortalException.class)
	public void testValidateDataException() {
		DeveloperBlockInput obj = new DeveloperBlockInput();
		obj.setAction(ActionEnum.BLOCK);
		obj.setDescription(null);
		String tppId = "123456";
		obj.setEmailId(tppId);
		developerBlockDataValidator.validateData(obj);
	}
}

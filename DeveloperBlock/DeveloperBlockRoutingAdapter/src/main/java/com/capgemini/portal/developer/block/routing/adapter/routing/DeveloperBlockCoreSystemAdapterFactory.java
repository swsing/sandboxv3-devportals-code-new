package com.capgemini.portal.developer.block.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.DeveloperBlockAdapter;

@Component
public class DeveloperBlockCoreSystemAdapterFactory implements ApplicationContextAware, DeveloperBlockAdapterFactory {

	private ApplicationContext applicationContext;

	@Override
	public DeveloperBlockAdapter getAdapterInstance(String adapterName){
		return (DeveloperBlockAdapter) applicationContext.getBean(adapterName);		
	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

}

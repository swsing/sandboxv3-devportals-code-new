package com.capgemini.portal.developer.block.routing.adapter.routing;

import com.capgemini.psd2.aisp.adapter.DeveloperBlockAdapter;

@FunctionalInterface
public interface DeveloperBlockAdapterFactory {
	public DeveloperBlockAdapter getAdapterInstance(String coreSystemName);
}

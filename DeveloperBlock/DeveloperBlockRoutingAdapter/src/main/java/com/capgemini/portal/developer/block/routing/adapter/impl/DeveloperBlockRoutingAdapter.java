package com.capgemini.portal.developer.block.routing.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.portal.developer.block.routing.adapter.routing.DeveloperBlockAdapterFactory;
import com.capgemini.psd2.aisp.adapter.DeveloperBlockAdapter;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockStatus;
import com.capgemini.psd2.internal.apis.domain.ApiResponse;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;

public class DeveloperBlockRoutingAdapter implements DeveloperBlockAdapter {

	@Autowired
	private DeveloperBlockAdapterFactory adapters;
	
	@Value("${app.defaultAdapter}")
	private String defaultAdapter;

	@Override
	public DeveloperBlockStatus fetchAccountDetails(String emailId) {
		DeveloperBlockAdapter apisBlockAdapter = adapters.getAdapterInstance(defaultAdapter);
		return apisBlockAdapter.fetchAccountDetails(emailId);
	}

	@Override
	public ApiResponse updateUserStatus(DeveloperBlockInput inputBody, DeveloperBlockStatus accountDetails ) {
		DeveloperBlockAdapter apisBlockAdapter = adapters.getAdapterInstance(defaultAdapter);
		return apisBlockAdapter.updateUserStatus(inputBody, accountDetails);
	}


}

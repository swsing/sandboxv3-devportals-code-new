package com.capgemini.portal.developer.block.routing.adapter.test.routing;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.portal.developer.block.routing.adapter.routing.DeveloperBlockCoreSystemAdapterFactory;


public class DeveloperBlockCoreSystemAdapterFactoryTest {

	@Mock
	private ApplicationContext applicationContext;
	
	@InjectMocks
	private DeveloperBlockCoreSystemAdapterFactory deactivateUserCoreSystemAdapterFactory=new DeveloperBlockCoreSystemAdapterFactory();
	//tppBlockCoreSystemAdapterFactory.setApplicationContext
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testSetterMethod() {
		deactivateUserCoreSystemAdapterFactory.setApplicationContext(applicationContext);
	}
	
	@Test
	public void testGetAdapterInstance() {
		when(applicationContext.getBean(anyString())).thenReturn(null);
		deactivateUserCoreSystemAdapterFactory.getAdapterInstance("deactivateUserLdapAdapter");
	}
}

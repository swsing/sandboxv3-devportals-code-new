package com.capgemini.portal.developer.block.routing.adapter.test.impl;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.portal.developer.block.routing.adapter.impl.DeveloperBlockRoutingAdapter;
import com.capgemini.portal.developer.block.routing.adapter.routing.DeveloperBlockAdapterFactory;
import com.capgemini.psd2.aisp.adapter.DeveloperBlockAdapter;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockStatus;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.ApiResponse;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;

public class DeveloperBlockRoutingAdapterTest {

	@Mock
	private DeveloperBlockAdapterFactory adapters;
	
	@Mock DeveloperBlockInput deactivateUserInput;
	
	@Mock DeveloperBlockStatus accountDetails;
	
	@InjectMocks
	private DeveloperBlockRoutingAdapter obj=new DeveloperBlockRoutingAdapter();
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		deactivateUserInput.setEmailId("abhijeet,aitavade@capgemini.com");
		deactivateUserInput.setDescription("deactivate User");
		deactivateUserInput.setAction(ActionEnum.BLOCK);
	}
	
	@Test
	public void testFetchStatusDetails() {
		ReflectionTestUtils.setField(obj, "defaultAdapter", "deactivateUserAdapter");
		when(adapters.getAdapterInstance(anyString())).thenReturn(new DeveloperBlockAdapter() {

			

			@Override
			public DeveloperBlockStatus fetchAccountDetails(String emailId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public ApiResponse updateUserStatus(DeveloperBlockInput inputBody, DeveloperBlockStatus accountStatusDetail) {
				// TODO Auto-generated method stub
				return null;
			}
			
			
			
		});
		obj.fetchAccountDetails("abhijeet,aitavade@capgemini.com");
		obj.updateUserStatus(deactivateUserInput, accountDetails);
		
		
	}
}

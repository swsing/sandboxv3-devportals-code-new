package com.capgemini.portal.developer.block.constants;

public class DeveloperBlockConstants {

	public static final String ASTRICK = "*";
	public static final String PLUS = "+";
	public static final String CN = "cn=";
	public static final String BLOCK_ATTRIBUTE = "x-block";
	public static final String COMMA = ",";

	public static final String IS_BLOCKED_APIS = "isBlockedApis";

	public static final String EMAIL_ID = "emailId";
	public static final String IS_MEMBER_OF = "isMemberOf";
	public static final String UNIQUE_MEMBER = "uniqueMember";
	public static final String UID = "uid";

	private DeveloperBlockConstants() {
		throw new IllegalAccessError("Constant Class");
	}
}

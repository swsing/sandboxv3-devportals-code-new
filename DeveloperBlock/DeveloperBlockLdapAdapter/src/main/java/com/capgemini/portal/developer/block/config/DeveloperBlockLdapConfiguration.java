package com.capgemini.portal.developer.block.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

@Configuration
public class DeveloperBlockLdapConfiguration {


	@Value("${ldap.url}")
	private String url;

	@Value("${ldap.userDn}")
	private String userDn;

	@Value("${ldap.password}")
	private String ldapPassword;

	@Value("${ldap.baseDn}")
	private String baseDn;

	@Value("${ldap.devuser.baseDn}")
	private String devUserBaseDn;

	@Value("${ldap.developerblock.baseDn}")
	private String developerBlockbaseDn;

	
	@Bean
	public ContextSource contextSource() {
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(url);
		contextSource.setUserDn(userDn);
		contextSource.setPassword(ldapPassword);
		//contextSource.setBase(baseDn);

		return contextSource;
	}

	@Bean
	public LdapTemplate ldapTemplate(){
		return new LdapTemplate(contextSource());
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the userDn
	 */
	public String getUserDn() {
		return userDn;
	}

	/**
	 * @param userDn the userDn to set
	 */
	public void setUserDn(String userDn) {
		this.userDn = userDn;
	}

	/**
	 * @return the ldapPassword
	 */
	public String getLdapPassword() {
		return ldapPassword;
	}

	/**
	 * @param ldapPassword the ldapPassword to set
	 */
	public void setLdapPassword(String ldapPassword) {
		this.ldapPassword = ldapPassword;
	}

	/**
	 * @return the baseDn
	 */
	public String getBaseDn() {
		return baseDn;
	}

	/**
	 * @param baseDn the baseDn to set
	 */
	public void setBaseDn(String baseDn) {
		this.baseDn = baseDn;
	}

	/**
	 * @return the devUserBaseDn
	 */
	public String getDevUserBaseDn() {
		return devUserBaseDn;
	}

	/**
	 * @param devUserBaseDn the devUserBaseDn to set
	 */
	public void setDevUserBaseDn(String devUserBaseDn) {
		this.devUserBaseDn = devUserBaseDn;
	}

	/**
	 * @return the developerBlockbaseDn
	 */
	public String getDeveloperBlockbaseDn() {
		return developerBlockbaseDn;
	}

	/**
	 * @param developerBlockbaseDn the developerBlockbaseDn to set
	 */
	public void setDeveloperBlockbaseDn(String developerBlockbaseDn) {
		this.developerBlockbaseDn = developerBlockbaseDn;
	}

}
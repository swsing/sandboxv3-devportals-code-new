package com.capgemini.portal.developer.block.ldap.adapter.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.InvalidAttributeValueException;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.stereotype.Component;

import com.capgemini.portal.developer.block.config.DeveloperBlockLdapConfiguration;
import com.capgemini.portal.developer.block.constants.DeveloperBlockConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.psd2.aisp.adapter.DeveloperBlockAdapter;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockStatus;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.ApiResponse;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DeveloperBlockLdapAdapter implements DeveloperBlockAdapter {

	@Autowired
	private LdapTemplate ldapTemplate;
	
	@Autowired
	public DeveloperBlockLdapConfiguration ldapConfig;
	
	

	private Name buildBlockedApisDN() {
		return LdapNameBuilder.newInstance().add(ldapConfig.getDeveloperBlockbaseDn()).build();
	}


	private List<Object> getLdapSearchResult(LdapQuery ldapQuery) {
		return ldapTemplate.search(ldapQuery, new AttributesMapper<Object>() {
			@Override
			public Object mapFromAttributes(Attributes attrs) throws NamingException {
				return attrs;
			}
		});
	}

	private static LdapQuery getLdapQuery(String baseDn, String searchAttribute, String attributeValue) {
		return LdapQueryBuilder.query().attributes(DeveloperBlockConstants.ASTRICK, DeveloperBlockConstants.PLUS).base(baseDn)
				.filter("(" + searchAttribute + "=" + attributeValue + ")");
	}

	private static List<String> getBasicAttributes(BasicAttributes object, String ldapAttr) {

		String[] str = null;

		List<String> retrunValue = null;
		try {
			if (object.get(ldapAttr) != null && object.get(ldapAttr).get() != null) {
				str = object.get(ldapAttr).toString().split(":");
				retrunValue = Arrays.asList(str[1].split(", "));
				return retrunValue;
			}
		} catch (org.springframework.ldap.NamingException | NamingException e) {
			throw PortalException.populatePortalException(e.getMessage(),
					PortalErrorCodeEnum.LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP);

		}
		return retrunValue;
	}

	@Override
	public DeveloperBlockStatus fetchAccountDetails(String emailId) {
		List<String> uniqueMemberString = null;
		List<Object> userDetails = new ArrayList<>();
		DeveloperBlockStatus accountStatusDetail = new DeveloperBlockStatus();
		
		userDetails = getDeveloperUserDetails(emailId);
		if (!NullCheckUtils.isNullOrEmpty(userDetails)&&!userDetails.isEmpty()) {
			uniqueMemberString = getBasicAttributes((BasicAttributes) userDetails.get(0), DeveloperBlockConstants.IS_MEMBER_OF);
			if (!NullCheckUtils.isNullOrEmpty(uniqueMemberString)) {
				for (String uniqueMemberDn : uniqueMemberString) {
					{
						if (uniqueMemberDn.contains(ldapConfig.getDeveloperBlockbaseDn()) && !userDetails.isEmpty()) {
							accountStatusDetail.setEmailId(emailId);
							accountStatusDetail.setAccountBlockedStatus(ActionEnum.BLOCK);
						}
					}
				}
				if(NullCheckUtils.isNullOrEmpty(accountStatusDetail)||NullCheckUtils.isNullOrEmpty(accountStatusDetail.getEmailId())) {
					accountStatusDetail.setEmailId(emailId);
					accountStatusDetail.setAccountBlockedStatus(ActionEnum.UNBLOCK);
				}
			}
		}
		return accountStatusDetail;
	}


	private List<Object> getDeveloperUserDetails(String emailId) {
		List<Object> userDetails = new ArrayList<>();
		LdapQuery ldapQuery = getLdapQuery(ldapConfig.getDevUserBaseDn(), DeveloperBlockConstants.UID, emailId);

		userDetails = getLdapSearchResult(ldapQuery);

		return userDetails;
	}

	@Override
	public ApiResponse updateUserStatus(DeveloperBlockInput inputBody, DeveloperBlockStatus accountDetails) {
		ApiResponse response = new ApiResponse();
		try {
			if (inputBody.getAction().getValue() ) {
				if(accountDetails.isAccountBlockedStatus().getValue()){
					throw PortalException.populatePortalException(PortalErrorCodeEnum.USER_ALREADY_DEACTIVATED);
				}else{
					deactivateUser(inputBody.getEmailId());
				}
			} else {
				if(accountDetails.isAccountBlockedStatus().getValue()){
					activateUser(inputBody.getEmailId());
				}else{
					throw PortalException.populatePortalException(PortalErrorCodeEnum.USER_ALREADY_ACTIVATED);
				}
				
			}
			response.setStatusCode("200");

		} catch (InvalidAttributeValueException ex) {
			throw PortalException.populatePortalException(ex.getMessage(),
					PortalErrorCodeEnum.LDAP_ATTRIBUTE_NOT_FOUND_IN_DEVELOPER_PORTAL);
		}
		return response;
	}

	private void deactivateUser(String emailId) {
		List<ModificationItem> mods = new ArrayList<>();
		mods.add(new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("uniqueMember",
				"uid=" + emailId + "," + ldapConfig.getDevUserBaseDn())));

		ldapTemplate.modifyAttributes(buildBlockedApisDN(),
				mods.toArray(new ModificationItem[mods.size()]));
	}

	private void activateUser(String emailId) {
		List<ModificationItem> mods = new ArrayList<>();
		mods.add(new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
				new BasicAttribute("uniqueMember")));

		ldapTemplate.modifyAttributes(buildBlockedApisDN(),
				mods.toArray(new ModificationItem[mods.size()]));
	}
}

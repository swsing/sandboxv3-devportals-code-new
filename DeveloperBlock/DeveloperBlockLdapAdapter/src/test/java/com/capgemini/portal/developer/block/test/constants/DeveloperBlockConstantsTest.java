package com.capgemini.portal.developer.block.test.constants;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.Test;

import com.capgemini.portal.developer.block.constants.DeveloperBlockConstants;

public class DeveloperBlockConstantsTest {

	@Test(expected=Throwable.class)
	public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
	  Constructor<DeveloperBlockConstants> constructor = DeveloperBlockConstants.class.getDeclaredConstructor();
	  assertTrue(Modifier.isPrivate(constructor.getModifiers()));
	  constructor.setAccessible(true);
	  constructor.newInstance();
	}

}

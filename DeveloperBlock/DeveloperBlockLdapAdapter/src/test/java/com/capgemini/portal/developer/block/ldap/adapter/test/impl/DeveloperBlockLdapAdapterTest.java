package com.capgemini.portal.developer.block.ldap.adapter.test.impl;


import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.BasicAttributes;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ldap.InvalidAttributeValueException;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;

import com.capgemini.portal.developer.block.config.DeveloperBlockLdapConfiguration;
import com.capgemini.portal.developer.block.constants.DeveloperBlockConstants;
import com.capgemini.portal.developer.block.ldap.adapter.impl.DeveloperBlockLdapAdapter;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockInput;
import com.capgemini.psd2.internal.apis.domain.DeveloperBlockStatus;

public class DeveloperBlockLdapAdapterTest {

	@Mock
	private LdapTemplate ldapTemplate;

	@Mock
	DeveloperBlockStatus accountStatusDetail;

	@Mock
	private DeveloperBlockLdapConfiguration ldapConfig;


	@Mock
	private DeveloperBlockConstants constants;;

	@Mock
	private DeveloperBlockInput inputBody;


	@InjectMocks
	DeveloperBlockLdapAdapter obj = new DeveloperBlockLdapAdapter();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFetchTppStatusDetails() {
		BasicAttributes attribute  = new BasicAttributes();
		attribute.put("isMemberOf", "cn=deactivatedUsers,ou=deactivatedDevelopers,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com");
		List<Object> userDetails = new ArrayList<>();
		userDetails.add(attribute);

		when(ldapTemplate.search(any(LdapQuery.class), any(AttributesMapper.class))).thenReturn(userDetails);
		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");
		when(ldapConfig.getDeveloperBlockbaseDn()).thenReturn("cn=deactivatedUsers,ou=deactivatedDevelopers,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com");
		obj.fetchAccountDetails("123456");
	}

	@Test
	public void testFetchTppStatusDetails1() {
		BasicAttributes attribute  = new BasicAttributes();
		attribute.put("isMemberOf", "cn=deactivatedUsers,ou=deactivatedDevelopers,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com");
		List<Object> userDetails = new ArrayList<>();
		userDetails.add(attribute);

		when(ldapTemplate.search(any(LdapQuery.class), any(AttributesMapper.class))).thenReturn(userDetails);
		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");
		when(ldapConfig.getDeveloperBlockbaseDn()).thenReturn("cn=deactivatedUsers,ou=deactivatedDevelopers,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com");
		obj.fetchAccountDetails(null);
	}
	@Test
	public void testFetchTppStatusDetails5() {
		BasicAttributes attribute  = new BasicAttributes();
		attribute.put("isMemberOf", "cn=deactivatedUsers,ou=deactivatedDevelopers,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com");
		List<Object> userDetails = new ArrayList<>();
		userDetails.add(attribute);

		when(ldapTemplate.search(any(LdapQuery.class), any(AttributesMapper.class))).thenReturn(userDetails);
		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");
		when(ldapConfig.getDeveloperBlockbaseDn()).thenReturn("cn=dummy,ou=deactivatedDevelopers,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com");
		accountStatusDetail.setAccountBlockedStatus(null);
		obj.fetchAccountDetails(null);
	}

	@Test
	public void testfetchAccountDetails() {
		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");
		List<String> userDetails = new ArrayList<>();
		obj.fetchAccountDetails("abhijeet.aitavade@capgemini.com");
	}
	
	@Test
	public void testfetchAccountDetails2() {
		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");
		List<String> userDetails = new ArrayList<>();
		when(ldapTemplate.search(any(LdapQuery.class), any(AttributesMapper.class))).thenReturn(null);
		obj.fetchAccountDetails("abhijeet.aitavade@capgemini.com");
	}

	@Test
	public void testupdateUserStatus(){
		when(inputBody.getAction()).thenReturn(ActionEnum.BLOCK);
		when(accountStatusDetail.isAccountBlockedStatus()).thenReturn(ActionEnum.UNBLOCK);
		when(inputBody.getEmailId()).thenReturn("abhijeet.aitavade@capgemini.com");
		when(inputBody.getDescription()).thenReturn("Dummy Description");
		when(ldapConfig.getDeveloperBlockbaseDn()).thenReturn("cn=deactivatedUsers,ou=deactivatedDevelopers,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com");
		obj.updateUserStatus(inputBody, accountStatusDetail);
	}

	@Test
	public void testupdateUserStatus1(){
		when(inputBody.getAction()).thenReturn(ActionEnum.UNBLOCK);
		when(accountStatusDetail.isAccountBlockedStatus()).thenReturn(ActionEnum.BLOCK);
		when(inputBody.getEmailId()).thenReturn("abhijeet.aitavade@capgemini.com");
		when(inputBody.getDescription()).thenReturn("Dummy Description");
		when(ldapConfig.getDeveloperBlockbaseDn()).thenReturn("cn=deactivatedUsers,ou=deactivatedDevelopers,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com");
		obj.updateUserStatus(inputBody,accountStatusDetail);
	}

	@Test(expected = PortalException.class)
	public void testupdateUserStatusException(){
		when(accountStatusDetail.isAccountBlockedStatus()).thenReturn(ActionEnum.BLOCK);
		when(inputBody.getAction()).thenThrow(InvalidAttributeValueException.class);
		obj.updateUserStatus(inputBody,accountStatusDetail);
	}


}


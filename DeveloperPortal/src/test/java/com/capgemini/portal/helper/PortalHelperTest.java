package com.capgemini.portal.helper;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import javax.naming.Name;
import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.ModificationItem;
import javax.naming.ldap.LdapName;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.ldap.OperationNotSupportedException;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;

import com.capgemini.mail.config.MailConfig;
import com.capgemini.mail.service.MailService;
import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.config.PortalLdapConfiguration;
import com.capgemini.portal.config.PortalValidationConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.portal.model.ChangePasswordRequest;
import com.capgemini.portal.model.UserProfile;
import com.capgemini.portal.security.user.SAMLUserDetails;
import com.capgemini.template.builder.TemplateBuilder;
import com.capgemini.template.builder.TemplateBuilder.Template;

@RunWith(MockitoJUnitRunner.class)
public class PortalHelperTest {

	@Mock
	HttpServletResponse response;

	@Mock
	PortalLoggerAttribute loggerAttribute;

	@Mock
	PortalLoggerUtils portalLoggerUtils;

	@Mock
	PortalLoggerAttribute populateLoggerData;

	@Mock
	PortalValidationConfig portalValidationConfig;

	@Mock
	private SAMLUserDetails samlUserDetails;

	@Mock
	private UserProfile userProfile;
	@Mock
	BasicAttributes basicAttributes;

	@InjectMocks
	private PortalHelper portalHelper;

	@Mock
	Attribute attr;

	@Mock
	Name name;

	@Mock
	private LdapTemplate ldapTemplate;

	@Mock
	PortalAspectUtils portalAspectUtils;

	@Mock
	LdapName dn;

	@Mock
	PortalLdapConfiguration ldapConfig;

	@Mock
	BasicAttributes devUser;

	@Mock
	ModificationItem modificationItem;

	@Mock
	LdapNameBuilder ldapNameBuilder;

	@Mock
	Environment env;

	@Mock
	HttpServletRequest request;

	@Mock
	TemplateBuilder templateBuilder;

	@Mock
	MailService mailService;

	@Mock
	PortalValidationConfig portalApplicationConfig;

	@Mock
	ChangePasswordRequest cpRequest;

	@Mock
	MailConfig mailConfig;

	@Mock
	ExecutorService executorService;

	@Mock
	MailServicTaskExecutor mailServiceWorker;

	@Mock
	Runnable runnable;

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
		devUser = new BasicAttributes("userPassword", "currentPassword");
		mailConfig.setChangedMsg("Your password for the Bank of Ireland Developer Hub has been successfully changed. You can now login using your new password.");
	}


	@Test
	public void testIsValidCurrentPwd() throws Exception {

		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");

		when(ldapTemplate.authenticate(any(String.class), any(String.class), any(String.class))).thenReturn(true);

		Assert.assertEquals(PortalConstants.VALID, portalHelper.isValidCurrentPwd("testuser@test.com", "oldPwd"));

	}

	@Test
	public void testIsValidCurrentPwdInvalid() throws Exception {

		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");

		when(ldapTemplate.authenticate(any(String.class), any(String.class), any(String.class))).thenReturn(false);

		Assert.assertEquals(PortalConstants.INVALID, portalHelper.isValidCurrentPwd("testuser@test.com", "oldPwd"));

	}

	@Test(expected = DeveloperPortalException.class)
	public void testIsValidCurrentPwdSpringLdapNamingException() throws Exception {

		String msg = "The system was unable to reset the password. Please contact a site administrator for assistance.";
		org.springframework.ldap.NamingException namingException = new org.springframework.ldap.NamingException(msg) {

		};
		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");
		when(ldapTemplate.authenticate(any(String.class), any(String.class), any(String.class)))
		.thenThrow(namingException);

		Assert.assertEquals(env.getProperty("error.code.pwd.change.failed"),
				portalHelper.isValidCurrentPwd("testuser@test.com", "oldPwd"));

	}

	@Test
	public void testupdateDevUserAttr() throws Exception {

		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");

		ldapTemplate.modifyAttributes(any(Name.class), any(ModificationItem[].class));

		Assert.assertEquals(PortalConstants.SUCCESS,
				portalHelper.updateDevUserAttr("testuser@test.com", "newPassword"));

	}

	@Test(expected = DeveloperPortalException.class)
	public void testupdateDevUserAttrExceptionTest() throws Exception {

		javax.naming.OperationNotSupportedException cause = null;

		org.springframework.ldap.OperationNotSupportedException exp = new OperationNotSupportedException(cause);

		when(ldapConfig.getDevUserBaseDn()).thenThrow(exp);
		Assert.assertEquals((PortalErrorCodeEnum.OPERATION_NOT_SUPPORTED),
				portalHelper.updateDevUserAttr("testuser@test.com", "newPassword"));

	}

	@Test
	public void buildDevUserDNTest() throws Exception {

		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");
		name = LdapNameBuilder.newInstance().add(ldapConfig.getDevUserBaseDn()).add("uid", "testuser@test.com").build();

		assertEquals(name, portalHelper.buildDevUserDN("testuser@test.com"));

	}

	@Test
	public void handleLogOutResponseTest() throws Exception {
		Cookie[] cookies = { new Cookie("fname", "ram") };
		when(request.getCookies()).thenReturn(cookies);
		portalHelper.handleLogoutCookies(request, response);
		verify(response).addCookie(any());

	}

	@Test
	public void handleRequestCookiesTest() throws Exception {
		Cookie[] cookies = { new Cookie("fname", "PF") };
		when(request.getCookies()).thenReturn(cookies);

		portalHelper.handleRequestCookies(request, response);
		verify(response).addCookie(any());

	}

	@Test
	public void handleRequestCookies1Test() throws Exception {
		Cookie[] cookies = { new Cookie("fname", "JSESSIONID") };
		when(request.getCookies()).thenReturn(cookies);
		portalHelper.handleRequestCookies(request, response);


	}


	@Test
	public void processInvalidateSessionAndCookiesTest() throws Exception {
		MockHttpSession session = new MockHttpSession();
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setSession(session);
		Cookie cookies = new Cookie("testName", "testValue");
		request.setCookies(cookies);
		portalHelper.processInvalidateSessionAndCookies(request, response, true);
	}

	@Test
	public void processInvalidateSessionAndCookiesElseTest() throws Exception {
		MockHttpSession session = new MockHttpSession();
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setSession(session);
		Cookie cookies = new Cookie("testName", "testValue");
		request.setCookies(cookies);
		portalHelper.processInvalidateSessionAndCookies(request, response, false);

	}

	//	@Test(expected = DeveloperPortalException.class)
	//	public void handleLocalSessionTimeoutTest() throws Exception {
	//		MockHttpSession session = new MockHttpSession();
	//		MockHttpServletRequest request = new MockHttpServletRequest();
	//		request.setSession(session);
	//		Cookie cookies = new Cookie("testName", "testValue");
	//		request.setCookies(cookies);
	//		portalHelper.processInvalidateSessionAndCookies(request, response, true);
	//		portalHelper.handleLocalSessionTimeout(null, request, response, false);
	//
	//	}

	@Test(expected = DeveloperPortalException.class)
	public void handleLocalSessionTimeoutSLOTest() throws Exception {
		MockHttpSession session = new MockHttpSession();
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setSession(session);
		Cookie cookies = new Cookie("testName", "testValue");
		request.setCookies(cookies);
		Mockito.spy(SAMLUserDetails.class);
		portalHelper.processInvalidateSessionAndCookies(request, response, true);
		portalHelper.handleLocalSessionTimeout(samlUserDetails, request, response, true);

	}

	@Test(expected = DeveloperPortalException.class)
	public void handleLocalSessionTimeoutSLO_UserNullTest() throws Exception {
		MockHttpSession session = new MockHttpSession();
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setSession(session);
		Cookie cookies = new Cookie("testName", "testValue");
		request.setCookies(cookies);
		portalHelper.processInvalidateSessionAndCookies(request, response, true);
		portalHelper.handleLocalSessionTimeout(null, request, response, true);

	}

	@Test(expected = DeveloperPortalException.class)
	public void handleLocalSessionTimeoutTes1t() throws Exception {
		MockHttpSession session = new MockHttpSession();
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setSession(session);
		Cookie cookies = new Cookie("testName", "testValue");
		request.setCookies(cookies);


		portalHelper.processInvalidateSessionAndCookies(request, response, false);
		portalHelper.handleLocalSessionTimeout(null, request, response, false);

	}

	//	@Test(expected = DeveloperPortalException.class)
	//	public void handleSessionTimeoutTest() throws Exception {
	//		MockHttpSession session = new MockHttpSession();
	//		MockHttpServletRequest request = new MockHttpServletRequest();
	//		request.setSession(session);
	//		Cookie cookies = new Cookie("testName", "testValue");
	//		request.setCookies(cookies);
	//
	//
	//		portalHelper.processInvalidateSessionAndCookies(request, response, true);
	//		portalHelper.handleSessionTimeout(request, response, false);
	//	}

	//	@Test(expected = DeveloperPortalException.class)
	//	public void handleSessionTimeout1Test() throws Exception {
	//		MockHttpSession session = new MockHttpSession();
	//		MockHttpServletRequest request = new MockHttpServletRequest();
	//		request.setSession(session);
	//		Cookie cookies = new Cookie("testName", "testValue");
	//		request.setCookies(cookies);
	//		portalHelper.processInvalidateSessionAndCookies(request, response, true);
	//
	//		portalHelper.handleSessionTimeout(request, response, false);
	//	}

	@Test(expected = DeveloperPortalException.class)
	public void handleSessionTimeoutSLOTest() throws Exception {
		MockHttpSession session = new MockHttpSession();
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setSession(session);
		Cookie cookies = new Cookie("testName", "testValue");
		request.setCookies(cookies);


		portalHelper.processInvalidateSessionAndCookies(request, response, true);
		portalHelper.handleSessionTimeout(request, response, true);
	}

	@Test(expected = DeveloperPortalException.class)
	public void validateChangePasswordRequestUserNullTest() throws Exception {
		portalHelper.validateChangePasswordRequest(null, cpRequest);

	}

	@Test(expected = DeveloperPortalException.class)
	public void validateChangePasswordRequestNullTest() throws Exception {
		portalHelper.validateChangePasswordRequest(samlUserDetails, null);

	}

	@Test(expected = DeveloperPortalException.class)
	public void validateChangePasswordRequestOldPwdNullTest() throws Exception {
		when(cpRequest.getNewPwd()).thenReturn("Test@2017");
		portalHelper.validateChangePasswordRequest(samlUserDetails, cpRequest);

	}

	@Test(expected = DeveloperPortalException.class)
	public void validateChangePasswordRequestNewPwdNullTest() throws Exception {
		portalHelper.validateChangePasswordRequest(samlUserDetails, cpRequest);

	}


	@Test(expected = DeveloperPortalException.class)
	public void validateChangePasswordRequestNewPwdInvalidTest() throws Exception {
		when(portalValidationConfig.getChangePwdRegxString()).thenReturn("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[^a-zA-Z0-9]).{8,}");
		cpRequest = new ChangePasswordRequest();
		cpRequest.setOldPwd("Test@2017");
		cpRequest.setNewPwd("test12	");
		portalHelper.validateChangePasswordRequest(samlUserDetails, cpRequest);

	}



	@Test
	public void validateChangePasswordRequestSuccessTest() throws Exception {
		when(portalValidationConfig.getChangePwdRegxString()).thenReturn("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[^a-zA-Z0-9]).{8,}");
		cpRequest = new ChangePasswordRequest();
		cpRequest.setOldPwd("Test@2017");
		cpRequest.setNewPwd("Test@2018");
		portalHelper.validateChangePasswordRequest(samlUserDetails, cpRequest);

	}

	@Test
	public void fetchUserProfileTest() throws Exception {
		String uid = "suhas.gajakosh@cg.com";
		List<BasicAttributes> list = new ArrayList<BasicAttributes>();
		list.add(new BasicAttributes(PortalConstants.FIRSTNAME, "Suhas"));
		list.add(new BasicAttributes(PortalConstants.LASTNAME, "Gajakosh"));
		list.add(new BasicAttributes(PortalConstants.ORGANIZATION, "O"));
		list.add(new BasicAttributes(PortalConstants.UID, uid));


		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");
		when(ldapTemplate.search(any(LdapQuery.class), any(AttributesMapper.class))).thenReturn(list);
		portalHelper.fetchUserProfile(uid);
	}

	@Test(expected = DeveloperPortalException.class)
	public void updateUserProfileTest() throws Exception {

		Map<String, String> map = new HashMap<>();
		map.put(PortalConstants.FIRSTNAME, "Suhas");
		map.put(PortalConstants.LASTNAME, "Gajakosh");
		map.put(PortalConstants.ORGANIZATION, "O");
		map.put(PortalConstants.UID, "uid");


		String[] lstArray = { PortalConstants.FIRSTNAME, PortalConstants.LASTNAME, PortalConstants.ORGANIZATION };

		when(samlUserDetails.getAttribute(PortalConstants.SAML_ATTRIBUTE_USER_NAME)).thenReturn("Suhas");
		when(ldapConfig.getDevUserBaseDn()).thenReturn("ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");


		portalHelper.updateUserProfile(samlUserDetails.getAttribute(PortalConstants.SAML_ATTRIBUTE_USER_NAME), map,
				Arrays.asList(lstArray));

	}

	@Test
	public void upouplateUserProfileParametersTest() throws Exception {
		UserProfile userProfile  = new UserProfile();
		userProfile.setFirstName("Suhas");
		userProfile.setLastName("Gajakosh");
		userProfile.setO("O");

		portalHelper.pouplateUserProfileParameters(userProfile);
	}

	@Test
	public void changePwdMailSentOperationTest() throws IOException{
		when(samlUserDetails.getAttribute(PortalConstants.USERNAME)).thenReturn("username");
		when(templateBuilder.getTemplate(any(Template.class),any(HashMap.class))).thenReturn("validHTML");
		DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException("MAIL SENT ERROR",
				PortalErrorCodeEnum.ERROR_SENDIND_MAIL);
		portalHelper.changePwdMailSentOperation(samlUserDetails,userProfile);

	}

	@Test
	public void buildBaseDNTest() throws Exception{
		String baseDn="";
		portalHelper.buildBaseDN(baseDn);
	}

	@Test
	public void changePwdMailSentOperationExceptionTest() throws Exception{
		when(samlUserDetails.getAttribute(PortalConstants.USERNAME)).thenReturn("");
		when(templateBuilder.getTemplate(any(Template.class),any(HashMap.class))).thenReturn("validHTML");
		when(mailConfig.getIsExceptionThrow()).thenReturn(Boolean.TRUE);
		DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException("MAIL SENT ERROR",
				PortalErrorCodeEnum.ERROR_SENDIND_MAIL);
		portalHelper.changePwdMailSentOperation(samlUserDetails,userProfile);
	}
	@Test(expected = DeveloperPortalException.class)
	public void changePwdMailSentOperationExpTest() throws Exception{
		when(samlUserDetails.getAttribute(PortalConstants.USERNAME)).thenReturn("");
		when(templateBuilder.getTemplate(any(Template.class),any(HashMap.class))).thenReturn("validHTML");
		when(mailConfig.getIsExceptionThrow()).thenReturn(Boolean.TRUE);
		DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException("MAIL SENT ERROR",
				PortalErrorCodeEnum.ERROR_SENDIND_MAIL);
		doThrow(DeveloperPortalException.class).when(mailConfig).getChangePwdSuccessMsgSubject();
		
		portalHelper.changePwdMailSentOperation(samlUserDetails,userProfile);
	}
	@Test
	public void changePwdMailSentOperationExp1Test() throws Exception{
		when(samlUserDetails.getAttribute(PortalConstants.USERNAME)).thenReturn("");
		when(templateBuilder.getTemplate(any(Template.class),any(HashMap.class))).thenReturn("validHTML");
		when(mailConfig.getIsExceptionThrow()).thenReturn(Boolean.FALSE);
		DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException("MAIL SENT ERROR",
				PortalErrorCodeEnum.ERROR_SENDIND_MAIL);
		doThrow(DeveloperPortalException.class).when(mailConfig).getChangePwdSuccessMsgSubject();
		
		portalHelper.changePwdMailSentOperation(samlUserDetails,userProfile);
	}
	@Test
	public void validateUpdateUserProfileRequestTest()throws Exception{
		when(userProfile.getFirstName()).thenReturn("firstname");
		when(portalValidationConfig.getFirstName()).thenReturn("firstname");
		when(userProfile.getLastName()).thenReturn("lastName");
		when(portalValidationConfig.getLastName()).thenReturn("lastName");
		when(userProfile.getO()).thenReturn("o");
		when(portalValidationConfig.getOrganization()).thenReturn("o");
		portalHelper.validateUpdateUserProfileRequest(userProfile);
	}

	@Test(expected=DeveloperPortalException.class)
	public void validateUpdateUserProfileRequestException2Test()throws Exception{
		when(userProfile.getFirstName()).thenReturn("firstname");
		when(portalValidationConfig.getFirstName()).thenReturn("firstname");
		when(userProfile.getLastName()).thenReturn("lastName");
		when(portalValidationConfig.getLastName()).thenReturn("lastName");
		when(userProfile.getO()).thenReturn("o");
		when(portalValidationConfig.getOrganization()).thenReturn("Organisation");
		portalHelper.validateUpdateUserProfileRequest(userProfile);
	}

	@Test(expected=DeveloperPortalException.class)
	public void validateUserEmailIdTest() throws Exception{
		String emailId="raksha.saxena@capgemini.com";
		portalHelper.validateUserEmailId(samlUserDetails, emailId);
	}

	@Test
	public void validateUserEmailIdExpTest() throws Exception{
		String emailId="";
		portalHelper.validateUserEmailId(samlUserDetails, emailId);
	}

	@Test(expected=DeveloperPortalException.class)
	public void validateUpdateUserProfileRequestExceptionTest()throws Exception{

		portalHelper.validateUpdateUserProfileRequest(userProfile);
	}

	@Test(expected=DeveloperPortalException.class)
	public void validateUpdateUserProfileRequestException1Test()throws Exception{
		when(userProfile.getFirstName()).thenReturn("firstname");
		when(portalValidationConfig.getFirstName()).thenReturn("firstname");
		portalHelper.validateUpdateUserProfileRequest(userProfile);
	}

	@Test
	public void sentMailAsynchronouslyTest() throws Exception{
		portalHelper.sentMailAsynchronously(samlUserDetails,userProfile);
	}

	
	
	@After
	public void tearDown() throws Exception {
		portalHelper = null;

	}
}

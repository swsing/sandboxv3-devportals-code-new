package com.capgemini.portal.helper;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.capgemini.portal.model.UserProfile;
import com.capgemini.portal.security.user.SAMLUserDetails;

@RunWith(MockitoJUnitRunner.class)
public class MailServiceWorkerTest {
	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL
	 * __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 *  
	 * NOTICE:  All information contained herein is, and remains
	 * the property of CAPGEMINI GROUP.
	 * The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered
	 * by patents, patents in process, and are protected by trade secret
	 * or copyright law.
	 * Dissemination of this information or reproduction of this material
	 * is strictly forbidden unless prior written permission is obtained
	 * from CAPGEMINI GROUP.
	 ******************************************************************************/
	@InjectMocks
	MailServiceWorker mailServiceWorker;
	
	@Mock
	PortalHelper portalHelper;
	
	@Mock
	SAMLUserDetails user;
	
	@Mock
	UserProfile userProfile;
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void MailServiceWorkerTest() throws Exception{
		mailServiceWorker.run();
	}
	
	@Test
	public void MailServiceWorkerExpTest() throws Exception{
		user=null;
		mailServiceWorker.run();
	}
	
	@After
	public void tearDown() throws Exception {
		mailServiceWorker = null;

	}
}

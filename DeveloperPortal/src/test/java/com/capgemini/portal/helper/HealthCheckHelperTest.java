package com.capgemini.portal.helper;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.actuate.health.Status;


@RunWith(MockitoJUnitRunner.class)
public class HealthCheckHelperTest {

	
	@InjectMocks
	HealthCheckHelper healthCheckHelper;
	
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testHealth() {
		
		Assert.assertEquals(Status.UP, healthCheckHelper.health().getStatus());
	}
			
	
	@After
	public void tearDown() throws Exception {
		healthCheckHelper = null;

	}
}

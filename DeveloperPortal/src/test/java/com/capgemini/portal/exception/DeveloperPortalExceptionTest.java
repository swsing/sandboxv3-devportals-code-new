package com.capgemini.portal.exception;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.capgemini.portal.enums.PortalErrorCodeEnum;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class DeveloperPortalExceptionTest {
	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL
	 * __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 *  
	 * NOTICE:  All information contained herein is, and remains
	 * the property of CAPGEMINI GROUP.
	 * The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered
	 * by patents, patents in process, and are protected by trade secret
	 * or copyright law.
	 * Dissemination of this information or reproduction of this material
	 * is strictly forbidden unless prior written permission is obtained
	 * from CAPGEMINI GROUP.
	 ******************************************************************************/
	
	@InjectMocks
	DeveloperPortalException developerPortalException;
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	@Test(expected=DeveloperPortalException.class)
	public void populatePortalExceptionExpTest() throws Exception{
		throw DeveloperPortalException.populateDeveloperPortalException(PortalErrorCodeEnum.AUTHENTICATION_FAILURE_ERROR);
	}
	
	@After
	public void tearDown() throws Exception {
		developerPortalException = null;

	}
}

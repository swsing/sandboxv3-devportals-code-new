package com.capgemini.portal.model;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ValidateSessionResponseTest {

	
	@InjectMocks
	private ValidateSessionResponse validateSessionResponse;
	
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void statusCodeTest() throws Exception{
		validateSessionResponse.setStatusCode("123");
		assertTrue(validateSessionResponse.getStatusCode() == "123");
	}
	
	@After
	public void tearDown() throws Exception {
		validateSessionResponse = null;

	}
}

package com.capgemini.portal.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class UserProfileTest {
	@InjectMocks
	private UserProfile userProfile;
	
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void test() throws Exception {
		userProfile.setContactNumber("12137319287");
		assertTrue("12137319287".equals(userProfile.getContactNumber()));
		
		userProfile.setCountryLocation("Ind");
		assertTrue("Ind".equals(userProfile.getCountryLocation()));
		
		userProfile.setFirstName("Suhas");
		assertTrue("Suhas".equals(userProfile.getFirstName()));
		
		userProfile.setLastName("Gajakosh");
		assertTrue("Gajakosh".equals(userProfile.getLastName()));
		
		userProfile.setUid("emailId");
		assertTrue("emailId".equals(userProfile.getUid()));
		
		userProfile.setO("Capgemini");
		assertTrue("Capgemini".equals(userProfile.getO()));
	}
	
	
	
	
	@After
	public void tearDown() throws Exception {
		userProfile = null;

	}
}

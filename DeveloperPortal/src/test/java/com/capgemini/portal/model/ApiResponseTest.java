package com.capgemini.portal.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ApiResponseTest {
	@InjectMocks
	private ApiResponse apiResponse;
	
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void test() throws Exception {
		
		apiResponse.setStatusCode("200");
		assertTrue("200".equals(apiResponse.getStatusCode()));
	}
	
	
	
	
	@After
	public void tearDown() throws Exception {
		apiResponse = null;

	}
}

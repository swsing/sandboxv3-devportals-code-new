package com.capgemini.portal.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ChangePasswordRequestTest {
	@InjectMocks
	private ChangePasswordRequest changePasswordRequest;
	
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void oldPassTest() throws Exception{
		changePasswordRequest.setOldPwd("123");
		assertTrue(changePasswordRequest.getOldPwd() == "123");
	}
	
	@Test
	public void newPassTest() throws Exception{
		changePasswordRequest.setNewPwd("123");
		assertTrue(changePasswordRequest.getNewPwd() == "123");
	}
	
	
	@After
	public void tearDown() throws Exception {
		changePasswordRequest = null;

	}
}

package com.capgemini.portal.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.ldap.OperationNotSupportedException;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.capgemini.mail.config.MailConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.helper.PortalHelper;
import com.capgemini.portal.model.ChangePasswordRequest;
import com.capgemini.portal.model.ChangePasswordResponse;
import com.capgemini.portal.model.UserProfile;
import com.capgemini.portal.model.ValidateSessionResponse;
import com.capgemini.portal.security.user.SAMLUserDetails;
import com.capgemini.portal.security.utilities.SecurityUtils;
import com.capgemini.portal.service.impl.DevPortalServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class DevPortalServiceImplTest {

	@Mock
	HttpServletResponse response;

	@Mock
	private SAMLUserDetails samlUserDetails;

	@Mock
	HttpServletRequest request;

	@Mock
	Collection<? extends GrantedAuthority> grantedAuthority;

	@Mock
	HttpServletResponse httpResponse;

	@InjectMocks
	private DevPortalServiceImpl devPortalServiceImpl;

	@Mock
	private ModelAndView homeView;

	@Mock
	private ModelAndView changePasswordView;

	@Mock
	private ModelAndView errorView;

	@Mock
	private HttpSession session;

	@Mock
	SecurityUtils utils;

	@Mock
	UserProfile userProfile;
	@Mock
	Exception ex;

	@Mock
	PortalHelper portalHelper;

	@Mock
	ChangePasswordRequest changePasswordRequest;

	@Mock
	private AuthenticationException autexception;

	@Mock
	private NullPointerException npexception;

	@Mock
	ChangePasswordResponse changePasswordResponse;

	@Mock
	ValidateSessionResponse validateSessionResponse;

	@Mock
	SecurityContext context;

	@Mock
	Authentication authentication;

	@Mock
	Environment env;

	@Mock
	MailConfig mailConfig;

	public static final String LOGOUTURL = "logoutURL";
	public static final String MULELAUNCHURL = "muleLaunchURL";
	public static final String SAMLATTRIBUTE = "samlAttributes";
	public static final String ERRORMESSAGE = "errorMessage";
	public static final String ERRORCODE = "errorCode";

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = DeveloperPortalException.class)
	public void testValidateSession_AuthenticationExpired() throws Exception {

		validateSessionResponse = new ValidateSessionResponse();
		validateSessionResponse.setStatusCode(HttpStatus.OK.value() + "");
		Mockito.spy(SAMLUserDetails.class);
		Mockito.when(context.getAuthentication()).thenReturn(authentication);
		Mockito.when(authentication.isAuthenticated()).thenReturn(Boolean.FALSE);
		SecurityContextHolder.setContext(context);

		Assert.assertEquals(validateSessionResponse.getStatusCode(),
				devPortalServiceImpl.validateSession(samlUserDetails, request, httpResponse).getStatusCode());
	}

	@Test(expected = DeveloperPortalException.class)
	public void testValidateSession_IDPSessionExpired() throws Exception {

		validateSessionResponse = new ValidateSessionResponse();
		validateSessionResponse.setStatusCode(HttpStatus.OK.value() + "");
		Mockito.spy(SAMLUserDetails.class);
		Mockito.when(context.getAuthentication()).thenReturn(authentication);
		Mockito.when(authentication.isAuthenticated()).thenReturn(Boolean.TRUE);

		SecurityContextHolder.setContext(context);

		Cookie[] cookies = { new Cookie("fname", "ram") };
	//	when(request.getCookies()).thenReturn(cookies);

		Assert.assertEquals(validateSessionResponse.getStatusCode(),
				devPortalServiceImpl.validateSession(null, request, httpResponse).getStatusCode());
	}

	@Test
	public void testValidateSession_RequestedSessionId() throws Exception {

		validateSessionResponse = new ValidateSessionResponse();
		validateSessionResponse.setStatusCode(HttpStatus.OK.value() + "");
		Mockito.spy(SAMLUserDetails.class);
		Mockito.when(context.getAuthentication()).thenReturn(authentication);
		Mockito.when(authentication.isAuthenticated()).thenReturn(Boolean.TRUE);
		SecurityContextHolder.setContext(context);

		Mockito.when(request.isRequestedSessionIdValid()).thenReturn(Boolean.TRUE);

		Assert.assertEquals(validateSessionResponse.getStatusCode(),
				devPortalServiceImpl.validateSession(samlUserDetails, request, httpResponse).getStatusCode());
	}

	@Test(expected = DeveloperPortalException.class)
	public void testValidateSessionUserNull() throws Exception {

		validateSessionResponse = new ValidateSessionResponse();
		validateSessionResponse.setStatusCode(HttpStatus.OK.value() + "");
		Mockito.spy(SAMLUserDetails.class);
		Mockito.when(context.getAuthentication()).thenReturn(authentication);
		Mockito.when(authentication.isAuthenticated()).thenReturn(Boolean.FALSE);
		SecurityContextHolder.setContext(context);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setSession(session);
		Cookie cookies = new Cookie("testName", "testValue");
		request.setCookies(cookies);
		request.setRequestedSessionIdValid(true);

		Assert.assertEquals(validateSessionResponse.getStatusCode(),
				devPortalServiceImpl.validateSession(null, request, httpResponse).getStatusCode());
	}

	@Test(expected = DeveloperPortalException.class)
	public void testValidateSession_RequestedSessionIdFalse() throws Exception {

		validateSessionResponse = new ValidateSessionResponse();
		validateSessionResponse.setStatusCode(HttpStatus.OK.value() + "");

		Mockito.spy(SAMLUserDetails.class);
		Mockito.when(context.getAuthentication()).thenReturn(authentication);
		Mockito.when(authentication.isAuthenticated()).thenReturn(Boolean.TRUE);
		SecurityContextHolder.setContext(context);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setSession(session);
		Cookie cookies = new Cookie("testName", "testValue");
		request.setCookies(cookies);
		request.setRequestedSessionIdValid(false);
		Assert.assertEquals(validateSessionResponse.getStatusCode(),
				devPortalServiceImpl.validateSession(samlUserDetails, request, httpResponse).getStatusCode());
	}

	@Test(expected = DeveloperPortalException.class)
	public void testValidateSession_RequestedSessionId1() throws Exception {

		validateSessionResponse = new ValidateSessionResponse();
		validateSessionResponse.setStatusCode(HttpStatus.OK.value() + "");
		Mockito.spy(SAMLUserDetails.class);
		Mockito.when(context.getAuthentication()).thenReturn(authentication);
		Mockito.when(authentication.isAuthenticated()).thenReturn(Boolean.TRUE);
		SecurityContextHolder.setContext(context);

		Cookie[] cookies = { new Cookie("fname", "ram") };
		//when(request.getCookies()).thenReturn(cookies);

		Assert.assertEquals(HttpStatus.OK.value(),
				devPortalServiceImpl.validateSession(samlUserDetails, request, httpResponse).getStatusCode());
	}


	@Test
	public void testChangePassword() throws Exception {

		Map<String, String> samlAttributes = new HashMap<String, String>();
		samlAttributes.put("1", "1");
		changePasswordRequest = new ChangePasswordRequest();

		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.OK.value() + "");

		Mockito.spy(SAMLUserDetails.class);
		Mockito.spy(ChangePasswordRequest.class);

		when(portalHelper.isValidCurrentPwd(any(), any())).thenReturn(PortalConstants.VALID);
		when(portalHelper.updateDevUserAttr(any(), any())).thenReturn(PortalConstants.SUCCESS);
		when(mailConfig.getIsAsyncCall()).thenReturn(Boolean.TRUE);

		Assert.assertEquals(changePasswordResponse.getStatusCode(),
				devPortalServiceImpl.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());
	}
	@Test
	public void testChangePasswordException() throws Exception {

		Map<String, String> samlAttributes = new HashMap<String, String>();
		samlAttributes.put("1", "1");
		changePasswordRequest = new ChangePasswordRequest();

		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.OK.value() + "");

		Mockito.spy(SAMLUserDetails.class);
		Mockito.spy(ChangePasswordRequest.class);

		when(portalHelper.isValidCurrentPwd(any(), any())).thenReturn(PortalConstants.VALID);
		when(portalHelper.updateDevUserAttr(any(), any())).thenReturn(PortalConstants.SUCCESS);
		when(mailConfig.getIsAsyncCall()).thenReturn(Boolean.FALSE);

		Assert.assertEquals(changePasswordResponse.getStatusCode(),
				devPortalServiceImpl.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());
	}

	@Test(expected = DeveloperPortalException.class)
	public void testChangePasswordFail() throws Exception {

		Map<String, String> samlAttributes = new HashMap<String, String>();
		samlAttributes.put("1", "1");

		changePasswordRequest = new ChangePasswordRequest();

		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.BAD_REQUEST.value() + "");

		Mockito.spy(SAMLUserDetails.class);

		when(portalHelper.isValidCurrentPwd(any(), any())).thenReturn("PWD_CHANGE_FAILED");

		Assert.assertEquals(changePasswordResponse.getStatusCode(),
				devPortalServiceImpl.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());

	}

	@Test(expected = DeveloperPortalException.class)
	public void testChangePasswordIncorrectCurrPwd() throws Exception {

		changePasswordRequest = new ChangePasswordRequest();
		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.BAD_REQUEST.value() + "");

		changePasswordResponse.setStatusCode(HttpStatus.BAD_REQUEST.value() + "");

		Mockito.spy(SAMLUserDetails.class);
		when(portalHelper.isValidCurrentPwd(any(), any())).thenReturn(PortalConstants.INVALID);

		Assert.assertEquals(HttpStatus.BAD_REQUEST,
				devPortalServiceImpl.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());

	}

	@Test
	public void testChangePasswordUpdateDevUserAttr() throws Exception {

		changePasswordRequest = new ChangePasswordRequest();
		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.OK.value() + "");

		Mockito.spy(SAMLUserDetails.class);

		when(portalHelper.isValidCurrentPwd(any(), any())).thenReturn(PortalConstants.VALID);
		when(portalHelper.updateDevUserAttr(any(), any())).thenReturn(PortalConstants.SUCCESS);

		Assert.assertEquals(changePasswordResponse.getStatusCode(),
				devPortalServiceImpl.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());

	}

	@Test(expected = DeveloperPortalException.class)
	public void testChangePwdUpdateDevUserAttrWithOldPwd() throws Exception {

		javax.naming.OperationNotSupportedException onse = new javax.naming.OperationNotSupportedException();
		OperationNotSupportedException lonse = new OperationNotSupportedException(onse);
		changePasswordRequest = new ChangePasswordRequest();
		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.BAD_REQUEST.value() + "");

		Mockito.spy(SAMLUserDetails.class);

		when(portalHelper.isValidCurrentPwd(any(), any())).thenReturn(PortalConstants.VALID);
		when(portalHelper.updateDevUserAttr(any(), any())).thenThrow(lonse);

		Assert.assertEquals(HttpStatus.BAD_REQUEST,
				devPortalServiceImpl.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());

	}

	@Test
	public void testChangePassworSet() throws Exception {
		changePasswordRequest = new ChangePasswordRequest();
		changePasswordRequest.setOldPwd("test");
		Assert.assertEquals("test", changePasswordRequest.getOldPwd());

	}

	@Test
	public void testChangePassworNewSet() throws Exception {
		changePasswordRequest = new ChangePasswordRequest();
		changePasswordRequest.setNewPwd("test");
		Assert.assertEquals("test", changePasswordRequest.getNewPwd());

	}

	@Test
	public void fetchUserProfileTest() throws Exception {

		Mockito.spy(SAMLUserDetails.class);
		String emailId = "suhas.gajakosh@capgemini.com";
		UserProfile userProfile = new UserProfile();
		userProfile.setFirstName("Suhas");
		userProfile.setLastName("Gajakosh");
		userProfile.setUid(emailId);

		when(samlUserDetails.getAttribute(PortalConstants.SAML_ATTRIBUTE_USER_NAME)).thenReturn(emailId);
		when(portalHelper.fetchUserProfile(emailId)).thenReturn(userProfile);

		UserProfile profile = devPortalServiceImpl.fetchUserProfile(samlUserDetails);

		Assert.assertEquals(emailId, profile.getUid());
		Assert.assertEquals(userProfile.getFirstName(), profile.getFirstName());		

	}
	@Test
	public void updateUserProfileTest() throws Exception{

		devPortalServiceImpl.updateUserProfile(samlUserDetails,userProfile);
	}


	@Test
	public void logoutTest() throws Exception {
		Cookie[] cookies = { new Cookie("fname", "PF") };
		when(request.getCookies()).thenReturn(cookies);

		devPortalServiceImpl.logout(samlUserDetails, request, httpResponse);
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		devPortalServiceImpl = null;

	}

}
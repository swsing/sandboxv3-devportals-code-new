package com.capgemini.portal.config;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class PortalLdapConfigurationTest {
	
	@InjectMocks
	PortalLdapConfiguration ldapConfig;
	
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void urlTest() throws Exception{
		ldapConfig.setUrl("123");
		assertTrue(ldapConfig.getUrl() == "123");
	}
	
	@Test
	public void ldapPasswordTest() throws Exception{
		ldapConfig.setLdapPassword("123");
		assertTrue(ldapConfig.getLdapPassword() == "123");
	}
	
	@Test
	public void baseDnTest() throws Exception{
		ldapConfig.setBaseDn("123");
		assertTrue(ldapConfig.getBaseDn() == "123");
	}
	
	@Test
	public void userDnTest() throws Exception{
		ldapConfig.setUserDn("123");
		assertTrue(ldapConfig.getUserDn() == "123");
	}
	
	@Test
	public void devUserBaseDnTest() throws Exception{
		ldapConfig.setDevUserBaseDn("123");
		assertTrue(ldapConfig.getDevUserBaseDn() == "123");
	}
	
	
	@After
	public void tearDown() throws Exception {
		ldapConfig = null;

	}

}

package com.capgemini.portal.mvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.servlet.ModelAndView;

import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.helper.PortalHelper;
import com.capgemini.portal.model.ChangePasswordRequest;
import com.capgemini.portal.model.ChangePasswordResponse;
import com.capgemini.portal.model.ValidateSessionResponse;
import com.capgemini.portal.security.user.SAMLUserDetails;
import com.capgemini.portal.security.utilities.SecurityUtils;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

	@Mock
	private SAMLUserDetails samlUserDetails;

	@Mock
	HttpServletRequest request;

	@InjectMocks
	private UserController userController;

	@Mock
	private ModelAndView homeView;

	@Mock
	private ModelAndView changePasswordView;

	@Mock
	private ModelAndView errorView;

	@Mock
	private HttpSession session;

	@Mock
	SecurityUtils utils;

	@Mock
	Exception ex;

	@Mock
	PortalHelper portalHelper;

	@Mock
	HttpServletResponse httpResponse;

	@Mock
	ChangePasswordRequest changePasswordRequest;

	@Mock
	private AuthenticationException autexception;

	@Mock
	private NullPointerException npexception;

	@Mock
	ChangePasswordResponse changePasswordResponse;

	@Mock
	ValidateSessionResponse validateSessionResponse;
	
	@Value("${tppPortalURL}")
	private String tppPortalURL;

	@Mock
	Environment env;

	public static final String LOGOUTURL = "logoutURL";
	public static final String MULELAUNCHURL = "muleLaunchURL";
	public static final String SAMLATTRIBUTE = "samlAttributes";
	public static final String ERRORMESSAGE = "errorMessage";
	public static final String ERRORCODE = "errorCode";
	public static final String SAML_ATTRIBUTE_USER_NAME = "username";
	public static final String SAML_ATTRIBUTE_GROUP = "group";
	public static final String SAML_ATTRIBUTE_USER_TYPE = "userType";
	public static final String PORTALBASEURL = "portalBaseURL";
	public static final String SAML_ATTRIBUTE_EMAIL_ID = "emailId";
	public static final String UID = "uid";
	public static final String TPPPORTAL_URL = "TPPPortalURL";
	public static final String IS_BLOCKED_APIS = "isBlockedApis";
	public static final String OVER_VIEW_PAGE_URL = "OverviewPageURL";
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}

	//@Test
	public void loadLandingPageTestDevportal() throws Exception {

		
		Map<String, String> samlAttributes = new HashMap<String, String>();
		samlAttributes.put("displayname", "Abhijeet");
		samlAttributes.put(SAML_ATTRIBUTE_GROUP, "cn=Portals Viewer,ou=dev,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com");
		samlAttributes.put(SAML_ATTRIBUTE_EMAIL_ID, "abhijeet.aitavade@capgemini.com");
		
		homeView = new ModelAndView("index");
		homeView.addObject(LOGOUTURL, LOGOUTURL);
		homeView.addObject(PORTALBASEURL, PORTALBASEURL);
		homeView.addObject(OVER_VIEW_PAGE_URL, OVER_VIEW_PAGE_URL);
		homeView.addObject(MULELAUNCHURL, MULELAUNCHURL + "devportal");
		homeView.addObject(SAMLATTRIBUTE, samlAttributes);
		homeView.addObject(SAML_ATTRIBUTE_USER_NAME, "Abhijeet");
		homeView.addObject(SAML_ATTRIBUTE_USER_TYPE, "devportal");
		homeView.addObject(SAML_ATTRIBUTE_EMAIL_ID, null);
		homeView.addObject(TPPPORTAL_URL, null);
		homeView.addObject(IS_BLOCKED_APIS, null);
		homeView.addObject(PortalConstants.IS_LOGGED_IN, true);
		homeView.addObject(PortalConstants.MULE_PUBLIC_LAUNCHURL, MULELAUNCHURL);
		homeView.addObject("userAction", "userAction");
		
		Mockito.spy(SAMLUserDetails.class);
		when(utils.isSLOEnable()).thenReturn(true);
		when(utils.getIDPLogoutURL()).thenReturn(LOGOUTURL);
		when(utils.getMuleLaunchURL()).thenReturn(MULELAUNCHURL);
		when(utils.getPortalBaseURL()).thenReturn(OVER_VIEW_PAGE_URL);
		when(utils.getBaseURL()).thenReturn(PORTALBASEURL);
		when(samlUserDetails.getAttributes()).thenReturn(samlAttributes);
		when(samlUserDetails.getAttribute("displayname")).thenReturn("Abhijeet");
		when(samlUserDetails.getAttribute("userAction")).thenReturn("userAction");
		when(utils.getMulePublicLaunchUrl()).thenReturn(MULELAUNCHURL);
		when(samlUserDetails.getAttribute("userAction")).thenReturn("userAction");
		//when(samlUserDetails.getAttribute(SAML_ATTRIBUTE_EMAIL_ID)).thenReturn("abhijeet.aitavade@capgemini.com");
		//when(PortalConstants.IS_LOGGED_IN).thenReturn(Boolean.toString(true));
		
		Assert.assertEquals(homeView.getModel(), userController.loadLandingPage(samlUserDetails).getModel());
		Assert.assertEquals(homeView.getView(), userController.loadLandingPage(samlUserDetails).getView());
	}

	//@Test
	public void loadLandingPageTestDevportalSloFalse() throws Exception {

		
		Map<String, String> samlAttributes = new HashMap<String, String>();
		samlAttributes.put("displayname", "Abhijeet");
		samlAttributes.put(SAML_ATTRIBUTE_GROUP, "cn=Portals Viewer,ou=dev,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com");
		samlAttributes.put(SAML_ATTRIBUTE_EMAIL_ID, "abhijeet.aitavade@capgemini.com");
		
		homeView = new ModelAndView("index");
		homeView.addObject(LOGOUTURL, LOGOUTURL);
		homeView.addObject(PORTALBASEURL, PORTALBASEURL);
		homeView.addObject(OVER_VIEW_PAGE_URL, OVER_VIEW_PAGE_URL);
		homeView.addObject(MULELAUNCHURL, MULELAUNCHURL + "devportal");
		homeView.addObject(SAMLATTRIBUTE, samlAttributes);
		homeView.addObject(SAML_ATTRIBUTE_USER_NAME, "Abhijeet");
		homeView.addObject(SAML_ATTRIBUTE_USER_TYPE, "devportal");
		homeView.addObject(SAML_ATTRIBUTE_EMAIL_ID, null);
		homeView.addObject(TPPPORTAL_URL, null);
		homeView.addObject(IS_BLOCKED_APIS, null);
		homeView.addObject(PortalConstants.IS_LOGGED_IN, true);
		homeView.addObject(PortalConstants.MULE_PUBLIC_LAUNCHURL, MULELAUNCHURL);
		homeView.addObject("userAction", "userAction");
		
		Mockito.spy(SAMLUserDetails.class);
		when(utils.isSLOEnable()).thenReturn(false);
		when(utils.getLocalLogoutUrl()).thenReturn(LOGOUTURL);
		when(utils.getMuleLaunchURL()).thenReturn(MULELAUNCHURL);
		when(utils.getPortalBaseURL()).thenReturn(PORTALBASEURL);
		when(utils.getBaseURL()).thenReturn(OVER_VIEW_PAGE_URL);
		when(samlUserDetails.getAttributes()).thenReturn(samlAttributes);
		when(samlUserDetails.getAttribute("displayname")).thenReturn("Abhijeet");
		when(utils.getMulePublicLaunchUrl()).thenReturn(MULELAUNCHURL);
		when(samlUserDetails.getAttribute("userAction")).thenReturn("userAction");
		//when(samlUserDetails.getAttribute(SAML_ATTRIBUTE_EMAIL_ID)).thenReturn("abhijeet.aitavade@capgemini.com");

		Assert.assertEquals(homeView.getModel(), userController.loadLandingPage(samlUserDetails).getModel());
		Assert.assertEquals(homeView.getView(), userController.loadLandingPage(samlUserDetails).getView());
	}
	
	@Test
	public void loadLandingPageTestDevportalElse() throws Exception {

		Assert.assertEquals(homeView.getModel(), userController.loadLandingPage(null).getModel());
		Assert.assertEquals(homeView.getView(), userController.loadLandingPage(null).getView());
	}

	@Test(expected = Exception.class)
	public void loadLandingPageTestDevportalCatch() throws Exception {
		Exception e = new Exception(SAMLATTRIBUTE);
		doThrow(e).when(userController.loadLandingPage(samlUserDetails).getModel());
		doThrow(e).when(userController.loadLandingPage(samlUserDetails).getView());

	}

	@Test
	public void loadLandingPageSessionExpiredTest() throws Exception {

		doNothing().when(httpResponse).sendRedirect(any());
		Assert.assertEquals(homeView.getModel(), userController.loadLandingPage(null).getModel());
		Assert.assertEquals(homeView.getView(), userController.loadLandingPage(null).getView());
	}

	@Test(expected = IOException.class)
	public void loadLandingPageSessionExpiredIOExTest() throws Exception {

		doThrow(IOException.class).when(httpResponse).sendRedirect(any());
		Assert.assertEquals(homeView.getModel(), userController.loadLandingPage(null).getModel());
		Assert.assertEquals(homeView.getView(), userController.loadLandingPage(null).getView());
	}

	@Test
	public void loadErrorPageTestHttpSesionNull() throws Exception {
		errorView = new ModelAndView("error");
		errorView.addObject(PORTALBASEURL, PORTALBASEURL);
		errorView.addObject(OVER_VIEW_PAGE_URL, OVER_VIEW_PAGE_URL);
		errorView.addObject(PortalConstants.MULE_PUBLIC_LAUNCHURL, MULELAUNCHURL);
		when(utils.getPortalBaseURL()).thenReturn(PORTALBASEURL);
		when(utils.getBaseURL()).thenReturn(OVER_VIEW_PAGE_URL);
		when(utils.getMulePublicLaunchUrl()).thenReturn(MULELAUNCHURL);
		when(request.getSession(false)).thenReturn(null);
		Assert.assertEquals(errorView.getModel(), userController.loadErrorPage(samlUserDetails).getModel());
		Assert.assertEquals(errorView.getView(), userController.loadErrorPage(samlUserDetails).getView());
	}

	@Test
	public void loadErrorPageTestOne() throws Exception {
		errorView = new ModelAndView("error");
		session.setAttribute("test", "test");
		errorView.addObject(PORTALBASEURL, PORTALBASEURL);
		errorView.addObject(OVER_VIEW_PAGE_URL, OVER_VIEW_PAGE_URL);
		errorView.addObject(PortalConstants.MULE_PUBLIC_LAUNCHURL, MULELAUNCHURL);
		when(utils.getPortalBaseURL()).thenReturn(PORTALBASEURL);
		when(utils.getBaseURL()).thenReturn(OVER_VIEW_PAGE_URL);
		when(utils.getMulePublicLaunchUrl()).thenReturn(MULELAUNCHURL);
		when(request.getSession(false)).thenReturn(session);
		when((Exception) session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(npexception);

		Assert.assertEquals(errorView.getModel(), userController.loadErrorPage(samlUserDetails).getModel());
		Assert.assertEquals(errorView.getView(), userController.loadErrorPage(samlUserDetails).getView());
	}

	@Test
	public void loadErrorPageTestTwo() throws Exception {

		errorView = new ModelAndView("error");
		errorView.addObject(PORTALBASEURL, PORTALBASEURL);
		errorView.addObject(OVER_VIEW_PAGE_URL, OVER_VIEW_PAGE_URL);
		errorView.addObject(PortalConstants.MULE_PUBLIC_LAUNCHURL, MULELAUNCHURL);
		errorView.addObject(ERRORMESSAGE, "Trouble signing in? Please validate your credentials.");
		errorView.addObject(ERRORCODE, "ALLOWED_LOGIN_ATTEMPTS_EXCEEDED");
		session.setAttribute("test", "test");

		when(utils.getPortalBaseURL()).thenReturn(PORTALBASEURL);
		when(utils.getBaseURL()).thenReturn(OVER_VIEW_PAGE_URL);
		when(utils.getMulePublicLaunchUrl()).thenReturn(MULELAUNCHURL);
		when(request.getSession(false)).thenReturn(session);
		when((Exception) session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(autexception);

		Assert.assertEquals(errorView.getModel(), userController.loadErrorPage(samlUserDetails).getModel());
		Assert.assertEquals(errorView.getView(), userController.loadErrorPage(samlUserDetails).getView());
	}

}
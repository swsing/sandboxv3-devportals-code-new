package com.capgemini.portal.mvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.StreamUtils;

import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.helper.PortalHelper;
import com.capgemini.portal.model.ValidateSessionResponse;
import com.capgemini.portal.security.user.SAMLUserDetails;
import com.capgemini.portal.service.DevPortalService;
import com.capgemini.portal.service.impl.DevPortalServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class FileDownloadControllerTest {

	private MockMvc mockMvc;

	@Value("/testData/Test_Accounts.xlsx")
	private String classpathUrl;

	@Mock
	HttpServletRequest request;
	
	@Mock
	ClassPathResource resource;

	@InjectMocks
	private UserRestController userRestController;
	
	@InjectMocks
	private DevPortalServiceImpl devPortalServiceImpl;

	@Mock
	private SAMLUserDetails samlUserDetails;

	@Mock
	private DevPortalService devPortalService;

	@Mock
	Environment env;

	@Mock
	PortalHelper portalHelper;

	@Mock
	ValidateSessionResponse validateSessionResponse;
	
	private static final String FILE_NAME = "/testData/Test_Accounts.xlsx";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(userRestController).dispatchOptions(true).build();
		mockMvc = MockMvcBuilders.standaloneSetup(devPortalServiceImpl).dispatchOptions(true).build();
		
		
		
	}


	@Test
	public void fileDownloadServiceTest() throws Exception {

		Mockito.spy(SAMLUserDetails.class);
		ReflectionTestUtils.setField(devPortalServiceImpl, "isClassPathResource", Boolean.TRUE);
		ReflectionTestUtils.setField(devPortalServiceImpl, "classpathUrl", FILE_NAME);
		
		assertNotNull(devPortalServiceImpl.downloadTestDataFile(samlUserDetails));
		assertEquals(HttpStatus.OK, devPortalServiceImpl.downloadTestDataFile(samlUserDetails).getStatusCode());
	}
	
		
	@Test(expected = DeveloperPortalException.class)
	public void fileDownloadServiceFailTest() throws Exception {

		Mockito.spy(SAMLUserDetails.class);
		ReflectionTestUtils.setField(devPortalServiceImpl, "isClassPathResource", Boolean.TRUE);
		ReflectionTestUtils.setField(devPortalServiceImpl, "classpathUrl", "InSync4_IrregularVerb.pdf");
		ResponseEntity<Object> object = devPortalServiceImpl.downloadTestDataFile(samlUserDetails);
		assertEquals(HttpStatus.SERVICE_UNAVAILABLE, object.getStatusCode());
	}
	
	@Test
	public void fileDownloadTest() throws Exception {

		Mockito.spy(SAMLUserDetails.class);
		ReflectionTestUtils.setField(userRestController, "classpathUrl", FILE_NAME);

		Resource resource = new ClassPathResource(FILE_NAME);

		when(devPortalService.downloadTestDataFile(any(SAMLUserDetails.class))).thenReturn(
				new ResponseEntity<>(StreamUtils.copyToByteArray(resource.getInputStream()), HttpStatus.OK));

		ResponseEntity<Object> object1 = userRestController.downloadTestDataFile(samlUserDetails);
		assertNotNull(object1);
		assertEquals(HttpStatus.OK, object1.getStatusCode());
	}

	@Test(expected = DeveloperPortalException.class)
	public void fileDownloadTestUserSessionExpired() throws Exception {

		doThrow(DeveloperPortalException.class).when(devPortalService).downloadTestDataFile(null);
		ResponseEntity<Object> object = userRestController.downloadTestDataFile(null);
		assertNotNull(object);
		assertEquals(HttpStatus.UNAUTHORIZED, object.getStatusCode());
	}

	@Test(expected = DeveloperPortalException.class)
	public void fileDownloadFailTest() throws Exception {

		Mockito.spy(SAMLUserDetails.class);
		ReflectionTestUtils.setField(userRestController, "classpathUrl", "InSync4_IrregularVerb.pdf");
		doThrow(DeveloperPortalException.class).when(devPortalService).downloadTestDataFile(samlUserDetails);
		ResponseEntity<Object> object = userRestController.downloadTestDataFile(samlUserDetails);
		assertNotNull(object);
		assertEquals(HttpStatus.SERVICE_UNAVAILABLE, object.getStatusCode());
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		userRestController = null;

	}

}

package com.capgemini.portal.mvc;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;

@RunWith(MockitoJUnitRunner.class)
public class ImplUIStaticControllerTest {

	@InjectMocks
	private ImplUIStaticController implUIStaticController;

	@Mock
	UIStaticContentUtilityController uiController;

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void retrieveUIDataTest() {
		when(uiController.getStaticContentForUI()).thenReturn("Mock url");
		Assert.assertEquals("Mock url", implUIStaticController.retrieveUIData());
	}

	@Test
	public void updateUIDataTest() throws Exception {
		doNothing().when(uiController).updateStaticContentoForUI();
		implUIStaticController.updateUIData();
	}

}

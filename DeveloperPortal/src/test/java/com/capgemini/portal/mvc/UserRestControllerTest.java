package com.capgemini.portal.mvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.servlet.ModelAndView;

import com.capgemini.portal.config.PortalValidationConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.helper.HealthCheckHelper;
import com.capgemini.portal.helper.PortalHelper;
import com.capgemini.portal.model.ApiResponse;
import com.capgemini.portal.model.ChangePasswordRequest;
import com.capgemini.portal.model.ChangePasswordResponse;
import com.capgemini.portal.model.UserProfile;
import com.capgemini.portal.model.ValidateSessionResponse;
import com.capgemini.portal.security.user.SAMLUserDetails;
import com.capgemini.portal.security.utilities.SecurityUtils;
import com.capgemini.portal.service.DevPortalService;

@RunWith(MockitoJUnitRunner.class)
public class UserRestControllerTest {

	@Mock
	private SAMLUserDetails samlUserDetails;

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse httpResponse;

	@Mock
	private HttpSession httpSession;

	@InjectMocks
	private UserRestController userRestController;

	@Mock
	private DevPortalService devPortalService;

	@Mock
	private ModelAndView homeView;

	@Mock
	private ModelAndView changePasswordView;

	@Mock
	private ModelAndView errorView;

	@Mock
	private HttpSession session;

	@Mock
	SecurityUtils utils;

	@Mock
	Exception ex;

	@Mock
	PortalHelper portalHelper;

	@Mock
	HealthCheckHelper healthCheckHelper;

	@Mock
	ChangePasswordRequest changePasswordRequest;

	@Mock
	private AuthenticationException autexception;

	@Mock
	private NullPointerException npexception;

	@Mock
	ChangePasswordResponse changePasswordResponse;

	@Mock
	ValidateSessionResponse validateSessionResponse;

	@Mock
	Environment env;

	@Mock
	DeveloperPortalException developerPortalException;

	@Mock
	PortalValidationConfig portalApplicationConfig;

	public static final String LOGOUTURL = "logoutURL";
	public static final String MULELAUNCHURL = "muleLaunchURL";
	public static final String SAMLATTRIBUTE = "samlAttributes";
	public static final String ERRORMESSAGE = "errorMessage";
	public static final String ERRORCODE = "errorCode";

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testHealthCheck() {

		when(userRestController.healthCheck()).thenReturn(new Health.Builder(Status.UP).build());
		Assert.assertEquals(Status.UP, userRestController.healthCheck().getStatus());
	}

	@Test
	public void testHealthCheckDown() {

		when(healthCheckHelper.health()).thenReturn(new Health.Builder(Status.DOWN).build());
		Assert.assertEquals(Status.DOWN, userRestController.healthCheck().getStatus());
	}

	@Test(expected = DeveloperPortalException.class)
	public void testValidateSessionWithSessionIsExpire() throws Exception {

		doThrow(DeveloperPortalException.class).when(devPortalService).validateSession(any(), any(), any());
		userRestController.validateSession(null, request, httpResponse);

	}

	@Test
	public void testValidateSession() throws Exception {

		validateSessionResponse = new ValidateSessionResponse();
		validateSessionResponse.setStatusCode(HttpStatus.OK.value() + "");
		Mockito.spy(SAMLUserDetails.class);
		when(devPortalService.validateSession(any(SAMLUserDetails.class), any(), any()))
				.thenReturn(validateSessionResponse);
		Assert.assertEquals(validateSessionResponse.getStatusCode(),
				userRestController.validateSession(samlUserDetails, request, httpResponse).getStatusCode());
	}

	@Test
	public void testChangePassword() throws Exception {

		Map<String, String> samlAttributes = new HashMap<String, String>();
		samlAttributes.put("1", "1");
		changePasswordRequest = new ChangePasswordRequest();

		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.OK.value() + "");

		Mockito.spy(SAMLUserDetails.class);
		Mockito.spy(ChangePasswordRequest.class);

		when(devPortalService.changePassword(any(SAMLUserDetails.class), any(ChangePasswordRequest.class)))
				.thenReturn(changePasswordResponse);

		Assert.assertEquals(changePasswordResponse.getStatusCode(),
				userRestController.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());
	}

	@Test(expected = DeveloperPortalException.class)
	public void testChangePasswordUserSessionExpired() throws Exception {

		changePasswordRequest = new ChangePasswordRequest();
		doThrow(DeveloperPortalException.class).when(portalHelper).validateChangePasswordRequest(null, changePasswordRequest);
		userRestController.changePassword(null, changePasswordRequest);
	}

	@Test(expected = DeveloperPortalException.class)
	public void testChangePasswordRequestObjectNull() throws Exception {

		Map<String, String> samlAttributes = new HashMap<String, String>();
		samlAttributes.put("1", "1");
		
		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.BAD_REQUEST.value() + "");

		Mockito.spy(SAMLUserDetails.class);

		doThrow(DeveloperPortalException.class).when(portalHelper).validateChangePasswordRequest(samlUserDetails, null);

		Assert.assertEquals(changePasswordResponse.getStatusCode(),
				userRestController.changePassword(samlUserDetails, null).getStatusCode());

	}

	@Test(expected = DeveloperPortalException.class)
	public void testChangePasswordNewPWdNull() throws Exception {

		Map<String, String> samlAttributes = new HashMap<String, String>();
		samlAttributes.put("1", "1");

		changePasswordRequest = new ChangePasswordRequest();
		changePasswordRequest.setNewPwd(null);
		changePasswordRequest.setOldPwd("Test@2018");

		Mockito.spy(SAMLUserDetails.class);

		doThrow(DeveloperPortalException.class).when(portalHelper).validateChangePasswordRequest(samlUserDetails,
				changePasswordRequest);
		
		userRestController.changePassword(samlUserDetails, changePasswordRequest).getStatusCode();

	}

	@Test(expected = DeveloperPortalException.class)
	public void testChangePasswordFail() throws Exception {

		Map<String, String> samlAttributes = new HashMap<String, String>();
		samlAttributes.put("1", "1");

		changePasswordRequest = new ChangePasswordRequest();

		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.BAD_REQUEST.value() + "");

		Mockito.spy(SAMLUserDetails.class);

		doThrow(DeveloperPortalException.class).when(devPortalService).changePassword(samlUserDetails,
				changePasswordRequest);

		Assert.assertEquals(changePasswordResponse.getStatusCode(),
				userRestController.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());

	}

	@Test(expected = DeveloperPortalException.class)
	public void testChangePasswordIncorrectCurrPwd() throws Exception {

		changePasswordRequest = new ChangePasswordRequest();

		Mockito.spy(SAMLUserDetails.class);

		doThrow(DeveloperPortalException.class).when(devPortalService).changePassword(samlUserDetails,
				changePasswordRequest);

		Assert.assertEquals(HttpStatus.BAD_REQUEST,
				userRestController.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());

	}

	@Test
	public void testChangePasswordUpdateDevUserAttr() throws Exception {

		changePasswordRequest = new ChangePasswordRequest();
		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.OK.value() + "");

		Mockito.spy(SAMLUserDetails.class);

		when(devPortalService.changePassword(any(SAMLUserDetails.class), any(ChangePasswordRequest.class)))
				.thenReturn(changePasswordResponse);

		Assert.assertEquals(changePasswordResponse.getStatusCode(),
				userRestController.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());

	}

	@Test(expected = DeveloperPortalException.class)
	public void testChangePwdUpdateDevUserAttrWithOldPwd() throws Exception {

		changePasswordRequest = new ChangePasswordRequest();
		changePasswordResponse = new ChangePasswordResponse();
		changePasswordResponse.setStatusCode(HttpStatus.BAD_REQUEST.value() + "");

		Mockito.spy(SAMLUserDetails.class);

		doThrow(DeveloperPortalException.class).when(devPortalService).changePassword(samlUserDetails,
				changePasswordRequest);

		Assert.assertEquals(HttpStatus.BAD_REQUEST,
				userRestController.changePassword(samlUserDetails, changePasswordRequest).getStatusCode());

	}

	@Test
	public void testChangePassworSet() throws Exception {
		changePasswordRequest = new ChangePasswordRequest();
		changePasswordRequest.setOldPwd("test");
		Assert.assertEquals("test", changePasswordRequest.getOldPwd());

	}

	@Test
	public void testChangePassworNewSet() throws Exception {
		changePasswordRequest = new ChangePasswordRequest();
		changePasswordRequest.setNewPwd("test");
		Assert.assertEquals("test", changePasswordRequest.getNewPwd());

	}

	@Test(expected = DeveloperPortalException.class)
	public void populateDeveloperPortalExceptionTest() throws Exception {
		throw DeveloperPortalException.populateDeveloperPortalException("test", PortalErrorCodeEnum.TECHNICAL_ERROR);
	}
	
	
	@Test
	public void getUserProfileTest() throws Exception {
		
		Mockito.spy(SAMLUserDetails.class);
		String emailId = "suhas.gajakosh@capgemini.com";
		UserProfile userProfile = new UserProfile();
		userProfile.setFirstName("Suhas");
		userProfile.setLastName("Gajakosh");
		userProfile.setUid(emailId);

		when(devPortalService.fetchUserProfile(any(SAMLUserDetails.class)))
				.thenReturn(userProfile);
		
		UserProfile profile = userRestController.getUserProfile(samlUserDetails, emailId);
		
		Assert.assertEquals(emailId, profile.getUid());
		Assert.assertEquals(userProfile.getFirstName(), profile.getFirstName());
	}
	
	@Test
	public void getUserProfileExceptionTest() throws Exception {
		
		Mockito.spy(SAMLUserDetails.class);
		String emailId = "suhas.gajakosh@capgemini.com";
		UserProfile userProfile = new UserProfile();
		userProfile.setFirstName("Suhas");
		userProfile.setLastName("Gajakosh");
		userProfile.setUid(emailId);

		when(devPortalService.fetchUserProfile(any(SAMLUserDetails.class)))
				.thenReturn(userProfile);
		
		userRestController.getUserProfile(samlUserDetails, emailId);		
	}
	@Test
	public void logoutTest() throws Exception{
		userRestController.logout(samlUserDetails);
	}
	@Test
	public void testUpdateUserProfile() throws Exception {

		UserProfile userProfile = new UserProfile();

		ApiResponse apiResponse = new ApiResponse();
		apiResponse.setStatusCode(String.valueOf(HttpStatus.OK.value()));

		Mockito.spy(SAMLUserDetails.class);
		Mockito.spy(UserProfile.class);

		when(devPortalService.updateUserProfile(any(SAMLUserDetails.class), any(UserProfile.class)))
				.thenReturn(apiResponse);
		
		doNothing().when(portalHelper).validateUpdateUserProfileRequest(any(UserProfile.class));

		Assert.assertEquals(apiResponse.getStatusCode(),
				userRestController.updateUserProfile(samlUserDetails, userProfile).getStatusCode());
	}
	

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		userRestController = null;

	}

}
package com.capgemini.portal.validator;

import static org.mockito.Mockito.doThrow;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.capgemini.portal.config.PortalValidationConfig;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.model.ChangePasswordRequest;
import com.capgemini.portal.security.user.SAMLUserDetails;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class PortalValidatorTest {
	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL
	 * __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 *  
	 * NOTICE:  All information contained herein is, and remains
	 * the property of CAPGEMINI GROUP.
	 * The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered
	 * by patents, patents in process, and are protected by trade secret
	 * or copyright law.
	 * Dissemination of this information or reproduction of this material
	 * is strictly forbidden unless prior written permission is obtained
	 * from CAPGEMINI GROUP.
	 ******************************************************************************/
	
	@InjectMocks
	PortalValidator portalValidator;
	
	@Mock
	SAMLUserDetails user;
	
	@Mock
	PortalValidationConfig portalValidationConfig;
	
	@Mock
	ChangePasswordRequest  cpRequest;	
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expected=DeveloperPortalException.class)
	public void validateChangePasswordRequestNullTest() throws Exception{
		portalValidator.validateChangePasswordRequest(null, cpRequest);
	}
	
	@Test(expected=DeveloperPortalException.class)
	public void validateChangePasswordRequestTest() throws Exception{
		portalValidator.validateChangePasswordRequest(user, cpRequest);
	}
	
	@Test(expected=DeveloperPortalException.class)
	public void validateChangePasswordRequestExceptionTest() throws Exception{
		portalValidator.validateChangePasswordRequest(user, null);
	}
	
	@Test(expected=DeveloperPortalException.class)
	public void validateChangePasswordRequestException1Test() throws Exception{
		doThrow(DeveloperPortalException.class).when(cpRequest).getNewPwd();
		portalValidator.validateChangePasswordRequest(user, cpRequest);
	}
	@Test(expected=DeveloperPortalException.class)
	public void validateChangePasswordRequestException2Test() throws Exception{
		doThrow(DeveloperPortalException.class).when(cpRequest).getOldPwd();
		portalValidator.validateChangePasswordRequest(user, cpRequest);
	}
	@Test(expected=DeveloperPortalException.class)
	public void validateChangePasswordRequestException3Test() throws Exception{
		doThrow(DeveloperPortalException.class).when(portalValidationConfig).getChangePwdRegxString();
		portalValidator.validateChangePasswordRequest(user, cpRequest);
	}
	@After
	public void tearDown() throws Exception {
		portalValidator = null;

	}
}

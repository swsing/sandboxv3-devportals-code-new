package com.capgemini.template.builder;


import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.capgemini.mail.config.MailConfig;
import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.template.builder.TemplateBuilder.Template;
import com.capgemini.template.service.ChangePwdSTemplateService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class TemplateBuilderTest {
	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL
	 * __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 *  
	 * NOTICE:  All information contained herein is, and remains
	 * the property of CAPGEMINI GROUP.
	 * The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered
	 * by patents, patents in process, and are protected by trade secret
	 * or copyright law.
	 * Dissemination of this information or reproduction of this material
	 * is strictly forbidden unless prior written permission is obtained
	 * from CAPGEMINI GROUP.
	 ******************************************************************************/
	@InjectMocks
	TemplateBuilder templateBuilder;

	@Mock
	ChangePwdSTemplateService changePwdSTemplateService;

	@Mock
	private PortalLoggerUtils portalLoggerUtils;

	@Mock
	private PortalAspectUtils portalAspectUtils;

	@Mock
	private PortalLoggerAttribute portalLoggerAttribute;

	@Mock
	MailConfig mailConfig;


	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getTemplateTest() throws Exception{
		Template template = null ;
		HashMap<String, String> map = new HashMap<String, String>();
		when(changePwdSTemplateService.enrichEmailTemplate(map, Template.CHANGEPASSWORD.getEmailTemplate())).thenReturn("TEMPLATE");
		templateBuilder.getTemplate(template.CHANGEPASSWORD, map );
	}


	@Test
	public void getTemplate1Test() throws Exception{
		Template template = null ;
		HashMap<String, String> map = new HashMap<String, String>();
		when(changePwdSTemplateService.enrichEmailTemplate(map, Template.REGISTEREMAIL.getEmailTemplate())).thenReturn("TEMPLATE");
		templateBuilder.getTemplate(template.REGISTEREMAIL, map );
	}






	@After
	public void tearDown() throws Exception {
		templateBuilder = null;

	}
}

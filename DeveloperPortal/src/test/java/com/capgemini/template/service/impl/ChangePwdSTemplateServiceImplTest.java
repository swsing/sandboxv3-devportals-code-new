package com.capgemini.template.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.capgemini.mail.config.MailConfig;
import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ChangePwdSTemplateServiceImplTest {

	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 * 
	 * NOTICE: All information contained herein is, and remains the property of
	 * CAPGEMINI GROUP. The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered by patents, patents
	 * in process, and are protected by trade secret or copyright law.
	 * Dissemination of this information or reproduction of this material is
	 * strictly forbidden unless prior written permission is obtained from
	 * CAPGEMINI GROUP.
	 ******************************************************************************/

	@InjectMocks
	ChangePwdSTemplateServiceImpl changePwdSTemplateServiceImpl;

	@Mock
	MailConfig mailConfig;

	@Mock
	private PortalLoggerUtils portalLoggerUtils;

	@Mock
	private PortalAspectUtils portalAspectUtils;

	@Mock
	private PortalLoggerAttribute portalLoggerAttribute;


	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getDynamicFieldsTest() throws Exception {
		changePwdSTemplateServiceImpl.getDynamicFields();

	}

	/* @Test
 public void getTemplatePathTest() throws Exception {
//
 	Path path = new Path() {
//
 		@Override
 		public URI toUri() {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Path toRealPath(LinkOption... options) throws IOException {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public File toFile() {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Path toAbsolutePath() {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Path subpath(int beginIndex, int endIndex) {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public boolean startsWith(String other) {
 			// TODO Auto-generated method stub
 			return false;
 		}
//
 		@Override
 		public boolean startsWith(Path other) {
 			// TODO Auto-generated method stub
 			return false;
 		}
//
 		@Override
 		public Path resolveSibling(String other) {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Path resolveSibling(Path other) {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Path resolve(String other) {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Path resolve(Path other) {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Path relativize(Path other) {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public WatchKey register(WatchService watcher, Kind<?>[] events, Modifier... modifiers) throws IOException {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public WatchKey register(WatchService watcher, Kind<?>... events) throws IOException {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Path normalize() {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Iterator<Path> iterator() {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public boolean isAbsolute() {
 			// TODO Auto-generated method stub
 			return false;
 		}
//
 		@Override
 		public Path getRoot() {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Path getParent() {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public int getNameCount() {
 			// TODO Auto-generated method stub
 			return 0;
 		}
//
 		@Override
 		public Path getName(int index) {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public FileSystem getFileSystem() {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public Path getFileName() {
 			// TODO Auto-generated method stub
 			return null;
 		}
//
 		@Override
 		public boolean endsWith(String other) {
 			// TODO Auto-generated method stub
 			return false;
 		}
//
 		@Override
 		public boolean endsWith(Path other) {
 			// TODO Auto-generated method stub
 			return false;
 		}
//
 		@Override
 		public int compareTo(Path other) {
 			// TODO Auto-generated method stub
 			return 0;
 		}
 	};
 	Resource resource = new ClassPathResource("message-template-change-password-success.html");
//
 	String emailTemplateCode = "message-template-change-password-success.html";
 	String value = "message-template-change-password-success.html";
 	when(mailConfig.getRelativePathChangePwd()).thenReturn("/emailTemplates/message-template-change-password-success.html");
 	when(mailConfig.getIsRelativePath()).thenReturn(Boolean.TRUE);
 	when(mailConfig.getUrlChangePwd()).thenReturn("/emailTemplates/message-template-change-password-success.html");

 	assertNotNull(changePwdSTemplateServiceImpl.getTemplatePath(path.toString()));
//
 }

	 */	
	@Test
	public void getListOfAttributesTest() throws Exception {
		assertNotNull(changePwdSTemplateServiceImpl.getListOfAttributes());
	}

	@Test
	public void replaceAllTest() throws Exception {
		String text = new String();
		Map<String, String> params = new HashMap<>();

		List<String> dyFields = new ArrayList<>();

		changePwdSTemplateServiceImpl.replaceAll(text, params, dyFields);
	}

	@Test
	public void enrichEmailTemplateTest() throws Exception {
		Boolean  isExceptionThrow = Boolean.TRUE;
		Map<String, String> map = new HashMap<>();
		map.put(PortalConstants.FIRSTNAME, "firstName");
		map.put(PortalConstants.CHANGE_PWD_COMPLETE_OPERATION_MESSAGE, mailConfig.getChangedMsg());
		Resource resource = new ClassPathResource("/emailTemplates/message-template-change-password-success.html");
		//
		String emailTemplateCode = "/emailTemplates/message-template-change-password-success.html";
		String value = "/emailTemplates/message-template-change-password-success.html";
		when(mailConfig.getRelativePathChangePwd()).thenReturn("/emailTemplates/message-template-change-password-success.html");
		when(mailConfig.getUrlChangePwd()).thenReturn(" ");
		when(mailConfig.getIsRelativePath()).thenReturn(Boolean.TRUE);

		Stream<String> value1 = value.codePoints().mapToObj(c -> String.valueOf((char) c));
		changePwdSTemplateServiceImpl.enrichEmailTemplate(map, emailTemplateCode);
	}

	@Test(expected=DeveloperPortalException.class)
	public void enrichEmailTemplateExceptionTest() throws Exception {
		Map<String, String> map = new HashMap<>();
		map.put(PortalConstants.FIRSTNAME, "firstName");
		map.put(PortalConstants.CHANGE_PWD_COMPLETE_OPERATION_MESSAGE, mailConfig.getChangedMsg());

		String emailTemplateCode = "/emailTemplates/message-template-change-password-success.html";
		when(mailConfig.getRelativePathChangePwd()).thenReturn("");
		when(mailConfig.getUrlChangePwd()).thenReturn("");
		when(mailConfig.getIsRelativePath()).thenReturn(Boolean.TRUE);
		when(mailConfig.getIsExceptionThrow()).thenReturn(Boolean.TRUE);
		changePwdSTemplateServiceImpl.enrichEmailTemplate(map, emailTemplateCode);
	}

	@Test
	public void replaceAll1Test() throws Exception {
		String text = "DYNAMIC FIELDS";
		Map<String, String> params = new HashMap<>();
		params.put("DYNAMIC", "firstName");
		List<String> dyFields = new ArrayList<>();
		dyFields.add("firstName");
		changePwdSTemplateServiceImpl.replaceAll(text, params, dyFields);

	}
	@Test
	public void replaceAllExpTest() throws Exception {
		String text = "DYNAMIC FIELDS";
		Map<String, String> params = new HashMap<>();
		params.put("DYNAMIC", "firstName");
		List<String> dyFields = new ArrayList<>();
		dyFields.add("");
		changePwdSTemplateServiceImpl.replaceAll(text, params, dyFields);

	}

	@After
	public void tearDown() throws Exception {
		changePwdSTemplateServiceImpl = null;

	}

}

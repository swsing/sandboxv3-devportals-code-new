package com.capgemini.mail.config;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchEvent.Modifier;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class MailConfigTest {
	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL
	 * __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 *  
	 * NOTICE:  All information contained herein is, and remains
	 * the property of CAPGEMINI GROUP.
	 * The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered
	 * by patents, patents in process, and are protected by trade secret
	 * or copyright law.
	 * Dissemination of this information or reproduction of this material
	 * is strictly forbidden unless prior written permission is obtained
	 * from CAPGEMINI GROUP.
	 ******************************************************************************/

	@InjectMocks
	MailConfig mailConfig;

	@Mock
	JavaMailSenderImpl mailSender;


	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
		mailConfig.setSmtpPort("253");
		mailConfig.setTransportProtocol("smtp");
		mailConfig.setSmtpAuth(Boolean.TRUE);
		mailConfig.setSslTrust("email-smtp.us-west-2.amazonaws.com");
		mailConfig.setSmtpHost("email-smtp.us-west-2.amazonaws.com");
		mailConfig.setChangedMsg("changedMsg");
		mailConfig.setChangePwdSuccessMsgSubject("changePwdSuccessMsgSubject");

	}

	@Test
	public void contentTypeTest() throws Exception{
		mailConfig.setContentType("contentType");
		assertTrue(mailConfig.getContentType()=="contentType");
	}

	@Test
	public void smtpAuthTest() throws Exception{
		mailConfig.setSmtpAuth(Boolean.TRUE);
		assertTrue(mailConfig.isSmtpAuth()==Boolean.TRUE);
	}

	@Test
	public void smtpStarttlsEnableTest() throws Exception{
		mailConfig.setSmtpStarttlsEnable(Boolean.TRUE);
		assertTrue(mailConfig.isSmtpStarttlsEnable()==Boolean.TRUE);
	}

	@Test
	public void smtpHostTest() throws Exception{
		mailConfig.setSmtpHost("SMTPHost");
		assertTrue(mailConfig.getSmtpHost()=="SMTPHost");
	}

	@Test
	public void smtpPortTest() throws Exception{
		mailConfig.setSmtpPort("636");
		assertTrue(mailConfig.getSmtpPort()=="636");
	}

	@Test
	public void passwordTest()throws Exception{
		mailConfig.setPassword("PASSWORD");
		assertTrue(mailConfig.getPassword()=="PASSWORD");
	}

	@Test
	public void transportProtocolTest()throws Exception{
		mailConfig.setTransportProtocol("SMTP");
		assertTrue(mailConfig.getTransportProtocol()=="SMTP");
	}

	@Test
	public void emailTemplateResourcesTest()throws Exception{
		String[] emailTemplateResources={"text","html"};
		mailConfig.setEmailTemplateResources(emailTemplateResources);
		assertTrue(mailConfig.getEmailTemplateResources()==emailTemplateResources);
	}

	@Test
	public void resourceTypeTest()throws Exception{
		mailConfig.setResourceType("classpath");
		assertTrue(mailConfig.getResourceType()=="classpath");
	}

	@Test
	public void UrlTest()throws Exception{
		mailConfig.setUrlChangePwd("www.google.com");
		assertTrue(mailConfig.getUrlChangePwd()=="www.google.com");
	}
	@Test
	public void usernameTest()throws Exception{
		mailConfig.setUserName("raksha");
		assertTrue(mailConfig.getUserName()=="raksha");
	}
	@Test
	public void sslTrustTest()throws Exception{
		mailConfig.setSslTrust("email-smtp.us-west-2.amazonaws.com");
		assertTrue(mailConfig.getSslTrust()=="email-smtp.us-west-2.amazonaws.com");
	}

	@Test
	public void senderAddressTest()throws Exception{
		mailConfig.setSenderAddress("no-reply@capgeminiwebservices.com");
		assertTrue(mailConfig.getSenderAddress()=="no-reply@capgeminiwebservices.com");
	}

	@Test
	public void messageSubjectTest()throws Exception{
		mailConfig.setDefaultMessageSubject("ChangePassword");
		assertTrue(mailConfig.getDefaultMessageSubject()=="ChangePassword");
	}

	@Test
	public void changedMsgTest()throws Exception{
		mailConfig.setChangedMsg("changedMsg");
		assertTrue(mailConfig.getChangedMsg()=="changedMsg");
	}

	@Test
	public void changePwdSuccessMsgSubjectTest()throws Exception{
		mailConfig.setChangePwdSuccessMsgSubject("changePwdSuccessMsgSubject");
		assertTrue(mailConfig.getChangePwdSuccessMsgSubject()=="changePwdSuccessMsgSubject");
	}

	@Test
	public void relativePathTest()throws Exception{
		Path relativePath=new Path() {

			@Override
			public URI toUri() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path toRealPath(LinkOption... options) throws IOException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public File toFile() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path toAbsolutePath() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path subpath(int beginIndex, int endIndex) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean startsWith(String other) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean startsWith(Path other) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Path resolveSibling(String other) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path resolveSibling(Path other) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path resolve(String other) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path resolve(Path other) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path relativize(Path other) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public WatchKey register(WatchService watcher, Kind<?>[] events, Modifier... modifiers) throws IOException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public WatchKey register(WatchService watcher, Kind<?>... events) throws IOException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path normalize() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Iterator<Path> iterator() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isAbsolute() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public Path getRoot() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path getParent() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getNameCount() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public Path getName(int index) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public FileSystem getFileSystem() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path getFileName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean endsWith(String other) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean endsWith(Path other) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public int compareTo(Path other) {
				// TODO Auto-generated method stub
				return 0;
			}
		};
		mailConfig.setRelativePathChangePwd("relativePath");
		assertTrue(mailConfig.getRelativePathChangePwd().equalsIgnoreCase("relativePath"));
	}

	@Test
	public void isRelativePathTest() throws Exception{
		mailConfig.setIsRelativePath(Boolean.TRUE);
		assertTrue(mailConfig.getIsRelativePath()==Boolean.TRUE);
	}

	@Test
	public void listOfAttributesTest()throws Exception{
		List listOfAttributes = new ArrayList<>();
		mailConfig.setListOfAttributes(listOfAttributes );
		assertTrue(mailConfig.getListOfAttributes()==listOfAttributes);
	}

	@Test
	public void isExceptionThrowTest() throws Exception{
		mailConfig.setIsExceptionThrow(Boolean.TRUE);
		assertTrue(mailConfig.getIsExceptionThrow()==Boolean.TRUE);
	}

	@Test
	public void isAsynCallTest() throws Exception{
		mailConfig.setIsAsyncCall((Boolean.TRUE));
		assertTrue(mailConfig.getIsAsyncCall()==Boolean.TRUE);
	}

	@Test
	public void javaMailSenderTest(){
		mailConfig.javaMailSender();
	}



	@After
	public void tearDown() throws Exception {
		mailConfig = null;

	}
}

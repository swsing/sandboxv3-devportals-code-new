package com.capgemini.mail.service;

import static org.mockito.Mockito.when;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.capgemini.mail.config.MailConfig;
import com.capgemini.mail.service.impl.MailServiceImpl;
import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class MailServiceImplTest {
	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL
	 * __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 *  
	 * NOTICE:  All information contained herein is, and remains
	 * the property of CAPGEMINI GROUP.
	 * The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered
	 * by patents, patents in process, and are protected by trade secret
	 * or copyright law.
	 * Dissemination of this information or reproduction of this material
	 * is strictly forbidden unless prior written permission is obtained
	 * from CAPGEMINI GROUP.
	 ******************************************************************************/
	@InjectMocks
	MailServiceImpl mailServiceImpl;

	@Mock
	JavaMailSender javaMailSender;

	@Mock
	HttpServletRequest request;

	@Mock
	MailConfig mailConfig;

	@Mock
	PortalLoggerAttribute portalLoggerAttribute;

	@Mock
	private PortalLoggerUtils portalLoggerUtils;

	@Mock
	private PortalAspectUtils portalAspectUtils;

	@Mock
	JavaMailSenderImpl mailSender;

	@Mock
	MimeMessage msg;

	private String uid="raksha";
	private String validateHTML ="validateHTML";

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);

		mailConfig.setSenderAddress("no-reply@capgeminiwebservices.com");
		mailConfig.setChangePwdSuccessMsgSubject("ChangePassword");

	}

	@Test
	public void sendEmailTest() throws AddressException, MessagingException{
		String uid=new String();
		String validateHTML=new String();
		Properties properties = new Properties();
		properties.put("mail.smtp.host", "smtp.example.com");
		properties.put("mail.smtp.port", "25");
		Session session = Session.getDefaultInstance(properties, null);
		MimeMessage message = new MimeMessage(session);
		when(javaMailSender.createMimeMessage()).thenReturn(message);
		when(mailConfig.getSenderAddress()).thenReturn("no-reply@capgeminiwebservices.com");
		mailServiceImpl.sendEmail(uid, validateHTML,"ChangePassword");

	}

	@Test
	public void populateEmailMimeMessageTest() throws AddressException, MessagingException {
		when(javaMailSender.createMimeMessage()).thenReturn(msg);
		when(mailConfig.getSenderAddress()).thenReturn("no-reply@capgeminiwebservices.com");
		mailServiceImpl.populateEmailMimeMessage(uid, validateHTML,"ChangePassword");
	}
	@Test(expected=DeveloperPortalException.class)
	public void sendEmailExceptionTest(){
		String uid=new String();
		String validateHTML=new String();
		when(mailConfig.getIsExceptionThrow()).thenReturn(Boolean.TRUE);
		when(javaMailSender.createMimeMessage()).thenThrow(DeveloperPortalException.class);
		mailServiceImpl.sendEmail(uid, validateHTML,"ChangePassword");
	}
	@Test
	public void sendEmailExpTest(){
		String uid=new String();
		String validateHTML=new String();
		when(mailConfig.getIsExceptionThrow()).thenReturn(Boolean.FALSE);
		when(javaMailSender.createMimeMessage()).thenThrow(DeveloperPortalException.class);
		mailServiceImpl.sendEmail(uid, validateHTML,"ChangePassword");
	}

	@Test(expected=DeveloperPortalException.class)
	public void populateEmailMimeMessageExceptionTest() throws AddressException, MessagingException {

		when(javaMailSender.createMimeMessage()).thenReturn(msg);
		when(mailConfig.getSenderAddress()).thenReturn(" ");
		when(mailConfig.getIsExceptionThrow()).thenReturn(Boolean.TRUE);
		mailServiceImpl.populateEmailMimeMessage(uid, validateHTML,"ChangePassword");
	}

	@After
	public void tearDown() throws Exception {
		mailServiceImpl = null;

	}
}

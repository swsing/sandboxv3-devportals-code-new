(function(module) {
try {
  module = angular.module('developerHubPartials');
} catch (e) {
  module = angular.module('developerHubPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/change-password.html',
    '<page-header skipToContent="skipToContent" focusContent="focusMainContent()" u-name="userName"></page-header>\n' +
    '<section class="container container-margin" ng-if="isCookieDisabled">\n' +
    '    <error-notice ng-if="errorData" error-data="errorData"></error-notice>\n' +
    '</section>\n' +
    '<section class="container container-margin" ng-if="!isCookieDisabled">\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-6 offset-md-3">\n' +
    '            <h2 class="form-header" ng-bind="changePasswordHeader"></h2>\n' +
    '            <div class="form-container change-password-form">\n' +
    '                <form id="changePasswordForm" name="changePasswordForm" ng-submit="changePasswordFn()" onkeydown="validateFormRequired(this.id, \'keydown\')" autocomplete="off" role="application">\n' +
    '                    <error-notice ng-if="errorData" error-data="errorData"></error-notice>\n' +
    '                    <div class="form-group">\n' +
    '                        <span class="required-sign" aria-hidden="true" ng-bind="astericSignText"></span>\n' +
    '                        <label for="currentPassword" class="devportal-input-label">\n' +
    '                            <span class="sr-only required-sign" ng-bind="astericSignText">&nbsp;</span>\n' +
    '                            <span ng-bind="currentPasswordLabel"></span>\n' +
    '                        </label>\n' +
    '                        <div class="devportal-input-container">\n' +
    '                            <input id="currentPassword" type="password" maxlength="50" name="currentPassword" placeholder={{currentPasswordLabel}} ng-model="passwords.currentPassword" onblur="validateInput(this, \'password\', true, false)" onfocus="focusInput(this)" onkeydown="checkKeyDown(this)"\n' +
    '                                autocomplete="off" />\n' +
    '\n' +
    '                            <span aria-hidden="true" class="err-content" ng-bind="currentPasswordError"></span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <span class="required-sign" aria-hidden="true" ng-bind="astericSignText"></span>\n' +
    '                        <label for="newPassword" class="devportal-input-label">\n' +
    '                            <span class="sr-only required-sign" ng-bind="astericSignText">&nbsp;</span>\n' +
    '                            <span ng-bind="newPasswordLabel"></span>\n' +
    '                        </label>\n' +
    '                        <div class="devportal-input-container">\n' +
    '                            <input id="newPassword" type="password" maxlength="50" name="newPassword" value="" autocorrect="off" autocapitalize="off" placeholder={{newPasswordLabel}} ng-model="passwords.newPassword" onblur="validateInput(this, \'newPassword\', true, true)" onfocus="focusInput(this);"\n' +
    '                                onkeydown="checkKeyDown(this)" autocomplete="off" />\n' +
    '                            <span aria-hidden="true" class="err-content" ng-bind="newPasswordError"></span>\n' +
    '                        </div>\n' +
    '\n' +
    '\n' +
    '                        <p class="rules" ng-bind="passwordRulesLabel"></p>\n' +
    '                        <div class="reset-para">\n' +
    '                            <ul>\n' +
    '                                <li><span ng-bind="passwordRulesLabel1"></span></li>\n' +
    '                                <li><span ng-bind="passwordRulesLabel2"></span></li>\n' +
    '                                <li><span ng-bind="passwordRulesLabel3"></span></li>\n' +
    '                                <li><span ng-bind="passwordRulesLabel4"></span></li>\n' +
    '                            </ul>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <span class="required-sign" aria-hidden="true" ng-bind="astericSignText"></span>\n' +
    '                        <label for="confirmPassword" class="devportal-input-label">\n' +
    '                            <span class="sr-only required-sign" ng-bind="astericSignText">&nbsp;</span>\n' +
    '                            <span ng-bind="confirmPasswordLabel"></span>\n' +
    '                        </label>\n' +
    '                        <div class="devportal-input-container">\n' +
    '                            <input id="confirmPassword" type="password" maxlength="50" name="confirmPassword" value="" autocorrect="off" autocapitalize="off" placeholder={{confirmPasswordLabel}} ng-model="passwords.confirmPassword" onblur="validateInput(this, \'confirmPassword\',true, true)"\n' +
    '                                onfocus="focusInput(this)" onkeydown="checkKeyDown(this)" autocomplete="off" />\n' +
    '                            <span aria-hidden="true" class="err-content" ng-bind="confirmPasswordError"></span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '\n' +
    '                    <div class="row">\n' +
    '                            <div class="col-md-12 col-xs-12 col-lg-12">\n' +
    '                                    <button id="submitButton" class="btn btn-submit float-right" type="submit" ng-bind="submitButtonLabel" disabled></button>\n' +
    '                                    <button id="cancelButton" type="button" role="button" class="btn btn-cancel float-left" ng-bind="cancelButtonLabel" ng-keydown="cancelClicked($event,\'gettingStarted\',\'keydown\', true)" ng-mousedown="cancelClicked($event, \'gettingStarted\', null, true)"></button>\n' +
    '                            </div>\n' +
    '                        \n' +
    '                    </div>\n' +
    '                </form>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '<page-footer></page-footer>');
}]);
})();

(function(module) {
try {
  module = angular.module('developerHubPartials');
} catch (e) {
  module = angular.module('developerHubPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/confirmation-popup.html',
    '<div id="ConfirmationPopup" role="dialog" aria-labelledby="ConfirmationPopupLabel">\n' +
    '    <div class="modal-dialog " role="document">\n' +
    '        <div class="modal-content">\n' +
    '            <div class="modal-header pop-confirm-header">\n' +
    '                <h3 class="modal-title" ng-bind="confirmationPopupHeader" id="ConfirmationPopupLabel"></h3>\n' +
    '            </div>\n' +
    '            <div class="modal-body pop-confirm-body">\n' +
    '                <p class="text-center" ng-bind="confirmationPopupMessage"></p>\n' +
    '            </div>\n' +
    '            <div class="horizontal-rule"></div>\n' +
    '            <div class="modal-footer pop-confirm-footer row">\n' +
    '                <div class="text-center col-md-12">\n' +
    '                    <button class="btn btn-submit btn-block" role="button" ng-click="okBtnClicked(popupAction)" ng-bind="confirmationPopupButtonLabel"></button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('developerHubPartials');
} catch (e) {
  module = angular.module('developerHubPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/dev-portal.html',
    '<!--Info section-->\n' +
    '<page-header skipToContent="skipToContent" focusContent="focusMainContent()" u-name="userName"></page-header>\n' +
    '<section class="intro-section" ng-if="isCookieDisabled">\n' +
    '    <div class="container">\n' +
    '        <error-notice ng-if="errorData" error-data="errorData"></error-notice>\n' +
    '    </div>\n' +
    '</section>\n' +
    '\n' +
    '<section class="intro-section" ng-if="!isCookieDisabled">\n' +
    '    <div class="container">\n' +
    '        <error-notice ng-if="errorData" error-data="errorData"></error-notice>\n' +
    '        <h2 ng-bind="gettingStartedHeader"></h2>\n' +
    '        <div class="row">\n' +
    '            <div class="col-sm-12">\n' +
    '                <h3 ng-bind="apiPortalInfoHeader" class="apiPortalHeader"></h3>\n' +
    '                <div class="info apiPara">\n' +
    '                    <p ng-repeat="idata in apiData" ng-bind-html="idata.para"></p>\n' +
    '                </div>\n' +
    '\n' +
    '                <button ng-disabled="isBlockedApis" id="downloadTestDataBtn" ng-attr-title="{{isBlockedApis?\'Your API access is blocked\':\'\'}}" type="button" role="button" class="btn down-btn" ng-click="downloadTestData()">\n' +
    '                        <span class="txt-label" ng-bind="apiPortalDownloadTestData"></span>\n' +
    '                        <span class="sr-only" ng-attr-aria-label="{{isBlockedApis?\'Your API access is blocked\':apiPortalDownloadScrLabel}}"                         ></span>\n' +
    '                        <span class="fa-stack fa-lg"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-chevron-left fa-rotate-270 fa-stack-1x"></i></span>\n' +
    '                </button>\n' +
    '\n' +
    '                <button ng-disabled="isBlockedApis" id="gotomulebutton" ng-attr-title="{{isBlockedApis?\'Your API access is blocked\':\'\'}}"type="button" role="button" class="btn sandbox-btn" ng-click="homeSessionCheck(\'goToMule\',true)">\n' +
    '                        <span class="txt-label" ng-bind="apiPortalAPISandboxButton"></span>\n' +
    '                        <span class="sr-only" ng-attr-aria-label="{{isBlockedApis?\'Your API access is blocked\':apiPortalAPISandboxScrLabel}}"></span>\n' +
    '                        <span class="fa-stack fa-lg"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-chevron-down fa-rotate-270 fa-stack-1x"></i></span>\n' +
    '                </button>\n' +
    '\n' +
    '                \n' +
    '\n' +
    '            </div>\n' +
    '           <!-- <div class="col-sm-6">\n' +
    '                <h3 ng-bind="apiTPPPortalInfoHeader" class="apiTPPHeader"></h3>\n' +
    '                <div class="info tppPara">\n' +
    '                    <p ng-repeat="idata in tppData" ng-bind-html="idata.para"></p>\n' +
    '                </div>\n' +
    '                <button id="accessTPPBtn" type="button" role="button" class="btn access_btn" ng-click="homeSessionCheck(\'accessTpp\')">\n' +
    '                        <span class="txt-label" ng-bind="apiPortalAccessBoiTpp"></span>\n' +
    '                        <span class="sr-only" aria-label="{{apiPortalAccessBoiLabel}}"></span>\n' +
    '                        <span class="fa-stack fa-lg"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-chevron-down fa-rotate-270 fa-stack-1x"></i></span>\n' +
    '                </button>\n' +
    '            </div>-->\n' +
    '\n' +
    '\n' +
    '        </div>\n' +
    '</section>\n' +
    '<!--Info section ends-->\n' +
    '<!--Test section-->\n' +
    '<section class="test-details" ng-if="!isCookieDisabled">\n' +
    '    <div class="container">\n' +
    '        <h3 ng-bind="testingAppHeader"></h3>\n' +
    '        <div class="row">\n' +
    '            <div class="col-sm-4">\n' +
    '\n' +
    '                <p class="testing-steps">\n' +
    '                    <span class="fa-stack fa-2x">\n' +
    '                              <i class="fa  fa-stack-1x"></i>\n' +
    '                              <i class="fa fa fa-eye"></i>\n' +
    '                        </span>\n' +
    '                </p>\n' +
    '                <h4 ng-bind="testingAppAPICatalogue"></h4>\n' +
    '                <p ng-bind="testingAppAPICatalogueDetails"></p>\n' +
    '            </div>\n' +
    '            <div class="col-sm-4">\n' +
    '                <p class="testing-steps" href="javascript:void(0);">\n' +
    '                    <span class="fa-stack fa-2x">\n' +
    '                                      <i class="fa  fa-stack-1x"></i>\n' +
    '                                      <i class="fa fa fa-address-card "></i>\n' +
    '                                </span>\n' +
    '                </p>\n' +
    '                <h4 ng-bind="testingAppAPIRequest"></h4>\n' +
    '                <p ng-bind="testingAppAPIRequestDetail"></p>\n' +
    '            </div>\n' +
    '            <div class="col-sm-4">\n' +
    '                <p class="testing-steps" href="javascript:void(0);">\n' +
    '                    <span class="fa-stack fa-2x">\n' +
    '                                      <i class="fa  fa-stack-1x"></i>\n' +
    '                                      <i class="fa fa fa-download "></i>\n' +
    '                                </span>\n' +
    '                </p>\n' +
    '                <h4 ng-bind="testingAppDownloadData"></h4>\n' +
    '                <p ng-bind="testingAppDownloadDataDetails"></p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <hr/>\n' +
    '        <h3 ng-bind="summaryAPIHeader"></h3>\n' +
    '        <div class="row">\n' +
    '            <div class="col-sm-4">\n' +
    '                <p class="testing-steps" href="javascript:void(0);">\n' +
    '                    <span class="fa-stack fa-2x">\n' +
    '                                      <i class="fa  fa-stack-1x"></i>\n' +
    '                                      <i class="fa fa fa-university "></i>\n' +
    '                                </span>\n' +
    '                </p>\n' +
    '                <h4 ng-bind="summaryAPIProvide"></h4>\n' +
    '                <p class="bank-link" ng-bind-html="summaryAPIProvideDetails">\n' +
    '                </p>\n' +
    '            </div>\n' +
    '            <div class="col-sm-4">\n' +
    '                <p class="testing-steps" href="javascript:void(0);">\n' +
    '                    <span class="fa-stack fa-2x">\n' +
    '                                      <i class="fa  fa-stack-1x"></i>\n' +
    '                                      <i class="fa fa fa-money "></i>\n' +
    '                                </span>\n' +
    '                </p>\n' +
    '                <h4 ng-bind="summaryAPIMoneyMovement"></h4>\n' +
    '                <p ng-bind="summaryAPIMoneyMovementDetails">\n' +
    '                </p>\n' +
    '            </div>\n' +
    '            <div class="col-sm-4">\n' +
    '                <p class="testing-steps" href="javascript:void(0);">\n' +
    '                    <span class="fa-stack fa-2x">\n' +
    '                                      <i class="fa  fa-stack-1x"></i>\n' +
    '                                      <i class="fa fa fa-calendar "></i>\n' +
    '                                </span>\n' +
    '                </p>\n' +
    '                <h4 ng-bind="summaryAPIAccount"></h4>\n' +
    '                <p ng-bind="summaryAPIAccountDetails">\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '<page-footer></page-footer>\n' +
    '<!--Test section ends-->');
}]);
})();

(function(module) {
try {
  module = angular.module('developerHubPartials');
} catch (e) {
  module = angular.module('developerHubPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/error-notice.html',
    '<div class="alert portals-alert portals-alert-danger col-xs-12 col-sm-12 col-md-12" role="alert" aria-atomic="true">\n' +
    '    <p>\n' +
    '        <span role="alert" class="alert-prefix" ng-bind="errorPrefix"></span>\n' +
    '        <span role="alert" class="alert-suffix" ng-bind="errorMsgText"></span>\n' +
    '    </p>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('developerHubPartials');
} catch (e) {
  module = angular.module('developerHubPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/loading-spinner.html',
    '<div class="block-ui-overlay"></div>\n' +
    '<div class="block-ui-message-container" aria-live="assertive" aria-atomic="true">\n' +
    '    <div class="block-ui-message" ng-class="$_blockUiMessageClass">\n' +
    '        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('developerHubPartials');
} catch (e) {
  module = angular.module('developerHubPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/page-footer.html',
    '<footer>\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="col-xs-12 col-md-12">\n' +
    '                <ul class="footer-links">\n' +
    '                    <li>\n' +
    '                        <a href="javascript:void(0)" ng-click="footerSessionCheck($event,\'privacyPolicy\')"  ng-mousedown="footerSessionCheck($event, \'privacyPolicy\')">\n' +
    '                            <span ng-bind="privacyPolicy" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                        <span class="pipe">|</span>\n' +
    '                    </li>\n' +
    '\n' +
    '                    <li>\n' +
    '                        <a href="javascript:void(0)" ng-click="footerSessionCheck($event,\'tnc\')" ng-mousedown="footerSessionCheck($event, \'tnc\')">\n' +
    '                            <span ng-bind="tnc" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                        <span class="pipe">|</span></li>\n' +
    '                        \n' +
    '                    <li>\n' +
    '                        \n' +
    '                        <a href="javascript:void(0)" ng-click="footerSessionCheck($event,\'help\')" ng-mousedown="footerSessionCheck($event, \'help\')">\n' +
    '                            <span ng-bind="help" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '        <p class="regulatory-statement" ng-bind-html="regulatoryLabel">\n' +
    '        </p>\n' +
    '\n' +
    '    </div>\n' +
    '</footer>');
}]);
})();

(function(module) {
try {
  module = angular.module('developerHubPartials');
} catch (e) {
  module = angular.module('developerHubPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/page-header.html',
    '<header>\n' +
    '    <div class="header-container">\n' +
    '        <div class="container ">\n' +
    '            <div class="brand-details">\n' +
    '                <a class="skip-main" href="#" ng-click="skipToContentFocus($event)">Skip to main content</a>\n' +
    '                <img class="navbar-brand float-left" ng-src={{logoPath}} alt={{logoAlternateLabel}} title={{logoAlternateLabel}} />\n' +
    '                <h1 class="portal-details" ng-bind="developerHubHeader"></h1>\n' +
    '                            </div>\n' +
    '        </div>\n' +
    '        <!--Navigation-->\n' +
    '        <nav class="navbar">\n' +
    '            <div class="container">\n' +
    '                <ul class="nav navbar-nav nav-menu navbar-expand-sm ml-auto">\n' +
    '                    <li ng-class="{active: activePath==\'/\'}">\n' +
    '                        <a  href="javascript:void(0)" ng-class="{active: activePath==\'/\'}" ng-click="headerSessionCheck($event,\'gettingStarted\',false)">\n' +
    '                            <span ng-bind="gettingStarted" title="{{headerTt}}"></span>\n' +
    '                             <span class="sr-only" aria-label="{{headerTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li ng-if="username.length>0" >\n' +
    '                        <a href="javascript:void(0)" ng-click="headerSessionCheck($event,\'goToMule\',true)">\n' +
    '                            <span ng-bind="apiDoc" title="{{helpTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{helpTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li ng-if="username==null || username.length==0">\n' +
    '                            <a href="javascript:void(0)" ng-click="mulePublicURL($event)">\n' +
    '                                <span ng-bind="apiDoc" title="{{helpTt}}"></span>\n' +
    '                                <span class="sr-only" aria-label="{{helpTt}}"></span>\n' +
    '                            </a>\n' +
    '                        </li>\n' +
    '                    <li >\n' +
    '                        <a  href="javascript:void(0)" ng-click="headerSessionCheck($event,\'faq\',false)" ng-mousedown="footerSessionCheck($event, \'faq\')">\n' +
    '                            <span ng-bind="faq" title="{{helpTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{helpTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li >\n' +
    '                        <a  href="javascript:void(0)" ng-click="headerSessionCheck($event,\'help\',false)" ng-mousedown="footerSessionCheck($event, \'help\')">\n' +
    '                            <span ng-bind="helpNav" title="{{helpTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{helpTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li class="dropdown portal-user" ng-if="usertype === portalUserType">\n' +
    '                        <a\n' +
    '                        aria-haspopup="true"\n' +
    '                        aria-expanded="false" href="javascript:void(0)">\n' +
    '                            <i class="fa fa-user-circle-o"></i>\n' +
    '                            <span class="caret-container">\n' +
    '                           <span ng-bind="welcome"></span>\n' +
    '                            <span ng-bind="username" class="text-capitalize"></span>\n' +
    '                            </span>\n' +
    '                            <span class="caret"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '\n' +
    '                    <li class="dropdown" ng-if="username.length>0" data-toggle="dropdown" ng-class="{active: activePath==\'/change-password\' ||  activePath==\'/view-profile\'}" ng-if="usertype !== portalUserType">\n' +
    '                        <a class="dropdown-toggle" id="dropdownId"\n' +
    '                        aria-haspopup="true"\n' +
    '                        aria-expanded="false"  href="javascript:void(0)" ng-class="{active: activePath==\'/change-password\' ||  activePath==\'/view-profile\'}" data-toggle="dropdown">\n' +
    '                            <i class="fa fa-user-circle-o"></i>\n' +
    '                            <span class="caret-container">\n' +
    '                            <span ng-bind="welcome" title="{{welcomeHeaderTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{welcomeHeaderTt}}"></span>\n' +
    '                            <span ng-bind="username" class="text-capitalize"></span>\n' +
    '                            </span>\n' +
    '                            <!-- <span class="caret"></span> -->\n' +
    '                        </a>\n' +
    '                        <ul  class="dropdown-menu">\n' +
    '                            <li class="dropdown-submenu">\n' +
    '                                <a class="dropdown-item" href="javascript:void(0)" ng-click="headerSessionCheck($event,\'changePassword\',true)">\n' +
    '                                    <span ng-bind="changePassword" title="{{headerTt}}"></span>\n' +
    '                                    <span class="sr-only" aria-label="{{headerTt}}"></span>\n' +
    '                                </a>\n' +
    '                            </li>\n' +
    '                            <li  class="dropdown-submenu">\n' +
    '                                <a class="dropdown-item" href="javascript:void(0)" ng-click="headerSessionCheck($event,\'viewProfile\',true)">\n' +
    '                                    <span title="{{headerTt}}" ng-bind="viewProfile"></span>\n' +
    '                                    <span class="sr-only" aria-label="{{headerTt}}"></span>\n' +
    '                                </a>\n' +
    '                            </li>\n' +
    '                        </ul>\n' +
    '                    </li>\n' +
    '\n' +
    '\n' +
    '<!--\n' +
    '                    <li class="nav-item dropdown">\n' +
    '                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">\n' +
    '                          Dropdown link\n' +
    '                        </a>\n' +
    '                        <div class="dropdown-menu">\n' +
    '                          <a class="dropdown-item" href="#">Link 1</a>\n' +
    '                          <a class="dropdown-item" href="#">Link 2</a>\n' +
    '                          <a class="dropdown-item" href="#">Link 3</a>\n' +
    '                        </div>\n' +
    '                      </li> -->\n' +
    '\n' +
    '                    <li ng-if="username==null || username.length==0">\n' +
    '                        <a href="javascript:void(0)" ng-click="portalBaseURL($event)">\n' +
    '                            <span ng-bind="login" title="{{headerTt}}"></span>\n' +
    '                             <span class="sr-only" aria-label="{{headerTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '\n' +
    '                    <li ng-if="username.length>0">\n' +
    '                        <a href="javascript:void(0)" ng-click="headerSessionCheck($event, \'logout\',true)">\n' +
    '                            <span ng-bind="logout" title="{{headerTt}}"></span>\n' +
    '                             <span class="sr-only" aria-label="{{headerTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </nav>\n' +
    '\n' +
    '\n' +
    '\n' +
    '\n' +
    '        <!--Navigation Ends-->\n' +
    '    </div>\n' +
    '</header>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('developerHubPartials');
} catch (e) {
  module = angular.module('developerHubPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/session-out-popup.html',
    '<div id="SessionOutPopup" role="dialog" aria-labelledby="SessionOutPopupLabel">\n' +
    '    <div class="modal-dialog " role="document">\n' +
    '        <div class="modal-content">\n' +
    '            <div class="modal-header-session pop-confirm-header">\n' +
    '                <h2 class="modal-title" ng-bind="sessionPopupHeaderLabel" id="SessionOutPopupLabel"></h2>\n' +
    '            </div>\n' +
    '            <div class="modal-body pop-confirm-body">\n' +
    '                <p class="text-center" ng-bind="sessionDescText1"> </p>\n' +
    '                <p class="text-center" ng-bind="sessionDescText2"> </p>\n' +
    '            </div>\n' +
    '            <div class="horizontal-rule"></div>\n' +
    '            <div class="modal-footer pop-confirm-footer row">\n' +
    '                <div class="text-center col-md-12">\n' +
    '                    <button role="button" class="btn btn-session btn-block" ng-click="okBtnClicked()" ng-bind="sessionPopupBtnText">  </button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('developerHubPartials');
} catch (e) {
  module = angular.module('developerHubPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('views/view-profile.html',
    '<page-header skipToContent="skipToContent" focusContent="focusMainContent($event)" u-name="userName"></page-header>\n' +
    '<section class="container container-margin">\n' +
    '      <error-notice ng-if="pageErrorData" error-data="pageErrorData"></error-notice>\n' +
    '</section>\n' +
    '<section class="container container-margin" >\n' +
    '    <div class="row">\n' +
    '      <div class="col-md-6 offset-md-3"  ng-hide="pageErrorData" >\n' +
    '       <div class="row margin-zero ">\n' +
    '           <div class="col-md-11 padd-o ">\n' +
    '            <h2 class="form-header" ng-bind="viewProfileHeader" ng-if="!editForm"> </h2>\n' +
    '            <h2 class="form-header" ng-if="editForm" ng-bind="editProfileHeader"></h2></div>\n' +
    '            <div class="col-md-1 padd-o">\n' +
    '            <a class="editBtn pull-right" id="editButton" href="#" ng-click="editProfile($event,true)" ng-if="!editForm" title="{{editTitle}}" >\n' +
    '                    <span ng-bind="editBtnLabel"></span>   \n' +
    '                <span class="sr-only" aria-label="{{editTitle}}"></span>\n' +
    '                   \n' +
    '            </a></div>\n' +
    '        </div>\n' +
    '            <div class="row view-container" ng-if="!editForm">\n' +
    '                <form id="viewProfileForm" name="viewProfileForm" ng-submit="editFormSubmit">\n' +
    '                     <error-notice ng-if="errorData" error-data="errorData"></error-notice> \n' +
    '                      <table class="borderless view-profile-table">\n' +
    '                          <caption class="sr-only" ng-bind="viewProfileHeader"> </caption>\n' +
    '                  <tbody>\n' +
    '                   <tr> \n' +
    '                        <td>\n' +
    '                                <span ng-bind="firstNameLabel"></span>\n' +
    '                                <span ng-bind="colonlabel"></span>\n' +
    '                        </td>\n' +
    '                        <td ng-bind="viewProfileData.firstName"> \n' +
    '                        </td>\n' +
    '                   </tr>\n' +
    '                    <tr>\n' +
    '                        <td>\n' +
    '                                <span ng-bind="lastNameLabel"></span>\n' +
    '                                <span ng-bind="colonlabel"></span>\n' +
    '                        </td>\n' +
    '                        <td ng-bind="viewProfileData.lastName"> \n' +
    '                        </td>\n' +
    '                   </tr>\n' +
    '                    <tr>\n' +
    '                         <td> \n' +
    '                                <span ng-bind="emailIDLabel"></span>\n' +
    '                                <span ng-bind="colonlabel"></span>\n' +
    '                        </td>\n' +
    '                        <td ng-bind="viewProfileData.emailId"> \n' +
    '                        </td>\n' +
    '                   </tr>\n' +
    '                    <!-- <tr>\n' +
    '                       <th ng-bind="contactNoLabel">:\n' +
    '                        </th>\n' +
    '                        <td ng-bind="viewProfileData.contactNo">\n' +
    '                        </td>\n' +
    '                   </tr>\n' +
    '                    <tr>\n' +
    '                         <th ng-bind="countryLabel">:\n' +
    '                        </th>\n' +
    '                        <td ng-bind="viewProfileData.countryLocation">\n' +
    '                        </td>\n' +
    '                      </tr>  -->\n' +
    '                      <tr>\n' +
    '                         <td>\n' +
    '                                <span ng-bind="organizationLabel"></span>\n' +
    '                                <span ng-bind="colonlabel"></span>\n' +
    '                        </td>\n' +
    '                        <td ng-bind="viewProfileData.organization">\n' +
    '                        </td>\n' +
    '                      </tr>\n' +
    '                      </tbody>\n' +
    '                   </table> \n' +
    '                </form>\n' +
    '            </div>\n' +
    '        \n' +
    '    <div class="form-container" ng-show="editForm">\n' +
    '            <form id="editProfileForm" name="editProfileForm" ng-submit="editFormSubmit(\'editProfileForm\', \'keydown\')" onkeydown="validateFormRequired(this.id, \'keydown\')" autocomplete="off"  role="application">\n' +
    '             <error-notice ng-if="errorData" error-data="errorData"></error-notice> \n' +
    '        <div class="row form-group">\n' +
    '         <div class="col-md-5 col-lg-5 col-xs-5 formedit"><span class="required-sign" aria-hidden="true" ng-bind="astericSignText"></span>\n' +
    '                <label for="firstName" class="devportal-input-label">\n' +
    '                    <span class="sr-only required-sign" ng-bind="astericSignText">&nbsp;</span>\n' +
    '                    <span ng-bind="firstNameLabel"></span>\n' +
    '                    <span ng-bind="colonlabel"></span>\n' +
    '                </label>\n' +
    '            </div>\n' +
    '                <div class="col-md-7 col-lg-7 col-xs-7 userProfile devportal-input-container">\n' +
    '                    <input id="firstName" type="text" maxlength="50" value="viewProfileData.firstName" name="firstName" placeholder={{firstNameLabel}} ng-model="viewProfileData.firstName" \n' +
    '                    onfocus="focusInput(this)" onkeydown="checkKeyDown(this)" onblur="validateInput(this, \'firstName\', true, true)" autocomplete="off" />\n' +
    '                    <span aria-hidden="true" class="err-content" ng-bind="firstNameError"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row form-group">\n' +
    '                <div class="col-md-5 col-lg-5 col-xs-5 formedit">   <span class="required-sign" aria-hidden="true" ng-bind="astericSignText"></span>\n' +
    '                     <label for="lastName" class="devportal-input-label">\n' +
    '                    <span class="sr-only required-sign" ng-bind="astericSignText" >&nbsp;</span>\n' +
    '                    <span ng-bind="lastNameLabel"></span>\n' +
    '                    <span ng-bind="colonlabel"></span>\n' +
    '                </label>\n' +
    '                </div>\n' +
    '                <div class="col-md-7 col-lg-7 col-xs-7 userProfile devportal-input-container">\n' +
    '                    <input id="lastName" type="text" maxlength="50" name="lastName" value="viewProfileData.lastName" autocorrect="off" autocapitalize="off" placeholder={{lastNameLabel}} ng-model="viewProfileData.lastName" \n' +
    '                    onfocus="focusInput(this)" onkeydown="checkKeyDown(this)" onblur="validateInput(this, \'lastName\', true, true)" autocomplete="off"/>\n' +
    '                    <span aria-hidden="true" class="err-content" ng-bind="lastNameError" ></span>\n' +
    '                                    </div> \n' +
    '            </div>\n' +
    '            <div class=" row form-group">\n' +
    '                <div class="col-md-5 col-lg-5 col-xs-5 formedit devportal-input-label"> \n' +
    '                        <label class="devportal-input-label">\n' +
    '                                <!-- <span >&nbsp;</span> -->\n' +
    '                                <span ng-bind="emailIDLabel"></span>\n' +
    '                                <span ng-bind="colonlabel"></span>\n' +
    '                            </label>\n' +
    '                              \n' +
    '            </div>\n' +
    '                <div class="userProfile col-md-7 col-lg-7 col-xs-7 devportal-input-container">\n' +
    '                    <span  ng-bind="viewProfileData.emailId"></span>\n' +
    '                    <span aria-hidden="true" class="err-content" ng-bind="emailIDError"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '<!-- <div class="row form-group">\n' +
    '  <div class="col-md-5">\n' +
    '        <span class="required-sign" aria-hidden="true" ng-bind="astericSignText"></span>\n' +
    '        <label for="contactNo" class="devportal-input-label">\n' +
    '        <span class="sr-only required-sign" ng-bind="astericSignText">&nbsp;</span>\n' +
    '        <span ng-bind="contactNoLabel">:</span>\n' +
    '        </label>\n' +
    '    </div>\n' +
    '    <div class="userProfile col-md-7 devportal-input-container">\n' +
    '    <input type="text" maxlength="50" name="contactNo" value="viewProfileData.contactNo" autocorrect="off" autocapitalize="off" placeholder={{contactNoLabel}} ng-model="viewProfileData.contactNo" \n' +
    '     autocomplete="off" />\n' +
    '   </div>\n' +
    '</div>\n' +
    '        <div class="form-group row">\n' +
    '               <div class="col-md-5">\n' +
    '                <span class="required-sign" aria-hidden="true" ng-bind="astericSignText"></span>\n' +
    '                <label for="countryLocation" class="devportal-input-label">\n' +
    '                    <span class="sr-only required-sign" ng-bind="astericSignText">&nbsp;</span>\n' +
    '                    <span ng-bind="countryLabel"></span>\n' +
    '                </label>\n' +
    '                </div>\n' +
    '                <div class="userProfile col-md-7 devportal-input-container">\n' +
    '                    <input  type="text" maxlength="50" name="countryLocation" value="viewProfileData.countryLocation" autocorrect="off" autocapitalize="off" placeholder={{countryLabel}} ng-model="viewProfileData.countryLocation"\n' +
    '                    onblur="validateInput(this, \'inputName\', true, true)" onfocus="focusInput(this)" onkeydown="checkKeyDown(this)"  autocomplete="off" />\n' +
    '                                   </div>\n' +
    '            </div> -->\n' +
    '            <div class="form-group row">\n' +
    '             <div class="col-md-5 col-lg-5 col-xs-5 formedit">\n' +
    '                    <label for="organization" class="devportal-input-label org-label">\n' +
    '                    <span ng-bind="organizationLabel"></span>\n' +
    '                    <span ng-bind="colonlabel"></span>\n' +
    '					</label>\n' +
    '					<span class="help-holder">\n' +
    '							<a aria-label="{{organizationScrTooltip}}"  class="orgTt" href="javascript:void(0)"\n' +
    '							popover-trigger="\'focus mouseenter\'"  popover-placement="{{placement.selected}}" popover-class="tooltip-content"\n' +
    '										uib-popover="The legal name of the organisation you work for.">\n' +
    '										<i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '							</a>\n' +
    '					</span>\n' +
    '                </div>\n' +
    '                <div class="col-md-7 col-lg-7 col-xs-7 userProfile devportal-input-container">\n' +
    '                    <input id="organization"type="text" maxlength="50" name="organization" value="viewProfileData.organization" autocorrect="off" autocapitalize="off" placeholder={{organizationLabel}} ng-model="viewProfileData.organization" \n' +
    '                    onfocus="focusInput(this)" onkeydown="checkKeyDown(this)" onblur="validateInput(this, \'organization\', false, true)"    autocomplete="off" />\n' +
    '                    <span aria-hidden="true" class="err-content" ng-bind="organizationError" ></span>  </div>\n' +
    '            </div>\n' +
    '\n' +
    '            <div class="row btn-section">\n' +
    '                <div class="col-md-12">\n' +
    '                        <button id="submitButton" class="btn btn-submit float-right" type="submit" value="submit"ng-bind="submitButtonLabel"disabled >Submit</button>\n' +
    '                    <button id="cancelButton" type="button" role="button" class="btn btn-cancel float-left" ng-bind="cancelButtonLabel" value="cancel" ng-keydown="cancelClicked($event,\'viewProfile\',\'keydown\',false)" ng-mousedown="cancelClicked($event, \'viewProfile\', null)">Cancel</button>\n' +
    '                </div>\n' +
    '                \n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '</div>\n' +
    '</section>\n' +
    '<page-footer></page-footer>');
}]);
})();

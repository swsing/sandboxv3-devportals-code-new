// public/js/app.js
"use strict";
var developerHub = angular.module("developerHub", ["ui.router", "ui.bootstrap", "ngSanitize", "pascalprecht.translate", "blockUI" , "developerHubPartials"]);

developerHub.run(["$rootScope", "config", "$state", "$location", function($rootScope, config, $state, $location) {
    $rootScope.activePath = null;
    $rootScope.$on("$stateChangeSuccess", function() {
        $rootScope.activePath = $location.path();
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}]);

developerHub.config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$translateProvider", "$qProvider", "$translatePartialLoaderProvider", "config", "blockUIConfig",
    function($stateProvider, $urlRouterProvider, $httpProvider, $translateProvider, $qProvider, $translatePartialLoaderProvider, config, blockUIConfig) {
        $translatePartialLoaderProvider.addPart("index");
        var assetsPath = document.querySelector("#cdnBaseURL").value;
        $translateProvider.useLoader("$translatePartialLoader", {
            urlTemplate: assetsPath + "/devportal/localization/locale-{lang}.json"
        }).preferredLanguage("en");
        $translateProvider.fallbackLanguage("en");

        // $translatePartialLoaderProvider.addPart("index");
        //   $translateProvider.useUrlLoader("./ui/staticContentGet");
        //   $translateProvider.preferredLanguage("en");
        //   $translateProvider.fallbackLanguage("en");

        blockUIConfig.templateUrl = "views/loading-spinner.html";
        $qProvider.errorOnUnhandledRejections(false);
        $httpProvider.interceptors.push(["$q", "blockUI", "$timeout", function($q, blockUI, $timeout) {
            return {
                request: function(config) {
                    blockUI.start();
                    return config;
                },
                requestError: function(rejection) {
                    blockUI.stop();
                    return $q.reject(rejection);
                },
                response: function(response) {
                    blockUI.stop();
                    return response;
                },
                responseError: function(rejection) {
                    blockUI.stop();
                    return $q.reject(rejection);
                }
            };
        }]);


        $stateProvider.
        state("developerHub", {
            "url": "/",
            templateUrl: "views/dev-portal.html",
            controller: "HomeCtrl",
            resolve: {
                fontResolved: ["fontloaderService", function(fontloaderService) {
                    return fontloaderService.isFontLoad();
                }]
            }
        }).
        state("changePassword", {
            "url": "/change-password",
            templateUrl: "views/change-password.html",
            controller: "ChangePwdCtrl",
            resolve: {
                isUserType: ["$q", function($q) {
                    var deferred = $q.defer();
                    var utype = $("#userType").val();

                    if (utype !== config.portalUserType) {
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }

                    return deferred.promise;

                }]
            }
        }).
         state("viewProfile", {
            "url": "/view-profile",
          templateUrl: "views/view-profile.html",
            controller: "ViewProfileCtrl"

        });

        $urlRouterProvider.otherwise("/");
    }
]);

// Constants for application
"use strict";
angular.module("developerHub").constant("config", {
    portalUserType: "support",
    isSessionCheck: false
});
'use strict'
var isCancelClicked = false
var isSubmitAllowed=true;
function required (data) {
  if (data.length === 0) {
    return true
  } else {
    return false
  }
}
function alphaNumeric (data) {
  var textRegx = /[^a-zA-Z\-\ 0-9]/; // With alpha numric we also checked spl character(-) and space.....
  if (textRegx.test(data)) {
    return true
  } else {
    return false
  }
}
function alphaNumericCheck (data) {
  var textRegx = /[^a-zA-Z\-\ 0-9\']/
  if (textRegx.test(data)) {
    return true
  } else {
    return false
  }
}
function validateEmail (data) {
  var emailRegx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (!emailRegx.test(data)) {
    return true
  } else {
    return false
  }
}
function confirmEmailCheck (email, confirmEmail) {
  if (email.toLowerCase() !== confirmEmail.toLowerCase()) {
    return true
  } else {
    return false
  }
}
function validatePassword (data) {
  var passwordRegx = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^a-zA-Z0-9]).{8,}/
  if (!passwordRegx.test(data)) {
    return true
  } else {
    return false
  }
}
function confirmPasswordCheck (password, confirmPassword) {
  if (password !== confirmPassword) {
    return true
  } else {
    return false
  }
}
function addError (ele, content) {
  var span = $('<span class="alert-inline" role="alert"></span>')
  span.text(content)
  $(ele).after(span)
}

function removeError (ele) {
  $('.alert-inline', ele).remove()
}

function validateInput (data, validateType, isRequired, isValidate) {
  isSubmitAllowed = false;
  if (isCancelClicked) { 
    isCancelClicked = false; 
    isSubmitAllowed = true;
    return; 
  }
  if (typeof (data.value) !== 'undefined') { data.value = (data.value).trim(); }

  var submitButton = document.getElementById('submitButton')
  var errorFlag = false
  switch (validateType) {
    case 'password':
      if (isRequired) { errorFlag = required(data.value); }
      if (errorFlag) { break; }
      if (isValidate) { errorFlag = validatePassword(data.value); }
      break
    case 'newPassword':
      var confirmPassword = document.getElementById('confirmPassword')
      if (isRequired) { errorFlag = required(data.value); }
      if (errorFlag) { break; }
      if (isValidate) {
        if (validatePassword(data.value)) {
          errorFlag = true
        } else if (confirmPassword !== null && confirmPassword.value !== '') {
          if (confirmPassword.value !== data.value) {
            if (confirmPassword.className.indexOf('invalid') === -1) {
              $(confirmPassword).addClass('invalid')
              errorFlag = true
              data = confirmPassword
            }
          }else {
            errorFlag = false
            data = confirmPassword
          }
        }
      }
      break
    case 'confirmPassword':
      var newPassword = document.getElementById('newPassword')
      if (isRequired) { errorFlag = required(data.value); }
      if (errorFlag) { break; }
      if (isValidate) { errorFlag = confirmPasswordCheck(newPassword.value, data.value); }
      break
    case 'inputName':
      if (isRequired) { errorFlag = required(data.value); }
      if (errorFlag) { break; }
      if (isValidate) { errorFlag = alphaNumeric(data.value); }
      break
    case 'email':
      var confirmEmail = document.getElementById('confirmEmail')
      if (isRequired) { errorFlag = required(data.value); }
      if (errorFlag) { break; }
      if (isValidate) {
        if (validateEmail(data.value)) {
          errorFlag = true
        } else {
          var stringToTest = data.value.substring(data.value.lastIndexOf('@') + 1, data.value.lastIndexOf('.'))
          var subStr = stringToTest.split('.')
          for (var i = 0; i < subStr.length; i++) {
            if (subStr[i].charAt(0) === '-' || subStr[i].charAt(subStr[i].length - 1) === '-') {
              errorFlag = true
            }
          }
        }
        if (confirmEmail !== null && confirmEmail.value !== '' && errorFlag === false) {
          if (confirmEmail.value.toLowerCase() !== data.value.toLowerCase()) {
            if (confirmEmail.className.indexOf('invalid') === -1) {
              $(confirmEmail).addClass('invalid')
              errorFlag = true
              data = confirmEmail
            }
          }else {
            data = confirmEmail
            errorFlag = false
          }
        }
      }
      break
    case 'confirmEmail':
      var email = document.getElementById('email')
      if (isRequired) { errorFlag = required(data.value); }
      if (errorFlag) { break; }
      if (isValidate) { errorFlag = confirmEmailCheck(email.value, data.value); }
      break
    case 'checkbox':
      if (isRequired) {
        if (!data.checked) { errorFlag = true; }
      }
      break
    case 'firstName':
      if (isRequired) { errorFlag = required(data.value); }
      if (errorFlag) { break; }
      if (isValidate) { errorFlag = alphaNumericCheck(data.value); }
      break
    case 'lastName':
      if (isRequired) { errorFlag = required(data.value); }
      if (errorFlag) { break; }
      if (isValidate) { errorFlag = alphaNumericCheck(data.value); }
      break
    case 'organization':
      if (isRequired) { errorFlag = required(data.value); }
      if (errorFlag) { break; }
      if (isValidate) { errorFlag = alphaNumericCheck(data.value); }
      break
  }
  if (errorFlag) {
	isSubmitAllowed = false; 
    if (data.className.indexOf('invalid') === -1) {
      $(data).addClass('invalid')
    }

    var errMsg = $(data).parent().children('.err-content').text()

    addError(data, errMsg)
    if ($(submitButton).is(':focus')) {
      if ($('#cancelButton').length) {
        $('#cancelButton').focus()
      } else {
        $('.footer-links>li:first>a').focus()
      }
    }
    submitButton.disabled = true
  } else {
	isSubmitAllowed = true; 
    if (data.className.indexOf('invalid') !== -1) {
      $(data).removeClass('invalid')
      removeError($(data).parent())
      validateFormRequired(data.form.id, 'blur')
    } else if (document.getElementsByClassName('invalid').length > 0) {
      if ($(submitButton).is(':focus')) {
        $('#cancelButton').focus()
      }
      submitButton.disabled = true
    }
  }
}

function validateFormRequired (formId, eventType, currentElementId) {
  isSubmitAllowed = false; 
  if (eventType === 'keydown') {
    if (window.event.keyCode !== 13) {
      return
    }
    if (event.target.type === 'checkbox' || event.target.nodeName === 'A') {
      return
    }
  }
  var elements = document.forms[formId].elements
  var submitButtonEnableFlag = true
  for (var i = 0; i < elements.length; i++) {
    if (currentElementId === elements[i].id && elements[i].type !== 'checkbox') {
      continue
    } else if (elements[i].type === 'text' || elements[i].type === 'email' || elements[i].type === 'password') {
      submitButtonEnableFlag = formElementsCheck(elements[i], 'onblur')
    } else if (elements[i].type === 'checkbox') {
      submitButtonEnableFlag = formElementsCheck(elements[i], 'onchange')
    }
    if (!submitButtonEnableFlag) {
	    	break;
    	}
  }
  var submitButton = document.getElementById('submitButton')
  if (submitButtonEnableFlag) {
    submitButton.disabled = false
    isSubmitAllowed = true; 
    $('.portals-alert').remove()
    if (!currentElementId) {
      if (window.event.keyCode === 13 && $(window.event.target).attr('id') !== 'cancelButton') {
        window.event.preventDefault();
        document.getElementById('submitButton').click();
      }
    }
  } else {
    submitButton.disabled = true;
	
    if ($(submitButton).is(':focus')) {
      $('#cancelButton').focus()
    }
    if (!currentElementId) {
      if (window.event.keyCode === 13) {
        window.event.preventDefault()
      }
    }
  }
}

function formElementsCheck (elements, eventType) {
  var str = '', delimiter = '', start = 0, end = 0, tokens = [], result = ''
  str = elements.getAttribute(eventType)
  if (str !== null) {
    delimiter = ','
    start = 2
    end = 3
    tokens = str.split(delimiter).slice(start, end)
    result = tokens.join(delimiter)
    if ( (result.indexOf('true') !== -1)) {
      if ((elements.value !== '' || elements.checked) && document.getElementsByClassName('invalid').length === 0) {
        return true
      } else {
        return false
      }
    } else {
      return true
    }
  }
}

function focusInput (data) {
  // var data=JSON.parse(data)
  if (data && data.className.indexOf('invalid') !== -1) {
    $(data).removeClass('invalid')
    removeError($(data).parent())
  }
  validateFormRequired(data.form.id, 'focus', data.id)
}

function checkKeyDown (data) {
  if (window.event.keyCode === 13) {
    data.blur()
  }
}

"use strict";
angular.module("developerHub").controller("confirmationPopupCtrl", ["$scope", "$uibModalInstance", "$state","$window", function($scope, $uibModalInstance,$state, $window) {
    $scope.cancelBtnClicked = function() {
        $uibModalInstance.dismiss("cancel");
    };
   
    $scope.okBtnClicked = function(urlType) {
        $uibModalInstance.dismiss("cancel");
        switch(urlType){
                 case "logout":
                        var logoutURL = $("#logoutURL").val();
                        $window.location.href = logoutURL;
                        break;
                case "viewProfile":
                        $state.go("viewProfile", {});
                        break;
                }
        };
    }]);


angular.module("developerHub").controller("ChangePwdCtrl", ["$scope", "$http", "$q", "$window", "$location", "$state", "$uibModal", "$timeout", "DevPortalService", "blockUI", "$translate", "SessionPopupService", "$rootScope", function($scope, $http, $q, $window, $location, $state, $uibModal, $timeout, DevPortalService, blockUI, $translate, SessionPopupService, $rootScope) {
    var $ = window.jQuery;
    $scope.errorDataState = "";
    $scope.errorData = null;
    $scope.isCookieDisabled = false;
    $scope.userName = $("#userName").val();
    $scope.init = function() {
        $scope.passwords = {
            currentPassword: "",
            newPassword: "",
            confirmPassword: ""
        };
        $scope.resetFields();
        if (!navigator.cookieEnabled) {
            $scope.errorData = {};
            $scope.errorData.errorCode = "COOKIE_ENABLE_MSG";
            $scope.isCookieDisabled = true;
        }
        $translate(["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.CHANGE_PASSWORD_HEADING", "CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.CURRENT_PASSWORD_LABEL", "CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.NEW_PASSWORD_LABEL", "CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.CONFIRM_PASSWORD_LABEL", "CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.PASSWORD_RULES_HEADEING", "CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.PASSWORD_RULE1_LABEL", "CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.PASSWORD_RULE2_LABEL", "CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.PASSWORD_RULE3_LABEL", "CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.PASSWORD_RULE4_LABEL", "CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.BUTTONS.CANCEL_BUTTON_LABEL", "CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.BUTTONS.SUBMIT_BUTTON_LABEL", "CHANGE_PASSWORD_PAGE.PASSWORD_CHANGE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_HEADING", "CHANGE_PASSWORD_PAGE.PASSWORD_CHANGE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_MESSAGE", "CHANGE_PASSWORD_PAGE.PASSWORD_CHANGE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_BUTTON_LABEL", "ERROR_MESSAGES.CURRENT_PASSWORD_ERROR", "ERROR_MESSAGES.NEW_PASSWORD_ERROR", "ERROR_MESSAGES.CONFIRM_PASSWORD_ERROR"]).then(function(translations) {
            $scope.changePasswordHeader = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.CHANGE_PASSWORD_HEADING"];
            $scope.currentPasswordLabel = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.CURRENT_PASSWORD_LABEL"];
            $scope.newPasswordLabel = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.NEW_PASSWORD_LABEL"];
            $scope.confirmPasswordLabel = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.CONFIRM_PASSWORD_LABEL"];
            $scope.passwordRulesLabel = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.PASSWORD_RULES_HEADEING"];
            $scope.passwordRulesLabel1 = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.PASSWORD_RULE1_LABEL"];
            $scope.passwordRulesLabel2 = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.PASSWORD_RULE2_LABEL"];
            $scope.passwordRulesLabel3 = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.PASSWORD_RULE3_LABEL"];
            $scope.passwordRulesLabel4 = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.PASSWORD_RULE4_LABEL"];
            $scope.cancelButtonLabel = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.BUTTONS.CANCEL_BUTTON_LABEL"];
            $scope.submitButtonLabel = translations["CHANGE_PASSWORD_PAGE.CHANGE_PASSWORD_FORM.BUTTONS.SUBMIT_BUTTON_LABEL"];
            $scope.confirmationPopupHeader = translations["CHANGE_PASSWORD_PAGE.PASSWORD_CHANGE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_HEADING"];
            $scope.confirmationPopupMessage = translations["CHANGE_PASSWORD_PAGE.PASSWORD_CHANGE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_MESSAGE"];
            $scope.confirmationPopupButtonLabel = translations["CHANGE_PASSWORD_PAGE.PASSWORD_CHANGE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_BUTTON_LABEL"];
            $scope.currentPasswordError = translations["ERROR_MESSAGES.CURRENT_PASSWORD_ERROR"];
            $scope.newPasswordError = translations["ERROR_MESSAGES.NEW_PASSWORD_ERROR"];
            $scope.confirmPasswordError = translations["ERROR_MESSAGES.CONFIRM_PASSWORD_ERROR"];
            $scope.astericSignText = "*";
        });
    };
    $scope.changePasswordFn = function() {
        if ($scope.passwords.currentPassword !== "" || $scope.passwords.newPassword !== "") {
            var payLoad = {
                "oldPwd": $scope.passwords.currentPassword,
                "newPwd": $scope.passwords.newPassword
            };
            $timeout(function(){
                $scope.resetFields();
                });
            DevPortalService.changePasswordRequest(payLoad).then(function(resp) {
                   // $scope.resetFields();
                    if (resp.data.statusCode === "200") {
                     //   $scope.resetFields();
                        $scope.confirmationPopup();
                    }
                },
                function(error) {
                //$scope.resetFields();
                    var submitButton = document.getElementById("submitButton");
                    submitButton.disabled = true;
                    $scope.errorDataState = error.data.errorCode;
                        if (!$scope.errorDataState || $scope.errorDataState === null) {
                        $scope.errorData = {};
                        $scope.errorData.errorCode = "1113";
                        $scope.errorDataState = $scope.errorData.errorCode;
                    } else {
                        if ($scope.errorDataState === "1115" || $scope.errorDataState === "1116") {
                            $scope.errorDataState = error.data.errorCode;
                            SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                        } else {
                            $scope.errorData = {};
                            $scope.errorData.errorCode = error.data ? $scope.errorDataState : "1113";
                        }
                    }
                });
        }
    };

    $scope.cancelClicked = function($evt, urlType, eventButton) {
        if (eventButton === "keydown") {
            if (window.event.keyCode !== 13) {
                return;
            }
        }
        if (($evt.type === "mousedown" && $evt.which === 1) || (($evt.type === "keydown" && (window.event.keyCode === 13)) || $evt.type === "click")) {
            window.isCancelClicked = true;
            DevPortalService.authSessionRequest().then(function(resp) {
                    if (resp.data.statusCode === "200") {
                        switch (urlType) {
                            case "gettingStarted":
                                $state.go("developerHub", {});
                                break;
                        }
                    }
                },
                function(error) {
                    $scope.resetFields();
                    var submitButton = document.getElementById("submitButton");
                    submitButton.disabled = true;
                    $scope.errorDataState = error.data.errorCode;
                    if (!$scope.errorDataState || $scope.errorDataState === null) {
                        $scope.errorData = {};
                        $scope.errorData.errorCode = "1113";
                        $scope.errorDataState = $scope.errorData.errorCode;
                    } else {
                        if (error.data.errorCode === "1115" || error.data.errorCode === "1116") {
                            $scope.errorData = null;
                            SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                        } else {
                            $scope.errorData = {};
                            $scope.errorData.errorCode = error.data ? error.data.errorCode : "1113";
                        }
                    }
                });
        }
        $evt.preventDefault();
    };

    $scope.resetFields = function() {
        // $scope.changePasswordForm.$setPristine();
        // $scope.changePasswordForm.$setUntouched();
        $scope.passwords = {};
        $scope.passwords.currentPassword = "";
        $scope.passwords.newPassword = "";
        $scope.passwords.confirmPassword = "";
     //   $(".alert-inline").remove();
      //  $("input").removeClass("invalid");
        $scope.errorData = null;
        blockUI.stop();
    };

    $scope.confirmationPopup = function() {
        $scope.popupAction="logout";
        $uibModal.open({
            templateUrl: "views/confirmation-popup.html",
            animation: true,
            backdrop: false,
            keyboard: false,
            windowClass: "pop-confirm",
            scope: $scope,
            resolve: {},
            controller: "confirmationPopupCtrl"
        });
    };

    $scope.focusMainContent = function() {
        $("#currentPassword").focus();
    };

    $scope.init();
}]);
"use strict";
angular.module("developerHub").controller("HomeCtrl", ["$scope", "$window", "$uibModal", "DevPortalService", "blockUI", "$translate", "$rootScope", "SessionPopupService", "$timeout", function($scope, $window, $uibModal, DevPortalService, blockUI, $translate, $rootScope, SessionPopupService, $timeout, fontResolved) {
    var $ = window.jQuery;
    $scope.popup = null;
    $scope.errorDataState = "";
    $scope.isCookieDisabled = false;
    $scope.accessURL={};
    $scope.userName = $("#userName").val();
    $scope.isBlockedApis = $("#isBlockedApis").val() == "true"? true: false;

    $scope.init = function() {
        if (!navigator.cookieEnabled) {
            $scope.errorData = {};
            $scope.errorData.errorCode = "COOKIE_ENABLE_MSG";
            $scope.isCookieDisabled = true;
        }
        $translate(["GETTING_STARTED_PAGE.GETTING_STARTED_HEADING", "GETTING_STARTED_PAGE.API_PORTAL_INFO.API_PORTAL_INTRO_HEADING",
            "GETTING_STARTED_PAGE.API_PORTAL_INFO.BUTTONS.DOWNLOAD_TEST_DATA_SCREEN_READER_LABEL", "GETTING_STARTED_PAGE.API_PORTAL_INFO.BUTTONS.GOTO_API_SANDBOX_SCREEN_READER_LABEL", "GETTING_STARTED_PAGE.API_PORTAL_INFO.BUTTONS.GOTO_API_SANDBOX_LABEL", "GETTING_STARTED_PAGE.API_PORTAL_INFO.BUTTONS.GOTO_DOWNLOAD_TEST_DATA", "GETTING_STARTED_PAGE.API_PORTAL_INFO.BUTTONS.GOTO_BOI_TPP",
            "GETTING_STARTED_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO_HEADING", "GETTING_STARTED_PAGE.TPP_PORTAL_INFO.BUTTONS.GOTO_TPP_HUB_SCREEN_READER_LABEL", "GETTING_STARTED_PAGE.TPP_PORTAL_INFO.BUTTONS.GOTO_TPP_HUB_SCREEN_READER_LABEL", "GETTING_STARTED_PAGE.TESTING_APP_FLOW.TEST_APP_FLOW_HEADING", "GETTING_STARTED_PAGE.TESTING_APP_FLOW.API_CATALOGUE_STEP_LABEL",
            "GETTING_STARTED_PAGE.TESTING_APP_FLOW.API_CATALOGUE_STEP_DETAIL_LABEL", "GETTING_STARTED_PAGE.TESTING_APP_FLOW.REQUEST_API_STEP_LABEL", "GETTING_STARTED_PAGE.TESTING_APP_FLOW.REQUEST_API_STEP_DETAIL_LABEL", "GETTING_STARTED_PAGE.TESTING_APP_FLOW.DOWNLOAD_TEST_DATA_STEP_LABEL",
            "GETTING_STARTED_PAGE.TESTING_APP_FLOW.DOWNLOAD_TEST_DATA_STEP_DETAILS_LABEL", "GETTING_STARTED_PAGE.API_SUMMARY_FLOW.API_SUMMARY_FLOW_HEADING", "GETTING_STARTED_PAGE.API_SUMMARY_FLOW.API_PROVIDED_SUMMARY_LABEL", "GETTING_STARTED_PAGE.API_SUMMARY_FLOW.API_PROVIDED_SUMMARY_DETAIL_LABEL", "GETTING_STARTED_PAGE.API_SUMMARY_FLOW.MONEY_MOVEMENT_SUMMARY_LABEL", "GETTING_STARTED_PAGE.API_SUMMARY_FLOW.MONEY_MOVEMENT_SUMMARY_DETAIL_LABEL", "GETTING_STARTED_PAGE.API_SUMMARY_FLOW.ACCOUNT_SUMMARY_LABEL", "GETTING_STARTED_PAGE.API_SUMMARY_FLOW.ACCOUNT_SUMMARY_DETAILS_LABEL"
        ]).then(function(translations) {
            $scope.gettingStartedHeader = translations["GETTING_STARTED_PAGE.GETTING_STARTED_HEADING"];
            $scope.apiPortalInfoHeader = translations["GETTING_STARTED_PAGE.API_PORTAL_INFO.API_PORTAL_INTRO_HEADING"];
            $scope.apiPortalAPISandboxButton = translations["GETTING_STARTED_PAGE.API_PORTAL_INFO.BUTTONS.GOTO_API_SANDBOX_LABEL"];
            $scope.apiPortalDownloadTestData = translations["GETTING_STARTED_PAGE.API_PORTAL_INFO.BUTTONS.GOTO_DOWNLOAD_TEST_DATA"];
            $scope.apiPortalAccessBoiTpp = translations["GETTING_STARTED_PAGE.API_PORTAL_INFO.BUTTONS.GOTO_BOI_TPP"];
            $scope.apiTPPPortalInfoHeader = translations["GETTING_STARTED_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO_HEADING"];
            $scope.testingAppHeader = translations["GETTING_STARTED_PAGE.TESTING_APP_FLOW.TEST_APP_FLOW_HEADING"];
            $scope.testingAppAPICatalogue = translations["GETTING_STARTED_PAGE.TESTING_APP_FLOW.API_CATALOGUE_STEP_LABEL"];
            $scope.testingAppAPICatalogueDetails = translations["GETTING_STARTED_PAGE.TESTING_APP_FLOW.API_CATALOGUE_STEP_DETAIL_LABEL"];
            $scope.testingAppAPIRequest = translations["GETTING_STARTED_PAGE.TESTING_APP_FLOW.REQUEST_API_STEP_LABEL"];
            $scope.testingAppAPIRequestDetail = translations["GETTING_STARTED_PAGE.TESTING_APP_FLOW.REQUEST_API_STEP_DETAIL_LABEL"];
            $scope.testingAppDownloadData = translations["GETTING_STARTED_PAGE.TESTING_APP_FLOW.DOWNLOAD_TEST_DATA_STEP_LABEL"];
            $scope.testingAppDownloadDataDetails = translations["GETTING_STARTED_PAGE.TESTING_APP_FLOW.DOWNLOAD_TEST_DATA_STEP_DETAILS_LABEL"];
            $scope.summaryAPIHeader = translations["GETTING_STARTED_PAGE.API_SUMMARY_FLOW.API_SUMMARY_FLOW_HEADING"];
            $scope.summaryAPIProvide = translations["GETTING_STARTED_PAGE.API_SUMMARY_FLOW.API_PROVIDED_SUMMARY_LABEL"];
            $scope.summaryAPIProvideDetails = translations["GETTING_STARTED_PAGE.API_SUMMARY_FLOW.API_PROVIDED_SUMMARY_DETAIL_LABEL"];
            $scope.summaryAPIMoneyMovement = translations["GETTING_STARTED_PAGE.API_SUMMARY_FLOW.MONEY_MOVEMENT_SUMMARY_LABEL"];
            $scope.summaryAPIMoneyMovementDetails = translations["GETTING_STARTED_PAGE.API_SUMMARY_FLOW.MONEY_MOVEMENT_SUMMARY_DETAIL_LABEL"];
            $scope.summaryAPIAccount = translations["GETTING_STARTED_PAGE.API_SUMMARY_FLOW.ACCOUNT_SUMMARY_LABEL"];
            $scope.summaryAPIAccountDetails = translations["GETTING_STARTED_PAGE.API_SUMMARY_FLOW.ACCOUNT_SUMMARY_DETAILS_LABEL"];
            $scope.apiPortalDownloadScrLabel = translations["GETTING_STARTED_PAGE.API_PORTAL_INFO.BUTTONS.DOWNLOAD_TEST_DATA_SCREEN_READER_LABEL"];
            $scope.apiPortalAPISandboxScrLabel = translations["GETTING_STARTED_PAGE.API_PORTAL_INFO.BUTTONS.GOTO_API_SANDBOX_SCREEN_READER_LABEL"];
            $scope.apiPortalThirdPartyScrLabel = translations["GETTING_STARTED_PAGE.TPP_PORTAL_INFO.BUTTONS.GOTO_TPP_HUB_SCREEN_READER_LABEL"];
            $scope.apiPortalAccessBoiLabel = translations["GETTING_STARTED_PAGE.TPP_PORTAL_INFO.BUTTONS.GOTO_TPP_HUB_SCREEN_READER_LABEL"];
        });


        var apiDataHeading = "GETTING_STARTED_PAGE.API_PORTAL_INFO.API_PORTAL_INTRO.";
        var apiPortalData = ["API_PORTAL_INTRO_PARA1", "API_PORTAL_INTRO_PARA2", "API_PORTAL_INTRO_PARA3", "API_PORTAL_INTRO_PARA4", "API_PORTAL_INTRO_PARA5"];
        $scope.apiData = {};
        $translate(apiPortalData.map(function(key) { return apiDataHeading + key; })).then(function(translations) {
            for (var i = 1; i <= 5; i++) {
                if (translations["GETTING_STARTED_PAGE.API_PORTAL_INFO.API_PORTAL_INTRO.API_PORTAL_INTRO_PARA" + i] !== "") {
                    $scope.apiData[i] = {};
                    $scope.apiData[i].para = translations["GETTING_STARTED_PAGE.API_PORTAL_INFO.API_PORTAL_INTRO.API_PORTAL_INTRO_PARA" + i];
                }
            }
        });

        var tppDataHeading = "GETTING_STARTED_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.";
        var tppPortalData = ["TPP_PORTAL_INTRO_PARA1", "TPP_PORTAL_INTRO_PARA2", "TPP_PORTAL_INTRO_PARA3", "TPP_PORTAL_INTRO_PARA4", "TPP_PORTAL_INTRO_PARA5"];
        $scope.tppData = {};
        $translate(tppPortalData.map(function(key) { return tppDataHeading + key; })).then(function(translations) {
            for (var i = 1; i <= 5; i++) {
                if (translations["GETTING_STARTED_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.TPP_PORTAL_INTRO_PARA" + i] !== "") {
                    $scope.tppData[i] = {};
                    $scope.tppData[i].para = translations["GETTING_STARTED_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.TPP_PORTAL_INTRO_PARA" + i];
                }
            }
        });
    };

    $scope.downloadTestData = function(){
        if($scope.userName == null || $scope.userName.length == 0){
            var portalBaseURL = $("#portalBaseURL").val();
            $window.location.href = portalBaseURL;
        }else{
            DevPortalService.testDataRequest().then(function(res){
                var octetStreamMime = "application/octet-stream";
                var headers = res.headers();
                var contentType = headers["content-type"] || octetStreamMime;
                var blob = new Blob([res.data], { type: contentType });
                if(navigator.msSaveBlob){
                    navigator.msSaveBlob(blob,"TestData_v1.xlsx");
                }else{
                    //var objectUrl = URL.createObjectURL(blob);
                    //window.open(objectUrl);
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var url = window.URL.createObjectURL(blob);
                    a.href = url;
                    a.download = "TestData_v1.xlsx";
                    a.click();
                    setTimeout(function(){
                    window.URL.revokeObjectURL(url);
                    document.body.removeChild(a);
                    }, 0);
                // saveBlob(blob, "my-excel.xlsx");
                }
            },
            function(error) {
                if(error.status =="401"){
                        $scope.errorDataState = "1115";
                    }else if(error.status =="400"){
                        $scope.errorDataState = "1114";
                    }else{
                        $scope.errorDataState = error.data.errorCode;
                    }
                //$scope.errorDataState = error.data.errorCode;
                if (!$scope.errorDataState || $scope.errorDataState === null) {
                        $scope.errorData = {};
                        $scope.errorData.errorCode = "1114";
                        $scope.errorDataState = $scope.errorData.errorCode;
                        $scope.errorData.errorCode = error.data ? $scope.errorDataState : "1114";
                    }
                        if ($scope.errorDataState === "1115") {
                            $scope.errorData = null;
                            if ($scope.popup !== null) {
                                $scope.popup.document.body.innerHTML = "Your session has timed out.";
                                $timeout(function() {
                                    $scope.popup.close();
                                    SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                                }, 1000);
                            } else {
                                SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                            }
                        }
                });
        }
    };

    // $scope.accessTPP = function(){
    //     DevPortalService.authSessionRequest().then(function(resp){
    //         if (resp.data.statusCode === "200"){
    //                 $scope.accessURL = $("#TPPPortalURL").val();
    //                 window.open($scope.accessURL);
    //         }
    //     },
    //     function(error){
    //         console.log("hh", error)

    //         $scope.errorDataState = error.data.errorCode;
    //         if ($scope.errorDataState === "1115") {
    //                     $scope.errorData = null;
    //                     if ($scope.popup !== null) {
    //                         $scope.popup.document.body.innerHTML = "Your session has timed out.";
    //                         $timeout(function() {
    //                             $scope.popup.close();
    //                             SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
    //                         }, 1000);
    //                     } else {
    //                         SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
    //                     }
    //                 }
    //     });
    // };

    $scope.homeSessionCheck = function(urlType) {
        if (urlType === "goToMule" || urlType === "accessTpp") {
            $scope.popup = $window.open("", "_blank");
            $scope.popup.document.body.innerHTML = "loading ...";
        }
        DevPortalService.authSessionRequest().then(function(resp) {
                if (resp.data.statusCode === "200") {
                    switch (urlType) {
                        case "goToMule":
                            var muleLaunchURL = $("#muleLaunchURL").val();
                            $scope.popup.location.href = muleLaunchURL;
                            break;
                        case "downloadTestData":
                            var url = "/auth/downloadTestData";
                            $window.location.href = url;
                            break;
                        case "accessTpp":
                         var accessURL = $("#TPPPortalURL").val();
                         $scope.popup.location.href = accessURL;

                    }
                }
            },
            function(error) {
                $scope.errorDataState = error.data.errorCode;
                if (!$scope.errorDataState || $scope.errorDataState === null) {

                    $scope.errorData = {};
                    $scope.errorData.errorCode = "1113";
                    $scope.errorDataState = $scope.errorData.errorCode;
                    if ($scope.popup !== null) {
                        $timeout(function() {
                            $scope.popup.close();
                        }, 1000);
                    }
                } else {
                    if ($scope.errorDataState === "1115" || $scope.errorDataState === "1116") {
                        $scope.errorData = null;
                        if ($scope.popup !== null) {
                            $scope.popup.document.body.innerHTML = "Your session has timed out.";
                            $timeout(function() {
                                $scope.popup.close();
                                SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                            }, 1000);
                        } else {
                            SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                        }
                    } else {
                        $scope.errorData = {};
                        $scope.errorData.errorCode = error.data ? $scope.errorDataState : "1113";
                    }
                }
            });
    };

    $scope.focusMainContent = function() {
        $("#downloadTestDataBtn").focus();
    };

    $scope.init();
}]);

"use strict";
angular.module("developerHub").controller("ViewProfileCtrl", ["$scope", "$http", "$q", "$window", "$location", "$state", "$uibModal", "$timeout", "DevPortalService", "blockUI", "$translate", "SessionPopupService", "$rootScope", function ($scope, $http, $q, $window, $location, $state, $uibModal, $timeout, DevPortalService, blockUI, $translate, SessionPopupService, $rootScope) {
  var $ = window.jQuery;
  $scope.errorDataState = "";
  $scope.errorData = null;
  $scope.isCookieDisabled = false;
  $scope.editForm = false;
  var editProfileSubmitAction = true;

  $scope.init = function () {
    if (!navigator.cookieEnabled) {
      $scope.pageErrorData = {};
      $scope.pageErrorData.errorCode = "COOKIE_ENABLE_MSG";
      $scope.isCookieDisabled = true;
    }
    $scope.viewUserProfile();
    $translate(["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.VIEW_PROFILE_HEADING",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.FIRST_NAME_LABEL",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.LAST_NAME_LABEL",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.EMAIL_ID_LABEL",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.CONTACT_NO_LABEL",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.COUNTRY_LABEL",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.ORGANIZATION_LABEL",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.EDIT_BUTTON_LABEL",
      "VIEW_PROFILE_PAGE.EDIT_PROFILE_SECTION.BUTTONS.CANCEL_BUTTON_LABEL" ,
      "VIEW_PROFILE_PAGE.EDIT_PROFILE_SECTION.EDIT_PROFILE_HEADING" ,
      "VIEW_PROFILE_PAGE.EDIT_PROFILE_SECTION.BUTTONS.SUBMIT_BUTTON_LABEL",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_HEADING",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_MESSAGE",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_BUTTON_LABEL",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.EDIT_BUTTON_TOOLTIP_LABEL",
      "VIEW_PROFILE_PAGE.EDIT_PROFILE_SECTION.ORGANIZATION_TOOLTIP_SCREEN_READER_LABEL",
      "ERROR_MESSAGES.FIRST_NAME_ERROR",
      "ERROR_MESSAGES.LAST_NAME_ERROR",
      "ERROR_MESSAGES.ORGANIZATION_ERROR",
      "VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.COLON_LABEL"
    ]).then(function (translations) {
      $scope.viewProfileHeader = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.VIEW_PROFILE_HEADING"];
      $scope.firstNameLabel = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.FIRST_NAME_LABEL"];
      $scope.lastNameLabel = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.LAST_NAME_LABEL"];
      $scope.emailIDLabel = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.EMAIL_ID_LABEL"];
      $scope.contactNoLabel = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.CONTACT_NO_LABEL"];
      $scope.countryLabel = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.COUNTRY_LABEL"];
      $scope.organizationLabel = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.ORGANIZATION_LABEL"];
      $scope.editBtnLabel = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.EDIT_BUTTON_LABEL"];
     $scope.editProfileHeader=translations[ "VIEW_PROFILE_PAGE.EDIT_PROFILE_SECTION.EDIT_PROFILE_HEADING"];
      $scope.cancelButtonLabel = translations["VIEW_PROFILE_PAGE.EDIT_PROFILE_SECTION.BUTTONS.CANCEL_BUTTON_LABEL"];
      $scope.submitButtonLabel = translations["VIEW_PROFILE_PAGE.EDIT_PROFILE_SECTION.BUTTONS.SUBMIT_BUTTON_LABEL"];
      $scope.confirmationPopupHeader = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_HEADING"];
      $scope.confirmationPopupMessage = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_MESSAGE"];
      $scope.confirmationPopupButtonLabel = translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_CONFIRMATION_POPUP.CONFIRMATION_POPUP_BUTTON_LABEL"];
      $scope.firstNameError=translations["ERROR_MESSAGES.FIRST_NAME_ERROR"];
      $scope.lastNameError=translations["ERROR_MESSAGES.LAST_NAME_ERROR"];
      $scope.organizationError=translations["ERROR_MESSAGES.ORGANIZATION_ERROR"];
      $scope.organizationScrTooltip=translations["VIEW_PROFILE_PAGE.EDIT_PROFILE_SECTION.ORGANIZATION_TOOLTIP_SCREEN_READER_LABEL"];
      $scope.astericSignText = "*";
      $scope.editTitle=translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.EDIT_BUTTON_TOOLTIP_LABEL"];
      $scope.colonlabel=translations["VIEW_PROFILE_PAGE.VIEW_PROFILE_SECTION.COLON_LABEL"];
      
     
    });
  };
  $scope.viewUserProfile = function () {
    $scope.emailId=$("#emailId").val();
    DevPortalService.viewProfileRequest($scope.emailId).then(function (resp) {
      $scope.viewProfileData = resp.data;
    },
      function (error) {
        $scope.resetFields();
          $scope.errorDataState = error.data.errorCode;
        if (!$scope.errorDataState || $scope.errorDataState === null) {
          $scope.pageErrorData = {};
          $scope.pageErrorData.errorCode = "1106";
          $scope.errorDataState = $scope.pageErrorData.errorCode;
        } else {
          if ($scope.errorDataState === "1115" || $scope.errorDataState === "1116") {
            $scope.errorDataState = error.data.errorCode;
            SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
          } else {
            $scope.pageErrorData = {};
            $scope.pageErrorData.errorCode = error.data ? $scope.errorDataState : "1104";
          }
        }
      });
  };



  $scope.resetFields = function (data) {
    $(".alert-inline").remove();
    $("input").removeClass("invalid");
    $scope.errorData = null;
    blockUI.stop();
  };

  $scope.editProfile = function ($evt, eventButton) {
    editProfileSubmitAction = true;
        if (eventButton === "keydown") {
      if (window.event.keyCode !== 13) {
          return;
      }
  }
  if (($evt.type === "mousedown" && $evt.which === 1) || (($evt.type === "keydown" && (window.event.keyCode === 13)) || $evt.type === "click")) {
      
      DevPortalService.authSessionRequest().then(function(resp) {
        $scope.editForm = true;
		var submitButton = document.getElementById("submitButton");
    submitButton.disabled = false;
    window.isSubmitAllowed = true;
    window.isCancelClicked = false;
       	$('[data-toggle="tooltip"]').tooltip();
         $scope.placement = {
                selected: "right"
            };
          },
          function(error) {
              $scope.resetFields();
              var submitButton = document.getElementById("submitButton");
              submitButton.disabled = true;
              $scope.errorDataState = error.data.errorCode;
              if (error.data.errorCode === "1115" || error.data.errorCode === "1116") {
                      $scope.errorData = null;
                      SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                  } 
                        });
  
  $evt.preventDefault();
                      }
};
  $scope.confirmationPopup = function () {
    $scope.popupAction = "viewProfile";
    $scope.editForm=false;
    $uibModal.open({
      templateUrl: "views/confirmation-popup.html",
      animation: true,
      backdrop: false,
      keyboard: true,
      windowClass: "pop-confirm",
      scope: $scope,
      resolve: {},
      controller: "confirmationPopupCtrl"
    });
  };

  $scope.cancelClicked = function($evt, urlType, eventButton) {
    
    $scope.resetFields();
    if (eventButton === "keydown") {
        if (window.event.keyCode !== 13) {
            return;
        }
    }
    if (($evt.type === "mousedown" && $evt.which === 1) || (($evt.type === "keydown" && (window.event.keyCode === 13)) || $evt.type === "click")) {
        window.isCancelClicked = true;
        $scope.viewUserProfile();
        DevPortalService.authSessionRequest().then(function(resp) {
                if (resp.data.statusCode === "200") {
                  $scope.editForm = false;
                }
            },
            function(error) {
               var submitButton = document.getElementById("submitButton");
                submitButton.disabled = true;
                              $scope.errorDataState = error.data.errorCode;
                if (!$scope.errorDataState || $scope.errorDataState === null) {
                    $scope.errorData = {};
                    $scope.errorData.errorCode = "1113";
                    $scope.errorDataState = $scope.errorData.errorCode;
                } else {
                    if (error.data.errorCode === "1115" || error.data.errorCode === "1116") {
                        $scope.errorData = null;
                        SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                    } else {
                        $scope.errorData = {};
                        $scope.errorData.errorCode = error.data ? error.data.errorCode : "1113";
                                          }
                }
            });
    }
    $evt.preventDefault();
};
$scope.editSubmit=function(id){
  if(window.isSubmitAllowed)
  {
  DevPortalService.editProfileRequest($scope.viewProfileData).then(function (resp) {
    if (resp.data.statusCode === "200") {
                $scope.resetFields();
                $scope.confirmationPopup();
                $scope.userName= document.getElementById("userName").value = $scope.viewProfileData.firstName;
    }
  },
    function (error) {
    var submitButton = document.getElementById("submitButton");
      submitButton.disabled = true;
       $scope.errorDataState = error.data.errorCode;     
      if (!$scope.errorDataState || $scope.errorDataState === null) {
        $scope.pageErrorData = {};
        $scope.pageErrorData.errorCode = "1113";
        $scope.errorDataState = $scope.pageErrorData.errorCode;
      } else {
        if ($scope.errorDataState === "1115" || $scope.errorDataState === "1116") {
          $scope.errorDataState = error.data.errorCode;
          SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
        } else {
          $scope.errorData = {};
          $scope.errorData.errorCode = error.data ? $scope.errorDataState : "1105";
        }
      }
    });
  }
}
 
  $scope.editFormSubmit = function () {   
     if(editProfileSubmitAction){
    $timeout(function(){
      $scope.editSubmit();
      });
      editProfileSubmitAction = false;
    }

        }; 
    $scope.focusMainContent = function(e) {
      if(!$scope.editForm){
      $("#editButton").focus();
      }
      else
      {
        $("#firstName").focus(); 
      }
  };

     $scope.init();
}]);
"use strict";
angular.module("developerHub").directive("errorNotice", ["$translate",
    function($translate) {
        return {
            restrict: "E",
            replace: true,
            scope: {
                errorData: "="
            },
            templateUrl: "views/error-notice.html",
            link: function($scope) {
                $scope.$watch("errorData", function() {
                    var errorCodeMsg = "ERROR_MESSAGES." + $scope.errorData.errorCode;
                    $translate(["ERROR_MESSAGES.ERROR_PREFIX_LABEL", errorCodeMsg]).then(function(translations) {
                        $scope.errorPrefix = translations["ERROR_MESSAGES.ERROR_PREFIX_LABEL"] + ": ";
                        $scope.errorMsgText = translations[errorCodeMsg];
                    });
                });
            }
        };
    }
]);
"use strict";
angular.module("developerHub").directive("pageFooter", ["DevPortalService", "blockUI", "$window", "$uibModal", "$translate", "SessionPopupService", "$timeout",
    function(DevPortalService, blockUI, $window, $uibModal, $translate, SessionPopupService, $timeout) {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "views/page-footer.html",
            scope: {},
            link: function($scope) {
                $scope.popup = null;
                $scope.errorDataState = "";
                $translate(["FOOTER.PRIVACY_POLICY_LABEL", "FOOTER.TNC_LABEL", "FOOTER.HELP_LABEL", "FOOTER.TOOLTIP_LABEL",
                    "FOOTER.PRIVACY_POLICY_URL", "FOOTER.TNC_URL", "FOOTER.HELP_URL", "FOOTER.REGULATORY_LABEL","FOOTER.FAQ_LABEL","FOOTER.FAQ_URL"
                ]).then(function(translations) {
                    $scope.privacyPolicy = translations["FOOTER.PRIVACY_POLICY_LABEL"];
                    $scope.tnc = translations["FOOTER.TNC_LABEL"];
                    $scope.help = translations["FOOTER.HELP_LABEL"];
                    $scope.privacyPolicyURL = translations["FOOTER.PRIVACY_POLICY_URL"];
                    $scope.tncURL = translations["FOOTER.TNC_URL"];
                    $scope.helpURL = translations["FOOTER.HELP_URL"];
                    $scope.regulatoryLabel = translations["FOOTER.REGULATORY_LABEL"];
                    $scope.tooltipTt = translations["FOOTER.TOOLTIP_LABEL"];
                    $scope.privacyPolicyURL = translations["FOOTER.PRIVACY_POLICY_URL"];
                    $scope.faq = translations["FOOTER.FAQ_LABEL"];
                    $scope.faqURL = translations["FOOTER.FAQ_URL"];
                });

                $scope.footerSessionCheck = function($evt, urlType) {
                    if (($evt.type === "mousedown" && $evt.which === 1) || (($evt.type === "keydown" && (window.event.keyCode === 13)) || $evt.type === "click")) {
                        window.isCancelClicked = true;

                        $scope.popup = $window.open("", "_blank");
                        $scope.popup.document.body.innerHTML = "loading ...";
                        DevPortalService.authSessionRequest().then(function(resp) {
                                if (resp.data.statusCode === "200") {
                                    switch (urlType) {
                                        case "privacyPolicy":
                                            var privacyPolicyURL = $scope.privacyPolicyURL;
                                            $scope.popup.location.href = privacyPolicyURL;
                                            break;
                                        case "tnc":
                                            var tncURL = $scope.tncURL;
                                            $scope.popup.location.href = tncURL;
                                            break;
                                        case "help":
                                            var helpURL = $scope.helpURL;
                                            $scope.popup.location.href = helpURL;
                                            break;
                                        case "faq":
                                            var faqURL = $scope.faqURL;
                                            $scope.popup.location.href = faqURL;
                                            break;
                                    }
                                }
                            },
                            function(error) {
                                $scope.errorDataState = error.data.errorCode;
                                if (!$scope.errorDataState || $scope.errorDataState === null) {
                                    $scope.errorData = {};
                                    $scope.errorData.errorCode = "1113";
                                    $scope.errorDataState = $scope.errorData.errorCode;
                                    if ($scope.popup !== null) {
                                        $timeout(function() {
                                            $scope.popup.close();
                                        }, 1000);
                                    }
                                } else {
                                    if ($scope.errorDataState === "1115" || $scope.errorDataState === "1116") {
                                        $scope.errorData = null;
                                        $scope.popup.document.body.innerHTML = "Your session has timed out.";
                                        $timeout(function() {
                                            $scope.popup.close();
                                            SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                                        }, 1000);
                                    } else {
                                        $scope.errorData = {};
                                        $scope.errorData.errorCode = error.data ? $scope.errorDataState : "1113";
                                    }
                                }
                            });
                    }
                };
            }
        };
    }
]);
"use strict";
angular.module("developerHub").directive("pageHeader", ["DevPortalService", "blockUI", "$window", "$uibModal", "$translate", "$state", "config", "SessionPopupService", "$timeout",
    function(DevPortalService, blockUI, $window, $uibModal, $translate, $state, config, SessionPopupService, $timeout) {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "views/page-header.html",
            scope: {
                focuscontent: "&",
                uName:"="
            },
            
            link: function($scope) {
                window.isCancelClicked = false;
                var $ = window.jQuery;
                $scope.username = $("#userName").val();
                $scope.$watch("uName", function() {
                    $scope.username = $("#userName").val();
                });
                $scope.popup = null;
                $scope.errorDataState = "";   
                $scope.usertype = $("#userType").val();
                $scope.portalUserType = config.portalUserType;
                $scope.activePath = $scope.$root.activePath;
                $translate(["HEADER.LOGO_URL", "HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL", "HEADER.HELP_TOOLTIP_LABEL", "HEADER.HEADER_TOOLTIP_LABEL", "HEADER.WELCOME_TOOLTIP_LABEL", "HEADER.PORTAL_TITLE", "NAVIGATION_MENU.GETTING_STARTED_LABEL", "NAVIGATION_MENU.HELP_LABEL","NAVIGATION_MENU.VIEW_PROFILE_LABEL", "NAVIGATION_MENU.WELCOME_LABEL", "NAVIGATION_MENU.CHANGE_PASSWORD_LABEL", "NAVIGATION_MENU.LOGOUT_LABEL", "NAVIGATION_MENU.HELP_URL", "NAVIGATION_MENU.LOGIN_LABEL","NAVIGATION_MENU.API_DOC_LABEL","NAVIGATION_MENU.FAQ_LABEL","NAVIGATION_MENU.FAQ_URL"]).then(function(translations) {
                    $scope.logoPath = translations["HEADER.LOGO_URL"];
                    $scope.logoAlternateLabel = translations["HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                    $scope.developerHubHeader = translations["HEADER.PORTAL_TITLE"];
                    $scope.gettingStarted = translations["NAVIGATION_MENU.GETTING_STARTED_LABEL"];
                    $scope.helpNav = translations["NAVIGATION_MENU.HELP_LABEL"];
                    $scope.welcome = translations["NAVIGATION_MENU.WELCOME_LABEL"];
                    $scope.changePassword = translations["NAVIGATION_MENU.CHANGE_PASSWORD_LABEL"];
                    $scope.viewProfile = translations["NAVIGATION_MENU.VIEW_PROFILE_LABEL"];
                    $scope.logout = translations["NAVIGATION_MENU.LOGOUT_LABEL"];
                    $scope.navHelpURL = translations["NAVIGATION_MENU.HELP_URL"];
                    $scope.helpTt = translations["HEADER.HELP_TOOLTIP_LABEL"];
                    $scope.headerTt = translations["HEADER.HEADER_TOOLTIP_LABEL"];
                    $scope.welcomeHeaderTt = translations["HEADER.WELCOME_TOOLTIP_LABEL"];
                    $scope.login = translations["NAVIGATION_MENU.LOGIN_LABEL"];
                    $scope.apiDoc = translations["NAVIGATION_MENU.API_DOC_LABEL"];
                    $scope.faq = translations["NAVIGATION_MENU.FAQ_LABEL"];
                    $scope.faqURL = translations["NAVIGATION_MENU.FAQ_URL"];
                });
                
                $scope.portalBaseURL = function($event){
                    $event.preventDefault();
                    var portalBaseURL = $("#portalBaseURL").val();
                    $window.location.href = portalBaseURL;
                }
                $scope.mulePublicURL = function($event){
                    $event.preventDefault();
                    var mulePublicURL = $("#mulePublicLaunchURL").val();
                    $scope.popup = $window.open("", "_blank");
                    $scope.popup.document.body.innerHTML = "loading ...";
                    $scope.popup.location.href = mulePublicURL;
                }
                $scope.headerSessionCheck = function($evt, urlType) {
                    $evt.preventDefault();
                    if (($evt.type === "mousedown" && $evt.which === 1) || (($evt.type === "keydown" && (window.event.keyCode === 13)) || $evt.type === "click")) {
                        window.isCancelClicked = true;
                        if (urlType === "help" || urlType === "faq" || urlType === "goToMule") {
                            $scope.popup = $window.open("", "_blank");
                            $scope.popup.document.body.innerHTML = "loading ...";
                        }
                        DevPortalService.authSessionRequest().then(function(resp) {
                                if (resp.data.statusCode === "200") {
                                    switch (urlType) {
                                        case "help":
                                            var navHelpURL = $scope.navHelpURL;
                                            $scope.popup.location.href = navHelpURL;
                                            break;
                                        case "goToMule":
                                            var muleLaunchURL = $("#muleLaunchURL").val();
                                            $scope.popup.location.href = muleLaunchURL;
                                            break;
                                        case "faq":
                                            var faqURL = $scope.faqURL;
                                            $scope.popup.location.href = faqURL;
                                            break;
                                        case "logout":
                                            var logoutURL = $("#logoutURL").val();
                                            $window.location.href = logoutURL;
                                            break;
                                        case "login":
                                            var loginURL = $("#portalBaseURL").val();
                                            $window.location.href = loginURL;
                                            break;
                                        case "gettingStarted":
                                            $state.go("developerHub", {});
                                            break;
                                        case "changePassword":
                                            $state.go("changePassword", {});
                                            break;
                                        case "viewProfile":
                                            $state.go("viewProfile",{}, {reload:true});
                                            break;
                                    }
                                }
                            },
                            function(error) {
                                $scope.errorDataState = error.data.errorCode;
                                if (!$scope.errorDataState || $scope.errorDataState === null) {
                                    $scope.errorData = {};
                                    $scope.errorData.errorCode = "1104";
                                    $scope.errorDataState = $scope.errorData.errorCode;
                                    if ($scope.popup !== null) {
                                        $timeout(function() {
                                            $scope.popup.close();
                                        }, 1000);
                                    }
                                } else {
                                    if ($scope.errorDataState === "1115" || $scope.errorDataState === "1116") {
                                        $scope.errorData = null;
                                        if ($scope.popup !== null) {
                                            $scope.popup.document.body.innerHTML = "Your session has timed out.";
                                            $timeout(function() {
                                                $scope.popup.close();
                                                SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                                            }, 1000);

                                        } else {
                                            SessionPopupService.openSessionPopup($scope, $scope.errorDataState);
                                        }
                                    } else {
                                        $scope.errorData = {};
                                        $scope.errorData.errorCode = error.data ? $scope.errorDataState : "1104";
                                    }
                                }
                            });
                    }
                };
                $scope.skipToContentFocus = function ($event) {
                    $event.preventDefault();
                    $scope.focuscontent();
                };
            }
        };

    }
]);
"use strict";
angular.module("developerHub").service("DevPortalService", ["$http","config","$q", function($http,config,$q) {
   
    return {
        changePasswordRequest: function(reqData) {
            if (reqData) {
         //return $http.get("./js/services/changePasswordResponse.json");
          return $http.post("/auth/changePassword", reqData);
            }
        },
        authSessionRequest: function() {
            if(config.isSessionCheck){
               // return $http.get("./js/services/changePasswordResponse.json");
                return $http.get("/auth/session");
            }else{
                var deferred = $q.defer();
                var response = { data:{statusCode:"200"} };
                deferred.resolve(response);
                return deferred.promise;
            }
        },
        testDataRequest: function () {
            return $http({
               url: "/auth/downloadTestData",
               method: "GET",
               responseType: "arraybuffer"
             });
                // return $http({
                //   url: "./assets/devportal/localization/TestData_v1.xlsx",
                //   method: "GET",
                //   responseType: "arraybuffer"
                //  });
        },
        viewProfileRequest:function(emailId){
      return $http.get("/auth/userProfile?emailId="+emailId);
      //return $http.get("./js/services/viewProfileResponse.json");
           
        },
         editProfileRequest:function(viewProfileData){
       return $http.put("/auth/userProfile", viewProfileData);
     //return $http.get("./js/services/editProfileResponse.json");
        }
    };
}]);



"use strict";
angular.module("developerHub").controller("sessionPopupCtrl", ["$scope", "$uibModalInstance", "$window", "errorData", "$translate", function($scope, $uibModalInstance, $window, errorData, $translate) {
    $scope.okBtnClicked = function() {
        $uibModalInstance.dismiss("cancel");
        var loginURL = "";
        if (errorData === "1115") {
            loginURL = "./";
            $window.location.href = loginURL;
        } else {
            loginURL = $("#logoutURL").val();
            $window.location.href = loginURL;
        }
    };
    $translate(["SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADING", "SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE1", "SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE2", "SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_BUTTON_LABEL"]).then(function(translations) {
        $scope.sessionPopupHeaderLabel = translations["SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_HEADING"];
        $scope.sessionDescText1 = translations["SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE1"];
        $scope.sessionDescText2 = translations["SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE2"];
        $scope.sessionPopupBtnText = translations["SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_BUTTON_LABEL"];
    });
}]);
angular.module("developerHub").service("SessionPopupService", ["$uibModal", "$window", "$translate", function($uibModal, $window, $translate) {
    var openSessionPopup = function($scope, errorData) {
        $uibModal.open({
            templateUrl: "views/session-out-popup.html",
            animation: true,
            backdrop: false,
            keyboard: false,
            windowClass: "pop-confirm",
            resolve: {
                errorData: function() {
                    return errorData;
                }
            },
            controller: "sessionPopupCtrl"
        });
    };

    return {
        openSessionPopup: openSessionPopup
    };
}]);
"use strict";
angular.module("developerHub").factory("fontloaderService", ["$q", "$window", function($q, $window) {
    return {
        isFontLoad: function() {
            var deferred = $q.defer();
            $window.WebFont.load({
                custom: {
                    families: ["FontAwesome", "OpenSans-Bold", "OpenSans-Italic", "OpenSans-Light", "OpenSans-Regular", "OpenSans-Semibold"]
                },
                fontactive: function(familyName, fvd) {
                    deferred.resolve("font loaded");
                }
            });
            return deferred.promise;
        }
    };
}]);
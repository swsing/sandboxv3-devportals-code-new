"use strict";
describe("developerHub.ChangePwdCtrl_test", function() {
    beforeEach(module("developerHub"));

    var $controller,
        $scope,
        q,
        mockDevPortalService,
        mockSessionPopupService,
        deferred,
        config,
        $translate,
        translateDeferred,
        mockBlockUI,
        mockModal,
        mockIsUserType,
        submitData,
        windowObj = { location: { href: "" }, event: { "keyCode": 13 } },
        eventObj,
        errorData = {
            errorCode: '1101',
            errorMessage: 'Service Unavailable.',
            detailErrorMessage: 'Service Unavailable.'
        };

    beforeEach(function() {
        mockModal = {
            result: {
                then: function(confirmCallback, cancelCallback) {
                    this.confirmCallback = confirmCallback;
                    this.cancelCallback = cancelCallback;
                    return this;
                }
            },
            opened: {
                then: function(confirmCallback, cancelCallback) {
                    this.confirmCallback = confirmCallback;
                    this.cancelCallback = cancelCallback;
                    return this;
                }
            },
            close: function() {
                this.opened.confirmCallback(); // covers opened.then success
                this.result.confirmCallback(); // covers result.then success
                this.result.cancelCallback(); // covers result.then error
            },
            open: function(object1) {
                return this;
            },
            dismiss: function() {
                this.opened.confirmCallback(); // covers opened.then success
                this.result.confirmCallback(); // covers result.then success
                this.result.cancelCallback(); // covers result.then error
            },
        };
        module(function($provide) {
            $provide.value("$window", windowObj);
            $provide.value('$uibModal', mockModal);
        });
        mockDevPortalService = {
            changePasswordRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            },
            authSessionRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            }
        };
        mockSessionPopupService = {
            openSessionPopup: function() {
                deferred = q.defer();
                return deferred.promise;
            },
        };
        mockIsUserType = function() {
            deferred = q.defer();
            return deferred.promise;
        }
        mockBlockUI = { start: function() { /**/ }, stop: function() { /**/ } };
        eventObj = jasmine.createSpyObj("eventObj", ["preventDefault"]);
    });

    beforeEach(inject(function($controller, $rootScope, $q, _$window_, DevPortalService, SessionPopupService) {
        $scope = $rootScope.$new();
        q = $q;
        windowObj = _$window_;
        deferred = $q.defer();
        $scope.changePasswordForm = {
            $valid: true,
            $setPristine: function() {},
            $setUntouched: function() {}
        };
        var controller = $controller("ChangePwdCtrl", { $scope: $scope, $window: windowObj, blockUI: mockBlockUI, DevPortalService: mockDevPortalService, $uibModal: mockModal, isUserType: mockIsUserType, SessionPopupService: mockSessionPopupService });
    }));

    describe("test cookie disabled", function() {
        it('cookei disabled', inject(function($state, $q) {
            spyOnProperty(navigator, "cookieEnabled").and.returnValue(false);
            $scope.init();
            expect($scope.isCookieDisabled).toEqual(true);
        }))
    });
    describe("test changePassword happy flow", function() {
        it('changePassword route', inject(function($state, $q) {
            $state.go('changePassword');
        }));
        it("changePassword", function() {
            inject(function(DevPortalService, blockUI, $uibModal) {
                $scope.passwords.currentPassword = "p@ssw0rD";
                $scope.passwords.newPassword = "p@ssw0rD123";
                var payload = { oldPwd: "p@ssw0rD", newPwd: "p@ssw0rD123" };
                spyOn($scope, "resetFields");
                spyOn($scope, "confirmationPopup");
                spyOn(DevPortalService, "changePasswordRequest").and.returnValue({});
                spyOn($uibModal, 'open').and.returnValue(mockModal);;
                $scope.changePasswordFn();
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                $scope.$digest();
                expect($scope.resetFields).toHaveBeenCalledTimes(1);
                expect($scope.confirmationPopup).toHaveBeenCalledTimes(1);
                //expect(mockModal.open).toHaveBeenCalled();
            });
        });
        it("confirmationPopup", function() {
            inject(function($controller, DevPortalService, $window, blockUI, $uibModal) {
                $controller("confirmationPopupCtrl", { $scope: $scope, $uibModalInstance: mockModal, $window: windowObj });
                spyOn($uibModal, 'open').and.returnValue({});
                spyOn($uibModal, 'dismiss').and.returnValue({});
                $scope.confirmationPopup();
                //expect($scope.confirmationPopup).toHaveBeenCalledTimes(1);
                expect($uibModal.open).toHaveBeenCalled();
                $scope.cancelBtnClicked();
                expect($uibModal.dismiss).toHaveBeenCalled();
                $scope.okBtnClicked();
                expect($uibModal.dismiss).toHaveBeenCalled();
            });
        });
    });
    describe("test changePassword error flow", function() {
        it("changePassword branch if", function() {
            inject(function(DevPortalService, blockUI, SessionPopupService) {
                $scope.passwords.currentPassword = "p@ssw0rD";
                $scope.passwords.newPassword = "p@ssw0rD123";

                spyOn($scope, "resetFields");
                spyOn(SessionPopupService, "openSessionPopup");
                spyOn(DevPortalService, "changePasswordRequest").and.returnValue({});
                $scope.changePasswordFn();
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1108',
                        errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                        detailErrorMessage: ''
                    }
                });
                $scope.$digest();
                expect($scope.resetFields).toHaveBeenCalledTimes(1);
                // expect($scope.sessionPopup).toHaveBeenCalledTimes(1);
            });
        });
        it("changePassword branch else", function() {
            inject(function(DevPortalService, blockUI) {
                $scope.passwords.currentPassword = "p@ssw0rD";
                $scope.passwords.newPassword = "p@ssw0rD123";
                spyOn($scope, "resetFields");
                spyOn(DevPortalService, "changePasswordRequest").and.returnValue({});
                $scope.changePasswordFn();
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1101',
                        errorMessage: 'You are not authorized to use this service.',
                        detailErrorMessage: ''
                    }
                });
                $scope.$digest();
                expect($scope.resetFields).toHaveBeenCalledTimes(1);
                expect($scope.errorData.errorCode).toBe(errorData.errorCode);

            });
        });
        it("changePassword branch else if no data", function() {
            inject(function(DevPortalService, blockUI) {
                $scope.passwords.currentPassword = "p@ssw0rD";
                $scope.passwords.newPassword = "p@ssw0rD123";
                spyOn($scope, "resetFields");
                spyOn(DevPortalService, "changePasswordRequest").and.returnValue({});
                $scope.changePasswordFn();
                deferred.reject({
                    status: 401,
                    data: {}
                });
                $scope.$digest();
                expect($scope.resetFields).toHaveBeenCalledTimes(1);
                expect($scope.errorData.errorCode).toBe("1104");
                expect($scope.errorDataState).toBe($scope.errorData.errorCode);

            });
        });
    });
    describe("test cancelClicked happy flow", function() {
        it("cancelClicked", function() {
            inject(function(DevPortalService, blockUI, $state, $window) {
                spyOn(DevPortalService, "authSessionRequest").and.returnValue({});
                spyOn($state, "go");
                window.event = { "keyCode": 13 };
                $scope.cancelClicked(eventObj, 'gettingStarted', 'keydown');
                //expect(eventObj.preventDefault).toHaveBeenCalled();
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                $scope.$digest();
                //expect(DevPortalService.authSessionRequest).toHaveBeenCalled();
                expect($state.go).toHaveBeenCalledWith("developerHub", {});
            });
        });
    });
    describe("test cancelClicked error flow", function() {
        it("cancelClicked branch if", function() {
            inject(function(DevPortalService, blockUI, SessionPopupService) {
                spyOn(SessionPopupService, "openSessionPopup");
                $scope.cancelClicked(eventObj, "gettingStarted");
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1108',
                        errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                        detailErrorMessage: ''
                    }
                });
                $scope.$digest();
                expect(eventObj.preventDefault).toHaveBeenCalled();
            });
        });
        it("cancelClicked branch else", function() {
            inject(function(DevPortalService, blockUI) {
                $scope.cancelClicked(eventObj, "gettingStarted");
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1101',
                        errorMessage: 'You are not authorized to use this service.',
                        detailErrorMessage: ''
                    }
                });
                $scope.$digest();
                expect($scope.errorData.errorCode).toBe(errorData.errorCode);
                expect(eventObj.preventDefault).toHaveBeenCalled();
            });
        });
        it("changePassword branch else if no data", function() {
            inject(function(DevPortalService, blockUI) {
                $scope.cancelClicked(eventObj, "gettingStarted");
                deferred.reject({
                    status: 401,
                    data: {}
                });
                $scope.$digest();
                expect($scope.errorData.errorCode).toBe("1104");
                expect($scope.errorDataState).toBe($scope.errorData.errorCode);
                expect(eventObj.preventDefault).toHaveBeenCalled();
            });
        });
    });
    describe("test resetFields", function() {
        it("resetFields", function() {
            inject(function(blockUI) {
                spyOn(blockUI, "stop").and.returnValue({});
                $scope.resetFields();
                expect($scope.passwords.currentPassword).toEqual("");
                expect($scope.passwords.newPassword).toEqual("");
                expect($scope.passwords.confirmPassword).toEqual("");
            });
        });
    });
    describe("test focusMainContent  error flow", function() {
        it("focusMainContent", function() {
            var fixture = '<input id="currentPassword" type="password" maxlength="50" name="currentPassword" placeholder={{currentPasswordLabel}} ng-model="passwords.currentPassword" />';
            document.body.insertAdjacentHTML(
                'afterbegin',
                fixture);
            $scope.focusMainContent();
        });
    });
});
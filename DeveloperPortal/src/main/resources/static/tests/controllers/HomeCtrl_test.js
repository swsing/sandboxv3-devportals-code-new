"use strict";
describe("developerHub.HomeCtrl_test", function() {
    beforeEach(module("developerHub"));
    var $controller,
        $scope,
        q,
        mockDevPortalService,
        mockSessionPopupService,
        mockFontResolved,
        deferred,
        config,
        $translate,
        translateDeferred,
        mockBlockUI,
        submitData,
        windowObj = {
            location: { href: "" },
            open: function(url, target) {
                return {
                    document: {
                        body: { /**/ }
                    },
                    location: {
                        href: url
                    },
                    close: function() {
                        /**/
                    }
                }
            },

        },
        errorData = {
            errorCode: '1102',
            errorMessage: 'You are not authorized to use this service.',
            detailErrorMessage: ''
        };


    beforeEach(function() {
        mockDevPortalService = {
            authSessionRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            }
        };
        mockSessionPopupService = {
            openSessionPopup: function() {
                deferred = q.defer();
                return deferred.promise;
            },
        };
        mockFontResolved = {
            isFontLoad: function() {
                deferred = q.defer();
                return deferred.promise;
            }
        };
        module(function($provide) {
            $provide.value("$window", windowObj);
        });
        mockBlockUI = { start: function() { /**/ }, stop: function() { /**/ } };

    });
    describe("test cookie disabled", function() {
        it('cookei disabled', inject(function($state, $q) {
            spyOnProperty(navigator, "cookieEnabled").and.returnValue(false);
            $scope.init();
            expect($scope.isCookieDisabled).toEqual(true);
        }))
    });
    beforeEach(inject(function($controller, $rootScope, $q, _$window_, DevPortalService, SessionPopupService) {
        $scope = $rootScope.$new();
        q = $q;
        windowObj = _$window_;
        deferred = $q.defer();
        var controller = $controller("HomeCtrl", { $scope: $scope, $window: windowObj, blockUI: mockBlockUI, DevPortalService: mockDevPortalService, fontResolved: mockFontResolved, SessionPopupService: mockSessionPopupService });
    }));
    describe("test init ", function() {
        it('init', inject(function() {}));
    });
    describe("test homeSessionCheck happy flow", function() {
        it('developerHub route', inject(function($state, $q) {
            $state.go('developerHub');
        }));
        it("homeSessionCheck goToMule", function() {
            inject(function(DevPortalService, blockUI, $window) {
                var fixture = '<input type="hidden" name="muleLaunchURL" id="muleLaunchURL" value="http://localhost:9876/muleLaunch">';
                document.body.insertAdjacentHTML(
                    'afterbegin',
                    fixture);
                spyOn($window, 'open').and.callThrough();
                spyOn(DevPortalService, "authSessionRequest").and.returnValue({});
                $scope.homeSessionCheck("goToMule");
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                $scope.$digest();
                expect($scope.popup.location.href).toEqual("http://localhost:9876/muleLaunch");
            });
        });
        it("homeSessionCheck downloadTestData", function() {
            inject(function(DevPortalService, blockUI, $window) {
                spyOn(DevPortalService, "authSessionRequest").and.returnValue({});
                $scope.homeSessionCheck("downloadTestData");
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                $scope.$digest();
                expect(windowObj.location.href).toEqual("/auth/downloadTestData");
            });
        });

    });
    describe("test homeSessionCheck error flow", function() {
        it("homeSessionCheck homeSessionPopup", function() {
            inject(function(DevPortalService, blockUI, SessionPopupService, $window, $timeout) {
                var fixture = '<input type="hidden" name="muleLaunchURL" id="muleLaunchURL" value="http://localhost:9876/muleLaunch">';
                document.body.insertAdjacentHTML(
                    'afterbegin',
                    fixture);
                spyOn($window, 'open').and.callThrough();

                spyOn(SessionPopupService, "openSessionPopup");
                spyOn(DevPortalService, "authSessionRequest").and.returnValue({});
                $scope.homeSessionCheck("goToMule");
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1108',
                        errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                        detailErrorMessage: ''
                    }
                });
                $scope.$digest();
                expect($scope.popup.document.body.innerHTML).toBe("Your session has timed out.");
                // $timeout.flush(1000);
                // expect(SessionPopupService.openSessionPopup).toHaveBeenCalled();
            });
        });
        it("homeSessionCheck homeSessionPopup no popup", function() {
            inject(function(DevPortalService, blockUI, SessionPopupService, $window, $timeout) {
                var fixture = '<input type="hidden" name="muleLaunchURL" id="muleLaunchURL" value="http://localhost:9876/muleLaunch">';
                document.body.insertAdjacentHTML(
                    'afterbegin',
                    fixture);
                spyOn($window, 'open').and.callThrough();

                spyOn(SessionPopupService, "openSessionPopup");
                spyOn(DevPortalService, "authSessionRequest").and.returnValue({});
                $scope.homeSessionCheck("downloadTestData");
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1108',
                        errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                        detailErrorMessage: ''
                    }
                });
                $scope.$digest();
            });
        });
        it("homeSessionCheck other error", function() {
            inject(function(DevPortalService, blockUI) {
                spyOn(DevPortalService, "authSessionRequest").and.returnValue({});
                $scope.homeSessionCheck("downloadTestData");
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1102',
                        errorMessage: 'You are not authorized to use this service.',
                        detailErrorMessage: ''
                    }
                });
                $scope.$digest();
                expect($scope.errorData.errorCode).toBe(errorData.errorCode);
            });
        });
        it("homeSessionCheck branch else if no data", function() {
            inject(function(DevPortalService, blockUI) {
                spyOn(DevPortalService, "authSessionRequest").and.returnValue({});
                $scope.homeSessionCheck("downloadTestData");
                deferred.reject({
                    status: 401,
                    data: {}
                });
                $scope.$digest();
                expect($scope.errorData.errorCode).toBe("1104");
                expect($scope.errorDataState).toBe($scope.errorData.errorCode);

            });
        });
        it("homeSessionCheck branch else if no data with popup", function() {
            inject(function(DevPortalService, blockUI, $window, $timeout) {
                spyOn(DevPortalService, "authSessionRequest").and.returnValue({});
                spyOn($window, 'open').and.callThrough();
                $scope.homeSessionCheck("goToMule");
                deferred.reject({
                    status: 401,
                    data: {}
                });
                $scope.$digest();
                // $timeout.flush(1000);
                // expect($scope.popup.close).toHaveBeenCalled();
                // expect($timeout.verifyNoPendingTasks).toThrow();

                // expect($scope.errorData.errorCode).toBe("1104");
                // expect($scope.errorDataState).toBe($scope.errorData.errorCode);
            });
        });
    });
    describe("test focusMainContent  error flow", function() {
        it("focusMainContent", function() {
            var fixture = '<button type="button" role="button" id="downloadTestDataButton" class="btn down-btn">';
            document.body.insertAdjacentHTML(
                'afterbegin',
                fixture);
            $scope.focusMainContent();
        });
    });
});
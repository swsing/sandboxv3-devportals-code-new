"use strict";
describe("developerHub.pageHeader_test", function() {
    beforeEach(module("developerHubTemplates", "developerHub"));
    var compile,
        scope,
        q,
        element,
        mockDevPortalService,
        mockSessionPopupService,
        deferred,
        mockBlockUI,
        mockModal,
        $mockTranslate,
        translateDeferred,
        controller,
        windowObj = {
            location: { href: "" },
            open: function(url, target) {
                return {
                    document: {
                        body: { /**/ }
                    },
                    location: {
                        href: url
                    }
                }
            }
        },
        errorData = {
            errorCode: '1101',
            errorMessage: 'Service Unavailable.',
            detailErrorMessage: 'Service Unavailable.'
        };
    beforeEach(function() {
        mockDevPortalService = {
            authSessionRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            }
        };
        mockSessionPopupService = {
            openSessionPopup: function() {
                deferred = q.defer();
                return deferred.promise;
            },
        };
        mockBlockUI = { start: function() { /**/ }, stop: function() { /**/ } };
        mockModal = {
            result: {
                then: function(confirmCallback, cancelCallback) {
                    this.confirmCallback = confirmCallback;
                    this.cancelCallback = cancelCallback;
                    return this;
                }
            },
            opened: {
                then: function(confirmCallback, cancelCallback) {
                    this.confirmCallback = confirmCallback;
                    this.cancelCallback = cancelCallback;
                    return this;
                }
            },
            close: function() {
                this.opened.confirmCallback(); // covers opened.then success
                this.result.confirmCallback(); // covers result.then success
                this.result.cancelCallback(); // covers result.then error
            },
            open: function(object1) {
                return this;
            },
            dismiss: function() {
                this.opened.confirmCallback(); // covers opened.then success
                this.result.confirmCallback(); // covers result.then success
                this.result.cancelCallback(); // covers result.then error
            },
        };

        module(function($provide) {
            $provide.value('$uibModal', mockModal);
        });
    });

    beforeEach(inject(function($rootScope, $compile, $q, $window, DevPortalService, $translate, SessionPopupService) {
        // jasmine.getFixtures().fixturesPath = "base/tests/fixtures/";  
        // loadFixtures("index.html");    
        q = $q;
        deferred = $q.defer();
        scope = $rootScope.$new();
        element = angular.element("<page-HEADER></page-HEADER>");
        translateDeferred = q.defer();
        $mockTranslate = jasmine.createSpy($translate).and.returnValue(translateDeferred.promise);
        spyOn(DevPortalService, "authSessionRequest").and.returnValue(deferred.promise);
        spyOn(SessionPopupService, "openSessionPopup");
        $compile(element)(scope);
        scope.$digest();
    }));

    xdescribe("test template", function() {
        it("should have placeholder for logo in template", function() {
            //expect($(".navbar-brand").length).toEqual(1);
        });
    });

    describe("test headerSessionCheck happy flow", function() {
        it("headerSessionCheck help", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate) {
                translateDeferred.resolve({
                    "HEADER.LOGO_URL": "assets/devportal/img/boi_logo.svg",
                    "HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL": "Bank of Ireland logo",
                    "HEADER.HELP_TOOLTIP_LABEL": "Link will open in a new window.",
                    "HEADER.PORTAL_TITLE": "Developer Hub",
                    "NAVIGATION_MENU.GETTING_STARTED_LABEL": "Getting Started",
                    "NAVIGATION_MENU.HELP_LABEL": "Help",
                    "NAVIGATION_MENU.WELCOME_LABEL": "Welcome",
                    "NAVIGATION_MENU.CHANGE_PASSWORD_LABEL": "Change Password",
                    "NAVIGATION_MENU.LOGOUT_LABEL": "Logout",
                    "NAVIGATION_MENU.HELP_URL": "https://www.bankofireland.com/developer/help"
                });
                spyOn($window, 'open').and.callThrough();
                element.isolateScope().headerSessionCheck("help");
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                scope.$digest();
                expect($window.open).toHaveBeenCalled();
            });
        });
        xit("headerSessionCheck logout", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate) {
                var fixture = '<input type="hidden" name="logoutURL" id="logoutURL" value="http://localhost:9876/logout">';
                document.body.insertAdjacentHTML(
                    'afterbegin',
                    fixture);
                element.isolateScope().headerSessionCheck("logout");
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                scope.$digest();
                expect(windowObj.location.href).toEqual("http://localhost:9876/logout");
            });
        });
        it("headerSessionCheck gettingStarted", function() {
            inject(function(DevPortalService, blockUI, $window, $translate, $state) {
                translateDeferred.resolve({
                    "HEADER.LOGO_URL": "assets/devportal/img/boi_logo.svg",
                    "HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL": "Bank of Ireland logo",
                    "HEADER.HELP_TOOLTIP_LABEL": "Link will open in a new window.",
                    "HEADER.PORTAL_TITLE": "Developer Hub",
                    "NAVIGATION_MENU.GETTING_STARTED_LABEL": "Getting Started",
                    "NAVIGATION_MENU.HELP_LABEL": "Help",
                    "NAVIGATION_MENU.WELCOME_LABEL": "Welcome",
                    "NAVIGATION_MENU.CHANGE_PASSWORD_LABEL": "Change Password",
                    "NAVIGATION_MENU.LOGOUT_LABEL": "Logout",
                    "NAVIGATION_MENU.HELP_URL": "https://www.bankofireland.com/developer/help"
                });
                spyOn($state, "go");
                element.isolateScope().headerSessionCheck("gettingStarted");
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                scope.$digest();
                expect($state.go).toHaveBeenCalledWith("developerHub", {});
            });
        });
        it("headerSessionCheck changePassword", function() {
            inject(function(DevPortalService, blockUI, $window, $translate, $state) {
                translateDeferred.resolve({
                    "HEADER.LOGO_URL": "assets/devportal/img/boi_logo.svg",
                    "HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL": "Bank of Ireland logo",
                    "HEADER.HELP_TOOLTIP_LABEL": "Link will open in a new window.",
                    "HEADER.PORTAL_TITLE": "Developer Hub",
                    "NAVIGATION_MENU.GETTING_STARTED_LABEL": "Getting Started",
                    "NAVIGATION_MENU.HELP_LABEL": "Help",
                    "NAVIGATION_MENU.WELCOME_LABEL": "Welcome",
                    "NAVIGATION_MENU.CHANGE_PASSWORD_LABEL": "Change Password",
                    "NAVIGATION_MENU.LOGOUT_LABEL": "Logout",
                    "NAVIGATION_MENU.HELP_URL": "https://www.bankofireland.com/developer/help"
                });
                spyOn($state, "go");
                element.isolateScope().headerSessionCheck("changePassword");
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                scope.$digest();
                expect($state.go).toHaveBeenCalledWith("changePassword", {});
            });
        });

    });
    describe("test headerSessionCheck error flow", function() {
        it("headerSessionCheck HEADERSessionPopup", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate, $uibModal) {
                translateDeferred.resolve({
                    "HEADER.LOGO_URL": "imgassets/devportal/boi_logo.svg",
                    "HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL": "Bank of Ireland logo",
                    "HEADER.HELP_TOOLTIP_LABEL": "Link will open in a new window.",
                    "HEADER.PORTAL_TITLE": "Developer Hub",
                    "NAVIGATION_MENU.GETTING_STARTED_LABEL": "Getting Started",
                    "NAVIGATION_MENU.HELP_LABEL": "Help",
                    "NAVIGATION_MENU.WELCOME_LABEL": "Welcome",
                    "NAVIGATION_MENU.CHANGE_PASSWORD_LABEL": "Change Password",
                    "NAVIGATION_MENU.LOGOUT_LABEL": "Logout",
                    "NAVIGATION_MENU.HELP_URL": "https://www.bankofireland.com/developer/help"
                });
                // spyOn($window, 'open').and.callThrough();
                element.isolateScope().headerSessionCheck("help");
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1108',
                        errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                        detailErrorMessage: ''
                    }
                });
                scope.$digest();
                expect(element.isolateScope().popup.document.body.innerHTML).toBe("Your session has timed out.");
            });
        });
        it("headerSessionCheck HEADERSessionPopup", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate, $uibModal) {
                translateDeferred.resolve({
                    "HEADER.LOGO_URL": "assets/devportal/img/boi_logo.svg",
                    "HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL": "Bank of Ireland logo",
                    "HEADER.HELP_TOOLTIP_LABEL": "Link will open in a new window.",
                    "HEADER.PORTAL_TITLE": "Developer Hub",
                    "NAVIGATION_MENU.GETTING_STARTED_LABEL": "Getting Started",
                    "NAVIGATION_MENU.HELP_LABEL": "Help",
                    "NAVIGATION_MENU.WELCOME_LABEL": "Welcome",
                    "NAVIGATION_MENU.CHANGE_PASSWORD_LABEL": "Change Password",
                    "NAVIGATION_MENU.LOGOUT_LABEL": "Logout",
                    "NAVIGATION_MENU.HELP_URL": "https://www.bankofireland.com/developer/help"
                });
                // spyOn($window, 'open').and.callThrough();
                element.isolateScope().headerSessionCheck("logout");
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1108',
                        errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                        detailErrorMessage: ''
                    }
                });
                scope.$digest();
            });
        });
        it("homeSessionCheck other error", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate) {
                translateDeferred.resolve({
                    "HEADER.LOGO_URL": "assets/devportal/img/boi_logo.svg",
                    "HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL": "Bank of Ireland logo",
                    "HEADER.HELP_TOOLTIP_LABEL": "Link will open in a new window.",
                    "HEADER.PORTAL_TITLE": "Developer Hub",
                    "NAVIGATION_MENU.GETTING_STARTED_LABEL": "Getting Started",
                    "NAVIGATION_MENU.HELP_LABEL": "Help",
                    "NAVIGATION_MENU.WELCOME_LABEL": "Welcome",
                    "NAVIGATION_MENU.CHANGE_PASSWORD_LABEL": "Change Password",
                    "NAVIGATION_MENU.LOGOUT_LABEL": "Logout",
                    "NAVIGATION_MENU.HELP_URL": "https://www.bankofireland.com/developer/help"
                });
                element.isolateScope().headerSessionCheck("goToMule");
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1101',
                        errorMessage: 'You are not authorized to use this service.',
                        detailErrorMessage: ''
                    }
                });
                scope.$digest();
                expect(element.isolateScope().errorData.errorCode).toBe(errorData.errorCode);
            });
        });
        it("homeSessionCheck branch else if no data", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate) {
                element.isolateScope().headerSessionCheck("help");
                deferred.reject({
                    status: 401,
                    data: {}
                });
                scope.$digest();
                expect(element.isolateScope().errorData.errorCode).toBe("1104");
                expect(element.isolateScope().errorDataState).toBe(element.isolateScope().errorData.errorCode);
            });
        });
    });
})
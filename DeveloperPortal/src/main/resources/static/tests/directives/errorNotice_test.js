"use strict";
describe("developerHub.errorNotice_test", function() {
    beforeEach(module("developerHubTemplates", "developerHub"));
    var compile, scope, element, errorData;

    beforeEach(inject(function($rootScope, $compile) {
        
        scope = $rootScope.$new();
        scope.errorData = {
                errorCode: '1104',
                errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                detailErrorMessage: ''
            }; 
        element = angular.element("<error-notice error-data='errorData'></error-notice>");
       
        $compile(element)(scope);
        scope.$digest();
    }));

    describe("test template", function() {
        it("should bind the errorData to template", function() {
            element.isolateScope().errorData = {
                errorCode: '1104',
                errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                detailErrorMessage: ''
            };
            scope.$digest();
            expect(scope.errorData).toBe(element.isolateScope().errorData);
        });
    });
});
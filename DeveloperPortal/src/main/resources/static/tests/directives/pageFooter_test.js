"use strict";
describe("developerHub.pageFooter_test", function() {
    beforeEach(module("developerHubTemplates", "developerHub"));
    var compile,
        scope,
        $scope,
        q,
        element,
        mockDevPortalService,
        mockSessionPopupService,
        deferred,
        mockBlockUI,
        mockModal,
        windowObj,
        $mockTranslate,
        translateDeferred,
        controller,
        errorData = {
            errorCode: '1101',
            errorMessage: 'Service Unavailable.',
            detailErrorMessage: 'Service Unavailable.'
        };
    beforeEach(function() {
        mockDevPortalService = {
            authSessionRequest: function() {
                deferred = q.defer();
                return deferred.promise;
            }
        };
        mockSessionPopupService = {
            openSessionPopup: function() {
                deferred = q.defer();
                return deferred.promise;
            },
        };
        mockBlockUI = { start: function() { /**/ }, stop: function() { /**/ } };
        mockModal = {
            result: {
                then: function(confirmCallback, cancelCallback) {
                    this.confirmCallback = confirmCallback;
                    this.cancelCallback = cancelCallback;
                    return this;
                }
            },
            opened: {
                then: function(confirmCallback, cancelCallback) {
                    this.confirmCallback = confirmCallback;
                    this.cancelCallback = cancelCallback;
                    return this;
                }
            },
            close: function() {
                this.opened.confirmCallback(); // covers opened.then success
                this.result.confirmCallback(); // covers result.then success
                this.result.cancelCallback(); // covers result.then error
            },
            open: function(object1) {
                return this;
            },
            dismiss: function() {
                this.opened.confirmCallback(); // covers opened.then success
                this.result.confirmCallback(); // covers result.then success
                this.result.cancelCallback(); // covers result.then error
            },
        };
        windowObj = {
            location: { href: "" },
            open: function(url, target) {
                return {
                    document: {
                        body: { /**/ }
                    },
                    location: {
                        href: url
                    }
                }
            }
        };
        module(function($provide) {
            $provide.value('$uibModal', mockModal);
        });
        // module(function($provide) {
        //     $provide.value('$window', windowObj);
        // });
    });
    beforeEach(inject(function($rootScope, $compile, $q, $window, DevPortalService, $translate, SessionPopupService) {
        q = $q;
        deferred = $q.defer();
        scope = $rootScope.$new();
        element = angular.element("<page-footer></page-footer>");
        translateDeferred = q.defer();
        $mockTranslate = jasmine.createSpy($translate).and.returnValue(translateDeferred.promise);
        spyOn(DevPortalService, "authSessionRequest").and.returnValue(deferred.promise);
        spyOn(SessionPopupService, "openSessionPopup");
        spyOn($window, 'open').and.callThrough();
        $compile(element)(scope);
        scope.$digest();
    }));

    describe("test template", function() {
        it("should have CSS classes in template", function() {
            expect(element.hasClass("footer-bg")).toBeFalsy();
        });
    });

    describe("test footerSessionCheck happy flow", function() {
        it("footerSessionCheck privacyPolicy", function() {
            inject(function(DevPortalService, blockUI, $window, $translate) {
                translateDeferred.resolve({
                    "FOOTER.PRIVACY_POLICY_LABEL": "Cookie and Privacy policy",
                    "FOOTER.TNC_LABEL": "Terms of use",
                    "FOOTER.HELP_LABEL": "Help",
                    "FOOTER.TOOLTIP_LABEL": "Link will open in a new window.",
                    "FOOTER.PRIVACY_POLICY_URL": "https://www.bankofireland.com/legal/privacy-statement",
                    "FOOTER.TNC_URL": "https://www.bankofireland.com/developer/termsofuse",
                    "FOOTER.HELP_URL": "https://www.bankofireland.com/developer/help",
                    "FOOTER.REGULATORY_LABEL": "Bank of Ireland (UK) plc is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority."
                });
                element.isolateScope().footerSessionCheck("privacyPolicy");
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                scope.$digest();
                expect($window.open).toHaveBeenCalled();
            });
        });
        it("footerSessionCheck tnc", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate) {
                translateDeferred.resolve({
                    "FOOTER.PRIVACY_POLICY_LABEL": "Cookie and Privacy policy",
                    "FOOTER.TNC_LABEL": "Terms of use",
                    "FOOTER.HELP_LABEL": "Help",
                    "FOOTER.TOOLTIP_LABEL": "Link will open in a new window.",
                    "FOOTER.PRIVACY_POLICY_URL": "https://www.bankofireland.com/legal/privacy-statement",
                    "FOOTER.TNC_URL": "https://www.bankofireland.com/developer/termsofuse",
                    "FOOTER.HELP_URL": "https://www.bankofireland.com/developer/help",
                    "FOOTER.REGULATORY_LABEL": "Bank of Ireland (UK) plc is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority."
                });
                element.isolateScope().footerSessionCheck("tnc");
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                scope.$digest();
                expect($window.open).toHaveBeenCalled();
            });
        });
        it("footerSessionCheck help", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate) {
                translateDeferred.resolve({
                    "FOOTER.PRIVACY_POLICY_LABEL": "Cookie and Privacy policy",
                    "FOOTER.TNC_LABEL": "Terms of use",
                    "FOOTER.HELP_LABEL": "Help",
                    "FOOTER.TOOLTIP_LABEL": "Link will open in a new window.",
                    "FOOTER.PRIVACY_POLICY_URL": "https://www.bankofireland.com/legal/privacy-statement",
                    "FOOTER.TNC_URL": "https://www.bankofireland.com/developer/termsofuse",
                    "FOOTER.HELP_URL": "https://www.bankofireland.com/developer/help",
                    "FOOTER.REGULATORY_LABEL": "Bank of Ireland (UK) plc is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority."
                });
                element.isolateScope().footerSessionCheck("help");
                deferred.resolve({ status: 200, data: { statusCode: "200" } });
                scope.$digest();
                expect($window.open).toHaveBeenCalled();
            });
        });
    });
    describe("test footerSessionCheck error flow", function() {
        it("footerSessionCheck footerSessionPopup", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate) {
                translateDeferred.resolve({
                    "FOOTER.PRIVACY_POLICY_LABEL": "Cookie and Privacy policy",
                    "FOOTER.TNC_LABEL": "Terms of use",
                    "FOOTER.HELP_LABEL": "Help",
                    "FOOTER.TOOLTIP_LABEL": "Link will open in a new window.",
                    "FOOTER.PRIVACY_POLICY_URL": "https://www.bankofireland.com/legal/privacy-statement",
                    "FOOTER.TNC_URL": "https://www.bankofireland.com/developer/termsofuse",
                    "FOOTER.HELP_URL": "https://www.bankofireland.com/developer/help",
                    "FOOTER.REGULATORY_LABEL": "Bank of Ireland (UK) plc is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority."
                });
                element.isolateScope().footerSessionCheck("help");
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1108',
                        errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                        detailErrorMessage: ''
                    }
                });
                scope.$digest();
                expect(element.isolateScope().popup.document.body.innerHTML).toBe("Your session has timed out.");
            });
        });
        it("homeSessionCheck other error", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate) {
                translateDeferred.resolve({
                    "FOOTER.PRIVACY_POLICY_LABEL": "Cookie and Privacy policy",
                    "FOOTER.TNC_LABEL": "Terms of use",
                    "FOOTER.HELP_LABEL": "Help",
                    "FOOTER.TOOLTIP_LABEL": "Link will open in a new window.",
                    "FOOTER.PRIVACY_POLICY_URL": "https://www.bankofireland.com/legal/privacy-statement",
                    "FOOTER.TNC_URL": "https://www.bankofireland.com/developer/termsofuse",
                    "FOOTER.HELP_URL": "https://www.bankofireland.com/developer/help",
                    "FOOTER.REGULATORY_LABEL": "Bank of Ireland (UK) plc is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority."
                });
                element.isolateScope().footerSessionCheck("goToMule");
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1101',
                        errorMessage: 'You are not authorized to use this service.',
                        detailErrorMessage: ''
                    }
                });
                scope.$digest();
                expect(element.isolateScope().errorData.errorCode).toBe(errorData.errorCode);
            });
        });
        it("homeSessionCheck branch else if no data", function() {
            inject(function(_$controller_, DevPortalService, blockUI, $window, $translate) {
                element.isolateScope().footerSessionCheck("goToMule");
                deferred.reject({
                    status: 401,
                    data: {}
                });
                scope.$digest();
                expect(element.isolateScope().errorData.errorCode).toBe("1104");
                expect(element.isolateScope().errorDataState).toBe(element.isolateScope().errorData.errorCode);
            });
        });
    });
});
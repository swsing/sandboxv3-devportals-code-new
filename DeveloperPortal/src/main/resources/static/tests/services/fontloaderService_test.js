"use strict";
describe("developerHub.fontloaderService_test", function() {

    beforeEach(module("developerHub"));

    var service, deferred, output;
    var windowObj = { WebFont: { load: function() {} } };

    beforeEach(function() {
        module(function($provide) {
            $provide.value("$window", windowObj);
        });
    });
    beforeEach(inject(function($injector) {
        $injector.get("fontloaderService");
        service = $injector.get("fontloaderService");
    }));

    describe("test isFontLoad", function() {
        it("test load function", function() {
            inject(function($q) {
                deferred = $q.defer();
                output = service.isFontLoad();
                deferred.resolve("font loaded");
                //expect(output).toBe("output");
            });
        });
    });
});
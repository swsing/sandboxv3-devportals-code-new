"use strict";
describe("developerHub.SessionPopupService_test", function() {

    beforeEach(module("developerHub"));

    var $scope,
        mockModal,
        service,
        q,
        deferred,
        translateDeferred,
        $mockTranslate,
        windowObj = {
            location: { href: "" },
            open: function(url, target) {
                return {
                    document: {
                        body: { /**/ }
                    },
                    location: {
                        href: url
                    }
                }
            }
        },
        errorCode = "1108",
        errorCodeElse = "1110";


    beforeEach(function() {
        mockModal = {
            result: {
                then: function(confirmCallback, cancelCallback) {
                    this.confirmCallback = confirmCallback;
                    this.cancelCallback = cancelCallback;
                    return this;
                }
            },
            opened: {
                then: function(confirmCallback, cancelCallback) {
                    this.confirmCallback = confirmCallback;
                    this.cancelCallback = cancelCallback;
                    return this;
                }
            },
            close: function() {
                this.opened.confirmCallback(); // covers opened.then success
                this.result.confirmCallback(); // covers result.then success
                this.result.cancelCallback(); // covers result.then error
            },
            open: function(object1) {
                return this;
            },
            dismiss: function() {
                this.opened.confirmCallback(); // covers opened.then success
                this.result.confirmCallback(); // covers result.then success
                this.result.cancelCallback(); // covers result.then error
            },
        };
        module(function($provide) {
            $provide.value('$uibModal', mockModal);
        });

        inject(function($injector) {
            service = $injector.get("SessionPopupService");
        });
    });

    beforeEach(inject(function($rootScope, $window, $translate, $q) {
        $scope = $rootScope.$new();
        q = $q;
        deferred = $q.defer();
        translateDeferred = q.defer();
        $mockTranslate = jasmine.createSpy($translate).and.returnValue(translateDeferred.promise);

    }));
    describe("test SessionPopupService flow", function() {
        it("SessionPopupService", function() {
            inject(function(_$controller_, $translate, $uibModal) {
                _$controller_("sessionPopupCtrl", { $scope: $scope, $uibModalInstance: mockModal, $window: windowObj, errorData: errorCode });
                spyOn($uibModal, 'open');
                spyOn($uibModal, 'dismiss').and.returnValue({});
                service.openSessionPopup();
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1108',
                        errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                        detailErrorMessage: ''
                    }
                });
                $scope.$digest();
                $scope.okBtnClicked();
                expect($uibModal.dismiss).toHaveBeenCalled();
            });
        });
        it("SessionPopupService else", function() {
            inject(function(_$controller_, $translate, $uibModal) {
                _$controller_("sessionPopupCtrl", { $scope: $scope, $uibModalInstance: mockModal, $window: windowObj, errorData: errorCodeElse });
                spyOn($uibModal, 'open');
                spyOn($uibModal, 'dismiss').and.returnValue({});
                service.openSessionPopup();
                deferred.reject({
                    status: 401,
                    data: {
                        errorCode: '1110',
                        errorMessage: 'The system was unable to reset the password. Please contact a site administrator for assistance.',
                        detailErrorMessage: ''
                    }
                });
                $scope.$digest();
                $scope.okBtnClicked();
                expect($uibModal.dismiss).toHaveBeenCalled();
            });
        });
    });
});
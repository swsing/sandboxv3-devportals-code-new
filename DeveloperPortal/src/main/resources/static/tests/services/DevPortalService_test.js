"use strict";
describe("developerHub.DevPortalService_test", function() {

    beforeEach(module("developerHub"));

    var service;

    beforeEach(inject(function($injector) {
        service = $injector.get("DevPortalService");
    }));

    describe("test changePasswordRequest", function() {
        it("test with request data", function() {
            inject(function($http) {
                spyOn($http, "post").and.returnValue("output");

                var reqData = "data";
                var output = "output";

                output = service.changePasswordRequest(reqData);
                expect($http.post).toHaveBeenCalledWith("/auth/changePassword", reqData);
                expect(output).toBe("output");
            });
        });

        it("test without request data", function() {
            inject(function($http) {
                spyOn($http, "post");

                var reqData = null;
                var output = null;

                output = service.changePasswordRequest(reqData);
                expect($http.post).not.toHaveBeenCalled();
                expect(output).toBeUndefined();
            });
        });
    });

    describe("test authSessionRequest", function() {
        it("test with request data", function() {
            inject(function($http) {
                spyOn($http, "get").and.returnValue("output");

                var output = "output";

                output = service.authSessionRequest();
                expect($http.get).toHaveBeenCalledWith("/auth/session");
                expect(output).toBe("output");
            });
        });
    });
});
"use strict";
describe("developerHub.FormValidation_test", function() {
    var errorFlag = true;
    var submitButtonEnableFlag = true;
    beforeEach(function() {
        var fixture = '<div id="fixture">' +
            '<form name="myForm">' +
            '<button id="submitButton">' +
            '</button>' +
            '</form>';

        document.body.insertAdjacentHTML(
            'afterbegin',
            fixture);
        // jasmine.getFixtures().fixturesPath = "base/tests/fixtures/";
        // loadFixtures("changePassword.html");
    });

    // remove the html fixture from the DOM
    afterEach(function() {
        document.body.removeChild(document.getElementById('fixture'));
    });

    describe("test validateInput", function() {
        it("validateInput currentPassword", function() {
            spyOn(window, "addError");
            var inputData = { "data": { "value": "abcd", "className": "" } };
            var validateType = "password";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput newPassword if", function() {
            spyOn(window, "addError");
            var inputData = { "data": { "value": "abcd", "className": "" } };

            var validateType = "newPassword";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput newPassword else", function() {
            var fixture = '<div id="fixture">' +
                '<form name="myForm">' +
                '<input id="confirmPassword" type="password" size="36" name="confirmPassword" value="abcd" autocorrect="off" autocapitalize="off" placeholder="Confirm New password" ng-model="confirmPassword" class="input-error ng-touched">' +
                '</input>' +
                '</form>';

            document.body.insertAdjacentHTML(
                'afterbegin',
                fixture);
            spyOn(window, "addError");
            var inputData = { "data": { "value": "p@ssw0rD123", "className": "" } };
            var validateType = "newPassword";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput confirmPassword if", function() {
            spyOn(window, "addError");
            var inputData = { "data": { "value": "abcd", "className": "" } };

            var validateType = "confirmPassword";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput confirmPassword else", function() {
            var fixture = '<div id="fixture">' +
                '<form name="myForm">' +
                '<input id="newPassword" type="password" size="36" name="newPassword" value="abcd" autocorrect="off" autocapitalize="off" placeholder="New password" ng-model="newPassword" class="input-error ng-touched">' +
                '</input>' +
                '</form>';

            document.body.insertAdjacentHTML(
                'afterbegin',
                fixture);
            spyOn(window, "addError");
            var inputData = { "data": { "value": "p@ssw0rD123", "className": "" } };
            var validateType = "confirmPassword";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput inputName", function() {
            spyOn(window, "addError");
            var inputData = { "data": { "value": "abcd123...", "className": "" } };
            var validateType = "inputName";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput email if", function() {
            spyOn(window, "addError");
            var inputData = { "data": { "value": "abcd", "className": "" } };
            var validateType = "email";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput email if", function() {
            spyOn(window, "addError");
            var inputData = { "data": { "value": "abcd@happy-.com", "className": "" } };
            var validateType = "email";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput email else", function() {
            var fixture = '<div id="fixture">' +
                '<form name="myForm">' +
                '<input id="confirmEmail" type="email" size="36" name="confirmEmail" value="abcd@gmail.com" autocorrect="off" autocapitalize="off" placeholder="Confirm New password" ng-model="confirmEmail" class="input-error ng-touched">' +
                '</input>' +
                '</form>';

            document.body.insertAdjacentHTML(
                'afterbegin',
                fixture);
            spyOn(window, "addError");
            var inputData = { "data": { "value": "abcd@gmail.com", "className": "" } };
            var validateType = "email";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput confirmEmail else", function() {
            var fixture = '<div id="fixture">' +
                '<form name="myForm">' +
                '<input id="email" type="email" size="36" name="email" value="abcd" >' +
                '</input>' +
                '</form>';

            document.body.insertAdjacentHTML(
                'afterbegin',
                fixture);
            spyOn(window, "addError");
            var inputData = { "data": { "value": "abcd@gmail.com", "className": "" } };
            var validateType = "confirmEmail";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput checkbox", function() {
            spyOn(window, "addError");
            var inputData = { "data": { "checked": false, "className": "", "value": "" } };

            var validateType = "checkbox";
            validateInput(inputData.data, validateType, true);
            expect(errorFlag).toBe(true);
        });
        it("validateInput none", function() {
            spyOn(window, "addError");
            var inputData = { "data": { "value": "p@ssw0rD", "className": "invalid" } };
            var validateType = "password";
            validateInput(inputData.data, validateType, true, true);
            expect(errorFlag).toBe(true);
        });
    });
    describe("test addError", function() {
        it("addError", function() {
            var element = document.getElementById('newPassword');
            addError(element, "content")
        });
    });
    describe("test focusInput", function() {
        it("focusInput", function() {
            spyOn(window, 'validateFormRequired');
            var inputData = { "data": { "value": "abcd@gmail.com", "className": "invalid", "form": { "id": "changePassword" }, "id": "newPassword" } };
            focusInput(inputData.data);
            expect(inputData.data.className).toBe("invalid");
            expect(validateFormRequired).toHaveBeenCalled();
        });
    });
    describe("test validateFormRequired", function() {
        var fixture = "<form id=\"changePasswordForm\" name=\"changePasswordForm\" ng-submit=\"changePassword()\" class=\"ng-valid ng-dirty ng-valid-parse\">" +
            "<div class=\"content-container\"> " +
            "<div class=\"devportal-input-label top-container\"><span class=\"alert-required\">*</span>Current password</div>" +
            "<div class=\"devportal-input-container\">" +
            "<input id=\"acceptTerms\" type=\"checkbox\" name=\"termsOfUse\" tabindex=\"8\" onchange=\"validateInput(this, 'acceptTerms',true)\" checked=true>I agree to the <a href=\"#\" class=\"custLink\">Terms of use</a>" +
            "<input id=\"currentPassword\" type=\"password\" size=\"36\" name=\"currentPassword\" placeholder=\"Current password\" value=\"p@ssw0rD123\" ng-model=\"currentPassword\" onblur=\"validateInput(this, 'password', true, true)\" onfocus=\"focusInput(this)\" class=\"ng-valid ng-not-empty ng-dirty ng-valid-parse ng-touched\"> " +
            "<div class=\"alert-inline\"> Please enter a valid password.</div></div></div>" +
            "<div class=\"devportal-input-label \"><span class=\"alert-required\">*</span>New password</div>" +
            "<div class=\"devportal-input-container\">" +
            "<input id=\"newPassword\" type=\"password\" size=\"36\" name=\"newPassword\" value=\"p@ssw0rD123\" autocorrect=\"off\" autocapitalize=\"off\" placeholder=\"New password\" ng-model=\"newPassword\" onblur=\"validateInput(this, 'newPassword', true, true)\" onfocus=\"focusInput(this)\" class=\"ng-valid ng-touched ng-not-empty ng-dirty ng-valid-parse\">" +
            "<div class=\"alert-inline\"> Please enter a valid password.</div></div>" +
            "<div class=\"content-container\">" +
            "<div class=\"devportal-input-label top-container\"><span class=\"alert-required\">*</span>Confirm New Password</div>" +
            "<div class=\"devportal-input-container\">" +
            "<input id=\"confirmPassword\" type=\"password\" size=\"36\" name=\"confirmPassword\" value=\"\" autocorrect=\"off\" autocapitalize=\"off\" placeholder=\"Confirm New password\" ng-model=\"confirmPassword\" onblur=\"validateInput(this, 'confirmPassword',true, true)\" onfocus=\"focusInput(this)\" class=\"ng-valid ng-touched ng-not-empty ng-dirty ng-valid-parse \">" +
            "<div class=\"alert-inline\"> Your passwords do not match.</div></div></div>" +
            "<div class=\"inline-buttons content-container\">" +
            "<div class=\"row\">" +
            "<div class=\"col-md-6\">" +
            "<button class=\"btn btn-default-pwd pull-left\" ui-sref=\"developerHub\" href=\"#!/developer-hub\"> Cancel </button></div>" +
            "<div class=\"col-md-6 padd-o\">" +
            "<button id=\"submitButton\" class=\"btn btn-submit\" disabled=\"\"> Submit </button></div></div></div>" +
            "</form>";
        document.body.insertAdjacentHTML(
            'afterbegin',
            fixture);
        xit("validateFormRequired if", function() {
            validateFormRequired("changePasswordForm", "", "currentPassword");
            expect(submitButtonEnableFlag).toBe(true);
        });
        it("validateFormRequired submitButtonEnableFlag true", function() {
            var element = document.getElementById('newPassword');
            element.insertAdjacentHTML('beforebegin', "<input id=\"acceptTerms\" type=\"checkbox\" name=\"termsOfUse\" tabindex=\"8\" onchange=\"validateInput(this, 'acceptTerms',true)\" checked=true>");
            validateFormRequired("changePasswordForm", "", "acceptTerms");
            expect(submitButtonEnableFlag).toBe(true);
        });
    });
    xdescribe("test submit button", function() {
        it("validateFormRequired submitButtonEnableFlag true", function() {
            document.body.removeChild(document.getElementById('fixture'));
            submitButtonEnableFlag = true;
            var element = document.getElementById('acceptTerms');
            element.insertAdjacentHTML('beforebegin', "<input id=\"acceptTermsData\" type=\"checkbox\" name=\"acceptTermsData\" tabindex=\"8\" onchange=\"validateInput(this, 'acceptTerms',true)\">I agree to the");
            validateFormRequired("changePasswordForm", "", "acceptTermsData");
            expect(submitButtonEnableFlag).toBe(false);
        });
    });
    describe("test checkRequired", function() {
        it("checkRequired", function() {
            var inputData = { "data": { "value": "", "className": "" } };
            var result = checkRequired(inputData.data);
            expect(result).toBe(true);
        });
    });
    describe("test checkKeyDown", function() {
        it("checkKeyDown", function() {
            window.event = { "keyCode": 13 };
            var element = document.getElementById('newPassword');
            checkKeyDown(element);
        });
    });
});
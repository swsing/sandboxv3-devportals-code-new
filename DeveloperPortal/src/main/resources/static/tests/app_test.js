"use strict";
describe("developerHub.app_test", function() {
    var $rootScope, $location;
    beforeEach(module("developerHub"));

    beforeEach(inject(function(_$rootScope_, _$location_) {
        $rootScope = _$rootScope_;
        $location = _$location_;
        spyOn($location, "path").and.returnValue("/");
        spyOn($rootScope, "$broadcast");
        $rootScope.$broadcast.and.callThrough();
    }));
    describe("test all", function() {
        it("test on page load scoll to be on top", function() {
            $rootScope.$broadcast("$stateChangeSuccess");
            expect(document.body.scrollTop).toEqual(0);
            expect(document.body.scrollTop).toEqual(document.documentElement.scrollTop);
        });
    });
    describe("test deffered", function() {
        it("test change password", function() {
            var fixture = '<input type="hidden" name="userType" id="userType" value="support">';
            document.body.insertAdjacentHTML(
                'afterbegin',
                fixture);
            $rootScope.$broadcast("$stateChangeSuccess");
            expect(document.body.scrollTop).toEqual(0);
            expect(document.body.scrollTop).toEqual(document.documentElement.scrollTop);
        });
        it("test change password resolve", function() {
            var fixture = '<input type="hidden" name="userType" id="userType" value="">';
            document.body.insertAdjacentHTML(
                'afterbegin',
                fixture);
            $rootScope.$broadcast("$stateChangeSuccess");
            // expect(document.body.scrollTop).toEqual(0);
            // expect(document.body.scrollTop).toEqual(document.documentElement.scrollTop);
        });
    });
});
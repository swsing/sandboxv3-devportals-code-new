package com.capgemini.portal.service.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestBody;

import com.capgemini.mail.config.MailConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.helper.PortalHelper;
import com.capgemini.portal.model.ApiResponse;
import com.capgemini.portal.model.ChangePasswordRequest;
import com.capgemini.portal.model.ChangePasswordResponse;
import com.capgemini.portal.model.UserProfile;
import com.capgemini.portal.model.ValidateSessionResponse;
import com.capgemini.portal.security.user.SAMLUser;
import com.capgemini.portal.security.user.SAMLUserDetails;
import com.capgemini.portal.service.DevPortalService;

@Service
public class DevPortalServiceImpl implements DevPortalService {

	@Autowired
	PortalHelper portalHelper;

	@Value("${download.file.classpathUrl}")
	private String classpathUrl;

	@Value("${download.file.UrlResource}")
	private String url;

	@Value("${download.file.name}")
	private String fileName;

	@Value("${download.file.isClassPathResource}")
	private boolean isClassPathResource;

	@Autowired
	private MailConfig mailConfig;

	@Override
	public ValidateSessionResponse validateSession(@SAMLUser SAMLUserDetails user, HttpServletRequest request,
			HttpServletResponse response) {

		ValidateSessionResponse vResponse = new ValidateSessionResponse();

		SecurityContext sc = SecurityContextHolder.getContext();
		Authentication auth = sc.getAuthentication();

		boolean isSLORequired = true;

	
		if (!auth.isAuthenticated()
				|| auth.getAuthorities().contains(new SimpleGrantedAuthority(PortalConstants.ROLE_ANONYMOUS))) {
			
			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.ROLE_ANONYMOUS_EXECEPTION,
					PortalErrorCodeEnum.USER_SESSION_EXPIRED);
		} else if (!request.isRequestedSessionIdValid()) {
			PortalHelper.handleLocalSessionTimeout(user, request, response, isSLORequired);
		} else if (user == null) {
			
			PortalHelper.handleSessionTimeout(request, response, isSLORequired);

		} else {

			vResponse.setStatusCode(Integer.toString(HttpStatus.OK.value()));
		}
		return vResponse;

	}

	@Override
	public ChangePasswordResponse changePassword(@SAMLUser SAMLUserDetails user,
			@RequestBody ChangePasswordRequest cpRequest) {

		ChangePasswordResponse response = new ChangePasswordResponse();

		String result = portalHelper.isValidCurrentPwd(user.getAttribute(PortalConstants.SAML_ATTRIBUTE_USER_NAME),
				cpRequest.getOldPwd());

		if (PortalConstants.VALID.equalsIgnoreCase(result)) {

			try {
				portalHelper.updateDevUserAttr(user.getAttribute(PortalConstants.SAML_ATTRIBUTE_USER_NAME),
						cpRequest.getNewPwd());
			} catch (org.springframework.ldap.OperationNotSupportedException e) {

				throw DeveloperPortalException.populateDeveloperPortalException(e.getMessage(),
						PortalErrorCodeEnum.OPERATION_NOT_SUPPORTED);
			}
		} else if (PortalConstants.INVALID.equalsIgnoreCase(result)) {

			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.INVALID_CURRENT_PWD_MSG,
					PortalErrorCodeEnum.INVALID_CURRENT_PASSWORD);

		} else {
			throw DeveloperPortalException.populateDeveloperPortalException(
					PortalConstants.ERROR_CHANGING_PWD_EXCEPTION, PortalErrorCodeEnum.ERROR_CHANGING_PASSWORD);
		}

		response.setStatusCode(Integer.toString(HttpStatus.OK.value()));

		UserProfile userProfile = portalHelper
				.fetchUserProfile(user.getAttribute(PortalConstants.SAML_ATTRIBUTE_USER_NAME));

		if (mailConfig.getIsAsyncCall()) {
			portalHelper.sentMailAsynchronously(user, userProfile);
		} else {
			portalHelper.changePwdMailSentOperation(user, userProfile);
		}

		return response;

	}

	@Override
	public ResponseEntity<Object> downloadTestDataFile(SAMLUserDetails user) {

		Resource resource = null;

		try {
			if (isClassPathResource) {
				resource = new ClassPathResource(classpathUrl);
				HttpHeaders headers = new HttpHeaders();

				headers.setContentType(
						MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
				headers.add("Access-Control-Allow-Methods", "GET");
				headers.add("Access-Control-Allow-Headers", "Content-Type");
				headers.add("Content-Disposition", "attachment; filename=" + resource.getFilename());
				headers.add("FileName", resource.getFilename());

				// headers.add("Cache-Control", "no-cache, no-store,
				// must-revalidate"); // headers.add("Pragma", "no-cache"); //
				// headers.add("Expires", "0");

				return new ResponseEntity<>(StreamUtils.copyToByteArray(resource.getInputStream()), headers,
						HttpStatus.OK);

			} else {

				HttpHeaders headers = new HttpHeaders();

				headers.setContentType(
						MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
				headers.add("Access-Control-Allow-Methods", "GET");
				headers.add("Access-Control-Allow-Headers", "Content-Type");
				headers.add("Content-Disposition", "attachment; filename=" + fileName);
				headers.add("FileName", fileName);

				return new ResponseEntity<>(StreamUtils.copyToByteArray(portalHelper.downloadTestDataFromS3(fileName)),
						headers, HttpStatus.OK);
			}

		} catch (IOException ex) {
			throw DeveloperPortalException.populateDeveloperPortalException(ex.getMessage(),
					PortalErrorCodeEnum.ERROR_DOWNLOADING_TESTDATA);
		}

	}

	@Override
	public UserProfile fetchUserProfile(SAMLUserDetails user) {
		return portalHelper.fetchUserProfile(user.getAttribute(PortalConstants.SAML_ATTRIBUTE_USER_NAME));

	}

	@Override
	public ApiResponse updateUserProfile(SAMLUserDetails user, UserProfile userProfile) {

		ApiResponse response = new ApiResponse();
		Map<String, String> map = portalHelper.pouplateUserProfileParameters(userProfile);
		String[] lstArray = { PortalConstants.FIRSTNAME, PortalConstants.LASTNAME, PortalConstants.ORGANIZATION };

		portalHelper.updateUserProfile(user.getAttribute(PortalConstants.SAML_ATTRIBUTE_USER_NAME), map,
				Arrays.asList(lstArray));
		response.setStatusCode(Integer.toString(HttpStatus.OK.value()));
		return response;
	}

	@Override
	public void logout(@SAMLUser SAMLUserDetails user, HttpServletRequest request, HttpServletResponse response) {

		portalHelper.processInvalidateSessionAndCookies(request, response, false);

	}

}

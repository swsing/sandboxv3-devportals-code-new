package com.capgemini.portal.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.capgemini.portal.model.ApiResponse;
import com.capgemini.portal.model.ChangePasswordRequest;
import com.capgemini.portal.model.ChangePasswordResponse;
import com.capgemini.portal.model.UserProfile;
import com.capgemini.portal.model.ValidateSessionResponse;
import com.capgemini.portal.security.user.SAMLUser;
import com.capgemini.portal.security.user.SAMLUserDetails;

public interface DevPortalService {

	public ValidateSessionResponse validateSession(@SAMLUser SAMLUserDetails user,HttpServletRequest request, HttpServletResponse response);

	public ChangePasswordResponse changePassword(@SAMLUser SAMLUserDetails user,
			@RequestBody ChangePasswordRequest cpRequest);

	public ResponseEntity<Object> downloadTestDataFile(@SAMLUser SAMLUserDetails user);
	
	public void logout(@SAMLUser SAMLUserDetails user,HttpServletRequest request, HttpServletResponse response);
	
	public  UserProfile fetchUserProfile(@SAMLUser SAMLUserDetails user);
	
	public  ApiResponse updateUserProfile(@SAMLUser SAMLUserDetails user, UserProfile userProfile);
}

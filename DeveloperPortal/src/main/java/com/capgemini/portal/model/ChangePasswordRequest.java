package com.capgemini.portal.model;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "oldPwd", "newPwd" })
public class ChangePasswordRequest implements Serializable {
	
	private static final long serialVersionUID = 7413378082690755561L;

	@JsonProperty("oldPwd")
	private String oldPwd;

	@JsonProperty("newPwd")
	private String newPwd;

	@JsonProperty("oldPwd")
	public String getOldPwd() {
		return oldPwd;
	}

	@JsonProperty("oldPwd")
	public void setOldPwd(String oldPwd) {
		this.oldPwd = oldPwd;
	}

	@JsonProperty("newPwd")
	public String getNewPwd() {
		return newPwd;
	}

	@JsonProperty("newPwd")
	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

}
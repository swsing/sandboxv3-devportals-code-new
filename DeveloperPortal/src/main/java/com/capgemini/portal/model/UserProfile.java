package com.capgemini.portal.model;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserProfile implements Serializable {
	
	private static final long serialVersionUID = 7413378082690755561L;

	@JsonProperty("firstName")
	private String firstName;

	@JsonProperty("lastName")
	private String lastName;
	
	@JsonProperty("emailId")
	private String uid;
	
	@JsonProperty("contactNumber")
	private String contactNumber;
	

	@JsonProperty("countryLocation")
	private String countryLocation;
	
	@JsonProperty("organization")
	private String o;
	
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * @param uid the uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * @return the countryLocation
	 */
	public String getCountryLocation() {
		return countryLocation;
	}

	/**
	 * @param countryLocation the countryLocation to set
	 */
	public void setCountryLocation(String countryLocation) {
		this.countryLocation = countryLocation;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
		
	/**
	 * @return the o
	 */
	public String getO() {
		return o;
	}

	/**
	 * @param o the o to set
	 */
	public void setO(String o) {
		this.o = o;
	}

	
}
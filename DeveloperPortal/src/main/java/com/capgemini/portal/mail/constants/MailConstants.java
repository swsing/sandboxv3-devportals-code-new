package com.capgemini.portal.mail.constants;

public class MailConstants {
	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL
	 * __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 *  
	 * NOTICE:  All information contained herein is, and remains
	 * the property of CAPGEMINI GROUP.
	 * The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered
	 * by patents, patents in process, and are protected by trade secret
	 * or copyright law.
	 * Dissemination of this information or reproduction of this material
	 * is strictly forbidden unless prior written permission is obtained
	 * from CAPGEMINI GROUP.
	 ******************************************************************************/
	
	private MailConstants() {
	}

	
	
	/** one time password and Email functionality constants */
	public static final String REGISTER_EMAIL_TEMPLATE_CODE = "REGISTER";
	public static final String RESEND_ACTIVATION_LINK_EMAIL_TEMPLATE_CODE = "RESEND_ACTIVATION_LINK";
	public static final String CLASSPATH = "CLASSPATH";
	public static final String CHANGE_PASSWORD_LINK_EMAIL_TEMPLATE_CODE = "CHANGE_PASSWORD_LINK";

	public static final String MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String MAIL_SMTP_HOST = "mail.smtp.host";
	public static final String MAIL_SMTP_STARTTLE_ENABLE = "mail.smtp.starttls.enable";
	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String MAIL_SMTP_SSL_TRUST = "mail.smtp.ssl.trust";

	public static final String CONTENT_TYPE_TEXT_HTML = "text/html";
	
	
}

package com.capgemini.portal.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.UnsatisfiedDependencyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Component;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.capgemini.mail.config.MailConfig;
import com.capgemini.mail.service.MailService;
import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.config.PortalAWSConfig;
import com.capgemini.portal.config.PortalLdapConfiguration;
import com.capgemini.portal.config.PortalValidationConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.portal.model.ChangePasswordRequest;
import com.capgemini.portal.model.UserProfile;
import com.capgemini.portal.security.user.SAMLUser;
import com.capgemini.portal.security.user.SAMLUserDetails;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.template.builder.TemplateBuilder;
import com.capgemini.template.builder.TemplateBuilder.Template;

@Component
public class PortalHelper {

	@Autowired
	public LdapTemplate ldapTemplate;

	@Autowired
	PortalLdapConfiguration ldapConfig;

	@Autowired
	private PortalValidationConfig portalValidationConfig;

	@Autowired
	MailService mailService;

	@Autowired
	TemplateBuilder templateBuilder;

	@Autowired
	private PortalLoggerUtils portalLoggerUtils;

	@Autowired
	private PortalAspectUtils portalAspectUtils;

	@Autowired
	MailConfig mailConfig;

	@Autowired
	private PortalAWSConfig portalAWSConfig;

	@Autowired
	private ClientConfiguration clientConfig;

//	@Autowired
//	private BasicAWSCredentials awsCredentials;

	/**
	 * 
	 * @param uid
	 * @param newPassword
	 * @return
	 * 
	 */
	public String updateDevUserAttr(String uid, String newPassword) {

		try {
			Name dn = buildDevUserDN(uid);
			List<ModificationItem> mods = new ArrayList<>();

			mods.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
					new BasicAttribute("userPassword", newPassword)));

			ldapTemplate.modifyAttributes(dn, mods.toArray(new ModificationItem[mods.size()]));
			return PortalConstants.SUCCESS;
		} catch (org.springframework.ldap.OperationNotSupportedException e) {
			throw DeveloperPortalException.populateDeveloperPortalException(e.getMessage(),
					PortalErrorCodeEnum.OPERATION_NOT_SUPPORTED);
		}
	}

	/**
	 * @param request
	 * @param response
	 */
	public static void processInvalidateSessionAndCookies(HttpServletRequest request, HttpServletResponse response,
			boolean isLogoutRequest) {
		/* Getting session and then invalidating it */
		HttpSession session = request.getSession(false);
		if (request.isRequestedSessionIdValid() && session != null) {
			session.invalidate();
		}
		if (isLogoutRequest) {
			handleLogoutCookies(request, response);
		} else {
			handleRequestCookies(request, response);
		}
	}

	/**
	 * Validate the current password into ldap directory
	 * 
	 * @param uid
	 * @param currentPassword
	 * @return String
	 * 
	 *
	 */
	public String isValidCurrentPwd(String uid, String currentPassword) {

		String result = PortalConstants.VALID;
		try {

			if (ldapPasswordCheck(uid, currentPassword)) {

				result = PortalConstants.VALID;

			} else {
				result = PortalConstants.INVALID;
			}

		} catch (org.springframework.ldap.NamingException se) {
			throw DeveloperPortalException.populateDeveloperPortalException(se.getMessage(),
					PortalErrorCodeEnum.ERROR_CHANGING_PASSWORD);

		}

		return result;
	}

	/**
	 * @param uid
	 * @return
	 */
	public Name buildDevUserDN(String uid) {
		return LdapNameBuilder.newInstance().add(ldapConfig.getDevUserBaseDn())
				.add(PortalConstants.LDAP_ATTRIBUTE_UID, uid).build();
	}

	/**
	 * 
	 * @param uid
	 * @param oldPwd
	 * @return
	 * 
	 */
	public boolean ldapPasswordCheck(String uid, String oldPwd) {

		LdapQuery ldapQuery = LdapQueryBuilder.query().attributes("*", "+").base(ldapConfig.getDevUserBaseDn())
				.filter("(uid=" + uid + ")");

		return ldapTemplate.authenticate(ldapConfig.getDevUserBaseDn(), ldapQuery.filter().toString(), oldPwd);

	}

	/**
	 * @param user
	 * @param request
	 * @param response
	 * @param isSLORequired
	 * @throws DeveloperPortalException
	 */
	public static void handleLocalSessionTimeout(SAMLUserDetails user, HttpServletRequest request,
			HttpServletResponse response, boolean isSLORequired) {
		if (isSLORequired) {
			if (user == null) {
				throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.LOCAL_SESSION_EXIPRED,
						PortalErrorCodeEnum.APP_USER_SESSION_EXPIRED);

			}
			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.LOCAL_SESSION_EXIPRED,
					PortalErrorCodeEnum.APP_USER_SESSION_EXPIRED);
		} else {

			processInvalidateSessionAndCookies(request, response, false);
			if (user == null) {
				throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.LOCAL_SESSION_EXIPRED,
						PortalErrorCodeEnum.USER_SESSION_EXPIRED);

			}
			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.LOCAL_SESSION_EXIPRED,
					PortalErrorCodeEnum.USER_SESSION_EXPIRED);
		}
	}

	/**
	 * @param request
	 * @param response
	 * @param isSLORequired
	 * @throws DeveloperPortalException
	 */
	public static void handleSessionTimeout(HttpServletRequest request, HttpServletResponse response,
			boolean isSLORequired) {
		if (isSLORequired) {
			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.LOCAL_SESSION_EXIPRED,
					PortalErrorCodeEnum.APP_USER_SESSION_EXPIRED);
		} else {
			processInvalidateSessionAndCookies(request, response, false);
			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.LOCAL_SESSION_EXIPRED,
					PortalErrorCodeEnum.USER_SESSION_EXPIRED);
		}
	}

	/**
	 * This method would edit the cookie information and make JSESSIONID empty
	 * while responding to logout.
	 * 
	 * @param response
	 */
	public static void handleLogoutCookies(HttpServletRequest request, HttpServletResponse response) {
		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {
			cookie.setMaxAge(0);
			cookie.setValue(null);
			cookie.setPath("/");
			response.addCookie(cookie);
		}
	}

	/**
	 * This method would edit the cookie information and make JSESSIONID empty
	 * while responding to logout.
	 * 
	 * @param response
	 */
	public static void handleRequestCookies(HttpServletRequest request, HttpServletResponse response) {
		Cookie[] cookies = request.getCookies();

		for (Cookie cookie : cookies) {
			if (cookie.getName() != null && (cookie.getValue().equalsIgnoreCase(PortalConstants.PF)
					|| cookie.getName().equalsIgnoreCase(PortalConstants.JSESSIONID))) {
				cookie.setValue(null);
				cookie.setMaxAge(0);
				response.addCookie(cookie);
			}
		}
	}

	/**
	 * Validate the Change Password Request
	 * 
	 * @param user
	 * @param cpRequest
	 */
	public void validateChangePasswordRequest(SAMLUserDetails user, ChangePasswordRequest cpRequest) {

		if (user == null) {

			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.SESSION_EXIPRED,
					PortalErrorCodeEnum.USER_SESSION_EXPIRED);
		}

		if (cpRequest == null || StringUtils.isEmpty(cpRequest.getNewPwd())
				|| StringUtils.isEmpty(cpRequest.getOldPwd()) || !(cpRequest.getNewPwd() != null
						&& cpRequest.getNewPwd().matches(portalValidationConfig.getChangePwdRegxString()))) {

			throw DeveloperPortalException.populateDeveloperPortalException(
					PortalConstants.INVALID_CURRENT_AND_OLD_PWD_MSG, PortalErrorCodeEnum.ERROR_CHANGING_PASSWORD);
		}
	}

	/**
	 * 
	 * @param uid
	 * @param map
	 * @param listOfAttribute
	 * @return
	 */
	public String updateUserProfile(String uid, Map<String, String> map, List<String> listOfAttribute) {

		try {
			Name dn = buildDevUserDN(uid);
			List<ModificationItem> mods = new ArrayList<>();

			DirContextAdapter dx = (DirContextAdapter) ldapTemplate.lookup(dn);

			for (String attr : listOfAttribute) {

				if (PortalConstants.LDAP_ATTRIBUTE_O.equalsIgnoreCase(attr)
						&& dx.attributeExists(PortalConstants.LDAP_ATTRIBUTE_O) && StringUtils.isEmpty(map.get(attr))) {

					mods.add(new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
							new BasicAttribute(PortalConstants.LDAP_ATTRIBUTE_O)));
				} else {
					mods.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
							new BasicAttribute(attr, map.get(attr))));
				}

			}

			ldapTemplate.modifyAttributes(dn, mods.toArray(new ModificationItem[mods.size()]));
			return PortalConstants.SUCCESS;
		} catch (Exception e) {// org.springframework.ldap.OperationNotSupportedException
								// e) {
			throw DeveloperPortalException.populateDeveloperPortalException(e.getMessage(),
					PortalErrorCodeEnum.UPDATE_PROFILE_OPERATION_NOT_SUPPORTED);
		}
	}

	/**
	 * 
	 * @param user
	 * @param emailId
	 */
	public void validateUserEmailId(SAMLUserDetails user, String emailId) {
		if (StringUtils.isNotEmpty(emailId)
				&& !emailId.equalsIgnoreCase(user.getAttribute(PortalConstants.SAML_ATTRIBUTE_USER_NAME))) {
			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.INVALID_USER_ID,
					PortalErrorCodeEnum.INVALID_USER_ID);
		}
	}

	/**
	 * 
	 * @param user
	 * @param userProfile
	 */
	public void validateUpdateUserProfileRequest(UserProfile userProfile) {

		if (NullCheckUtils.isNullOrEmpty(userProfile.getFirstName())
				|| !userProfile.getFirstName().matches(portalValidationConfig.getFirstName())) {
			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.INVALID_FIRST_NAME,
					PortalErrorCodeEnum.INVALID_USER_DETAILS);
		}

		if (NullCheckUtils.isNullOrEmpty(userProfile.getLastName())
				|| !userProfile.getLastName().matches(portalValidationConfig.getLastName())) {
			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.INVALID_LAST_NAME,
					PortalErrorCodeEnum.INVALID_USER_DETAILS);
		}

		if (!NullCheckUtils.isNullOrEmpty(userProfile.getO())
				&& !userProfile.getO().matches(portalValidationConfig.getOrganization())) {
			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.INVALID_ORGANIZATION,
					PortalErrorCodeEnum.INVALID_USER_DETAILS);
		}

		/*
		 * if (NullCheckUtils.isNullOrEmpty(userProfile.getContactNumber()) &&
		 * userProfile.getContactNumber().matches(portalApplicationConfig.
		 * getContactNumber())) { throw
		 * DeveloperPortalException.populateDeveloperPortalException(
		 * PortalConstants.INVALID_CONTACT_NUMBER,
		 * PortalErrorCodeEnum.INVALID_USER_DETAILS); }
		 * 
		 * if (NullCheckUtils.isNullOrEmpty(userProfile.getCountryLocation()) &&
		 * userProfile.getCountryLocation().matches(portalApplicationConfig.
		 * getCountryLocation())) { throw
		 * DeveloperPortalException.populateDeveloperPortalException(
		 * PortalConstants.INVALID_COUNTRY_LOCATION,
		 * PortalErrorCodeEnum.INVALID_USER_DETAILS); }
		 */

	}

	public Map<String, String> pouplateUserProfileParameters(UserProfile userProfile) {

		Map<String, String> parameters = new HashMap<>();
		parameters.put(PortalConstants.FIRSTNAME, userProfile.getFirstName());
		parameters.put(PortalConstants.LASTNAME, userProfile.getLastName());
		// parameters.put(PortalConstants.USERNAME, userProfile.getUid());
		parameters.put(PortalConstants.ORGANIZATION, userProfile.getO());
		/*
		 * parameters.put(PortalConstants.CONTACT_NUMBER,
		 * userProfile.getContactNumber());
		 * parameters.put(PortalConstants.COUNTRY_LOCATION,
		 * userProfile.getCountryLocation());
		 */

		return parameters;
	}

	public UserProfile fetchUserProfile(String uid) {
		List<Object> userDetails = null;

		// Ldap query to retrieve User details from
		// ou=developer,ou=people,dc=boi,dc=co.uk,dc=capgeminibank,dc=com basedn
		LdapQuery ldapQuery = getLdapQuery(ldapConfig.getDevUserBaseDn(), PortalConstants.UID, uid);
		try {

			userDetails = getLdapSearchResult(ldapQuery);

			if (userDetails == null || userDetails.isEmpty()) {
				return null;
			}

			return populateUserDetails((BasicAttributes) userDetails.get(0));

		} catch (org.springframework.ldap.NamingException | PortalException e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.LDAP_ATTRIBUTE_NOT_FOUND);
		}
	}

	/**
	 * 
	 * @param baseDn
	 * @return
	 */
	public static Name buildBaseDN(String baseDn) {
		return LdapNameBuilder.newInstance().add(baseDn).build();
	}

	/**
	 * 
	 * Common utility method to get execute the LdapQuery and get List of
	 * Objects
	 * 
	 * @param ldapQuery
	 * @return
	 */
	public List<Object> getLdapSearchResult(LdapQuery ldapQuery) {
		return ldapTemplate.search(ldapQuery, new AttributesMapper<Object>() {
			@Override
			public Object mapFromAttributes(Attributes attrs) throws NamingException {
				return attrs;
			}
		});
	}

	/**
	 * 
	 * @param baseDn
	 * @param searchAttribute
	 * @param attributeValue
	 * @return
	 */
	public static LdapQuery getLdapQuery(String baseDn, String searchAttribute, String attributeValue) {
		return LdapQueryBuilder.query().attributes(PortalConstants.ASTERISK, PortalConstants.PLUS).base(baseDn)
				.filter("(" + searchAttribute + "=" + attributeValue + ")");
	}

	/**
	 * @param ldapObject
	 * @return
	 */
	public static UserProfile populateUserDetails(BasicAttributes ldapObject) {

		UserProfile userPorfile = new UserProfile();

		userPorfile.setFirstName(getAttributeValue(ldapObject, PortalConstants.FIRSTNAME));
		userPorfile.setLastName(getAttributeValue(ldapObject, PortalConstants.LASTNAME));
		userPorfile.setO(getAttributeValue(ldapObject, PortalConstants.ORGANIZATION));
		userPorfile.setUid(getAttributeValue(ldapObject, PortalConstants.UID));
		/*
		 * userPorfile.setContactNumber(getAttributeValue(ldapObject,
		 * PortalConstants.CONTACT_NUMBER));
		 * userPorfile.setCountryLocation(getAttributeValue(ldapObject,
		 * PortalConstants.COUNTRY_LOCATION));
		 */

		return userPorfile;
	}

	public static String getAttributeValue(BasicAttributes basicAttr, String ldapAttr) {
		String attributeValue = null;
		try {
			if (basicAttr.get(ldapAttr) != null && basicAttr.get(ldapAttr).get() != null) {
				attributeValue = basicAttr.get(ldapAttr).get().toString();
			}
		} catch (NamingException ex) {
			throw PortalException.populatePortalException(ex.getMessage(),
					PortalErrorCodeEnum.LDAP_ATTRIBUTE_NOT_FOUND);
		}

		return attributeValue;
	}

	/**
	 * Send Mail in case of Change Password
	 * 
	 * @param user
	 * @param userProfile
	 */
	public void changePwdMailSentOperation(SAMLUserDetails user, UserProfile userProfile) {
		PortalLoggerAttribute loggerAttribute = portalLoggerUtils
				.populateLoggerData(PortalConstants.CHANGE_PWD_MAIL_SENT_OPERATION_METHOD);

		String uid = user.getAttribute(PortalConstants.SAML_ATTRIBUTE_USER_NAME);
		if (StringUtils.isEmpty(uid) && mailConfig.getIsExceptionThrow()) {
			DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException(
					PortalConstants.USER_ID_NULL, PortalErrorCodeEnum.ERROR_SENDIND_MAIL);
			portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, de);
		}

		try {
			Map<String, String> map = populateChangePwdMap(userProfile.getFirstName());
			// 1. Fetch the email template string using Template builder
			String validateHTML = templateBuilder.getTemplate(Template.CHANGEPASSWORD, map);

			// sent an email for success operation
			mailService.sendEmail(uid, validateHTML, mailConfig.getChangePwdSuccessMsgSubject());
		} catch (IOException | MailSendException | DeveloperPortalException e) {
			if (mailConfig.getIsExceptionThrow()) {
				DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException(e.getMessage(),
						PortalErrorCodeEnum.ERROR_SENDIND_MAIL);
				portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, de);
				throw de;
			}
			portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, e);
		}

	}

	private HashMap<String, String> populateChangePwdMap(String firstName) {
		HashMap<String, String> map = new HashMap<>();
		map.put(PortalConstants.FIRSTNAME, firstName);
		map.put(PortalConstants.CHANGE_PWD_COMPLETE_OPERATION_MESSAGE, mailConfig.getChangedMsg());
		return map;
	}

	public void sentMailAsynchronously(@SAMLUser SAMLUserDetails user, UserProfile userProfile) {

		PortalLoggerAttribute loggerAttribute = portalLoggerUtils
				.populateLoggerData(PortalConstants.SENT_MAIL_ASYNCHROUNOUSLY);
		try {
			// create ExecutorService
			ExecutorService executor = Executors.newSingleThreadExecutor();

			for (int i = 0; i < 10; i++) {
				Runnable worker = new MailServicTaskExecutor(user, userProfile);
				executor.execute(worker);
			}

			executor.shutdown();
			while (!executor.isTerminated()) {
			}

		} catch (UnsatisfiedDependencyException | IllegalStateException | DeveloperPortalException e) {
			if (mailConfig.getIsExceptionThrow()) {
				DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException(e.getMessage(),
						PortalErrorCodeEnum.ERROR_SENDIND_MAIL);
				portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, de);
				throw de;
			}
			portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, e);
		}
	}

	public InputStream downloadTestDataFromS3(String fileName) {

		try {

			S3Object fullObject = null;
			AmazonS3 s3Client = null;

			if (clientConfig != null) {
				s3Client = AmazonS3ClientBuilder.standard()
						.withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
						.withClientConfiguration(clientConfig).withRegion(portalAWSConfig.getAwsRegion()).build();
			} else {
				s3Client = AmazonS3ClientBuilder.standard()
						.withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
						.withRegion(portalAWSConfig.getAwsRegion()).build();
			}

			GetObjectRequest gor = new GetObjectRequest(portalAWSConfig.getAwsS3BucketName().trim(),portalAWSConfig.getAwsS3BucketFilePath().trim());
			fullObject = s3Client.getObject(new GetObjectRequest(portalAWSConfig.getAwsS3BucketName(),portalAWSConfig.getAwsS3BucketFilePath()));

			if (fullObject != null && fullObject.getObjectContent() != null && fullObject.getObjectMetadata() != null) {

				return fullObject.getObjectContent();
			} else {
				return null;
			}

		} catch (SdkClientException | IllegalStateException exp) {
			throw DeveloperPortalException.populateDeveloperPortalException(exp.getMessage(),
					PortalErrorCodeEnum.ERROR_DOWNLOADING_TESTDATA);
		}

	}
}
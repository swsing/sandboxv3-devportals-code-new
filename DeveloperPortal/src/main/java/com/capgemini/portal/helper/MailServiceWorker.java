package com.capgemini.portal.helper;

import org.springframework.beans.factory.annotation.Autowired;

import com.capgemini.portal.model.UserProfile;
import com.capgemini.portal.security.user.SAMLUserDetails;

/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE: All information contained herein is, and remains the property of
 * CAPGEMINI GROUP. The intellectual and technical concepts contained herein are
 * proprietary to CAPGEMINI GROUP and may be covered by patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from CAPGEMINI GROUP.
 ******************************************************************************/
public class MailServiceWorker implements Runnable {

	@Autowired
	PortalHelper portalHelper;

	private SAMLUserDetails user;
	
	private UserProfile userProfile;

	public MailServiceWorker(SAMLUserDetails user, UserProfile userProfile) {
		this.user = user;
		this.userProfile = userProfile;
	}

	@Override
	public void run() {
		if (this.user != null) {
			portalHelper.changePwdMailSentOperation(this.user, this.userProfile);
		} 
	}

}
package com.capgemini.portal.helper;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class HealthCheckHelper implements HealthIndicator {

	@Override
	public Health health() {
		return Health.up().build();
	}
}

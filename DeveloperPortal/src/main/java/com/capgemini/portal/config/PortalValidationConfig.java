package com.capgemini.portal.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class PortalValidationConfig {

	@Value("${validation.regxString.changePassword}")
	private String changePwdRegxString;
	
	@Value("${validation.regxString.firstName}")
	private String firstName;
	
	@Value("${validation.regxString.lastName}")
	private String lastName;
	
	@Value("${validation.regxString.organization}")
	private String organization;
	
	@Value("${validation.regxString.countryLocation}")
	private String countryLocation;
	
	@Value("${validation.regxString.contactNumber}")
	private String contactNumber;

	public String getChangePwdRegxString() {
		return changePwdRegxString;
	}

	public void setChangePwdRegxString(String changePwdRegxString) {
		this.changePwdRegxString = changePwdRegxString;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the organization
	 */
	public String getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	/**
	 * @return the countryLocation
	 */
	public String getCountryLocation() {
		return countryLocation;
	}

	/**
	 * @param countryLocation the countryLocation to set
	 */
	public void setCountryLocation(String countryLocation) {
		this.countryLocation = countryLocation;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

}

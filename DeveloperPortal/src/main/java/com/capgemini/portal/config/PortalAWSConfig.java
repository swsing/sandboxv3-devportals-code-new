package com.capgemini.portal.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;

@Configuration
public class PortalAWSConfig {

	@Value("${aws.proxyHost:null}")
	private String proxyHost;

	@Value("${aws.proxyPort:null}")
	private String proxyPort;

	@Value("#{T(com.amazonaws.Protocol).valueOf('${aws.protocol:HTTPS}')}")
	private Protocol protocol;

	@Value("${aws.proxy:#{false}}")
	private boolean proxy;
	
	@Value("${aws.s3.awsRegion}")
	private String awsRegion;

	@Value("${aws.s3.awsS3BucketName}")
	private String awsS3BucketName;
	
	@Value("${aws.s3.awsS3BucketFilePath}")
	private String awsS3BucketFilePath;

	/**
	 * @return the proxyHost
	 */
	public String getProxyHost() {
		return proxyHost;
	}

	/**
	 * @param proxyHost
	 *            the proxyHost to set
	 */
	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	/**
	 * @return the proxyPort
	 */
	public String getProxyPort() {
		return proxyPort;
	}

	/**
	 * @param proxyPort
	 *            the proxyPort to set
	 */
	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}

	/**
	 * @return the protocol
	 */
	public Protocol getProtocol() {
		return protocol;
	}

	/**
	 * @param protocol
	 *            the protocol to set
	 */
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	/**
	 * @return the proxy
	 */
	public boolean isProxy() {
		return proxy;
	}

	/**
	 * @param proxy
	 *            the proxy to set
	 */
	public void setProxy(boolean proxy) {
		this.proxy = proxy;
	}

	
	
	/**
	 * @return the awsRegion
	 */
	public String getAwsRegion() {
		return awsRegion;
	}

	/**
	 * @param awsRegion the awsRegion to set
	 */
	public void setAwsRegion(String awsRegion) {
		this.awsRegion = awsRegion;
	}

	/**
	 * @return the awsS3BucketName
	 */
	public String getAwsS3BucketName() {
		return awsS3BucketName;
	}

	/**
	 * @param awsS3BucketName the awsS3BucketName to set
	 */
	public void setAwsS3BucketName(String awsS3BucketName) {
		this.awsS3BucketName = awsS3BucketName;
	}

	
	
	public String getAwsS3BucketFilePath() {
		return awsS3BucketFilePath;
	}

	public void setAwsS3BucketFilePath(String awsS3BucketFilePath) {
		this.awsS3BucketFilePath = awsS3BucketFilePath;
	}

	@Bean
	public ClientConfiguration getClientConfig() {
		ClientConfiguration clientConfiguration = null;
		if(proxy){
			clientConfiguration = new ClientConfiguration();
			clientConfiguration.setProxyHost(proxyHost);
			clientConfiguration.setProxyPort(Integer.valueOf(proxyPort));
			clientConfiguration.setProtocol(protocol);
		}
		return clientConfiguration;
	} 
	

//	/*@Bean
//	public BasicAWSCredentials getAWSCredentials(
//			@Value("${aws.s3.awsAccesskey}") String accessKey, @Value("${aws.s3.awsSecretkey}")String secretKey) throws Exception {
//		
//		BasicAWSCredentials awsCredentials = null;
//		try {
//			awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
//			return awsCredentials;
//
//		}catch(Exception exception) {
//			throw DeveloperPortalException.populateDeveloperPortalException(exception.getMessage(),
//					PortalErrorCodeEnum.ERROR_DOWNLOADING_TESTDATA); 
//		}
//		
//	}*/

}

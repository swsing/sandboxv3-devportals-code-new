package com.capgemini.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

import com.capgemini.portal.logger.PortalFilter;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountIdentificationMappingAdapter;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages="com.capgemini",
	excludeFilters=@ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE,value= {AccountIdentificationMappingAdapter.class,AccountMappingAdapter.class}))
public class DevPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevPortalApplication.class, args);
	}
	
	@Bean
	public PortalFilter portalFilter(){
		return new PortalFilter();
	}
}

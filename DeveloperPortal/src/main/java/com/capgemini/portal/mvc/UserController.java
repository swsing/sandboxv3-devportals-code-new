package com.capgemini.portal.mvc;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.security.user.SAMLUser;
import com.capgemini.portal.security.user.SAMLUserDetails;
import com.capgemini.portal.security.utilities.SecurityUtils;
import com.capgemini.portal.utilities.SessionCookieUtils;

@Controller
public class UserController {

	@Autowired
	private SecurityUtils utils;

	@Value("${tppPortalURL}")
	private String tppPortalURL;

	@Autowired
	private HttpServletResponse httpServletResponse;

	@Autowired
	private HttpServletRequest httpServletRequest;
	
	private static final String DEFAULT_TARGET_URL = "/login";
	
	/**
	 * @return Developer portal Login page.
	 */
	@RequestMapping(value={"/","/**"})
	public ModelAndView loginPage() {
		ModelAndView mav = new ModelAndView(PortalConstants.INDEX);
		//cookie exist login
		Cookie loginCookie = getPortalLoginCookie(httpServletRequest);
		if (null != loginCookie) {
			try {
				httpServletResponse.sendRedirect(utils.getPortalBaseURL());

			} catch (IOException e) {
				throw PortalException.populatePortalException(PortalErrorCodeEnum.LOGIN_ERROR);
			}
		} else {
		
		String uType = PortalConstants.DEVPORTAL_USER;
		mav.addObject(PortalConstants.MULELAUNCHURL, utils.getMuleLaunchURL() + uType);
		//mav.addObject(PortalConstants.SAML_ATTRIBUTE_EMAIL_ID, user.getAttribute(PortalConstants.USERNAME));
		mav.addObject(PortalConstants.PORTALBASEURL, utils.getPortalBaseURL());
		mav.addObject(PortalConstants.OVER_VIEW_PAGE_URL, utils.getBaseURL());
		mav.addObject(PortalConstants.MULE_PUBLIC_LAUNCHURL, utils.getMulePublicLaunchUrl());
		
		}
		return mav;
	}
	@RequestMapping("/login")
	public ModelAndView loadLandingPage(@SAMLUser SAMLUserDetails user) throws IOException {

		ModelAndView homeView = new ModelAndView(PortalConstants.INDEX);
		try {

			if (user != null) {

				if (utils.isSLOEnable()) {
					
					homeView.addObject(PortalConstants.LOGOUTURL, utils.getIDPLogoutURL());
				} else {
					homeView.addObject(PortalConstants.LOGOUTURL, utils.getLocalLogoutUrl());
				}

				String uType = PortalConstants.DEVPORTAL_USER;
				homeView.addObject(PortalConstants.MULELAUNCHURL, utils.getMuleLaunchURL() + uType);
				homeView.addObject(PortalConstants.PORTALBASEURL, utils.getPortalBaseURL());
				homeView.addObject(PortalConstants.OVER_VIEW_PAGE_URL, utils.getBaseURL());
				homeView.addObject(PortalConstants.SAML_ATTRIBUTE_USER_TYPE, uType);
				homeView.addObject(PortalConstants.SAMLATTRIBUTE, user.getAttributes());
				homeView.addObject(PortalConstants.SAML_ATTRIBUTE_EMAIL_ID, user.getAttribute(PortalConstants.USERNAME));
				homeView.addObject(PortalConstants.SAML_ATTRIBUTE_USER_NAME,
						user.getAttribute(PortalConstants.SAML_ATTRIBUTE_DISPLAY_NAME));
				homeView.addObject(PortalConstants.TPPPORTAL_URL, tppPortalURL); 
				homeView.addObject(PortalConstants.IS_BLOCKED_APIS, user.getAttribute(PortalConstants.IS_BLOCKED_APIS)); 
				homeView.addObject(PortalConstants.MULE_PUBLIC_LAUNCHURL, utils.getMulePublicLaunchUrl());
				loginCookieHandle();
			} else {

				httpServletResponse.sendRedirect("/authError");
			}
		} catch (Exception e) {
			httpServletResponse.sendRedirect("/authError");
		}
		return homeView;

	}

	@RequestMapping("/authError")
	public ModelAndView loadErrorPage(@SAMLUser SAMLUserDetails user) {

		ModelAndView errorView = new ModelAndView("error");
		errorView.addObject(PortalConstants.PORTALBASEURL, utils.getPortalBaseURL());
		errorView.addObject(PortalConstants.OVER_VIEW_PAGE_URL, utils.getBaseURL());
		errorView.addObject(PortalConstants.MULE_PUBLIC_LAUNCHURL, utils.getMulePublicLaunchUrl());
		HttpSession session = httpServletRequest.getSession(false);
		if (session != null) {
			Exception ex = (Exception) session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
			if (ex instanceof AuthenticationException) {
				errorView.addObject(PortalConstants.ERRORMESSAGE,
						"Trouble signing in? Please validate your credentials.");
				errorView.addObject(PortalConstants.ERRORCODE, "ALLOWED_LOGIN_ATTEMPTS_EXCEEDED");
			}
		}

		return errorView;
	}
	
	/**
	 * If Developer portal session login set the cookie
	 */
	private void loginCookieHandle(){
		
			Cookie cookie = new Cookie(PortalConstants.PORTAL_CUSTOM_SESSION_COOKIE, null);
			cookie.setSecure(true);
			cookie.setHttpOnly(true);
			httpServletResponse.addCookie(cookie);
	}
	
	/**
	 * This method finds Session Cookie for Portal Login cookie
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return {@link Cookie}
	 */
	public static Cookie getPortalLoginCookie(HttpServletRequest request) {

		Cookie[] cookies = request.getCookies();
		Cookie portalLoginCookie = null;
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equalsIgnoreCase(PortalConstants.PORTAL_CUSTOM_SESSION_COOKIE)) {
					portalLoginCookie = cookie;
					break;
				}
			}
		}
		return portalLoginCookie;

	}
	
	
}
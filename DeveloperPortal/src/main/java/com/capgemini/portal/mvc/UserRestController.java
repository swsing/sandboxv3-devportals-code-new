package com.capgemini.portal.mvc;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.portal.helper.HealthCheckHelper;
import com.capgemini.portal.helper.PortalHelper;
import com.capgemini.portal.model.ApiResponse;
import com.capgemini.portal.model.ChangePasswordRequest;
import com.capgemini.portal.model.ChangePasswordResponse;
import com.capgemini.portal.model.UserProfile;
import com.capgemini.portal.model.ValidateSessionResponse;
import com.capgemini.portal.security.user.SAMLUser;
import com.capgemini.portal.security.user.SAMLUserDetails;
import com.capgemini.portal.security.utilities.SecurityUtils;
import com.capgemini.portal.service.DevPortalService;

@RestController
public class UserRestController {

	@Value("${download.file.classpathUrl}")
	private String classpathUrl;

	@Autowired
	private SecurityUtils utils;

	@Autowired
	private HttpServletResponse httpServletResponse;

	@Autowired
	private HttpServletRequest httpServletRequest;

	@Autowired
	private DevPortalService devPortalService;

	@Autowired
	private HealthCheckHelper healthCheckHelper;

	@Autowired
	private PortalHelper portalHelper;

	@RequestMapping(value = "/healthCheck", method = RequestMethod.GET)
	@ResponseBody
	public Health healthCheck() {

		return healthCheckHelper.health();
	}

	@RequestMapping(value = "/auth/logout", method = RequestMethod.GET)
	public void logout(@SAMLUser SAMLUserDetails user) throws IOException {
		validateSession(user, httpServletRequest, httpServletResponse);
		devPortalService.logout(user, httpServletRequest, httpServletResponse);
		httpServletResponse.sendRedirect(utils.getPortalBaseURL());

	}

	@RequestMapping(value = "/auth/userProfile", method = RequestMethod.GET)
	public UserProfile getUserProfile(@SAMLUser SAMLUserDetails user, @RequestParam String emailId) {

		validateSession(user, httpServletRequest, httpServletResponse);
		portalHelper.validateUserEmailId(user, emailId);
		return devPortalService.fetchUserProfile(user);
	}

	@RequestMapping(value = "/auth/userProfile", method = RequestMethod.PUT)
	public ApiResponse updateUserProfile(@SAMLUser SAMLUserDetails user, @RequestBody UserProfile userProfile) {

		validateSession(user, httpServletRequest, httpServletResponse);
		portalHelper.validateUpdateUserProfileRequest(userProfile);
		return devPortalService.updateUserProfile(user, userProfile);
	}

	@RequestMapping(value = "/auth/session", method = RequestMethod.GET)
	public ValidateSessionResponse validateSession(@SAMLUser SAMLUserDetails user, HttpServletRequest request,
			HttpServletResponse response) {

		return devPortalService.validateSession(user, request, response);
	}

	@RequestMapping(value = "/auth/changePassword", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ChangePasswordResponse changePassword(@SAMLUser SAMLUserDetails user,
			@RequestBody ChangePasswordRequest cpRequest) {

		validateSession(user, httpServletRequest, httpServletResponse);
		portalHelper.validateChangePasswordRequest(user, cpRequest);

		return devPortalService.changePassword(user, cpRequest);
	}

	@RequestMapping(value = "/auth/downloadTestData", method = RequestMethod.GET, produces = {
	"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" })
	@ResponseBody
	public ResponseEntity<Object> downloadTestDataFile(@SAMLUser SAMLUserDetails user) {

		validateSession(user, httpServletRequest, httpServletResponse);

		return devPortalService.downloadTestDataFile(user);

	}
}

package com.capgemini.portal.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;

@RestController
public class ImplUIStaticController {
	
	@Autowired
	UIStaticContentUtilityController uiController;
	
	@CrossOrigin
	@GetMapping("/co/ui/staticContentGet")
	public String retrieveUIData() {
		return uiController.getStaticContentForUI();
	}
	
	@CrossOrigin
	@GetMapping("/co/ui/staticContentUpdate")
	public void updateUIData() {
		uiController.updateStaticContentoForUI();
	}
}

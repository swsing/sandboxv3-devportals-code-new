package com.capgemini.portal.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.portal.config.PortalValidationConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.model.ChangePasswordRequest;
import com.capgemini.portal.security.user.SAMLUserDetails;

@Component
public class PortalValidator {
	
	@Autowired
	private PortalValidationConfig portalValidationConfig;

	
	/**
	 * Validate the Change Password Request
	 * 
	 * @param user
	 * @param cpRequest
	 */
	public void validateChangePasswordRequest(SAMLUserDetails user, ChangePasswordRequest cpRequest) {
		
		if (user == null) {

			throw DeveloperPortalException.populateDeveloperPortalException(PortalConstants.SESSION_EXIPRED,PortalErrorCodeEnum.USER_SESSION_EXPIRED);
		}
		
		if(cpRequest == null || StringUtils.isEmpty(cpRequest.getNewPwd()) || StringUtils.isEmpty(cpRequest.getOldPwd()) || !(cpRequest.getNewPwd()!= null && cpRequest.getNewPwd().matches(portalValidationConfig.getChangePwdRegxString()))){
			
			throw DeveloperPortalException
			.populateDeveloperPortalException(PortalConstants.INVALID_CURRENT_AND_OLD_PWD_MSG, PortalErrorCodeEnum.ERROR_CHANGING_PASSWORD);
		}
	}
	
	
}

package com.capgemini.template.builder;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.mail.config.MailConfig;
import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.helper.PortalHelper;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.portal.mail.constants.MailConstants;
import com.capgemini.template.service.ChangePwdSTemplateService;

@Component
public class TemplateBuilder {
	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL
	 * __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 *  
	 * NOTICE:  All information contained herein is, and remains
	 * the property of CAPGEMINI GROUP.
	 * The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered
	 * by patents, patents in process, and are protected by trade secret
	 * or copyright law.
	 * Dissemination of this information or reproduction of this material
	 * is strictly forbidden unless prior written permission is obtained
	 * from CAPGEMINI GROUP.
	 ******************************************************************************/

	@Autowired
	MailConfig mailConfig;

	static Path templatePath = null;

	@Autowired
	ChangePwdSTemplateService changePwdSTemplateService;

	@Autowired
	private PortalLoggerUtils portalLoggerUtils;

	@Autowired
	private PortalAspectUtils portalAspectUtils;




	public enum Template {
		REGISTEREMAIL(MailConstants.REGISTER_EMAIL_TEMPLATE_CODE), RESENDACTIVATION(MailConstants.RESEND_ACTIVATION_LINK_EMAIL_TEMPLATE_CODE),CHANGEPASSWORD(MailConstants.CHANGE_PASSWORD_LINK_EMAIL_TEMPLATE_CODE);
		private String emailTemplate;	    


		Template(String emailTemplate){
			this.emailTemplate= emailTemplate;
		}

		public String getEmailTemplate() {
			return emailTemplate;
		}


	}


	public String getTemplate(Template template, Map<String, String> map) throws IOException {
		PortalLoggerAttribute loggerAttribute = portalLoggerUtils
				.populateLoggerData(PortalConstants.GET_TEMPLATE);
		switch(template){
		case CHANGEPASSWORD:

			return changePwdSTemplateService.enrichEmailTemplate(map, Template.CHANGEPASSWORD.getEmailTemplate());


		default:
			templatePath = null;
			break;

		}
		if (templatePath == null && mailConfig.getIsExceptionThrow()) {


			DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException("Email Template Not Found!!",
					PortalErrorCodeEnum.ERROR_GENRIC);
			portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, de);


		}
		return "";

	}


	//	public  Path resendActivation() throws IOException{
	//		if (MailConstants.CLASSPATH.equalsIgnoreCase(mailConfig.getResourceType()))
	//			templatePath = new ClassPathResource(mailConfig.getEmailTemplateResources()[1]).getFile().toPath();
	//		else
	//			templatePath = new UrlResource(mailConfig.getEmailTemplateResources()[1]).getFile().toPath();
	//		return templatePath;
	//
	//	}
	//
	//	public  Path registerEmail() throws IOException {
	//
	//		if (MailConstants.CLASSPATH.equalsIgnoreCase(mailConfig.getResourceType()))
	//			templatePath = new ClassPathResource(mailConfig.getEmailTemplateResources()[0]).getFile().toPath();
	//		new
	//		ClassPathResource(mailConfig.getEmailTemplateResources()[1]).getFile().toPath();
	//
	//		templatePath =Paths.get(mailConfig.getEmailTemplateResources()[0]);
	//		return templatePath;
	//	}

	//	public  Path changePassword() throws IOException {
	//
	//		if (MailConstants.CLASSPATH.equalsIgnoreCase(mailConfig.getResourceType()))
	//			templatePath = new ClassPathResource(mailConfig.getRelativePathChangePwd()).getFile().toPath();
	//		else
	//			templatePath = new UrlResource(mailConfig.getUrlChangePwd()).getFile().toPath();
	//
	//		return templatePath;
	//	}




}


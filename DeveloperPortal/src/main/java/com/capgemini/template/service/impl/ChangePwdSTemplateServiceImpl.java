package com.capgemini.template.service.impl;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import com.capgemini.mail.config.MailConfig;
import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.helper.PortalHelper;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.template.service.ChangePwdSTemplateService;

@Service
public class ChangePwdSTemplateServiceImpl implements ChangePwdSTemplateService{

	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL
	 * __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 *  
	 * NOTICE:  All information contained herein is, and remains
	 * the property of CAPGEMINI GROUP.
	 * The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered
	 * by patents, patents in process, and are protected by trade secret
	 * or copyright law.
	 * Dissemination of this information or reproduction of this material
	 * is strictly forbidden unless prior written permission is obtained
	 * from CAPGEMINI GROUP.
	 ******************************************************************************/
	@Autowired
	MailConfig mailConfig;

	@Autowired
	LdapTemplate ldapTemplate;

	@Autowired
	private PortalLoggerUtils portalLoggerUtils;

	@Autowired
	private PortalAspectUtils portalAspectUtils;




	@Override
	public Resource getTemplatePath(String emailTemplateCode){
		PortalLoggerAttribute loggerAttribute = portalLoggerUtils
				.populateLoggerData(PortalConstants.POPULATE_EMAIL_MIME_MESSAGE);
		try{

			if( !mailConfig.getRelativePathChangePwd().isEmpty() && mailConfig.getIsRelativePath().equals(Boolean.TRUE))
				return new ClassPathResource(mailConfig.getRelativePathChangePwd());

			else
				return new UrlResource(mailConfig.getUrlChangePwd());

		}
		catch(IOException | NullPointerException e){
			if(mailConfig.getIsExceptionThrow()){
				DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException(e.getMessage(),
						PortalErrorCodeEnum.ERROR_GENRIC);
				portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, de);
				throw de;
			}
			portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, e);
		}
		return null;

	}

	@Override
	public String getTemplateString(String emailTemplateCode) {
		String templateMessage = null;
		PortalLoggerAttribute loggerAttribute = portalLoggerUtils
				.populateLoggerData(PortalConstants.POPULATE_EMAIL_MIME_MESSAGE);
		/*
		 * URL urlObject = new URL("/"); URLConnection urlConnection =
		 * urlObject.openConnection(); InputStream inputStream =
		 * urlConnection.getInputStream(); String data =
		 * readFromInputStream(inputStream);
		 */

		try {
			/*Stream<String> stream = Files.lines(getTemplatePath(emailTemplateCode));
			stream.forEachOrdered(templateMessage::append);
			stream.close();*/
			templateMessage = StreamUtils.copyToString(getTemplatePath(emailTemplateCode).getInputStream(),Charset.forName("UTF-8"));
		} catch (IOException | DeveloperPortalException |NullPointerException e) {
			if(mailConfig.getIsExceptionThrow()){
				DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException(e.getMessage(),
						PortalErrorCodeEnum.ERROR_GENRIC);
				portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, de);
				throw de;
			}
			portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, e);
		}

		if (StringUtils.isBlank(templateMessage) && mailConfig.getIsExceptionThrow() ) {
				DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException("Email Tempalte content is empty.",
						PortalErrorCodeEnum.ERROR_GENRIC);
				portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, de);
			
		}
		return templateMessage;

	}

	@Override
	public String getDynamicFields() {
		return null;
	}

	@Override
	public List<String> getListOfAttributes() {
		return mailConfig.getListOfAttributes();
	}



	@Override
	public String replaceAll(String text, Map<String, String> params, List<String> dyFields) {
		PortalLoggerAttribute loggerAttribute = portalLoggerUtils
				.populateLoggerData(PortalConstants.REPLACE_ALL);
		for(String key  : dyFields){ 		
			if(StringUtils.isNotEmpty(key) && StringUtils.isNotEmpty(params.get(key))){
				try{
					text = text.replace("{"+key+"}", params.get(key));
				}
				catch(NullPointerException e){
					if(mailConfig.getIsExceptionThrow()){
						DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException(PortalConstants.USER_ID_NULL,
								PortalErrorCodeEnum.ERROR_SENDIND_MAIL);
						portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, de);
						throw de;
					}
					portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, e);
				}
			}
		}
		return text;
	}

	@Override
	public String enrichEmailTemplate(Map<String, String> map, String emailTemplateCode) {

		String[] dyFields = {PortalConstants.FIRSTNAME, PortalConstants.CHANGE_PWD_COMPLETE_OPERATION_MESSAGE};
		return replaceAll(getTemplateString(emailTemplateCode),map,  Arrays.asList(dyFields));
		
	}



}

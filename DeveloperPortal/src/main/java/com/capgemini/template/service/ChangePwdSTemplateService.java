package com.capgemini.template.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.springframework.core.io.Resource;


public interface ChangePwdSTemplateService {

	public Resource getTemplatePath(String emailTemplateCode) throws IOException;
	public String getTemplateString(String emailTemplateCode);
	public String getDynamicFields();
	public List<String> getListOfAttributes();
	public String replaceAll(String text, Map<String, String> params, List<String> dyFields);
	public String enrichEmailTemplate(Map<String, String> map, String emailTemplateCode);
	

}

package com.capgemini.mail.service.impl;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.capgemini.mail.config.MailConfig;
import com.capgemini.mail.service.MailService;
import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.DeveloperPortalException;
import com.capgemini.portal.helper.PortalHelper;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.portal.mail.constants.MailConstants;

@Service
public class MailServiceImpl implements MailService {
	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 * 
	 * NOTICE: All information contained herein is, and remains the property of
	 * CAPGEMINI GROUP. The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered by patents, patents
	 * in process, and are protected by trade secret or copyright law.
	 * Dissemination of this information or reproduction of this material is
	 * strictly forbidden unless prior written permission is obtained from
	 * CAPGEMINI GROUP.
	 ******************************************************************************/

	@Autowired
	MailConfig mailConfig;

	@Autowired
	private PortalLoggerUtils portalLoggerUtils;

	@Autowired
	private PortalAspectUtils portalAspectUtils;

	@Autowired
	JavaMailSender javaMailSender;



	public MimeMessage populateEmailMimeMessage(String uid, String validateHTML, String messageSubject) {

		PortalLoggerAttribute loggerAttribute = portalLoggerUtils
				.populateLoggerData(PortalConstants.POPULATE_EMAIL_MIME_MESSAGE);
		MimeMessage msg = javaMailSender.createMimeMessage();
		try {

			msg.setFrom(new InternetAddress(mailConfig.getSenderAddress()));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(uid));
			msg.setSubject(messageSubject);
			msg.setContent(validateHTML, MailConstants.CONTENT_TYPE_TEXT_HTML);

			// Add a configuration set header. Comment or delete the
			// next line if you are not using a configuration set
			// msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);

		} catch (MessagingException | MailSendException | DeveloperPortalException e) {
			if(mailConfig.getIsExceptionThrow()){
				DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException(e.getMessage(),
						PortalErrorCodeEnum.ERROR_SENDIND_MAIL);
				portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, de);
				throw de;

			}
			portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, e);
		}
		return msg;
	}

	@Override
	public void sendEmail(String uid, String validateHTML, String messageSubject) {

		PortalLoggerAttribute loggerAttribute = portalLoggerUtils.populateLoggerData(PortalConstants.SEND_EMAIL);
		try {

			MimeMessage msg = populateEmailMimeMessage(uid, validateHTML,messageSubject);
			javaMailSender.send(msg);

		} catch (MailSendException | DeveloperPortalException e) {
			if(mailConfig.getIsExceptionThrow()){


				DeveloperPortalException de = DeveloperPortalException.populateDeveloperPortalException(e.getMessage(),
						PortalErrorCodeEnum.ERROR_SENDIND_MAIL);
				portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, de);
				throw de;
			}
			portalAspectUtils.exceptionPayload(PortalHelper.class.getName(), loggerAttribute, e);
		}
	}


}

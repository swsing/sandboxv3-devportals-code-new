package com.capgemini.mail.service;

import javax.mail.internet.MimeMessage;


public interface MailService {
	
	public MimeMessage populateEmailMimeMessage(String uid, String validateHTML, String messageSubject);
	
	public void sendEmail(String uid, String validateHTML, String messageSubject);
}

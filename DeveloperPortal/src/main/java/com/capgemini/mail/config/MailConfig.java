package com.capgemini.mail.config;

import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.capgemini.portal.mail.constants.MailConstants;

@Configuration
public class MailConfig {
	/*******************************************************************************
	 * CAPGEMINI CONFIDENTIAL
	 * __________________
	 * 
	 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
	 *  
	 * NOTICE:  All information contained herein is, and remains
	 * the property of CAPGEMINI GROUP.
	 * The intellectual and technical concepts contained herein
	 * are proprietary to CAPGEMINI GROUP and may be covered
	 * by patents, patents in process, and are protected by trade secret
	 * or copyright law.
	 * Dissemination of this information or reproduction of this material
	 * is strictly forbidden unless prior written permission is obtained
	 * from CAPGEMINI GROUP.
	 ******************************************************************************/

	@Value("${mail.contentType}")
	private String contentType;

	@Value("${mail.smtp.auth}")
	private boolean smtpAuth;

	@Value("${mail.smtp.starttls.enable}")
	private boolean smtpStarttlsEnable;

	@Value("${mail.smtp.host}")
	private String smtpHost;

	@Value("${mail.smtp.port}")
	private String smtpPort;

	@Value("${mail.smtp.username}")
	private String userName;

	@Value("${mail.smtp.password}")
	private String password;

	@Value("${mail.transport.protocol}")
	private String transportProtocol;

	@Value("${mail.templateDetails.resource}")
	private String[] emailTemplateResources;

	@Value("${mail.templateDetails.resourceType}")
	private String resourceType;

	@Value("${mail.smtp.ssl.trust}")
	private String sslTrust;

	@Value("${mail.senderAddress}")
	private String senderAddress;

	@Value("${mail.messageSubject}")
	private String defaultMessageSubject;

	@Value("${mail.template.ChangePaswordSuccess.path.relativePath}")
	private String relativePathChangePwd;

	@Value("${mail.template.ChangePaswordSuccess.path.URL}")
	private String urlChangePwd;

	@Value("${mail.template.ChangePaswordSuccess.path.isRelativePath}")
	private Boolean isRelativePath;

	@Value("${mail.template.ChangePaswordSuccess.changedMsg}")
	private String changedMsg;

	@Value("${mail.template.ChangePaswordSuccess.messageSubject}")
	private String changePwdSuccessMsgSubject;

	@Value("${mail.template.ChangePaswordSuccess.listOfAttributes}")
	private List listOfAttributes;

	@Value("${mail.template.ChangePaswordSuccess.isExceptionThrow}")
	private Boolean isExceptionThrow;

	@Value("${mail.smtp.isAsynchronousCall}")
	private Boolean isAsyncCall;


	public List getListOfAttributes() {
		return listOfAttributes;
	}



	public void setListOfAttributes(List listOfAttributes) {
		this.listOfAttributes = listOfAttributes;
	}



	public Boolean getIsExceptionThrow() {
		return isExceptionThrow;
	}



	public void setIsExceptionThrow(Boolean isExceptionThrow) {
		this.isExceptionThrow = isExceptionThrow;
	}



	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}




	public String getRelativePathChangePwd() {
		return relativePathChangePwd;
	}



	public void setRelativePathChangePwd(String relativePathChangePwd) {
		this.relativePathChangePwd = relativePathChangePwd;
	}



	public String getUrlChangePwd() {
		return urlChangePwd;
	}



	public Boolean getIsAsyncCall() {
		return isAsyncCall;
	}



	public void setIsAsyncCall(Boolean isAsyncCall) {
		this.isAsyncCall = isAsyncCall;
	}



	public void setUrlChangePwd(String urlChangePwd) {
		this.urlChangePwd = urlChangePwd;
	}



	public Boolean getIsRelativePath() {
		return isRelativePath;
	}

	public void setIsRelativePath(Boolean isRelativePath) {
		this.isRelativePath = isRelativePath;
	}

	public void setEmailTemplateResources(String[] emailTemplateResources) {
		this.emailTemplateResources = emailTemplateResources;
	}

	/**
	 * @param contentType
	 *            the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the smtpAuth
	 */
	public boolean isSmtpAuth() {
		return smtpAuth;
	}

	/**
	 * @param smtpAuth
	 *            the smtpAuth to set
	 */
	public void setSmtpAuth(boolean smtpAuth) {
		this.smtpAuth = smtpAuth;
	}

	/**
	 * @return the smtpStarttlsEnable
	 */
	public boolean isSmtpStarttlsEnable() {
		return smtpStarttlsEnable;
	}

	/**
	 * @param smtpStarttlsEnable
	 *            the smtpStarttlsEnable to set
	 */
	public void setSmtpStarttlsEnable(boolean smtpStarttlsEnable) {
		this.smtpStarttlsEnable = smtpStarttlsEnable;
	}

	/**
	 * @return the smtpHost
	 */
	public String getSmtpHost() {
		return smtpHost;
	}

	/**
	 * @param smtpHost
	 *            the smtpHost to set
	 */
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	/**
	 * @return the smtpPort
	 */
	public String getSmtpPort() {
		return smtpPort;
	}

	/**
	 * @param smtpPort
	 *            the smtpPort to set
	 */
	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the transportProtocol
	 */
	public String getTransportProtocol() {
		return transportProtocol;
	}

	/**
	 * @param transportProtocol
	 *            the transportProtocol to set
	 */
	public void setTransportProtocol(String transportProtocol) {
		this.transportProtocol = transportProtocol;
	}

	/**
	 * 
	 * /** Returns the email messsage template resource file path string array,
	 * 
	 * @return String [] Email Message Template resource file path string
	 */
	public String[] getEmailTemplateResources() {
		return emailTemplateResources;
	}

	/**
	 * Sets the MailTemplate Details resources file path String,
	 * 
	 * @param emailTemplateResources
	 *            email message template resource
	 */
	public void setMailTemplateDetailsResource(String[] emailTemplateResources) {
		this.emailTemplateResources = emailTemplateResources;
	}

	/**
	 * @return the resourceType
	 */
	public String getResourceType() {
		return resourceType;
	}

	/**
	 * @param resourceType
	 *            the resourceType to set
	 */
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * @return the sslTrust
	 */
	public String getSslTrust() {
		return sslTrust;
	}

	/**
	 * @param sslTrust
	 *            the sslTrust to set
	 */
	public void setSslTrust(String sslTrust) {
		this.sslTrust = sslTrust;
	}

	/**
	 * @return the senderAddress
	 */
	public String getSenderAddress() {
		return senderAddress;
	}

	/**
	 * @param senderAddress
	 *            the senderAddress to set
	 */
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}



	/**
	 * @return the defaultMessageSubject
	 */
	public String getDefaultMessageSubject() {
		return defaultMessageSubject;
	}



	/**
	 * @param defaultMessageSubject the defaultMessageSubject to set
	 */
	public void setDefaultMessageSubject(String defaultMessageSubject) {
		this.defaultMessageSubject = defaultMessageSubject;
	}



	/**
	 * @return the changePwdSuccessMsgSubject
	 */
	public String getChangePwdSuccessMsgSubject() {
		return changePwdSuccessMsgSubject;
	}



	/**
	 * @param changePwdSuccessMsgSubject the changePwdSuccessMsgSubject to set
	 */
	public void setChangePwdSuccessMsgSubject(String changePwdSuccessMsgSubject) {
		this.changePwdSuccessMsgSubject = changePwdSuccessMsgSubject;
	}

	/**
	 * @return the changedMsg
	 */
	public String getChangedMsg() {
		return changedMsg;
	}

	/**
	 * @param changedMsg the changedMsg to set
	 */
	public void setChangedMsg(String changedMsg) {
		this.changedMsg = changedMsg;
	}



	@Bean
	public JavaMailSender javaMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setPort(new Integer(getSmtpPort()).intValue());
		mailSender.setUsername(getUserName());
		mailSender.setPassword(getPassword());
		mailSender.setHost(getSmtpHost());

		// Create a Properties object to contain connection configuration
		// information.
		Properties props = mailSender.getJavaMailProperties();
		props.put(MailConstants.MAIL_SMTP_HOST, getSmtpHost());
		props.put(MailConstants.MAIL_TRANSPORT_PROTOCOL, getTransportProtocol());
		//props.put(MailConstants.MAIL_SMTP_PORT, getSmtpPort());
		props.put(MailConstants.MAIL_SMTP_STARTTLE_ENABLE, isSmtpAuth());
		props.put(MailConstants.MAIL_SMTP_AUTH, isSmtpAuth());
		props.put(MailConstants.MAIL_SMTP_SSL_TRUST, getSslTrust());

		mailSender.setJavaMailProperties(props);

		return mailSender;
	}


}

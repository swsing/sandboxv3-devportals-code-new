package com.capgemini.portal.security.config;

import java.util.List;

import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.util.resource.ResourceException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.saml.SAMLBootstrap;
import org.springframework.security.saml.context.SAMLContextProviderImpl;
import org.springframework.security.saml.log.SAMLDefaultLogger;
import org.springframework.security.saml.metadata.CachingMetadataManager;
import org.springframework.security.saml.parser.ParserPoolHolder;
import org.springframework.security.saml.storage.EmptyStorageFactory;
import org.springframework.security.saml.websso.SingleLogoutProfile;
import org.springframework.security.saml.websso.SingleLogoutProfileImpl;
import org.springframework.security.saml.websso.WebSSOProfile;
import org.springframework.security.saml.websso.WebSSOProfileConsumer;
import org.springframework.security.saml.websso.WebSSOProfileConsumerHoKImpl;
import org.springframework.security.saml.websso.WebSSOProfileConsumerImpl;
import org.springframework.security.saml.websso.WebSSOProfileECPImpl;
import org.springframework.security.saml.websso.WebSSOProfileHoKImpl;
import org.springframework.security.saml.websso.WebSSOProfileImpl;

@Configuration
public class SAMLConfigDefaults {
	
	@Bean
	public static SAMLBootstrap sAMLBootstrap() {
		return new SAMLBootstrap();
	}

	@Bean
	public ParserPoolHolder parserPoolHolder() {
		return new ParserPoolHolder();
	}

	@Bean
	public SAMLContextProviderImpl contextProvider() {
		SAMLContextProviderImpl samlContextProviderImpl = new SAMLContextProviderImpl();
    	samlContextProviderImpl.setStorageFactory(emptyStorageFactory());
        return samlContextProviderImpl;
    }
    
    @Bean
    public EmptyStorageFactory emptyStorageFactory() {
		return new EmptyStorageFactory();
    }

	@Bean
	public SAMLDefaultLogger samlLogger() {
		return new SAMLDefaultLogger();
	}
	
	@Bean
	public SingleLogoutProfile logoutProfile() {
		return new SingleLogoutProfileImpl();
	}

	@Bean
	public CachingMetadataManager metadataManager(List<MetadataProvider> metadataProviders)
			throws MetadataProviderException, ResourceException {
		return new CachingMetadataManager(metadataProviders);
	}
	
	@Bean
	public WebSSOProfileConsumer webSSOprofileConsumer() {
		
		WebSSOProfileConsumerImpl webSSOProfileConsumerImpl = new WebSSOProfileConsumerImpl();
		
		//Fix for defect PSD-1798
		// Skew time added to handle time difference between IDP and SP
		webSSOProfileConsumerImpl.setResponseSkew(200);
		return webSSOProfileConsumerImpl;
	}

	@Bean
	public WebSSOProfileConsumerHoKImpl hokWebSSOprofileConsumer() {
		return new WebSSOProfileConsumerHoKImpl();
	}

	@Bean
	public WebSSOProfile webSSOprofile() {
		return new WebSSOProfileImpl();
	}

	@Bean
	public WebSSOProfileECPImpl ecpProfile() {
		return new WebSSOProfileECPImpl();
	}

	@Bean
	public WebSSOProfileHoKImpl hokWebSSOProfile() {
		return new WebSSOProfileHoKImpl();
	}
}

package com.capgemini.portal.security.utilities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SecurityUtils {
	
	@Value("${server.port}")
	private String port;

	@Value("${idp.url.metadata}")
	private String idpMetadataUrl;
	
	@Value("${portal.hostname}")
	private String portalHostname;
	
	@Value("${portal.baseURL}")
	private String baseURL;
	
	@Value("${idp.hostname}")
	private String idpHostname;
	
	@Value("${idp.isSLOEnable}")
	private boolean isSLOEnable;
	
	@Value("${portal.entity-id}")
	private String portalEntityId;
	
	@Value("${idp.url.logout}")
	private String idpLogoutUrl;
	
	@Value("${idp.url.localLogout}")
	private String localLogoutUrl;
	
	@Value("${mule.entity-id}")
	private String muleEntityId;
	
	@Value("${mule.url.launch}")
	private String muleLaunchUrl;
	
	@Value("${pf.internalDNS}")
	private String pfInternalDns;
	
	@Value("${userGroupDn}")
	private String userGroupDn;
	
	@Value("${entityBaseURL}")
	private String entityBaseURL;
	
	@Value("${mule.url.publicLaunch}")
	private String mulePublicLaunchUrl;
	
	
	
    public String getPortalEntityId(){
    	return String.format(portalEntityId, portalHostname);
    }
    
    public String getPortalBaseURL(){
    	return "https://" + portalHostname;
    }
    
    public String getIDPMetadataURL(){
    	return String.format(idpMetadataUrl, pfInternalDns, getPortalEntityId());
    }
    
    public String getIDPLogoutURL(){
    	return String.format(idpLogoutUrl, idpHostname, getPortalEntityId());
    }
    
    public String getMuleLaunchURL(){
    	return String.format(muleLaunchUrl, idpHostname, muleEntityId);
    }
    
    public String getUserGroupDn(){
    	return userGroupDn;
    }
    
    public String getEntityBaseURL(){
    	return entityBaseURL;
    }

	

	/**
	 * @return the baseURL
	 */
	public String getBaseURL() {
		return "https://" + baseURL;
	}

	/**
	 * @return the isSLOEnable
	 */
	public boolean isSLOEnable() {
		return isSLOEnable;
	}

	/**
	 * @param isSLOEnable the isSLOEnable to set
	 */
	public void setSLOEnable(boolean isSLOEnable) {
		this.isSLOEnable = isSLOEnable;
	}

	/**
	 * @return the localLogoutUrl
	 */
	public String getLocalLogoutUrl() {
		return localLogoutUrl;
	}

	/**
	 * @param localLogoutUrl the localLogoutUrl to set
	 */
	public void setLocalLogoutUrl(String localLogoutUrl) {
		this.localLogoutUrl = localLogoutUrl;
	}

	/**
	 * @return the mulePublicLaunchUrl
	 */
	public String getMulePublicLaunchUrl() {
		return mulePublicLaunchUrl;
	}

	/**
	 * @param mulePublicLaunchUrl the mulePublicLaunchUrl to set
	 */
	public void setMulePublicLaunchUrl(String mulePublicLaunchUrl) {
		this.mulePublicLaunchUrl = mulePublicLaunchUrl;
	}
    
    
    
}

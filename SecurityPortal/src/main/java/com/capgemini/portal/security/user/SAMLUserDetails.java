package com.capgemini.portal.security.user;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.opensaml.saml2.core.Attribute;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.XSAny;
import org.opensaml.xml.schema.XSString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.saml.SAMLCredential;

public class SAMLUserDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6300623175939704488L;
	
	private SAMLCredential samlCredential;
	
	public SAMLUserDetails() {
		
	}

	public SAMLUserDetails(SAMLCredential samlCredential) {
		this.samlCredential = samlCredential;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));
	}

	@Override
	public String getPassword() {
		return "";
	}

	@Override
	public String getUsername() {
		return samlCredential.getNameID().getValue();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getAttribute(String name) {
		return samlCredential.getAttributeAsString(name);
	}

	public Map<String, String> getAttributes() {
		return samlCredential.getAttributes().stream().collect(Collectors.toMap(Attribute::getName, this::getValue));
	}

	private String getValue(Attribute attribute) {
		int index=0;
		List<XMLObject> attributeValues = attribute.getAttributeValues();
		if (attributeValues.isEmpty()) {
			return null;
		}
		StringBuilder value=new StringBuilder();
		Iterator<XMLObject> itr = attributeValues.iterator();
		while(itr.hasNext()){
			XMLObject xmlValue = itr.next();
			if(index>0)
				value= value.append(",").append(getString(xmlValue));
			else
				value = value.append(getString(xmlValue));
			index++;
		}
		return value.toString();
	}

	private String getString(XMLObject xmlValue) {
		if (xmlValue instanceof XSString) {
			return ((XSString) xmlValue).getValue();
		} else if (xmlValue instanceof XSAny) {
			return ((XSAny) xmlValue).getTextContent();
		} else {
			return null;
		}
	}
}

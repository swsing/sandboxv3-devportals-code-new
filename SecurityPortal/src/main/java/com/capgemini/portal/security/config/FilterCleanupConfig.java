package com.capgemini.portal.security.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterCleanupConfig {
	@Bean
	public static BeanDefinitionRegistryPostProcessor removeUnwantedAutomaticFilterRegistration() {
		return new BeanDefinitionRegistryPostProcessor() {
			@Override
			public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
			}

			@Override
			public void postProcessBeanFactory(ConfigurableListableBeanFactory bf) throws BeansException {
				DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) bf;
				Set<String> filtersToDisable = new HashSet<String>(Arrays.asList(new String[] { "samlEntryPoint", "samlFilter",
						"metadataDisplayFilter", "samlWebSSOHoKProcessingFilter", "samlWebSSOProcessingFilter",
						"samlLogoutProcessingFilter", "samlLogoutFilter", "metadataGeneratorFilter" }));
				Arrays.stream(beanFactory.getBeanNamesForType(javax.servlet.Filter.class))
						.filter(filtersToDisable::contains).forEach(name -> {
							BeanDefinition definition = BeanDefinitionBuilder
									.genericBeanDefinition(
											org.springframework.boot.web.servlet.FilterRegistrationBean.class)
									.setScope(BeanDefinition.SCOPE_SINGLETON).addConstructorArgReference(name)
									.addConstructorArgValue(
											new org.springframework.boot.web.servlet.ServletRegistrationBean[] {})
									.addPropertyValue("enabled", false).getBeanDefinition();
							beanFactory.registerBeanDefinition(name + "FilterRegistrationBean", definition);
						});
			}
		};
	}
}

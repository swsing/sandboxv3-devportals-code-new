package com.capgemini.portal.security.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.saml.SAMLEntryPoint;
import org.springframework.security.saml.SAMLLogoutFilter;
import org.springframework.security.saml.SAMLLogoutProcessingFilter;
import org.springframework.security.saml.SAMLProcessingFilter;
import org.springframework.security.saml.SAMLWebSSOHoKProcessingFilter;
import org.springframework.security.saml.metadata.MetadataDisplayFilter;
import org.springframework.security.saml.metadata.MetadataGeneratorFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import com.capgemini.portal.logger.PortalFilter;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private SAMLLogoutFilter samlLogoutFilter;

    @Autowired
    private SAMLLogoutProcessingFilter samlLogoutProcessingFilter;
    
    @Autowired
    private MetadataDisplayFilter metadataDisplayFilter;

    @Autowired
    private MetadataGeneratorFilter metadataGeneratorFilter;
    
    @Autowired
    private SAMLProcessingFilter samlWebSSOProcessingFilter;

    @Autowired
    private SAMLWebSSOHoKProcessingFilter samlWebSSOHoKProcessingFilter;

    @Autowired
    private SAMLEntryPoint samlEntryPoint;

    @Autowired
    private AuthenticationManager authenticationManager;

    private SecurityProperties security;
    
    @Autowired
    private PortalFilter portalFilter;
    

   	protected SecurityConfiguration(SecurityProperties security) {
   		this.security = security;
   	}
    /**
     * Defines the web based security configuration.
     *
     * @param http It allows configuring web based security for specific http requests.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	HttpSessionSecurityContextRepository securityContextRepository = new HttpSessionSecurityContextRepository();
        securityContextRepository.setSpringSecurityContextKey("SPRING_SECURITY_CONTEXT_SAML");
        http
                .securityContext()
                .securityContextRepository(securityContextRepository);
        http
                .csrf()
                .disable();
        http
               .addFilterAfter(portalFilter,AbstractPreAuthenticatedProcessingFilter.class)
        		.addFilterAfter(metadataGeneratorFilter, BasicAuthenticationFilter.class)
                .addFilterAfter(metadataDisplayFilter, MetadataGeneratorFilter.class)
                .addFilterAfter(samlEntryPoint, MetadataDisplayFilter.class)
                .addFilterAfter(samlWebSSOProcessingFilter, SAMLEntryPoint.class)
                .addFilterAfter(samlWebSSOHoKProcessingFilter, SAMLProcessingFilter.class)
                .addFilterAfter(samlLogoutProcessingFilter, SAMLWebSSOHoKProcessingFilter.class)
                .addFilterAfter(samlLogoutFilter, LogoutFilter.class);
        http
        .authorizeRequests()
        
        	.antMatchers(getSecureApplicationPaths()).permitAll()
        
        	.anyRequest().authenticated();
               
        http
                .exceptionHandling()
                .authenticationEntryPoint(samlEntryPoint);
        http.headers().cacheControl();
    }

    @Override
    public void init(WebSecurity web) throws Exception {
        super.init(web);
    }

    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return authenticationManager;
    }
    
    private String[] getSecureApplicationPaths() {
		List<String> list = new ArrayList<String>();
		for (String path : this.security.getBasic().getPath()) {
			path = (path == null ? "" : path.trim());
			if (path.equals("/**")) {
				return new String[] { path };
			}
			if (!path.equals("")) {
				list.add(path);
			}
		}
		return list.toArray(new String[list.size()]);
	}
	
	@Override
    public void configure(WebSecurity web) throws Exception {
		for (String antPatterns : this.security.getIgnored()) {
			if(StringUtils.isNoneBlank(antPatterns))
				web.ignoring().antMatchers(antPatterns);
		}
    } 
   
}
package com.capgemini.portal.security.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

import org.apache.commons.httpclient.HttpClient;
import org.apache.velocity.app.VelocityEngine;
import org.opensaml.saml2.metadata.provider.HTTPMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.xml.parse.StaticBasicParserPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.saml.SAMLAuthenticationProvider;
import org.springframework.security.saml.SAMLEntryPoint;
import org.springframework.security.saml.SAMLLogoutFilter;
import org.springframework.security.saml.SAMLLogoutProcessingFilter;
import org.springframework.security.saml.SAMLProcessingFilter;
import org.springframework.security.saml.SAMLWebSSOHoKProcessingFilter;
import org.springframework.security.saml.key.JKSKeyManager;
import org.springframework.security.saml.key.KeyManager;
import org.springframework.security.saml.metadata.ExtendedMetadata;
import org.springframework.security.saml.metadata.ExtendedMetadataDelegate;
import org.springframework.security.saml.metadata.MetadataDisplayFilter;
import org.springframework.security.saml.metadata.MetadataGenerator;
import org.springframework.security.saml.metadata.MetadataGeneratorFilter;
import org.springframework.security.saml.processor.HTTPPostBinding;
import org.springframework.security.saml.processor.HTTPRedirectDeflateBinding;
import org.springframework.security.saml.processor.SAMLBinding;
import org.springframework.security.saml.processor.SAMLProcessorImpl;
import org.springframework.security.saml.trust.httpclient.TLSProtocolConfigurer;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.security.saml.util.VelocityFactory;
import org.springframework.security.saml.websso.WebSSOProfileOptions;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.security.utilities.SecurityUtils;

@AutoConfigureBefore(SecurityConfiguration.class)
@Configuration
public class SAMLConfig {

	@Value("${app.key-alias}")
	private String keyAlias;

	@Value("${server.ssl.key-store-password}")
	private String keyPassword;

	@Value("${server.ssl.key-store}")
	private String keyStoreFilePath;
	
	private static final String DEFAULT_TARGET_URL = "/login";

	@Autowired
	SecurityUtils utils;

	@Autowired
	private SAMLUserDetailsService samlUserDetailsService;

	@Bean
	public SAMLAuthenticationProvider samlAuthenticationProvider() {
		SAMLAuthenticationProvider provider = new SAMLAuthenticationProvider();
		provider.setUserDetails(samlUserDetailsService);
		provider.setForcePrincipalAsString(false);
		return provider;
	}

	@Bean
	public AuthenticationManager authenticationManager() {
		return new ProviderManager(Collections.singletonList(samlAuthenticationProvider()));
	}

	@Bean(initMethod = "initialize")
	public StaticBasicParserPool parserPool() {
		return new StaticBasicParserPool();
	}

	@Bean
	public SAMLProcessorImpl processor() {

		VelocityEngine velocityEngine = VelocityFactory.getEngine();
		Collection<SAMLBinding> bindings = new ArrayList<SAMLBinding>();
		bindings.add(new HTTPRedirectDeflateBinding(parserPool()));
		bindings.add(new HTTPPostBinding(parserPool(), velocityEngine));
		return new SAMLProcessorImpl(bindings);
	}

	@Bean
	public SimpleUrlLogoutSuccessHandler successLogoutHandler() {
		SimpleUrlLogoutSuccessHandler handler = new SimpleUrlLogoutSuccessHandler();
		handler.setDefaultTargetUrl(DEFAULT_TARGET_URL);
		return handler;
	}

	
	@Bean
	public SecurityContextLogoutHandler logoutHandler() {
		SecurityContextLogoutHandler handler = new SecurityContextLogoutHandler();
		handler.setInvalidateHttpSession(true);
		handler.setClearAuthentication(true);
		return handler;
	}

	@Bean
	public CookieClearingLogoutHandler cookielogoutHandler(){
		return new CookieClearingLogoutHandler(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE, PortalConstants.PORTAL_CUSTOM_SESSION_COOKIE);
	}
	
	@Bean
	public SAMLLogoutFilter samlLogoutFilter() {
		SAMLLogoutFilter filter = new SAMLLogoutFilter(successLogoutHandler(), new LogoutHandler[] { logoutHandler(),cookielogoutHandler() },
				new LogoutHandler[] { logoutHandler() ,cookielogoutHandler()});
		filter.setFilterProcessesUrl("/saml/logout");
		return filter;
	}

	@Bean
	public SAMLLogoutProcessingFilter samlLogoutProcessingFilter() {
		SAMLLogoutProcessingFilter filter = new SAMLLogoutProcessingFilter(successLogoutHandler(), logoutHandler(),cookielogoutHandler());
		filter.setFilterProcessesUrl("/saml/SingleLogout");
		return filter;
	}

	@Bean
	public MetadataGeneratorFilter metadataGeneratorFilter(MetadataGenerator metadataGenerator) {
		metadataGenerator.setEntityBaseURL(utils.getEntityBaseURL());
		return new MetadataGeneratorFilter(metadataGenerator);
	}

	@Bean
	public MetadataDisplayFilter metadataDisplayFilter() throws Exception {
		MetadataDisplayFilter filter = new MetadataDisplayFilter();
		filter.setFilterProcessesUrl("/saml/metadata");
		return filter;
	}

	/**
	 * The HTTP Client used to communicate with the IDP.
	 *
	 * @return The http client
	 * @see HttpClient
	 */
	@Bean
	public HttpClient httpClient() {
		return new HttpClient();
	}

	/**
	 * Setup the extended metadata delegate for the IDP.
	 *
	 * @param properties
	 *            The SAML properties
	 * @return Metadata provider configured via the url.
	 * @throws MetadataProviderException
	 *             On any configuration error
	 * @see ExtendedMetadataDelegate
	 * @see HTTPMetadataProvider
	 */
	@Bean
	public ExtendedMetadataDelegate extendedMetadataProvider() throws MetadataProviderException {

		ExtendedMetadataDelegate extendedMetadataDelegate = null;
		try {

			String metadataFilePath = utils.getIDPMetadataURL();
			try {

				final Timer backgroundTaskTimer = new Timer(true);
				final HTTPMetadataProvider httpMetadataProvider = new HTTPMetadataProvider(backgroundTaskTimer,
						httpClient(), metadataFilePath);
				httpMetadataProvider.setParserPool(parserPool());

				ExtendedMetadata extendedMetadata = extendedMetadata().clone();
				extendedMetadataDelegate = new ExtendedMetadataDelegate(httpMetadataProvider, extendedMetadata);
				extendedMetadataDelegate.setMetadataTrustCheck(false);
				extendedMetadataDelegate.setMetadataRequireSignature(false);

			} catch (Exception e) {
				throw new IllegalStateException("Unable to initialize IDP Metadata", e);
			}
		} catch (Exception e) {
			throw new IllegalStateException("Unable to initialize IDP Metadata", e);
		}
		return extendedMetadataDelegate;
	}

	/**
	 * Setup the extended metadata for the SAML request.
	 *
	 * @return The extended metadata
	 * @see ExtendedMetadata
	 */
	@Bean
	public ExtendedMetadata extendedMetadata() {
		ExtendedMetadata extendedMetadata = new ExtendedMetadata();
		extendedMetadata.setRequireLogoutRequestSigned(true);
		extendedMetadata.setRequireLogoutResponseSigned(true);
		return extendedMetadata;
	}

	@Bean
	public MetadataGenerator metadataGenerator(KeyManager keyManager) {
		MetadataGenerator generator = new MetadataGenerator();
		generator.setEntityId(utils.getPortalEntityId());
		generator.setExtendedMetadata(extendedMetadata());
		generator.setIncludeDiscoveryExtension(false);
		generator.setKeyManager(keyManager);
		generator.setEntityBaseURL(utils.getPortalBaseURL()+DEFAULT_TARGET_URL);
		return generator;
	}

	@Bean(name = "samlWebSSOProcessingFilter")
	public SAMLProcessingFilter samlWebSSOProcessingFilter() throws Exception {
		SAMLProcessingFilter filter = new SAMLProcessingFilter();
		filter.setAuthenticationManager(authenticationManager());
		filter.setAllowSessionCreation(false); // Added for SLO testing
		filter.setAuthenticationSuccessHandler(successRedirectHandler());
		filter.setAuthenticationFailureHandler(authenticationFailureHandler());
		filter.setFilterProcessesUrl("/saml/SSO");
		return filter;
	}

	@Bean
	public SAMLWebSSOHoKProcessingFilter samlWebSSOHoKProcessingFilter() throws Exception {
		SAMLWebSSOHoKProcessingFilter filter = new SAMLWebSSOHoKProcessingFilter();
		filter.setAuthenticationSuccessHandler(successRedirectHandler());
		filter.setAuthenticationManager(authenticationManager());
		filter.setAuthenticationFailureHandler(authenticationFailureHandler());
		return filter;
	}

	@Bean
	public SavedRequestAwareAuthenticationSuccessHandler successRedirectHandler() {
		SavedRequestAwareAuthenticationSuccessHandler handler = new SavedRequestAwareAuthenticationSuccessHandler();
		handler.setDefaultTargetUrl(DEFAULT_TARGET_URL);
		handler.setAlwaysUseDefaultTargetUrl(true);
		return handler;
	}

	@Bean
	public SimpleUrlAuthenticationFailureHandler authenticationFailureHandler() {
		SimpleUrlAuthenticationFailureHandler handler = new SimpleUrlAuthenticationFailureHandler();
		handler.setUseForward(false);
		handler.setDefaultFailureUrl("/authError");
		return handler;
	}

	@Bean
	public SAMLEntryPoint samlEntryPoint() {
		WebSSOProfileOptions options = new WebSSOProfileOptions();
		options.setIncludeScoping(false);
		/** Requirement to login again with credentials, Developer Portal application can create a SAML2 Auth request with force auth and send to the PF IDP. Then user would be promoted for login page. */
		options.setForceAuthN(true);
		SAMLEntryPoint entryPoint = new SAMLEntryPoint();
		entryPoint.setDefaultProfileOptions(options);
		entryPoint.setFilterProcessesUrl("/saml/login");
		return entryPoint;
	}

	@Bean
	public KeyManager keyManager() {
		Resource keystore = new ClassPathResource(keyStoreFilePath);
		Map<String, String> map = new HashMap<String, String>();
		map.put(keyAlias, keyPassword);
		return new JKSKeyManager(keystore, keyPassword, map, keyAlias);
	}

	@Bean
	public TLSProtocolConfigurer tlsProtocolConfigurer(KeyManager keyManager) {
		TLSProtocolConfigurer configurer = new TLSProtocolConfigurer();
		configurer.setKeyManager(keyManager);
		configurer.setSslHostnameVerification("allowAll");
		return configurer;
	}
}
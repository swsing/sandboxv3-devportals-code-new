package com.capgemini.portal.security.user;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.portal.security.user.service.SAMLUserDetailsServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class SAMLUserDetailsServiceTests {

	@Mock
	SAMLCredential credential;

	@Mock
	SAMLUserDetailsService samlUserDetailsService;

	@InjectMocks
	private SAMLUserDetailsServiceImpl samlUserDetailsServiceImpl;

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void retrieveUserDataTest() {
		when(samlUserDetailsService.loadUserBySAML(credential)).thenReturn("test");
		Object userData = samlUserDetailsServiceImpl.loadUserBySAML(credential);
		Assert.assertEquals(userData, userData);
	}

	@Test
	public void retrieveUserDataTestFalse() {
		when(samlUserDetailsService.loadUserBySAML(credential)).thenReturn("Test User");
		Object userData = samlUserDetailsServiceImpl.loadUserBySAML(credential);
		Assert.assertNotEquals("Test123", userData);
	}

}

package com.capgemini.portal.security.user;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.NameID;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.XSAny;
import org.opensaml.xml.schema.XSString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.portal.security.user.SAMLUserDetails;

@RunWith(SpringJUnit4ClassRunner.class)
public class SAMLUserDetailsTests {

	@InjectMocks
	SAMLUserDetails samlUserDetails;

	@Mock
	UserDetails userDetails;

	@Mock
	SAMLCredential credential;

	@Mock
	Attribute attribute;

	@Mock
	XSString xmlString;

	@Mock
	XSAny xmlAny;

	@Mock
	NameID nameId;

	@Mock
	GrantedAuthority grantedAuthority;

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void constructorTest() {
		Assert.assertNotNull(new SAMLUserDetails());
	}

	@Test
	public void retrievePasswordTest() {
		when(userDetails.getPassword()).thenReturn("");
		Assert.assertEquals("", samlUserDetails.getPassword());
	}

	@Test
	public void retrieveUsernameTest() {
		when(credential.getNameID()).thenReturn(nameId);
		when(nameId.getValue()).thenReturn("xxx");
		Assert.assertEquals("xxx", samlUserDetails.getUsername());
	}

	@Test
	public void isAccountNonExpiredTest() {
		boolean ans = true;
		boolean value = samlUserDetails.isAccountNonExpired();
		Assert.assertEquals(ans, value);
	}

	@Test
	public void isAccountNonLockedTest() {
		boolean ans = true;
		boolean value = samlUserDetails.isAccountNonLocked();
		Assert.assertEquals(ans, value);
	}

	@Test
	public void isCredentialsNonExpiredTest() {
		boolean ans = true;
		boolean value = samlUserDetails.isCredentialsNonExpired();
		Assert.assertEquals(ans, value);
	}

	@Test
	public void isEnabledTest() {
		boolean ans = true;
		boolean value = samlUserDetails.isEnabled();
		Assert.assertEquals(ans, value);
	}

	@Test
	public void getAttribute() {
		String name = "xmlString";
		when(samlUserDetails.getAttribute(name)).thenReturn("xmlString");
		Assert.assertEquals(name, "xmlString");
	}

	@Test
	public void getAttributesString() {
		List<Attribute> list = new ArrayList<>();
		list.add(attribute);
		List<XMLObject> xmlO = new ArrayList<>();
		xmlO.add(xmlString);
		when(xmlString.getValue()).thenReturn("Test");
		when(attribute.getAttributeValues()).thenReturn(xmlO);
		when(attribute.getName()).thenReturn("Test");
		when(credential.getAttributes()).thenReturn(list);
		samlUserDetails.getAttributes();
	}

	@Test
	public void getAttributesNullOne() {
		List<Attribute> list = new ArrayList<>();
		list.add(attribute);
		List<XMLObject> xml1 = new ArrayList<>();
		when(attribute.getName()).thenReturn("Test");
		when(attribute.getAttributeValues()).thenReturn(xml1);
		when(credential.getAttributes()).thenReturn(list);
		try {
			samlUserDetails.getAttributes();
		} catch (Exception e) {

		}
	}

	@Test
	public void getAttributesNullTwo() {
		List<Attribute> list = new ArrayList<>();
		list.add(attribute);
		List<XMLObject> xml1 = new ArrayList<>();
		xml1.add(nameId);
		when(attribute.getName()).thenReturn("Test");
		when(attribute.getAttributeValues()).thenReturn(xml1);
		when(credential.getAttributes()).thenReturn(list);
		try {
			samlUserDetails.getAttributes();
		} catch (Exception e) {

		}
	}

	@Test
	public void getAttributesAny() {
		List<Attribute> list = new ArrayList<>();
		list.add(attribute);
		List<XMLObject> xml1 = new ArrayList<>();
		xml1.add(xmlAny);
		when(xmlAny.getTextContent()).thenReturn("Test");
		when(attribute.getAttributeValues()).thenReturn(xml1);
		when(attribute.getName()).thenReturn("Test");
		when(credential.getAttributes()).thenReturn(list);
		samlUserDetails.getAttributes();
	}

	@Test
	public void getAuthorities() {
		when(grantedAuthority.getAuthority()).thenReturn("test");
		Collections.singletonList(grantedAuthority.getAuthority());
		samlUserDetails.getAuthorities();

	}

}

package com.capgemini.portal.security.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
public class SecurityUtilsTests {

	private SecurityUtils securityUtils = new SecurityUtils();

	@Before
	public void setUp() {
		ReflectionTestUtils.setField(securityUtils, "port", "Test");
		ReflectionTestUtils.setField(securityUtils, "portalEntityId", "Test");
		ReflectionTestUtils.setField(securityUtils, "portalHostname", "Test");
		ReflectionTestUtils.setField(securityUtils, "muleLaunchUrl", "Test");
		ReflectionTestUtils.setField(securityUtils, "muleEntityId", "Test");
		ReflectionTestUtils.setField(securityUtils, "muleLaunchUrl", "Test");
		ReflectionTestUtils.setField(securityUtils, "idpHostname", "Test");
		ReflectionTestUtils.setField(securityUtils, "idpMetadataUrl", "Test");
		ReflectionTestUtils.setField(securityUtils, "idpLogoutUrl", "Test");
		ReflectionTestUtils.setField(securityUtils, "userGroupDn", "Test");
		ReflectionTestUtils.setField(securityUtils, "entityBaseURL", "Test");
		ReflectionTestUtils.setField(securityUtils, "isSLOEnable", Boolean.TRUE);
		ReflectionTestUtils.setField(securityUtils, "localLogoutUrl", "Test");
	}

	@Test
	public void getPortalEntityId() {
		assertEquals("Test", securityUtils.getPortalEntityId());
	}

	@Test
	public void getPortalBaseURL() {
		assertEquals("https://Test", securityUtils.getPortalBaseURL());
	}

	@Test
	public void getIDPMetadataURL() {
		assertEquals("Test", securityUtils.getIDPMetadataURL());
	}

	@Test
	public void getIDPLogoutURL() {
		assertEquals("Test", securityUtils.getIDPLogoutURL());
	}

	@Test
	public void getMuleLaunchURL() {
		assertEquals("Test", securityUtils.getMuleLaunchURL());
	}
	
	@Test
	public void getUserGroupDn() {
		assertEquals("Test", securityUtils.getUserGroupDn());
	}
	
	@Test
	public void getEntityBaseURL() {
		assertEquals("Test", securityUtils.getEntityBaseURL());
	}
	
	@Test
	public void getIsSLOEnable() {
		securityUtils.setSLOEnable(Boolean.TRUE);
		assertTrue(securityUtils.isSLOEnable()==Boolean.TRUE);
		
	}
	
	@Test
	public void localLogoutUrl() {
		securityUtils.setLocalLogoutUrl("test");
		assertTrue(securityUtils.getLocalLogoutUrl()=="test");
	}


}

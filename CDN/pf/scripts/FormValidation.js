"use strict";
var isCancelClicked = false;

function checkRequired(data) {
    if (data.value.length === 0) {
        return true;
    } else {
        return false;
    }
}

function addError(ele, content) {
    var span = $("<span class=\"alert-inline\" role=\"alert\"></span>");
    span.text(content);
    $(ele).after(span);
}

function removeError(ele) {
    $(".alert-inline", ele).remove();
}

function validateInput(data, validateType, isRequired, isValidate) {
    var passwordRegx = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^a-zA-Z0-9]).{8,}/;
    var nameRegx = /[^a-zA-Z\-\ 0-9]/;
    var emailRegx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (isCancelClicked) {
        return;
    }

    data.value = (data.value).trim();

    var submitButton = document.getElementById("submitButton");
    var errorFlag = false;
    switch (validateType) {
        case "password":
            if (isRequired) {
                errorFlag = checkRequired(data);
            }
            if (isValidate) {
                if (!passwordRegx.test(data.value)) {
                    errorFlag = true;
                }
            }
            break;
        case "newPassword":
            var confirmPassword = document.getElementById("confirmPassword");
            if (isRequired) {
                errorFlag = checkRequired(data);
            }
            if (isValidate) {
                if (!passwordRegx.test(data.value)) {
                    errorFlag = true;

                } else if (confirmPassword !== null && confirmPassword.value !== "") {
                    if (confirmPassword.value !== data.value) {
                        if (confirmPassword.className.indexOf("invalid") === -1) {
                            confirmPassword.className = confirmPassword.className + " invalid";
                            errorFlag = true;
                            data = confirmPassword;
                        }
                    }
                }
            }
            break;
        case "confirmPassword":
            var newPassword = document.getElementById("newPassword");
            if (isRequired) {
                errorFlag = checkRequired(data);
            }
            if (isValidate) {
                if (newPassword.value !== data.value) {
                    errorFlag = true;
                }
            }
            break;
        case "inputName":
            if (isRequired) {
                errorFlag = checkRequired(data);
            }
            if (isValidate) {
                if (nameRegx.test(data.value)) {
                    errorFlag = true;
                }
            }
            break;
        case "email":
            var confirmEmail = document.getElementById("confirmEmail");
            if (isRequired) {
                errorFlag = checkRequired(data);
            }
            if (isValidate) {
                if (!emailRegx.test(data.value)) {
                    errorFlag = true;
                }  else {
                    var stringToTest = data.value.substring(data.value.lastIndexOf("@") + 1, data.value.lastIndexOf("."));
                    var subStr = stringToTest.split(".");
                    for (var i = 0; i < subStr.length; i++) {
                        if (subStr[i].charAt(0) == "-" || subStr[i].charAt(subStr[i].length - 1) == "-") {
                            errorFlag = true;
                        }
                    }
                }
                if (confirmEmail !== null && confirmEmail.value !== "" && errorFlag === false) {
                    if (confirmEmail.value.toLowerCase() !== data.value.toLowerCase()) {
                        if (confirmEmail.className.indexOf("invalid") === -1) {
                            confirmEmail.className = confirmEmail.className + " invalid";
                            errorFlag = true;
                            data = confirmEmail;
                        }
                    }
                }
            }
            break;
        case "confirmEmail":
            var email = document.getElementById("email");
            if (isRequired) {
                errorFlag = checkRequired(data);
            }
            if (isValidate) {
                if (email.value.toLowerCase() != data.value.toLowerCase()) {
                    errorFlag = true;
                }
            }
            break;
        case "checkbox":
            if (isRequired) {
                if (!data.checked) {
                    errorFlag = true;
                }
            }
            break;
    }
    if (errorFlag) {
        if (data.className.indexOf("invalid") === -1) {
            data.className = data.className + " invalid";
        }
        var errMsg = $(data).parent().children(".err-content").text();
        addError(data, errMsg);
        if($(submitButton).is(":focus")){
            if($("#cancelButton").length){
                $("#cancelButton").focus();
            }else{
                $(".footer-links>li:first>a").focus();
            }
        }
        submitButton.disabled = true;
    } else {
        if (data.className.indexOf("invalid") !== -1) {
            submitButton.disabled = false;
            data.className = (data.className).replace(" invalid", "");
            removeError($(data).parent());
        } else if (document.getElementsByClassName("invalid").length > 0) {
            if($(submitButton).is(":focus")){
                $("#cancelButton").focus();
            }
            submitButton.disabled = true;
        }
    }
}

function validateFormRequired(formId, eventType, currentElementId) {
    if (eventType === "keydown") {
        if (window.event.keyCode !== 13) {
            return;
        }
    }
    var str = "",
        delimiter = "",
        start = 0,
        end = 0,
        tokens = [],
        result = "";
    var elements = document.forms[formId].elements;
    var submitButtonEnableFlag = true;
    for (var i = 0; i < elements.length; i++) {
        if (currentElementId === elements[i].id && elements[i].type !== "checkbox") {
            continue;
        } else if (elements[i].type === "text" || elements[i].type === "email" || elements[i].type === "password") {

            str = elements[i].getAttribute("onblur");
            if (str !== null) {
                delimiter = ",";
                start = 2;
                end = 3;
                tokens = str.split(delimiter).slice(start, end);
                result = tokens.join(delimiter);

                if ((result.indexOf("true") !== -1)) {
                    if (elements[i].value !== "" && document.getElementsByClassName("invalid").length === 0) {
                        submitButtonEnableFlag = true;
                    } else {
                        submitButtonEnableFlag = false;
                        break;
                    }
                }
            }
        } else if (elements[i].type === "checkbox") {
            str = elements[i].getAttribute("onchange");
            if (str !== null) {
                delimiter = ",";
                start = 2;
                end = 3;
                tokens = str.split(delimiter).slice(start, end);
                result = tokens.join(delimiter);
                if (result.indexOf("true") !== -1) {
                    if (elements[i].checked && document.getElementsByClassName("invalid").length === 0) {
                        submitButtonEnableFlag = true;
                    } else {
                        submitButtonEnableFlag = false;
                        break;
                    }
                }
            }
        }
    }

    var submitButton = document.getElementById("submitButton");
    if (submitButtonEnableFlag) {
        submitButton.disabled = false;
        if (!currentElementId) {
            if (window.event.keyCode === 13 && $(window.event.target).attr("id") !== "cancelButton") {
                document.getElementById("submitButton").click();
            }
        }
    } else {
        submitButton.disabled = true;
        if($(submitButton).is(":focus")){
            $("#cancelButton").focus();
        }
        if (!currentElementId) {
            if (window.event.keyCode === 13) {
                window.event.preventDefault();
            }
        }
    }

}

function focusInput(data) {
    if (data.className.indexOf("invalid") !== -1) {
        data.className = (data.className).replace(" invalid", "");
        removeError($(data).parent());
    }
    validateFormRequired(data.form.id, "focus", data.id);
}

function checkKeyDown(data) {
    if (window.event.keyCode === 13) {
        data.blur();
    }
}
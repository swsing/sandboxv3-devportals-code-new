//In JS Disabled Case
    $(".main-container.no-display-disabled-cookie-js").removeClass("no-display-disabled-cookie-js");
    
//In Cookie Disabled Case
if (!window.navigator.cookieEnabled) {
    $(".container.container-margin.cookie-error").removeClass("cookie-error");
}

$( document ).ready(function() {
    if($("#authErrorLockedUser").val() == "authn.srvr.msg.user.account.locked"){
        var msg = $("#authErrorLockedUserMsg").val();
        var link = $("#lockedUserLink").val();
        var changedMsg = msg.replace("<a>", "<a href='"+link+"'>");
        $("#LockedUserMsg").html(changedMsg);
    }

    $('.skip-main').on('click',function(event){
        event.preventDefault();
        $('#username').focus();
    });

});

$( window ).load(function(){
    enableLoginBtn();
});

$( "input" ).on("change", function(){
    enableLoginBtn();
});

function postOk() {
    var submitButtonEnableFlag = $("#submitButton").is(':disabled');
    if(!submitButtonEnableFlag){
        var emailRegx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $("#username").val($("#username").val().trim());
        var email_input = $("#username").val();
        if (!emailRegx.test(email_input)) {
            $("#errorAlert").addClass("server-error");
            $("#ariaAlert2").addClass("server-error");
            $("#frontError").removeClass("invalid-credentials");
            return false;
        }else{
            $('.main-container').hide();
            $( "#loadingDiv" ).show();
            $("#ok").val('clicked');
            $("#snewPass").val($("#newPassword").val());
            $("#newPassword").val("");
            $("#newPassword").attr("type","text");
            $("#loginForm").submit();
        }
    }
}

function enableLoginBtn(){
    var username = $("#username").val();
    var password = $("#newPassword").val();
    var autofilled_username = false, autofilled_password=false;
    var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    if(isChrome){
        autofilled_username = $("#username").is("*:-webkit-autofill");
        autofilled_password = $("#newPassword").is("*:-webkit-autofill");
    }
    submitButton = document.getElementById("submitButton");
    if((autofilled_username && autofilled_password) || ((username.trim().length) > 0 && (password.length) > 0)){
        submitButton.disabled = false;
        if(event.keyCode == 13)
            postOk();
    }else{
        submitButton.disabled = true;
    }

}
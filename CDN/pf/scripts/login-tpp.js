//In JS Disabled Case
    $(".main-container.no-display-disabled-cookie-js").removeClass("no-display-disabled-cookie-js");
    
//In Cookie Disabled Case
if (!window.navigator.cookieEnabled) {
    $(".alert.alert-margin.portals-alert.portals-alert-danger.cookie-error").removeClass("cookie-error");
}

$( document ).ready(function() {
    $('.skip-main').on('click',function(event){
        event.preventDefault();
        $('.btn.btn-submit.btn-block').focus();
    });

    if($('#isTestingEnabled').val()=="true"){
        $("#onlyForTesting").css("display","block");
    }else{
        $("#onlyForTesting").css("display","none");
    }
});

// public/js/app.js
"use strict";
var tpp = angular.module("tpp", ["ui.router", "ui.bootstrap", "ngSanitize", "pascalprecht.translate", "blockUI", "angular-clipboard" , "tppPartials"]);

tpp.constant("Constants", {
    NoticeConstants: {
        error: "error",
        alert: "alert",
        warning: "warning",
        success: "success"
    }
});

tpp.run(["$rootScope", "config", "$state", "$location", "$window", function ($rootScope, config, $state, $location, $window) {
    $rootScope.activePath = null;
    $rootScope.$on("$stateChangeSuccess", function () {
        $rootScope.activePath = $location.path();
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
    try {
        if ($window.localStorage.tppList) {
            $window.localStorage.removeItem("tppList");
        }
        if ($window.localStorage.selectedOrg) {
            $window.localStorage.removeItem("selectedOrg");
        }
    } catch (e) {
        //
    }
    var isInnerPage = $("#isAuthorized").val() ? $("#isAuthorized").val() : "false";
    if (isInnerPage !== "false") {
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
            if (toState.name === "myApplication" || toState.name === "addApplication" || toState.name === "addApplicationReview") {
                if (!$window.localStorage.selectedOrg) {
                    event.preventDefault();
                    $state.go("selectOrganisation");
                }
            }
        });
    }
}]);

tpp.config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$translateProvider", "$qProvider", "$translatePartialLoaderProvider", "config", "blockUIConfig",
    function ($stateProvider, $urlRouterProvider, $httpProvider, $translateProvider, $qProvider, $translatePartialLoaderProvider, config, blockUIConfig) {
        var staticContent = angular.isObject(window.uiStaticContent) ? window.uiStaticContent : JSON.parse(window.uiStaticContent);
        $translatePartialLoaderProvider.addPart("index");
        $translateProvider.translations("en", staticContent);
        $translateProvider.preferredLanguage("en");
        $translateProvider.fallbackLanguage("en");
        $translateProvider.useSanitizeValueStrategy("sceParameters");

        blockUIConfig.templateUrl = "app/views/loading-spinner.html";
        $qProvider.errorOnUnhandledRejections(false);
        $httpProvider.interceptors.push(["$q", "blockUI", "$timeout", function ($q, blockUI, $timeout) {
            return {
                request: function (config) {
                    blockUI.start();
                    $(".block-ui-message-container span").attr("aria-hidden", "false");
                    var msg = $(".block-ui-message-container span").html();
                    $(".block-ui-message-container span").html("");
                    $(".block-ui-message-container span").html(msg);
                    return config;
                },
                requestError: function (rejection) {
                    blockUI.stop();
                    $(".block-ui-message-container span").attr("aria-hidden", "true");
                    return $q.reject(rejection);
                },
                response: function (response) {
                        blockUI.stop();
                        $(".block-ui-message-container span").attr("aria-hidden", "true");
                        return response;

                },
                responseError: function (rejection) {
                    blockUI.stop();
                    $(".block-ui-message-container span").attr("aria-hidden", "true");
                    return $q.reject(rejection);
                }
            };
        }]);

        var isAuthorize = $("#isAuthorized").val() ? $("#isAuthorized").val() : "false";
        if (isAuthorize === "false") {
            $stateProvider.
                state("login", {
                    "url": "/login",
                    templateUrl: "app/views/login.html",
                    controller: "LoginCtrl"
                });
            $urlRouterProvider.otherwise("/login");

        } else {
            $stateProvider.
                state("myApplication", {
                    "url": "/view-application",
                    templateUrl: "app/views/my-application.html",
                    controller: "ApplicationCtrl",
                    params: {
                        "addedAppData": null
                    }
                }).
                state("addApplication", {
                    "url": "/add-application",
                    templateUrl: "app/views/add-application.html",
                    controller: "AddApplicationCtrl"
                }).
                state("addApplicationReview", {
                    "url": "/add-application-review",
                    templateUrl: "app/views/add-application-review.html",
                    controller: "AddApplicationReviewCtrl",
                    params: {
                        "applicationData": null,
                        "selectedFile": null
                    }
                }).
                state("selectOrganisation", {
                    "url": "/select-organisation",
                    templateUrl: "app/views/select-organisation.html",
                    controller: "SelectOrganisationCtrl"
                });
            $urlRouterProvider.otherwise("/select-organisation");
        }
    }
]);

// Constants for application
"use strict";
angular.module("tpp").constant("config", {});

/**
 * Add application controller is to add new application in selected tpp.
 */
"use strict";
angular.module("tpp").controller("AddApplicationCtrl", ["$scope", "$window", "$timeout", "$translate", "$uibModal", "$state", "RestService", "CommonService", "Constants",
    function($scope, $window, $timeout, $translate, $uibModal, $state, RestService, CommonService, Constants) {
        /**
         * Init method translates all static content for add application.
         * It fetches selected tpp name from local storage.
         */
        $scope.init = function() {
            var assetsPath = document.querySelector("#cdnBaseURL").value;
            $scope.arrowImagePath = assetsPath + "/tppportal/images/arrow.png";
            $translate(["DASH_SCREEN_READER_LABEL", "ADD_APPLICATION_PAGE.ADD_APPLICATION_HEADING", "ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.ADD_APPLICATION_FORM_HEADING",
                "ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.UPLOAD_SOFTWARE_STATEMENT_TOOLTIP_SCREEN_READER_LABEL",
                "ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.UPLOAD_SSA_INSTRUCTION_LABEL",
                "ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.UPLOAD_SOFTWARE_STATEMENT_TOOLTIP_LABEL",
                "ERROR_MESSAGES.INVALID_FILE_SIZE_ERROR",
                "ERROR_MESSAGES.INVALID_FILE_TYPE_ERROR",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_CAPTION",
                "ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.FILE_INPUT_LABEL",
                "ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.BUTTONS.BROWSE_BUTTON_LABEL",
                "ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.BUTTONS.UPLOAD_BUTTON_LABEL",
                "ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.BUTTONS.CANCEL_BUTTON_LABEL"
            ]).then(function(translations) {
                $scope.dashScrLabel = translations["DASH_SCREEN_READER_LABEL"];
                $scope.addAppHeader = translations["ADD_APPLICATION_PAGE.ADD_APPLICATION_HEADING"];
                $scope.addAppFormHeading = translations["ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.ADD_APPLICATION_FORM_HEADING"];
                $scope.uploadTooltipScrLabel = translations["ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.UPLOAD_SOFTWARE_STATEMENT_TOOLTIP_SCREEN_READER_LABEL"];
                $scope.uploadInstLabel = translations["ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.UPLOAD_SSA_INSTRUCTION_LABEL"];
                $scope.appTooltipMsg =
                    translations["ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.UPLOAD_SOFTWARE_STATEMENT_TOOLTIP_LABEL"];
                $scope.invalidSizeError =
                    translations["ERROR_MESSAGES.INVALID_FILE_SIZE_ERROR"];
                $scope.invalidTypeError =
                    translations["ERROR_MESSAGES.INVALID_FILE_TYPE_ERROR"];
                $scope.addAppDetailsCaptionLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_CAPTION"];
                $scope.fileInputLabel =
                    translations["ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.FILE_INPUT_LABEL"];
                $scope.browseButtonLabel = translations["ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.BUTTONS.BROWSE_BUTTON_LABEL"];
                $scope.uploadButtonLabel =
                    translations["ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.BUTTONS.UPLOAD_BUTTON_LABEL"];
                $scope.cancelButtonLabel = translations["ADD_APPLICATION_PAGE.ADD_APPLICATION_FORM.BUTTONS.CANCEL_BUTTON_LABEL"];
            });
            $scope.selectedFile = { name: null };
            $scope.placement = {
                selected: "right"
            };
            $scope.invalidFile = false;
            $scope.inlineErrMsg = "";
            $scope.formData = null;
            $scope.noticeData = null;
            $scope.selectedTppOrgName = JSON.parse($window.localStorage.getItem("selectedOrg")).organizationName;
        };
        /**
         * Select file method called by browse click.
         * It selects file from file browser.
         * It also shift focus to browse button.
         */
        $scope.selectFile = function() {
            $("#ssa").click();
            $("#btn_browse").focus();
        };
        /**
         * This method calls onchange of file input field. So after selection of file it added to file input box.
         * It removes inline error when selected file has data.
         * It calls validate method to validating selected file.
         */
        $scope.filesSelected = function(ele) {
            if ($scope.selectedFile && ($scope.selectedFile.name !== ele.files[0].name)) {
                $scope.inlineErrMsg = "";
                $scope.invalidSize = false;
                $scope.invalidFile = false;
            }
            $timeout(function() {
                $scope.$apply(function() {
                    if (ele.files[0]) {
                        $scope.selectedFile = ele.files[0];
                        $scope.applicationData = null;
                        $scope.noticeData = null;
                        if ($scope.validate($scope.selectedFile)) {
                            $scope.formData = new FormData();
                            $scope.formData.append("file", $scope.selectedFile);
                            $scope.formData.append("tppOrgId", JSON.parse($window.localStorage.getItem("selectedOrg")).tppOrganizationId);
                        } else {
                            $scope.formData = null;
                        }
                    }
                });
            });
        };
        /**
         * Validate method calls to validate selected file.
         * It takes file as param.
         * It validates file type should be text file.
         * It validates file size should be less then 50KB.
         * If any validation fails it show inline error message.
         */
        $scope.validate = function(file) {
            if (file.type !== "text/plain") {
                $scope.inlineErrMsg = $scope.invalidTypeError;
                $scope.invalidFile = true;
                $scope.applicationData = null;
                return false;
            } else if (file.size > 50000) {
                $scope.inlineErrMsg = $scope.invalidSizeError;
                $scope.invalidFile = true;
                $scope.applicationData = null;
                return false;
            }
            $scope.invalidFile = false;
            return true;
        };
        /**
         * Upload file uploads selecte text file to server.
         * While uploading it first validates selected file.
         * It calls rest api uploadSSAtoken to upload selected file. If rest call has been successful then it pass select file data to application review controller.
         * If uploadSSAtoken rest call fails method returns associated error.
         */
        $scope.uploadFile = function() {
            if (!$scope.validate($scope.selectedFile)) {
                return;
            }
            $scope.noticeData = null;
            RestService.uploadSSAtoken($scope.formData).then(function(resp) {
                $state.go("addApplicationReview", { "applicationData": resp.data, "selectedFile": $scope.selectedFile });
            }, function(error) {
                $scope.uploadNoticeObj = CommonService.getNoticeObject(error);
                if ($scope.uploadNoticeObj.noticeMessage) {
                    $scope.noticeData = {};
                    $scope.noticeData.noticeMessage = $scope.uploadNoticeObj.noticeMessage;
                    $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                    $scope.noticeData.noticeDescription = $scope.uploadNoticeObj.noticeDescription;
                    $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                }
                $timeout(function() {
                    $("#btn_upload").focus();
                }, 500);
            });
        };
        /**
         * This method is for accesibility purpose to skip the content and on tab focus on main content.
         */
        $scope.focusMainContent = function() {
            $("#btn_browse").focus();
        };
        $scope.init();
    }
]);

/**
 * Add application Review controller is to display data of selected file from add application controller.
 */
"use strict";
angular.module("tpp").controller("AddApplicationReviewCtrl", ["$scope", "$window", "$timeout", "$translate", "$uibModal", "$state", "RestService", "CommonService", "Constants",
    function($scope, $window, $timeout, $translate, $uibModal, $state, RestService, CommonService, Constants) {
        /**
         * Init method translates all static content for add application.
         * It takes all selected file data from add application controller.
         * It fetches selected tpp name from local storage.
         */
        $scope.init = function() {
            $scope.applicationData = $state.params.applicationData;
            $scope.selectedFile = $state.params.selectedFile;
            var assetsPath = document.querySelector("#cdnBaseURL").value;
            $scope.arrowImagePath = assetsPath + "/tppportal/images/arrow.png";
            $translate(["DASH_SCREEN_READER_LABEL", "ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_CAPTION",
                "ADD_APPLICATION_REVIEW_PAGE.ADD_APPLICATION_REVIEW_HEADING",
                "ADD_APPLICATION_REVIEW_PAGE.ADD_APPLICATION_REVIEW_HEADER",
                "ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.APPLICATION_NAME_LABEL",
                "ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.APPLICATION_DESCRIPTION_LABEL",
                "ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.CLIENT_ID_LABEL",
                "ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.REDIRECT_URI_LABEL",
                "ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.TERMS_OF_SERVICE_URI_LABEL",
                "ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.POLICY_URI_LABEL",
                "ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.JWKS_END_POINT_LABEL",
                "ADD_APPLICATION_REVIEW_PAGE.BUTTONS.CANCEL_BUTTON_LABEL",
                "ADD_APPLICATION_REVIEW_PAGE.BUTTONS.ADD_APPLICATION_BUTTON_LABEL",
                "ADD_APPLICATION_REVIEW_PAGE.BUTTONS.CHOOSE_DIFFERENT_FILE_BUTTON_LABEL"
            ]).then(function(translations) {
                $scope.addAppDetailsCaptionLabel = translations["ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_CAPTION"];
                $scope.dashScrLabel = translations["DASH_SCREEN_READER_LABEL"];
                $scope.addAppHeader = translations["ADD_APPLICATION_REVIEW_PAGE.ADD_APPLICATION_REVIEW_HEADING"];
                $scope.addApplicationReviewHeader = translations["ADD_APPLICATION_REVIEW_PAGE.ADD_APPLICATION_REVIEW_HEADER"];
                $scope.applicationNameLabel = translations["ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.APPLICATION_NAME_LABEL"];
                $scope.applicationDescriptionLabel = translations["ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE." +
                    "APPLICATION_DETAILS_TABLE_ROW_HEADERS.APPLICATION_DESCRIPTION_LABEL"];
                $scope.clientIdLabel = translations["ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.CLIENT_ID_LABEL"];
                $scope.redirectUriLabel = translations["ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.REDIRECT_URI_LABEL"];
                $scope.termsOfServiceUrlLabel = translations["ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE." +
                    "APPLICATION_DETAILS_TABLE_ROW_HEADERS.TERMS_OF_SERVICE_URI_LABEL"];
                $scope.policyUriLabel = translations["ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.POLICY_URI_LABEL"];
                $scope.jwksEndpointLabel = translations["ADD_APPLICATION_REVIEW_PAGE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.JWKS_END_POINT_LABEL"];
                $scope.cancelButtonLabel = translations["ADD_APPLICATION_REVIEW_PAGE.BUTTONS.CANCEL_BUTTON_LABEL"];
                $scope.addApplicationButtonLabel = translations["ADD_APPLICATION_REVIEW_PAGE.BUTTONS.ADD_APPLICATION_BUTTON_LABEL"];
                $scope.chooseDiffFileButtonLabel = translations["ADD_APPLICATION_REVIEW_PAGE.BUTTONS.CHOOSE_DIFFERENT_FILE_BUTTON_LABEL"];
            });
            $scope.placement = {
                selected: "right"
            };
            $scope.invalidFile = false;
            $scope.inlineErrMsg = "";
            $scope.formData = null;
            $scope.noticeData = null;
            $scope.selectedTppOrgName = JSON.parse($window.localStorage.getItem("selectedOrg")).organizationName;
        };
        /**
         * Submit Application Data submit selected file data to server.
         * It calls rest api addApplication to submit selected file.
         * If rest call has been successful then it pass successful added application response to my application controller.
         * If addApplication rest call fails method returns associated error.
         */
        $scope.submitApplicationData = function() {
            $scope.noticeData = null;
            $scope.formData = new FormData();
            $scope.formData.append("file", $scope.selectedFile);
            $scope.formData.append("tppOrgId", JSON.parse($window.localStorage.getItem("selectedOrg")).tppOrganizationId);
            RestService.addApplication($scope.formData).then(function(resp) {
                $state.go("myApplication", { "addedAppData": resp.data });
            }, function(error) {
                $scope.submitNoticeObj = CommonService.getNoticeObject(error);
                if ($scope.submitNoticeObj.noticeMessage) {
                    $scope.noticeData = {};
                    $scope.noticeData.noticeMessage = $scope.submitNoticeObj.noticeMessage;
                    $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                    $scope.noticeData.noticeDescription = $scope.submitNoticeObj.noticeDescription;
                    $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                }
                $timeout(function() {
                    $("#btn_addApp").focus();
                }, 500);
            });
        };
        /**
         * This method is for accesibility purpose to skip the content and on tab focus on main content.
         */
        $scope.focusMainContent = function() {
            $("#btn_addApp").focus();
        };
        $scope.init();
    }
]);

/**
 * This controller is used for display application list respective of selected organization display client secret, role, status and delete application also.
 * User can also add new application using add application button.
 */
"use strict";
angular.module("tpp").controller("ApplicationCtrl", ["$scope", "$timeout", "$translate", "$uibModal", "$state", "RestService", "CommonService", "$filter", "$window", "Constants",
    function ($scope, $timeout, $translate, $uibModal, $state, RestService, CommonService, $filter, $window, Constants) {
        /**
         * Init method translates all static content for application.
         * It generates array of labels which used in applicationDetails directive.
         * It checked if it comes from app application screen then it shows success notice.
         * Init method fetch selected tpp id from local storage and calls getApplicationLists method.
         */
        $scope.init = function () {
            $scope.placement = {
                selected: "top"
            };
            var assetsPath = document.querySelector("#cdnBaseURL").value;
            $scope.arrowImagePath = assetsPath + "/tppportal/images/arrow.png";
            $scope.isAppListsDataNotFound = null;
            $scope.applicationsList = [];
            $scope.tppList = [];
            $scope.selectedTppOrgId = null;
            $scope.selectedTppOrgName = null;
            $scope.tppOrgID = null;
            $scope.tppBlocked = false;
            $scope.hideskipToContLink = false;
            $scope.noticeData = null;
            $scope.warningData = null;
            $scope.successData = null;
            $scope.alertData = null;
            $scope.addedApp = $state.params.addedAppData;
            $scope.errorPrefix = null;
            $scope.applicationDetailsLabels = {};

            $translate(["DASH_SCREEN_READER_LABEL", "APPLICATION_PAGE.APPLICATION_PAGE_HEADING",
                "APPLICATION_PAGE.APPLICATION_ADDED_SUCCESS_MESSAGE_PRE",
                "APPLICATION_PAGE.APPLICATION_ADDED_SUCCESS_MESSAGE_POST",
                "APPLICATION_PAGE.APPLICATION_DELETED_SUCCESS_MESSAGE_PRE",
                "APPLICATION_PAGE.APPLICATION_DELETED_SUCCESS_MESSAGE_POST",
                "APPLICATION_PAGE.CLIENT_SECRET_RESET_SUCCESS_MESSAGE_PRE",
                "APPLICATION_PAGE.CLIENT_SECRET_RESET_SUCCESS_MESSAGE_POST",
                "APPLICATION_PAGE.BUTTON.ADD_APPLICATION_BUTTON_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.APPLICATION_NAME_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.APPLICATION_VERSION_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.ROLE_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.STATUS_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.REMOVE_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.ROLE_TOOLTIP_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.ROLE_TOOLTIP_SCREEN_READER_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_CAPTION",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_CAPTION",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.APPLICATION_DESCRIPTION_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.CLIENT_ID_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.CLIENT_SECRET_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.REDIRECT_URI_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.TERMS_OF_SERVICE_URI_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.POLICY_URI_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.JWKS_END_POINT_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.SHOW_MORE_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.SHOW_LESS_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.BUTTONS.RESET_SECRET_BUTTON_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.BUTTONS.SHOW_SECRET_BUTTON_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.CLIENT_SECRET_TOOLTIP_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.CLIENT_ID_TOOLTIP_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.CLIENT_ID_TOOLTIP_SCREEN_READER_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.CLIENT_SECRET_TOOLTIP_SCREEN_READER_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.REMOVE_ICON_ALTERNATE_TEXT_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.BUTTONS.RESET_SECRET_BUTTON_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.BUTTONS.SHOW_SECRET_BUTTON_LABEL",
                "APPLICATION_PAGE.APPLICATION_TABLE.PROTECTED_CLIENT_SECRET"
            ]).then(function (translations) {
                $scope.dashScrLabel = translations["DASH_SCREEN_READER_LABEL"];
                $scope.myApplicationHeading = translations["APPLICATION_PAGE.APPLICATION_PAGE_HEADING"];
                $scope.applicationSuccessPreLabel = translations["APPLICATION_PAGE.APPLICATION_ADDED_SUCCESS_MESSAGE_PRE"];
                $scope.applicationSuccessPostLabel = translations["APPLICATION_PAGE.APPLICATION_ADDED_SUCCESS_MESSAGE_POST"];
                $scope.applicationDeleteSuccessPreLabel = translations["APPLICATION_PAGE.APPLICATION_DELETED_SUCCESS_MESSAGE_PRE"];
                $scope.applicationDeleteSuccessPostLabel = translations["APPLICATION_PAGE.APPLICATION_DELETED_SUCCESS_MESSAGE_POST"];
                $scope.clientSecretResetSuccessPreLabel = translations["APPLICATION_PAGE.CLIENT_SECRET_RESET_SUCCESS_MESSAGE_PRE"];
                $scope.clientSecretResetSuccessPostLabel = translations["APPLICATION_PAGE.CLIENT_SECRET_RESET_SUCCESS_MESSAGE_POST"];
                $scope.myApplicationButtonLabel = translations["APPLICATION_PAGE.BUTTON.ADD_APPLICATION_BUTTON_LABEL"];
                $scope.applicationNameLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.APPLICATION_NAME_LABEL"];
                $scope.applicationVersionLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.APPLICATION_VERSION_LABEL"];
                $scope.roleLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.ROLE_LABEL"];
                $scope.statusLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.STATUS_LABEL"];
                $scope.deleteLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.REMOVE_LABEL"];
                $scope.roleTooltipLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_COLUMN_HEADERS.ROLE_TOOLTIP_LABEL"];
                $scope.applicationDetailsLabels.applicationDescriptionLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.APPLICATION_DESCRIPTION_LABEL"];
                $scope.applicationDetailsLabels.clientIdLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.CLIENT_ID_LABEL"];
                $scope.applicationDetailsLabels.clientSecretLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_LIST_TABLE_COLUMN_HEADERS.CLIENT_SECRET_LABEL"];
                $scope.applicationDetailsLabels.redirectURILabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.REDIRECT_URI_LABEL"];
                $scope.applicationDetailsLabels.termsServicesLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.TERMS_OF_SERVICE_URI_LABEL"];
                $scope.applicationDetailsLabels.policyURILabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.POLICY_URI_LABEL"];
                $scope.applicationDetailsLabels.JWKSEndpointLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.JWKS_END_POINT_LABEL"];
                $scope.applicationDetailsLabels.clientIDTooltipLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_DETAILS_TABLE." +
                    "APPLICATION_DETAILS_TABLE_ROW_HEADERS.CLIENT_ID_TOOLTIP_LABEL"];
                $scope.applicationDetailsLabels.clientSecretTooltipLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_LIST_TABLE_COLUMN_HEADERS.CLIENT_SECRET_TOOLTIP_LABEL"];
                $scope.applicationDetailsLabels.showMoreLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.SHOW_MORE_LABEL"];
                $scope.applicationDetailsLabels.showLessLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.SHOW_LESS_LABEL"];
                $scope.applicationDetailsLabels.resetSecretButtonLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.BUTTONS.RESET_SECRET_BUTTON_LABEL"];
                $scope.applicationDetailsLabels.showSecretButtonLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.BUTTONS.SHOW_SECRET_BUTTON_LABEL"];
                $scope.applicationDetailsLabels.roleTooltipScrLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_LIST_TABLE_COLUMN_HEADERS.ROLE_TOOLTIP_SCREEN_READER_LABEL"];
                $scope.applicationDetailsLabels.appListCaptionLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE.APPLICATION_LIST_TABLE_CAPTION"];
                $scope.applicationDetailsLabels.appDetailsCaptionLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_CAPTION"];
                $scope.applicationDetailsLabels.clientIDTooltipScrLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_DETAILS_TABLE.APPLICATION_DETAILS_TABLE_ROW_HEADERS.CLIENT_ID_TOOLTIP_SCREEN_READER_LABEL"];
                $scope.applicationDetailsLabels.clientSecretTooltipScrLabel = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "APPLICATION_LIST_TABLE_COLUMN_HEADERS.CLIENT_SECRET_TOOLTIP_SCREEN_READER_LABEL"];
                $scope.applicationDetailsLabels.deleteIcon = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "REMOVE_ICON_ALTERNATE_TEXT_LABEL"];
                $scope.applicationDetailsLabels.resetButton = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "BUTTONS.RESET_SECRET_BUTTON_LABEL"];
                $scope.applicationDetailsLabels.showButton = translations["APPLICATION_PAGE.APPLICATION_TABLE." +
                    "BUTTONS.SHOW_SECRET_BUTTON_LABEL"];
                $scope.applicationDetailsLabels.protectedClientSecret = translations["APPLICATION_PAGE.APPLICATION_TABLE.PROTECTED_CLIENT_SECRET"];
                if ($scope.addedApp) {
                    var applicationSuccessLabel = $scope.applicationSuccessPreLabel + $scope.addedApp.tppApplicationName + $scope.applicationSuccessPostLabel;
                    $scope.successData = {};
                    $scope.successData.noticeMessage = applicationSuccessLabel;
                    $scope.successData.noticeType = Constants.NoticeConstants["success"];
                    $scope.successData.noticePrefix = $translate.instant("ERROR_MESSAGES.SUCCESS_LABEL");
                    $scope.successData.successCloseScreenReaderLabel = $translate.instant("APPLICATION_PAGE.SUCCESS_CLOSE_BUTTON_SCREEN_READER_LABEL");
                }
            });
            $scope.selectedTppOrgId = JSON.parse($window.localStorage.getItem("selectedOrg")).tppOrganizationId;
            $scope.selectedTppOrgName = JSON.parse($window.localStorage.getItem("selectedOrg")).organizationName;
            if ($scope.selectedTppOrgId !== null || $scope.selectedTppOrgId !== "") {
                $scope.getApplicationLists();
            }
        };

        /**
         * This method call on app application button clicked.
         * It calls check session rest call on app application button clicked. If session is valid it take user to app application screen, if not then it will show session popup.
         * While calling check session if rest call fails then it will show error.
         */
        $scope.addApplicationBtnClicked = function () {
            CommonService.checkSession("addApplication", false).then(function (noticeObject) {
                if (noticeObject) {
                    $scope.noticeData = {};
                    $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                    $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                    $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                    $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                }
            });
        };

        /**
         * This method fetches application list respective of selected organizationId through api rest call applicationsListRequest.
         * If applicationsListRequest rest call fails method returns associated error.
         * It also checks if tpp is blocked, if so it disabled add application button, reset button and remove button. It shows alert for tpp blocked.
         * It sets logic of accordian show more and show less.
         * If there are no application found from rest call it will show notice.
         */
        $scope.getApplicationLists = function () {
            $scope.noticeData = null;
            $scope.warningData = null;
            $scope.alertData = null;
            $scope.applicationsList = null;
            RestService.applicationsListRequest($scope.selectedTppOrgId).then(function (resp) {
                $scope.applicationsList = resp.data;
                $scope.tppOrgID = $scope.applicationsList.tppOrgId;
                $scope.tppBlocked = $scope.applicationsList.tppblocked;
                if ($scope.tppBlocked) {
                    $scope.alertData = {};
                    $scope.alertData.noticeMessage = $translate.instant("APPLICATION_PAGE.TPP_BLOCK_INSTRUCTION_LABEL");
                    $scope.alertData.noticeType = Constants.NoticeConstants["error"];
                    $scope.alertData.noticePrefix = $translate.instant("ERROR_MESSAGES.ALERT_LABEL");
                }
                if ($scope.applicationsList.tppAppList.length > 0) {
                    angular.forEach($scope.applicationsList.tppAppList, function (applicationData) {
                        applicationData.isExpanded = false;
                    });
                } else {
                    $scope.warningData = {};
                    $scope.warningData.noticeMessage = $translate.instant("APPLICATION_PAGE.APPLICATION_NO_DATA_INSTRUCTION_LABEL");
                    $scope.warningData.noticeType = Constants.NoticeConstants["warning"];
                    $scope.warningData.noticePrefix = $translate.instant("ERROR_MESSAGES.NOTICE_LABEL");
                    $scope.hideskipToContLink = $scope.tppBlocked;
                }
            },
                function (error) {
                    var noticeObject = CommonService.getNoticeObject(error);
                    if (noticeObject.noticeMessage) {
                        $scope.noticeData = {};
                        $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                        $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                        $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                        $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                    }
                });
        };

        /**
         * This method calls to toggle show more details button.
         */
        $scope.toggleApplicationDetails = function () {
            var applicationFilter = $filter("filter")($scope.applicationsList.tppAppList, { "isExpanded": true });
            if (applicationFilter.length > 0) {
                applicationFilter[0].isExpanded = false;
            }
        };

        /**
         * This method fetchestppID, clientID, applicationName, applicationID from application details directive and use as a parameter and return the same.
         */
        $scope.getSecretData = function (tppID, clientID, applicationName, applicationID) {
            return {
                tppID: tppID,
                clientID: clientID,
                applicationName: applicationName,
                applicationID: applicationID
            };
        };

        /**
         * Show secret method calls from directive applicationDetails showSecretData.
         * This method takes tppID, clientID, applicationName, applicationID as a parameters.
         * This method fetches secret through api rest call showSecretRequest based on tpp id, cliend id and applicaiton id.
         * After successful call from rest api method will call showSecretPopupOpen.
         * If showSecretRequest rest call fails method returns associated error.
         */
        $scope.showSecret = function (tppID, clientID, applicationName, applicationID) {
            RestService.showSecretRequest($scope.getSecretData(tppID, clientID, applicationName, applicationID)).then(function (resp) {
                if (resp.data.secret) {
                    $scope.clientSecret = resp.data;
                    $scope.showSecretPopupOpen($scope.clientSecret, applicationName);
                }
            },
                function (error) {
                    $scope.noticeData = null;
                    var noticeObject = CommonService.getNoticeObject(error);
                    if (noticeObject.noticeMessage) {
                        $scope.noticeData = {};
                        $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                        $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                        $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                        $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                    }
                    $timeout(function () {
                        $("#show" + clientID).focus();
                    }, 500);
                });
        };

        /**
         * This method opens show secret modal pop up bootstrap.
         * It takes clientSecret and applicationName as params.
         * uibModal directive is used and its controller is ShowSecretPopupController. It returns clientSecret.
         */
        $scope.showSecretPopupOpen = function (clientSecret, applicationName) {
            $uibModal.open({
                templateUrl: "app/views/show-secret-popup.html",
                animation: true,
                backdrop: true,
                keyboard: true,
                scope: $scope,
                resolve: {
                    secretData: function () {
                        return {
                            clientSecret: clientSecret,
                            applicationName: applicationName
                        };
                    }
                },
                controller: "ShowSecretPopupController"
            }).opened.then(function () {
                $scope.removeErrorData();
            });
        };

        /**
         * Reset secret method calls from directive applicationDetails resetSecretData.
         * This method takes currentAppData as a parameters.
         * This method fetches secret through api rest call resetSecretRequest based on tpp id, cliend id and applicaiton id.
         * After successful call from rest api method shows success notice.
         * If resetSecretRequest rest call fails method returns associated error.
         */
        $scope.resetSecret = function (currentAppData) {
            var applicationName = currentAppData.applicationName;
            var applicationclientId = currentAppData.clientID;
            RestService.resetSecretRequest(currentAppData).then(function (resp) {
                var applicationSuccessLabel = $scope.clientSecretResetSuccessPreLabel + applicationName + $scope.clientSecretResetSuccessPostLabel;
                $scope.successData = {};
                $scope.successData.noticeMessage = applicationSuccessLabel;
                $scope.successData.noticeType = Constants.NoticeConstants["success"];
                $scope.successData.noticePrefix = $translate.instant("ERROR_MESSAGES.SUCCESS_LABEL");
                $scope.successData.successCloseScreenReaderLabel = $translate.instant("APPLICATION_PAGE.SUCCESS_CLOSE_BUTTON_SCREEN_READER_LABEL");
                $timeout(function () {
                    $("#reset" + applicationclientId).focus();
                }, 500);
            },
                function (error) {
                    var noticeObject = CommonService.getNoticeObject(error);
                    if (noticeObject.noticeMessage) {
                        $scope.noticeData = {};
                        $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                        $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                        $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                        $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                    }
                    $timeout(function () {
                        $("#reset" + applicationclientId).focus();
                    }, 500);
                });
        };

        /**
         * This method opens reset secret modal pop up bootstrap.
         * It takes currentApp as params.
         * uibModal directive is used and its controller is ResetSecretPopupController.
         */
        $scope.resetSecretPopupOpen = function (currentApp) {
            $uibModal.open({
                templateUrl: "app/views/reset-secret-popup.html",
                animation: true,
                backdrop: true,
                keyboard: true,
                scope: $scope,
                resolve: {
                    currentAppData: function () {
                        return currentApp;
                    }
                },
                controller: "ResetSecretPopupController"
            }).opened.then(function () {
                $scope.removeErrorData();
            });
        };

        /**
         * Delete secret method calls from directive applicationDetails deleteApplicaiton.
         * This method takes currentAppData as a parameters.
         * This method fetches secret through api rest call deleteApplicationRequest based on tpp id, cliend id and applicaiton id.
         * After successful call from rest api method shows success notice.
         * If deleteApplicationRequest rest call fails method returns associated error.
         */
        $scope.deleteApplication = function (currentAppData) {
            var applicationName = currentAppData.applicationName;
            var applicationclientId = currentAppData.clientID;
            RestService.deleteApplicationRequest(currentAppData).then(function (resp) {
                var applicationSuccessLabel = $scope.applicationDeleteSuccessPreLabel + applicationName + $scope.applicationDeleteSuccessPostLabel;
                $scope.successData = {};
                $scope.successData.noticeMessage = applicationSuccessLabel;
                $scope.successData.noticeType = Constants.NoticeConstants["success"];
                $scope.successData.noticePrefix = $translate.instant("ERROR_MESSAGES.SUCCESS_LABEL");
                $scope.successData.successCloseScreenReaderLabel = $translate.instant("APPLICATION_PAGE.SUCCESS_CLOSE_BUTTON_SCREEN_READER_LABEL");
                $scope.getApplicationLists();
                $timeout(function () {
                    $("#btn-close").focus();
                }, 500);
            },
                function (error) {
                    var noticeObject = CommonService.getNoticeObject(error);
                    if (noticeObject.noticeMessage) {
                        $scope.noticeData = {};
                        $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                        $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                        $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                        $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                    }
                    $timeout(function () {
                        $("#delete" + applicationclientId).focus();
                    }, 500);
                });
        };

        /**
         * This method opens delete application modal pop up bootstrap.
         * It takes currentApp as params.
         * uibModal directive is used and its controller is DeleteApplicationPopupController.
         */
        $scope.deleteApplicationPopupOpen = function (currentApp) {
            $uibModal.open({
                templateUrl: "app/views/delete-application-popup.html",
                animation: true,
                backdrop: true,
                keyboard: true,
                scope: $scope,
                resolve: {
                    currentAppData: function () {
                        return currentApp;
                    }
                },

                controller: "DeleteApplicationPopupController"
            }).opened.then(function () {
                $scope.removeErrorData();
            });

        };

        /**
         * Remove Error Data reset all fields while doing any activity on my application screen.
         * It comes from applicationDetails directive.
         */
        $scope.removeErrorData = function () {
            $timeout(function () {
                $scope.noticeData = null;
                $scope.warningData = null;
                $scope.alertData = null;
            });
        };

        /**
         * This method is for accesibility purpose to skip the content and on tab focus on main content.
         */
        $scope.focusMainContent = function () {
            if ($scope.tppBlocked) {
                $("#roleTooltipID").focus();
            } else {
                $("#btn-add-application").focus();
            }

        };

        /**
         * This method shift focus after close button on notice has been pressed.
         */
        $scope.focusAfterNoticeDismissed = function (noticeType, isCrossBtnEnable) {
            $("#btn-add-application").focus();
            if (noticeType === Constants.NoticeConstants["success"] && isCrossBtnEnable) {
                $scope.successData = null;
            }
        };

        $scope.init();
    }
]);

/**
 * This controller renders delete secret popup on click of delete icon button.
 */

"use strict";
angular.module("tpp").controller("DeleteApplicationPopupController", ["$scope", "$uibModalInstance", "$window", "$translate", "currentAppData",
    function($scope, $uibModalInstance, $window, $translate, currentAppData) {
        /**
         * This method is used to dismiss the delete secret popup modal on click of noButton.
         */
        $scope.noBtnClicked = function() {
            $uibModalInstance.dismiss("cancel");
        };
        /**
         * This method is used to delete client secret using its api call and dismiss the delete secret popup modal on click of yesButton.
         * It also calls deleteApplication method of applicationCtroller its param is currentAppData.
         */
        $scope.yesBtnClicked = function() {
            $scope.deleteApplication(currentAppData);
            $uibModalInstance.dismiss("cancel");
        };
        /**
         * Init method translates all static content for delete secret modal popup.
         * popCheckBox method is used for checbox check uncheck value.
         */
        $scope.init = function() {
            $scope.popupCheckBox = {
                acceptValue: false
            };
            $translate(["DELETE_APPLICATION_POPUP.POPUP_HEADING", "DELETE_APPLICATION_POPUP.POPUP_MESSAGE1",
                "DELETE_APPLICATION_POPUP.POPUP_MESSAGE2",
                "DELETE_APPLICATION_POPUP.POPUP_MESSAGE_CHECKBOX_LABEL",
                "DELETE_APPLICATION_POPUP.BUTTONS.YES_BUTTON_LABEL",
                "DELETE_APPLICATION_POPUP.BUTTONS.NO_BUTTON_LABEL",
                "BUTTON_SCREEN_READER_LABEL",
                "CLOSE_BUTTON_SCREEN_READER_LABEL",
                "DELETE_APPLICATION_POPUP.POPUP_SCREEN_READER_LABEL"
            ]).then(function(translations) {
                $scope.deleteApplicationPopupHeaderLabel = translations["DELETE_APPLICATION_POPUP.POPUP_HEADING"];
                $scope.deleteApplicationDescText = translations["DELETE_APPLICATION_POPUP.POPUP_MESSAGE1"];
                $scope.deleteApplicationDescText2 = translations["DELETE_APPLICATION_POPUP.POPUP_MESSAGE2"];
                $scope.deleteApplicationCheckboxLabel = translations["DELETE_APPLICATION_POPUP.POPUP_MESSAGE_CHECKBOX_LABEL"];
                $scope.deleteApplicationBtnText1 = translations["DELETE_APPLICATION_POPUP.BUTTONS.NO_BUTTON_LABEL"];
                $scope.deleteApplicationBtnText2 = translations["DELETE_APPLICATION_POPUP.BUTTONS.YES_BUTTON_LABEL"];
                $scope.deleteApplicationScrBtnText = translations["BUTTON_SCREEN_READER_LABEL"];
                $scope.deleteApplicationScrCloseBtnText = translations["CLOSE_BUTTON_SCREEN_READER_LABEL"];
                $scope.deleteApplicationScrPopupHeading = translations["DELETE_APPLICATION_POPUP.POPUP_SCREEN_READER_LABEL"];

            });
        };
        $scope.init();

    }
]);

/**
 * This controller is when user logs into tpp portal.
 */
"use strict";
angular.module("tpp").controller("LoginCtrl", ["$scope", "$window", "$translate", "$filter", "RestService", "CommonService", "$state", "Constants", "$timeout",
    function ($scope, $window, $translate, $filter, RestService, CommonService, $state, Constants, $timeout) {
        /**
         * Init Method thorws notice if cookie is disabled.
         * It displays page level error.
         * It displays maintenance level notice.
         * Init method translates all static content for select organisation. After translation has been load it loads cookie, page or maintenance errors.
         */
        $scope.init = function () {
            $scope.pageError = $("#error").val();
            $scope.translationLoaded = false;
            $scope.maintenanceAlertData = null;
            $scope.cookieNoticeObject = null;

            $scope.$watch("translationLoaded", function (newVal) {
                if ($scope.isCookieDisclaimer === "true") {
                    var cookieNoticeObj = CommonService.getNoticeObject({ "errorCode": "COOKIE_DISCLAIMER_NOTICE" });
                    if (cookieNoticeObj.noticeMessage) {
                        $scope.cookieNoticeObject = {};
                        $scope.cookieNoticeObject.noticeMessage = cookieNoticeObj.noticeMessage;
                        $scope.cookieNoticeObject.cookieDefault = true;
                        $scope.cookieNoticeObject.noticeType = Constants.NoticeConstants["warning"];
                        $scope.cookieNoticeObject.noticePrefix = $scope.noticePrefix;
                        $scope.cookieNoticeObject.noticeDescription = cookieNoticeObj.noticeDescription;
                        $scope.cookieNoticeObject.successCloseScreenReaderLabel = $scope.successCloseScreenReaderLabel;
                    }
                }
                if (newVal) {
                    var noticeObject;
                    if (!navigator.cookieEnabled) {
                        noticeObject = CommonService.getNoticeObject({ "errorCode": "COOKIE_DISABLED_ERROR_MSG" });
                    } else if ($scope.pageError) {
                        noticeObject = CommonService.getNoticeObject($scope.pageError);
                    }
                    if (noticeObject && noticeObject.noticeMessage) {
                        $scope.noticeData = {};
                        $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                        $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                        $scope.noticeData.noticePrefix = $scope.pageErrorPrefix;
                        $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                    }
                    if ($scope.maintenanceNotice === "true") {
                        var maintenanceNoticeObj = CommonService.getNoticeObject({ "errorCode": "NOTICE_MESSAGE" });
                        if (maintenanceNoticeObj && maintenanceNoticeObj.noticeMessage) {
                            $scope.maintenanceNoticeData = {};
                            $scope.maintenanceNoticeData.noticeMessage = maintenanceNoticeObj.noticeMessage;
                            $scope.maintenanceNoticeData.noticeType = Constants.NoticeConstants["warning"];
                            $scope.maintenanceNoticeData.noticePrefix = $scope.noticePrefix;
                            $scope.maintenanceNoticeData.noticeDescription = maintenanceNoticeObj.noticeDescription;
                        }
                    }
                    if ($scope.maintenanceAlert === "true") {
                        var maintenanceAlertObj = CommonService.getNoticeObject({ "errorCode": "ALERT_MESSAGE" });
                        if (maintenanceAlertObj && maintenanceAlertObj.noticeMessage) {
                            $scope.maintenanceAlertData = {};
                            $scope.maintenanceAlertData.noticeMessage = maintenanceAlertObj.noticeMessage;
                            $scope.maintenanceAlertData.noticeType = Constants.NoticeConstants["alert"];
                            $scope.maintenanceAlertData.noticePrefix = $scope.alertPrefix;
                            $scope.maintenanceAlertData.noticeDescription = maintenanceAlertObj.noticeDescription;
                        }
                    }
                }
            });
            $translate(["LOGIN_OPEN_BANKING_PAGE.TPP_PORTAL_INFO.LOGIN_HEADING",
                "LOGIN_OPEN_BANKING_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.TPP_PORTAL_INTRO_PARA1",
                "LOGIN_OPEN_BANKING_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.TPP_PORTAL_INTRO_PARA2",
                "LOGIN_OPEN_BANKING_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.TPP_PORTAL_INTRO_PARA3",
                "LOGIN_OPEN_BANKING_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.TPP_PORTAL_INTRO_PARA4",
                "LOGIN_OPEN_BANKING_PAGE.LOGIN_FORM.LOGIN_FORM_HEADING",
                "LOGIN_OPEN_BANKING_PAGE.LOGIN_FORM.LOGIN_FORM_INSTRUCTION_LABEL",
                "LOGIN_OPEN_BANKING_PAGE.LOGIN_FORM.BUTTON.LOGIN_BUTTON_LABEL",
                "LINK_TOOLTIP_SAME_WINDOW",
                "ERROR_MESSAGES.ERROR_LABEL", "ERROR_MESSAGES.SHOW_NOTICE",
                "ERROR_MESSAGES.NOTICE_MESSAGE", "ERROR_MESSAGES.COOKIE_DISCLAIMER_NOTICE", "ERROR_MESSAGES.NOTICE_LABEL",
                "LOGIN_OPEN_BANKING_PAGE.SUCCESS_CLOSE_BUTTON_SCREEN_READER_LABEL",
                "ERROR_MESSAGES.SHOW_COOKIE_DISCLAIMER_NOTICE",
                "ERROR_MESSAGES.SHOW_ALERT",
                "ERROR_MESSAGES.ALERT_MESSAGE",
                "ERROR_MESSAGES.ALERT_LABEL"
            ]).then(function (translations) {
                $scope.tppLoginHeading = translations["LOGIN_OPEN_BANKING_PAGE.TPP_PORTAL_INFO.LOGIN_HEADING"];
                $scope.tppPortalIntroPara1 = translations["LOGIN_OPEN_BANKING_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.TPP_PORTAL_INTRO_PARA1"];
                $scope.tppPortalIntroPara2 = translations["LOGIN_OPEN_BANKING_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.TPP_PORTAL_INTRO_PARA2"];
                $scope.tppPortalIntroPara3 = translations["LOGIN_OPEN_BANKING_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.TPP_PORTAL_INTRO_PARA3"];
                $scope.tppPortalIntroPara4 = translations["LOGIN_OPEN_BANKING_PAGE.TPP_PORTAL_INFO.TPP_PORTAL_INTRO.TPP_PORTAL_INTRO_PARA4"];
                $scope.LoginFormHeading = translations["LOGIN_OPEN_BANKING_PAGE.LOGIN_FORM.LOGIN_FORM_HEADING"];
                $scope.LoginInstructionLabel = translations["LOGIN_OPEN_BANKING_PAGE.LOGIN_FORM.LOGIN_FORM_INSTRUCTION_LABEL"];
                $scope.LoginButtonLabel = translations["LOGIN_OPEN_BANKING_PAGE.LOGIN_FORM.BUTTON.LOGIN_BUTTON_LABEL"];
                $scope.sameWindowTt = translations["LINK_TOOLTIP_SAME_WINDOW"];
                $scope.pageErrorPrefix = translations["ERROR_MESSAGES.ERROR_LABEL"];
                $scope.maintenanceNotice = translations["ERROR_MESSAGES.SHOW_NOTICE"];
                $scope.maintenanceMsg = translations["ERROR_MESSAGES.NOTICE_MESSAGE"];
                $scope.maintenanceAlert = translations["ERROR_MESSAGES.SHOW_ALERT"];
                $scope.maintenanceAlertMsg = translations["ERROR_MESSAGES.ALERT_MESSAGE"];
                $scope.alertPrefix = translations["ERROR_MESSAGES.ALERT_LABEL"];
                $scope.cookieNotice = translations["ERROR_MESSAGES.COOKIE_DISCLAIMER_NOTICE"];
                $scope.noticePrefix = translations["ERROR_MESSAGES.NOTICE_LABEL"];
                $scope.successCloseScreenReaderLabel = translations["LOGIN_OPEN_BANKING_PAGE.SUCCESS_CLOSE_BUTTON_SCREEN_READER_LABEL"];
                $scope.isCookieDisclaimer = translations["ERROR_MESSAGES.SHOW_COOKIE_DISCLAIMER_NOTICE"];
                $scope.translationLoaded = true;
            });
        };
        /**
         * This method is for accesibility purpose to skip the content and on tab focus on main content.
         */
        $scope.focusMainContent = function () {
            $("#loginButton").focus();
        };

        /**
         * This method shift focus after close button on notice has been pressed.
         */
        $scope.focusAfterNoticeDismissed = function (noticeType, isCrossBtnEnable) {
            $("#loginButton").focus();
            if (noticeType === Constants.NoticeConstants["warning"] && isCrossBtnEnable) {
                $scope.cookieNoticeObject = null;
            }
        };

        $scope.init();
    }
]);

/**
 * This controller renders reset secret popup on click of reset button.
 */
"use strict";
angular.module("tpp").controller("ResetSecretPopupController", ["$scope", "$uibModalInstance", "$window", "$translate", "currentAppData",
    function($scope, $uibModalInstance, $window, $translate, currentAppData) {
        /**
         * This method is used to dismiss the reset secret popup modal on click of noButton.
         */
        $scope.noBtnClicked = function() {
            $uibModalInstance.dismiss("cancel");
        };
        /**
         * This method is used to reset client secret using its api call and dismiss the reset secret popup modal on click of yesButton.
         * It also calls resetSecret method of applicationCtroller its param is currentAppData.
         */
        $scope.yesBtnClicked = function() {
            $scope.resetSecret(currentAppData);
            $uibModalInstance.dismiss("cancel");
        };
        /**
         * Init method translates all static content for reset secret modal popup.
         * popCheckBox method is used for checbox check uncheck value.
         */
        $scope.init = function() {
            $scope.popupCheckBox = {
                acceptValue: false
            };
            $translate(["RESET_CLIENT_SECRET_POPUP.POPUP_HEADING", "RESET_CLIENT_SECRET_POPUP.POPUP_MESSAGE",
                "RESET_CLIENT_SECRET_POPUP.POPUP_MESSAGE_CHECKBOX_LABEL",
                "RESET_CLIENT_SECRET_POPUP.BUTTONS.YES_BUTTON_LABEL",
                "RESET_CLIENT_SECRET_POPUP.BUTTONS.NO_BUTTON_LABEL",
                "BUTTON_SCREEN_READER_LABEL",
                "CLOSE_BUTTON_SCREEN_READER_LABEL",
                "RESET_CLIENT_SECRET_POPUP.POPUP_SCREEN_READER_LABEL"
            ]).then(function(translations) {
                $scope.resetSecretPopupHeaderLabel = translations["RESET_CLIENT_SECRET_POPUP.POPUP_HEADING"];
                $scope.resetSecretDescText = translations["RESET_CLIENT_SECRET_POPUP.POPUP_MESSAGE"];
                $scope.resetSecretCheckboxLabel = translations["RESET_CLIENT_SECRET_POPUP.POPUP_MESSAGE_CHECKBOX_LABEL"];
                $scope.resetSecretBtnText1 = translations["RESET_CLIENT_SECRET_POPUP.BUTTONS.NO_BUTTON_LABEL"];
                $scope.resetSecretBtnText2 = translations["RESET_CLIENT_SECRET_POPUP.BUTTONS.YES_BUTTON_LABEL"];
                $scope.resetSecretScrBtnText = translations["BUTTON_SCREEN_READER_LABEL"];
                $scope.resetSecretScrCloseBtnText = translations["CLOSE_BUTTON_SCREEN_READER_LABEL"];
                $scope.resetSecretScrPopupHeading = translations["RESET_CLIENT_SECRET_POPUP.POPUP_SCREEN_READER_LABEL"];
            });
        };
        $scope.init();
    }
]);

/**
 * This controller is for tpp selection and stores selected tpp org Id in localStorage.
 */
"use strict";
angular.module("tpp").controller("SelectOrganisationCtrl", ["$scope", "$window", "$translate", "$filter", "RestService", "CommonService", "$state", "Constants", "$timeout",
    function ($scope, $window, $translate, $filter, RestService, CommonService, $state, Constants, $timeout) {
        /**
         * This method fetches tppList through api rest call tppListRequest and set tppList in localstorage.
         * If tppListRequest rest call fails method returns associated error.
         * It sets logic of accordian open and close.
         * If there is only one tpp then it will bypass this screen. Directly continue button has been pressed.
         * If there are no tpp found from rest call it will show notice.
         */
        $scope.getTppLists = function () {
            $scope.noticeData = null;
            $scope.warningData = null;
            RestService.tppListRequest().then(function (resp) {
                $scope.tppList = resp.data;
                if ($scope.tppList.length === 0) {
                    $scope.disableMyAppNav = true;
                       $scope.warningData = {};
                        $scope.warningData.noticeMessage = $scope.notTppLabel;
                        $scope.warningData.noticeType = Constants.NoticeConstants["warning"];
                        $scope.warningData.noticePrefix = $translate.instant("ERROR_MESSAGES.NOTICE_LABEL");
                                      $scope.hideskipToContLink = true;
                } else {
                    angular.forEach($scope.tppList, function (tpp) {
                        tpp.isOpen = false;
                    }, this);
                    $window.localStorage.setItem("tppList", JSON.stringify($scope.tppList));
                    $scope.disableMyAppNav = true;
                    if ($scope.tppList.length === 1) {
                        $scope.status.selectedOrg = $scope.tppList[0];
                        $scope.continueButtonClicked();
                    }else{
                        $scope.showOrgList = true;
                    }
                }
            },
                function (error) {
                    $scope.disableMyAppNav = true;
                    $scope.hideskipToContLink = true;
                    var noticeObject = CommonService.getNoticeObject(error);
                    if (noticeObject.noticeMessage) {
                        $scope.noticeData = {};
                        $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                        $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                        $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                        $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                    }
                });
        };
        /**
         * Init Method thorws notice if cookie is disabled.
         * It display page level error.
         * It fetches selected Org from local storage and enable continue button.
         * Init method translates all static content for select organisation.
         * It checkes if any tpp found in localstorage it will display those tpp but if any tpp not found then it will call getTppLists.
         */
        $scope.init = function () {
            var noticeObject = null;
            $scope.showOrgList = false
            $scope.hideskipToContLink = false;
            $scope.pageError = $("#error").val();
            if (!$window.navigator.cookieEnabled) {
                $scope.disableMyAppNav = true;
                noticeObject = CommonService.getNoticeObject({ "errorCode": "COOKIE_DISABLED_ERROR_MSG" });
                if (noticeObject.noticeMessage) {
                    $scope.noticeData = {};
                    $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                    $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                    $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                    $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                }
            } else if ($scope.pageError) {
                $scope.disableMyAppNav = true;
                noticeObject = CommonService.getNoticeObject($scope.pageError);
                if (noticeObject.noticeMessage) {
                    $scope.noticeData = {};
                    $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                    $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                    $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                    $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                }
            } else {
                $scope.status = {
                    isCustomHeaderOpen: false,
                    isFirstOpen: true,
                    isFirstDisabled: false,
                    selectedOrg: null,
                    disablebtn: true
                };
                $scope.disableMyAppNav = false;
                $scope.tppList = [];
                $scope.noticeData = null;
                $scope.tppDetailsLabels = {};
                $translate(["TPP_SELECTION_PAGE.TPP_SELECTION_PAGE_HEADING",
                    "TPP_SELECTION_PAGE.NO_TPP_INSTRUCTION_LABEL",
                    "TPP_SELECTION_PAGE.TPP_SELECTION_ORG_INSTRUCTION_LABEL",
                    "TPP_SELECTION_PAGE.TPP_DETAILS.ORGANIZATION_ID_LABEL",
                    "TPP_SELECTION_PAGE.TPP_DETAILS.ORGANIZATION_ROLE_LABEL",
                    "TPP_SELECTION_PAGE.TPP_DETAILS.PHONE_NUMBER_LABEL",
                    "TPP_SELECTION_PAGE.TPP_DETAILS.STATE_COMPETENT_AUTHORITY_LABEL",
                    "TPP_SELECTION_PAGE.TPP_DETAILS.EMAIL_ID_LABEL",
                    "TPP_SELECTION_PAGE.TPP_DETAILS.REG_ID_LABEL",
                    "TPP_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL",
                    "TPP_SELECTION_PAGE.TPP_DETAILS.NA_LABEL",
                    "TPP_SELECTION_PAGE.TPP_DETAILS.NA_LABEL"
                ]).then(function (translations) {
                    $scope.tppSelectionPageHeaderLabel = translations["TPP_SELECTION_PAGE.TPP_SELECTION_PAGE_HEADING"];
                    $scope.tppSelectOrgMessageText = translations["TPP_SELECTION_PAGE.TPP_SELECTION_ORG_INSTRUCTION_LABEL"];
                    $scope.notTppLabel = translations["TPP_SELECTION_PAGE.NO_TPP_INSTRUCTION_LABEL"];
                    $scope.tppDetailsLabels.tppSelectionOrgIdText = translations["TPP_SELECTION_PAGE.TPP_DETAILS.ORGANIZATION_ID_LABEL"];
                    $scope.tppDetailsLabels.tppSelectionOrgRoleText = translations["TPP_SELECTION_PAGE.TPP_DETAILS.ORGANIZATION_ROLE_LABEL"];
                    $scope.tppDetailsLabels.tppSelectionPhoneNoText = translations["TPP_SELECTION_PAGE.TPP_DETAILS.PHONE_NUMBER_LABEL"];
                    $scope.tppDetailsLabels.tppSelectionCompetentAuthority = translations["TPP_SELECTION_PAGE.TPP_DETAILS.STATE_COMPETENT_AUTHORITY_LABEL"];
                    $scope.tppDetailsLabels.tppSelectionEmailId = translations["TPP_SELECTION_PAGE.TPP_DETAILS.EMAIL_ID_LABEL"];
                    $scope.tppDetailsLabels.tppSelectionRegId = translations["TPP_SELECTION_PAGE.TPP_DETAILS.REG_ID_LABEL"];
                    $scope.continueButtonLabel = translations["TPP_SELECTION_PAGE.BUTTONS.CONTINUE_BUTTON_LABEL"];
                    $scope.tppDetailsLabels.emailId = translations["TPP_SELECTION_PAGE.TPP_DETAILS.NA_LABEL"];
                    $scope.tppDetailsLabels.phoneNo = translations["TPP_SELECTION_PAGE.TPP_DETAILS.NA_LABEL"];
                });
                try {
                    if ($window.localStorage.tppList) {
                        var selectedOrg = $window.localStorage.getItem("selectedOrg");
                        if (selectedOrg) {
                            var tppList = JSON.parse($window.localStorage.getItem("tppList"));
                            $filter("filter")(tppList, { "tppOrganizationId": JSON.parse(selectedOrg).tppOrganizationId })[0].isOpen = true;
                            $scope.tppList = tppList;
                            $scope.status.selectedOrg = selectedOrg;
                            $scope.status.disablebtn = false;
                            $scope.showOrgList = true;
                        }
                    } else {
                        $scope.getTppLists();
                    }
                } catch (e) {
                    //
                }

            }
        };
        /**
         * This method call on continue button clicked and set selected org in local storage.
         * It calls check session rest call on continue button clicked. If session is valid it take user to myapplication screen, if not then it will show session popup.
         * While calling check session if rest call fails then it will show error.
         */
        $scope.continueButtonClicked = function () {
            var stringifiedData = angular.isObject($scope.status.selectedOrg) ? JSON.stringify($scope.status.selectedOrg) : $scope.status.selectedOrg;
            $window.localStorage.setItem("selectedOrg", stringifiedData);
            CommonService.checkSession("myApplication", false).then(function (noticeObject) {
                if (noticeObject) {
                    $scope.hideskipToContLink = true;
                    $scope.noticeData = {};
                    $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                    $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                    $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                    $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                }
            });
        };
        /**
         * This method is for accesibility purpose to skip the content and on tab focus on main content.
         */
        $scope.focusMainContent = function () {
            if ($scope.tppList.length) {
                $("input:radio[name=tpp]:first").focus();
            }
        };

        $scope.init();
    }
]);

/**
 * This controller renders show secret popup on click of show button.
 */
"use strict";
angular.module("tpp").controller("ShowSecretPopupController", ["$scope", "$uibModalInstance", "$window", "$translate", "RestService", "secretData", "clipboard",
    function($scope, $uibModalInstance, $window, $translate, RestService, secretData, clipboard) {
        /**
         * This method copies client secret on clipboard while pressing Copy client secret button.
         */
        $scope.copyClientSecretClicked = function() {
            $scope.copyToClipboard();
            $uibModalInstance.dismiss("cancel");
        };
        /**
         * This method is used to dismiss the show secret popup modal on click of ok button.
         */
        $scope.okBtnClicked = function() {
            $uibModalInstance.dismiss("cancel");
        };
        /**
         * This method copies client secret on clipboard.
         * angular-clipboard has been used for copy
         */
        $scope.copyToClipboard = function() {
            if ($scope.clientSecret) {
                clipboard.copyText($scope.clientSecret.secret);
            }
        };
        /**
         * Init method translates all static content for reset secret modal popup.
         * It calls showSecretDetails to render message on show secret popup.
         */
        $scope.init = function() {
            $translate(["SHOW_CLIENT_SECRET_POPUP.POPUP_HEADING",
                "SHOW_CLIENT_SECRET_POPUP.BUTTONS.COPY_CLIENT_SECRET_BUTTON_LABEL",
                "SHOW_CLIENT_SECRET_POPUP.BUTTONS.OK_BUTTON_LABEL",
                "BUTTON_SCREEN_READER_LABEL",
                "CLOSE_BUTTON_SCREEN_READER_LABEL",
                "SHOW_CLIENT_SECRET_POPUP.POPUP_SCREEN_READER_LABEL"
            ]).then(function(translations) {
                $scope.showSecretPopupHeaderLabel = translations["SHOW_CLIENT_SECRET_POPUP.POPUP_HEADING"];
                $scope.showSecretBtnText1 = translations["SHOW_CLIENT_SECRET_POPUP.BUTTONS.COPY_CLIENT_SECRET_BUTTON_LABEL"];
                $scope.showSecretBtnText2 = translations["SHOW_CLIENT_SECRET_POPUP.BUTTONS.OK_BUTTON_LABEL"];
                $scope.showSecretScrBtnText = translations["BUTTON_SCREEN_READER_LABEL"];
                $scope.showSecretScrCloseBtnText = translations["CLOSE_BUTTON_SCREEN_READER_LABEL"];
                $scope.showSecretScrPopupHeading = translations["SHOW_CLIENT_SECRET_POPUP.POPUP_SCREEN_READER_LABEL"];
            });
            $scope.showSecretDetails(secretData);
        };

        $scope.showSecretDetails = function(secretData) {
            $translate(["SHOW_CLIENT_SECRET_POPUP.POPUP_MESSAGE_PRE",
                "SHOW_CLIENT_SECRET_POPUP.POPUP_MESSAGE_POST"
            ]).then(function(translations) {
                $scope.showSecretDescTextPre = translations["SHOW_CLIENT_SECRET_POPUP.POPUP_MESSAGE_PRE"];
                $scope.showSecretDescTextPost = translations["SHOW_CLIENT_SECRET_POPUP.POPUP_MESSAGE_POST"];
                $scope.showSecretDescText = $scope.showSecretDescTextPre +
                    secretData.applicationName +
                    $scope.showSecretDescTextPost +
                    " " + secretData.clientSecret.secret;
            });
        };

        $scope.init();
    }
]);

/**
 * This directive is for application details under my application view.
 * It has show secret secret, reset secret and delete application functionality.
 * applicationData is data passed from application controller.
 * toggleApplicationDetails show more - show less function.
 * showSecret call function from controller to show secret popup.
 * resetSecret call function from controller to reser secret popup.
 * deleteApplication call function from controller to delete application popup.
 * tppOrgId selected tpp id.
 * tppBlocked disable show secret, reset secret and delete application icon.
 * removeErrorData reset error, notice, warning object on controller.
 * labels static content data passed from controller.
 */
"use strict";
angular.module("tpp").directive("applicationDetails", ["blockUI", "$window", "$timeout",
    function (blockUI, $window, $timeout) {
        return {
            restrict: "EA",
            scope: {
                applicationData: "=",
                toggleApplicationDetails: "&",
                showSecret: "&",
                resetSecret: "&",
                deleteApplication: "&",
                tppOrgId: "=",
                tppBlocked: "=",
                removeErrorData: "&",
                labels: "="
            },
            templateUrl: "app/views/application-details.html",
            /**
             * This link method expand and collapse table rows.
             * It calls showSecretData, resetSecretData, deleteApplicaiton functions.
             */
            link: function (scope) {
                scope.applicationData.isExpanded = false;
                /**
                 * This method collpase all rows expect one row which has been expanded.
                 * Also it shifts focus while row is expanded or collapsed.
                 */
                scope.expandCollapseRow = function () {
                    if (!scope.applicationData.isExpanded) {
                        scope.toggleApplicationDetails();
                        scope.applicationData.isExpanded = true;
                        $("#btn-expand" + scope.applicationData.clientId).focus();
                    } else {
                        scope.applicationData.isExpanded = false;
                        $("#btn-collapse" + scope.applicationData.clientId).focus();
                    }
                };
                /**
                * This method call show secret method of controller with tppID, clientID, applicationName and applicationID..
                * It resets notice, warning and error object.
                */
                scope.showSecretData = function () {
                    scope.showSecret({
                        tppID: scope.tppOrgId, clientID: scope.applicationData.clientId,
                        applicationName: scope.applicationData.cn, applicationID: scope.applicationData.muleAppId
                    });
                };
                /**
                * This method call reset secret method of controller with tppID, clientID, applicationName and applicationID..
                * It resets notice, warning and error object.
                */
                scope.resetSecretData = function () {
                    scope.resetSecret({
                        "currentApp": {
                            tppID: scope.tppOrgId, clientID: scope.applicationData.clientId,
                            applicationName: scope.applicationData.cn, applicationID: scope.applicationData.muleAppId
                        }
                    });
                };
                /**
                * This method call delete application method of controller with tppID, clientID, applicationName and applicationID..
                * It resets notice, warning and error object.
                */
                scope.deleteApplicaiton = function () {
                    scope.deleteApplication({
                        "currentApp": {
                            tppID: scope.tppOrgId, clientID: scope.applicationData.clientId,
                            applicationName: scope.applicationData.cn, applicationID: scope.applicationData.muleAppId
                        }
                    });
                };

            }
        };
    }
]);

/**
 * public/js/directives/acct-pagination.js
 * This directive is used for creating custom accordian with radio button.
 * Scope objects are tppOrgList,status,labels.tppOrgList is a tpporginasation list stored in local storage or from rest api getTppList call.Its template is customUibAccordian.html.
 */

"use strict";
angular.module("tpp").directive("customUibAccordian", ["$timeout", function ($timeout) {
    return {
        restrict: "E",
        scope: {
            tppOrgList: "=",
            status: "=",
            labels: "="
        },
        templateUrl: "app/views/customUibAccordian.html",
        /**
         * Inside this link function timeout is call to remove href and role from anchor tag present uibaccordian default html,
         * unwrap method is used removing h4 present in its default html and panel-title class of h4 is added to anchor tag using addClass.
         * cecked method is used for radio button check uncheck. If space or enter is pressed isOpen function of default
         * accordian.js is called, then user can't close radio accordian user only toggle between radio buttons. Else selectedOrg model will update.
         * If target is input type and its parent isOpen function is not called then its parent toggle open method will call and
         * continue button will be disabled unless a raio button is checked.
         */
        link: function (scope, ele) {
            scope.oneAtATime = true;
            $timeout(function () {
                $("a", ele).removeAttr("href role").addClass("panel-title").unwrap();
                var panelId = $(".panel-collapse", ele).attr("id");
                $(".panel-collapse",ele).removeAttr("aria-expanded");
                $(".panel-heading",ele).removeAttr("role");
                $("a.accordion-toggle", ele).removeAttr("aria-controls aria-expanded");
                $("input", ele).attr("aria-controls", panelId);
                
            });
            scope.checked = function ($event, scope) {
                if ($event.keyCode === 32 || $event.keyCode === 13) {
                    if (scope.$parent.isOpen) {
                        $event.stopPropagation();
                    } else {
                        scope.status.selectedOrg = scope.tpp;
                    }
                }
                if ($event.target.nodeName === "INPUT" && !scope.$parent.isOpen) {
                    scope.$parent.toggleOpen();
                }
                $timeout(function () {
                    if ($(".panel-collapse", ele).attr("aria-expanded")) {
                        $(".panel-collapse", ele).attr("aria-live", "assertive");
                    }
                    else {
                        $(".panel-collapse", ele).removeAttr("aria-live");
                    }
                });

                $event.stopPropagation();
                scope.status.disablebtn = false;
            };
        }
    };
}]);

/**
 * This directive renders different type messages like notice, error, alert and warning.
 * noticeData is json response of error, notice and warning.
 * focusafterdismissed is calls on close button of any error, warning and notice.
 */
"use strict";
angular.module("tpp").directive("errorNotice", ["$translate", "Constants",
    function ($translate, Constants) {
        return {
            restrict: "EA",
            scope: {
                noticeData: "=",
                focusafterdismissed: "&"
            },
            templateUrl: "app/views/error-notice.html",
            /**
            * This link function renders different type messages like notice, error, alert and warning.
            * It also enable - disable close button based on requirement.
            */
            link: function ($scope) {
                $scope.alertCssClass = "";
                $scope.noticeDismissed = false;
                $scope.isCrossBtnEnable = false;
                $scope.isShowNoticeDescription = false;
                $scope.isClosable = "";
                $scope.$watch("noticeData", function () {
                    $scope.noticeDismissed = false;
                    if ($scope.noticeData.noticeType === Constants.NoticeConstants["success"]) {
                        $scope.alertCssClass = "page-alert-success";
                        $scope.isCrossBtnEnable = true;
                        $scope.isClosable = "alert-closeable";
                    } else if ($scope.noticeData.noticeType === Constants.NoticeConstants["warning"]) {
                        $scope.alertCssClass = "page-alert-warning";
                        if ($scope.noticeData.cookieDefault) {
                            $scope.isCrossBtnEnable = true;
                            $scope.isClosable = "alert-closeable";
                        }
                    } else {
                        $scope.alertCssClass = "page-alert-danger";
                    }
                    if ($scope.noticeData.noticeDescription) {
                        $scope.isShowNoticeDescription = true;
                    }
                });
                /**
                 * This function dismisses the notice after close button tapped and shifts focus as per requirement.
                 */
                $scope.dismissNotice = function (type) {
                    $scope.noticeDismissed = true;
                    $scope.focusafterdismissed({ noticeType: type, isCrossBtnEnable: $scope.isCrossBtnEnable });
                };
            }
        };
    }
]);

/**
 * This directive is for footer links.
 * It displays cookie and privacy policy link, help link and regulatory statement.
 * The template for this directive is page-footer.html.
 * Scope object used is noticeData which is for two way data binding to display associted error message.
 */
"use strict";
angular.module("tpp").directive("pageFooter", ["config", "$translate", "RestService", "SessionPopupService",
    "$state", "blockUI", "$window", "$timeout", "CommonService", "Constants",
    function (config, $translate, RestService, SessionPopupService, $state, blockUI, $window, $timeout, CommonService, Constants) {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "app/views/page-footer.html",
            scope: {
                noticeData: "="
            },
            /**
             * This link method translate localized data from localization file.
             * It calls check session rest call on footer link clicked. If session is valid it take user to particular path, if not then it will show session popup.
             * While calling check session if rest call fails then it will show error.
             * It also check if the page is login it will not call session check api.login footer menu will display once the loginfootermenu is true.
             */
            link: function ($scope) {
                $translate(["FOOTER.PRIVACY_POLICY_LABEL", "HELP_LABEL", "LINK_TOOLTIP_NEW_WINDOW",
                    "FOOTER.PRIVACY_POLICY_URL", "HELP_URL",
                    "FOOTER.REGULATORY_LABEL"
                ]).then(function (translations) {
                    $scope.privacyPolicy = translations["FOOTER.PRIVACY_POLICY_LABEL"];
                    $scope.help = translations["HELP_LABEL"];
                    $scope.privacyPolicyURL = translations["FOOTER.PRIVACY_POLICY_URL"];
                    $scope.helpURL = translations["HELP_URL"];
                    $scope.regulatoryLabel = translations["FOOTER.REGULATORY_LABEL"];
                    $scope.tooltipTt = translations["LINK_TOOLTIP_NEW_WINDOW"];
                });
                /**
                 * It calls check session rest call on header link clicked. If session is valid it take user to particular path, if not then it will show session popup.
                 * While calling check session if rest call fails then it will show error.
                 */
                $scope.sessionCheck = function (path, isExternal, $event) {
                    $event.preventDefault();
                    if ($state.current.name !== path) {
                        CommonService.checkSession(path, isExternal).then(function (noticeObject) {
                            if (noticeObject) {
                                $scope.noticeData = {};
                                $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                                $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                                $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                                $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                            }
                        });
                    }
                };

                $scope.loginFooterMenu = false;
                if ($scope.$root.activePath === "/login") {
                    $scope.loginFooterMenu = true;
                }
            }
        };
    }
]);

/**
 * This directive is for header Navigation bar menu.
 * It display header logo, myOrganization, myApplication, Help Welcome user, Logout in navigation bar.
 * Welcome user is not a link.
 * myAppDisableNav is a paremter for two way data binding for disable myapplication navbar when no tpp is selected and continue button is not clicked.
 * pageError is scope object for two way data binding. It is used for page level error.
 * focuscontent is a method passed to every controller used for skip to content.
 * On tab when we click on skip to content focus on main content will redirect.
 * hideSkipToContLink is scope object used to hide skip to content link.
 * When no tppList.noticeData scope object is used to display error when tppis blocked.
 * loginHeaderMenu is used to show hide navigation bar on login page.
 */
"use strict";
angular.module("tpp").directive("pageHeader", ["config", "$translate", "RestService", "SessionPopupService",
    "$state", "blockUI", "$window", "$timeout", "CommonService", "Constants",
    function (config, $translate, RestService, SessionPopupService, $state, blockUI, $window, $timeout, CommonService, Constants) {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "app/views/page-header.html",
            scope: {
                myAppDisableNav: "=",
                pageError: "=",
                focuscontent: "&",
                hideSkipToContLink: "=",
                noticeData: "=",
                loginHeaderMenu: "=?"
            },
            /**
             * This link function translates the localized data which are used in pageheader.
             * It calls check session rest call on header link clicked. If session is valid it take user to particular path, if not then it will show session popup.
             * While calling check session if rest call fails then it will show error.
             */
            link: function ($scope) {
                var $ = window.jQuery;
                $scope.hide = false;
                $scope.$watch("hideSkipToContLink", function (newVal) {
                    if (newVal) {
                        $scope.hide = newVal;
                    }
                });
                $scope.username = $("#userName").val();
                $scope.activePath = $scope.$root.activePath;
                $translate(["HEADER.LOGO_URL", "HEADER.BANK_LOGO_IMAGE_ALTERNATE_LABEL", "LINK_TOOLTIP_NEW_WINDOW", "HEADER.PORTAL_TITLE",
                    "NAVIGATION_MENU.ORGANISATIONS_LABEL", "NAVIGATION_MENU.APPLICATIONS_LABEL", "HELP_LABEL", "NAVIGATION_MENU.WELCOME_LABEL",
                    "NAVIGATION_MENU.LOGOUT_LABEL", "HELP_URL",
                    "LINK_TOOLTIP_SAME_WINDOW", "LINK_UNAVAILABLE",
                    "LINK_CURRENT_PAGE",
                    "HEADER.SKIP_TO_MAIN_CONTENT"
                ]).then(function (translations) {
                    $scope.logoPath = translations["HEADER.LOGO_URL"];
                    $scope.logoAlternateLabel = translations["HEADER.BANK_LOGO_IMAGE_ALTERNATE_LABEL"];
                    $scope.tppHeader = translations["HEADER.PORTAL_TITLE"];
                    $scope.myOrganisation = translations["NAVIGATION_MENU.ORGANISATIONS_LABEL"];
                    $scope.myApplication = translations["NAVIGATION_MENU.APPLICATIONS_LABEL"];
                    $scope.helpNav = translations["HELP_LABEL"];
                    $scope.welcome = translations["NAVIGATION_MENU.WELCOME_LABEL"];
                    $scope.logout = translations["NAVIGATION_MENU.LOGOUT_LABEL"];
                    $scope.navHelpURL = translations["HELP_URL"];
                    $scope.helpTt = translations["LINK_TOOLTIP_NEW_WINDOW"];
                    $scope.sameWindowTt = translations["LINK_TOOLTIP_SAME_WINDOW"];
                    $scope.unAvailableTt = translations["LINK_UNAVAILABLE"];
                    $scope.currentPage = translations["LINK_CURRENT_PAGE"];
                    $scope.skiptoContent = translations["HEADER.SKIP_TO_MAIN_CONTENT"];
                });

                /**
                 * It calls check session rest call on header link clicked. If session is valid it take user to particular path, if not then it will show session popup.
                 * While calling check session if rest call fails then it will show error.
                 */
                $scope.sessionCheck = function (path, isExternal, $event) {
                    $event.preventDefault();
                    if ($state.current.name !== path) {
                        if (!$scope.myAppDisableNav || path !== "myApplication") {
                            CommonService.checkSession(path, isExternal).then(function (noticeObject) {
                                if (noticeObject) {
                                    $scope.noticeData = {};
                                    $scope.noticeData.noticeMessage = noticeObject.noticeMessage;
                                    $scope.noticeData.noticeType = Constants.NoticeConstants["error"];
                                    $scope.noticeData.noticeDescription = noticeObject.noticeDescription;
                                    $scope.noticeData.noticePrefix = $translate.instant("ERROR_MESSAGES.ERROR_LABEL");
                                }
                            });
                        }
                    }
                };
                /**
                 * This method return the page as per the associated path.
                 */
                $scope.checkPage = function (path) {
                    return "page";
                };
                /**
                 * This method call preventDefault method to prevent selected state otherwise because of href # in anchor will redirect state to my organization bydefault.
                 * It also call focuscontent to move focus on click of skip to content link to main content.
                 */
                $scope.skipToContentFocus = function ($event) {
                    $event.preventDefault();
                    $scope.focuscontent();
                };
            }
        };
    }
]);

"use strict";
angular.module("tpp").directive("popoverLeave", ["$timeout", function($timeout) {
    return {
        restrict: "A",
        link: function($scope, element) {
            $(element).on("mouseenter", function(e) {
                $("[popover-trigger]").each(function() {
                    //Only do this for all popovers other than the current one that cause this event
                    if (!($(this).is(e.currentTarget)) && ($(this).siblings("[uib-popover-popup]").length)) {
                        $(this).siblings("[uib-popover-popup]").remove();
                    }
                });
            });
        }
    };
}]);

/**
 * This is common utility service.
 */
"use strict";
angular.module("tpp").service("CommonService", ["$window", "$translate", "RestService", "$state", "SessionPopupService", "$timeout", "$q",
    function ($window, $translate, RestService, $state, SessionPopupService, $timeout, $q) {
        /**
         * This method provides notice message from static content based on error object from server.
         * It checkes error object is string or object, or does it have data parameter.
         * It opens session popup if particular error code comes from server. eg. 1204
         * It return errorCode in case of session check api rest call passes and throws session related error code. eg. 1225
         */
        function getNotice(err) {
           err = angular.isObject(err) ? err : JSON.parse(err);
            if (err.data) {
                err = err.data;
            }
            var errObj = {};
            errObj.errorCode = err ? (err.errorCode ? err.errorCode : "1200") : "1200";
            if (errObj.errorCode === "1204") {
                SessionPopupService.openSessionPopup(errObj.errorCode);
                return null;
            } else if (errObj.errorCode === "1225") {
                return errObj.errorCode;
            }
            else {
                var noticeObject = {};
                var noticeCodeDesc = "ERROR_MESSAGES." + errObj.errorCode + "_DETAILS";
                noticeObject.noticeMessage = $translate.instant("ERROR_MESSAGES." + errObj.errorCode);
                var desc = $translate.instant("ERROR_MESSAGES." + errObj.errorCode + "_DETAILS");
                noticeObject.noticeDescription = desc === noticeCodeDesc ? null : desc;
                return noticeObject;
            }
        }
        return {
            /**
             * This method provides notice message from static content based on error object from server.
             */
            getNoticeObject: function (err) {
                return getNotice(err);
            },
            /**
             * Check session checkes session on any element.
             * It takes path and isExternal params.
             * path after session check if session is valid then it redirects to path provided in params.
             * isExternal this checkes if redirection needs to open in another window or same window.
             * To check session through api rest call authSessionRequest
             * If authSessionRequest rest call fails, method returns associated error.
             * It error code is particular then it shows session popup for eg. 1225
             */
            checkSession: function (path, isExternal) {
                var deferred = $q.defer();
                var $ = window.jQuery;
                var popup = null;
                if (isExternal) {
                    popup = $window.open("", "_blank");
                    popup.document.body.innerHTML = "Loading, may take some time.";
                }
                RestService.authSessionRequest().then(function (resp) {
                    if (resp.data.statusCode === "200") {
                        if (isExternal) {
                            popup.location.href = path;
                        } else {
                            if (path === "logout") {
                                $window.location.href = $("#logoutURL").val();
                            } else {
                                $state.go(path);
                            }
                        }
                    }
                },
                    function (error) {
                        var errObj = {};
                        var errorData = getNotice(error);
                        if (errorData === "1225") {
                            if (popup === null || popup === undefined) {
                                SessionPopupService.openSessionPopup(errObj.errorCode);
                            } else {
                                popup.document.body.innerHTML = "Your session has timed out.";
                                $timeout(function () {
                                    popup.close();
                                    SessionPopupService.openSessionPopup(errObj.errorCode);
                                }, 1000);
                            }
                        }
                        else {
                            if (popup === null || popup === undefined) {
                                deferred.resolve(errorData);
                            }
                            else {
                                popup.document.body.innerHTML = "Loading, may take some time.";
                                $timeout(function () {
                                    popup.close();
                                    deferred.resolve(errorData);
                                }, 1000);
                            }
                        }
                    });
                return deferred.promise;
            },
            checkAuthorize: function () {
                var $ = window.jQuery;
                return $("#isAuthorize").val();
            }
        };
    }]);

/**
 * This is common rest api endpoint service.
 * corelationId is id which backend needs from each api endpoint call to track in logs.
 */
"use strict";
angular.module("tpp").service("RestService", ["$http", function ($http) {
    var corelationId = $("#corelationId").val();
    return {
        /**
         * authSessionRequest check session endpoint.
         * This is get http call and it passes URL and headers parameters.
         */
        authSessionRequest: function () {
            return $http.get("/tppportal/auth/session", { headers: { "x-fapi-interaction-id": corelationId } });
        },
        /**
         * applicationsListRequest fetch application list by tppOrgId endpoint.
         * tppOrgId is query string param.
         * This is get http call and it passes URL and headers parameters.
         */
        applicationsListRequest: function (reqData) {
            return $http.get("/tppportal/applications?tppOrgId="
                + reqData, { headers: { "x-fapi-interaction-id": corelationId } });
        },
        /**
         * tppListRequest fetch tpp list.
         * This is get http call and it passes URL and headers parameters.
         */
        tppListRequest: function () {
            return $http.get("/tppportal/organizations", { headers: { "x-fapi-interaction-id": corelationId } });
        },
        /**
         * uploadSSAtoken upload ssa token file to server endpoint
         * This is post http call and it passes URL, payload, transformRequest and headers parameters.
         */
        uploadSSAtoken: function (payload) {
            return $http.post("/tppportal/ssaToken", payload,
                { transformRequest: angular.identity, headers: { "Content-Type": undefined, "x-fapi-interaction-id": corelationId } });
        },
        /**
         * addApplication adds application to server endpoint
         * This is post http call and it passes URL, payload, transformRequest and headers parameters.
         */
        addApplication: function (payload) {
            return $http.post("/tppportal/application", payload,
                { transformRequest: angular.identity, headers: { "Content-Type": undefined, "x-fapi-interaction-id": corelationId } });
        },
        /**
         * showSecretRequest show secret by applicationID, tppID and clientId from server endpoint
         * applicationID id of selected application
         * tppID id of selected tpp
         * clientId of particular selected application
         * This is get http call and it passes URL and headers parameters.
         */
        showSecretRequest: function (reqData) {
            return $http.get("/tppportal/application/"
                + reqData.applicationID + "/secret?tppId=" + reqData.tppID
                + "&clientId=" + reqData.clientID, { headers: { "x-fapi-interaction-id": corelationId } });
        },
        /**
         * resetSecretRequest resets secret by applicationID, tppID and clientId from server endpoint
         * applicationID id of selected application
         * tppID id of selected tpp
         * clientId of particular selected application
         * This is get http call and it passes URL and headers parameters.
         */
        resetSecretRequest: function (reqData) {
            return $http.get("/tppportal/application/"
                + reqData.applicationID + "/secret/reset?tppId=" + reqData.tppID
                + "&clientId=" + reqData.clientID, { headers: { "x-fapi-interaction-id": corelationId } });
        },
        /**
         * deleteApplicationRequest deletes application by applicationID, tppID and clientId from server endpoint
         * applicationID id of selected application
         * tppID id of selected tpp
         * clientId of particular selected application
         * This is get http call and it passes URL and headers parameters.
         */
        deleteApplicationRequest: function (reqData) {
            return $http.delete("/tppportal/application/"
                + reqData.applicationID + "?tppId=" + reqData.tppID
                + "&clientId=" + reqData.clientID, { headers: { "x-fapi-interaction-id": corelationId } });
        }
    };
}]);

/**
 * Session popup controller is controller for SessionPopupService.
 * It translates messages on session popup.
 * It has only ok button.
 */
"use strict";
angular.module("tpp").controller("sessionPopupCtrl", ["$scope", "$uibModalInstance", "$window", "errorData", "$translate", "blockUI",
    function($scope, $uibModalInstance, $window, errorData, $translate, blockUI) {
        /**
         * This method calls on ok button click.
         * It dismiss popup as well as redirect to logout url as session is invalid.
         */
        $scope.okBtnClicked = function() {
            $uibModalInstance.dismiss("cancel");
            blockUI.start();
            var logoutURL = $("#logoutURL").val();
            $window.location.href = logoutURL;
        };
        $translate(["SESSION_TIMEOUT_POPUP.POPUP_HEADING",
            "SESSION_TIMEOUT_POPUP.POPUP_MESSAGE1",
            "SESSION_TIMEOUT_POPUP.POPUP_MESSAGE2",
            "SESSION_TIMEOUT_POPUP.POPUP_BUTTON_LABEL",
            "BUTTON_SCREEN_READER_LABEL",
            "SESSION_TIMEOUT_POPUP.POPUP_HEADING_SCREEN_READER_LABEL"
        ]).then(function(translations) {
            $scope.sessionPopupHeaderLabel = translations["SESSION_TIMEOUT_POPUP.POPUP_HEADING"];
            $scope.sessionDescText1 = translations["SESSION_TIMEOUT_POPUP.POPUP_MESSAGE1"];
            $scope.sessionDescText2 = translations["SESSION_TIMEOUT_POPUP.POPUP_MESSAGE2"];
            $scope.sessionPopupBtnText = translations["SESSION_TIMEOUT_POPUP.POPUP_BUTTON_LABEL"];
            $scope.sessionPopupScrButtonText = translations["BUTTON_SCREEN_READER_LABEL"];
            $scope.sessionPopupScrHeadingText = translations["SESSION_TIMEOUT_POPUP.POPUP_HEADING_SCREEN_READER_LABEL"];
        });
    }
]);
/**
 * This service opens sesssion popup while session is invalid.
 * It passes errorData to sessionPopupCtrl
 */
angular.module("tpp").service("SessionPopupService", ["$uibModal", "$window", "$translate", function($uibModal, $window, $translate) {
    var openSessionPopup = function(errorData) {
        $uibModal.open({
            templateUrl: "app/views/session-out-popup.html",
            animation: true,
            backdrop: false,
            keyboard: false,
            resolve: {
                errorData: function() {
                    return errorData;
                }
            },
            controller: "sessionPopupCtrl"
        });
    };

    return {
        openSessionPopup: openSessionPopup
    };
}]);

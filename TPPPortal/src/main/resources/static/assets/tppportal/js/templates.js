(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/add-application-review.html',
    '<page-header skipToContent="skipToContent" focusContent="focusMainContent()"></page-header>\n' +
    '<section class="container main-content-section">\n' +
    '    <!--page title-->\n' +
    '    <div class="row heading-section">\n' +
    '        <div class="col-sm-10 col-sm-offset-1">\n' +
    '            <h2>\n' +
    '                <span ng-bind="selectedTppOrgName"></span>\n' +
    '                <span class="line-dash" aria-label="{{dashScrLabel}}"></span>\n' +
    '                <span ng-bind="addAppHeader"></span>\n' +
    '            </h2>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--page title ends-->\n' +
    '\n' +
    '\n' +
    '    <div class="row">\n' +
    '        <div class="col-sm-10 col-sm-offset-1">\n' +
    '\n' +
    '            <!--card container-->\n' +
    '            <div class="card-container add-application">\n' +
    '                <!--alert-->\n' +
    '                <div error-notice ng-if="noticeData" notice-data="noticeData" class="add-app-error" role="alert"></div>\n' +
    '                <!--alert ends-->\n' +
    '                <!--file component-->\n' +
    '                <div class="row">\n' +
    '                    <div class="col-sm-10">\n' +
    '                        <p class="input-label" ng-bind="addApplicationReviewHeader"></p>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <!--application details table -->\n' +
    '\n' +
    '                <table class="table application-details-table" ng-if="applicationData" id="addApplicationDetails">\n' +
    '                    <caption class="sr-only" ng-bind="addAppDetailsCaptionLabel"></caption>\n' +
    '\n' +
    '                    <tbody>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="applicationNameLabel"></th>\n' +
    '                            <td ng-bind="applicationData.applicationName"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="applicationDescriptionLabel"></th>\n' +
    '                            <td ng-bind="applicationData.applicationDescription"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="clientIdLabel"></th>\n' +
    '                            <td ng-bind="applicationData.clientId"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="redirectUriLabel"></th>\n' +
    '                            <td>\n' +
    '                                <p ng-repeat="uri in applicationData.redirectUris track by $index" ng-bind="uri"></p>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="termsOfServiceUrlLabel"></th>\n' +
    '                            <td ng-bind="applicationData.termOfServiceURI"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="policyUriLabel"></th>\n' +
    '                            <td ng-bind="applicationData.policyURI"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="jwksEndpointLabel"></th>\n' +
    '                            <td ng-bind="applicationData.JWKSEndPoint"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '                <!--application details table ends-->\n' +
    '                <!--button container -->\n' +
    '                <div class="button-container clearfix">\n' +
    '                    <button class="btn btn-submit pull-right" id="btn_addApp" type="submit" ng-disabled="!applicationData" ng-click="submitApplicationData()" ng-bind="addApplicationButtonLabel"> </button>\n' +
    '                    <button type="button" class="btn btn-cancel" ui-sref="myApplication" ng-bind="cancelButtonLabel"></button>\n' +
    '                    <button type="button" class="btn btn-cancel" ui-sref="addApplication" ng-bind="chooseDiffFileButtonLabel"></button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '<page-footer></page-footer>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/add-application.html',
    '<page-header skipToContent="skipToContent" focusContent="focusMainContent()" notice-data="noticeData"></page-header>\n' +
    '<section class="container main-content-section">\n' +
    '    <!--page title-->\n' +
    '    <div class="row heading-section">\n' +
    '        <div class="col-sm-10 col-sm-offset-1">\n' +
    '            <h2>\n' +
    '                <span ng-bind="selectedTppOrgName"></span>\n' +
    '                <span class="line-dash" aria-label="{{dashScrLabel}}"></span>\n' +
    '                <span ng-bind="addAppHeader"></span>\n' +
    '            </h2>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--page title ends-->\n' +
    '\n' +
    '\n' +
    '    <div class="row">\n' +
    '        <div class="col-sm-10 col-sm-offset-1">\n' +
    '\n' +
    '            <!--card container-->\n' +
    '            <div class="card-container">\n' +
    '                <!--alert-->\n' +
    '                <div error-notice ng-if="noticeData" notice-data="noticeData" class="add-app-error" aria-live="assertive"></div>\n' +
    '                <!--alert ends-->\n' +
    '                <!--file component-->\n' +
    '                <div class="row">\n' +
    '                    <div class="col-sm-10 col-sm-offset-1">\n' +
    '                        <label for="uploadSoftwareStatement" class="input-label"><span class="required-sign">* </span>Upload Software Statement</label>\n' +
    '                        <span class="help-holder">\n' +
    '                            <a href="javascript:void(0)" aria-label="{{uploadTooltipScrLabel}}"\n' +
    '                            popover-trigger="\'focus mouseenter\'"\n' +
    '                                popover-placement="{{placement.selected}}" popover-class="tooltip-content"\n' +
    '                                uib-popover="{{appTooltipMsg}}">\n' +
    '                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '                            </a>\n' +
    '                        </span>\n' +
    '                        <form method="POST" action="#" enctype="multipart/form-data">\n' +
    '\n' +
    '                            <div class="form-group file-input-component">\n' +
    '                                <div class="input-group input-file" name="">\n' +
    '                                    <input type="text" id="uploadSoftwareStatement" onfocus=this.blur(); class="form-control" readonly="true" ng-click="selectFile()" placeholder="Upload Software Statement" ng-model="selectedFile.name" ng-class="{\'error-border\': invalidFile == true }" tabindex="-1">\n' +
    '                                    <label for="ssa" class="hidden" ng-bind="fileInputLabel"></label>\n' +
    '                                    <input type="file" class="hidden" name="ssa" id="ssa" value="" onchange="angular.element(this).scope().filesSelected(this)">\n' +
    '                                    <span class="input-group-btn">\n' +
    '                                        <button class="btn btn-cancel btn-submit" id="btn_browse" type="button" ng-click="selectFile()" ng-bind="browseButtonLabel">\n' +
    '                                        </button>\n' +
    '                                    </span>\n' +
    '\n' +
    '                                </div>\n' +
    '                                <p class="alert-inline filetypeAlert" id="test" ng-if="invalidFile" aria-live="assertive" aria-atomic="true" ng-bind="inlineErrMsg"></p>\n' +
    '                            </div>\n' +
    '                        </form>\n' +
    '\n' +
    '                        <p class="instruction-text" ng-bind="uploadInstLabel"></p>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <!--file component ends-->\n' +
    '\n' +
    '                <!--button container -->\n' +
    '                <div class="button-container clearfix">\n' +
    '                    <button class="btn btn-submit pull-right" type="button" id="btn_upload" ng-disabled="invalidFile || !formData || applicationData" ng-click="uploadFile()" aria-controls="addApplicationDetails" ng-bind="uploadButtonLabel"></button>\n' +
    '                    <button type="button" class="btn btn-cancel" ui-sref="myApplication" ng-bind="cancelButtonLabel"></button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '<page-footer notice-data="noticeData"></page-footer>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/application-details.html',
    '<tbody>\n' +
    '    <tr class="table-row">\n' +
    '        <td ng-bind="applicationData.cn"></td>\n' +
    '        <td ng-bind="applicationData.softwareVersion"></td>\n' +
    '        <td>\n' +
    '            <a ng-repeat="roles in applicationData.softwareRoles track by $index">\n' +
    '                <span class="badge" ng-bind="roles"></span>\n' +
    '            </a>\n' +
    '        </td>\n' +
    '        <td ng-bind="applicationData.status"></td>\n' +
    '        <td>\n' +
    '            <p ng-bind="labels.protectedClientSecret"></p>\n' +
    '            <button type="button" class="btn-show" id="{{\'show\'+applicationData.clientId}}" aria-label="{{applicationData.cn+\' \'+labels.showButton}}"\n' +
    '                ng-click="showSecretData()" ng-disabled="tppBlocked">\n' +
    '                <span ng-bind="labels.showSecretButtonLabel"></span>\n' +
    '            </button>\n' +
    '            <button type="button" id="{{\'reset\'+applicationData.clientId}}" class="btn-reset" ng-click="resetSecretData()" ng-disabled="tppBlocked"\n' +
    '                aria-label="{{applicationData.cn+\' \'+labels.resetButton}}">\n' +
    '                <span ng-bind="labels.resetSecretButtonLabel"></span>\n' +
    '            </button>\n' +
    '        </td>\n' +
    '        <td>\n' +
    '            <button type="button" id="{{\'delete\'+applicationData.clientId}}" class="fa fa-trash-o" ng-click="!tppBlocked?deleteApplicaiton():true"\n' +
    '                ng-disabled="tppBlocked" aria-label="{{applicationData.cn+\' \'+labels.deleteIcon}}" title="{{labels.deleteIcon}}">\n' +
    '            </button>\n' +
    '        </td>\n' +
    '    </tr>\n' +
    '\n' +
    '    <tr class="collapse-button-section">\n' +
    '        <td colspan="6">\n' +
    '            <button id="btn_shw" ng-show="!applicationData.isExpanded" type="button" role="button" ng-click="expandCollapseRow()" aria-expanded="false"\n' +
    '                data-toggle="collapse" aria-controls="{{applicationData.clientId}}" data-target="{{\'#\' + applicationData.clientId}}">\n' +
    '                <span class="btn-span">\n' +
    '                    <i class="fa fa-plus"></i>\n' +
    '                    <span class="sr-only" aria-label="{{applicationData.cn}}"></span>\n' +
    '                    <span ng-bind="labels.showMoreLabel"></span>\n' +
    '                </span>\n' +
    '            </button>\n' +
    '            <button id="{{\'btn-expand\'+applicationData.clientId}}" class="sr-only" aria-label="expanded" aria-hidden="true" tabindex="-1">\n' +
    '            </button>\n' +
    '\n' +
    '        </td>\n' +
    '    </tr>\n' +
    '\n' +
    '    <tr class="collapse-section">\n' +
    '        <td colspan="6">\n' +
    '            <div uib-collapse="!applicationData.isExpanded" id="{{applicationData.clientId}}" aria-hidden="{{!applicationData.isExpanded}}">\n' +
    '                <table class="table">\n' +
    '                    <caption class="sr-only" ng-bind="labels.appDetailsCaptionLabel"></caption>\n' +
    '                    <tbody>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="labels.applicationDescriptionLabel"></th>\n' +
    '                            <td ng-bind="applicationData.softwareClientDescription"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th>\n' +
    '                                <span ng-bind="labels.clientIdLabel"></span>\n' +
    '                                <span class="help-holder">\n' +
    '                                    <a href="javascript:void(0)" aria-label="{{labels.clientIDTooltipScrLabel}}" popover-trigger="\'focus mouseenter\'" popover-placement="{{placement.selected}}"\n' +
    '                                        popover-class="tooltip-content" uib-popover="{{labels.clientIDTooltipLabel}}" popover-leave>\n' +
    '                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '                                    </a>\n' +
    '                                </span>\n' +
    '                            </th>\n' +
    '                            <td ng-bind="applicationData.clientId"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="labels.redirectURILabel"></th>\n' +
    '                            <td>\n' +
    '                                <p ng-repeat="redirectUrl in applicationData.redirectUrl track by $index" ng-bind="redirectUrl"></p>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="labels.termsServicesLabel"></th>\n' +
    '                            <td ng-bind="applicationData.termOfServiceURI"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="labels.policyURILabel"></th>\n' +
    '                            <td ng-bind="applicationData.policyURI"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-bind="labels.JWKSEndpointLabel"></th>\n' +
    '                            <td ng-bind="applicationData.jwksUrl"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <td colspan="6">\n' +
    '                                <button type="button" class="btn-span" ng-click="expandCollapseRow()" aria-expanded="true" data-toggle="collapse" aria-controls="{{applicationData.clientId}}"\n' +
    '                                    data-target="{{\'#\' + applicationData.clientId}}">\n' +
    '                                    <span ng-show="applicationData.isExpanded">\n' +
    '                                        <i class="fa fa-minus"></i>\n' +
    '                                        <span class="sr-only" aria-label="{{applicationData.cn}}"></span>\n' +
    '                                        <span ng-bind="labels.showLessLabel"></span>\n' +
    '                                    </span>\n' +
    '                                </button>\n' +
    '                                <button id="{{\'btn-collapse\'+applicationData.clientId}}" class="sr-only" aria-label="collapsed" aria-hidden="true" tabindex="-1">\n' +
    '                                </button>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </td>\n' +
    '    </tr>\n' +
    '\n' +
    '\n' +
    '</tbody>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/customUibAccordian.html',
    '<uib-accordion close-others="oneAtATime">\n' +
    '    <div uib-accordion-group class="panel-default" ng-repeat="tpp in tppOrgList" is-disabled="status.isFirstDisabled" is-open="tpp.isOpen">\n' +
    '        <uib-accordion-heading>\n' +
    '             <label class="radio" for="{{tpp.tppOrganizationId}}" ng-click="checked($event, this)" >\n' +
    '               <input type="radio" aria-expanded="{{tpp.isOpen}}" name="tpp" id="{{tpp.tppOrganizationId}}" ng-checked="tpp.isOpen" aria-label="{{tpp.organizationName}}" ng-model="status.selectedOrg"  ng-value="{{tpp}}" ng-keypress="checked($event, this)"   >\n' +
    '              <span aria-hidden="true" ng-bind="tpp.organizationName"></span>\n' +
    '            </label>\n' +
    '        </uib-accordion-heading>\n' +
    '        <ul class="organisation-details-list">\n' +
    '            <li>\n' +
    '                <span ng-bind="labels.tppSelectionOrgIdText"></span>\n' +
    '                <span ng-bind="tpp.tppOrganizationId"></span>\n' +
    '            </li>\n' +
    '            <li>\n' +
    '                <span ng-bind="labels.tppSelectionCompetentAuthority"></span>\n' +
    '                <span ng-bind="tpp.tppMemberStateCompetentAuthority"></span>\n' +
    '            </li>\n' +
    '            <li>\n' +
    '                <span ng-bind="labels.tppSelectionOrgRoleText"> </span>\n' +
    '                <span ng-repeat="roles in tpp.tppRoles" ng-bind="(roles) +($last ? \'\' : \',&nbsp;\')"></span>\n' +
    '            </li>\n' +
    '            <li><span ng-bind="labels.tppSelectionEmailId"></span>\n' +
    '                <span ng-if="tpp.tppEmailID != null" ng-bind="tpp.tppEmailID"></span>\n' +
    '                <span ng-if="tpp.tppEmailID == null" ng-bind="labels.emailId"></span>\n' +
    '            </li>\n' +
    '            <li>\n' +
    '                <span ng-bind="labels.tppSelectionPhoneNoText"> </span>\n' +
    '                <span ng-if="tpp.tppPhoneNumber!= null" ng-bind="tpp.tppPhoneNumber"></span>\n' +
    '                <span ng-if="tpp.tppPhoneNumber == null" ng-bind="labels.phoneNo"></span>\n' +
    '            </li>\n' +
    '            <li><span ng-bind="labels.tppSelectionRegId"> </span>\n' +
    '                <span ng-bind="tpp.tppRegistrationID"></span>\n' +
    '            </li>\n' +
    '        </ul>\n' +
    '\n' +
    '    </div>\n' +
    '</uib-accordion>');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/delete-application-popup.html',
    '<div id="deleteApplicationPopup" class="popup-container" aria-labelledby="deleteApplicationPopupHeaderLabel">\n' +
    '    <div role="document">\n' +
    '        <div class="modal-header">\n' +
    '            <span class="sr-only" ng-bind="deleteApplicationScrPopupHeading"></span>\n' +
    '            <span class="modal-title" ng-bind="deleteApplicationPopupHeaderLabel" id="deleteApplicationPopupHeaderLabel"></span>\n' +
    '            <button type="button" role="button" class="btn btn-close pull-right fa fa-close" aria-label="{{deleteApplicationScrCloseBtnText}}" ng-click="noBtnClicked()" title="{{deleteApplicationScrCloseBtnText}}">\n' +
    '                <span class="sr-only" ng-bind="deleteApplicationScrCloseBtnText"></span>\n' +
    '            </button>\n' +
    '        </div>\n' +
    '        <div class="modal-body">\n' +
    '            <p class="text-center desc" ng-bind="deleteApplicationDescText"> </p>\n' +
    '            <p class="text-center desc" ng-bind="deleteApplicationDescText2"> </p>\n' +
    '            <hr class="horizontal-line">\n' +
    '            <div class="checkbox">\n' +
    '                <label for="accpt_cond" ng-bind="deleteApplicationCheckboxLabel"></label>\n' +
    '                <input role="checkbox" type="checkbox" id="accpt_cond" ng-model="popupCheckBox.acceptValue" autofocus>\n' +
    '                <span></span>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '        <div class="modal-footer no-top-padding">\n' +
    '            <button role="button" type="button" class="btn btn-submit" ng-bind="deleteApplicationBtnText2" ng-click="yesBtnClicked()"\n' +
    '                ng-disabled="!popupCheckBox.acceptValue"></button>\n' +
    '            <button role="button" class="btn btn-cancel pull-left" ng-bind="deleteApplicationBtnText1" ng-click="noBtnClicked()"></button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/error-notice.html',
    '<div ng-class="alertCssClass" class="alert page-alert" id="noTppList" tabindex="-1" ng-if="!noticeDismissed">\n' +
    '    <p class="message" ng-class="isClosable">\n' +
    '        <span class="alert-prefix" ng-bind="noticeData.noticePrefix"></span>\n' +
    '        <span class="text-link" ng-bind-html="noticeData.noticeMessage"></span>\n' +
    '    </p>\n' +
    '    <button ng-if="isCrossBtnEnable" type="button" class="close fa fa-close" id="btn-close" ng-click="dismissNotice(noticeData.noticeType)"\n' +
    '        title="{{noticeData.successCloseScreenReaderLabel}}">\n' +
    '        <span class="sr-only" ng-bind="noticeData.successCloseScreenReaderLabel"></span>\n' +
    '    </button>\n' +
    '    <p class="text-link" ng-if="isShowNoticeDescription" ng-bind-html="noticeData.noticeDescription"> </p>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/generic-error.html',
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/loading-spinner.html',
    '<div class="block-ui-overlay"></div>\n' +
    '<div class="block-ui-message-container" aria-live="assertive" >\n' +
    '    <div class="block-ui-message" ng-class="$_blockUiMessageClass">\n' +
    '        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>\n' +
    '        <span aria-hidden="true" >Loading, may take some time.\n' +
    '            </span>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/login.html',
    '<page-header skipToContent="skipToContent" page-error="pageError" focusContent="focusMainContent()" login-header-menu="true"></page-header>\n' +
    '\n' +
    '<section class="container main-content-section">\n' +
    '\n' +
    '    <div class="alert-content-section">\n' +
    '\n' +
    '        <div error-notice ng-if="maintenanceAlertData" notice-data="maintenanceAlertData" role="alert"></div>\n' +
    '        <div error-notice ng-if="maintenanceNoticeData" notice-data="maintenanceNoticeData" role="alert"></div>\n' +
    '        <div error-notice ng-if="noticeData" notice-data="noticeData" role="alert"></div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col-sm-6">\n' +
    '            <h2 ng-bind="tppLoginHeading"></h2>\n' +
    '            <p ng-bind="tppPortalIntroPara1">\n' +
    '            </p>\n' +
    '            <p ng-bind="tppPortalIntroPara2">\n' +
    '            </p>\n' +
    '            <p class="text-link" ng-bind-html="tppPortalIntroPara3">\n' +
    '            </p>\n' +
    '            <p class="text-link" ng-bind-html="tppPortalIntroPara4">\n' +
    '\n' +
    '            </p>\n' +
    '        </div>\n' +
    '        <div class="col-sm-6">\n' +
    '            <h2 ng-bind="LoginFormHeading"></h2>\n' +
    '            <div class="card-container">\n' +
    '                <p ng-bind="LoginInstructionLabel"></p>\n' +
    '                <div>\n' +
    '                    <a id="loginButton" href="./redirect" class="btn btn-submit btn-block" ng-bind="LoginButtonLabel">\n' +
    '                    </a>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '        <!--button container ends-->\n' +
    '    </div>\n' +
    '    <!--card container ends-->\n' +
    '</section>\n' +
    '\n' +
    '\n' +
    '\n' +
    '<page-footer></page-footer>\n' +
    '<!--cookies disclaimer -->\n' +
    '<div class="cookies-disclaimer" ng-if="cookieNoticeObject" role="alert">\n' +
    '        <p class="alert-closeable text-link cookie-link" ng-bind-html="cookieNotice"></p>\n' +
    '        <button type="button" class="close" ng-click="focusAfterNoticeDismissed(\'warning\',true)">\n' +
    '            <span class="sr-only" ng-bind="successCloseScreenReaderLabel"></span>\n' +
    '            <i class="fa fa-close" title={{successCloseScreenReaderLabel}}></i>\n' +
    '        </button>\n' +
    '</div>\n' +
    '    <!--cookies disclaimer ends-->\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/my-application.html',
    '<page-header skipToContent="skipToContent" focusContent="focusMainContent()" hide-skip-to-cont-link="hideskipToContLink"\n' +
    '    notice-data="noticeData"></page-header>\n' +
    '<section class="container main-content-section">\n' +
    '    <div error-notice ng-if="tppBlocked" notice-data="alertData" role="alert"></div>\n' +
    '    <div class="row heading-section">\n' +
    '        <!--page title-->\n' +
    '\n' +
    '        <div class="col-sm-10">\n' +
    '            <h2 class="header-myapp">\n' +
    '                <span ng-bind="selectedTppOrgName"></span>\n' +
    '                <span class="line-dash" aria-label="{{dashScrLabel}}"></span>\n' +
    '                <span ng-bind="myApplicationHeading"></span>\n' +
    '            </h2>\n' +
    '        </div>\n' +
    '        <!--page title ends-->\n' +
    '        <div class="col-md-2">\n' +
    '            <button type="button" class="btn btn-default" ng-click="addApplicationBtnClicked()" ng-disabled="tppBlocked" id="btn-add-application">\n' +
    '                <span class="fa fa-plus" aria-hidden="true"></span>\n' +
    '                <span ng-bind="myApplicationButtonLabel"></span>\n' +
    '            </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="card-container">\n' +
    '        <div error-notice ng-if="noticeData" notice-data="noticeData" ng-attr-role="{{successData?\'\':\'alert\'}}"></div>\n' +
    '        <div error-notice ng-if="warningData" notice-data="warningData" ng-attr-role="{{successData?\'\':\'alert\'}}"></div>\n' +
    '        <div error-notice ng-if="successData" notice-data="successData" role="alert" focusAfterDismissed="focusAfterNoticeDismissed(noticeType, isCrossBtnEnable)"></div>\n' +
    '        <div class="table-section" ng-if="applicationsList.tppAppList.length > 0">\n' +
    '            <table>\n' +
    '                <caption class="sr-only" ng-bind="applicationDetailsLabels.appListCaptionLabel"></caption>\n' +
    '                <thead class="table-header-section">\n' +
    '                    <tr class="table-header-row">\n' +
    '                        <th ng-bind="applicationNameLabel"></th>\n' +
    '                        <th ng-bind="applicationVersionLabel"></th>\n' +
    '                        <th>\n' +
    '                            <span ng-bind="roleLabel"></span>\n' +
    '                            <span class="help-holder">\n' +
    '                                <a href="javascript:void(0)" id="roleTooltipID" aria-label="{{applicationDetailsLabels.roleTooltipScrLabel}}" popover-trigger="\'focus mouseenter\'"\n' +
    '                                    popover-placement="{{placement.selected}}" popover-class="tooltip-content" uib-popover="{{roleTooltipLabel}}"\n' +
    '                                    popover-leave>\n' +
    '                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '                                </a>\n' +
    '                            </span>\n' +
    '                        </th>\n' +
    '                        <th ng-bind="statusLabel"></th>\n' +
    '                        <th>\n' +
    '                            <span ng-bind="applicationDetailsLabels.clientSecretLabel"></span>\n' +
    '                            <span class="help-holder">\n' +
    '                                <a href="javascript:void(0)" aria-label="{{applicationDetailsLabels.clientSecretTooltipScrLabel}}" popover-trigger="\'focus mouseenter\'"\n' +
    '                                    popover-placement="{{placement.selected}}" popover-class="tooltip-content" uib-popover="{{applicationDetailsLabels.clientSecretTooltipLabel}}"\n' +
    '                                    popover-leave>\n' +
    '                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '                                </a>\n' +
    '                            </span>\n' +
    '                        </th>\n' +
    '                        <th ng-bind="deleteLabel"></th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody application-details application-data="applicationData" ng-repeat="applicationData in applicationsList.tppAppList"\n' +
    '                    toggle-application-details="toggleApplicationDetails()" show-secret="showSecret(tppID, clientID, applicationName, applicationID)"\n' +
    '                    reset-secret="resetSecretPopupOpen(currentApp)" delete-application="deleteApplicationPopupOpen(currentApp)"\n' +
    '                    tpp-org-id="tppOrgID" tpp-blocked="tppBlocked" remove-error-data="removeErrorData()" labels="applicationDetailsLabels"></tbody>\n' +
    '\n' +
    '\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '<page-footer notice-data="noticeData"></page-footer>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/page-footer.html',
    '<footer>\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="col-xs-12">\n' +
    '                <ul class="footer-links" ng-if="!loginFooterMenu">\n' +
    '                    <li>\n' +
    '                        <a href="#" ng-click="sessionCheck(privacyPolicyURL, true, $event)">\n' +
    '                            <span ng-bind="privacyPolicy" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li aria-hidden="true">\n' +
    '                        <span class="pipe">|</span>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="#" ng-click="sessionCheck(helpURL, true, $event)">\n' +
    '                            <span ng-bind="help" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '                <ul class="footer-links" ng-if="loginFooterMenu">\n' +
    '                    <li>\n' +
    '                        <a href="{{privacyPolicyURL}}" target="_blank">\n' +
    '                            <span ng-bind="privacyPolicy" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li aria-hidden="true">\n' +
    '                        <span class="pipe">|</span>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="{{helpURL}}" target="_blank">\n' +
    '                            <span ng-bind="help" title="{{tooltipTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{tooltipTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <p class="regulatory-statement" ng-bind="regulatoryLabel">\n' +
    '        </p>\n' +
    '    </div>\n' +
    '</footer>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/page-header.html',
    '<header>\n' +
    '    <div class="header-container">\n' +
    '        <div class="container ">\n' +
    '            <div class="brand-details">\n' +
    '                <a class="skip-main" href="#" ng-click="skipToContentFocus($event)" ng-if="!hide" ng-bind="skiptoContent"></a>\n' +
    '                <img class="navbar-brand" ng-src={{logoPath}} alt={{logoAlternateLabel}} title={{logoAlternateLabel}} />\n' +
    '                <h1 class="portal-details" ng-bind="tppHeader"></h1>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!--Navigation-->\n' +
    '        <nav class="navbar">\n' +
    '            <div class="container">\n' +
    '                <ul class="nav navbar-nav nav-menu" ng-if="!loginHeaderMenu">\n' +
    '                    <li ng-class="{active: activePath==\'/select-organisation\'}">\n' +
    '                        <a href="#" ng-click="sessionCheck(\'selectOrganisation\',false, $event)" ng-class="{active: activePath==\'/select-organisation\'}">\n' +
    '                            <span ng-bind="myOrganisation" ng-attr-title="{{activePath==\'/select-organisation\'?currentPage:sameWindowTt}}"></span>\n' +
    '                            <span class="sr-only" ng-attr-aria-label="{{activePath==\'/select-organisation\'?currentPage:sameWindowTt}}"></span>\n' +
    '                        </a>\n' +
    '\n' +
    '                    </li>\n' +
    '                    <li ng-class="{active: activePath==\'/view-application\' || activePath==\'/add-application\' || activePath==\'/add-application-review\',disabled:myAppDisableNav}">\n' +
    '\n' +
    '                        <a href="#" ng-click="sessionCheck(\'myApplication\', false, $event)" ng-class="{active: activePath==\'/view-application\' || activePath==\'/add-application\' || activePath==\'/add-application-review\',disabled:myAppDisableNav}">\n' +
    '                            <span ng-bind="myApplication" ng-attr-title="{{activePath==\'/view-application\' || activePath==\'/add-application\' || activePath==\'/add-application-review\'?currentPage:sameWindowTt}}"></span>\n' +
    '                            <span ng-if="!myAppDisableNav" class="sr-only" ng-attr-aria-label="{{activePath==\'/view-application\' || activePath==\'/add-application\' || activePath==\'/add-application-review\'?currentPage:sameWindowTt}}"></span>\n' +
    '                            <span ng-if="myAppDisableNav" class="sr-only" aria-label="{{unAvailableTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '\n' +
    '                    <li>\n' +
    '                        <a href="#" ng-click="sessionCheck(navHelpURL, true, $event)">\n' +
    '                            <span ng-bind="helpNav" title="{{helpTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{helpTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '\n' +
    '                    <li class="username">\n' +
    '                        <i class="fa fa-user-circle-o"></i>\n' +
    '                        <span class="username-text">\n' +
    '                                <span ng-bind="welcome"></span>\n' +
    '                        <span class="text-capitalize" ng-bind="username"></span>\n' +
    '                        </label>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="#" ng-click="sessionCheck(\'logout\', false, $event)">\n' +
    '                            <span ng-bind="logout" title="{{sameWindowTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{sameWindowTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '                <ul class="nav navbar-nav nav-menu" ng-if="loginHeaderMenu">\n' +
    '                    <li>\n' +
    '                        <a href="{{navHelpURL}}" target="_blank">\n' +
    '                            <span ng-bind="helpNav" title="{{helpTt}}"></span>\n' +
    '                            <span class="sr-only" aria-label="{{helpTt}}"></span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </nav>\n' +
    '        <!--Navigation Ends-->\n' +
    '    </div>\n' +
    '</header>');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/reset-secret-popup.html',
    '<div id="resetSecretPopup" class="popup-container" aria-labelledby="resetSecretPopupHeaderLabel">\n' +
    '    <div role="document">\n' +
    '        <div class="modal-header">\n' +
    '            <span class="sr-only" ng-bind="resetSecretScrPopupHeading"></span>\n' +
    '            <span class="modal-title" ng-bind="resetSecretPopupHeaderLabel" id="resetSecretPopupHeaderLabel"></span>\n' +
    '            <button type="button" role="button" class="btn btn-close pull-right fa fa-close" aria-label="{{resetSecretScrCloseBtnText}}" ng-click="noBtnClicked()" title="{{resetSecretScrCloseBtnText}}">\n' +
    '                <span class="sr-only" ng-bind="resetSecretScrCloseBtnText"></span>\n' +
    '            </button>\n' +
    '        </div>\n' +
    '        <div class="modal-body">\n' +
    '            <p class="text-center desc" ng-bind="resetSecretDescText"></p>\n' +
    '            <hr class="horizontal-line">\n' +
    '            <div class="checkbox">\n' +
    '                <label for="accpt_cond" ng-bind="resetSecretCheckboxLabel"></label>\n' +
    '                <input role="checkbox" type="checkbox" id="accpt_cond" ng-model="popupCheckBox.acceptValue" autofocus>\n' +
    '                <span></span>\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '        <div class="modal-footer no-top-padding">\n' +
    '            <button role="button" type="button" class="btn btn-submit" ng-bind="resetSecretBtnText2" ng-click="yesBtnClicked()" ng-disabled="!popupCheckBox.acceptValue"></button>\n' +
    '            <button role="button" class="btn btn-cancel pull-left" ng-bind="resetSecretBtnText1" ng-click="noBtnClicked()"></button>\n' +
    '            </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/select-organisation.html',
    '<page-header my-app-disable-nav="disableMyAppNav" skipToContent="skipToContent" hide-skip-to-cont-link="hideskipToContLink" focusContent="focusMainContent()" page-error="pageError" notice-data="noticeData"></page-header>\n' +
    '\n' +
    '<section class="container main-content-section" ng-show="noticeData || warningData || showOrgList">\n' +
    '    <!--page title-->\n' +
    '    <div class="row">\n' +
    '        <div class="col-sm-10 col-sm-offset-1">\n' +
    '            <h2 ng-bind="tppSelectionPageHeaderLabel"></h2>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!--page title ends-->\n' +
    '\n' +
    '    <div class="row">\n' +
    '        <div class="col-sm-10 col-sm-offset-1">\n' +
    '            <!--card container-->\n' +
    '            <div error-notice ng-if="warningData" notice-data="warningData" role="alert"></div>\n' +
    '            <div class="card-container" ng-if="!warningData">\n' +
    '                <div error-notice ng-if="noticeData" notice-data="noticeData" role="alert"></div>\n' +
    '                <p ng-bind="tppSelectOrgMessageText" ng-if="!noticeData"></p>\n' +
    '                <div ng-if="!noticeData && tppList.length">\n' +
    '                    <custom-uib-accordian tpp-org-list="tppList" status="status" labels="tppDetailsLabels"></custom-uib-accordian>\n' +
    '                </div>\n' +
    '                <!--button container -->\n' +
    '                <div class="button-container clearfix" ng-if="!noticeData">\n' +
    '                    <button class="btn btn-submit pull-right" ng-disabled="status.disablebtn" type="submit" ng-click="continueButtonClicked()" ng-bind="continueButtonLabel"></button>\n' +
    '                </div>\n' +
    '                <!--button container ends-->\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '    </div>\n' +
    '    <!--card container ends-->\n' +
    '    </div>\n' +
    '</section>\n' +
    '<page-footer notice-data="noticeData"></page-footer>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/session-out-popup.html',
    '<div id="SessionOutPopup" class="popup-container" aria-labelledby="SessionOutPopupLabel">\n' +
    '    <div role="document">\n' +
    '        <div class="modal-header">\n' +
    '            <span class="sr-only" ng-bind="sessionPopupScrHeadingText"></span>\n' +
    '            <span class="modal-title" ng-bind="sessionPopupHeaderLabel" id="SessionOutPopupLabel"></span>\n' +
    '        </div>\n' +
    '        <div class="modal-body">\n' +
    '            <p class="text-center" ng-bind="sessionDescText1"> </p>\n' +
    '            <p class="text-center" ng-bind="sessionDescText2"> </p>\n' +
    '        </div>\n' +
    '        <hr class="horizontal-line hr-margin">\n' +
    '        <div class="modal-footer">\n' +
    '            <div>\n' +
    '                <button role="button" class="btn btn-submit btn-block" id="btn-ok" ng-click="okBtnClicked()" ng-bind="sessionPopupBtnText"></button>\n' +
    '                <span class="sr-only" ng-bind="sessionPopupScrButtonText"></span>\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/show-secret-popup.html',
    '<div id="showSecretPopup" aria-labelledby="showSecretPopupHeaderLabel">\n' +
    '    <div role="document">\n' +
    '        <div class="modal-header">\n' +
    '            <span class="sr-only" ng-bind="showSecretScrPopupHeading"></span>\n' +
    '            <span class="modal-title" ng-bind="showSecretPopupHeaderLabel" id="showSecretPopupHeaderLabel"></span>\n' +
    '            <button type="button" role="button" class="btn btn-close pull-right fa fa-close" aria-label="{{showSecretScrCloseBtnText}}" ng-click="okBtnClicked()" title="{{showSecretScrCloseBtnText}}">\n' +
    '                    <span class="sr-only" ng-bind="showSecretScrCloseBtnText"></span>\n' +
    '                    <span class="sr-only" ng-bind="showSecretScrBtnText"></span>\n' +
    '                </button>\n' +
    '    \n' +
    '        </div>\n' +
    '        <div class="modal-body">\n' +
    '            <p class="text-center" ng-bind="showSecretDescText"></p>\n' +
    '        </div>\n' +
    '        <hr class="horizontal-line hr-margin">\n' +
    '        <div class="modal-footer">\n' +
    '            <button role="button" class="btn btn-cancel pull-left" ng-bind="showSecretBtnText1" ng-click="copyClientSecretClicked()"></button>\n' +
    '           <button role="button" type="button" class="btn btn-submit" ng-bind="showSecretBtnText2" ng-click="okBtnClicked()"></button>\n' +
    '          \n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/success-notice.html',
    '<div class="alert page-alert page-alert-success" role="alert" aria-atomic="true">\n' +
    '        <p role="alert">\n' +
    '            <span  class="alert-prefix" ng-bind="errorPrefix"></span>\n' +
    '            <span ng-bind="errorMsgText"></span>\n' +
    '        </p>\n' +
    '    </div>');
}]);
})();

(function(module) {
try {
  module = angular.module('tppPartials');
} catch (e) {
  module = angular.module('tppPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('app/views/view-profile.html',
    '<page-header skipToContent="skipToContent"></page-header>\n' +
    '<section class="container main-content-section">\n' +
    '    <div class="row heading-section">\n' +
    '        <div class="col-md-12">\n' +
    '            <h2>My Application</h2>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="content-section">\n' +
    '        <div class="alert page-alert page-alert-warning" role="alert" aria-atomic="true">\n' +
    '            <p>\n' +
    '                <span role="alert" class="alert-prefix">Notice:</span>\n' +
    '                <span role="alert" class="alert-suffix">This information is sourced from the Open Banking Directory. If incorrect please <a>contact</a> Open Banking to update.\n' +
    '                </span>\n' +
    '            </p>\n' +
    '        </div>\n' +
    '\n' +
    '        <div class="info-section row">\n' +
    '            <div class="col-md-6">\n' +
    '                <h3>TPP Information</h3>\n' +
    '                <div>\n' +
    '                    <span><strong>Legal Name</strong></span><br>\n' +
    '                    <span>Moneywise</span>\n' +
    '                </div>\n' +
    '                <div>\n' +
    '                    <span>TPP Registration ID</span><br>\n' +
    '                    <span>09999999999999</span>\n' +
    '                </div>\n' +
    '                <div>\n' +
    '                    <span>TPP Roles</span><br>\n' +
    '                    <span>AISP,PISP,CISP</span>\n' +
    '                </div>\n' +
    '                <div>\n' +
    '                    <span>TPP Competent Authority</span><br>\n' +
    '                    <span>MSCA</span>\n' +
    '                </div>\n' +
    '                <div>\n' +
    '                    <span>Domain Names(2)</span><br>\n' +
    '                    <span>moneywise.co.uk</span><br>\n' +
    '                    <span>aismoneywise.co.uk</span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '            <div class="col-md-6">\n' +
    '                <h3>Contact Information</h3>\n' +
    '                <div>\n' +
    '                    <span>Name</span><br>\n' +
    '                    <span>John Doe</span>\n' +
    '                </div>\n' +
    '                <div>\n' +
    '                    <span>Title </span><br>\n' +
    '                    <span>Mr.</span>\n' +
    '                </div>\n' +
    '                <div>\n' +
    '                    <span>Phone</span><br>\n' +
    '                    <span>+412343423424234</span>\n' +
    '                </div>\n' +
    '                <div>\n' +
    '                    <span>Email</span><br>\n' +
    '                    <span>john.doe@moneywise.co.in</span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '    </div>\n' +
    '\n' +
    '\n' +
    '</section>\n' +
    '<page-footer></page-footer>\n' +
    '');
}]);
})();

<!DOCTYPE html>
#*
The server renders this HTML page in an end-user's browser when
needed authentication credentials may be obtained via HTTP Basic
Authentication or an HTML form.

Velocity variables (identified by the $ character) are generated
at runtime by the server.

The following variables are available on this page, but not used by default:

$entityId       - The entity id (connection id) of the SP Connection used in this SSO transaction
$connectionName - The name of the SP Connection used in this SSO transaction
$client_id      - The id of the OAuth client used in this transaction
$spAdapterId    - The SP Adapter ID used in this transaction

It is recommended to sanitize the values that are displayed using $escape.escape() for example $escape.escape($client_id).

Change text or formatting as needed. Modifying Velocity statements
is not recommended as it may interfere with expected server behavior.
*#

<!-- template name: html.form.login.template.html -->

#set( $messageKeyPrefix = "html.form.login.template." )

<!-- Configurable default behavior for the Remember Username checkbox -->
#set ($enableCheckboxByDefault = false)
#if($rememberUsernameCookieExists)
    #set ($rememberUsernameChecked = "checked")
#else
    #if($enableCheckboxByDefault)
        <!-- allow the checkbox to be enabled by default -->
        #set ($rememberUsernameChecked = "checked")
    #else
        <!-- set the checkbox to unchecked -->
        #set ($rememberUsernameChecked = "")
    #end
#end

<html lang="$locale.getLanguage()" dir="ltr">
<head>
    <title>title</title>
    <base href="$PingFedBaseURL"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="x-ua-compatible" content="IE=edge" />
    <link rel="stylesheet" type="text/css" href="assets/css/main.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/cgportal.css"/>
	<link rel="shortcut icon" type="image/x-icon" href="/assets/images/devportal/favicon.ico?rev=16" />
</head>

<body onload="setFocus()" class="bg">
    <div class="fluid-container header-container">
		<header class="container">
			<a href="javascript:getHomePageUrl()" class="logo" >
				<img src="assets/images/devportal/logo2.png"/>
			</a>
        </header>
	</div>
	<div class="fluid-container main-container">
		<div class="container">
			<div class="row">
			   <div class="col-md-6">
			     <div class="welcm-header">
				 <h2>Welcome</h2>
				  </div>
				  
				  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				  </p>
				    
			   </div>	
			  <div class="col-md-6">
				<div class="signup">$templateMessages.getMessage($messageKeyPrefix, "notRegistered")
						<a href="javascript:getSignUpUrl()" class="signup-link">$templateMessages.getMessage($messageKeyPrefix, "signUp")
						</a>
					</div>
				<form method="POST" action="$url" autocomplete="off">
                <div class="devportal-messages">

                    #if($authnMessageKey)
                        <div class="devportal-error">$templateMessages.getMessage($authnMessageKey)</div>
                    #end
                    #if($errorMessageKey)
                        <div class="devportal-error">$templateMessages.getMessage($messageKeyPrefix, $errorMessageKey)</div>
                    #end

                    ## Uncomment below to display any additional server error:
                    #*
                    #if($serverError)
                        <div class="devportal-error">$serverError</div>
                    #end
                    *#
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1"><small class="wrn-color"> * </small> $templateMessages.getMessage($messageKeyPrefix, "usernameTitle")</label>
						#if($usernameEditable)
						<input id="username" type="text" size="36" name="$name" value="$username" placeholder="Example@boi.com" autocorrect="off" autocapitalize="off" onKeyPress="return postOnReturn(event)" class="form-control " /><!--#* Username value is explicitly HTML encoded in HtmlFormIdpAuthnAdapter *#-->
						#else
						<input id="username" type="text" size="36" name="$name" value="$username" disabled="disabled" autocorrect="off" autocapitalize="off" onKeyPress="return postOnReturn(event)" class="form-control" /><!--#* Username value is explicitly HTML encoded in HtmlFormIdpAuthnAdapter *#-->
						#end
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1"><small class="wrn-color"> * </small> $templateMessages.getMessage($messageKeyPrefix, "passwordTitle")</label>
						<input id="password" type="password" placeholder="Password" size="36" name="$pass" onKeyPress="return postOnReturn(event)"  />
					</div>
					
				<!-- Forgot Password Link-->
					<!--<div class="devportal-input-link devportal-pass-change">
					   
						#if($supportsPasswordReset)
							<a href="javascript:getForgotPasswordUrl()" class="forgot-password">$templateMessages.getMessage($messageKeyPrefix, "forgotPassword")</a>
						#end 
					</div>-->
					<div class="forgot-password">
					<a href="javascript:getForgotPasswordUrl()" class="wrn-color">$templateMessages.getMessage($messageKeyPrefix, "forgotPassword")</a>
					</div>	

                #if($enableRememberUsername)
                    <label class="remember-username">
                        <div class="devportal-checkbox-container stacked">
                            <input id="rememberUsername" type="checkbox" name="$rememberUsername" $rememberUsernameChecked />
                            $templateMessages.getMessage($messageKeyPrefix, "rememberUsernameTitle")
                            <div class="icon"></div>
                        </div>
                    </label>
                #end

                <div class="devportal-buttons button-section">
                    <input type="hidden" name="$ok" value=""/>
                    <input type="hidden" name="$cancel" value=""/>
					<a onclick="postOk();" class="devportal-button success allow" title="$templateMessages.getMessage($messageKeyPrefix, "signInButtonTitle")">
                        $templateMessages.getMessage($messageKeyPrefix, "signInButtonTitle")
                    </a>
                </div><!-- .devportal-buttons -->

				<!--
                <div class="devportal-input-link devportal-pass-change">
                    #if($supportsPasswordChange)
                        <a href="$changePasswordUrl" class="password-change">$templateMessages.getMessage($messageKeyPrefix, "changePassword")</a>
                    #end
                    #if($supportsPasswordChange && $supportsPasswordReset)
                        <span class="divider">|</span>
                    #end
                    #if($supportsPasswordReset)
                    	<a href="javascript:getForgotPasswordUrl()" class="forgot-password">$templateMessages.getMessage($messageKeyPrefix, "forgotPassword")</a>
                    #end
                </div> -->

                <input type="hidden" name="$adapterIdField" id="$adapterIdField" value="$adapterId"/>
            </form>
				</div>
                </div>
			</div>
		</div>
	
	<div class="fluid-container footer-container">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="devportal-footer">$templateMessages.getMessage("global.footerMessage")</div>
				</div>
				<div class="col-md-6">
				<ul class="footer-links">
					<li><a href="#"> Privacy Policy</a></li>
					<li><a href="#"> Terms of Use</a></li>
					<li><a href="#"> Affiliates</a></li>
				</ul>
				</div>
			</div>
		</div>
	</div>
	
    
<script type="text/javascript">

	function getForgotPasswordUrl() 
	{
		var base = "$forgotPasswordUrl";
		#if ($enableRememberUsername)		
		if (document.getElementById('rememberUsername').checked) 
		{
			base += "&prEnableRememberUsername";
		}
		#end 
		window.location.href = base;
	}

    function postOk() {
        document.forms[0]['$ok'].value = 'clicked';
        document.forms[0].submit();
    }
    function postCancel() {
        document.forms[0]['$cancel'].value = 'clicked';
        document.forms[0].submit();
    }
    function postOnReturn(e) {
        var keycode;
        if (window.event) keycode = window.event.keyCode;
        else if (e) keycode = e.which;
        else return true;

        if (keycode == 13) {
            document.forms[0].submit();
            return false;
        } else {
            return true;
        }
    }
    function setFocus() {
        var platform = navigator.platform;
        if(platform != null && platform.indexOf("iPhone") == -1) {
            #if($loginFailed || ($rememberUsernameCookieExists && $enableRememberUsername) || $isChainedUsernameAvailable)
                document.getElementById('password').focus();
            #else
                document.getElementById('username').focus();
            #end
        }
    }
    function setMobile(mobile) {
        var className = ' mobile',
            hasClass = (bodyTag.className.indexOf(className) !== -1);

        if(mobile && !hasClass) {
            bodyTag.className += className;

        } else if (!mobile && hasClass) {
            bodyTag.className = bodyTag.className.replace(className, '');
        }

        #if($enableRememberUsername)
            checkbox.checked = mobile || remember;
        #end

        <!-- Check if this is the PingOne Mobile App -->
        #if($HttpServletRequest.getHeader('X-devportal-Client-Version'))
            if(mobile) {
                bodyTag.className += ' embedded';
            }
        #end
    }
    function getScreenWidth() {
        return (window.outerHeight) ? window.outerWidth : document.body.clientWidth;
    }

    var bodyTag = document.getElementsByTagName('body')[0],
        width = getScreenWidth(),
        remember = $rememberUsernameCookieExists && $enableRememberUsername;

    #if($enableRememberUsername)
        var checkbox = document.getElementById('rememberUsername');
    #end

    if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        setMobile(true);
    } else {
        setMobile((width <= 480));
        window.onresize = function() {
            width = getScreenWidth();
            setMobile((width <= 480));
        }
    }
	
	
	function getSignUpUrl() 
	{
		var base = "https://registration.dp.capgeminiwebservices.com/ssam/register";
		
		window.location.href = base;
		
	}
	
	function getHomePageUrl() 
	{
		var base = "https://cgportal.dp.capgeminiwebservices.com";
		
		window.location.href = base;
		
	}
	
	
</script>

</body>
</html>

var labelList = [];
updateStaticContent();

function updateStaticContent() {
    var contentData = getStaticContent();//getJsonData();
    //console.log("contentData>>"+contentData);
    generateLabelList(contentData, '', labelList);
    var staticElements = $("[data-static-content-key]");
    staticElements.each(function(i, node) {
        node = $(node);
        var content = labelList[node.attr("data-static-content-key")];
        if (content != undefined) {
            node.html(content);
        } else {
            node.html(node.attr("data-static-content-key"));
        }
    });
}

function generateLabelList(contentData, labelKey, labelList) {
    if (contentData instanceof Object) {
        for (var key in contentData) {
            var newLabelKey = (labelKey !== '') ? labelKey + '.' + key : key;
            if (contentData[key] instanceof Object ||
                contentData[key] instanceof Array) {
                generateLabelList(contentData[key], newLabelKey, labelList);
            } else {
                labelList[newLabelKey] = contentData[key];
            }
        }
    } else if (contentData instanceof Array) {
        for (var i = 0; i < contentData.length; i++) {
            generateLabelList(contentData[i], newLabelKey, labelList);
        }
    }
}

function getJsonData(){
    $.getJSON( "assets/scripts/StaticContent.json", function( data ) {
       // return data;
        console.log("contentData "+data);
        return data;
      });
}
function getStaticContent() {
    return {
        "HEADER": {
            "BANK_LOGO_IMAGE_ALTERNET_LABEL": "Bank of Ireland logo",
            "PORTAL_TITLE": "Developer Hub",
            "HELP_LABEL": "Help",
            "LOGO_URL": "img/boi_logo.svg",
            "HELP_TOOLTIP_LABEL": "Help. Link will open in a new window."
        },
        "NAVIGATION_MENU": {
            "GETTING_STARTED_LABEL": "Getting Started",
            "HELP_LABEL": "Help",
            "WELCOME_LABEL": "Welcome",
            "USERNAME_LABEL": "Username",
            "CHANGE_PASSWORD_LABEL": "Change password",
            "LOGOUT_LABEL": "Logout",
            "HELP_URL": "https://www.bankofireland.com/thirdpartyanddeveloperhub/help"
        },
        "FOOTER": {
            "PRIVACY_POLICY_LABEL": "Cookie & Privacy policy ",
            "TNC_LABEL": "Terms of use",
            "HELP_LABEL": "Help",
            "TNC_TOOLTIP_LABEL": "Terms of use. Link will open in a new window.",
            "HELP_TOOLTIP_LABEL": "Help. Link will open in a new window.",
            "COOKIES_TOOLTIP_LABEL": "Cookie and Privacy policy . Link will open in a new  window.",
            "PRIVACY_POLICY_URL": "https://www.bankofireland.com/legal/privacy-statement",
            "TNC_URL": "https://www.bankofireland.com/developerhub/termsofuse",
            "HELP_URL": "https://www.bankofireland.com/thirdpartyanddeveloperhub/help",
            "REGULATORY_LABEL": "Bank of Ireland (UK) plc is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority."
        },
        "LOGIN_PAGE": {
            "HOME_PAGE_INTRO": {
                "HOME_PAGE_HEADING": "Welcome",
                "HOME_PAGE_INTRO_PARA1": "Welcome to the Bank of Ireland Developer Hub. Here you will find everything you need to know about our APIs.",
                "HOME_PAGE_INTRO_PARA2": "The portal offers API documentation to support you in using our APIs, and test data so you can test your applications during developement.",
                "HOME_PAGE_INTRO_PARA3": "Simply <a href='https://federation-sandbox.apiboitest.com/ssam/register'>register</a> your details to access our Developer Hub.",
                "HOME_PAGE_INTRO_PARA4": "We currently provide the Open Banking Read/Write APIs that were developed by Open Banking Limited <a href='https://www.openbanking.org.uk'>https://www.openbanking.org.uk</a>",
                "HOME_PAGE_INTRO_PARA5": ""
            },
            "LOGIN_FORM": {
                "LOGIN_FORM_HEADING": "Login",
                "REGISTRATION_INSTRUCTION_LABEL": "Not registered?",
                "EMAIL_LABEL": "Email",
                "PASSWORD_LABEL": "Password",
                "FORGOT_PASSWORD_LABEL": "Forgot password?",
                "EMAIL_PLACEHOLDER_LABEL": "Example@boi.com",
                "PASSWORD_PLACEHOLDER_LABEL": "Password",
                "BUTTONS": {
                    "LOGIN_BUTTON_LABEL": "Login"
                }
            }
        },
        "REGISTRATION_PAGE": {
            "REGISTER_HEADING": "Register",
            "LOGIN_INSTRUCTION_LABEL": "Already registered?",
            "REGISTER_FORM_INSTRUCTION_LABEL": "Register here to access the Developer Portal.",
            "FIRST_NAME_LABEL": "First name",
            "LAST_NAME_LABEL": "Last name",
            "EMAIL_LABEL": "Email",
            "CONFIRM_EMAIL_LABEL": "Confirm email",
            "PASSWORD_LABEL": "Password",
            "CONFIRM_PASSWORD_LABEL": "Confirm password",
            "PASSWORD_RULES_HEADEING": "Password must contain: ",
            "PASSWORD_RULE1_LABEL": "Minimum 8 characters",
            "PASSWORD_RULE2_LABEL": "Combination of uppercase and lowercase letters",
            "PASSWORD_RULE3_LABEL": "At least one numeric character",
            "PASSWORD_RULE4_LABEL": "At least one special character",
            "ORGANISATION_LABEL": "Organisation",
            "EMAIL_TOOLTIP_LABEL": "Your email address will be required whenever you login to the site.",
            "ORGANISATION_TOOLTIP_LABEL": "The Legal Name of the organisation you work for.",
            "AGREEMENT_CHECKBOX_LABEL": "I agree to the terms of use",
            "EMAIL_PLACEHOLDER_LABEL": "Example@boi.com",
            "PASSWORD_PLACEHOLDER_LABEL": "Password",
            "FIRST_NAME_PLACEHOLDER_LABEL": "First Name",
            "LAST_NAME_PLACEHOLDER_LABEL": "Last Name",
            "CONFIRM_EMAIL_PLACEHOLDER_LABEL": "Example@boi.com",
            "CONFIRM_PASSWORD_PLACEHOLDER_LABEL": "Confirm password",
            "ORGANISATION_PLACEHOLDER_LABEL": "Organisation",
            "BUTTONS": {
                "CANCEL_BUTTON_LABEL": "Cancel",
                "SUBMIT_BUTTON_LABEL": "Submit"
            },
            "REGISTER_POPUP": {
                "REGISTER_POPUP_HEADING": "Registration",
                "REGISTER_POPUP_MESSAGE1": "Your request has been submitted successfully.",
                "REGISTER_POPUP_MESSAGE2": "An activation link has been sent to your registered email address, follow the instructions to complete registration.",
                "REGISTER_POPUP_BUTTON_LABEL": "OK"
            }
    
        },
        "FORGOT_PASSWORD_PAGE": {
            "FORGOT_PASSWORD_HEADING": "Forgot password",
            "REGISTERD_EMAIL_LABEL": "Registered email",
            "REGISTERD_EMAIL_PLACEHOLDER_LABEL": "Registered email",
            "BUTTONS": {
                "CANCEL_BUTTON_LABEL": "Cancel",
                "SUBMIT_BUTTON_LABEL": "Submit"
            },
            "FORGOT_PASSWORD_POPUP": {
                "FORGOT_PASSWORD_POPUP_HEADING": "Forgot password",
                "FORGOT_PASSWORD_POPUP_MESSAGE1": "Your request has been submitted successfully.",
                "FORGOT_PASSWORD_POPUP_MESSAGE2": "An activation link has been sent to your registered email address, follow the instructions to complete registration.",
                "FORGOT_PASSWORD_POPUP_BUTTON_LABEL": "OK"
            }
        },
        "VERIFY_RESET_PASSWORD_LINK_PAGE": {
            "VERIFY_RESET_PASSWORD_LINK_HEADING": "Verify reset password link",
            "VERIFY_RESET_PASSWORD_LINK_MESSAGE": "Please submit another password reset request and try again.",
            "BUTTONS": {
                "VERIFY_RESET_PASSWORD_LINK_BUTTON_LABEL": "Ok"
            }
        },
        "RESEND_ACTIVATION_LINK_PAGE": {
            "RESEND_ACTIVATION_LINK_HEADING": "Resend activation link",
            "RESEND_ACTIVATION_LINK_INSTRUCTION_LABEL": "Please provide your email address for activation",
            "EMAIL_LABEL": "Email",
            "EMAIL_PLACEHOLDER_LABEL": "Example@boi.com",
            "BUTTONS": {
                "CANCEL_BUTTON_LABEL": "Cancel",
                "SUBMIT_BUTTON_LABEL": "Submit"
            },
            "RESEND_ACTIVATION_LINK_POPUP": {
                "RESEND_ACTIVATION_LINK_POPUP_HEADING": "Resend activation link",
                "RESEND_ACTIVATION_LINK_POPUP_MESSAGE1": "Your request has been submitted successfully.",
                "RESEND_ACTIVATION_LINK_POPUP_MESSAGE2": "An activation link has been sent to your registered email address, follow the instructions to complete registration.",
                "RESEND_ACTIVATION_LINK_POPUP_BUTTON_LABEL": "OK"
            }
        },
        "RESET_PASSWORD_PAGE": {
            "RESET_PASSWORD_HEADING": "Reset",
            "NEW_PASSWORD_LABEL": "New password",
            "CONFIRM_PASSWORD_LABEL": "Confirm password",
            "PASSWORD_RULES_HEADEING": "Password must contain: ",
            "PASSWORD_RULE1_LABEL": "Minimum 8 characters",
            "PASSWORD_RULE2_LABEL": "Combination of uppercase and lowercase letters",
            "PASSWORD_RULE3_LABEL": "At least one numeric character",
            "PASSWORD_RULE4_LABEL": "At least one special character",
            "NEW_PASSWORD_PLACEHOLDER_LABEL": "New password",
            "CONFIRM_PASSWORD_PLACEHOLDER_LABEL": "Confirm password",
            "BUTTONS": {
                "CANCEL_BUTTON_LABEL": "Cancel",
                "SUBMIT_BUTTON_LABEL": "Submit"
            },
            "RESET_PASSWORD_POPUP": {
                "RESET_PASSWORD_POPUP_HEADING": "Reset password",
                "RESET_PASSWORD_POPUP_MESSAGE1": "Your password has been succesfully reset.",
                "RESET_PASSWORD_POPUP_BUTTON_LABEL": "OK"
            }
        },
        "VERIFY_ACTIVATION_LINK_PAGE": {
            "VERIFY_ACTIVATION_LINK_HEADING": "Verify activation link",
            "VERIFY_ACTIVATION_LINK_MESSAGE": "You may request for a fresh activation link to present.",
            "BUTTONS": {
                "VERIFY_ACTIVATION_LINK_BUTTON_LABEL": "Resend activation link"
            }
        },
        "GETTING_STARTED_PAGE": {
            "GETTING_STARTED_HEADING": "Welcome to Bank of Ireland Developer Portal",
            "API_PORTAL_INFO": {
                "API_PORTAL_INTRO_HEADING": "Bank of Ireland Developer Portal",
                "API_PORTAL_INTRO": {
                    "API_PORTAL_INTRO_PARA1": "This is a Sandbox environment for testing your applications with our APIs.",
                    "API_PORTAL_INTRO_PARA2": "We recommend that you test your applications in this Sandbox before going live.",
                    "API_PORTAL_INTRO_PARA3": "",
                    "API_PORTAL_INTRO_PARA4": "",
                    "API_PORTAL_INTRO_PARA5": ""
                },
                "BUTTONS": {
                    "DOWNLOAD_TEST_DATA_LABEL": "Download test data",
                    "DOWNLOAD_TEST_DATA_SCREEN_READER_LABEL": "This  will download the test data.",
                    "GOTO_API_SANDBOX_LABEL": "Test your app in API Sandbox",
                    "GOTO_API_SANDBOX_SCREEN_READER_LABEL": "This  will open new mule soft window"
    
                }
            },
            "TPP_PORTAL_INFO": {
                "TPP_PORTAL_INTRO_HEADING": "Go Live",
                "TPP_PORTAL_INTRO": {
                    "TPP_PORTAL_INTRO_PARA1": "Requests for production access to our APIs must be made through our Third Party Hub.",
                    "TPP_PORTAL_INTRO_PARA2": "Please contact your Organisation’s Primary Technical Contact who can register applications.",
                    "TPP_PORTAL_INTRO_PARA3": "",
                    "TPP_PORTAL_INTRO_PARA4": "",
                    "TPP_PORTAL_INTRO_PARA5": ""
                },
                "BUTTONS": {
                    "GOTO_TPP_HUB_LABEL": "Access the Bank of Ireland Third Party Hub",
                    "GOTO_TPP_HUB_SCREEN_READER_LABEL": "This will open third party hub in a new window."
    
                }
            },
            "TESTING_APP_FLOW": {
                "TEST_APP_FLOW_HEADING": "Testing your app",
                "API_CATALOGUE_STEP_LABEL": "View our API catalogue",
                "API_CATALOGUE_STEP_DETAIL_LABEL": "Learn more about the APIs we have to offer",
                "REQUEST_API_STEP_LABEL": "Request API access",
                "REQUEST_API_STEP_DETAIL_LABEL": "Register your app to receive a CID/Secret for API access",
                "DOWNLOAD_TEST_DATA_STEP_LABEL": "Download test data",
                "DOWNLOAD_TEST_DATA_STEP_DETAILS_LABEL": "Test your app using our sample test data"
            },
            "API_SUMMARY_FLOW": {
                "API_SUMMARY_FLOW_HEADING": "Summary of API",
                "API_PROVIDED_SUMMARY_LABEL": "APIs we provide",
                "API_PROVIDED_SUMMARY_DETAIL_LABEL": "We currently provide the Open Banking Read/Write APIs that were developed by <a href='https://www.openbanking.org.uk/' >Open Banking Limited</a>.",
                "MONEY_MOVEMENT_SUMMARY_LABEL": "Money movement",
                "MONEY_MOVEMENT_SUMMARY_DETAIL_LABEL": "Give bank customers the ability to move money across accounts and institutions",
                "ACCOUNT_SUMMARY_LABEL": "Account",
                "ACCOUNT_SUMMARY_DETAILS_LABEL": "Allow bank customers to access their account summaries in an innovative way"
            }
        },
        "CHANGE_PASSWORD_PAGE": {
            "CHANGE_PASSWORD_FORM": {
                "CHANGE_PASSWORD_HEADING": "Change password",
                "CURRENT_PASSWORD_LABEL": "Current password",
                "NEW_PASSWORD_LABEL": "New password",
                "CONFIRM_PASSWORD_LABEL": "Confirm new password",
                "PASSWORD_RULES_HEADEING": "Password must contain: ",
                "PASSWORD_RULE1_LABEL": "Minimum 8 characters",
                "PASSWORD_RULE2_LABEL": "Combination of uppercase and lowercase letters",
                "PASSWORD_RULE3_LABEL": "At least one numeric character",
                "PASSWORD_RULE4_LABEL": "At least one special character",
                "CURRENT_PASSWORD_PLACEHOLDER_LABEL": "Current password",
                "NEW_PASSWORD_PLACEHOLDER_LABEL": "New password",
                "CONFIRM_PASSWORD_PLACEHOLDER_LABEL": "Confirm password",
                "BUTTONS": {
                    "CANCEL_BUTTON_LABEL": "Cancel",
                    "SUBMIT_BUTTON_LABEL": "Submit"
                }
            },
            "PASSWORD_CHANGE_CONFIRMATION_POPUP": {
                "CONFIRMATION_POPUP_HEADING": "Change password",
                "CONFIRMATION_POPUP_MESSAGE": "Your password has been successfully changed.",
                "CONFIRMATION_POPUP_BUTTON_LABEL": "OK"
            }
        },
        "SESSION_TIMEOUT_POPUP": {
            "SESSION_TIMEOUT_POPUP_HEADING": "Session timeout",
            "SESSION_TIMEOUT_POPUP_MESSAGE1": "Your session has timed out.",
            "SESSION_TIMEOUT_POPUP_MESSAGE2": "To continue accessing the site please login again.",
            "SESSION_TIMEOUT_POPUP_BUTTON_LABEL": "Ok"
        },
        "ERROR_MESSAGES": {
            "ERROR_PREFIX_LABEL": "Error",
            "ALERT_PREFIX_LABEL": "Alert",
            "NOTICE_PREFIX_LABEL": "Notice",
            "SUCCESS_PREFIX_LABEL": "Success",
            "MAINTENANCE_ERROR": "true",
            "MAINTENANCE_ERROR_MSG": "Scheduled site maintenance",
            "CORRELATION_LABEL": "Your error reference number is ",
            "CURRENT_PASSWORD_ERROR": "Please enter a valid current password.",
            "NEW_PASSWORD_ERROR": "Please enter a valid password.",
            "CONFIRM_PASSWORD_ERROR": "Your passwords do not match.",
            "JAVASCRIPT_ENABLE_MSG": "You must have JavaScript enabled to use this app.",
            "FIRST_NAME_ERROR": "Please enter valid first name.",
            "LAST_NAME_ERROR": "Please enter valid last name.",
            "EMAIL_ERROR": "Please enter valid email.",
            "CONFIRM_EMAIL_ERROR": "Your emails do not match.",
            "1100": "Technical Error.",
            "1101": "Service Unavailable.",
            "1102": "You are not authorized to use this service.",
            "1103": "Incorrect current password.",
            "1104": "The system was unable to reset the password. Please contact a site administrator for assistance.",
            "1105": "Unable to change password. Please try again later.",
            "1106": "Unable to change password. Please try again later.",
            "1107": "Unable to change password. Please try again later.",
            "1108": "Your session has expired. Please log in again."
    
        }
    }
}
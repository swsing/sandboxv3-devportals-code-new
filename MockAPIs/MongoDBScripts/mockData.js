db = db.getSiblingDB('aspspPSD2')
db.loginDetails.remove({"loginId": "32118465"})
db.loginDetails.remove({"loginId": "91807656"})
db.loginDetails.remove({"loginId": "89941690"})
db.loginDetails.remove({"loginId": "47508347"})
db.loginDetails.remove({"loginId": "10546298"})
db.loginDetails.remove({"loginId": "18070751"})
db.loginDetails.remove({"loginId": "70613119"})
db.loginDetails.remove({"loginId": "34080157"})
db.loginDetails.remove({"loginId": "58330126"})
db.loginDetails.remove({"loginId": "88072266"})

db = db.getSiblingDB('aspspPSD2')
db.mockChannelProfile.remove({"psuId": "32118465"})
db.mockChannelProfile.remove({"psuId": "91807656"})
db.mockChannelProfile.remove({"psuId": "89941690"})
db.mockChannelProfile.remove({"psuId": "47508347"})
db.mockChannelProfile.remove({"psuId": "10546298"})
db.mockChannelProfile.remove({"psuId": "18070751"})
db.mockChannelProfile.remove({"psuId": "70613119"})
db.mockChannelProfile.remove({"psuId": "34080157"})
db.mockChannelProfile.remove({"psuId": "58330126"})
db.mockChannelProfile.remove({"psuId": "88072266"})

db = db.getSiblingDB('aspspPSD2')
db.mockAccount.remove({"psuId" : "32118465"})
db.mockBalance.remove({"psuId" : "32118465"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "23682780"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "23682779"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "25369421"})
db.mockAccount.remove({"psuId" : "91807656"})
db.mockBalance.remove({"psuId" : "91807656"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "15369600"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "15369605"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "15369606"})
db.mockAccount.remove({"psuId" : "89941690"})
db.mockBalance.remove({"psuId" : "89941690"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "47550843"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "23682877"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "95369699"})
db.mockAccount.remove({"psuId" : "47508347"})
db.mockBalance.remove({"psuId" : "47508347"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "23681001"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "23681002"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "53111155"})
db.mockAccount.remove({"psuId" : "10546298"})
db.mockBalance.remove({"psuId" : "10546298"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "23781190"})
db.mockAccount.remove({"psuId" : "18070751"})
db.mockBalance.remove({"psuId" : "18070751"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "12345678"})
db.mockAccount.remove({"psuId" : "34080157"})
db.mockBalance.remove({"psuId" : "34080157"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "15369611"})
db.mockAccount.remove({"psuId" : "70613119"})
db.mockBalance.remove({"psuId" : "70613119"})
db.mockAccountTransactionsGETResponseData.remove({"accountNumber" : "15369608"})

db.MockAccountInformationCMA2.remove({"psuId" : "32118465"})
db.MockAccountInformationCMA2.remove({"psuId" : "91807656"})
db.MockAccountInformationCMA2.remove({"psuId" : "89941690"})
db.MockAccountInformationCMA2.remove({"psuId" : "47508347"})
db.MockAccountInformationCMA2.remove({"psuId" : "10546298"})
db.MockAccountInformationCMA2.remove({"psuId" : "18070751"})
db.MockAccountInformationCMA2.remove({"psuId" : "70613119"})
db.MockAccountInformationCMA2.remove({"psuId" : "34080157"})

db.MockAccountBalanceCMA2.remove({"psuId" : "32118465"})
db.MockAccountBalanceCMA2.remove({"psuId" : "91807656"})
db.MockAccountBalanceCMA2.remove({"psuId" : "89941690"})
db.MockAccountBalanceCMA2.remove({"psuId" : "47508347"})
db.MockAccountBalanceCMA2.remove({"psuId" : "10546298"})
db.MockAccountBalanceCMA2.remove({"psuId" : "18070751"})
db.MockAccountBalanceCMA2.remove({"psuId" : "70613119"})
db.MockAccountBalanceCMA2.remove({"psuId" : "34080157"})

db.MockAccountTransactionCMA2.remove({"psuId" : "32118465"})
db.MockAccountTransactionCMA2.remove({"psuId" : "91807656"})
db.MockAccountTransactionCMA2.remove({"psuId" : "89941690"})
db.MockAccountTransactionCMA2.remove({"psuId" : "47508347"})
db.MockAccountTransactionCMA2.remove({"psuId" : "10546298"})
db.MockAccountTransactionCMA2.remove({"psuId" : "18070751"})
db.MockAccountTransactionCMA2.remove({"psuId" : "70613119"})
db.MockAccountTransactionCMA2.remove({"psuId" : "34080157"})

db.MockAccountBeneficiaryCMA2.remove({"psuId" : "32118465"})
db.MockAccountBeneficiaryCMA2.remove({"psuId" : "91807656"})
db.MockAccountBeneficiaryCMA2.remove({"psuId" : "89941690"})
db.MockAccountBeneficiaryCMA2.remove({"psuId" : "47508347"})
db.MockAccountBeneficiaryCMA2.remove({"psuId" : "10546298"})
db.MockAccountBeneficiaryCMA2.remove({"psuId" : "18070751"})
db.MockAccountBeneficiaryCMA2.remove({"psuId" : "70613119"})
db.MockAccountBeneficiaryCMA2.remove({"psuId" : "34080157"})

db.MockAccountDirectDebitCMA2.remove({"psuId" : "32118465"})
db.MockAccountDirectDebitCMA2.remove({"psuId" : "91807656"})
db.MockAccountDirectDebitCMA2.remove({"psuId" : "89941690"})
db.MockAccountDirectDebitCMA2.remove({"psuId" : "47508347"})
db.MockAccountDirectDebitCMA2.remove({"psuId" : "10546298"})
db.MockAccountDirectDebitCMA2.remove({"psuId" : "18070751"})
db.MockAccountDirectDebitCMA2.remove({"psuId" : "70613119"})
db.MockAccountDirectDebitCMA2.remove({"psuId" : "34080157"})

db.MockAccountStandingOrderCMA2.remove({"psuId" : "32118465"})
db.MockAccountStandingOrderCMA2.remove({"psuId" : "91807656"})
db.MockAccountStandingOrderCMA2.remove({"psuId" : "89941690"})
db.MockAccountStandingOrderCMA2.remove({"psuId" : "47508347"})
db.MockAccountStandingOrderCMA2.remove({"psuId" : "10546298"})
db.MockAccountStandingOrderCMA2.remove({"psuId" : "18070751"})
db.MockAccountStandingOrderCMA2.remove({"psuId" : "70613119"})
db.MockAccountStandingOrderCMA2.remove({"psuId" : "34080157"})


db.MockAccountProductCMA2.remove({"psuId" : "32118465"})
db.MockAccountProductCMA2.remove({"psuId" : "91807656"})
db.MockAccountProductCMA2.remove({"psuId" : "89941690"})
db.MockAccountProductCMA2.remove({"psuId" : "47508347"})
db.MockAccountProductCMA2.remove({"psuId" : "10546298"})
db.MockAccountProductCMA2.remove({"psuId" : "18070751"})
db.MockAccountProductCMA2.remove({"psuId" : "70613119"})
db.MockAccountProductCMA2.remove({"psuId" : "34080157"})


db = db.getSiblingDB('aspspPSD2')
db.loginDetails.save(
{
    "loginId": "32118465",
    "password": "331947"
} 
)

db.loginDetails.save(
{
    "loginId": "91807656",
    "password": "331948"
} 
)

db.loginDetails.save(
{
    "loginId": "89941690",
    "password": "331949"
} 
)

db.loginDetails.save(
{
    "loginId": "47508347",
    "password": "331950"
} 
)

db.loginDetails.save(
{
    "loginId": "10546298",
    "password": "331951"
} 
)

db.loginDetails.save(
{
    "loginId": "18070751",
    "password": "331952"
} 
)

db.loginDetails.save(
{
    "loginId": "70613119",
    "password": "331953"
} 
)

db.loginDetails.save(
{
    "loginId": "34080157",
    "password": "331954"
} 
)

db.loginDetails.save(
{
    "loginId": "58330126",
    "password": "331955"
} 
)

db.loginDetails.save(
{
    "loginId": "88072266",
    "password": "331956"
} 
)

db = db.getSiblingDB('aspspPSD2')
db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "32118465",
	        "nickname" : "Greg Chappel Test User-1",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "90144223682780",
	                "secondaryIdentification" : "Roll 54988"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI90144223682780",
	                "ACCOUNT-NUMBER" : "23682780",
	                "ACCOUNT-NSC" : "901442",
	                "PAYER-JURISDICTION" : "GREAT_BRITAIN",
	                "accountPermission" : "V"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "32118465",
	        "nickname" : "Greg Chappel",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "90144123682779",
	                "secondaryIdentification" : "Roll 54988"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI90144123682779",
	                "ACCOUNT-NUMBER" : "23682779",
	                "ACCOUNT-NSC" : "901441",
	                "PAYER-JURISDICTION" : "GREAT_BRITAIN",
	                "accountPermission" : "V"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "32118465",
	        "nickname" : "Rutsara Aysan",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "90347925369421",
	                "secondaryIdentification" : "Roll 78352"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI90347925369421",
	                "ACCOUNT-NUMBER" : "25369421",
	                "ACCOUNT-NSC" : "903479",
	                "PAYER-JURISDICTION" : "GREAT_BRITAIN",
	                "accountPermission" : "X"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "91807656",
	        "nickname" : "Shannon Good",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "91378915369600",
	                "secondaryIdentification" : "Roll 54678"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI91378915369600",
	                "ACCOUNT-NUMBER" : "15369600",
	                "ACCOUNT-NSC" : "913789",
	                "PAYER-JURISDICTION" : "NORTHERN_IRELAND",
	                "accountPermission" : "V"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "91807656",
	        "nickname" : "Shannon",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "91310515369605",
	                "secondaryIdentification" : "Roll 54968"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI91310515369605",
	                "ACCOUNT-NUMBER" : "15369605",
	                "ACCOUNT-NSC" : "913105",
	                "PAYER-JURISDICTION" : "NORTHERN_IRELAND",
	                "accountPermission" : "A"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "91807656",
	        "nickname" : "Ms. Good",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "91310615369606",
	                "secondaryIdentification" : "Roll 55638"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI91310615369606",
	                "ACCOUNT-NUMBER" : "15369606",
	                "ACCOUNT-NSC" : "913106",
	                "PAYER-JURISDICTION" : "NORTHERN_IRELAND",
	                "accountPermission" : "X"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "89941690",
	        "nickname" : "Kristen Balley",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "90134347550843",
	                "secondaryIdentification" : "Roll 34678"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI90134347550843",
	                "ACCOUNT-NUMBER" : "47550843",
	                "ACCOUNT-NSC" : "901343",
	                "PAYER-JURISDICTION" : "NORTHERN_IRELAND",
	                "accountPermission" : "V"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "89941690",
	        "nickname" : "Joint_AccountJX_Test user2",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "90310123682877",
	                "secondaryIdentification" : "Roll 26968"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI90310123682877",
	                "ACCOUNT-NUMBER" : "23682877",
	                "ACCOUNT-NSC" : "903101",
	                "PAYER-JURISDICTION" : "NORTHERN_IRELAND",
	                "accountPermission" : "JX"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "89941690",
	        "nickname" : "Kristen Balley_JV",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "90347995369699",
	                "secondaryIdentification" : "Roll 56738"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI90347995369699",
	                "ACCOUNT-NUMBER" : "95369699",
	                "ACCOUNT-NSC" : "903479",
	                "PAYER-JURISDICTION" : "GREAT_BRITAIN",
	                "accountPermission" : "JV"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "47508347",
	        "nickname" : "Adam Cris",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "90144123681001",
	                "secondaryIdentification" : "Roll 34731"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI90144123681001",
	                "ACCOUNT-NUMBER" : "23681001",
	                "ACCOUNT-NSC" : "901441",
	                "PAYER-JURISDICTION" : "GREAT_BRITAIN",
	                "accountPermission" : "V"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "47508347",
	        "nickname" : "Joint Account_2 JA_Adam",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "90144123681002",
	                "secondaryIdentification" : "Roll 93668"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI90144123681002",
	                "ACCOUNT-NUMBER" : "23681002",
	                "ACCOUNT-NSC" : "901441",
	                "PAYER-JURISDICTION" : "NORTHERN_IRELAND",
	                "accountPermission" : "JA"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "47508347",
	        "nickname" : "Joint Account_4 JZ_Adam",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "90144153111155",
	                "secondaryIdentification" : "Roll 47802"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI90144153111155",
	                "ACCOUNT-NUMBER" : "53111155",
	                "ACCOUNT-NSC" : "901441",
	                "PAYER-JURISDICTION" : "GREAT_BRITAIN",
	                "accountPermission" : "JZ"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "GBP",
	        "psuId" : "10546298",
	        "nickname" : "John Snow",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "93115223781190",
	                "secondaryIdentification" : "Roll 23429"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI93115223781190",
	                "ACCOUNT-NUMBER" : "23781190",
	                "ACCOUNT-NSC" : "931152",
	                "PAYER-JURISDICTION" : "GREAT_BRITAIN",
	                "accountPermission" : "V"
	        }
})


db.mockChannelProfile.save(
{
	        
	        "currency" : "EUR",
	        "psuId" : "18070751",
	        "nickname" : "Joseph Barney",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "93115212345678",
	                "secondaryIdentification" : "Roll 34731"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI93115212345678",
	                "ACCOUNT-NUMBER" : "12345678",
	                "ACCOUNT-NSC" : "931152",
	                "PAYER-JURISDICTION" : "GREAT_BRITAIN",
	                "accountPermission" : "A"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "EUR",
	        "psuId" : "70613119",
	        "nickname" : "Joe Root",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "91310815369608",
	                "secondaryIdentification" : "Roll 65783"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI91310815369608",
	                "ACCOUNT-NUMBER" : "15369608",
	                "ACCOUNT-NSC" : "913108",
	                "PAYER-JURISDICTION" : "NORTHERN_IRELAND",
	                "accountPermission" : "V"
	        }
})

db.mockChannelProfile.save(
{
	        
	        "currency" : "BGP",
	        "psuId" : "34080157",
	        "nickname" : "Alan Hunter",
	        "account" : {
	                "schemeName" : "SORTCODEACCOUNTNUMBER",
	                "identification" : "91311115369611",
	                "secondaryIdentification" : "Roll 42396"
	        },
	        "accountType" : "Current",
	        "additionalInformation" : {
	                "BICFI" : "BOFIIE2DXXX",
	                "IBAN" : "IE76BOFI91311115369611",
	                "ACCOUNT-NUMBER" : "15369611",
	                "ACCOUNT-NSC" : "913111",
	                "PAYER-JURISDICTION" : "NORTHERN_IRELAND",
	                "accountPermission" : "V"
	        }
})


db = db.getSiblingDB('aspspPSD2')
db.mockAccount.save(
{
   
    "psuId" : "32118465",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "23682780",
        "name" : "Ian Chappel",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901442"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "32118465",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "23682779",
        "name" : "Master Greg",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901441"
    }
}
)


db.mockAccount.save(
{
   
    "psuId" : "32118465",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "25369421",
        "name" : "Kristen Stuart",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "903479"
    }
}
)


db.mockBalance.save(
{
    "psuId" : "32118465",
    "accountNumber" : "23682780",
    "accountNSC" : "901442",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockBalance.save(
{
    "psuId" : "32118465",
    "accountNumber" : "23682779",
    "accountNSC" : "901441",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockBalance.save(
{
    "psuId" : "32118465",
    "accountNumber" : "25369421",
    "accountNSC" : "903479",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "23682780",
    "accountNSC" : "901442",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "23682779",
    "accountNSC" : "901441",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)


db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "25369421",
    "accountNSC" : "903479",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "91807656",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "15369600",
        "name" : "Ian Chappel",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "913789"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "91807656",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "15369605",
        "name" : "Master Greg",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "913105"
    }
}
)


db.mockAccount.save(
{
   
    "psuId" : "91807656",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "15369606",
        "name" : "Kristen Stuart",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "913106"
    }
}
)

db.mockBalance.save(
{
    "psuId" : "91807656",
    "accountNumber" : "15369600",
    "accountNSC" : "913789",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockBalance.save(
{
    "psuId" : "91807656",
    "accountNumber" : "15369605",
    "accountNSC" : "913105",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockBalance.save(
{
    "psuId" : "91807656",
    "accountNumber" : "15369606",
    "accountNSC" : "913106",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)


db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "15369600",
    "accountNSC" : "913789",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "15369605",
    "accountNSC" : "913105",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "15369606",
    "accountNSC" : "913106",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "89941690",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "47550843",
        "name" : "Ian Chappel",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901343"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "89941690",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "23682877",
        "name" : "Master Greg",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "903101"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "89941690",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "95369699",
        "name" : "Kristen Stuart",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "903479"
    }
}
)

db.mockBalance.save(
{
    "psuId" : "89941690",
    "accountNumber" : "47550843",
    "accountNSC" : "901343",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)


db.mockBalance.save(
{
    "psuId" : "89941690",
    "accountNumber" : "23682877",
    "accountNSC" : "903101",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)


db.mockBalance.save(
{
    "psuId" : "89941690",
    "accountNumber" : "95369699",
    "accountNSC" : "903479",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "47550843",
    "accountNSC" : "901343",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)


db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "23682877",
    "accountNSC" : "903101",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)


db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "95369699",
    "accountNSC" : "903479",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "47508347",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "23681001",
        "name" : "Ian Chappel",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901441"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "47508347",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "23681002",
        "name" : "Master Greg",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901441"
    }
}
)


db.mockAccount.save(
{
   
    "psuId" : "47508347",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "53111155",
        "name" : "Kristen Stuart",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901441"
    }
}
)


db.mockBalance.save(
{
    "psuId" : "47508347",
    "accountNumber" : "23681001",
    "accountNSC" : "901441",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)


db.mockBalance.save(
{
    "psuId" : "47508347",
    "accountNumber" : "23681002",
    "accountNSC" : "901441",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)


db.mockBalance.save(
{
    "psuId" : "47508347",
    "accountNumber" : "53111155",
    "accountNSC" : "901441",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "23681001",
    "accountNSC" : "901441",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)


db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "23681002",
    "accountNSC" : "901441",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)


db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "53111155",
    "accountNSC" : "901441",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "10546298",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "23781190",
        "name" : "Ian Chappel",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "931152"
    }
}
)

db.mockBalance.save(
{
    "psuId" : "10546298",
    "accountNumber" : "23781190",
    "accountNSC" : "931152",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "23781190",
    "accountNSC" : "931152",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "18070751",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "12345678",
        "name" : "Ian Chappel",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "931152"
    }
}
)

db.mockBalance.save(
{
    "psuId" : "18070751",
    "accountNumber" : "12345678",
    "accountNSC" : "931152",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "12345678",
    "accountNSC" : "931152",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "34080157",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "15369611",
        "name" : "Ian Chappel",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "913111"
    }
}
)

db.mockBalance.save(
{
    "psuId" : "34080157",
    "accountNumber" : "15369611",
    "accountNSC" : "913111",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "15369611",
    "accountNSC" : "913111",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db.mockAccount.save(
{
   
    "psuId" : "70613119",
    "currency" : "GBP",
    "nickname" : "KKR",
    "account" : {
        "schemeName" : "IBAN",
        "identification" : "15369608",
        "name" : "Ian Chappel",
        "secondaryIdentification" : "0002"
    },
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "913108"
    }
}
)

db.mockBalance.save(
{
    "psuId" : "70613119",
    "accountNumber" : "15369608",
    "accountNSC" : "913108",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.mockAccountTransactionsGETResponseData.save(
{
    "accountNumber" : "15369608",
    "accountNSC" : "913108",
    "bookingDateTimeCopy" : ISODate("2016-12-01T21:36:20.201Z"),
    "transactionId" : "567",
    "transactionReference" : "CR_01_Nov_16",
    "amount" : {
        "amount" : "100.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2016-11-01T21:36:20.201Z",
    "valueDateTime" : "2016-11-01T00:00:00.000Z",
    "transactionInformation" : "Paid the gas bill 12345678 from account 123123123123123123 to 567567567567",
    "addressLine" : "Coventry",
    "bankTransactionCode" : {
        "code" : "IssuedCreditTransfer",
        "subCode" : "AutomaticTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "DirectDebit",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "57.36",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "merchantDetails" : {
        "merchantName" : "XYZname",
        "merchantCategoryCode" : "XYZ"
    }
}
)

db = db.getSiblingDB('aspspPSD2')
db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "32118465",
	"accountNumber": "23682780",
    "nsc": "901442",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "23682780",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901442"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "32118465",
	"accountNumber": "23682779",
    "nsc": "901441",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "23682779",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901441"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "32118465",
	"accountNumber": "25369421",
    "nsc": "903479",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "25369421",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "903479"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "91807656",
	"accountNumber": "15369600",
    "nsc": "913789",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "15369600",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "913789"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "91807656",
	"accountNumber": "15369605",
    "nsc": "913105",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "15369605",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "913105"
    }
}
)


db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "91807656",
	"accountNumber": "15369606",
    "nsc": "913106",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "15369606",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "913106"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "89941690",
	"accountNumber": "47550843",
    "nsc": "901343",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "47550843",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901343"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "89941690",
	"accountNumber": "23682877",
    "nsc": "903101",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "23682877",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "903101"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "89941690",
	"accountNumber": "95369699",
    "nsc": "903479",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "95369699",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "903479"
    }
}
)


db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "47508347",
	"accountNumber": "23681001",
    "nsc": "901441",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "23681001",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901441"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "47508347",
	"accountNumber": "23681002",
    "nsc": "901441",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "23681002",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901441"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "47508347",
	"accountNumber": "53111155",
    "nsc": "901441",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "53111155",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "901441"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "10546298",
	"accountNumber": "23781190",
    "nsc": "931152",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "23781190",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "931152"
    }
}
)


db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "18070751",
	"accountNumber": "12345678",
    "nsc": "931152",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "12345678",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "931152"
    }
}
)


db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "70613119",
	"accountNumber": "15369608",
    "nsc": "913108",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "15369608",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "913108"
    }
}
)

db.MockAccountInformationCMA2.save(
{
  
    "psuId" : "34080157",
	"accountNumber": "15369611",
    "nsc": "913111",
    "currency" : "GBP",
    "accountType" : "BUSINESS",
    "accountSubType" : "CHARGECARD",
    "nickname" : "KKR",
    "account" : [ 
        {
            "schemeName" : "IBAN",
            "identification" : "15369611",
            "name" : "Avinash",
            "secondaryIdentification" : "0002"
        }
    ],
    "servicer" : {
        "schemeName" : "BICFI",
        "identification" : "913111"
    }
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "23682780",
    "accountNSC": "901442",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "23682779",
    "accountNSC": "901441",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "25369421",
    "accountNSC": "903479",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369600",
    "accountNSC": "913789",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369605",
    "accountNSC": "913105",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369606",
    "accountNSC": "913106",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "47550843",
    "accountNSC": "901343",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "23682877",
    "accountNSC": "903101",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "95369699",
    "accountNSC": "903479",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "23681001",
    "accountNSC": "901441",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "23681002",
    "accountNSC": "901441",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "53111155",
    "accountNSC": "901441",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "10546298",
    "accountNumber": "23781190",
    "accountNSC": "931152",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "18070751",
    "accountNumber": "12345678",
    "accountNSC": "931152",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "70613119",
    "accountNumber": "15369608",
    "accountNSC": "913108",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)

db.MockAccountBalanceCMA2.save(
{
    "psuId" : "34080157",
    "accountNumber": "15369611",
    "accountNSC": "913111",
    "amount" : {
        "amount" : "1230.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "type" : "INTERIMAVAILABLE",
    "dateTime" : "2017-04-05T10:43:07+00:00",
    "creditLine" : [ 
        {
            "included" : true,
            "amount" : {
                "amount" : "1000.00",
                "currency" : "GBP"
            },
            "type" : "PRE_AGREED"
        }
    ]
}
)


db.MockAccountTransactionCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "23682780",
    "accountNSC": "901442",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "23682779",
    "accountNSC": "901441",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "25369421",
    "accountNSC": "903479",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369600",
    "accountNSC": "913789",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369605",
    "accountNSC": "913105",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369606",
    "accountNSC": "913106",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "47550843",
    "accountNSC": "901343",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "23682877",
    "accountNSC": "903101",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "95369699",
    "accountNSC": "903479",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "23681001",
    "accountNSC": "901441",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "23681002",
    "accountNSC": "901441",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "53111155",
    "accountNSC": "901441",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "10546298",
    "accountNumber": "23781190",
    "accountNSC": "931152",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "18070751",
    "accountNumber": "12345678",
    "accountNSC": "931152",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "70613119",
    "accountNumber": "15369608",
    "accountNSC": "913108",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountTransactionCMA2.save(
{
    "psuId" : "34080157",
    "accountNumber": "15369611",
    "accountNSC": "913111",
    "transactionId" : "123",
    "transactionReference" : "Ref 123",
    "amount" : {
        "amount" : "10.00",
        "currency" : "GBP"
    },
    "creditDebitIndicator" : "CREDIT",
    "status" : "BOOKED",
    "bookingDateTime" : "2017-04-05T10:45:22+00:00",
    "bookingDateTimeCopy" : ISODate("2017-04-05T14:50:22.201Z"),
    "valueDateTime" : "2017-04-05T10:45:22+00:00",
    "transactionInformation" : "Cash from Aubrey",
    "bankTransactionCode" : {
        "code" : "receivedCreditTransfer",
        "subCode" : "domesticCreditTransfer"
    },
    "proprietaryBankTransactionCode" : {
        "code" : "Transfer",
        "issuer" : "AlphaBank"
    },
    "balance" : {
        "amount" : {
            "amount" : "230.00",
            "currency" : "GBP"
        },
        "creditDebitIndicator" : "CREDIT",
        "type" : "INTERIMBOOKED"
    },
    "firstAvailableDateTime" : "2017-05-03T00:00:00+00:00",
    "lastAvailableDateTime" : "2017-12-03T00:00:00+00:00"
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "23682780",
    "accountNSC": "901442",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "23682779",
    "accountNSC": "901441",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "25369421",
    "accountNSC": "903479",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369600",
    "accountNSC": "913789",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)


db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369605",
    "accountNSC": "913105",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369606",
    "accountNSC": "913106",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "47550843",
    "accountNSC": "901343",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "23682877",
    "accountNSC": "903101",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "95369699",
    "accountNSC": "903779",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "23681001",
    "accountNSC": "901541",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "23681002",
    "accountNSC": "901541",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "53111155",
    "accountNSC": "901541",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "10546298",
    "accountNumber": "23781190",
    "accountNSC": "931152",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "18070751",
    "accountNumber": "12345678",
    "accountNSC": "931152",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "70613119",
    "accountNumber": "15369608",
    "accountNSC": "913108",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

db.MockAccountBeneficiaryCMA2.save(
{
    "psuId" : "34080157",
    "accountNumber": "15369611",
    "accountNSC": "913111",
    "beneficiaryId" : "string",
        "reference" : "string",
        "creditorAgent" : {
                "schemeName" : "BICFI",
                "identification" : "string",
                "name" : "string",
                "postalAddress" : {
                        "addressType" : "BUSINESS",
                        "department" : "string",
                        "subDepartment" : "string",
                        "streetName" : "string",
                        "buildingNumber" : "string",
                        "postCode" : "string",
                        "townName" : "string",
                        "countrySubDivision" : "string",
                        "country" : "IN",
                        "addressLine" : "string"
                }
        },
        "creditorAccount" : {
                "schemeName" : "IBAN",
                "identification" : "string",
                "name" : "string",
                "secondaryIdentification" : "123"
        }
}
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "23682780",
    "accountNSC": "901442",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "23682779",
    "accountNSC": "901441",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "25369421",
    "accountNSC": "903479",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369600",
    "accountNSC": "913789",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369605",
    "accountNSC": "913105",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369606",
    "accountNSC": "913106",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "47550843",
    "accountNSC": "901343",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "23682877",
    "accountNSC": "903101",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "95369699",
    "accountNSC": "903479",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "23681001",
    "accountNSC": "901441",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "23681002",
    "accountNSC": "901441",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "53111155",
    "accountNSC": "901441",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "10546298",
    "accountNumber": "23781190",
    "accountNSC": "931152",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "18070751",
    "accountNumber": "12345678",
    "accountNSC": "931152",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "70613119",
    "accountNumber": "15369608",
    "accountNSC": "913108",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountDirectDebitCMA2.save(
{
    "psuId" : "34080157",
    "accountNumber": "15369611",
    "accountNSC": "913111",
    "directDebitId" : "string",
    "mandateIdentification" : "string",
    "directDebitStatusCode" : "ACTIVE",
    "name" : "Towbar Club 3 - We Love Towbars",
    "previousPaymentDateTime" : "2018-05-08T07:24:39.113Z",
    "previousPaymentAmount" : {
        "amount" : "0.57",
        "currency" : "GBP"
    }
}
)

db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "23682780",
    "accountNSC": "901442",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)  

db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "23682779",
    "accountNSC": "901441",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)  

db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "32118465",
    "accountNumber": "25369421",
    "accountNSC": "903479",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)  

db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369600",
    "accountNSC": "913789",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)


db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369605",
    "accountNSC": "913105",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)


db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "91807656",
    "accountNumber": "15369606",
    "accountNSC": "913106",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)


db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "47550843",
    "accountNSC": "901343",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)


db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "23682877",
    "accountNSC": "903101",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)


db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "89941690",
    "accountNumber": "95369699",
        "accountNSC": "903479",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)


db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "23681001",
    "accountNSC": "901441",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)


db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "23681002",
    "accountNSC": "901441",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)

db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "47508347",
    "accountNumber": "53111155",
    "accountNSC": "901441",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)

db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "10546298",
    "accountNumber": "23781190",
    "accountNSC": "931152",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)

db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "18070751",
    "accountNumber": "12345678",
    "accountNSC": "931152",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)

db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "70613119",
    "accountNumber": "15369608",
    "accountNSC": "913108",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)

db.MockAccountStandingOrderCMA2.save(
{
    "psuId" : "34080157",
    "accountNumber": "15369611",
    "accountNSC": "913111",
    "standingOrderId" : "string",
    "frequency" : "QtrDay:ENGLISH",
    "reference" : "string",
    "firstPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "firstPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "nextPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "nextPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "finalPaymentDateTime" : "2018-05-09T11:10:12.064Z",
    "finalPaymentAmount" : {
        "amount" : "123456.765",
        "currency" : "GBP"
    },
    "standingOrderStatusCode" : "ACTIVE",
    "creditorAgent" : {
        "schemeName" : "BICFI",
        "identification" : "string"
    },
    "creditorAccount" : {
        "schemeName" : "IBAN",
        "identification" : "string",
        "name" : "string",
        "secondaryIdentification" : "string"
    }
}
)

db.MockAccountProductCMA2.save(
{
	"psuId" : "32118465",
    "accountNumber": "23682780",
    "accountNSC": "901442",
    "productName" : "string",
    "productId" : "string",
    "secondaryProductId" : "string",
    "productType" : "PERSONALCURRENTACCOUNT",
    "marketingStateId" : "string",
    "PCA" : {
        "productDetails" : {
            "segment" : [ 
                "BASIC"
            ],
            "monthlyMaximumCharge" : "99999.99",
            "notes" : [ 
                "string"
            ]
        },
        "creditInterest" : {
            "tierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "calculationMethod" : "COMPOUND",
                    "destination" : "PAYAWAY",
                    "notes" : [ 
                        "string"
                    ],
                    "tierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMinimum" : "99999.99",
                            "tierValueMaximum" : "99999.99",
                            "calculationFrequency" : "PERACADEMICTERM",
                            "applicationFrequency" : "PERACADEMICTERM",
                            "depositInterestAppliedCoverage" : "TIERED",
                            "fixedVariableInterestRateType" : "FIXED",
                            "AER" : "333.33",
                            "bankInterestRateType" : "LINKEDBASERATE",
                            "bankInterestRate" : "888.88",
                            "notes" : [ 
                                "string"
                            ],
                            "otherBankInterestType" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherApplicationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherCalculationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            }
                        }
                    ]
                }
            ]
        },
        "overdraft" : {
            "notes" : [ 
                "string"
            ],
            "overdraftTierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "overdraftType" : "COMMITTED",
                    "identification" : "string",
                    "authorisedIndicator" : true,
                    "bufferAmount" : "99999.99",
                    "notes" : [ 
                        "string"
                    ],
                    "overdraftTierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMin" : "99999.99",
                            "tierValueMax" : "99999.99",
                            "overdraftInterestChargingCoverage" : "TIERED",
                            "bankGuaranteedIndicator" : true,
                            "EAR" : "333.33",
                            "notes" : [ 
                                "string"
                            ],
                            "overdraftFeesCharges" : [ 
                                {
                                    "overdraftFeeChargeCap" : [ 
                                        {
                                            "feeType" : [ 
                                                "ARRANGEDOVERDRAFT"
                                            ],
                                            "overdraftControlIndicator" : true,
                                            "minMaxType" : "MINIMUM",
                                            "feeCapOccurrence" : 0,
                                            "feeCapAmount" : "99999.99",
                                            "cappingPeriod" : "ACADEMICTERM",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : [ 
                                                {
                                                    "code" : "str4",
                                                    "name" : "string",
                                                    "description" : "string"
                                                }
                                            ]
                                        }
                                    ],
                                    "overdraftFeeChargeDetail" : [ 
                                        {
                                            "feeType" : "ARRANGEDOVERDRAFT",
                                            "overdraftControlIndicator" : true,
                                            "incrementalBorrowingAmount" : "99999.99",
                                            "feeAmount" : "99999.99",
                                            "feeRate" : "333.11",
                                            "feeRateType" : "LINKEDBASERATE",
                                            "applicationFrequency" : "ACCOUNTCLOSING",
                                            "calculationFrequency" : "ACCOUNTCLOSING",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherFeeRateType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherApplicationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherCalculationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "overdraftFeeChargeCap" : {
                                                "feeType" : [ 
                                                    "ARRANGEDOVERDRAFT"
                                                ],
                                                "overdraftControlIndicator" : true,
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "99999.99",
                                                "cappingPeriod" : "ACADEMICTERM",
                                                "notes" : [ 
                                                    "string"
                                                ],
                                                "otherFeeType" : [ 
                                                    {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "overdraftFeesCharges" : [ 
                        {
                            "overdraftFeeChargeCap" : [ 
                                {
                                    "feeType" : [ 
                                        "ARRANGEDOVERDRAFT"
                                    ],
                                    "overdraftControlIndicator" : true,
                                    "minMaxType" : "MINIMUM",
                                    "feeCapOccurrence" : 0,
                                    "feeCapAmount" : "99999.99",
                                    "cappingPeriod" : "ACADEMICTERM",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : [ 
                                        {
                                            "code" : "str4",
                                            "name" : "string",
                                            "description" : "string"
                                        }
                                    ]
                                }
                            ],
                            "overdraftFeeChargeDetail" : [ 
                                {
                                    "feeType" : "ARRANGEDOVERDRAFT",
                                    "overdraftControlIndicator" : true,
                                    "incrementalBorrowingAmount" : "99999.99",
                                    "feeAmount" : "99999.99",
                                    "feeRate" : "333.11",
                                    "feeRateType" : "LINKEDBASERATE",
                                    "applicationFrequency" : "ACCOUNTCLOSING",
                                    "calculationFrequency" : "ACCOUNTCLOSING",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherFeeRateType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherApplicationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherCalculationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "overdraftFeeChargeCap" : {
                                        "feeType" : [ 
                                            "ARRANGEDOVERDRAFT"
                                        ],
                                        "overdraftControlIndicator" : true,
                                        "minMaxType" : "MINIMUM",
                                        "feeCapOccurrence" : 0,
                                        "feeCapAmount" : "99999.99",
                                        "cappingPeriod" : "ACADEMICTERM",
                                        "notes" : [ 
                                            "string"
                                        ],
                                        "otherFeeType" : [ 
                                            {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "otherFeesCharges" : {
            "feeChargeDetail" : [ 
                {
                    "feeCategory" : "OTHER",
                    "feeType" : "SERVICECACCOUNTFEE",
                    "feeAmount" : "99999.99",
                    "feeRate" : "333.11",
                    "feeRateType" : "LINKEDBASERATE",
                    "applicationFrequency" : "ACCOUNTCLOSING",
                    "calculationFrequency" : "ACCOUNTCLOSING",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeCategoryType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeType" : {
                        "code" : "str4",
                        "feeCategory" : "OTHER",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeRateType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherApplicationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherCalculationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "feeChargeCap" : [ 
                        {
                            "feeType" : [ 
                                "SERVICECACCOUNTFEE"
                            ],
                            "minMaxType" : "MINIMUM",
                            "feeCapOccurrence" : 0,
                            "feeCapAmount" : "99999.99",
                            "cappingPeriod" : "ACADEMICTERM",
                            "notes" : [ 
                                "string"
                            ],
                            "otherFeeType" : [ 
                                {
                                    "code" : "str4",
                                    "name" : "string",
                                    "description" : "string"
                                }
                            ]
                        }
                    ],
                    "feeApplicableRange" : {
                        "minimumAmount" : "99999.99",
                        "maximumAmount" : "99999.99",
                        "minimumRate" : "333.33",
                        "maximumRate" : "333.33"
                    }
                }
            ],
            "feeChargeCap" : [ 
                {
                    "feeType" : [ 
                        "SERVICECACCOUNTFEE"
                    ],
                    "minMaxType" : "MINIMUM",
                    "feeCapOccurrence" : 0,
                    "feeCapAmount" : "99999.99",
                    "cappingPeriod" : "ACADEMICTERM",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeType" : [ 
                        {
                            "code" : "str4",
                            "name" : "string",
                            "description" : "string"
                        }
                    ]
                }
            ]
        }
    }
}

)


db.MockAccountProductCMA2.save(
{
		"psuId" : "32118465",
        "accountNumber": "23682779",
        "accountNSC": "901441",
        "productName" : "test Data test data",
        "productId" : "string",
        "secondaryProductId" : "string",
        "productType" : "BUSINESSCURRENTACCOUNT",
        "marketingStateId" : "string",
        "BCA" : {
                "productDetails" : {
                        "segment" : [
                                "CLIENTACCOUNT",
                                "STANDARD",
                                "NONCOMMERCIALCHAITIESCLBSOC",
                                "NONCOMMERCIALPUBLICAUTHGOVT",
                                "RELIGIOUS",
                                "SECTORSPECIFIC",
                                "STARTUP",
                                "SWITCHER"
                        ],
                        "feeFreeLength" : 10,
                        "feeFreeLengthPeriod" : "YEAR",
                        "notes" : [
                                "111111111"
                        ]
                },
                "creditInterest" : {
                        "tierBandSet" : [
                                {
                                        "tierBandMethod" : "WHOLE",
                                        "calculationMethod" : "SIMPLEINTEREST",
                                        "destination" : "SELFCREDIT",
                                        "notes" : [
                                                "111111111"
                                        ],
                                        "tierBand" : [
                                                {
                                                        "identification" : "qwertyuiopasdfghkjlzxcv",
                                                        "tierValueMinimum" : "-10000000000000.001",
                                                        "tierValueMaximum" : "-10000000000000.0",
                                                        "calculationFrequency" : "PERSTATEMENTDATE",
                                                        "applicationFrequency" : "OTHER",
                                                        "depositInterestAppliedCoverage" : "WHOLE",
                                                        "fixedVariableInterestRateType" : "VARIABLE",
                                                        "AER" : "334.3305",
                                                        "bankInterestRateType" : "OTHER",
                                                        "bankInterestRate" : "334.3305",
                                                        "notes" : [
                                                                "11111"
                                                        ],
                                                        "otherBankInterestType" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        },
                                                        "otherApplicationFrequency" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        },
                                                        "otherCalculationFrequency" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        }
                                                }
                                        ]
                                }
                        ]
                },
                "overdraft" : {
                        "notes" : [
                                "string"
                        ],
                        "overdraftTierBandSet" : [
                                {
                                        "tierBandMethod" : "BANDED",
                                        "overdraftType" : "COMMITTED",
                                        "identification" : "string",
                                        "authorisedIndicator" : true,
                                        "bufferAmount" : "99999.99",
                                        "notes" : [
                                                "string"
                                        ],
                                        "overdraftTierBand" : [
                                                {
                                                        "identification" : "string",
                                                        "tierValueMin" : "99999.99",
                                                        "tierValueMax" : "99999.99",
                                                        "EAR" : "333.33",
                                                        "agreementLengthMin" : 0,
                                                        "agreementLengthMax" : 0,
                                                        "agreementPeriod" : "DAY",
                                                        "overdraftInterestChargingCoverage" : "BANDED",
                                                        "bankGuaranteedIndicator" : true,
                                                        "notes" : [
                                                                "string"
                                                        ],
                                                        "overdraftFeesCharges" : [
                                                                {
                                                                        "overdraftFeeChargeCap" : [
                                                                                {
                                                                                        "feeType" : [
                                                                                                "ARRANGEDOVERDRAFT"
                                                                                        ],
                                                                                        "minMaxType" : "MINIMUM",
                                                                                        "feeCapOccurrence" : 0,
                                                                                        "feeCapAmount" : "99999.99",
                                                                                        "cappingPeriod" : "DAY",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "otherFeeType" : [
                                                                                                {
                                                                                                        "code" : "str4",
                                                                                                        "name" : "string",
                                                                                                        "description" : "string"
                                                                                                }
                                                                                        ]
                                                                                }
                                                                        ],
                                                                        "overdraftFeeChargeDetail" : [
                                                                                {
                                                                                        "feeType" : "ARRANGEDOVERDRAFT",
                                                                                        "negotiableIndicator" : true,
                                                                                        "overdraftControlIndicator" : true,
                                                                                        "incrementalBorrowingAmount" : "99999.99",
                                                                                        "feeAmount" : "99999.99",
                                                                                        "feeRate" : "333.11",
                                                                                        "feeRateType" : "GROSS",
                                                                                        "applicationFrequency" : "ONCLOSING",
                                                                                        "calculationFrequency" : "ONCLOSING",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "overdraftFeeChargeCap" : [
                                                                                                {
                                                                                                        "feeType" : [
                                                                                                                "ARRANGEDOVERDRAFT"
                                                                                                        ],
                                                                                                        "minMaxType" : "MINIMUM",
                                                                                                        "feeCapOccurrence" : 0,
                                                                                                        "feeCapAmount" : "99999.99",
                                                                                                        "cappingPeriod" : "DAY",
                                                                                                        "notes" : [
                                                                                                                "string"
                                                                                                        ],
                                                                                                        "otherFeeType" : [
                                                                                                                {
                                                                                                                        "code" : "str4",
                                                                                                                        "name" : "string",
                                                                                                                        "description" : "string"
                                                                                                                }
                                                                                                        ]
                                                                                                }
                                                                                        ],
                                                                                        "otherFeeType" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherFeeRateType" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherApplicationFrequency" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherCalculationFrequency" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        }
                                                                                }
                                                                        ]
                                                                }
                                                        ]
                                                }
                                        ],
                                        "overdraftFeesCharges" : [
                                                {
                                                        "overdraftFeeChargeCap" : [
                                                                {
                                                                        "feeType" : [
                                                                                "ARRANGEDOVERDRAFT"
                                                                        ],
                                                                        "minMaxType" : "MINIMUM",
                                                                        "feeCapOccurrence" : 0,
                                                                        "feeCapAmount" : "99999.99",
                                                                        "cappingPeriod" : "DAY",
                                                                        "notes" : [
                                                                                "string"
                                                                        ],
                                                                        "otherFeeType" : [
                                                                                {
                                                                                        "code" : "str4",
                                                                                        "name" : "string",
                                                                                        "description" : "string"
                                                                                }
                                                                        ]
                                                                }
                                                        ],
                                                        "overdraftFeeChargeDetail" : [
                                                                {
                                                                        "feeType" : "ARRANGEDOVERDRAFT",
                                                                        "negotiableIndicator" : true,
                                                                        "overdraftControlIndicator" : true,
                                                                        "incrementalBorrowingAmount" : "99999.99",
                                                                        "feeAmount" : "99999.99",
                                                                        "feeRate" : "333.11",
                                                                        "feeRateType" : "GROSS",
                                                                        "applicationFrequency" : "ONCLOSING",
                                                                        "calculationFrequency" : "ONCLOSING",
                                                                        "notes" : [
                                                                                "string"
                                                                        ],
                                                                        "overdraftFeeChargeCap" : [
                                                                                {
                                                                                        "feeType" : [
                                                                                                "ARRANGEDOVERDRAFT"
                                                                                        ],
                                                                                        "minMaxType" : "MINIMUM",
                                                                                        "feeCapOccurrence" : 0,
                                                                                        "feeCapAmount" : "99999.99",
                                                                                        "cappingPeriod" : "DAY",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "otherFeeType" : [
                                                                                                {
                                                                                                        "code" : "str4",
                                                                                                        "name" : "string",
                                                                                                        "description" : "string"
                                                                                                }
                                                                                        ]
                                                                                }
                                                                        ],
                                                                        "otherFeeType" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherFeeRateType" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherApplicationFrequency" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherCalculationFrequency" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        }
                                                                }
                                                        ]
                                                }
                                        ]
                                }
                        ]
                },
                "otherFeesCharges" : [
                        {
                                "tariffType" : "OTHER",
                                "tariffName" : "1111111",
                                "otherTariffType" : {
                                        "code" : "qwe2",
                                        "name" : "1111111111111111111111111111111111111111111111111111111111111111111qw",
                                        "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                },
                                "feeChargeDetail" : [
                                        {
                                                "feeCategory" : "SERVICING",
                                                "feeType" : "SERVICECOTHER",
                                                "negotiableIndicator" : true,
                                                "feeAmount" : "-10000000000000.0001",
                                                "feeRate" : "334.3305",
                                                "feeRateType" : "OTHER",
                                                "applicationFrequency" : "PERHUNDREDPOUNDS",
                                                "calculationFrequency" : "ONCLOSING",
                                                "notes" : [
                                                        "string"
                                                ],
                                                "feeChargeCap" : [
                                                        {
                                                                "feeType" : [
                                                                        "OTHER",
                                                                        "SERVICECACCOUNTFEE",
                                                                        "SERVICECACCOUNTFEEMONTHLY",
                                                                        "SERVICECACCOUNTFEEQUARTERLY",
                                                                        "SERVICECFIXEDTARIFF",
                                                                        "SERVICECBUSIDEPACCBREAKAGE",
                                                                        "SERVICECMINIMUMMONTHLYFEE",
                                                                        "SERVICECOTHER"
                                                                ],
                                                                "minMaxType" : "MAXIMUM",
                                                                "feeCapOccurrence" : 50,
                                                                "feeCapAmount" : "-10000000000000.0001",
                                                                "cappingPeriod" : "HALF_YEAR",
                                                                "notes" : [
                                                                        "string"
                                                                ],
                                                                "otherFeeType" : [
                                                                        {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        }
                                                                ]
                                                        }
                                                ],
                                                "otherFeeCategoryType" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherFeeType" : {
                                                        "code" : "str4",
                                                        "feeCategory" : "SERVICING",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherFeeRateType" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherApplicationFrequency" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherCalculationFrequency" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "feeApplicableRange" : {
                                                        "minimumAmount" : "-10000000000000.0001",
                                                        "maximumAmount" : "-10000000000000.0001",
                                                        "minimumRate" : "334.3305",
                                                        "maximumRate" : "334.3305"
                                                }
                                        }
                                ],
                                "feeChargeCap" : [
                                        {
                                                "feeType" : [
                                                        "OTHER",
                                                        "SERVICECACCOUNTFEE",
                                                        "SERVICECACCOUNTFEEMONTHLY",
                                                        "SERVICECACCOUNTFEEQUARTERLY",
                                                        "SERVICECFIXEDTARIFF",
                                                        "SERVICECBUSIDEPACCBREAKAGE",
                                                        "SERVICECMINIMUMMONTHLYFEE",
                                                        "SERVICECOTHER"
                                                ],
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "-10000000000000.0001",
                                                "cappingPeriod" : "DAY",
                                                "notes" : [
                                                        "string"
                                                ],
                                                "otherFeeType" : [
                                                        {
                                                                "code" : "str4",
                                                                "name" : "string",
                                                                "description" : "string"
                                                        }
                                                ]
                                        }
                                ]
                        }
                ]
        }
}
)

db.MockAccountProductCMA2.save(
{
		"psuId" : "32118465",
		"accountNumber": "25369421",
        "accountNSC": "903479",
		"productName" : "string",
		"productId" : "string",
		"secondaryProductId" : "string",
		"productType" : "OTHER",
		"marketingStateId" : "string",
		"otherProductType" : {
        "name" : "string",
        "description" : "string"
    }
}
)


db.MockAccountProductCMA2.save(
{
	"psuId" : "91807656",
    "accountNumber": "15369600",
    "accountNSC": "913789",
    "productName" : "string",
    "productId" : "string",
    "secondaryProductId" : "string",
    "productType" : "PERSONALCURRENTACCOUNT",
    "marketingStateId" : "string",
    "PCA" : {
        "productDetails" : {
            "segment" : [ 
                "BASIC"
            ],
            "monthlyMaximumCharge" : "99999.99",
            "notes" : [ 
                "string"
            ]
        },
        "creditInterest" : {
            "tierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "calculationMethod" : "COMPOUND",
                    "destination" : "PAYAWAY",
                    "notes" : [ 
                        "string"
                    ],
                    "tierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMinimum" : "99999.99",
                            "tierValueMaximum" : "99999.99",
                            "calculationFrequency" : "PERACADEMICTERM",
                            "applicationFrequency" : "PERACADEMICTERM",
                            "depositInterestAppliedCoverage" : "TIERED",
                            "fixedVariableInterestRateType" : "FIXED",
                            "AER" : "333.33",
                            "bankInterestRateType" : "LINKEDBASERATE",
                            "bankInterestRate" : "888.88",
                            "notes" : [ 
                                "string"
                            ],
                            "otherBankInterestType" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherApplicationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherCalculationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            }
                        }
                    ]
                }
            ]
        },
        "overdraft" : {
            "notes" : [ 
                "string"
            ],
            "overdraftTierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "overdraftType" : "COMMITTED",
                    "identification" : "string",
                    "authorisedIndicator" : true,
                    "bufferAmount" : "99999.99",
                    "notes" : [ 
                        "string"
                    ],
                    "overdraftTierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMin" : "99999.99",
                            "tierValueMax" : "99999.99",
                            "overdraftInterestChargingCoverage" : "TIERED",
                            "bankGuaranteedIndicator" : true,
                            "EAR" : "333.33",
                            "notes" : [ 
                                "string"
                            ],
                            "overdraftFeesCharges" : [ 
                                {
                                    "overdraftFeeChargeCap" : [ 
                                        {
                                            "feeType" : [ 
                                                "ARRANGEDOVERDRAFT"
                                            ],
                                            "overdraftControlIndicator" : true,
                                            "minMaxType" : "MINIMUM",
                                            "feeCapOccurrence" : 0,
                                            "feeCapAmount" : "99999.99",
                                            "cappingPeriod" : "ACADEMICTERM",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : [ 
                                                {
                                                    "code" : "str4",
                                                    "name" : "string",
                                                    "description" : "string"
                                                }
                                            ]
                                        }
                                    ],
                                    "overdraftFeeChargeDetail" : [ 
                                        {
                                            "feeType" : "ARRANGEDOVERDRAFT",
                                            "overdraftControlIndicator" : true,
                                            "incrementalBorrowingAmount" : "99999.99",
                                            "feeAmount" : "99999.99",
                                            "feeRate" : "333.11",
                                            "feeRateType" : "LINKEDBASERATE",
                                            "applicationFrequency" : "ACCOUNTCLOSING",
                                            "calculationFrequency" : "ACCOUNTCLOSING",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherFeeRateType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherApplicationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherCalculationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "overdraftFeeChargeCap" : {
                                                "feeType" : [ 
                                                    "ARRANGEDOVERDRAFT"
                                                ],
                                                "overdraftControlIndicator" : true,
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "99999.99",
                                                "cappingPeriod" : "ACADEMICTERM",
                                                "notes" : [ 
                                                    "string"
                                                ],
                                                "otherFeeType" : [ 
                                                    {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "overdraftFeesCharges" : [ 
                        {
                            "overdraftFeeChargeCap" : [ 
                                {
                                    "feeType" : [ 
                                        "ARRANGEDOVERDRAFT"
                                    ],
                                    "overdraftControlIndicator" : true,
                                    "minMaxType" : "MINIMUM",
                                    "feeCapOccurrence" : 0,
                                    "feeCapAmount" : "99999.99",
                                    "cappingPeriod" : "ACADEMICTERM",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : [ 
                                        {
                                            "code" : "str4",
                                            "name" : "string",
                                            "description" : "string"
                                        }
                                    ]
                                }
                            ],
                            "overdraftFeeChargeDetail" : [ 
                                {
                                    "feeType" : "ARRANGEDOVERDRAFT",
                                    "overdraftControlIndicator" : true,
                                    "incrementalBorrowingAmount" : "99999.99",
                                    "feeAmount" : "99999.99",
                                    "feeRate" : "333.11",
                                    "feeRateType" : "LINKEDBASERATE",
                                    "applicationFrequency" : "ACCOUNTCLOSING",
                                    "calculationFrequency" : "ACCOUNTCLOSING",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherFeeRateType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherApplicationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherCalculationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "overdraftFeeChargeCap" : {
                                        "feeType" : [ 
                                            "ARRANGEDOVERDRAFT"
                                        ],
                                        "overdraftControlIndicator" : true,
                                        "minMaxType" : "MINIMUM",
                                        "feeCapOccurrence" : 0,
                                        "feeCapAmount" : "99999.99",
                                        "cappingPeriod" : "ACADEMICTERM",
                                        "notes" : [ 
                                            "string"
                                        ],
                                        "otherFeeType" : [ 
                                            {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "otherFeesCharges" : {
            "feeChargeDetail" : [ 
                {
                    "feeCategory" : "OTHER",
                    "feeType" : "SERVICECACCOUNTFEE",
                    "feeAmount" : "99999.99",
                    "feeRate" : "333.11",
                    "feeRateType" : "LINKEDBASERATE",
                    "applicationFrequency" : "ACCOUNTCLOSING",
                    "calculationFrequency" : "ACCOUNTCLOSING",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeCategoryType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeType" : {
                        "code" : "str4",
                        "feeCategory" : "OTHER",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeRateType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherApplicationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherCalculationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "feeChargeCap" : [ 
                        {
                            "feeType" : [ 
                                "SERVICECACCOUNTFEE"
                            ],
                            "minMaxType" : "MINIMUM",
                            "feeCapOccurrence" : 0,
                            "feeCapAmount" : "99999.99",
                            "cappingPeriod" : "ACADEMICTERM",
                            "notes" : [ 
                                "string"
                            ],
                            "otherFeeType" : [ 
                                {
                                    "code" : "str4",
                                    "name" : "string",
                                    "description" : "string"
                                }
                            ]
                        }
                    ],
                    "feeApplicableRange" : {
                        "minimumAmount" : "99999.99",
                        "maximumAmount" : "99999.99",
                        "minimumRate" : "333.33",
                        "maximumRate" : "333.33"
                    }
                }
            ],
            "feeChargeCap" : [ 
                {
                    "feeType" : [ 
                        "SERVICECACCOUNTFEE"
                    ],
                    "minMaxType" : "MINIMUM",
                    "feeCapOccurrence" : 0,
                    "feeCapAmount" : "99999.99",
                    "cappingPeriod" : "ACADEMICTERM",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeType" : [ 
                        {
                            "code" : "str4",
                            "name" : "string",
                            "description" : "string"
                        }
                    ]
                }
            ]
        }
    }
}

)


db.MockAccountProductCMA2.save(
{
		"psuId" : "91807656",
        "accountNumber": "15369605",
        "accountNSC": "913105",
        "productName" : "test Data test data",
        "productId" : "string",
        "secondaryProductId" : "string",
        "productType" : "BUSINESSCURRENTACCOUNT",
        "marketingStateId" : "string",
        "BCA" : {
                "productDetails" : {
                        "segment" : [
                                "CLIENTACCOUNT",
                                "STANDARD",
                                "NONCOMMERCIALCHAITIESCLBSOC",
                                "NONCOMMERCIALPUBLICAUTHGOVT",
                                "RELIGIOUS",
                                "SECTORSPECIFIC",
                                "STARTUP",
                                "SWITCHER"
                        ],
                        "feeFreeLength" : 10,
                        "feeFreeLengthPeriod" : "YEAR",
                        "notes" : [
                                "111111111"
                        ]
                },
                "creditInterest" : {
                        "tierBandSet" : [
                                {
                                        "tierBandMethod" : "WHOLE",
                                        "calculationMethod" : "SIMPLEINTEREST",
                                        "destination" : "SELFCREDIT",
                                        "notes" : [
                                                "111111111"
                                        ],
                                        "tierBand" : [
                                                {
                                                        "identification" : "qwertyuiopasdfghkjlzxcv",
                                                        "tierValueMinimum" : "-10000000000000.001",
                                                        "tierValueMaximum" : "-10000000000000.0",
                                                        "calculationFrequency" : "PERSTATEMENTDATE",
                                                        "applicationFrequency" : "OTHER",
                                                        "depositInterestAppliedCoverage" : "WHOLE",
                                                        "fixedVariableInterestRateType" : "VARIABLE",
                                                        "AER" : "334.3305",
                                                        "bankInterestRateType" : "OTHER",
                                                        "bankInterestRate" : "334.3305",
                                                        "notes" : [
                                                                "11111"
                                                        ],
                                                        "otherBankInterestType" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        },
                                                        "otherApplicationFrequency" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        },
                                                        "otherCalculationFrequency" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        }
                                                }
                                        ]
                                }
                        ]
                },
                "overdraft" : {
                        "notes" : [
                                "string"
                        ],
                        "overdraftTierBandSet" : [
                                {
                                        "tierBandMethod" : "BANDED",
                                        "overdraftType" : "COMMITTED",
                                        "identification" : "string",
                                        "authorisedIndicator" : true,
                                        "bufferAmount" : "99999.99",
                                        "notes" : [
                                                "string"
                                        ],
                                        "overdraftTierBand" : [
                                                {
                                                        "identification" : "string",
                                                        "tierValueMin" : "99999.99",
                                                        "tierValueMax" : "99999.99",
                                                        "EAR" : "333.33",
                                                        "agreementLengthMin" : 0,
                                                        "agreementLengthMax" : 0,
                                                        "agreementPeriod" : "DAY",
                                                        "overdraftInterestChargingCoverage" : "BANDED",
                                                        "bankGuaranteedIndicator" : true,
                                                        "notes" : [
                                                                "string"
                                                        ],
                                                        "overdraftFeesCharges" : [
                                                                {
                                                                        "overdraftFeeChargeCap" : [
                                                                                {
                                                                                        "feeType" : [
                                                                                                "ARRANGEDOVERDRAFT"
                                                                                        ],
                                                                                        "minMaxType" : "MINIMUM",
                                                                                        "feeCapOccurrence" : 0,
                                                                                        "feeCapAmount" : "99999.99",
                                                                                        "cappingPeriod" : "DAY",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "otherFeeType" : [
                                                                                                {
                                                                                                        "code" : "str4",
                                                                                                        "name" : "string",
                                                                                                        "description" : "string"
                                                                                                }
                                                                                        ]
                                                                                }
                                                                        ],
                                                                        "overdraftFeeChargeDetail" : [
                                                                                {
                                                                                        "feeType" : "ARRANGEDOVERDRAFT",
                                                                                        "negotiableIndicator" : true,
                                                                                        "overdraftControlIndicator" : true,
                                                                                        "incrementalBorrowingAmount" : "99999.99",
                                                                                        "feeAmount" : "99999.99",
                                                                                        "feeRate" : "333.11",
                                                                                        "feeRateType" : "GROSS",
                                                                                        "applicationFrequency" : "ONCLOSING",
                                                                                        "calculationFrequency" : "ONCLOSING",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "overdraftFeeChargeCap" : [
                                                                                                {
                                                                                                        "feeType" : [
                                                                                                                "ARRANGEDOVERDRAFT"
                                                                                                        ],
                                                                                                        "minMaxType" : "MINIMUM",
                                                                                                        "feeCapOccurrence" : 0,
                                                                                                        "feeCapAmount" : "99999.99",
                                                                                                        "cappingPeriod" : "DAY",
                                                                                                        "notes" : [
                                                                                                                "string"
                                                                                                        ],
                                                                                                        "otherFeeType" : [
                                                                                                                {
                                                                                                                        "code" : "str4",
                                                                                                                        "name" : "string",
                                                                                                                        "description" : "string"
                                                                                                                }
                                                                                                        ]
                                                                                                }
                                                                                        ],
                                                                                        "otherFeeType" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherFeeRateType" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherApplicationFrequency" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherCalculationFrequency" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        }
                                                                                }
                                                                        ]
                                                                }
                                                        ]
                                                }
                                        ],
                                        "overdraftFeesCharges" : [
                                                {
                                                        "overdraftFeeChargeCap" : [
                                                                {
                                                                        "feeType" : [
                                                                                "ARRANGEDOVERDRAFT"
                                                                        ],
                                                                        "minMaxType" : "MINIMUM",
                                                                        "feeCapOccurrence" : 0,
                                                                        "feeCapAmount" : "99999.99",
                                                                        "cappingPeriod" : "DAY",
                                                                        "notes" : [
                                                                                "string"
                                                                        ],
                                                                        "otherFeeType" : [
                                                                                {
                                                                                        "code" : "str4",
                                                                                        "name" : "string",
                                                                                        "description" : "string"
                                                                                }
                                                                        ]
                                                                }
                                                        ],
                                                        "overdraftFeeChargeDetail" : [
                                                                {
                                                                        "feeType" : "ARRANGEDOVERDRAFT",
                                                                        "negotiableIndicator" : true,
                                                                        "overdraftControlIndicator" : true,
                                                                        "incrementalBorrowingAmount" : "99999.99",
                                                                        "feeAmount" : "99999.99",
                                                                        "feeRate" : "333.11",
                                                                        "feeRateType" : "GROSS",
                                                                        "applicationFrequency" : "ONCLOSING",
                                                                        "calculationFrequency" : "ONCLOSING",
                                                                        "notes" : [
                                                                                "string"
                                                                        ],
                                                                        "overdraftFeeChargeCap" : [
                                                                                {
                                                                                        "feeType" : [
                                                                                                "ARRANGEDOVERDRAFT"
                                                                                        ],
                                                                                        "minMaxType" : "MINIMUM",
                                                                                        "feeCapOccurrence" : 0,
                                                                                        "feeCapAmount" : "99999.99",
                                                                                        "cappingPeriod" : "DAY",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "otherFeeType" : [
                                                                                                {
                                                                                                        "code" : "str4",
                                                                                                        "name" : "string",
                                                                                                        "description" : "string"
                                                                                                }
                                                                                        ]
                                                                                }
                                                                        ],
                                                                        "otherFeeType" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherFeeRateType" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherApplicationFrequency" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherCalculationFrequency" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        }
                                                                }
                                                        ]
                                                }
                                        ]
                                }
                        ]
                },
                "otherFeesCharges" : [
                        {
                                "tariffType" : "OTHER",
                                "tariffName" : "1111111",
                                "otherTariffType" : {
                                        "code" : "qwe2",
                                        "name" : "1111111111111111111111111111111111111111111111111111111111111111111qw",
                                        "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                },
                                "feeChargeDetail" : [
                                        {
                                                "feeCategory" : "SERVICING",
                                                "feeType" : "SERVICECOTHER",
                                                "negotiableIndicator" : true,
                                                "feeAmount" : "-10000000000000.0001",
                                                "feeRate" : "334.3305",
                                                "feeRateType" : "OTHER",
                                                "applicationFrequency" : "PERHUNDREDPOUNDS",
                                                "calculationFrequency" : "ONCLOSING",
                                                "notes" : [
                                                        "string"
                                                ],
                                                "feeChargeCap" : [
                                                        {
                                                                "feeType" : [
                                                                        "OTHER",
                                                                        "SERVICECACCOUNTFEE",
                                                                        "SERVICECACCOUNTFEEMONTHLY",
                                                                        "SERVICECACCOUNTFEEQUARTERLY",
                                                                        "SERVICECFIXEDTARIFF",
                                                                        "SERVICECBUSIDEPACCBREAKAGE",
                                                                        "SERVICECMINIMUMMONTHLYFEE",
                                                                        "SERVICECOTHER"
                                                                ],
                                                                "minMaxType" : "MAXIMUM",
                                                                "feeCapOccurrence" : 50,
                                                                "feeCapAmount" : "-10000000000000.0001",
                                                                "cappingPeriod" : "HALF_YEAR",
                                                                "notes" : [
                                                                        "string"
                                                                ],
                                                                "otherFeeType" : [
                                                                        {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        }
                                                                ]
                                                        }
                                                ],
                                                "otherFeeCategoryType" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherFeeType" : {
                                                        "code" : "str4",
                                                        "feeCategory" : "SERVICING",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherFeeRateType" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherApplicationFrequency" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherCalculationFrequency" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "feeApplicableRange" : {
                                                        "minimumAmount" : "-10000000000000.0001",
                                                        "maximumAmount" : "-10000000000000.0001",
                                                        "minimumRate" : "334.3305",
                                                        "maximumRate" : "334.3305"
                                                }
                                        }
                                ],
                                "feeChargeCap" : [
                                        {
                                                "feeType" : [
                                                        "OTHER",
                                                        "SERVICECACCOUNTFEE",
                                                        "SERVICECACCOUNTFEEMONTHLY",
                                                        "SERVICECACCOUNTFEEQUARTERLY",
                                                        "SERVICECFIXEDTARIFF",
                                                        "SERVICECBUSIDEPACCBREAKAGE",
                                                        "SERVICECMINIMUMMONTHLYFEE",
                                                        "SERVICECOTHER"
                                                ],
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "-10000000000000.0001",
                                                "cappingPeriod" : "DAY",
                                                "notes" : [
                                                        "string"
                                                ],
                                                "otherFeeType" : [
                                                        {
                                                                "code" : "str4",
                                                                "name" : "string",
                                                                "description" : "string"
                                                        }
                                                ]
                                        }
                                ]
                        }
                ]
        }
}
)

db.MockAccountProductCMA2.save(
{
		"psuId" : "91807656",
		"accountNumber": "15369606",
        "accountNSC": "913106",
		"productName" : "string",
		"productId" : "string",
		"secondaryProductId" : "string",
		"productType" : "OTHER",
		"marketingStateId" : "string",
		"otherProductType" : {
        "name" : "string",
        "description" : "string"
    }
}
)


db.MockAccountProductCMA2.save(
{
	"psuId" : "89941690",
    "accountNumber": "47550843",
    "accountNSC": "901343",
    "productName" : "string",
    "productId" : "string",
    "secondaryProductId" : "string",
    "productType" : "PERSONALCURRENTACCOUNT",
    "marketingStateId" : "string",
    "PCA" : {
        "productDetails" : {
            "segment" : [ 
                "BASIC"
            ],
            "monthlyMaximumCharge" : "99999.99",
            "notes" : [ 
                "string"
            ]
        },
        "creditInterest" : {
            "tierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "calculationMethod" : "COMPOUND",
                    "destination" : "PAYAWAY",
                    "notes" : [ 
                        "string"
                    ],
                    "tierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMinimum" : "99999.99",
                            "tierValueMaximum" : "99999.99",
                            "calculationFrequency" : "PERACADEMICTERM",
                            "applicationFrequency" : "PERACADEMICTERM",
                            "depositInterestAppliedCoverage" : "TIERED",
                            "fixedVariableInterestRateType" : "FIXED",
                            "AER" : "333.33",
                            "bankInterestRateType" : "LINKEDBASERATE",
                            "bankInterestRate" : "888.88",
                            "notes" : [ 
                                "string"
                            ],
                            "otherBankInterestType" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherApplicationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherCalculationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            }
                        }
                    ]
                }
            ]
        },
        "overdraft" : {
            "notes" : [ 
                "string"
            ],
            "overdraftTierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "overdraftType" : "COMMITTED",
                    "identification" : "string",
                    "authorisedIndicator" : true,
                    "bufferAmount" : "99999.99",
                    "notes" : [ 
                        "string"
                    ],
                    "overdraftTierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMin" : "99999.99",
                            "tierValueMax" : "99999.99",
                            "overdraftInterestChargingCoverage" : "TIERED",
                            "bankGuaranteedIndicator" : true,
                            "EAR" : "333.33",
                            "notes" : [ 
                                "string"
                            ],
                            "overdraftFeesCharges" : [ 
                                {
                                    "overdraftFeeChargeCap" : [ 
                                        {
                                            "feeType" : [ 
                                                "ARRANGEDOVERDRAFT"
                                            ],
                                            "overdraftControlIndicator" : true,
                                            "minMaxType" : "MINIMUM",
                                            "feeCapOccurrence" : 0,
                                            "feeCapAmount" : "99999.99",
                                            "cappingPeriod" : "ACADEMICTERM",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : [ 
                                                {
                                                    "code" : "str4",
                                                    "name" : "string",
                                                    "description" : "string"
                                                }
                                            ]
                                        }
                                    ],
                                    "overdraftFeeChargeDetail" : [ 
                                        {
                                            "feeType" : "ARRANGEDOVERDRAFT",
                                            "overdraftControlIndicator" : true,
                                            "incrementalBorrowingAmount" : "99999.99",
                                            "feeAmount" : "99999.99",
                                            "feeRate" : "333.11",
                                            "feeRateType" : "LINKEDBASERATE",
                                            "applicationFrequency" : "ACCOUNTCLOSING",
                                            "calculationFrequency" : "ACCOUNTCLOSING",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherFeeRateType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherApplicationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherCalculationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "overdraftFeeChargeCap" : {
                                                "feeType" : [ 
                                                    "ARRANGEDOVERDRAFT"
                                                ],
                                                "overdraftControlIndicator" : true,
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "99999.99",
                                                "cappingPeriod" : "ACADEMICTERM",
                                                "notes" : [ 
                                                    "string"
                                                ],
                                                "otherFeeType" : [ 
                                                    {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "overdraftFeesCharges" : [ 
                        {
                            "overdraftFeeChargeCap" : [ 
                                {
                                    "feeType" : [ 
                                        "ARRANGEDOVERDRAFT"
                                    ],
                                    "overdraftControlIndicator" : true,
                                    "minMaxType" : "MINIMUM",
                                    "feeCapOccurrence" : 0,
                                    "feeCapAmount" : "99999.99",
                                    "cappingPeriod" : "ACADEMICTERM",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : [ 
                                        {
                                            "code" : "str4",
                                            "name" : "string",
                                            "description" : "string"
                                        }
                                    ]
                                }
                            ],
                            "overdraftFeeChargeDetail" : [ 
                                {
                                    "feeType" : "ARRANGEDOVERDRAFT",
                                    "overdraftControlIndicator" : true,
                                    "incrementalBorrowingAmount" : "99999.99",
                                    "feeAmount" : "99999.99",
                                    "feeRate" : "333.11",
                                    "feeRateType" : "LINKEDBASERATE",
                                    "applicationFrequency" : "ACCOUNTCLOSING",
                                    "calculationFrequency" : "ACCOUNTCLOSING",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherFeeRateType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherApplicationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherCalculationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "overdraftFeeChargeCap" : {
                                        "feeType" : [ 
                                            "ARRANGEDOVERDRAFT"
                                        ],
                                        "overdraftControlIndicator" : true,
                                        "minMaxType" : "MINIMUM",
                                        "feeCapOccurrence" : 0,
                                        "feeCapAmount" : "99999.99",
                                        "cappingPeriod" : "ACADEMICTERM",
                                        "notes" : [ 
                                            "string"
                                        ],
                                        "otherFeeType" : [ 
                                            {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "otherFeesCharges" : {
            "feeChargeDetail" : [ 
                {
                    "feeCategory" : "OTHER",
                    "feeType" : "SERVICECACCOUNTFEE",
                    "feeAmount" : "99999.99",
                    "feeRate" : "333.11",
                    "feeRateType" : "LINKEDBASERATE",
                    "applicationFrequency" : "ACCOUNTCLOSING",
                    "calculationFrequency" : "ACCOUNTCLOSING",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeCategoryType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeType" : {
                        "code" : "str4",
                        "feeCategory" : "OTHER",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeRateType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherApplicationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherCalculationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "feeChargeCap" : [ 
                        {
                            "feeType" : [ 
                                "SERVICECACCOUNTFEE"
                            ],
                            "minMaxType" : "MINIMUM",
                            "feeCapOccurrence" : 0,
                            "feeCapAmount" : "99999.99",
                            "cappingPeriod" : "ACADEMICTERM",
                            "notes" : [ 
                                "string"
                            ],
                            "otherFeeType" : [ 
                                {
                                    "code" : "str4",
                                    "name" : "string",
                                    "description" : "string"
                                }
                            ]
                        }
                    ],
                    "feeApplicableRange" : {
                        "minimumAmount" : "99999.99",
                        "maximumAmount" : "99999.99",
                        "minimumRate" : "333.33",
                        "maximumRate" : "333.33"
                    }
                }
            ],
            "feeChargeCap" : [ 
                {
                    "feeType" : [ 
                        "SERVICECACCOUNTFEE"
                    ],
                    "minMaxType" : "MINIMUM",
                    "feeCapOccurrence" : 0,
                    "feeCapAmount" : "99999.99",
                    "cappingPeriod" : "ACADEMICTERM",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeType" : [ 
                        {
                            "code" : "str4",
                            "name" : "string",
                            "description" : "string"
                        }
                    ]
                }
            ]
        }
    }
}

)


db.MockAccountProductCMA2.save(
{
		"psuId" : "89941690",
        "accountNumber": "23682877",
        "accountNSC": "903101",
        "productName" : "test Data test data",
        "productId" : "string",
        "secondaryProductId" : "string",
        "productType" : "BUSINESSCURRENTACCOUNT",
        "marketingStateId" : "string",
        "BCA" : {
                "productDetails" : {
                        "segment" : [
                                "CLIENTACCOUNT",
                                "STANDARD",
                                "NONCOMMERCIALCHAITIESCLBSOC",
                                "NONCOMMERCIALPUBLICAUTHGOVT",
                                "RELIGIOUS",
                                "SECTORSPECIFIC",
                                "STARTUP",
                                "SWITCHER"
                        ],
                        "feeFreeLength" : 10,
                        "feeFreeLengthPeriod" : "YEAR",
                        "notes" : [
                                "111111111"
                        ]
                },
                "creditInterest" : {
                        "tierBandSet" : [
                                {
                                        "tierBandMethod" : "WHOLE",
                                        "calculationMethod" : "SIMPLEINTEREST",
                                        "destination" : "SELFCREDIT",
                                        "notes" : [
                                                "111111111"
                                        ],
                                        "tierBand" : [
                                                {
                                                        "identification" : "qwertyuiopasdfghkjlzxcv",
                                                        "tierValueMinimum" : "-10000000000000.001",
                                                        "tierValueMaximum" : "-10000000000000.0",
                                                        "calculationFrequency" : "PERSTATEMENTDATE",
                                                        "applicationFrequency" : "OTHER",
                                                        "depositInterestAppliedCoverage" : "WHOLE",
                                                        "fixedVariableInterestRateType" : "VARIABLE",
                                                        "AER" : "334.3305",
                                                        "bankInterestRateType" : "OTHER",
                                                        "bankInterestRate" : "334.3305",
                                                        "notes" : [
                                                                "11111"
                                                        ],
                                                        "otherBankInterestType" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        },
                                                        "otherApplicationFrequency" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        },
                                                        "otherCalculationFrequency" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        }
                                                }
                                        ]
                                }
                        ]
                },
                "overdraft" : {
                        "notes" : [
                                "string"
                        ],
                        "overdraftTierBandSet" : [
                                {
                                        "tierBandMethod" : "BANDED",
                                        "overdraftType" : "COMMITTED",
                                        "identification" : "string",
                                        "authorisedIndicator" : true,
                                        "bufferAmount" : "99999.99",
                                        "notes" : [
                                                "string"
                                        ],
                                        "overdraftTierBand" : [
                                                {
                                                        "identification" : "string",
                                                        "tierValueMin" : "99999.99",
                                                        "tierValueMax" : "99999.99",
                                                        "EAR" : "333.33",
                                                        "agreementLengthMin" : 0,
                                                        "agreementLengthMax" : 0,
                                                        "agreementPeriod" : "DAY",
                                                        "overdraftInterestChargingCoverage" : "BANDED",
                                                        "bankGuaranteedIndicator" : true,
                                                        "notes" : [
                                                                "string"
                                                        ],
                                                        "overdraftFeesCharges" : [
                                                                {
                                                                        "overdraftFeeChargeCap" : [
                                                                                {
                                                                                        "feeType" : [
                                                                                                "ARRANGEDOVERDRAFT"
                                                                                        ],
                                                                                        "minMaxType" : "MINIMUM",
                                                                                        "feeCapOccurrence" : 0,
                                                                                        "feeCapAmount" : "99999.99",
                                                                                        "cappingPeriod" : "DAY",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "otherFeeType" : [
                                                                                                {
                                                                                                        "code" : "str4",
                                                                                                        "name" : "string",
                                                                                                        "description" : "string"
                                                                                                }
                                                                                        ]
                                                                                }
                                                                        ],
                                                                        "overdraftFeeChargeDetail" : [
                                                                                {
                                                                                        "feeType" : "ARRANGEDOVERDRAFT",
                                                                                        "negotiableIndicator" : true,
                                                                                        "overdraftControlIndicator" : true,
                                                                                        "incrementalBorrowingAmount" : "99999.99",
                                                                                        "feeAmount" : "99999.99",
                                                                                        "feeRate" : "333.11",
                                                                                        "feeRateType" : "GROSS",
                                                                                        "applicationFrequency" : "ONCLOSING",
                                                                                        "calculationFrequency" : "ONCLOSING",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "overdraftFeeChargeCap" : [
                                                                                                {
                                                                                                        "feeType" : [
                                                                                                                "ARRANGEDOVERDRAFT"
                                                                                                        ],
                                                                                                        "minMaxType" : "MINIMUM",
                                                                                                        "feeCapOccurrence" : 0,
                                                                                                        "feeCapAmount" : "99999.99",
                                                                                                        "cappingPeriod" : "DAY",
                                                                                                        "notes" : [
                                                                                                                "string"
                                                                                                        ],
                                                                                                        "otherFeeType" : [
                                                                                                                {
                                                                                                                        "code" : "str4",
                                                                                                                        "name" : "string",
                                                                                                                        "description" : "string"
                                                                                                                }
                                                                                                        ]
                                                                                                }
                                                                                        ],
                                                                                        "otherFeeType" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherFeeRateType" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherApplicationFrequency" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherCalculationFrequency" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        }
                                                                                }
                                                                        ]
                                                                }
                                                        ]
                                                }
                                        ],
                                        "overdraftFeesCharges" : [
                                                {
                                                        "overdraftFeeChargeCap" : [
                                                                {
                                                                        "feeType" : [
                                                                                "ARRANGEDOVERDRAFT"
                                                                        ],
                                                                        "minMaxType" : "MINIMUM",
                                                                        "feeCapOccurrence" : 0,
                                                                        "feeCapAmount" : "99999.99",
                                                                        "cappingPeriod" : "DAY",
                                                                        "notes" : [
                                                                                "string"
                                                                        ],
                                                                        "otherFeeType" : [
                                                                                {
                                                                                        "code" : "str4",
                                                                                        "name" : "string",
                                                                                        "description" : "string"
                                                                                }
                                                                        ]
                                                                }
                                                        ],
                                                        "overdraftFeeChargeDetail" : [
                                                                {
                                                                        "feeType" : "ARRANGEDOVERDRAFT",
                                                                        "negotiableIndicator" : true,
                                                                        "overdraftControlIndicator" : true,
                                                                        "incrementalBorrowingAmount" : "99999.99",
                                                                        "feeAmount" : "99999.99",
                                                                        "feeRate" : "333.11",
                                                                        "feeRateType" : "GROSS",
                                                                        "applicationFrequency" : "ONCLOSING",
                                                                        "calculationFrequency" : "ONCLOSING",
                                                                        "notes" : [
                                                                                "string"
                                                                        ],
                                                                        "overdraftFeeChargeCap" : [
                                                                                {
                                                                                        "feeType" : [
                                                                                                "ARRANGEDOVERDRAFT"
                                                                                        ],
                                                                                        "minMaxType" : "MINIMUM",
                                                                                        "feeCapOccurrence" : 0,
                                                                                        "feeCapAmount" : "99999.99",
                                                                                        "cappingPeriod" : "DAY",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "otherFeeType" : [
                                                                                                {
                                                                                                        "code" : "str4",
                                                                                                        "name" : "string",
                                                                                                        "description" : "string"
                                                                                                }
                                                                                        ]
                                                                                }
                                                                        ],
                                                                        "otherFeeType" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherFeeRateType" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherApplicationFrequency" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherCalculationFrequency" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        }
                                                                }
                                                        ]
                                                }
                                        ]
                                }
                        ]
                },
                "otherFeesCharges" : [
                        {
                                "tariffType" : "OTHER",
                                "tariffName" : "1111111",
                                "otherTariffType" : {
                                        "code" : "qwe2",
                                        "name" : "1111111111111111111111111111111111111111111111111111111111111111111qw",
                                        "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                },
                                "feeChargeDetail" : [
                                        {
                                                "feeCategory" : "SERVICING",
                                                "feeType" : "SERVICECOTHER",
                                                "negotiableIndicator" : true,
                                                "feeAmount" : "-10000000000000.0001",
                                                "feeRate" : "334.3305",
                                                "feeRateType" : "OTHER",
                                                "applicationFrequency" : "PERHUNDREDPOUNDS",
                                                "calculationFrequency" : "ONCLOSING",
                                                "notes" : [
                                                        "string"
                                                ],
                                                "feeChargeCap" : [
                                                        {
                                                                "feeType" : [
                                                                        "OTHER",
                                                                        "SERVICECACCOUNTFEE",
                                                                        "SERVICECACCOUNTFEEMONTHLY",
                                                                        "SERVICECACCOUNTFEEQUARTERLY",
                                                                        "SERVICECFIXEDTARIFF",
                                                                        "SERVICECBUSIDEPACCBREAKAGE",
                                                                        "SERVICECMINIMUMMONTHLYFEE",
                                                                        "SERVICECOTHER"
                                                                ],
                                                                "minMaxType" : "MAXIMUM",
                                                                "feeCapOccurrence" : 50,
                                                                "feeCapAmount" : "-10000000000000.0001",
                                                                "cappingPeriod" : "HALF_YEAR",
                                                                "notes" : [
                                                                        "string"
                                                                ],
                                                                "otherFeeType" : [
                                                                        {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        }
                                                                ]
                                                        }
                                                ],
                                                "otherFeeCategoryType" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherFeeType" : {
                                                        "code" : "str4",
                                                        "feeCategory" : "SERVICING",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherFeeRateType" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherApplicationFrequency" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherCalculationFrequency" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "feeApplicableRange" : {
                                                        "minimumAmount" : "-10000000000000.0001",
                                                        "maximumAmount" : "-10000000000000.0001",
                                                        "minimumRate" : "334.3305",
                                                        "maximumRate" : "334.3305"
                                                }
                                        }
                                ],
                                "feeChargeCap" : [
                                        {
                                                "feeType" : [
                                                        "OTHER",
                                                        "SERVICECACCOUNTFEE",
                                                        "SERVICECACCOUNTFEEMONTHLY",
                                                        "SERVICECACCOUNTFEEQUARTERLY",
                                                        "SERVICECFIXEDTARIFF",
                                                        "SERVICECBUSIDEPACCBREAKAGE",
                                                        "SERVICECMINIMUMMONTHLYFEE",
                                                        "SERVICECOTHER"
                                                ],
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "-10000000000000.0001",
                                                "cappingPeriod" : "DAY",
                                                "notes" : [
                                                        "string"
                                                ],
                                                "otherFeeType" : [
                                                        {
                                                                "code" : "str4",
                                                                "name" : "string",
                                                                "description" : "string"
                                                        }
                                                ]
                                        }
                                ]
                        }
                ]
        }
}
)

db.MockAccountProductCMA2.save(
{
		"psuId" : "89941690",
		"accountNumber": "95369699",
        "accountNSC": "903479",
		"productName" : "string",
		"productId" : "string",
		"secondaryProductId" : "string",
		"productType" : "OTHER",
		"marketingStateId" : "string",
		"otherProductType" : {
        "name" : "string",
        "description" : "string"
    }
}
)


db.MockAccountProductCMA2.save(
{
	"psuId" : "47508347",
    "accountNumber": "23681001",
    "accountNSC": "901441",
    "productName" : "string",
    "productId" : "string",
    "secondaryProductId" : "string",
    "productType" : "PERSONALCURRENTACCOUNT",
    "marketingStateId" : "string",
    "PCA" : {
        "productDetails" : {
            "segment" : [ 
                "BASIC"
            ],
            "monthlyMaximumCharge" : "99999.99",
            "notes" : [ 
                "string"
            ]
        },
        "creditInterest" : {
            "tierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "calculationMethod" : "COMPOUND",
                    "destination" : "PAYAWAY",
                    "notes" : [ 
                        "string"
                    ],
                    "tierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMinimum" : "99999.99",
                            "tierValueMaximum" : "99999.99",
                            "calculationFrequency" : "PERACADEMICTERM",
                            "applicationFrequency" : "PERACADEMICTERM",
                            "depositInterestAppliedCoverage" : "TIERED",
                            "fixedVariableInterestRateType" : "FIXED",
                            "AER" : "333.33",
                            "bankInterestRateType" : "LINKEDBASERATE",
                            "bankInterestRate" : "888.88",
                            "notes" : [ 
                                "string"
                            ],
                            "otherBankInterestType" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherApplicationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherCalculationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            }
                        }
                    ]
                }
            ]
        },
        "overdraft" : {
            "notes" : [ 
                "string"
            ],
            "overdraftTierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "overdraftType" : "COMMITTED",
                    "identification" : "string",
                    "authorisedIndicator" : true,
                    "bufferAmount" : "99999.99",
                    "notes" : [ 
                        "string"
                    ],
                    "overdraftTierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMin" : "99999.99",
                            "tierValueMax" : "99999.99",
                            "overdraftInterestChargingCoverage" : "TIERED",
                            "bankGuaranteedIndicator" : true,
                            "EAR" : "333.33",
                            "notes" : [ 
                                "string"
                            ],
                            "overdraftFeesCharges" : [ 
                                {
                                    "overdraftFeeChargeCap" : [ 
                                        {
                                            "feeType" : [ 
                                                "ARRANGEDOVERDRAFT"
                                            ],
                                            "overdraftControlIndicator" : true,
                                            "minMaxType" : "MINIMUM",
                                            "feeCapOccurrence" : 0,
                                            "feeCapAmount" : "99999.99",
                                            "cappingPeriod" : "ACADEMICTERM",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : [ 
                                                {
                                                    "code" : "str4",
                                                    "name" : "string",
                                                    "description" : "string"
                                                }
                                            ]
                                        }
                                    ],
                                    "overdraftFeeChargeDetail" : [ 
                                        {
                                            "feeType" : "ARRANGEDOVERDRAFT",
                                            "overdraftControlIndicator" : true,
                                            "incrementalBorrowingAmount" : "99999.99",
                                            "feeAmount" : "99999.99",
                                            "feeRate" : "333.11",
                                            "feeRateType" : "LINKEDBASERATE",
                                            "applicationFrequency" : "ACCOUNTCLOSING",
                                            "calculationFrequency" : "ACCOUNTCLOSING",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherFeeRateType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherApplicationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherCalculationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "overdraftFeeChargeCap" : {
                                                "feeType" : [ 
                                                    "ARRANGEDOVERDRAFT"
                                                ],
                                                "overdraftControlIndicator" : true,
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "99999.99",
                                                "cappingPeriod" : "ACADEMICTERM",
                                                "notes" : [ 
                                                    "string"
                                                ],
                                                "otherFeeType" : [ 
                                                    {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "overdraftFeesCharges" : [ 
                        {
                            "overdraftFeeChargeCap" : [ 
                                {
                                    "feeType" : [ 
                                        "ARRANGEDOVERDRAFT"
                                    ],
                                    "overdraftControlIndicator" : true,
                                    "minMaxType" : "MINIMUM",
                                    "feeCapOccurrence" : 0,
                                    "feeCapAmount" : "99999.99",
                                    "cappingPeriod" : "ACADEMICTERM",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : [ 
                                        {
                                            "code" : "str4",
                                            "name" : "string",
                                            "description" : "string"
                                        }
                                    ]
                                }
                            ],
                            "overdraftFeeChargeDetail" : [ 
                                {
                                    "feeType" : "ARRANGEDOVERDRAFT",
                                    "overdraftControlIndicator" : true,
                                    "incrementalBorrowingAmount" : "99999.99",
                                    "feeAmount" : "99999.99",
                                    "feeRate" : "333.11",
                                    "feeRateType" : "LINKEDBASERATE",
                                    "applicationFrequency" : "ACCOUNTCLOSING",
                                    "calculationFrequency" : "ACCOUNTCLOSING",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherFeeRateType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherApplicationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherCalculationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "overdraftFeeChargeCap" : {
                                        "feeType" : [ 
                                            "ARRANGEDOVERDRAFT"
                                        ],
                                        "overdraftControlIndicator" : true,
                                        "minMaxType" : "MINIMUM",
                                        "feeCapOccurrence" : 0,
                                        "feeCapAmount" : "99999.99",
                                        "cappingPeriod" : "ACADEMICTERM",
                                        "notes" : [ 
                                            "string"
                                        ],
                                        "otherFeeType" : [ 
                                            {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "otherFeesCharges" : {
            "feeChargeDetail" : [ 
                {
                    "feeCategory" : "OTHER",
                    "feeType" : "SERVICECACCOUNTFEE",
                    "feeAmount" : "99999.99",
                    "feeRate" : "333.11",
                    "feeRateType" : "LINKEDBASERATE",
                    "applicationFrequency" : "ACCOUNTCLOSING",
                    "calculationFrequency" : "ACCOUNTCLOSING",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeCategoryType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeType" : {
                        "code" : "str4",
                        "feeCategory" : "OTHER",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeRateType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherApplicationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherCalculationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "feeChargeCap" : [ 
                        {
                            "feeType" : [ 
                                "SERVICECACCOUNTFEE"
                            ],
                            "minMaxType" : "MINIMUM",
                            "feeCapOccurrence" : 0,
                            "feeCapAmount" : "99999.99",
                            "cappingPeriod" : "ACADEMICTERM",
                            "notes" : [ 
                                "string"
                            ],
                            "otherFeeType" : [ 
                                {
                                    "code" : "str4",
                                    "name" : "string",
                                    "description" : "string"
                                }
                            ]
                        }
                    ],
                    "feeApplicableRange" : {
                        "minimumAmount" : "99999.99",
                        "maximumAmount" : "99999.99",
                        "minimumRate" : "333.33",
                        "maximumRate" : "333.33"
                    }
                }
            ],
            "feeChargeCap" : [ 
                {
                    "feeType" : [ 
                        "SERVICECACCOUNTFEE"
                    ],
                    "minMaxType" : "MINIMUM",
                    "feeCapOccurrence" : 0,
                    "feeCapAmount" : "99999.99",
                    "cappingPeriod" : "ACADEMICTERM",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeType" : [ 
                        {
                            "code" : "str4",
                            "name" : "string",
                            "description" : "string"
                        }
                    ]
                }
            ]
        }
    }
}

)


db.MockAccountProductCMA2.save(
{
		"psuId" : "47508347",
        "accountNumber": "23681002",
        "accountNSC": "901441",
        "productName" : "test Data test data",
        "productId" : "string",
        "secondaryProductId" : "string",
        "productType" : "BUSINESSCURRENTACCOUNT",
        "marketingStateId" : "string",
        "BCA" : {
                "productDetails" : {
                        "segment" : [
                                "CLIENTACCOUNT",
                                "STANDARD",
                                "NONCOMMERCIALCHAITIESCLBSOC",
                                "NONCOMMERCIALPUBLICAUTHGOVT",
                                "RELIGIOUS",
                                "SECTORSPECIFIC",
                                "STARTUP",
                                "SWITCHER"
                        ],
                        "feeFreeLength" : 10,
                        "feeFreeLengthPeriod" : "YEAR",
                        "notes" : [
                                "111111111"
                        ]
                },
                "creditInterest" : {
                        "tierBandSet" : [
                                {
                                        "tierBandMethod" : "WHOLE",
                                        "calculationMethod" : "SIMPLEINTEREST",
                                        "destination" : "SELFCREDIT",
                                        "notes" : [
                                                "111111111"
                                        ],
                                        "tierBand" : [
                                                {
                                                        "identification" : "qwertyuiopasdfghkjlzxcv",
                                                        "tierValueMinimum" : "-10000000000000.001",
                                                        "tierValueMaximum" : "-10000000000000.0",
                                                        "calculationFrequency" : "PERSTATEMENTDATE",
                                                        "applicationFrequency" : "OTHER",
                                                        "depositInterestAppliedCoverage" : "WHOLE",
                                                        "fixedVariableInterestRateType" : "VARIABLE",
                                                        "AER" : "334.3305",
                                                        "bankInterestRateType" : "OTHER",
                                                        "bankInterestRate" : "334.3305",
                                                        "notes" : [
                                                                "11111"
                                                        ],
                                                        "otherBankInterestType" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        },
                                                        "otherApplicationFrequency" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        },
                                                        "otherCalculationFrequency" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        }
                                                }
                                        ]
                                }
                        ]
                },
                "overdraft" : {
                        "notes" : [
                                "string"
                        ],
                        "overdraftTierBandSet" : [
                                {
                                        "tierBandMethod" : "BANDED",
                                        "overdraftType" : "COMMITTED",
                                        "identification" : "string",
                                        "authorisedIndicator" : true,
                                        "bufferAmount" : "99999.99",
                                        "notes" : [
                                                "string"
                                        ],
                                        "overdraftTierBand" : [
                                                {
                                                        "identification" : "string",
                                                        "tierValueMin" : "99999.99",
                                                        "tierValueMax" : "99999.99",
                                                        "EAR" : "333.33",
                                                        "agreementLengthMin" : 0,
                                                        "agreementLengthMax" : 0,
                                                        "agreementPeriod" : "DAY",
                                                        "overdraftInterestChargingCoverage" : "BANDED",
                                                        "bankGuaranteedIndicator" : true,
                                                        "notes" : [
                                                                "string"
                                                        ],
                                                        "overdraftFeesCharges" : [
                                                                {
                                                                        "overdraftFeeChargeCap" : [
                                                                                {
                                                                                        "feeType" : [
                                                                                                "ARRANGEDOVERDRAFT"
                                                                                        ],
                                                                                        "minMaxType" : "MINIMUM",
                                                                                        "feeCapOccurrence" : 0,
                                                                                        "feeCapAmount" : "99999.99",
                                                                                        "cappingPeriod" : "DAY",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "otherFeeType" : [
                                                                                                {
                                                                                                        "code" : "str4",
                                                                                                        "name" : "string",
                                                                                                        "description" : "string"
                                                                                                }
                                                                                        ]
                                                                                }
                                                                        ],
                                                                        "overdraftFeeChargeDetail" : [
                                                                                {
                                                                                        "feeType" : "ARRANGEDOVERDRAFT",
                                                                                        "negotiableIndicator" : true,
                                                                                        "overdraftControlIndicator" : true,
                                                                                        "incrementalBorrowingAmount" : "99999.99",
                                                                                        "feeAmount" : "99999.99",
                                                                                        "feeRate" : "333.11",
                                                                                        "feeRateType" : "GROSS",
                                                                                        "applicationFrequency" : "ONCLOSING",
                                                                                        "calculationFrequency" : "ONCLOSING",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "overdraftFeeChargeCap" : [
                                                                                                {
                                                                                                        "feeType" : [
                                                                                                                "ARRANGEDOVERDRAFT"
                                                                                                        ],
                                                                                                        "minMaxType" : "MINIMUM",
                                                                                                        "feeCapOccurrence" : 0,
                                                                                                        "feeCapAmount" : "99999.99",
                                                                                                        "cappingPeriod" : "DAY",
                                                                                                        "notes" : [
                                                                                                                "string"
                                                                                                        ],
                                                                                                        "otherFeeType" : [
                                                                                                                {
                                                                                                                        "code" : "str4",
                                                                                                                        "name" : "string",
                                                                                                                        "description" : "string"
                                                                                                                }
                                                                                                        ]
                                                                                                }
                                                                                        ],
                                                                                        "otherFeeType" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherFeeRateType" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherApplicationFrequency" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherCalculationFrequency" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        }
                                                                                }
                                                                        ]
                                                                }
                                                        ]
                                                }
                                        ],
                                        "overdraftFeesCharges" : [
                                                {
                                                        "overdraftFeeChargeCap" : [
                                                                {
                                                                        "feeType" : [
                                                                                "ARRANGEDOVERDRAFT"
                                                                        ],
                                                                        "minMaxType" : "MINIMUM",
                                                                        "feeCapOccurrence" : 0,
                                                                        "feeCapAmount" : "99999.99",
                                                                        "cappingPeriod" : "DAY",
                                                                        "notes" : [
                                                                                "string"
                                                                        ],
                                                                        "otherFeeType" : [
                                                                                {
                                                                                        "code" : "str4",
                                                                                        "name" : "string",
                                                                                        "description" : "string"
                                                                                }
                                                                        ]
                                                                }
                                                        ],
                                                        "overdraftFeeChargeDetail" : [
                                                                {
                                                                        "feeType" : "ARRANGEDOVERDRAFT",
                                                                        "negotiableIndicator" : true,
                                                                        "overdraftControlIndicator" : true,
                                                                        "incrementalBorrowingAmount" : "99999.99",
                                                                        "feeAmount" : "99999.99",
                                                                        "feeRate" : "333.11",
                                                                        "feeRateType" : "GROSS",
                                                                        "applicationFrequency" : "ONCLOSING",
                                                                        "calculationFrequency" : "ONCLOSING",
                                                                        "notes" : [
                                                                                "string"
                                                                        ],
                                                                        "overdraftFeeChargeCap" : [
                                                                                {
                                                                                        "feeType" : [
                                                                                                "ARRANGEDOVERDRAFT"
                                                                                        ],
                                                                                        "minMaxType" : "MINIMUM",
                                                                                        "feeCapOccurrence" : 0,
                                                                                        "feeCapAmount" : "99999.99",
                                                                                        "cappingPeriod" : "DAY",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "otherFeeType" : [
                                                                                                {
                                                                                                        "code" : "str4",
                                                                                                        "name" : "string",
                                                                                                        "description" : "string"
                                                                                                }
                                                                                        ]
                                                                                }
                                                                        ],
                                                                        "otherFeeType" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherFeeRateType" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherApplicationFrequency" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherCalculationFrequency" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        }
                                                                }
                                                        ]
                                                }
                                        ]
                                }
                        ]
                },
                "otherFeesCharges" : [
                        {
                                "tariffType" : "OTHER",
                                "tariffName" : "1111111",
                                "otherTariffType" : {
                                        "code" : "qwe2",
                                        "name" : "1111111111111111111111111111111111111111111111111111111111111111111qw",
                                        "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                },
                                "feeChargeDetail" : [
                                        {
                                                "feeCategory" : "SERVICING",
                                                "feeType" : "SERVICECOTHER",
                                                "negotiableIndicator" : true,
                                                "feeAmount" : "-10000000000000.0001",
                                                "feeRate" : "334.3305",
                                                "feeRateType" : "OTHER",
                                                "applicationFrequency" : "PERHUNDREDPOUNDS",
                                                "calculationFrequency" : "ONCLOSING",
                                                "notes" : [
                                                        "string"
                                                ],
                                                "feeChargeCap" : [
                                                        {
                                                                "feeType" : [
                                                                        "OTHER",
                                                                        "SERVICECACCOUNTFEE",
                                                                        "SERVICECACCOUNTFEEMONTHLY",
                                                                        "SERVICECACCOUNTFEEQUARTERLY",
                                                                        "SERVICECFIXEDTARIFF",
                                                                        "SERVICECBUSIDEPACCBREAKAGE",
                                                                        "SERVICECMINIMUMMONTHLYFEE",
                                                                        "SERVICECOTHER"
                                                                ],
                                                                "minMaxType" : "MAXIMUM",
                                                                "feeCapOccurrence" : 50,
                                                                "feeCapAmount" : "-10000000000000.0001",
                                                                "cappingPeriod" : "HALF_YEAR",
                                                                "notes" : [
                                                                        "string"
                                                                ],
                                                                "otherFeeType" : [
                                                                        {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        }
                                                                ]
                                                        }
                                                ],
                                                "otherFeeCategoryType" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherFeeType" : {
                                                        "code" : "str4",
                                                        "feeCategory" : "SERVICING",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherFeeRateType" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherApplicationFrequency" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherCalculationFrequency" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "feeApplicableRange" : {
                                                        "minimumAmount" : "-10000000000000.0001",
                                                        "maximumAmount" : "-10000000000000.0001",
                                                        "minimumRate" : "334.3305",
                                                        "maximumRate" : "334.3305"
                                                }
                                        }
                                ],
                                "feeChargeCap" : [
                                        {
                                                "feeType" : [
                                                        "OTHER",
                                                        "SERVICECACCOUNTFEE",
                                                        "SERVICECACCOUNTFEEMONTHLY",
                                                        "SERVICECACCOUNTFEEQUARTERLY",
                                                        "SERVICECFIXEDTARIFF",
                                                        "SERVICECBUSIDEPACCBREAKAGE",
                                                        "SERVICECMINIMUMMONTHLYFEE",
                                                        "SERVICECOTHER"
                                                ],
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "-10000000000000.0001",
                                                "cappingPeriod" : "DAY",
                                                "notes" : [
                                                        "string"
                                                ],
                                                "otherFeeType" : [
                                                        {
                                                                "code" : "str4",
                                                                "name" : "string",
                                                                "description" : "string"
                                                        }
                                                ]
                                        }
                                ]
                        }
                ]
        }
}
)

db.MockAccountProductCMA2.save(
{
		"psuId" : "47508347",
		"accountNumber": "53111155",
        "accountNSC": "901441",
		"productName" : "string",
		"productId" : "string",
		"secondaryProductId" : "string",
		"productType" : "OTHER",
		"marketingStateId" : "string",
		"otherProductType" : {
        "name" : "string",
        "description" : "string"
    }
}
)

db.MockAccountProductCMA2.save(
{
	"psuId" : "10546298",
    "accountNumber": "23781190",
    "accountNSC": "931152",
    "productName" : "string",
    "productId" : "string",
    "secondaryProductId" : "string",
    "productType" : "PERSONALCURRENTACCOUNT",
    "marketingStateId" : "string",
    "PCA" : {
        "productDetails" : {
            "segment" : [ 
                "BASIC"
            ],
            "monthlyMaximumCharge" : "99999.99",
            "notes" : [ 
                "string"
            ]
        },
        "creditInterest" : {
            "tierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "calculationMethod" : "COMPOUND",
                    "destination" : "PAYAWAY",
                    "notes" : [ 
                        "string"
                    ],
                    "tierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMinimum" : "99999.99",
                            "tierValueMaximum" : "99999.99",
                            "calculationFrequency" : "PERACADEMICTERM",
                            "applicationFrequency" : "PERACADEMICTERM",
                            "depositInterestAppliedCoverage" : "TIERED",
                            "fixedVariableInterestRateType" : "FIXED",
                            "AER" : "333.33",
                            "bankInterestRateType" : "LINKEDBASERATE",
                            "bankInterestRate" : "888.88",
                            "notes" : [ 
                                "string"
                            ],
                            "otherBankInterestType" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherApplicationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherCalculationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            }
                        }
                    ]
                }
            ]
        },
        "overdraft" : {
            "notes" : [ 
                "string"
            ],
            "overdraftTierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "overdraftType" : "COMMITTED",
                    "identification" : "string",
                    "authorisedIndicator" : true,
                    "bufferAmount" : "99999.99",
                    "notes" : [ 
                        "string"
                    ],
                    "overdraftTierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMin" : "99999.99",
                            "tierValueMax" : "99999.99",
                            "overdraftInterestChargingCoverage" : "TIERED",
                            "bankGuaranteedIndicator" : true,
                            "EAR" : "333.33",
                            "notes" : [ 
                                "string"
                            ],
                            "overdraftFeesCharges" : [ 
                                {
                                    "overdraftFeeChargeCap" : [ 
                                        {
                                            "feeType" : [ 
                                                "ARRANGEDOVERDRAFT"
                                            ],
                                            "overdraftControlIndicator" : true,
                                            "minMaxType" : "MINIMUM",
                                            "feeCapOccurrence" : 0,
                                            "feeCapAmount" : "99999.99",
                                            "cappingPeriod" : "ACADEMICTERM",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : [ 
                                                {
                                                    "code" : "str4",
                                                    "name" : "string",
                                                    "description" : "string"
                                                }
                                            ]
                                        }
                                    ],
                                    "overdraftFeeChargeDetail" : [ 
                                        {
                                            "feeType" : "ARRANGEDOVERDRAFT",
                                            "overdraftControlIndicator" : true,
                                            "incrementalBorrowingAmount" : "99999.99",
                                            "feeAmount" : "99999.99",
                                            "feeRate" : "333.11",
                                            "feeRateType" : "LINKEDBASERATE",
                                            "applicationFrequency" : "ACCOUNTCLOSING",
                                            "calculationFrequency" : "ACCOUNTCLOSING",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherFeeRateType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherApplicationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherCalculationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "overdraftFeeChargeCap" : {
                                                "feeType" : [ 
                                                    "ARRANGEDOVERDRAFT"
                                                ],
                                                "overdraftControlIndicator" : true,
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "99999.99",
                                                "cappingPeriod" : "ACADEMICTERM",
                                                "notes" : [ 
                                                    "string"
                                                ],
                                                "otherFeeType" : [ 
                                                    {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "overdraftFeesCharges" : [ 
                        {
                            "overdraftFeeChargeCap" : [ 
                                {
                                    "feeType" : [ 
                                        "ARRANGEDOVERDRAFT"
                                    ],
                                    "overdraftControlIndicator" : true,
                                    "minMaxType" : "MINIMUM",
                                    "feeCapOccurrence" : 0,
                                    "feeCapAmount" : "99999.99",
                                    "cappingPeriod" : "ACADEMICTERM",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : [ 
                                        {
                                            "code" : "str4",
                                            "name" : "string",
                                            "description" : "string"
                                        }
                                    ]
                                }
                            ],
                            "overdraftFeeChargeDetail" : [ 
                                {
                                    "feeType" : "ARRANGEDOVERDRAFT",
                                    "overdraftControlIndicator" : true,
                                    "incrementalBorrowingAmount" : "99999.99",
                                    "feeAmount" : "99999.99",
                                    "feeRate" : "333.11",
                                    "feeRateType" : "LINKEDBASERATE",
                                    "applicationFrequency" : "ACCOUNTCLOSING",
                                    "calculationFrequency" : "ACCOUNTCLOSING",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherFeeRateType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherApplicationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherCalculationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "overdraftFeeChargeCap" : {
                                        "feeType" : [ 
                                            "ARRANGEDOVERDRAFT"
                                        ],
                                        "overdraftControlIndicator" : true,
                                        "minMaxType" : "MINIMUM",
                                        "feeCapOccurrence" : 0,
                                        "feeCapAmount" : "99999.99",
                                        "cappingPeriod" : "ACADEMICTERM",
                                        "notes" : [ 
                                            "string"
                                        ],
                                        "otherFeeType" : [ 
                                            {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "otherFeesCharges" : {
            "feeChargeDetail" : [ 
                {
                    "feeCategory" : "OTHER",
                    "feeType" : "SERVICECACCOUNTFEE",
                    "feeAmount" : "99999.99",
                    "feeRate" : "333.11",
                    "feeRateType" : "LINKEDBASERATE",
                    "applicationFrequency" : "ACCOUNTCLOSING",
                    "calculationFrequency" : "ACCOUNTCLOSING",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeCategoryType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeType" : {
                        "code" : "str4",
                        "feeCategory" : "OTHER",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeRateType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherApplicationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherCalculationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "feeChargeCap" : [ 
                        {
                            "feeType" : [ 
                                "SERVICECACCOUNTFEE"
                            ],
                            "minMaxType" : "MINIMUM",
                            "feeCapOccurrence" : 0,
                            "feeCapAmount" : "99999.99",
                            "cappingPeriod" : "ACADEMICTERM",
                            "notes" : [ 
                                "string"
                            ],
                            "otherFeeType" : [ 
                                {
                                    "code" : "str4",
                                    "name" : "string",
                                    "description" : "string"
                                }
                            ]
                        }
                    ],
                    "feeApplicableRange" : {
                        "minimumAmount" : "99999.99",
                        "maximumAmount" : "99999.99",
                        "minimumRate" : "333.33",
                        "maximumRate" : "333.33"
                    }
                }
            ],
            "feeChargeCap" : [ 
                {
                    "feeType" : [ 
                        "SERVICECACCOUNTFEE"
                    ],
                    "minMaxType" : "MINIMUM",
                    "feeCapOccurrence" : 0,
                    "feeCapAmount" : "99999.99",
                    "cappingPeriod" : "ACADEMICTERM",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeType" : [ 
                        {
                            "code" : "str4",
                            "name" : "string",
                            "description" : "string"
                        }
                    ]
                }
            ]
        }
    }
}

)

db.MockAccountProductCMA2.save(
{
	"psuId" : "18070751",
    "accountNumber": "12345678",
    "accountNSC": "931152",
    "productName" : "string",
    "productId" : "string",
    "secondaryProductId" : "string",
    "productType" : "PERSONALCURRENTACCOUNT",
    "marketingStateId" : "string",
    "PCA" : {
        "productDetails" : {
            "segment" : [ 
                "BASIC"
            ],
            "monthlyMaximumCharge" : "99999.99",
            "notes" : [ 
                "string"
            ]
        },
        "creditInterest" : {
            "tierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "calculationMethod" : "COMPOUND",
                    "destination" : "PAYAWAY",
                    "notes" : [ 
                        "string"
                    ],
                    "tierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMinimum" : "99999.99",
                            "tierValueMaximum" : "99999.99",
                            "calculationFrequency" : "PERACADEMICTERM",
                            "applicationFrequency" : "PERACADEMICTERM",
                            "depositInterestAppliedCoverage" : "TIERED",
                            "fixedVariableInterestRateType" : "FIXED",
                            "AER" : "333.33",
                            "bankInterestRateType" : "LINKEDBASERATE",
                            "bankInterestRate" : "888.88",
                            "notes" : [ 
                                "string"
                            ],
                            "otherBankInterestType" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherApplicationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            },
                            "otherCalculationFrequency" : {
                                "code" : "str4",
                                "name" : "string",
                                "description" : "string"
                            }
                        }
                    ]
                }
            ]
        },
        "overdraft" : {
            "notes" : [ 
                "string"
            ],
            "overdraftTierBandSet" : [ 
                {
                    "tierBandMethod" : "TIERED",
                    "overdraftType" : "COMMITTED",
                    "identification" : "string",
                    "authorisedIndicator" : true,
                    "bufferAmount" : "99999.99",
                    "notes" : [ 
                        "string"
                    ],
                    "overdraftTierBand" : [ 
                        {
                            "identification" : "string",
                            "tierValueMin" : "99999.99",
                            "tierValueMax" : "99999.99",
                            "overdraftInterestChargingCoverage" : "TIERED",
                            "bankGuaranteedIndicator" : true,
                            "EAR" : "333.33",
                            "notes" : [ 
                                "string"
                            ],
                            "overdraftFeesCharges" : [ 
                                {
                                    "overdraftFeeChargeCap" : [ 
                                        {
                                            "feeType" : [ 
                                                "ARRANGEDOVERDRAFT"
                                            ],
                                            "overdraftControlIndicator" : true,
                                            "minMaxType" : "MINIMUM",
                                            "feeCapOccurrence" : 0,
                                            "feeCapAmount" : "99999.99",
                                            "cappingPeriod" : "ACADEMICTERM",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : [ 
                                                {
                                                    "code" : "str4",
                                                    "name" : "string",
                                                    "description" : "string"
                                                }
                                            ]
                                        }
                                    ],
                                    "overdraftFeeChargeDetail" : [ 
                                        {
                                            "feeType" : "ARRANGEDOVERDRAFT",
                                            "overdraftControlIndicator" : true,
                                            "incrementalBorrowingAmount" : "99999.99",
                                            "feeAmount" : "99999.99",
                                            "feeRate" : "333.11",
                                            "feeRateType" : "LINKEDBASERATE",
                                            "applicationFrequency" : "ACCOUNTCLOSING",
                                            "calculationFrequency" : "ACCOUNTCLOSING",
                                            "notes" : [ 
                                                "string"
                                            ],
                                            "otherFeeType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherFeeRateType" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherApplicationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "otherCalculationFrequency" : {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            },
                                            "overdraftFeeChargeCap" : {
                                                "feeType" : [ 
                                                    "ARRANGEDOVERDRAFT"
                                                ],
                                                "overdraftControlIndicator" : true,
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "99999.99",
                                                "cappingPeriod" : "ACADEMICTERM",
                                                "notes" : [ 
                                                    "string"
                                                ],
                                                "otherFeeType" : [ 
                                                    {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "overdraftFeesCharges" : [ 
                        {
                            "overdraftFeeChargeCap" : [ 
                                {
                                    "feeType" : [ 
                                        "ARRANGEDOVERDRAFT"
                                    ],
                                    "overdraftControlIndicator" : true,
                                    "minMaxType" : "MINIMUM",
                                    "feeCapOccurrence" : 0,
                                    "feeCapAmount" : "99999.99",
                                    "cappingPeriod" : "ACADEMICTERM",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : [ 
                                        {
                                            "code" : "str4",
                                            "name" : "string",
                                            "description" : "string"
                                        }
                                    ]
                                }
                            ],
                            "overdraftFeeChargeDetail" : [ 
                                {
                                    "feeType" : "ARRANGEDOVERDRAFT",
                                    "overdraftControlIndicator" : true,
                                    "incrementalBorrowingAmount" : "99999.99",
                                    "feeAmount" : "99999.99",
                                    "feeRate" : "333.11",
                                    "feeRateType" : "LINKEDBASERATE",
                                    "applicationFrequency" : "ACCOUNTCLOSING",
                                    "calculationFrequency" : "ACCOUNTCLOSING",
                                    "notes" : [ 
                                        "string"
                                    ],
                                    "otherFeeType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherFeeRateType" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherApplicationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "otherCalculationFrequency" : {
                                        "code" : "str4",
                                        "name" : "string",
                                        "description" : "string"
                                    },
                                    "overdraftFeeChargeCap" : {
                                        "feeType" : [ 
                                            "ARRANGEDOVERDRAFT"
                                        ],
                                        "overdraftControlIndicator" : true,
                                        "minMaxType" : "MINIMUM",
                                        "feeCapOccurrence" : 0,
                                        "feeCapAmount" : "99999.99",
                                        "cappingPeriod" : "ACADEMICTERM",
                                        "notes" : [ 
                                            "string"
                                        ],
                                        "otherFeeType" : [ 
                                            {
                                                "code" : "str4",
                                                "name" : "string",
                                                "description" : "string"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "otherFeesCharges" : {
            "feeChargeDetail" : [ 
                {
                    "feeCategory" : "OTHER",
                    "feeType" : "SERVICECACCOUNTFEE",
                    "feeAmount" : "99999.99",
                    "feeRate" : "333.11",
                    "feeRateType" : "LINKEDBASERATE",
                    "applicationFrequency" : "ACCOUNTCLOSING",
                    "calculationFrequency" : "ACCOUNTCLOSING",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeCategoryType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeType" : {
                        "code" : "str4",
                        "feeCategory" : "OTHER",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherFeeRateType" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherApplicationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "otherCalculationFrequency" : {
                        "code" : "str4",
                        "name" : "string",
                        "description" : "string"
                    },
                    "feeChargeCap" : [ 
                        {
                            "feeType" : [ 
                                "SERVICECACCOUNTFEE"
                            ],
                            "minMaxType" : "MINIMUM",
                            "feeCapOccurrence" : 0,
                            "feeCapAmount" : "99999.99",
                            "cappingPeriod" : "ACADEMICTERM",
                            "notes" : [ 
                                "string"
                            ],
                            "otherFeeType" : [ 
                                {
                                    "code" : "str4",
                                    "name" : "string",
                                    "description" : "string"
                                }
                            ]
                        }
                    ],
                    "feeApplicableRange" : {
                        "minimumAmount" : "99999.99",
                        "maximumAmount" : "99999.99",
                        "minimumRate" : "333.33",
                        "maximumRate" : "333.33"
                    }
                }
            ],
            "feeChargeCap" : [ 
                {
                    "feeType" : [ 
                        "SERVICECACCOUNTFEE"
                    ],
                    "minMaxType" : "MINIMUM",
                    "feeCapOccurrence" : 0,
                    "feeCapAmount" : "99999.99",
                    "cappingPeriod" : "ACADEMICTERM",
                    "notes" : [ 
                        "string"
                    ],
                    "otherFeeType" : [ 
                        {
                            "code" : "str4",
                            "name" : "string",
                            "description" : "string"
                        }
                    ]
                }
            ]
        }
    }
}

)

db.MockAccountProductCMA2.save(
{
		"psuId" : "70613119",
        "accountNumber": "15369608",
        "accountNSC": "913108",
		"productName" : "string",
		"productId" : "string",
		"secondaryProductId" : "string",
		"productType" : "OTHER",
		"marketingStateId" : "string",
		"otherProductType" : {
        "name" : "string",
        "description" : "string"
    }
}
)

db.MockAccountProductCMA2.save(
{
		"psuId" : "34080157",
        "accountNumber": "15369611",
        "accountNSC": "913111",
        "productName" : "test Data test data",
        "productId" : "string",
        "secondaryProductId" : "string",
        "productType" : "BUSINESSCURRENTACCOUNT",
        "marketingStateId" : "string",
        "BCA" : {
                "productDetails" : {
                        "segment" : [
                                "CLIENTACCOUNT",
                                "STANDARD",
                                "NONCOMMERCIALCHAITIESCLBSOC",
                                "NONCOMMERCIALPUBLICAUTHGOVT",
                                "RELIGIOUS",
                                "SECTORSPECIFIC",
                                "STARTUP",
                                "SWITCHER"
                        ],
                        "feeFreeLength" : 10,
                        "feeFreeLengthPeriod" : "YEAR",
                        "notes" : [
                                "111111111"
                        ]
                },
                "creditInterest" : {
                        "tierBandSet" : [
                                {
                                        "tierBandMethod" : "WHOLE",
                                        "calculationMethod" : "SIMPLEINTEREST",
                                        "destination" : "SELFCREDIT",
                                        "notes" : [
                                                "111111111"
                                        ],
                                        "tierBand" : [
                                                {
                                                        "identification" : "qwertyuiopasdfghkjlzxcv",
                                                        "tierValueMinimum" : "-10000000000000.001",
                                                        "tierValueMaximum" : "-10000000000000.0",
                                                        "calculationFrequency" : "PERSTATEMENTDATE",
                                                        "applicationFrequency" : "OTHER",
                                                        "depositInterestAppliedCoverage" : "WHOLE",
                                                        "fixedVariableInterestRateType" : "VARIABLE",
                                                        "AER" : "334.3305",
                                                        "bankInterestRateType" : "OTHER",
                                                        "bankInterestRate" : "334.3305",
                                                        "notes" : [
                                                                "11111"
                                                        ],
                                                        "otherBankInterestType" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        },
                                                        "otherApplicationFrequency" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        },
                                                        "otherCalculationFrequency" : {
                                                                "code" : "123e",
                                                                "name" : "111111111111111111111111111111111111111111111111111111111111111111111",
                                                                "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                                        }
                                                }
                                        ]
                                }
                        ]
                },
                "overdraft" : {
                        "notes" : [
                                "string"
                        ],
                        "overdraftTierBandSet" : [
                                {
                                        "tierBandMethod" : "BANDED",
                                        "overdraftType" : "COMMITTED",
                                        "identification" : "string",
                                        "authorisedIndicator" : true,
                                        "bufferAmount" : "99999.99",
                                        "notes" : [
                                                "string"
                                        ],
                                        "overdraftTierBand" : [
                                                {
                                                        "identification" : "string",
                                                        "tierValueMin" : "99999.99",
                                                        "tierValueMax" : "99999.99",
                                                        "EAR" : "333.33",
                                                        "agreementLengthMin" : 0,
                                                        "agreementLengthMax" : 0,
                                                        "agreementPeriod" : "DAY",
                                                        "overdraftInterestChargingCoverage" : "BANDED",
                                                        "bankGuaranteedIndicator" : true,
                                                        "notes" : [
                                                                "string"
                                                        ],
                                                        "overdraftFeesCharges" : [
                                                                {
                                                                        "overdraftFeeChargeCap" : [
                                                                                {
                                                                                        "feeType" : [
                                                                                                "ARRANGEDOVERDRAFT"
                                                                                        ],
                                                                                        "minMaxType" : "MINIMUM",
                                                                                        "feeCapOccurrence" : 0,
                                                                                        "feeCapAmount" : "99999.99",
                                                                                        "cappingPeriod" : "DAY",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "otherFeeType" : [
                                                                                                {
                                                                                                        "code" : "str4",
                                                                                                        "name" : "string",
                                                                                                        "description" : "string"
                                                                                                }
                                                                                        ]
                                                                                }
                                                                        ],
                                                                        "overdraftFeeChargeDetail" : [
                                                                                {
                                                                                        "feeType" : "ARRANGEDOVERDRAFT",
                                                                                        "negotiableIndicator" : true,
                                                                                        "overdraftControlIndicator" : true,
                                                                                        "incrementalBorrowingAmount" : "99999.99",
                                                                                        "feeAmount" : "99999.99",
                                                                                        "feeRate" : "333.11",
                                                                                        "feeRateType" : "GROSS",
                                                                                        "applicationFrequency" : "ONCLOSING",
                                                                                        "calculationFrequency" : "ONCLOSING",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "overdraftFeeChargeCap" : [
                                                                                                {
                                                                                                        "feeType" : [
                                                                                                                "ARRANGEDOVERDRAFT"
                                                                                                        ],
                                                                                                        "minMaxType" : "MINIMUM",
                                                                                                        "feeCapOccurrence" : 0,
                                                                                                        "feeCapAmount" : "99999.99",
                                                                                                        "cappingPeriod" : "DAY",
                                                                                                        "notes" : [
                                                                                                                "string"
                                                                                                        ],
                                                                                                        "otherFeeType" : [
                                                                                                                {
                                                                                                                        "code" : "str4",
                                                                                                                        "name" : "string",
                                                                                                                        "description" : "string"
                                                                                                                }
                                                                                                        ]
                                                                                                }
                                                                                        ],
                                                                                        "otherFeeType" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherFeeRateType" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherApplicationFrequency" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        },
                                                                                        "otherCalculationFrequency" : {
                                                                                                "code" : "str4",
                                                                                                "name" : "string",
                                                                                                "description" : "string"
                                                                                        }
                                                                                }
                                                                        ]
                                                                }
                                                        ]
                                                }
                                        ],
                                        "overdraftFeesCharges" : [
                                                {
                                                        "overdraftFeeChargeCap" : [
                                                                {
                                                                        "feeType" : [
                                                                                "ARRANGEDOVERDRAFT"
                                                                        ],
                                                                        "minMaxType" : "MINIMUM",
                                                                        "feeCapOccurrence" : 0,
                                                                        "feeCapAmount" : "99999.99",
                                                                        "cappingPeriod" : "DAY",
                                                                        "notes" : [
                                                                                "string"
                                                                        ],
                                                                        "otherFeeType" : [
                                                                                {
                                                                                        "code" : "str4",
                                                                                        "name" : "string",
                                                                                        "description" : "string"
                                                                                }
                                                                        ]
                                                                }
                                                        ],
                                                        "overdraftFeeChargeDetail" : [
                                                                {
                                                                        "feeType" : "ARRANGEDOVERDRAFT",
                                                                        "negotiableIndicator" : true,
                                                                        "overdraftControlIndicator" : true,
                                                                        "incrementalBorrowingAmount" : "99999.99",
                                                                        "feeAmount" : "99999.99",
                                                                        "feeRate" : "333.11",
                                                                        "feeRateType" : "GROSS",
                                                                        "applicationFrequency" : "ONCLOSING",
                                                                        "calculationFrequency" : "ONCLOSING",
                                                                        "notes" : [
                                                                                "string"
                                                                        ],
                                                                        "overdraftFeeChargeCap" : [
                                                                                {
                                                                                        "feeType" : [
                                                                                                "ARRANGEDOVERDRAFT"
                                                                                        ],
                                                                                        "minMaxType" : "MINIMUM",
                                                                                        "feeCapOccurrence" : 0,
                                                                                        "feeCapAmount" : "99999.99",
                                                                                        "cappingPeriod" : "DAY",
                                                                                        "notes" : [
                                                                                                "string"
                                                                                        ],
                                                                                        "otherFeeType" : [
                                                                                                {
                                                                                                        "code" : "str4",
                                                                                                        "name" : "string",
                                                                                                        "description" : "string"
                                                                                                }
                                                                                        ]
                                                                                }
                                                                        ],
                                                                        "otherFeeType" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherFeeRateType" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherApplicationFrequency" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        },
                                                                        "otherCalculationFrequency" : {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        }
                                                                }
                                                        ]
                                                }
                                        ]
                                }
                        ]
                },
                "otherFeesCharges" : [
                        {
                                "tariffType" : "OTHER",
                                "tariffName" : "1111111",
                                "otherTariffType" : {
                                        "code" : "qwe2",
                                        "name" : "1111111111111111111111111111111111111111111111111111111111111111111qw",
                                        "description" : "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
                                },
                                "feeChargeDetail" : [
                                        {
                                                "feeCategory" : "SERVICING",
                                                "feeType" : "SERVICECOTHER",
                                                "negotiableIndicator" : true,
                                                "feeAmount" : "-10000000000000.0001",
                                                "feeRate" : "334.3305",
                                                "feeRateType" : "OTHER",
                                                "applicationFrequency" : "PERHUNDREDPOUNDS",
                                                "calculationFrequency" : "ONCLOSING",
                                                "notes" : [
                                                        "string"
                                                ],
                                                "feeChargeCap" : [
                                                        {
                                                                "feeType" : [
                                                                        "OTHER",
                                                                        "SERVICECACCOUNTFEE",
                                                                        "SERVICECACCOUNTFEEMONTHLY",
                                                                        "SERVICECACCOUNTFEEQUARTERLY",
                                                                        "SERVICECFIXEDTARIFF",
                                                                        "SERVICECBUSIDEPACCBREAKAGE",
                                                                        "SERVICECMINIMUMMONTHLYFEE",
                                                                        "SERVICECOTHER"
                                                                ],
                                                                "minMaxType" : "MAXIMUM",
                                                                "feeCapOccurrence" : 50,
                                                                "feeCapAmount" : "-10000000000000.0001",
                                                                "cappingPeriod" : "HALF_YEAR",
                                                                "notes" : [
                                                                        "string"
                                                                ],
                                                                "otherFeeType" : [
                                                                        {
                                                                                "code" : "str4",
                                                                                "name" : "string",
                                                                                "description" : "string"
                                                                        }
                                                                ]
                                                        }
                                                ],
                                                "otherFeeCategoryType" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherFeeType" : {
                                                        "code" : "str4",
                                                        "feeCategory" : "SERVICING",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherFeeRateType" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherApplicationFrequency" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "otherCalculationFrequency" : {
                                                        "code" : "str4",
                                                        "name" : "string",
                                                        "description" : "string"
                                                },
                                                "feeApplicableRange" : {
                                                        "minimumAmount" : "-10000000000000.0001",
                                                        "maximumAmount" : "-10000000000000.0001",
                                                        "minimumRate" : "334.3305",
                                                        "maximumRate" : "334.3305"
                                                }
                                        }
                                ],
                                "feeChargeCap" : [
                                        {
                                                "feeType" : [
                                                        "OTHER",
                                                        "SERVICECACCOUNTFEE",
                                                        "SERVICECACCOUNTFEEMONTHLY",
                                                        "SERVICECACCOUNTFEEQUARTERLY",
                                                        "SERVICECFIXEDTARIFF",
                                                        "SERVICECBUSIDEPACCBREAKAGE",
                                                        "SERVICECMINIMUMMONTHLYFEE",
                                                        "SERVICECOTHER"
                                                ],
                                                "minMaxType" : "MINIMUM",
                                                "feeCapOccurrence" : 0,
                                                "feeCapAmount" : "-10000000000000.0001",
                                                "cappingPeriod" : "DAY",
                                                "notes" : [
                                                        "string"
                                                ],
                                                "otherFeeType" : [
                                                        {
                                                                "code" : "str4",
                                                                "name" : "string",
                                                                "description" : "string"
                                                        }
                                                ]
                                        }
                                ]
                        }
                ]
        }
}
)
#!/bin/bash
#
# Copyright 2015-2016 UnboundID Corp.
#
# Executes the Self-Service Account Manager application installer.  Run the
# script without any arguments to display the help, and refer to the
# documentation for additional information.
#
username='ApiAdmin'
password='Test@2017'
credentials="$(echo -n "$username:$password" | base64)"
echo $credentials
header="Authorization: Basic $credentials"

#sed -i 's:'$credentials':credentials:g' pf_OauthClient_DELETE.sh
#sed -i 's:'federationserver-test.private.apibank-cma2plus.in':URL:g' pf_OauthClient_DELETE.sh

#sed -i 's:'$credentials':credentials:g' pf_OauthClient_Create.sh
#sed -i 's:'federationserver-test.private.apibank-cma2plus.in':URL:g' pf_OauthClient_Create.sh

sed -i 's:credentials:'$credentials':g' pf_OauthClient_DELETE.sh
sed -i 's:URL:'federationserver-test.private.apibank-cma2plus.in':g' pf_OauthClient_DELETE.sh

sed -i 's:credentials:'$credentials':g' pf_OauthClient_Create.sh
sed -i 's:URL:'federationserver-test.private.apibank-cma2plus.in':g' pf_OauthClient_Create.sh


#!/bin/bash
#
# Copyright 2015-2016 UnboundID Corp.
#
# Executes the Self-Service Account Manager application installer.  Run the
# script without any arguments to display the help, and refer to the
# documentation for additional information.
#

curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "43fb5e4c-b22d-46da-acfa-0793a6d1713c",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP AISP Client-1",
  "description": "Mock TPP AISP Client-1",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
    "accounts"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "vK3qpZjzf8KUUdvT5qkWjvgR97szienLAYLHJP6h6hp0Wv6aCJMss1m33zzYXCEO",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"72867005\", \"e\": \"AQAB\",   \"n\": \"nV3-prkf3-YXf4hUxjGC0Fi2F2K_6s0uVBQ56PVP5CXilVfbM8zg7CM5VGqRInQNzNsiplpvLuEgJ8tu6M1_KtumN8qO-RzPMmuLKpaafFOxs6NwlFpZq4oLdBIjNhYfAnzeJY2jD9yImMFBvfHcAXf3MHbOplucv6qLSE56DuT42DkpvMrlsOwXJ-tyFOlj2z6o9d4bCjFpoAP3sSDxdu_qy-ef6BneqHAEil8ysHwbNqmtqQTGt_AO-KJSM7DUEMWChYiCilxN3tsNcNX4bL8mA_rAgCx85T2VoP5Ck3nHKQR79EqIrqhAnhKNwQRLyCmgfq_5bcPpFV7Vc2z4gw\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'


curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "52aeedb1-5100-485c-8b7c-b3498bf341c5",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP AISP Client-2",
  "description": "Mock TPP AISP Client-2",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
    "accounts"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "jNWNM6lL76o1YuYdFwAvAN4Ua2jFwtppOJ3hwaEB9LMSrX2DpIasTdK8vWUKbq6S",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"93821366\", \"e\": \"AQAB\",   \"n\": \"q04OuOoxEGea_IHqzFR3KYUQufEbTY65zFrnhfeEjWPp7TArhb7vTqiEm-Lzt3hW6IhPENunChSVBG9ifxUgF8eaRGuqNsLIO4JOFXXN-wWakHkkslLLHvy44VVepHkDNMHtxPHCAMRzpHCFrIuzgPYlMDgnNaA-nrJy30YigOrouJ24wBkzmrSDzKaRfqCih2AWAZuxau2FZ3L_mBCfZDg4boozOyWCPWUir0BPu_Rwc-Z5m0MYuhMpZF_RD4CYCLxErzCjvsj2bZBHGvWJWR0Rxr5nByQ5hXiR53M6m6blP6HWp7gBBNK8zqoflZQvEopkdYJGKqRNa1Amfe61tQ\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'



curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "71c78f42-528f-4e95-bd8a-6c1d55663c83",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP AISP Client-3",
  "description": "Mock TPP AISP Client-3",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
    "accounts"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "cwukN1UIXvwar1VkLl1Zo79bqnAx1cvLBfxbpZFpPvVzmYCZ81HVhVQbxGYwR6WL",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"82107272\", \"e\": \"AQAB\",   \"n\": \"u-xRLzhvUZKGqmA-T_-4A8MjRx9RvtbgXRJeCnSgpe2dSqmI875zn9GHTocligqb7SBSa9exfiIsgxhdBQ-0kVv4s2N2Qk1cFxM4_VK1EldBKmzlnOrknvW3OnB4C4tuUPGH7ebmxAff5Xct7xtgqavAYU60BPLR5XcA3T8iIx8h96dC67cKMtQ3aEvrXfWh9rVybfXBGvbO96xn8qYezLtewh5i5PY-TkfsGmsKd6BgF1qVkBhCJMmaBsIGsUpzWs8r7WYlUaG-7iGm2SMSloXerx7GFbGCwYWRfg11p77YPnoLAy8EVBHfjkdnoWD8toHUqYKr-5N_aUUxH6_HGw\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'


curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "cf840acd-1b28-423c-8fdd-1695f63e4fa9",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP AISP Client-4",
  "description": "Mock TPP AISP Client-4",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
    "accounts"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "OpHwA927ZnYnbaalbu3EITF4w19NBE18Sc0upDSNsHXFf4ZHfWd29eOS3xE9e85o",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"71527480\", \"e\": \"AQAB\",   \"n\": \"kJ8pEFsIKDSIrKoYemeOqx2U7QUpjXSlnNgozdpUsTC2z-j5mKoKZXfrzyWIwdEuWSyr-s2GD3U-Eg7ZX1BBgEsosgdbb7E1gksSLfyj1_mzLhjRF6aM0sNn7pohuLa3pMNuBEChrrPKfD3T9XVnvt2x7rxYhBUXpGZiJ3aZ5Q73SlpBmHyv8Ate3AvLZx_3W5dl0TK_O-aCAWEKVn5yhjBikt_BcHdKQmXK7wA25coeYBFdugW5jRGLFJZndWcUc3GBjFBAeefvWZrnINoi5UURTkhKRmNFByGJys2KpJPQ7AHBR5n5gOFpsm5T2_D71JX1iZasO8gAwqKl8kc2tw\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'


curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "15151444-54df-4c57-aa98-da099b15e83a",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP PISP Client-1",
  "description": "Mock TPP PISP Client-1",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
    "payments"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "Him5T7M8f0MXCqa6wbqnSmS0budY83h9p6x3pmjgaaROyjM9wtdpXIlA9xj0JXqi",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"97692617\", \"e\": \"AQAB\",   \"n\": \"roXzkH_w_MtJ0ud_SWhpaedXsqncnZpWfizU5cUBXfeGgxhtxWW06968RLxys41vCjVG9kO4BOQrqZTpoFaqR6TDraXtKIiHNCENdJJx7er7hburSEVqn2CISPTwLwsDjmzKB7vtpE6nVKtemHxX5wFEQUAI4IyJLSSWxBdfH-moyukJSgQ-LFdjW_UT7UgjS3bF1OzlOnNqduqzylRHfrd-gjWABKkGTHujtnr5IbPgq6yOGYvLQkBXbXSeq5iP2seMvV5Irq_cVoBjyN8HQT5RTTbJXOmBIRbg66upYZzsZU5CgJVrZEaeMnULf6ORTn4MkH5Fmpwm1hSTAYD1Xw\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'




curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "ff611234-bc84-4ae4-9173-ab24c4f0fcf3",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP PISP Client-2",
  "description": "Mock TPP PISP Client-2",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
    "payments"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "yYOLDIDdjjdmoscW6cDf4YV2301GbyBqPV2Kom301tJrDyTAB4dL3W5ZUb407iTL",
    "encryptedSecret": "",
    "clientCertIssuerDn": "",
    "clientCertSubjectDn": "",
    "enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"43125073\", \"e\": \"AQAB\",   \"n\": \"v9BRxKO3BhTkExcxHEICjynCW2UXOL_IJcR62TGt7JIad0IJH2EIzsSPODuqjVrnzxKVAw2tNDRxblciWlonNMofrbelMJyNoJGbIER8IVsXN5B00FzfFSW8N7XDo3mEDa9x6jxO-R5i5-Hjsnfe53aQ4DNk3rpDkP3K6tSAh0jgpLqj3z5R7b2y98vOCUUofOijnOZURlASiO18xWpDNNJTGvnrqBChnccX8YiBnoQnDGkrSz3hNAD8oEQGjuvSJ8R4116ZrcVGqBTwV5eo_TEeIP2Y__mqtc6vxXlgh3YR-yvqgq0ONnrblJR7kq7EWsNee603-UX4BtbFqrYxXQ\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'




curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "c20e58ed-642b-48d7-8283-e63c8aad13e7",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP PISP Client-3",
  "description": "Mock TPP PISP Client-3",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
    "payments"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "X3HC6mgEGG09GTdi2hmkZpBrrigCc6kaAwFVcpDEBZnXP32Z7MNkJuDLubaRT6Da",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"92581923\", \"e\": \"AQAB\",   \"n\": \"pknB6pqgOplWmGLShT6LRC1naNkbtqIUkaWA_tJHdMtlQ8Mf2kyWBf4I5nllJnjb34RJJBU_4P0nY8YIFpNddkc1J04d24ZDd4_XS_tWZ-guIsOJ6Acg9ARmhbXLKHXKBI-7N7zL5SiUbDx1c9DJ0p1UMbmVQSVcSFimXAsT5sEAGnoFNSYuOtklqQu4Ni50tfJ7EdbaEN8CQbu00__OKn4usq56r2Pqmr3jtxhkps92nvzja3VrF0nIR-cRiTS_ziJhoz7gRLKkkmTkuxsgjBZW-Xgj3ebbzQd5WdxNfsPZSpBC-UQcXEZyz3wqaF6Wxe00BfOLONRc_roB6RL0uw\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'




curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "47ee5d43-8d6f-48c2-b3fa-ea733231a8e0",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP PISP Client-4",
  "description": "Mock TPP PISP Client-4",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
    "payments"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "r2kYLKB9e1z2yhidbZX7o0vIKUJ4SjbV2JAqc4vyyRSICk4Wlb6o4JKFBEYME9x7",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"90020081\", \"e\": \"AQAB\",   \"n\": \"i_Y5HAsQUEZT48I7MN3B5e34p8pZcPUZchh_inFBwxMtSAL8Wy2W5pCpuWalk8_I5tciFfGRqxndUCtSBU4XO02RnDg7DzCVdoIXRHbRMY8lPhkneEcy5ZgbJ51Wwm0sbR7E-w1yGvM7PD2wPreLkRozx_NypCwS2-ol3WEeaLYWxwp30r_tzZiKb2I0tdFqwPKPh5Gy1i70sayOQ0PGTWXVVynJoB59y9NpeQ5Qsuvek7mrL2iBqbABVx6oD3LkDVIiKHSr-e4jgZ3zBEuz_IEvXzHKvK2WKL6m-mQunwd4Tx86CbOPMmgaLCEuiZIC2AnFoheKNmi56Xcx1p22dw\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'


curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "c528982b-5246-4634-873b-79e74f1796ab",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP AISP - PISP Client-",
  "description": "Mock TPP AISP - PISP Client-1",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
	"accounts",
    "payments"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "PIgIDWfBVre3BRHWtMK08s1K6kaWGzQXTla3Gr5OcoUiJuNZP7h2eQgmMkmVEgrL",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"17430452\", \"e\": \"AQAB\",   \"n\": \"yDMpGPNQHA5x3Zcpo5Psrem-uXbfjtpqoVwjaoz5TP5XRuzldUoo91OoebJov-6_1eTucZSbjWQ8fhwvwQFkLwLxS7Qbm7LUuWY8qsSfFNRWXqIlqvU36NkqqRFONuJwC0-gmJHNlfMjNd-dwK7XnbKKm7AMpofWSd-e2D9VJP5tfyneCnhdQ63gf3IakijDRRSHIJ5D6vNcFXuknCXYyR1x5bNIUJc1sFETjVdS3psaDSZPEdDvZMCAcR1M_Te_v8cYeKEMoAAJzk9ArcQ6t8DcC9aQSnGY5IGnIg7hgFxvpKPb8zKIVEAE7TCLdjLftONvkJ4p2GmCSVw-NyvZAQ\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'


curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "f0122270-d42c-4920-8596-8e20e284b0db",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP AISP - PISP Client-2",
  "description": "Mock TPP AISP - PISP Client-2",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
	"accounts",
    "payments"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "mXO1lF44ghdF7Xvyk8634KohfcFxd7p1C8gBemry3xonS83yC6xdWX09BmqMkPdE",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"42215839\", \"e\": \"AQAB\",   \"n\": \"7G7a8OOooVlCoepvjx7vvxS0M2biE-TlXyjCDWUicVg148IXrlHxL-tg8hr6JkDaTQSOaA13oLU42jQTNA-yzXiPuv2FoZmPxT-HWXvQsgn1oTrB4O1CL1X8g3M2Crp5cY99DoqQ7-yS0ZIDe4UuVu8paeHpcAFOz_pTdl8FX4Yn8z7u51EGUns4lI1ay45ykS-M_kjN-jS5D5WOIErYTFehLwCo8YowBN5ovyUPHtQaMaM6gBR0lb6z_IeDfnBofFoKP1Bed_nU1A9XAlRy9Q9xs9Blvpg_6G71HQxhMxejbToSIaw8Oy6YUyd7J2x2iCbhaBpw1AVglcu0N_sd5w\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'



curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "75511920-cb4c-4ec5-baa0-1347f1d4435c",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP AISP - PISP Client-3",
  "description": "Mock TPP AISP - PISP Client-3",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
	"accounts",
    "payments"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "JcKk6x6GF5aPmr4xXwRUUMEdgVj8tfhGxbmr8FmppqBpzxIlxYYAtkKlYuHmudLk",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"12540176\", \"e\": \"AQAB\",   \"n\": \"u4hC48mxBEL0UlOiZLy-ZFx2qNXhamD0PmuCYz_GTXVeWAywwIkaYWXzridBhmP7hik4SzlhPsOT4Y0kZWTIn1c5BZ2jp_LPwP2QLcmokVXPzroO6OuFKuL5DLYoQNNEm3tZWcP9CjQOAT8OkJQ4kIKMJ2GPhbiVdqSVCrnjcGIStrYY8GfIP8ZqYTU3_47gTGnQ1iF8Y1UUuU9pfn-0ctEeVqcacd4onBmk7tmVSZk7yKTk8u6x1jzgIuNHZS0MKLzu50A-xf7_QLViA7PG54hnMP3fg_vrYlQfmOmk98G4ZTRv2VaZ2OLHnqzfn0ybOz8OrSmzNm3d8AmF7Vzppw\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'


curl -X POST \
  https://URL/pf-admin-api/v1/oauth/clients \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  -d '{
    "clientId": "40458261-6920-43df-a964-9fae9c8d89cc",
  "redirectUris": [
    "https://www.getpostman.com/oauth2/callback"
  ],
  "grantTypes": [
    "IMPLICIT",
    "AUTHORIZATION_CODE",
    "CLIENT_CREDENTIALS",
    "REFRESH_TOKEN"
  ],
  "name": "Mock TPP AISP - PISP Client-4",
  "description": "Mock TPP AISP - PISP Client-4",
  "refreshRolling": "DONT_ROLL",
  "persistentGrantExpirationType": "SERVER_DEFAULT",
  "persistentGrantExpirationTime": 0,
  "persistentGrantExpirationTimeUnit": "DAYS",
  "bypassApprovalPage": true,
  "restrictScopes": true,
  "restrictedScopes": [
    "openid",
	"accounts",
    "payments"
  ],
  "exclusiveScopes": [
    
  ],
  "restrictedResponseTypes": [
    "code id_token"
  ],
  "defaultAccessTokenManagerRef": {
    "id": "INTREFATM",
    "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/accessTokenManagers/INTREFATM"
  },
  "validateUsingAllEligibleAtms": false,
  "oidcPolicy": {
    "idTokenSigningAlgorithm": "RS256",
    "policyGroup": {
      "id": "oidcpol",
      "location": "https://ip-172-27-6-36.eu-west-1.compute.internal:9999/pf-admin-api/v1/oauth/openIdConnect/policies/oidcpol"
    },
    "grantAccessSessionRevocationApi": false,
    "pingAccessLogoutCapable": false
  },
  "clientAuth": {
    "type": "SECRET",
    "secret": "Ur266H2XOvWX37JytlcpCSzkIVE7FoYdlxEg2pK3gqMv9bJ5gRVlTknZBX31Gqri",
    "encryptedSecret": "",
	"clientCertIssuerDn": "",
	"clientCertSubjectDn": "",
	"enforceReplayPrevention": "false"
  },
  "jwksSettings": {
    "jwks": "{\"keys\": [ { \"kty\": \"RSA\",   \"use\": \"sig\", \"alg\" : \"RS256\", \"kid\": \"61912003\", \"e\": \"AQAB\",   \"n\": \"pRuSxfC1pfyFBUSBVSzVlukAALBMRy9w-SzjnHPbTY9_LLQ1_4CPqFbpsVfMkJMy-xHBgn46OMJKKYdSvyEkcNdX9nxBs3RgxlYISu0oX5McIgHIUZ76uzMixA6XGI4w8sXmmfvCRbNyx4yXo6haWJM2HKh7hyy3kIAdvm0tQ3QGS6cEcWg6e-1aQKHDwHMd3fVRnLyTRBV2j662mNCKBz1gZLew1pKxJbCmktO4XLQtycLhf8UGEyrS0U6V89v3FJTTTBuYM1bNz8Ey9SDpeTmVQzKwJWT_ewfRNC3QWIM5p1IJdpdayiVkmrFESOHzMa304o6qfqSzIZdmdbB3qQ\" } ] }"
  },
  "extendedParameters": {
    
  },
  "requireSignedRequests": true
}'
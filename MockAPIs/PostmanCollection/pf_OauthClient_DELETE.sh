#!/bin/bash
#
# Copyright 2015-2016 UnboundID Corp.
#
# Executes the Self-Service Account Manager application installer.  Run the
# script without any arguments to display the help, and refer to the
# documentation for additional information.
#

curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/43fb5e4c-b22d-46da-acfa-0793a6d1713c \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  
curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/52aeedb1-5100-485c-8b7c-b3498bf341c5 \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fd4c91fc-0d82-4a04-abc6-867acbe8fe40' \
  -H 'X-XSRF-Header: WERWESDXXRT32341' \
  
curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/71c78f42-528f-4e95-bd8a-6c1d55663c83 \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 41006a22-2a24-4636-b2c7-0cd0bfb7dbf6' \
  -H 'X-XSRF-Header: WERWESDXXRT3234'
  
curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/cf840acd-1b28-423c-8fdd-1695f63e4fa9 \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 41006a22-2a24-4636-b2c7-0cd0bfb7dbf6' \
  -H 'X-XSRF-Header: WERWESDXXRT3234'

curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/15151444-54df-4c57-aa98-da099b15e83a \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 41006a22-2a24-4636-b2c7-0cd0bfb7dbf6' \
  -H 'X-XSRF-Header: WERWESDXXRT3234'
  
curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/ff611234-bc84-4ae4-9173-ab24c4f0fcf3 \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 41006a22-2a24-4636-b2c7-0cd0bfb7dbf6' \
  -H 'X-XSRF-Header: WERWESDXXRT3234'
  
curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/c20e58ed-642b-48d7-8283-e63c8aad13e7 \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 41006a22-2a24-4636-b2c7-0cd0bfb7dbf6' \
  -H 'X-XSRF-Header: WERWESDXXRT3234'

curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/47ee5d43-8d6f-48c2-b3fa-ea733231a8e0 \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 41006a22-2a24-4636-b2c7-0cd0bfb7dbf6' \
  -H 'X-XSRF-Header: WERWESDXXRT3234'  
  
curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/c528982b-5246-4634-873b-79e74f1796ab \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 41006a22-2a24-4636-b2c7-0cd0bfb7dbf6' \
  -H 'X-XSRF-Header: WERWESDXXRT3234'
  
curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/f0122270-d42c-4920-8596-8e20e284b0db \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 41006a22-2a24-4636-b2c7-0cd0bfb7dbf6' \
  -H 'X-XSRF-Header: WERWESDXXRT3234'
  
curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/75511920-cb4c-4ec5-baa0-1347f1d4435c \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 41006a22-2a24-4636-b2c7-0cd0bfb7dbf6' \
  -H 'X-XSRF-Header: WERWESDXXRT3234'

curl -X DELETE \
  https://URL/pf-admin-api/v1/oauth/clients/40458261-6920-43df-a964-9fae9c8d89cc \
  -k \
  -H 'Authorization: Basic credentials' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 41006a22-2a24-4636-b2c7-0cd0bfb7dbf6' \
  -H 'X-XSRF-Header: WERWESDXXRT3234'
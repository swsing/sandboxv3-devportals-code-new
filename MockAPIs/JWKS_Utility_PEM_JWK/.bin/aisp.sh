#!/bin/bash
#
# Copyright 2015-2016 UnboundID Corp.
#
# Executes the Self-Service Account Manager application installer.  Run the
# script without any arguments to display the help, and refer to the
# documentation for additional information.
#
pem-jwk 71c78f42_528f_4e95_bd8a_6c1d55663c83.pub > 71c78f42_528f_4e95_bd8a_6c1d55663c83.jwks
pem-jwk cf840acd_1b28_423c_8fdd_1695f63e4fa9.pub > cf840acd_1b28_423c_8fdd_1695f63e4fa9.jwks
pem-jwk 43fb5e4c_b22d_46da_acfa_0793a6d1713c.pub > 43fb5e4c_b22d_46da_acfa_0793a6d1713c.jwks
pem-jwk 52aeedb1_5100_485c_8b7c_b3498bf341c5.pub > 52aeedb1_5100_485c_8b7c_b3498bf341c5.jwks
#!/bin/bash
#
# Copyright 2015-2016 UnboundID Corp.
#
# Executes the Self-Service Account Manager application installer.  Run the
# script without any arguments to display the help, and refer to the
# documentation for additional information.
#
pem-jwk 47ee5d43_8d6f_48c2_b3fa_ea733231a8e0.pub > 47ee5d43_8d6f_48c2_b3fa_ea733231a8e0.jwks
pem-jwk 15151444_54df_4c57_aa98_da099b15e83a.pub > 15151444_54df_4c57_aa98_da099b15e83a.jwks
pem-jwk c20e58ed_642b_48d7_8283_e63c8aad13e7.pub > c20e58ed_642b_48d7_8283_e63c8aad13e7.jwks
pem-jwk ff611234_bc84_4ae4_9173_ab24c4f0fcf3.pub > ff611234_bc84_4ae4_9173_ab24c4f0fcf3.jwks
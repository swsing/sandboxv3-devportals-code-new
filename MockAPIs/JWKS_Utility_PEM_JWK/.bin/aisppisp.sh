#!/bin/bash
#
# Copyright 2015-2016 UnboundID Corp.
#
# Executes the Self-Service Account Manager application installer.  Run the
# script without any arguments to display the help, and refer to the
# documentation for additional information.
#
pem-jwk 40458261_6920_43df_a964_9fae9c8d89cc.pub > 40458261_6920_43df_a964_9fae9c8d89cc.jwks
pem-jwk 75511920_cb4c_4ec5_baa0_1347f1d4435c.pub > 75511920_cb4c_4ec5_baa0_1347f1d4435c.jwks
pem-jwk c528982b_5246_4634_873b_79e74f1796ab.pub > c528982b_5246_4634_873b_79e74f1796ab.jwks
pem-jwk f0122270_d42c_4920_8596_8e20e284b0db.pub > f0122270_d42c_4920_8596_8e20e284b0db.jwks
package com.capgemini.tppportal.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.tpp.dtos.ThirdPartyProvidersDetails;
import com.capgemini.tpp.model.ValidateSessionResponse;
import com.capgemini.tpp.ob.model.OBThirdPartyProviders;
import com.capgemini.tppportal.services.ThirdPartyProvidersService;
import com.capgemini.tppportal.transformers.ThirdPartyProvidersTransformer;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;

/**
 * ThirdPartyProvidersServiceImpl.java is used to call the OpenBanking for retrieving the Organisations Details
 * Retrieve the private key of siging cert from jks 
 * Calling of Openbanking Api is done in 3 steps
 * 1.Create the jwtToken for the clientId
 * 2. Use this jwtToken as a client assertion to create client_credential Access Token from OpenBanking
 * 3.Use the Access Token as Bearer Token for calling Open Banking Api for TppDetails 
 * @author tbhola
 *
 */
@Service
public class ThirdPartyProvidersServiceImpl implements ThirdPartyProvidersService{

	@Value("${openbanking.tokenUrl}")
	private String tokenUrl;

	@Value("${openbanking.apiUrl}")
	private String apiUrl;

	@Value("${openbanking.clientAssertionType}")
	private String clientAssertionType;

	@Value("${openbanking.grantType}")
	private String grantType;
	@Value("${openbanking.scope}")
	private String scope;

	@Value("${openbanking.keyId}")
	private String obKeyId;
	@Value("${openbanking.clientId}")
	private String clientId;
	@Value("${app.jwtTokenValidity}")
	private long jwtTokenValidity;

	@Value("${server.ssl.key-store}")
	private String keystorePath;
	
	@Value("${server.ssl.key-store-password}")
	private String keystorePassword;
	
	@Value("${server.ssl.key-password}")
	private String keyPassword;

	@Value("${openbanking.signing.key-alias}")
	private String keyAlias;

	@Value("${openbanking.transport.key-alias}")
	private String transportkeyAlias;

	private PrivateKey privKey;

	@Autowired
	ThirdPartyProvidersTransformer thirdPartyProvidersTransformer;

	@Autowired
	@Qualifier("tppPortalRestClientMATpp")
	private RestClientSync restClientMATpp;

	
    /**
     * This is used to retrieve the private key from the jks .
     * The corresponding certificate  is added in OpenBanking
     */
	@PostConstruct
	public void init(){
		try {
			Resource resource = new ClassPathResource(keystorePath);
			 InputStream in = resource.getInputStream();
			 KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			 keystore.load(in, keystorePassword.toCharArray());
			 privKey = (PrivateKey)keystore.getKey(keyAlias,keyPassword.toCharArray());
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | UnrecoverableKeyException e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.TECHNICAL_ERROR);
		}
	}
    /**
     * This method is used to retrieve the ThirdPartyProvidersDetails corresponding to Organisation list
     * if the organisation list is empty or null ,then it will return empty list of details
     * otherwise return List<ThirdPartyProvidersDetails>
     * It is divided into 3 steps
     * 1. Create client_credential JWT Token corresponding to clientId
     * 2. This client_credential token is used as client_assertion to retrieve AccessToken from Open Banking
     * 3. Now retrieve the tpp details from Open Banking using above AccessToken
     * 4. Then Call thirdPartyProvidersTransformer to transform the Tpp details  to List<ThirdPartyProvidersDetails>
     * 
     */
	@Override
	public List<ThirdPartyProvidersDetails> retrieveThirdPartyProvidersDetails(List<String> tppOrgIdList) {
		if(tppOrgIdList == null || tppOrgIdList.isEmpty()) {
			return new ArrayList<>();
		}
		String jwtToken = createJWTToken(clientId ,clientId ,jwtTokenValidity);
		String obAccessToken = retrieveOBAccessToken(jwtToken);
		List<OBThirdPartyProviders> obThirdPartyProviders = retrieveThirdPartyDetails(tppOrgIdList,obAccessToken);
		return thirdPartyProvidersTransformer.tranformToThirdPartyProvidersDetails(obThirdPartyProviders);
	}
    /**
     * This method is called from retrieveThirdPartyProvidersDetails method and Retrieve the AccessToken from Open Banking
     * @param jwtToken
     * @return String(AccessToken)
     */
	private String retrieveOBAccessToken(String jwtToken) {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(tokenUrl);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String,String> inputBody = new LinkedMultiValueMap<>();
		Map<String,String> obAccessToken = null;
		String accessToken;
		inputBody.add("client_id", clientId);
		inputBody.add("client_assertion_type",clientAssertionType);
		inputBody.add("grant_type",grantType);
		inputBody.add("scope",scope);
		inputBody.add("client_assertion", jwtToken);
		try {
			
			restClientMATpp.buildSSLRequest(transportkeyAlias);
			obAccessToken = restClientMATpp.callForPost(requestInfo, inputBody, Map.class, headers);
			accessToken = obAccessToken.get("access_token");
		} catch (PortalException ex) {
			throw PortalException.populatePortalException(ex.getErrorInfo().getActualDetailErrorMessage(),
					PortalErrorCodeEnum.TPP_ERROR_GENRIC);
		}
		catch(Exception ex){
			throw PortalException.populatePortalException(ex.getMessage(),PortalErrorCodeEnum.TPP_ERROR_GENRIC);
		}
		return accessToken;
	}
    /**
     * This method is used to call the OpenBanking to retrieve the details of Tpp Organisations
     * if Rest Call to Open Banking fails,then return exception
     * if obThirdPartyProvider is null then return exception 
     * @param tppOrgIdList - tpp Organisation list
     * @param accessToken  - access token
     * @return List<OBThirdPartyProviders> - list of OBThirdPartyProviders
     */
	private List<OBThirdPartyProviders> retrieveThirdPartyDetails(List<String> tppOrgIdList,String accessToken) {

		List<OBThirdPartyProviders> obThirdPartyProviders = new ArrayList<>();      
		OBThirdPartyProviders obThirdPartyProvider = null;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Authorization","Bearer "+accessToken);
		RequestInfo requestInfoforApiCall = new RequestInfo();
		for (String tppOrgId : tppOrgIdList){
			requestInfoforApiCall.setUrl(apiUrl+tppOrgId);
			obThirdPartyProvider = restClientMATpp.callForGetWithParams(requestInfoforApiCall,
					OBThirdPartyProviders.class,
					null, httpHeaders);
			if(obThirdPartyProvider == null || obThirdPartyProvider.getUrnopenbankingorganisation10() == null ) {
				throw PortalException.populatePortalException(PortalErrorCodeEnum.NO_TPP_DATA_AVAILABLE);
			}
			obThirdPartyProviders.add(obThirdPartyProvider);
		}
		return obThirdPartyProviders;
	}
    /**
     * This method is used to create the JWT Token corresponding to the clientId
     * @param issuer - clientId is issuer
     * @param subject - clientId is issuer
     * @param ttlMillis - time till token is valid
     * @return String (JwtToken)
     */
	private String createJWTToken(String issuer, String subject, long ttlMillis) {
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);
		Date exp = new Date(nowMillis + ttlMillis);
		JWTClaimsSet jwtClaims = new JWTClaimsSet.Builder().expirationTime(exp).issueTime(now).subject(subject)
				.issuer(issuer).jwtID(GenerateUniqueIdUtilities.getUniqueId().toString()).claim("scope", scope)
				.audience(tokenUrl).build();

		JWSSigner signer = new RSASSASigner(privKey);

		JWSObject jwsObject = new JWSObject(new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(obKeyId).build(),
				new Payload(jwtClaims.toJSONObject()));
		try {
			jwsObject.sign(signer);
		} catch (JOSEException e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.TPP_ERROR_GENRIC);
		}
		// Builds the JWT and serializes it to a compact, URL-safe string
		return jwsObject.serialize();
	}
	
	 
	  public ValidateSessionResponse validateSession(PTCInfo ptcInfo, HttpServletRequest request, HttpServletResponse response)
	  {
	    ValidateSessionResponse vResponse = new ValidateSessionResponse();
	    if (ptcInfo == null) {
	      throw PortalException.populatePortalException("Application OR IDP session expired", PortalErrorCodeEnum.TPP_USER_SESSION_EXPIRED);
	    }
	    vResponse.setStatusCode(Integer.toString(HttpStatus.OK.value()));
	    
	    return vResponse;
	  }
	

}

package com.capgemini.tppportal.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.capgemini.portal.model.PTCInfo;
import com.capgemini.tpp.dtos.ThirdPartyProvidersDetails;
import com.capgemini.tpp.model.ValidateSessionResponse;

public interface ThirdPartyProvidersService {

	public List<ThirdPartyProvidersDetails> retrieveThirdPartyProvidersDetails(List<String> tppOrgIds);
	
	 public ValidateSessionResponse validateSession(PTCInfo paramPTCInfo, HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse);
}

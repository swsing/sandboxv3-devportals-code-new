package com.capgemini.tppportal.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.portal.utilities.SessionCookieUtils;
import com.capgemini.psd2.jwt.JWTReader;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.PortalService;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.tppportal.config.OpenBankingConfig;
import com.capgemini.tppportal.helper.PortalHelper;

/**
 * PortalDashboardController handles login and logout of Tpp portal.
 * Authenticate to OB login page to get bearer token and make rest call to OB
 * token url to get logged in PTC details with the help of bearer token.
 *
 */
@Controller
public class PortalDashboardController {

	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	private HttpServletResponse httpServletResponse;
	
	@Autowired
	private PortalRequestHeaderAttributes portalRequestHeaderAttributes;

	@Autowired
	private UIStaticContentUtilityController uiStaticContentcontroller;
	@Value("${openbanking.tokenUrl}")
	private String tokenUrl;
	
	@Value("${portal.hostname}")
	private String entityBaseURL;
	
	@Value("${portal.logoutUrl}")
	private String logoutUrl;
	
	@Autowired
	private OpenBankingConfig obConfig;
	
	@Autowired
	@Qualifier("tppPortalRestClientMA")
	private RestClientSync restClientMA;
	
	/**
	 * flag used to by pass OB login.
	 */
	@Value("${testing.flag:false}")
	private boolean isTestMode;
	
	/**
	 * end point of well-known/openid-confguration JWKS
	 */
	@Value("${obAuthconfig.jwks.endpoint}")
	private String obJWKSEndpoint;
	
	@Value("${obAuthconfig.transport.key-alias}")
	private String transportkeyAlias;

	@Autowired
	private PortalService portalService;
	
	
	/**
	 * Redirect to open banking login page.
	 * 
	 * @throws IOException
	 */
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	public void redirect() throws IOException {
		if (isTestMode) {
			PTCInfo ptcInfo = new PTCInfo();
			ptcInfo.setPtcName("TestUser");
			ptcInfo.setPtcId("testuser@capgemini.com");
			List<String> tpps = new ArrayList(Arrays.asList("0015800000jfQ9aAAE"));
			ptcInfo.setOrganizationIds(PortalHelper.removeBlockedTpps(tpps));
			Cookie cookie = SessionCookieUtils.modifyTPPPortalCookie(ptcInfo, httpServletRequest);
			httpServletResponse.addCookie(cookie);
			httpServletResponse.sendRedirect(entityBaseURL + "/home");
			// restClientMA.buildSSLRequest(transportkeyAlias);
			
		} else {
			httpServletResponse.sendRedirect(obConfig.getRedirectOBUrl());
		}
	}

	/**
	 * Open Banking will redirect to below Url after authentication, having bearer code or error code as url params. With bearer code, 
	 * rest call to OB is done for OIDC profile claims,than create tpp portal session cookie with PTC details(decoded OIDC profile claims) and redirecting to home page.
	 * @param parameters bearer code or error code from OB as URL param.
	 * @return Redirect to home url to land on dashboard page.
	 */
	@RequestMapping(value = "/redirectUrl", method = RequestMethod.GET)
	@ResponseBody
	public RedirectView redirectResponse(@RequestParam Map<String, String> parameters) {
		if (null != parameters.get(PortalConstants.ERRORS) || null == parameters.get(PortalConstants.CODE))
			return new RedirectView(entityBaseURL + "/error", true);
		else {
			Map map = restCallToOB(parameters.get(PortalConstants.CODE));
			PTCInfo ptcInfo = new PTCInfo();
			ptcInfo.setPtcName((String) map.get(PortalConstants.OIDC_GIVEN_NAME));
			ptcInfo.setPtcId((String) map.get(PortalConstants.OIDC_SUB));
			if (null != map.get(PortalConstants.TPP_ASSOCIATIONS_LIST)) {
			ptcInfo.setOrganizationIds(
					PortalHelper.removeBlockedTpps((List) map.get(PortalConstants.TPP_ASSOCIATIONS_LIST)));
			}
			Cookie cookie = SessionCookieUtils.modifyTPPPortalCookie(ptcInfo, httpServletRequest);
			httpServletResponse.addCookie(cookie);
			return new RedirectView(entityBaseURL + "/home", true);
		}
	}
	
	
	/**
	 * @return Tpp portal Login page.
	 */
	@RequestMapping(value={"/"})
	public ModelAndView loginPage() {
		staleCookieHandle();
		ModelAndView mav = new ModelAndView(PortalConstants.INDEX);
		mav.addObject(PortalConstants.ISAUTHORIZED, PortalConstants.FALSE);
		mav.addObject(PortalConstants.UISTATICCONTENT, uiStaticContentcontroller.getStaticContentForUI());
		return mav;
	}
	
	/**
	 * Load Tpp portal dashboard page on successful validations with ISAUTHORIZED flag as true.
	 * To validate if user comes from tpp login page,check for temp cookie, if present delete it and proceed to dashboard.
	 * if not redirect to logout to avoid hacking.
	 * @param model
	 * @return 
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home(Model model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView(PortalConstants.INDEX);
			PTCInfo ptcInfo = SessionCookieUtils.getPTCInfo(httpServletRequest);
			Cookie tempCookie = SessionCookieUtils.findCookie(httpServletRequest,
					PortalConstants.TPPPORTAL_TEMP_COOKIE);
			// check if user is coming from tpp login page.
			if (null != tempCookie && tempCookie.getName().equalsIgnoreCase(PortalConstants.TPPPORTAL_TEMP_COOKIE)
					&& null != ptcInfo) {
				tempCookie.setMaxAge(0);
				tempCookie.setHttpOnly(true);
				tempCookie.setSecure(true);
				httpServletResponse.addCookie(tempCookie);
			} else {
				httpServletResponse.sendRedirect(entityBaseURL + logoutUrl);
			}
			mav.addObject(PortalConstants.LOGOUT_URL, entityBaseURL + logoutUrl);
			if (null != ptcInfo) {
				mav.addObject(PortalConstants.PTC_NAME, ptcInfo.getPtcName());
			}
			mav.addObject(PortalConstants.ISAUTHORIZED, PortalConstants.TRUE);
			mav.addObject(PortalConstants.CORELATION_ID, portalRequestHeaderAttributes.getCorrelationId());
			mav.addObject(PortalConstants.UISTATICCONTENT, uiStaticContentcontroller.getStaticContentForUI());
			return mav;
		} catch (Exception ex) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.LOGIN_ERROR);
		}

	}

	/**
	 * On logout, deleting the tpp portal session cookie and redirecting to open banking SLO.
	 * @throws IOException
	 */
	@RequestMapping("/logout")
	@ResponseStatus(value = HttpStatus.OK)
	public void logout() throws IOException{
		PortalHelper.logout(httpServletRequest, httpServletResponse);
		httpServletResponse.sendRedirect(obConfig.getLogoutUrl());
	}
	
	/**
	 * Handles any Exceptions of PortalDashboardController and return to index/login page with error code.
	 * @param exception
	 * @return ModelAndView index/login page.
	 */
	@ExceptionHandler(PortalException.class)
	public ModelAndView handleException(Exception ex){
		ModelAndView mav = new ModelAndView(PortalConstants.INDEX);
		staleCookieHandle();
		mav.addObject(PortalConstants.ERRORS,PortalException.populatePortalException(PortalErrorCodeEnum.LOGIN_ERROR).getErrorInfo());
	    mav.addObject(PortalConstants.CORELATION_ID,portalRequestHeaderAttributes.getCorrelationId());
	    mav.addObject(PortalConstants.LOGOUT_URL, entityBaseURL+logoutUrl);
	    mav.addObject(PortalConstants.ISAUTHORIZED,PortalConstants.FALSE);
		mav.addObject(PortalConstants.UISTATICCONTENT, uiStaticContentcontroller.getStaticContentForUI());
	    return mav;
	}
	
	/**
	 * Handles error scenarios at the time of login.
	 * @return ModelAndView index/login page with Error code.
	 */
	@RequestMapping(value = {"/error","/authError"}, method = RequestMethod.GET,produces = "text/html")
    public ModelAndView errorHtml() {
		ModelAndView mav = new ModelAndView(PortalConstants.INDEX);
	    Exception exception = (Exception) httpServletRequest.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
	    if ( exception instanceof PortalException) {
	    	mav.addObject(PortalConstants.ERRORS,((PortalException)exception).getErrorInfo());
	    }else {
	    	mav.addObject(PortalConstants.ERRORS,PortalException.populatePortalException(PortalErrorCodeEnum.TPP_ERROR_GENRIC).getErrorInfo());
	    }
	    staleCookieHandle();
	    mav.addObject(PortalConstants.LOGOUT_URL, entityBaseURL+logoutUrl);
	    mav.addObject(PortalConstants.CORELATION_ID,portalRequestHeaderAttributes.getCorrelationId());
	    mav.addObject(PortalConstants.ISAUTHORIZED, PortalConstants.FALSE);
		mav.addObject(PortalConstants.UISTATICCONTENT, uiStaticContentcontroller.getStaticContentForUI());
	    return mav;
    }
	
	/**
	 * Make rest call to Open Banking to get OIDC Profile Claims. after that validate the
	 * ID_Token using the .well-known/openid-confguration JWKS file link. The ID
	 * token will use a kid present in the JWKS file - which will enable us to
	 * validate the ID_Token against the signing key.
	 * 
	 * @param code
	 *            : bearer token received from Open Banking on successful login.
	 * @return decoded Map having Tpp details.
	 */
	private Map restCallToOB(String code) {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(tokenUrl);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String,String> inputBody = new LinkedMultiValueMap<>();
		Map<String,Object> token=null;
		inputBody.add("client_id", obConfig.getClientId());
		inputBody.add("grant_type",obConfig.getGrantType());
		inputBody.add("scope",obConfig.getScopes());
		inputBody.add("client_secret", obConfig.getClientSecret());
		inputBody.add("code", code);
		inputBody.add("redirect_uri", obConfig.getRedirectURI());
		try{
			restClientMA.buildSSLRequest(transportkeyAlias);
			Map<String, String> obj = restClientMA.callForPost(requestInfo, inputBody, Map.class, headers);
			token = JWTReader.verifyAndDecodeTokenIntoMap(obj.get("id_token"),
					portalService.getOBPublicKey(obJWKSEndpoint));
		}
		catch(PortalException ex){
			throw PortalException.populatePortalException(ex.getErrorInfo().getActualDetailErrorMessage(),PortalErrorCodeEnum.LOGIN_ERROR);
		}
		catch(Exception ex){
			throw PortalException.populatePortalException(ex.getMessage(),PortalErrorCodeEnum.LOGIN_ERROR);
		}
		return token;
	}
	
	/**
	 * If tpp portal session stale cookie is present initiate logout. Else create temp cookie at login page.
	 * Example: 1) If close tpp portal tab after login and hit tpp login url again, stale cookie will present.
	 *          2) Try to hit login url when already logged in other tab, user should logout as per BRD.
	 */
	private void staleCookieHandle(){
		Cookie staleCookie = SessionCookieUtils.getTPPPortalCookie(httpServletRequest);
		if (null != staleCookie) {
			try {
				httpServletResponse.sendRedirect(entityBaseURL + logoutUrl);

			} catch (IOException e) {
				throw PortalException.populatePortalException(PortalErrorCodeEnum.LOGIN_ERROR);
			}
		} else {
			Cookie cookie = new Cookie(PortalConstants.TPPPORTAL_TEMP_COOKIE, null);
			cookie.setSecure(true);
			cookie.setHttpOnly(true);
			httpServletResponse.addCookie(cookie);
		}
	}
}
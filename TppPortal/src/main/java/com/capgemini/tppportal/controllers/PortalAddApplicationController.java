package com.capgemini.tppportal.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.psd2.service.PortalService;
import com.capgemini.tpp.dtos.ApplicationResponse;
import com.capgemini.tpp.dtos.TPPDetails;
import com.capgemini.tpp.ssa.model.SSAModel;
import com.capgemini.tppportal.helper.PortalHelper;

/**
 * @author atongiya
 *
 */
@RestController
public class PortalAddApplicationController {

	@Autowired
	private PortalService portalService;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private PortalRequestHeaderAttributes requestAttributes;

	@Autowired
	private PortalLoggerUtils portalLoggerUtils;

	@Autowired
	private PortalAspectUtils portalAspectUtils;

	@Autowired
	private PortalHelper portalHelper;

	/**
	 * On uploading the SSA Token this request Mapping is called Below are the
	 * steps it performs 1. Validates the request 2. Fetches the Tpp Application
	 * Details to be shown on screen from SSA Token
	 * 
	 * @param file
	 * @param tppOrgId
	 * @return TppDetails i.e the fields mentioned in VD
	 * @throws IOException
	 */
	@RequestMapping(value = "/ssaToken", method = RequestMethod.POST)
	public TPPDetails decodeTppTokenFromFile(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "tppOrgId", required = true) String tppOrgId) {
		PortalLoggerAttribute loggerAttribute = portalLoggerUtils.populateLoggerData("decodeTppTokenFromFile");
		if (file == null || file.isEmpty())
			throw PortalException.populatePortalException(PortalErrorCodeEnum.INVALID_SSA);
		String ssaToken = null;
		try {
			ssaToken = new String(file.getBytes());
		} catch (IOException e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.INVALID_SSA);
		}
		SSAModel ssaDataElements = validateRequest(tppOrgId, ssaToken);
		portalAspectUtils.enterPayloadlogger(PortalAddApplicationController.class.getName(), loggerAttribute,
				ssaDataElements);
		TPPDetails tppDetails = portalService.fetchTppDetailsFromSSAToken(ssaDataElements);
		tppDetails.setFile(ssaToken);
		portalAspectUtils.exitPayloadlogger(PortalAddApplicationController.class.getName(), loggerAttribute,
				tppDetails);
		return tppDetails;
	}

	/**
	 * On clicking on Submit button while add Application this request Mapping
	 * is called Below are the steps it performs 1. Validates the request 2.
	 * Adds the Tpp Application
	 * 
	 * @param file
	 * @param tppOrgId
	 * @return TppApplicationName
	 */
	@RequestMapping(value = "/application", method = RequestMethod.POST)
	public ApplicationResponse createApplication(@RequestParam("file") String file,
			@RequestParam(value = "tppOrgId", required = false, defaultValue = "") String tppOrgId) {
		PortalLoggerAttribute loggerAttribute = portalLoggerUtils.populateLoggerData("createApplication");
		if(file==null || file.isEmpty())
			throw PortalException.populatePortalException(PortalErrorCodeEnum.INVALID_SSA);
		
		SSAModel ssaDataElements = validateRequest(tppOrgId, file);
		// For logging of SSA Token, Aspect could not be used for Multipart
		// Request
		portalAspectUtils.enterPayloadlogger(PortalAddApplicationController.class.getName(), loggerAttribute,
				ssaDataElements);
		try {
			portalService.createTppApplication(ssaDataElements, tppOrgId, file);
		} catch (Exception e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.TPP_ERROR_GENRIC);
		}
		ApplicationResponse addApplicationResponse = new ApplicationResponse();
		addApplicationResponse.setTppApplicationName(ssaDataElements.getSoftware_client_name());
		portalAspectUtils.exitPayloadlogger(PortalAddApplicationController.class.getName(), loggerAttribute,
				addApplicationResponse);
		return addApplicationResponse;
	}

	/**
	 * Validates the request on Upload and Submit of Add Application 1. if the
	 * tpp is in blocked status in LDAP 2. if the organisation belongs to the
	 * PTC 3. verifies the token type, token authenticity, token expiry 4. if
	 * the organisation in the token belongs to the PTC 5. if the token is in
	 * active status 6. if the selected organisation is same as that in the
	 * token.
	 * 
	 * @param tppOrgId
	 * @param file
	 * @return SSAModel (Conversion of SSA token elements to SSAModel class)
	 */
	public SSAModel validateRequest(String tppOrgId, String ssaToken) {
		PTCInfo ptcInfo = portalHelper.validatePTCInfoAndTppBlockedStatus(request, requestAttributes, tppOrgId);
		SSAModel ssaDataElements = null;
		try {
			// verifies the token type, token authenticity, token expiry
			ssaDataElements = portalService.decodeAndVerifyToken(ssaToken);
			if (null != ssaDataElements.getSoftware_roles() && !ssaDataElements.getSoftware_roles().isEmpty()) {
				requestAttributes.setRoles(ssaDataElements.getSoftware_roles().toString());
			}
		} catch (PortalException e) {
			if (e.getErrorInfo().getErrorCode() == PortalErrorCodeEnum.TPP_ERROR_GENRIC.getErrorCode()) {
				throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.TPP_ERROR_GENRIC);
			} else {
				throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.INVALID_SSA);
			}
		} catch (Exception e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.INVALID_SSA);
		}

		// ssa does not belong to ptc
		if (!ptcInfo.getOrganizationIds().contains(ssaDataElements.getOrg_id()))
			throw PortalException.populatePortalException(PortalErrorCodeEnum.INVALID_SSA);

		// ssa does not belong to selected org
		if (!tppOrgId.equals(ssaDataElements.getOrg_id()))
			throw PortalException.populatePortalException(PortalErrorCodeEnum.SSA_NOT_OF_SELECTED_ORG);
		return ssaDataElements;
	}
}
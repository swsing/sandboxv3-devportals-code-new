/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.tppportal.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.psd2.service.PortalService;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.tpp.muleapi.schema.ResetSecretResponse;
import com.capgemini.tpp.muleapi.schema.ShowSecretResponse;
import com.capgemini.tppportal.helper.PortalHelper;

/**
 * PortalManageApplicationController.java is serving reset api for manage tpp
 * application.
 * 
 * Rest API 1. Show Secret 
 *          2. Reset Secret 
 *          3. Delete Application
 */
@RestController
public class PortalManageApplicationController {

	@Autowired
	private PortalService portalService;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private PortalRequestHeaderAttributes requestAttributes;

	@Autowired
	private PingDirectoryService pingDirectoryService;

	@Autowired
	private PortalHelper portalHelper;

	/**
	 * Shows client secret for requested application to be used with clietId to
	 * make API requests.
	 * 
	 * @param tppId
	 * @param clientId
	 * @param applicationId
	 * @return {@link ShowSecretResponse}
	 */
	@RequestMapping(value = "/application/{applicationId}/secret", method = RequestMethod.GET)
	public ShowSecretResponse showSecret(
			@RequestParam(value = "tppId", required = true, defaultValue = "") String tppId,
			@RequestParam(value = "clientId", required = true, defaultValue = "") String clientId,
			@PathVariable(value = "applicationId") String applicationId) {

		ShowSecretResponse showSecret;
		validateRequest(tppId, clientId);

		try {
			showSecret = portalService.fetchClientSecret(clientId, applicationId);

		} catch (PortalException e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.TPP_ERROR_GENRIC);
		}

		return showSecret;
	}

	/**
	 * Resets client secret for requested application.
	 * 
	 * @param tppId
	 * @param clientId
	 * @param applicationId
	 * @return ResetSecretResponse
	 */
	@RequestMapping(value = "/application/{applicationId}/secret/reset", method = RequestMethod.GET)
	public ResetSecretResponse resetClientSecret(
			@RequestParam(value = "tppId", required = true, defaultValue = "") String tppId,
			@RequestParam(value = "clientId", required = true, defaultValue = "") String clientId,
			@PathVariable(value = "applicationId") String applicationId) {

		ResetSecretResponse resetSecret = null;
		validateRequest(tppId, clientId);

		try {
			resetSecret = portalService.resetClientSecret(clientId, applicationId);

		} catch (PortalException e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.TPP_ERROR_GENRIC);
		}
		return resetSecret;
	}

	/**
	 * Deletes the requested application.
	 * 
	 * @param tppId
	 * @param clientId
	 * @param applicationId
	 * @return AddApplicationResponse
	 */
	@RequestMapping(value = "/application/{applicationId}", method = RequestMethod.DELETE)
	public void deleteTppApplication(@RequestParam(value = "tppId", required = true, defaultValue = "") String tppId,
			@RequestParam(value = "clientId", required = true, defaultValue = "") String clientId,
			@PathVariable(value = "applicationId") String applicationId) {

		validateRequest(tppId, clientId);
		portalService.deleteTppApplication(clientId, tppId, applicationId);

	}

	/**
	 * Validate the api request against tppId, clientId and applicationId
	 * attributes Example 1. Null check for input attributes. 2. validate
	 * PTCInfo And TppBlocked Status for requested TPP 3. validate is client Id
	 * is associated TPP or not?
	 * 
	 * @param tppId
	 * @param clientId
	 */
	private void validateRequest(String tppId, String clientId) {

		if (NullCheckUtils.isNullOrEmpty(tppId) || NullCheckUtils.isNullOrEmpty(clientId))
			throw PortalException.populatePortalException(PortalErrorCodeEnum.NO_TPP_CLIENTID_FOUND);

		portalHelper.validatePTCInfoAndTppBlockedStatus(request, requestAttributes, tppId);

		if (!pingDirectoryService.getClientID(tppId).contains(clientId))
			throw PortalException.populatePortalException(PortalErrorCodeEnum.TPP_CLIENTID_INVALID);

	}

}

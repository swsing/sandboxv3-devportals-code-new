/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.tppportal.controllers;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.encoder.Encode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.portal.utilities.SessionCookieUtils;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.tpp.dtos.ViewApplicationResponse;
import com.capgemini.tpp.ldap.client.model.TPPApplication;
import com.capgemini.tpp.model.ValidateSessionResponse;
import com.capgemini.tppportal.services.ThirdPartyProvidersService;

/**
 * The Class PortalViewApplicationController.
 */

@RestController
public class PortalViewApplicationController {

	@Autowired
	private PingDirectoryService pingDirectoryService;

	@Autowired
	private HttpServletRequest httpServletRequest;

	@Autowired
	private HttpServletResponse httpServletResponse;

	@Autowired
	private ThirdPartyProvidersService thirdPartyService;

	/**
	 * Fetch List of Tpp Applications sort them in reverse chronological
	 * order,check is Tpp is blocked to add applications or not.
	 * 
	 * @param tppOrgId
	 * @return
	 */
	@RequestMapping(value = "/applications", method = RequestMethod.GET)
	public ViewApplicationResponse loadApplicationsByTPPOrgId(String tppOrgId) {

		ViewApplicationResponse apiResponse = new ViewApplicationResponse();
		try {
			List<TPPApplication> applicationList = pingDirectoryService.fetchListOfTPPApplications(tppOrgId);
			Collections.sort(applicationList, new Comparator<TPPApplication>() {
				public int compare(TPPApplication o1, TPPApplication o2) {
					return o2.getCreateTimestamp().compareTo(o1.getCreateTimestamp());
				}
			});
			String jsEncodedTppOrgId = Encode.forJavaScript(tppOrgId);
			String htmlEncodedTppOrgId = Encode.forHtml(jsEncodedTppOrgId);
			apiResponse.setTppOrgId(htmlEncodedTppOrgId);
			apiResponse.setTppAppList(applicationList);
			apiResponse.setTPPBlocked(pingDirectoryService.isTppBlockEnabled(tppOrgId));
		} catch (PortalException e) {
			throw PortalException.populatePortalException(PortalErrorCodeEnum.TPP_ERROR_GENRIC);
		}
		return apiResponse;

	}

	@RequestMapping(value = "/auth/session", method = RequestMethod.GET)
	public ValidateSessionResponse validateSession() {
		PTCInfo ptcInfo = SessionCookieUtils.getPTCInfo(httpServletRequest);
		return thirdPartyService.validateSession(ptcInfo, httpServletRequest, httpServletResponse);
	}

	@RequestMapping(value = "/errors", produces = "application/json")
	@ResponseBody
	public ErrorInfo errors() {
		Exception exception = (Exception) httpServletRequest.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
		Integer status = 500;
		ErrorInfo errorInfo;
		if (exception instanceof PortalException) {
			httpServletResponse
					.setStatus(Integer.parseInt(((PortalException) exception).getErrorInfo().getStatusCode()));
			errorInfo = ((PortalException) exception).getErrorInfo();

		} else {
			httpServletResponse.setStatus(status);
			errorInfo = PortalException.populatePortalException(PortalErrorCodeEnum.TPP_ERROR_GENRIC).getErrorInfo();
		}
		return errorInfo;
	}
}

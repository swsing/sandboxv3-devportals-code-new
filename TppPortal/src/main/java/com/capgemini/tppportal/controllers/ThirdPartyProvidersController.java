package com.capgemini.tppportal.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.portal.model.PTCInfo;
import com.capgemini.portal.utilities.SessionCookieUtils;
import com.capgemini.tpp.dtos.ThirdPartyProvidersDetails;
import com.capgemini.tppportal.services.ThirdPartyProvidersService;
/**
 *ThirdPartyProvidersController.java  is used to call the OpenBanking to retrieve the TPP Organizations Details
 *@author tbhola
 */
@RestController
public class ThirdPartyProvidersController {

	@Autowired
	private ThirdPartyProvidersService thirdPartyProvidersService;
	
	@Autowired
	private HttpServletRequest request;
	
	/**
	 * This method is used to retrieve the ThirdPartyProviderDetails 
	 * 1. Retrieve PTCInfo from cookie 
	 * 2. tppOrganisationIds is retrieved from PTCInfo
	 * 3. Call the Service for retrieving the Tpp details corresponding to tppOrganisatin List 
	 * @return List<ThirdPartyProvidersDetails> 
	 */
	
	@RequestMapping(value ="/organizations", method=RequestMethod.GET)
	@ResponseBody
	public List<ThirdPartyProvidersDetails> retrieveThirdPartyProvidersDetails(){
		PTCInfo ptcInfo =SessionCookieUtils.getPTCInfo(request);
		return thirdPartyProvidersService.retrieveThirdPartyProvidersDetails(ptcInfo.getOrganizationIds());
	}

}

package com.capgemini.tppportal.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.tppportal.exception.handler.TppPortalExceptionHandlerImpl;

@Configuration
public class RestClientConfig {
	
	@Bean(name = "tppPortalRestClientMA")
	@Scope(value = "prototype")
	public RestClientSync restClientSyncImplMA() {
		return new RestClientSyncImpl(exceptionHandlImpl());
	}
    
	@Bean
	public ExceptionHandler exceptionHandlImpl() {
		return new TppPortalExceptionHandlerImpl();
	}
	
	@Bean(name = "tppPortalRestClient")
	@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
	public RestClientSync restClientSyncImpl() {
		return new RestClientSyncImpl(exceptionHandlImpl());
	}

	@Bean(name = "tppPortalRestClientMATpp")
	@Scope(value = "prototype")
	public RestClientSync restClientSyncImplMATpp() {
		return new RestClientSyncImpl(exceptionHandlImpl());
	}
}

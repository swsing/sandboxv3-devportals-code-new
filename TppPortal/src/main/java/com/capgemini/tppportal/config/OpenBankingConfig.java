package com.capgemini.tppportal.config;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Load open banking configurations from yaml file.
 */
@Configuration
@ConfigurationProperties(prefix="obAuthconfig")
public class OpenBankingConfig {

	private String clientId;
	private String grantType;
	private String scopes;
	private String clientSecret;
	private String redirectURI;
	private String tokenURL;
	private String hostname;
	private String redirectOBUrl;
	private String authUrlOB;
	private String logoutUrl;
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getGrantType() {
		return grantType;
	}
	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}
	public String getScopes() {
		return scopes;
	}
	public void setScopes(String scopes) {
		this.scopes = scopes;
	}
	public String getClientSecret() {
		return clientSecret;
	}
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	public String getRedirectURI() {
		return String.format(redirectURI,getHostname());
	}
	public void setRedirectURI(String redirectURI) {
		this.redirectURI = redirectURI;
	}
	public String getTokenURL() {
		return tokenURL;
	}
	public void setTokenURL(String tokenURL) {
		this.tokenURL = tokenURL;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getRedirectOBUrl() throws UnsupportedEncodingException {
		String encodedScopes = URLEncoder.encode(scopes, StandardCharsets.UTF_8.name());
		return String.format(redirectOBUrl, authUrlOB,getClientId(),encodedScopes,getRedirectURI());
	}
	public void setRedirectOBUrl(String redirectOBUrl) {
		this.redirectOBUrl = redirectOBUrl;
	}
	public String getAuthUrlOB() {
		return authUrlOB;
	}
	public void setAuthUrlOB(String authUrlOB) {
		this.authUrlOB = authUrlOB;
	}
	public String getLogoutUrl() {
		return logoutUrl;
	}
	public void setLogoutUrl(String logoutUrl) {
		this.logoutUrl = logoutUrl;
	}
}

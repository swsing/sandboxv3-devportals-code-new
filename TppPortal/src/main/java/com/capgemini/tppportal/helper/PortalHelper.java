package com.capgemini.tppportal.helper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.portal.utilities.SessionCookieUtils;
import com.capgemini.psd2.service.PingDirectoryService;

@Component
public class PortalHelper {

	@Autowired
	private PingDirectoryService pingDirectoryService;

	private PortalHelper() {

	}

	private static List<String> blackListedTpp = new ArrayList<>();

	@Value("${app.blackListedTpplist}")
	private void setBlackListedTpp(List<String> blackListedTpps) {
		blackListedTpp = blackListedTpps;
	}
	/**
	 * 
	 * @param request
	 * @param response
	 */
	public static void logout(HttpServletRequest request, HttpServletResponse response) {
		Cookie[] cookiesToClear = request.getCookies();
		if (null != cookiesToClear) {
			for (Cookie cookie : cookiesToClear) {
				cookie.setMaxAge(0);
				cookie.setSecure(true);
				cookie.setHttpOnly(true);
				response.addCookie(cookie);
			}
		}
	}

	/**
	 * Validate the PTC details for tpp.
	 * 1. Given tpp belongs to PTC (current cookie session for PTC)
	 * 2. Verify the tpp block status in ping directory
	 * 
	 * @param request
	 * @param portalRequestHeaderAttributes
	 * @param tppId
	 * @return PTCInfo
	 */
	public PTCInfo validatePTCInfoAndTppBlockedStatus(HttpServletRequest request,
			PortalRequestHeaderAttributes portalRequestHeaderAttributes, String tppId) {

		PTCInfo ptcInfo = SessionCookieUtils.getPTCInfo(request);

		if (!ptcInfo.getOrganizationIds().contains(tppId))
			throw PortalException.populatePortalException(PortalErrorCodeEnum.TPP_CLIENTID_INVALID);

		// TPP should not be in blocked status to execute any operation.
		if (pingDirectoryService.isTppBlockEnabled(tppId))
			throw PortalException.populatePortalException(PortalErrorCodeEnum.TPP_BLOCKED);

		return ptcInfo;
	}

	/**
	 * Remove if any tpp is blacklisted.
	 * 
	 * @param listTpp
	 * @return
	 */
	public static List<String> removeBlockedTpps(List<String> listTpp) {
		if (blackListedTpp.size() == 0) {
			return listTpp;
		}
		listTpp.removeIf(x -> blackListedTpp.contains(x));
		return listTpp;
	}

}
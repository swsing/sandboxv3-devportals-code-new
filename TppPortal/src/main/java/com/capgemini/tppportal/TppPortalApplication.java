package com.capgemini.tppportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.context.annotation.FilterType;

import com.capgemini.portal.filter.CookieHandlerFilter;
import com.capgemini.portal.logger.PortalFilter;
import com.capgemini.tppportal.filter.TppHeaderFilter;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountIdentificationMappingAdapter;
import com.capgemini.psd2.jwt.JWSVerifierImpl;
import com.capgemini.psd2.mongo.config.MongoDBConfiguration;
import com.capgemini.psd2.mongo.config.PSD2MongoTemplate;
import com.capgemini.psd2.utilities.AbstractJWSVerifier;

@SpringBootApplication(exclude={CassandraDataAutoConfiguration.class})
@EnableEurekaClient
@ComponentScan(basePackages = "com.capgemini",excludeFilters={
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = { PSD2MongoTemplate.class,
				MongoDBConfiguration.class,AccountIdentificationMappingAdapter.class,AccountMappingAdapter.class}) })
@EnableRetry
public class TppPortalApplication {

	static ConfigurableApplicationContext context = null;
	
	public static void main(String[] args) {
		context = SpringApplication.run(TppPortalApplication.class, args);
	}
	
	@Bean
	public CookieHandlerFilter cookieHandlerFilter(){
		return new CookieHandlerFilter();
	}
	
	@Bean
	public PortalFilter portalFilter(){
		return new PortalFilter();
	}
	@Bean
	public TppHeaderFilter heaaderFilter(){
		return new TppHeaderFilter();
	}
	
	@Bean
	public AbstractJWSVerifier jwsVerifier(){
		return new JWSVerifierImpl();
	}
}
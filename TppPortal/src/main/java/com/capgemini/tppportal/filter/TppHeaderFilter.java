package com.capgemini.tppportal.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.logger.PortalLoggerUtils;

public class TppHeaderFilter extends OncePerRequestFilter{
	private static final Logger LOG = LoggerFactory.getLogger(TppHeaderFilter.class);
	
	private static final String ABSOLUTE_METHOD_NAME = "com.capgemini.tppportal.filter.TppHeaderFilter.doFilterInternal()";
	
	@Autowired
	private PortalLoggerUtils loggerUtils;
	
	@Value("${app.headerexcludepatterns}")  
	private String headerexcludepatterns;

	/**
	 * This method is called for every request
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		LOG.info("{\"Enter\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"{}\"}",
				ABSOLUTE_METHOD_NAME,request.getRemoteAddr(), request.getRemoteHost(), loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME));		
		populateResponseHeader(response);
		doFilter(request, response, filterChain);
		LOG.info("{\"Exit\":\"{}\",\"{}\"}",
				ABSOLUTE_METHOD_NAME, loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME));

	}
	/**
	 * This method is used to populate the headers in response
	 * @param response
	 */
	private void populateResponseHeader(HttpServletResponse response) {
	    response.setHeader(PortalConstants.X_XSS_PROTECTION_HEADER,PortalConstants.X_XSS_PROTECTION_VALUE);
	    response.setHeader(PortalConstants.CACHE_CONTROL_HEADER,PortalConstants.CACHE_CONTROL_VALUE);
	    response.setHeader(PortalConstants.PRAGMA_HEADER,PortalConstants.PRAGMA_VALUE);
	    response.setHeader(PortalConstants.X_FRAME_OPTIONS_HEADER,PortalConstants.X_FRAME_OPTIONS_VALUE);
	    response.setHeader(PortalConstants.X_CONTENT_TYPE_OPTIONS_HEADER,PortalConstants.X_CONTENT_TYPE_OPTIONS_VALUE);
		response.setHeader(PortalConstants.STRICT_TRANSPORT_SECURITY, PortalConstants.STRICT_TRANSPORT_SECURITY_VALUE);
	}
	/**
	 * The method is used to remove to  requests where filter is not required.
	 */
	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.addAll(Arrays.asList(headerexcludepatterns.split(",")));
		AntPathMatcher matcher = new AntPathMatcher();
		return excludeUrlPatterns.stream().anyMatch(p -> matcher.match(p, request.getServletPath()));
	}
	
}

package com.capgemini.tppportal.transformers;

import java.util.List;

import com.capgemini.tpp.dtos.ThirdPartyProvidersDetails;
import com.capgemini.tpp.ob.model.OBThirdPartyProviders;

public interface ThirdPartyProvidersTransformer {

	public List<ThirdPartyProvidersDetails> tranformToThirdPartyProvidersDetails(List<OBThirdPartyProviders> obThirdPartyProviders);
	
}

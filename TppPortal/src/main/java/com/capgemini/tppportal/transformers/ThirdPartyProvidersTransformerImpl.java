package com.capgemini.tppportal.transformers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.owasp.encoder.Encode;
import org.springframework.stereotype.Component;

import com.capgemini.tpp.dtos.ThirdPartyProvidersDetails;
import com.capgemini.tpp.ob.model.OBThirdPartyProviders;
import com.capgemini.tpp.ob.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations;
import com.capgemini.tpp.ob.model.OrganisationEmailAddresses;
import com.capgemini.tpp.ob.model.OrganisationPhoneNumbers;
/**
 * ThirdPartyProvidersTransformerImpl.java is used to transform the OBThirdPartyProviders data into ThirdPartyProvidersDetails
 * @author tbhola
 *
 */
@Component
public class ThirdPartyProvidersTransformerImpl implements ThirdPartyProvidersTransformer{
    /**
     *  for each OBThirdPartyProviders details in List ,transform into  ThirdPartyProvidersDetails
     */
	@Override
	public List<ThirdPartyProvidersDetails> tranformToThirdPartyProvidersDetails(List<OBThirdPartyProviders> obThirdPartyProviders) {
		List<ThirdPartyProvidersDetails> thirdPartyProvidersDetailList = new ArrayList<>();
		ThirdPartyProvidersDetails thirdPartyProvidersDetail = null;
		for(OBThirdPartyProviders obThirdPartyProvider : obThirdPartyProviders){
			thirdPartyProvidersDetail = populateThirdPartyDetail(obThirdPartyProvider);
			thirdPartyProvidersDetailList.add(thirdPartyProvidersDetail);
		}
		return thirdPartyProvidersDetailList;
	}

	/**
	 * This method is used to populate the OBThirdPartyProviders object data into ThirdPartyProvidersDetails object
	 * @param obThirdPartyProvider - OB ThirdPartyProvider Details
	 * @return ThirdPartyProvidersDetails-  ThirdPartyProvidersDetails object
	 */
	private ThirdPartyProvidersDetails populateThirdPartyDetail(OBThirdPartyProviders obThirdPartyProvider) {
		ThirdPartyProvidersDetails thirdPartyProvidersDetail = new ThirdPartyProvidersDetails();
		thirdPartyProvidersDetail.setTppRoles(new ArrayList<String>());
		thirdPartyProvidersDetail.setOrganizationName(obThirdPartyProvider.getUrnopenbankingorganisation10().getOrganisationCommonName());
		if (!CollectionUtils.isEmpty(obThirdPartyProvider.getUrnopenbankingorganisation10().getEmailAddresses())) {
		thirdPartyProvidersDetail.setTppEmailID(obThirdPartyProvider.getUrnopenbankingorganisation10()
				.getEmailAddresses().stream().map(OrganisationEmailAddresses::getValue).collect(Collectors.toList()));
		}
		thirdPartyProvidersDetail.setTppMemberStateCompetentAuthority(obThirdPartyProvider.getUrnopenbankingcompetentauthorityclaims10().getAuthorityId());
		String tppOrgId = obThirdPartyProvider.getUrnopenbankingorganisation10().getObOrganisationId();
		String jsEncodedTppOrgId = Encode.forJavaScript(tppOrgId);
		String htmlEncodedTppOrgId = Encode.forHtml(jsEncodedTppOrgId);
		thirdPartyProvidersDetail.setTppOrganizationId(htmlEncodedTppOrgId);
		if (!CollectionUtils.isEmpty(obThirdPartyProvider.getUrnopenbankingorganisation10().getPhoneNumbers())) {
		thirdPartyProvidersDetail.setTppPhoneNumber(obThirdPartyProvider.getUrnopenbankingorganisation10()
				.getPhoneNumbers().stream().map(OrganisationPhoneNumbers::getValue).collect(Collectors.toList()));
		}
		thirdPartyProvidersDetail.setTppRegistrationID(obThirdPartyProvider.getUrnopenbankingcompetentauthorityclaims10().getRegistrationId());
		List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations = obThirdPartyProvider.getUrnopenbankingcompetentauthorityclaims10().getAuthorisations();
		for(OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations authorisation :authorisations)
		{
			thirdPartyProvidersDetail.getTppRoles().add(authorisation.getPsd2Role());
		}
		return thirdPartyProvidersDetail;
	}

}

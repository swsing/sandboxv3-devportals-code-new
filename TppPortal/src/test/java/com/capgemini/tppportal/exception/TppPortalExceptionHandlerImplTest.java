package com.capgemini.tppportal.exception;


import java.nio.charset.Charset;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.portal.exception.PortalException;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.tppportal.exception.handler.TppPortalExceptionHandlerImpl;


public class TppPortalExceptionHandlerImplTest {
	
		/** The resource access exception. */
		@Mock
		private ResourceAccessException resourceAccessException;

		/** The http client error exception. */
		@Mock
		private HttpClientErrorException httpClientErrorException;
		
		@Mock
		private HttpServerErrorException httpServerErrorException;

		/** The exception handler impl. */
		@InjectMocks
		private ExceptionHandler exceptionHandlerImpl = new TppPortalExceptionHandlerImpl();

		/**
		 * Test resource access exception.
		 */
		@Test(expected = PortalException.class)
		public void testResourceAccessException() {
			resourceAccessException = new ResourceAccessException("Connetion Time Out");
			exceptionHandlerImpl.handleException(resourceAccessException);
		}

		/**
		 * Test handle exception with client error.
		 */
		@Test(expected = PortalException.class)
		public void testHandleExceptionWithClientErrorTest() {
			ErrorInfo info = new ErrorInfo("123", "message", HttpStatus.BAD_REQUEST.toString());
			/*info.setErrorCode("123");
			info.setErrorMessage("message");*/
			byte[] responseBody = JSONUtilities.getJSONOutPutFromObject(info).getBytes();
			httpClientErrorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad Request", responseBody,
					Charset.forName("UTF-8"));
			exceptionHandlerImpl.handleException(httpClientErrorException);
		}
		@Test(expected = PortalException.class)
		public void testHandleExceptionWithServerErrorTest() {
			ErrorInfo info = new ErrorInfo("123", "message", HttpStatus.BAD_REQUEST.toString());
			/*info.setErrorCode("123");
			info.setErrorMessage("message");*/
			byte[] responseBody = JSONUtilities.getJSONOutPutFromObject(info).getBytes();
			httpServerErrorException = new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Bad Request", responseBody,
					Charset.forName("UTF-8"));
			exceptionHandlerImpl.handleException(httpServerErrorException);
		}
		@Test(expected = PortalException.class)
		public void testHandleExceptionWithServerErrorElseTest() {
			ErrorInfo info = new ErrorInfo("1234","message","error_description", HttpStatus.BAD_REQUEST.toString());
			/*info.setErrorCode("123");
			info.setErrorMessage("message");*/
			byte[] responseBody = JSONUtilities.getJSONOutPutFromObject(info).getBytes();
			httpServerErrorException = new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Bad Request", responseBody,
					Charset.forName("UTF-8"));
			exceptionHandlerImpl.handleException(httpServerErrorException);
		}
		@Test(expected = PortalException.class)
		public void testHandleExceptionWithClientErrorElseTest() {
			ErrorInfo info = new ErrorInfo(null,null,"error_description", HttpStatus.BAD_REQUEST.toString());
			
			byte[] responseBody = JSONUtilities.getJSONOutPutFromObject(info).getBytes();
			httpClientErrorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad Request", responseBody,
					Charset.forName("UTF-8"));
			exceptionHandlerImpl.handleException(httpClientErrorException);
		}
		@Test(expected = PortalException.class)
		public void testHandleExceptionWithClientErrorCatchTest() {
			ErrorInfo info = new ErrorInfo(null,null,"error", HttpStatus.BAD_REQUEST.toString());
			/*info.setErrorCode("123");
			info.setErrorMessage("message");*/
			byte[] responseBody = JSONUtilities.getJSONOutPutFromObject(info).getBytes();
			httpClientErrorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad Request", responseBody,
					Charset.forName("UTF-8"));
			exceptionHandlerImpl.handleException(httpClientErrorException);
		}
		
}

package com.capgemini.tppportal.filter;

import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;

@RunWith(SpringJUnit4ClassRunner.class)
public class TppHeaderFilterTest {

	@InjectMocks
	TppHeaderFilter tppHeaderFilter;

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;
	@Mock
	private PortalLoggerUtils loggerUtils;
	
	@Value("${app.headerexcludepatterns}")  
	private String headerexcludepatterns;
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	@Test(expected = StackOverflowError.class)
	public void doFilterInternalTest() throws ServletException, IOException{
		FilterChain filterChain = new FilterChain() {

			@Override
			public void doFilter(ServletRequest request, ServletResponse response) throws IOException, ServletException {
				// TODO Auto-generated method stub

			}
		};
		ReflectionTestUtils.setField(tppHeaderFilter, "headerexcludepatterns", headerexcludepatterns);
		when(request.getRemoteHost()).thenReturn("10.0.0.1");
		when(request.getRemoteAddr()).thenReturn("1.1.1");
		PortalLoggerAttribute attributes = new PortalLoggerAttribute();
		attributes.setCorrelationId("242354366");
		when(request.getServletPath()).thenReturn("/restart");
		when(loggerUtils.populateLoggerData(PortalConstants.RELATIVE_METHOD_NAME)).thenReturn(attributes );
		tppHeaderFilter.doFilterInternal(request, response, filterChain );
	}

	@After
	public void tearDown() throws Exception {
		tppHeaderFilter = null;

	}

}

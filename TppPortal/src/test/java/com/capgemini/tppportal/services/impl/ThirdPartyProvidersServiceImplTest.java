package com.capgemini.tppportal.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.portal.config.SessionCookieConfig;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.tpp.dtos.ThirdPartyProvidersDetails;
import com.capgemini.tpp.ob.model.OBThirdPartyProviders;
import com.capgemini.tpp.ob.model.OBThirdPartyProvidersUrnopenbankingsoftwarestatement10;
import com.capgemini.tpp.ob.model.Organisation;
import com.capgemini.tppportal.mockdata.TppPortalMockData;
import com.capgemini.tppportal.transformers.ThirdPartyProvidersTransformer;

@RunWith(SpringJUnit4ClassRunner.class)
public class ThirdPartyProvidersServiceImplTest {

	@Mock
	private PortalRequestHeaderAttributes requestAttributes;

	@Mock
	private SessionCookieConfig sessionCookieConfig;

	@Mock
	@Qualifier("tppPortalRestClient")
	private RestClientSync restClient;
	@Mock
	private ThirdPartyProvidersTransformer thirdPartyProvidersTransformer ;

	@InjectMocks
	private  ThirdPartyProvidersServiceImpl service;

	@Mock
	@Qualifier("tppPortalRestClientMATpp")
	private RestClientSync restClientMATpp;

	private PrivateKey privkey;
	
	private static String keystorePassword = "P@ssw0rd";
	private static String keyAlias = "signingcert";
	private String keyPassword = "P@ssw0rd";

	
	@Before
	public  void  setupMockData() {
		MockitoAnnotations.initMocks(this);
		/*try {
			Resource resource = new ClassPathResource("keystore.jks");
			InputStream in = resource.getInputStream();
			KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(in, keystorePassword.toCharArray());
			privkey = (PrivateKey)keystore.getKey(keyAlias,keyPassword.toCharArray());

		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | UnrecoverableKeyException e) {
			throw PortalException.populatePortalException(e.getMessage(), PortalErrorCodeEnum.TECHNICAL_ERROR);
		}*/
		ReflectionTestUtils.setField(service, "keystorePath", "keystore.jks");
		ReflectionTestUtils.setField(service, "keystorePassword", "P@ssw0rd");
		ReflectionTestUtils.setField(service, "keyPassword", "P@ssw0rd");
		ReflectionTestUtils.setField(service, "keyAlias", "signingcert");
		ReflectionTestUtils.setField(service, "clientId",TppPortalMockData.getClientId());
		ReflectionTestUtils.setField(service, "jwtTokenValidity", 1800);
		ReflectionTestUtils.setField(service, "obKeyId",TppPortalMockData.getOBKeyId());
		ReflectionTestUtils.setField(service, "tokenUrl",TppPortalMockData.getTokenUrl() );
		ReflectionTestUtils.setField(service, "scope", TppPortalMockData.getScopeValue());
		ReflectionTestUtils.setField(service, "transportkeyAlias", "transporttpp");
		service.init();
	}
	


	@Test
	public void test() {
		Map<String,String> map =  new HashMap<>();
		map.put("access_token",TppPortalMockData.getAccessToken());
		when(restClientMATpp.callForPost(any(RequestInfo.class), any(), any(), any())).thenReturn(map);
		OBThirdPartyProviders thirdProviderDetails = new OBThirdPartyProviders();
		Organisation urnopenbankingorganisation10 = new Organisation();
		thirdProviderDetails.setUrnopenbankingorganisation10(urnopenbankingorganisation10 );
		OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 urnopenbankingsoftwarestatement10 = new OBThirdPartyProvidersUrnopenbankingsoftwarestatement10();
		thirdProviderDetails.setUrnopenbankingsoftwarestatement10(urnopenbankingsoftwarestatement10 );
		when(restClientMATpp.callForGetWithParams(any(RequestInfo.class), any(), any(), any()))
				.thenReturn(thirdProviderDetails);
		List<ThirdPartyProvidersDetails> thirdProviderDetailsList = new ArrayList<>();
		thirdProviderDetailsList.add(TppPortalMockData.getThirdPartyDetails());

		when(thirdPartyProvidersTransformer.tranformToThirdPartyProvidersDetails(any())).thenReturn(thirdProviderDetailsList);
		assertNotNull(service.retrieveThirdPartyProvidersDetails(TppPortalMockData.getTppOrgList()));
	}

	@Test
	public void testttlMillisNullTest() {
		Map<String,String> map =  new HashMap<>();
		map.put("access_token",TppPortalMockData.getAccessToken());
		when(restClientMATpp.callForPost(any(RequestInfo.class), any(), any(), any())).thenReturn(map);
		OBThirdPartyProviders thirdProviderDetails = new OBThirdPartyProviders();
		Organisation urnopenbankingorganisation10 = new Organisation();
		thirdProviderDetails.setUrnopenbankingorganisation10(urnopenbankingorganisation10 );
		OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 urnopenbankingsoftwarestatement10 = new OBThirdPartyProvidersUrnopenbankingsoftwarestatement10();
		thirdProviderDetails.setUrnopenbankingsoftwarestatement10(urnopenbankingsoftwarestatement10 );
		when(restClientMATpp.callForGetWithParams(any(RequestInfo.class), any(), any(), any()))
				.thenReturn(thirdProviderDetails);
		List<ThirdPartyProvidersDetails> thirdProviderDetailsList = new ArrayList<>();
		thirdProviderDetailsList.add(TppPortalMockData.getThirdPartyDetails());
		when(thirdPartyProvidersTransformer.tranformToThirdPartyProvidersDetails(any())).thenReturn(thirdProviderDetailsList);
		assertNotNull(service.retrieveThirdPartyProvidersDetails(TppPortalMockData.getTppOrgList()));
	}
	@Test(expected=PortalException.class)
	public void testException() {
		Map<String,String> map =  new HashMap<>();
		map.put("access_token",TppPortalMockData.getAccessToken());
		OBThirdPartyProviders thirdProviderDetails = new OBThirdPartyProviders();
		Organisation urnopenbankingorganisation10 = new Organisation();
		thirdProviderDetails.setUrnopenbankingorganisation10(urnopenbankingorganisation10 );
		OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 urnopenbankingsoftwarestatement10 = new OBThirdPartyProvidersUrnopenbankingsoftwarestatement10();
		thirdProviderDetails.setUrnopenbankingsoftwarestatement10(urnopenbankingsoftwarestatement10 );
		when(restClient.callForGetWithParams(any(RequestInfo.class), any(), any(), any())).thenReturn(thirdProviderDetails);
		List<ThirdPartyProvidersDetails> thirdProviderDetailsList = new ArrayList<>();
		thirdProviderDetailsList.add(TppPortalMockData.getThirdPartyDetails());
		when(thirdPartyProvidersTransformer.tranformToThirdPartyProvidersDetails(any())).thenReturn(thirdProviderDetailsList);
		assertNotNull(service.retrieveThirdPartyProvidersDetails(TppPortalMockData.getTppOrgList()));
	}

	@Test(expected = PortalException.class)
	public void testError() {
		Map<String,String> map =  new HashMap<>();
		map.put("access_token",TppPortalMockData.getAccessToken());
		when(restClient.callForPost(any(RequestInfo.class),any(), any(),any())).thenReturn(map);
		OBThirdPartyProviders thirdProviderDetails = new OBThirdPartyProviders();
		Organisation urnopenbankingorganisation10 = new Organisation();
		thirdProviderDetails.setUrnopenbankingorganisation10(urnopenbankingorganisation10 );
		OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 urnopenbankingsoftwarestatement10 = new OBThirdPartyProvidersUrnopenbankingsoftwarestatement10();
		thirdProviderDetails.setUrnopenbankingsoftwarestatement10(urnopenbankingsoftwarestatement10 );
		when(restClient.callForGetWithParams(any(RequestInfo.class), any(), any(), any())).thenReturn(null);
		assertNotNull(service.retrieveThirdPartyProvidersDetails(TppPortalMockData.getTppOrgList()));
	}

	/*@Test
	public void testGetOBPublicKey(){
		String OBPublicKey=TppPortalMockData.getOBPublicKey();
		when(restClient.callForGet(any(),any(),any())).thenReturn(OBPublicKey);
		assertEquals(OBPublicKey, service.getOBPublicKey("https://keystore-stable.openbanking.xyz/keystore/openbanking.jwks"));
	}*/

	@After
	public void tearDown() throws Exception {
		service = null;
	}

}

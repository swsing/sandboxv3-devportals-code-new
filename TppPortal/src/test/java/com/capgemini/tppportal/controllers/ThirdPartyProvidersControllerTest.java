package com.capgemini.tppportal.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.tpp.dtos.ThirdPartyProvidersDetails;
import com.capgemini.tppportal.helper.PortalHelper;
import com.capgemini.tppportal.mockdata.TppPortalMockData;
import com.capgemini.tppportal.services.ThirdPartyProvidersService;

@RunWith(SpringJUnit4ClassRunner.class)
public class ThirdPartyProvidersControllerTest {
	
	private HttpServletRequest request;
	
	private HttpServletResponse response; 
	
	@Mock
	private ThirdPartyProvidersService thirdPartyProvidersService;
	
	@Mock
	private PortalHelper portalHelper;
	
	@InjectMocks
	private ThirdPartyProvidersController controller;
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testRetrieveThirdPartyProvidersDetails() {
		request = mock(HttpServletRequest.class);
		ReflectionTestUtils.setField(controller, "request", request);
		Cookie cookie = new Cookie(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE, "21edwjfhcjsafasdgjsadjf");
		PTCInfo user = new PTCInfo();
		user.setPtcId("21242");
		user.setPtcName("user");
		List<String> organizationIds = new ArrayList<>();
		organizationIds.add("Qsdjsjfjd");
		user.setOrganizationIds(organizationIds );
		String sessionTokenStr = TppPortalMockData.getJwtTokenString(user);
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(1800);
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(request.getCookies()).thenReturn(cookies);
		response = mock(HttpServletResponse.class);
		List<ThirdPartyProvidersDetails> thirdPartyProvidersDetails = new ArrayList<>();
		thirdPartyProvidersDetails.add(TppPortalMockData.getThirdPartyDetails());
		when(thirdPartyProvidersService.retrieveThirdPartyProvidersDetails(any())).thenReturn(thirdPartyProvidersDetails);
		//assertNotNull(controller.retrieveThirdPartyProvidersDetails());*/
	}
	
	@After
	public void tearDown() throws Exception {
		controller = null;

	}
}

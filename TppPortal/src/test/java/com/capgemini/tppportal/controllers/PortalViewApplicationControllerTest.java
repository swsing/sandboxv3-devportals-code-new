package com.capgemini.tppportal.controllers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.capgemini.portal.config.SessionCookieConfig;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.psd2.service.PortalService;
import com.capgemini.tpp.dtos.ViewApplicationResponse;
import com.capgemini.tpp.ldap.client.model.TPPApplication;
import com.capgemini.tpp.model.ValidateSessionResponse;
import com.capgemini.tppportal.services.ThirdPartyProvidersService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class PortalViewApplicationControllerTest {

	@InjectMocks
	private PortalViewApplicationController portalViewApplicationController;

	@Mock
	TPPApplication tPPApplication;

	@Mock
	ViewApplicationResponse viewApplicationResponse;

	@Mock
	PingDirectoryService pingDirectoryService;

	@Mock
	ThirdPartyProvidersService thirdPartyService;

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	PortalRequestHeaderAttributes portalRequestHeaderAttributes;

	@Mock
	SessionCookieConfig sessionCookieConfig;

	@Before
	public void setupMock() throws PortalException {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void loadApplicationsByTPPOrgIdTest() {
		List<String> RedirectUrl = new ArrayList<>();
		RedirectUrl.add("www.abcd.com");
		Date date = new Date();
		String tppOrgId = "Q5wqFjpnTAeCtDc1Qx";
		Mockito.when(viewApplicationResponse.getTppOrgId()).thenReturn("Q5wqFjpnTAeCtDc1Qx");
		List<TPPApplication> tppApp = new ArrayList<>();
		TPPApplication tppApplist = new TPPApplication();
		tppApplist.setClientId("1234");
		tppApplist.setCn("abcd");
		tppApplist.setCreateTimestamp(date);
		tppApplist.setJwksUrl("www.abcd.com");
		tppApplist.setMuleAppId("11024");
		tppApplist.setPolicyURI("www.abcd.com");
		tppApplist.setRedirectUrl(RedirectUrl);
		tppApplist.setSoftwareClientDescription("abcd");
		tppApplist.setSoftwareClientName("abcd");
		List<String> softwareRoles = new ArrayList<>();
		softwareRoles.add("PISP");
		tppApplist.setSoftwareRoles(softwareRoles);
		tppApplist.setSoftwareVersion(1.0);
		tppApplist.setStatus("Approved");
		tppApplist.setTermOfServiceURI("www.abcd.com");

		tppApp.add(tppApplist);

		TPPApplication tppApplist1 = new TPPApplication();
		List<String> RedirectUrls = new ArrayList<>();
		RedirectUrls.add("www.xyz.com");
		tppApplist1.setClientId("2345");
		tppApplist1.setCn("xyz");
		tppApplist1.setCreateTimestamp(date);
		tppApplist1.setJwksUrl("www.xyz.com");
		tppApplist1.setMuleAppId("12024");
		tppApplist1.setPolicyURI("www.xyz.com");
		tppApplist1.setRedirectUrl(RedirectUrls);
		tppApplist1.setSoftwareClientDescription("xyz");
		tppApplist1.setSoftwareClientName("xyz");
		List<String> softwareRole = new ArrayList<>();
		softwareRoles.add("AISP");
		tppApplist1.setSoftwareRoles(softwareRole);
		tppApplist1.setSoftwareVersion(2.0);
		tppApplist1.setStatus("Approved");
		tppApplist1.setTermOfServiceURI("www.xyz.com");

		tppApp.add(tppApplist);
		tppApp.add(tppApplist1);

		Mockito.when(pingDirectoryService.fetchListOfTPPApplications(tppOrgId)).thenReturn(tppApp);
		Mockito.when(viewApplicationResponse.getTppAppList()).thenReturn(tppApp);
		Mockito.when(pingDirectoryService.isTppBlockEnabled(tppOrgId)).thenReturn(false);
		Mockito.when(viewApplicationResponse.isTPPBlocked()).thenReturn(false);

		assertNotNull(portalViewApplicationController.loadApplicationsByTPPOrgId(tppOrgId));

	}

	@Test(expected = PortalException.class)
	public void loadApplicationsByTPPOrgIdCatchTest() {
		when(pingDirectoryService.fetchListOfTPPApplications(null)).thenThrow(PortalException.class);
		portalViewApplicationController.loadApplicationsByTPPOrgId(null);
	}

	@Test
	public void validateSessionTest() {
		ValidateSessionResponse value = new ValidateSessionResponse();
		PTCInfo ptcInfo = new PTCInfo();
		Mockito.when(thirdPartyService.validateSession(ptcInfo, request, response)).thenReturn(value);
		Mockito.when(sessionCookieConfig.getTokenSigningKey()).thenReturn("abcd");
		portalViewApplicationController.validateSession();
	}

	@Test
	public void errorsTestElseTest() {
		Exception exception = new Exception();
		Mockito.when(request.getAttribute(RequestDispatcher.ERROR_EXCEPTION)).thenReturn(exception);
		portalViewApplicationController.errors();
	}

	@Test
	public void errorsTestIfTest() {
		ErrorInfo info = new ErrorInfo("123", "message", HttpStatus.BAD_REQUEST.toString());
		PortalException exception = new PortalException("error", info);
		Mockito.when(request.getAttribute(RequestDispatcher.ERROR_EXCEPTION)).thenReturn(exception);
		portalViewApplicationController.errors();
	}

	@After
	public void tearDown() throws PortalException {
		portalViewApplicationController = null;
	}

}

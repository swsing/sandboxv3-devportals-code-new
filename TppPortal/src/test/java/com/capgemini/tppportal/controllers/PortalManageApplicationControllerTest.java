package com.capgemini.tppportal.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.psd2.service.PortalService;
import com.capgemini.psd2.service.impl.PortalServiceImpl;
import com.capgemini.tppportal.helper.PortalHelper;
import com.capgemini.tppportal.mockdata.TppPortalMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class PortalManageApplicationControllerTest {
	
	private HttpServletRequest request;

	@Mock
	private PortalRequestHeaderAttributes requestAttributes;

	@Mock
	PortalRequestHeaderAttributes portalRequestHeaderAttributes;

	@Mock
	private PortalService portalService;
	
	@Mock
	private PortalHelper portalHelper;
	
	@Mock 
	private PortalServiceImpl portalServiceImpl;
	
	@Mock
	private PingDirectoryService pingDirectoryService;

	@Value("${aws.region:eu-west-1}")
	private String awsRegion;

	@InjectMocks
	private PortalManageApplicationController portalManageApplicationController;
	
	private PTCInfo user = new PTCInfo();
	
	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
		request = mock(HttpServletRequest.class);
		//ReflectionTestUtils.setField(portalHelper, "sessionCookieConfig", sessionCookieConfig);
		ReflectionTestUtils.setField(portalManageApplicationController, "request", request);
		ReflectionTestUtils.setField(portalManageApplicationController, "requestAttributes", requestAttributes);
		//when(sessionCookieConfig.getTokenSigningKey()).thenReturn(TppPortalMockData.getTokenSigningKey());
		//when(sessionCookieConfig.getJweDecryptionKey()).thenReturn(TppPortalMockData.getJweDecryptionKey());
		Cookie cookie = new Cookie(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE, "21edwjfhcjsafasdgjsadjf");
		user.setPtcName("TestUser");
		user.setPtcId("testuser@capgemini.com");
		user.setOrganizationIds(Arrays.asList("Qsdjsjfjd", "Q5wqFjpnTAeCtDc1Qx", "nBRcYAcACnghbGFOBk"));
		String sessionTokenStr = TppPortalMockData.getJwtTokenString(user);
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(1800);
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(request.getCookies()).thenReturn(cookies);
	}
	
	@Test(expected = PortalException.class)
	public void showSecretNullTest(){
		
		String applicationId="1234";
		portalManageApplicationController.showSecret(null, null, applicationId);
	}
	@Test
	public void showSecretptcInfoTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add(clientId);
	    Mockito.when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		portalManageApplicationController.showSecret(tppId, clientId, applicationId);
	}
	
	@Test(expected=PortalException.class)
	public void showSecretptcInfoThrowTest(){
		String tppId = "1234";
		String clientId="4567";
		String applicationId="110245";
		portalManageApplicationController.showSecret(tppId, clientId, applicationId);
	}
	@Test(expected=PortalException.class)
	public void showSecretptcInfoTppIdTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add(clientId);
	    Mockito.when(pingDirectoryService.isTppBlockEnabled("1234")).thenReturn(Boolean.FALSE);
		portalManageApplicationController.showSecret(tppId, clientId, applicationId);
	}
	
	@Test(expected=PortalException.class)
	public void showSecretptcInfoClientIDTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add("1234");
	    Mockito.when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		portalManageApplicationController.showSecret(tppId, clientId, applicationId);
	}
	
	@Test(expected=PortalException.class)
	public void showSecretptcInfoNullClientIDTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add("1234");
	    Mockito.when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		portalManageApplicationController.showSecret(tppId, null, applicationId);
	}
	
	@Test(expected=PortalException.class)
	public void showSecretptcInfoClientID1Test(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		
		List<String> ClientId = new ArrayList<>();
		ClientId.add("1234");
	    Mockito.when(pingDirectoryService.isTppBlockEnabled("1234")).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		portalManageApplicationController.showSecret(tppId, clientId, null);
	}
	
	@Test(expected = PortalException.class)
	public void showSecretptcInfoCatchTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add(clientId);
	    Mockito.when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
	    ErrorInfo errorInfo = new ErrorInfo();
	    errorInfo.setErrorCode("400");
	    when(portalService.fetchClientSecret(clientId, applicationId)).thenThrow(new PortalException("msg", errorInfo));
		portalManageApplicationController.showSecret(tppId, clientId, applicationId);
	}
	
	@Test
	public void showSecretptcInfoCatchTPPBlockedTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add(clientId);
	    Mockito.when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.TRUE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
	    ErrorInfo errorInfo = new ErrorInfo();
	    errorInfo.setErrorCode("400");
		portalManageApplicationController.showSecret(tppId, clientId, applicationId);
	}
	
	@Test
	public void resetClientSecretptcInfoTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add(clientId);
	    Mockito.when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		portalManageApplicationController.showSecret(tppId, clientId, applicationId);
	}
	@Test
	public void resetClientSecretptcInfo1Test(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add(clientId);
	    Mockito.when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		portalManageApplicationController.resetClientSecret(tppId, clientId, applicationId);
	}
	
	@Test(expected=PortalException.class)
	public void resetClientSecretptcInfoThrowTest(){
		String tppId = "1234";
		String clientId="4567";
		String applicationId="110245";
		portalManageApplicationController.resetClientSecret(tppId, clientId, applicationId);
	}
	
	@Test(expected = PortalException.class)
	public void resetClientSecretNullTest(){
		
		String applicationId="1234";
		portalManageApplicationController.resetClientSecret(null, null, applicationId);
	}
	
	@Test(expected=PortalException.class)
	public void resetClientSecretptcInfoTppIdTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add(clientId);
	    Mockito.when(pingDirectoryService.isTppBlockEnabled("1234")).thenReturn(Boolean.FALSE);
		portalManageApplicationController.resetClientSecret(tppId, clientId, applicationId);
	}
	@Test(expected=PortalException.class)
	public void restClientSecretTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add(clientId);
	    Mockito.when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		Mockito.when(portalService.resetClientSecret(clientId, applicationId)).thenThrow(PortalException.class);
		portalManageApplicationController.resetClientSecret(tppId, clientId, applicationId);
	}
	@Test(expected=PortalException.class)
	public void resetClientSecretptcInfoClientIDTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add("1234");
	    Mockito.when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		portalManageApplicationController.resetClientSecret(tppId, clientId, applicationId);
	}
	
	@Test(expected=PortalException.class)
	public void resetClientSecretptcInfoNullClientIDTest(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String applicationId="110245";
		List<String> ClientId = new ArrayList<>();
		ClientId.add("1234");
	    Mockito.when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		portalManageApplicationController.resetClientSecret(tppId, null, applicationId);
	}
	
	@Test(expected=PortalException.class)
	public void resetClientSecretptcInfoClientID1Test(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String clientId="4567";
		
		List<String> ClientId = new ArrayList<>();
		ClientId.add("1234");
	    Mockito.when(pingDirectoryService.isTppBlockEnabled("1234")).thenReturn(Boolean.FALSE);
	    Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		portalManageApplicationController.resetClientSecret(tppId, clientId, null);
	}
	
	@Test
	public void deleteTppApplication(){
		String tppId = "Q5wqFjpnTAeCtDc1Qx";
		String applicationId="110245";
		String clientId="1234";
		List<String> ClientId = new ArrayList<>();
		ClientId.add("1234");
		 Mockito.when(pingDirectoryService.getClientID(tppId)).thenReturn(ClientId);
		portalManageApplicationController.deleteTppApplication(tppId, clientId, applicationId);
	}
	


}

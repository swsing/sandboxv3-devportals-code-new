package com.capgemini.tppportal.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import com.capgemini.portal.aspect.PortalAspectUtils;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalLoggerAttribute;
import com.capgemini.portal.logger.PortalLoggerUtils;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.psd2.jwt.JWTReader;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.psd2.service.PortalService;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.tpp.dtos.ApplicationResponse;
import com.capgemini.tpp.dtos.TPPDetails;
import com.capgemini.tppportal.helper.PortalHelper;
import com.capgemini.tppportal.mockdata.TppPortalMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class PortalAddApplicationControllerTest {

	private HttpServletRequest request;

	private MockMvc mockMvc;

	@Mock
	private PortalRequestHeaderAttributes requestAttributes;

	@Mock
	PortalRequestHeaderAttributes portalRequestHeaderAttributes;

	@Mock
	private PortalService portalService;

	@Mock
	private PortalHelper portalHelper;

	@Mock
	private PingDirectoryService pingDirectoryService;

	@Mock
	private PortalLoggerUtils loggerUtils;

	@Mock
	private PortalLoggerAttribute loggerAttribute;

	@Mock
	private PortalAspectUtils portalAspectUtils;

	@Value("${aws.region:eu-west-1}")
	private String awsRegion;

	@InjectMocks
	private PortalAddApplicationController portalAddApplicationController;

	private String token;

	private PTCInfo user = new PTCInfo();

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(loggerAttribute);

		request = mock(HttpServletRequest.class);
		//ReflectionTestUtils.setField(portalHelper, "sessionCookieConfig", sessionCookieConfig);
		ReflectionTestUtils.setField(portalAddApplicationController, "request", request);
		ReflectionTestUtils.setField(portalAddApplicationController, "requestAttributes", requestAttributes);
		//when(sessionCookieConfig.getTokenSigningKey()).thenReturn(TppPortalMockData.getTokenSigningKey());
		//when(sessionCookieConfig.getJweDecryptionKey()).thenReturn(TppPortalMockData.getJweDecryptionKey());
		Cookie cookie = new Cookie(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE, "21edwjfhcjsafasdgjsadjf");
		
		user.setPtcName("TestUser");
		user.setPtcId("testuser@capgemini.com");
		user.setOrganizationIds(Arrays.asList("Qsdjsjfjd", "Q5wqFjpnTAeCtDc1Qx", "nBRcYAcACnghbGFOBk"));
		String sessionTokenStr = TppPortalMockData.getJwtTokenString(user);
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(1800);
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(request.getCookies()).thenReturn(cookies);
		mockMvc = MockMvcBuilders.standaloneSetup(portalAddApplicationController).dispatchOptions(true).build();
		token = TppPortalMockData.getSSAToken();

	}

	@Test(expected = NestedServletException.class)
	public void testCreateApplicationTppOrgIdNotOfPTCException() throws Exception {
		when(portalService.decodeAndVerifyToken(token)).thenReturn(JWTReader.decodeTokenIntoSSAModel(token));
		when(portalHelper.validatePTCInfoAndTppBlockedStatus(any(), any(), any())).thenReturn(user);
		mockMvc.perform(
				MockMvcRequestBuilders.fileUpload("/application").param("file",new String(token.getBytes())).contentType(MediaType.APPLICATION_JSON));
	}
	
	@Test(expected = NestedServletException.class)
	public void testCreateApplicationNullFileException() throws Exception {
		when(portalService.decodeAndVerifyToken(token)).thenReturn(JWTReader.decodeTokenIntoSSAModel(token));
		when(portalHelper.validatePTCInfoAndTppBlockedStatus(any(), any(), any())).thenReturn(user);
		mockMvc.perform(
				MockMvcRequestBuilders.fileUpload("/application").param("file","").param("tppOrgId", "Qsdjsjfjd").contentType(MediaType.APPLICATION_JSON));
	}
	
	@Test(expected = NestedServletException.class)
	public void testCreateApplicationServiceException() throws Exception {
		when(portalService.decodeAndVerifyToken(any())).thenReturn(JWTReader.decodeTokenIntoSSAModel(token));
		doThrow(Exception.class).when(portalService).createTppApplication(any(),any(),any());
		when(portalHelper.validatePTCInfoAndTppBlockedStatus(any(), any(), any())).thenReturn(user);
		mockMvc.perform(
				MockMvcRequestBuilders.fileUpload("/application").param("file",new String(token.getBytes())).param("tppOrgId", "nBRcYAcACnghbGFOBk").contentType(MediaType.APPLICATION_JSON));
	}

	@Test(expected = NestedServletException.class)
	public void testCreateApplicationInvalidTokenException() throws Exception {
		when(portalService.decodeAndVerifyToken(token)).thenThrow(Exception.class);
		mockMvc.perform(MockMvcRequestBuilders.fileUpload("/application").param("file",new String(token.getBytes())).param("tppOrgId", "Qsdjsjfjd")
				.contentType(MediaType.APPLICATION_JSON));
	}

	/*@Test(expected = NestedServletException.class)
	public void testCreateApplicationExpiredTokenException() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file", "file", null, token.getBytes());
		when(portalService.decodeAndVerifyToken(token)).thenThrow(TokenExpiredException.class);
		mockMvc.perform(MockMvcRequestBuilders.fileUpload("/application").file(file).param("tppOrgId", "Qsdjsjfjd")
				.contentType(MediaType.APPLICATION_JSON));
	}*/

	@Test(expected = NestedServletException.class)
	public void testCreateApplicationTppOrgIdNotOfSelectedOrgException() throws Exception {
		when(portalService.decodeAndVerifyToken(token)).thenReturn(JWTReader.decodeTokenIntoSSAModel(token));
		List<String> organizationIds = new ArrayList<>();
		organizationIds.add("Qsdjsjfjd");
		organizationIds.add("Q5wqFjpnTAeCtDc1Qx");
		user.setOrganizationIds(organizationIds);
		when(portalHelper.validatePTCInfoAndTppBlockedStatus(any(), any(), any())).thenReturn(user);
		mockMvc.perform(MockMvcRequestBuilders.fileUpload("/application").param("file",new String(token.getBytes())).param("tppOrgId", "Qsdjsjfjd")
				.contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	public void testCreateApplication() throws Exception {
		when(portalService.decodeAndVerifyToken(token)).thenReturn(JWTReader.decodeTokenIntoSSAModel(token));
		portalService.createTppApplication(any(), any(), any());
		when(portalHelper.validatePTCInfoAndTppBlockedStatus(any(), any(), any())).thenReturn(user);
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.fileUpload("/application").param("file",new String(token.getBytes()))
						.param("tppOrgId", "nBRcYAcACnghbGFOBk").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		String content = result.getResponse().getContentAsString();
		ApplicationResponse addApplicationReponse = JSONUtilities.getObjectFromJSONString(content,
				ApplicationResponse.class);
		assertEquals("Test Dec", addApplicationReponse.getTppApplicationName());
	}

	@Test
	public void testDecodeTppTokenFromFile() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file", "file", null, token.getBytes());
		when(portalService.decodeAndVerifyToken(token)).thenReturn(JWTReader.decodeTokenIntoSSAModel(token));
		TPPDetails tppDetails = TppPortalMockData.getTppDetails();
		when(portalService.fetchTppDetailsFromSSAToken(any())).thenReturn(tppDetails);
		when(portalHelper.validatePTCInfoAndTppBlockedStatus(any(), any(), any())).thenReturn(user);
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.fileUpload("/ssaToken").file(file).param("tppOrgId", "nBRcYAcACnghbGFOBk"))
				.andExpect(status().isOk()).andReturn();
		String content = result.getResponse().getContentAsString();
		TPPDetails tppDetailsExpected = JSONUtilities.getObjectFromJSONString(content, TPPDetails.class);
		assertEquals(tppDetails.getClientId(), tppDetailsExpected.getClientId());
		assertEquals(tppDetails.getApplicationName(), tppDetailsExpected.getApplicationName());
	}

	@Test(expected = PortalException.class)
	public void validateRequestTest() {
		portalAddApplicationController.validateRequest("1234", null);
	}

	@Test(expected = PortalException.class)
	public void validateRequestEmptyFileTest() {
		portalAddApplicationController.validateRequest("1234", "");
	}

	@Test(expected = PortalException.class)
	public void validateRequestTPPBlockedTest() {
		String tppOrgId = "123";
		when(pingDirectoryService.isTppBlockEnabled(tppOrgId)).thenReturn(true);
		portalAddApplicationController.validateRequest(tppOrgId, new String(token.getBytes()));
	}

	@Test(expected = NestedServletException.class)
	public void testDecodeTppTokenFromCookieFile() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file", "file", null, token.getBytes());
		when(portalService.decodeAndVerifyToken(token)).thenReturn(JWTReader.decodeTokenIntoSSAModel(token));
		TPPDetails tppDetails = TppPortalMockData.getTppDetails();
		when(portalService.fetchTppDetailsFromSSAToken(any())).thenReturn(tppDetails);

		Cookie cookie = new Cookie(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE, "21edwjfhcjsafasdgjsadjf");
		
		List<String> organizationIds = new ArrayList<>();
		organizationIds.add("abcd");
		user.setOrganizationIds(organizationIds);
		String sessionTokenStr = TppPortalMockData.getJwtTokenString(user);
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(1800);
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(request.getCookies()).thenReturn(cookies);

		mockMvc.perform(
				MockMvcRequestBuilders.fileUpload("/ssaToken").file(file).param("tppOrgId", "nBRcYAcACnghbGFOBk"))
				.andExpect(status().isOk()).andReturn();

	}

	@Test(expected = NestedServletException.class)
	public void testDecodeTppTokenFromTokenFile() throws Exception {
		token = TppPortalMockData.getInvalidOrgStatusToken();
		MockMultipartFile file = new MockMultipartFile("file", "file", null, token.getBytes());
		when(portalService.decodeAndVerifyToken(token)).thenReturn(JWTReader.decodeTokenIntoSSAModel(token));
		TPPDetails tppDetails = TppPortalMockData.getTppDetails();
		when(portalService.fetchTppDetailsFromSSAToken(any())).thenReturn(tppDetails);
		mockMvc.perform(
				MockMvcRequestBuilders.fileUpload("/ssaToken").file(file).param("tppOrgId", "nBRcYAcACnghbGFOBk"))
				.andExpect(status().isOk()).andReturn();

	}

	@After
	public void tearDown() throws Exception {
		portalAddApplicationController = null;
	}
}

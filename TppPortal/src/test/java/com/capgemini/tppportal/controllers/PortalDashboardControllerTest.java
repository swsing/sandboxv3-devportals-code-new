package com.capgemini.tppportal.controllers;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.capgemini.portal.config.SessionCookieConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.enums.PortalErrorCodeEnum;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.portal.utilities.SessionCookieUtils;
import com.capgemini.psd2.jwt.JWTReader;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.AbstractJWSVerifier;
import com.capgemini.tppportal.config.OpenBankingConfig;
import com.capgemini.tppportal.helper.PortalHelper;
import com.capgemini.tppportal.mockdata.TppPortalMockData;
import com.capgemini.tppportal.services.ThirdPartyProvidersService;
import com.nimbusds.jose.JOSEException;

@RunWith(MockitoJUnitRunner.class)
public class PortalDashboardControllerTest {

	@InjectMocks
	private PortalDashboardController portalDashboardController;
	
	@Mock
	private ModelAndView homeView;
	
	@InjectMocks
	private SessionCookieUtils sessionCookieUtils;
	
	@Mock
	private HttpServletRequest httpServletRequest;
	
	@Mock
	private HttpServletResponse httpServletResponse;

	@Mock
	PortalRequestHeaderAttributes portalRequestHeaderAttributes;
	
	@Mock
	private UIStaticContentUtilityController uiStaticContentcontroller;

	@Mock
	Exception ex;
	
	@Mock
	private OpenBankingConfig obConfig;
	
	@Mock
	private ThirdPartyProvidersService thirdPartyProvidersService;
	
	@Mock
	@Qualifier("tppPortalRestClientMA")
	private RestClientSync restClientMA;
	
	@Value("${obAuthconfig.jwks.endpoint}")  
	private String obJWKSEndpoint;
	
	private PTCInfo user;
	
	private Cookie cookie;
	
	private boolean isTestMode;

	@InjectMocks
	private static SessionCookieConfig staticsessionCookieConfig;

	@InjectMocks
	private static SessionCookieConfig sessionCookieConfig;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		when(portalRequestHeaderAttributes.getCorrelationId()).thenReturn("1124");
		staticsessionCookieConfig.setTokenIssuer("12133");
		sessionCookieConfig.setTokenIssuer("12133");
		sessionCookieConfig.setSessiontimeout(100);
		sessionCookieConfig.setTokenSigningKey("2K4M5N7Q8R9TBUCVEXFYG2J3K4N6P7Q9SATBUDWEXFZH2J3M5N6P8R9SAUqwerty");
		sessionCookieConfig.setJweDecryptionKey("GmnfWICOPrsxsxmb");
		ReflectionTestUtils.setField(sessionCookieUtils, "sessionCookieConfig", sessionCookieConfig);
		this.sessionCookieUtils.init();
		cookie = new Cookie(PortalConstants.TPPPORTAL_TEMP_COOKIE, "21edwjfhcjsafasdgjsadjf");
	}
	
	public static final String LOGOUTURL = "logoutURL";
	public static final String SAMLATTRIBUTE = "samlAttributes";
	public static final String ERRORMESSAGE = "errorMessage";
	public static final String ERROR = "error";
	public static final String SAML_ATTRIBUTE_GIVEN_NAME = "given_name";
	public static final String EXCEPTION = "Exception";
	public static final String CORELATION_ID="corelationId";
	public static final String TPP_ASSOCIATIONS_LIST="tpp_associations_list";
	public static final String SUBJECT="subject";
	public static final String PTC_NAME="username";
	public static final String PTC_ID="subject";
	public static final String SAML_ATTRIBUTE_FAMILY_NAME="family_name";
	public static final String REDIRECT_URL="https://test.com";
	public static final String ENTITYBASE="entityBaseURL";
	public static final String UISTATICCONTENT="uiStaticContent";
	
	
  
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	@Test
	public void redirectTest() throws IOException{
		isTestMode = false;
		when(obConfig.getRedirectOBUrl()).thenReturn(REDIRECT_URL);
		portalDashboardController.redirect();
	}
	
	
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	@Test
	public void redirectTestMode() throws IOException {
		isTestMode = true;
		ReflectionTestUtils.setField(portalDashboardController, "isTestMode", isTestMode);
		PTCInfo ptcInfo = new PTCInfo();
		ptcInfo.setPtcName("TestUser");
		ptcInfo.setPtcId("testuser@capgemini.com");
		List<String> tpps = new ArrayList(Arrays.asList("0015800000jfQ9aAAE"));
		ptcInfo.setOrganizationIds(PortalHelper.removeBlockedTpps(tpps));
		httpServletResponse.addCookie(cookie);
		httpServletResponse.sendRedirect(ENTITYBASE + "/home");
		portalDashboardController.redirect();
	}

	@Test
	public void homeTest(){
		homeView = new ModelAndView("index");
		homeView.addObject(LOGOUTURL, LOGOUTURL);
		homeView.addObject(CORELATION_ID, "xyz");
		homeView.addObject("username", "Naveen");
		httpServletResponse.addCookie(cookie);
	}
	
	@Test
	public void homeTempCookieNotNullPDCTest() {
		homeView = new ModelAndView("index");
		homeView.addObject(LOGOUTURL, LOGOUTURL);
		homeView.addObject(CORELATION_ID, "xyz");
		homeView.addObject("username", "Naveen");
		httpServletResponse.addCookie(cookie);
	}

	@Test(expected = PortalException.class)
	public void homeTestCatch()  {
		PortalException e = PortalException.populatePortalException(PortalErrorCodeEnum.LOGIN_ERROR);
		throw e;
	}
	
    @Test
	public void handleExceptionTest()  {
		homeView = new ModelAndView("index");
		PortalException e = PortalException.populatePortalException(PortalErrorCodeEnum.LOGIN_ERROR);
		homeView.addObject(ERROR, PortalException.populatePortalException(PortalErrorCodeEnum.LOGIN_ERROR).getErrorInfo());
		homeView.addObject(CORELATION_ID, "xyz");
		//when(utils.getPortalBaseURL()).thenReturn(LOGOUTURL);
		//when(utils.getIDPLogoutURL()).thenReturn("");
		when(portalRequestHeaderAttributes.getCorrelationId()).thenReturn("xyz");
		Assert.assertEquals(homeView.getModel().get(CORELATION_ID), portalDashboardController.handleException(e).getModel().get(CORELATION_ID));
		when(uiStaticContentcontroller.getStaticContentForUI()).thenReturn(UISTATICCONTENT);
		Assert.assertEquals(homeView.getView(), portalDashboardController.handleException(e).getView());
	}
    
    @Test
   	public void testErrorHtml()  {
   		homeView = new ModelAndView("index");
   		PortalException e = PortalException.populatePortalException(PortalErrorCodeEnum.LOGIN_ERROR);
   		homeView.addObject(ERROR, e.getErrorInfo());
   		homeView.addObject(CORELATION_ID, "xyz");
   		//when(utils.getPortalBaseURL()).thenReturn(LOGOUTURL);
		//when(utils.getIDPLogoutURL()).thenReturn("");
   		when(portalRequestHeaderAttributes.getCorrelationId()).thenReturn("xyz");
   		Assert.assertEquals(homeView.getModel().get(CORELATION_ID), portalDashboardController.errorHtml().getModel().get(CORELATION_ID));
   		when(uiStaticContentcontroller.getStaticContentForUI()).thenReturn(UISTATICCONTENT);
   		Assert.assertEquals(homeView.getView(), portalDashboardController.errorHtml().getView());
   	}
    
    @Test
    public void redirectResponseTestIf() throws IOException{
    	Map<String, String> parameters = new HashMap<>();
		portalDashboardController.redirectResponse(parameters);
		
    }
    
    @Test
    public void logoutTest() throws IOException{
    	portalDashboardController.logout();
    }
    
    @Test
    public void redirectResponseTest() throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException{
		ReflectionTestUtils.setField(portalDashboardController, "restClientMA", restClientMA);
    	ReflectionTestUtils.setField(portalDashboardController,"obJWKSEndpoint","https://auth.mit.openbanking.qa/pf/JWKS");
    	Map<String, String> parameters = new HashMap<>();
    	parameters.put("code", "code");
    	RequestInfo requestInfo = new RequestInfo();
    	requestInfo.setUrl("https://auth.mit.openbanking.qa/as/token.oauth2");
    	MultiValueMap<String,String> inputBody = new LinkedMultiValueMap<>();
		Map<String,Object> token=null;
		obConfig.setClientId("1234");
		inputBody.add("client_id", "1234");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		Map<String, String> parameter = new HashMap<>();
		parameter.put("id_token", TppPortalMockData.getSSAToken());
		//assertNotNull(JWTReader.verifyAndDecodeTokenIntoMap(TppPortalMockData.getSSAToken(),TppPortalMockData.getOBPublicKey()));
		// assertNotNull(portalDashboardController.redirectResponse(parameters));
    }
    
    @Test(expected = PortalException.class)
    public void redirectResponseCatchTest() throws IOException{
    
    	Map<String, String> parameters = new HashMap<>();
    	parameters.put("code", "code");
		HttpHeaders headers = new HttpHeaders();
		Map<String, String> parameter = new HashMap<>();
		parameter.put("access_token", TppPortalMockData.getSSAToken());
		portalDashboardController.redirectResponse(parameters);
		}
    
    @Test
    public void loginPageTest(){
    	assertNotNull(portalDashboardController.loginPage());
    }
    
    @Test
    public void loginPageTestStaleCookieCase(){
    	Cookie[] cookies = new Cookie[1];
    	Cookie cookie = new Cookie("TPPPORTAL_SESSION_COOKIE", null);
    	cookies[0]=cookie;
    	when(httpServletRequest.getCookies()).thenReturn(cookies);
    	assertNotNull(portalDashboardController.loginPage());
    }
    
	@SuppressWarnings("static-access")
	@Test
	public void homeNullTest(){
		user = new PTCInfo();
		user.setPtcId("21242");
		user.setPtcName("user");
		Cookie cookie = new Cookie("TPPPORTAL_TEMP_COOKIE", null);
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(httpServletRequest.getCookies()).thenReturn(cookies);
		Cookie cookie1 = sessionCookieUtils.findCookie(httpServletRequest,"TPPPORTAL_TEMP_COOKIE");
		assertTrue(cookie.getName().equals(cookie1.getName()));
		
		cookies = new Cookie[2];
		cookie = new Cookie("Dummy", null);
		cookies[0] = cookie;
		Cookie cookie2 = new Cookie("Dummy2", null);
		cookies[1] = cookie2;
		when(httpServletRequest.getCookies()).thenReturn(cookies);
		cookie1 = sessionCookieUtils.findCookie(httpServletRequest,"Dummy");
		
		Model model = new Model() {
			
			@Override
			public Model mergeAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean containsAttribute(String attributeName) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Map<String, Object> asMap() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(String attributeName, Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAttribute(Object attributeValue) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Map<String, ?> attributes) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Model addAllAttributes(Collection<?> attributeValues) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		portalDashboardController.home(model);
		
	}

}

package com.capgemini.tppportal.transformers;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.capgemini.tpp.ob.model.OBThirdPartyProviders;
import com.capgemini.tppportal.mockdata.TppPortalMockData;

public class ThirdPartyProvidersTransformerImplTest {

	private ThirdPartyProvidersTransformerImpl transformer;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		transformer = new ThirdPartyProvidersTransformerImpl();
	}
	
	@Test
	public void test() {
		List<OBThirdPartyProviders> obThirdPartyProviders = new ArrayList<>();
		obThirdPartyProviders.add(TppPortalMockData.getOBThirdPartyProvidersDetails());
		assertNotNull(transformer.tranformToThirdPartyProvidersDetails(obThirdPartyProviders));
	}
	
	@After
	public void tearDown() throws Exception {
		transformer = null;

	}
}

package com.capgemini.tppportal.helper;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.portal.config.SessionCookieConfig;
import com.capgemini.portal.constants.PortalConstants;
import com.capgemini.portal.exception.PortalException;
import com.capgemini.portal.logger.PortalRequestHeaderAttributes;
import com.capgemini.portal.model.PTCInfo;
import com.capgemini.portal.utilities.SessionCookieUtils;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.tppportal.mockdata.TppPortalMockData;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PortalHelperTest {

	@Mock
	HttpServletResponse response;

	@Mock
	private PingDirectoryService pingDirectoryService;

	@Mock
	PortalRequestHeaderAttributes portalRequestHeaderAttributes;

	@InjectMocks
	private PortalHelper portalHelper;

	@Mock
	private SessionCookieConfig sessionCookieConfig;

	@Mock
	private SessionCookieUtils sessionCookieUtils;

	private PTCInfo user = new PTCInfo();

	private static List<String> blackListedTpp = new ArrayList<>();
	@Mock
	HttpServletRequest request;

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
		request = mock(HttpServletRequest.class);
		Cookie cookie = new Cookie(PortalConstants.TPPPORTAL_CUSTOM_SESSION_COOKIE, "21edwjfhcjsafasdgjsadjf");
		user.setPtcName("TestUser");
		user.setPtcId("testuser@capgemini.com");
		user.setOrganizationIds(Arrays.asList("Qsdjsjfjd", "Q5wqFjpnTAeCtDc1Qx", "nBRcYAcACnghbGFOBk"));
		String sessionTokenStr = TppPortalMockData.getJwtTokenString(user);
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(1800);
		Cookie[] cookies = new Cookie[2];
		cookies[0] = cookie;
		when(request.getCookies()).thenReturn(cookies);

	}

	@Test
	public void logoutTest() throws Exception {
		Cookie[] cookies = { new Cookie("dummy", "test") };
		when(request.getCookies()).thenReturn(cookies);
		portalHelper.logout(request, response);
		verify(response).addCookie(any());

	}

	@Ignore
	@Test
	public void validatePTCInfoAndTppBlockedStatusTest() {
		String tppId = "1234";
		when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.FALSE);
		portalHelper.validatePTCInfoAndTppBlockedStatus(request, portalRequestHeaderAttributes, tppId);
	}

	@Test(expected = PortalException.class)
	public void validatePTCInfoAndTppBlockedStatus1Test() {
		String tppId = "1234";
		when(pingDirectoryService.isTppBlockEnabled(tppId)).thenReturn(Boolean.TRUE);
		portalHelper.validatePTCInfoAndTppBlockedStatus(request, portalRequestHeaderAttributes, tppId);
	}

	@Test
	public void removeBlockedTppsTest() throws Exception {
		blackListedTpp.add("abc");
		List<String> listTpp = new ArrayList<>();
		listTpp.add("abc");
		listTpp.add("xyz");
		ReflectionTestUtils.setField(portalHelper, "blackListedTpp", blackListedTpp);
		PortalHelper.removeBlockedTpps(listTpp);
	}

	@Test
	public void removeBlockedTppsSizeTest() throws Exception {
		blackListedTpp.clear();
		List<String> listTpp = new ArrayList<>();
		listTpp.add("abc");
		listTpp.add("xyz");
		PortalHelper.removeBlockedTpps(listTpp);
	}
}

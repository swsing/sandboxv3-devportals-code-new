getStaticContent();//getJsonData();

function getStaticContent() {
	var staticContent = null; 
	var statticContentUrl = $("#staticContentUrl").val();
	$.ajax({
		type:"GET",
		url:statticContentUrl,
		dataType: "json",
		async: false,
		success:function(data, status, xhr){
			if(status === "success"){
				contentData = data;
				 var getJsonAlertValue = contentData.ERROR_MESSAGES.SHOW_ALERT;
				   var getJsonNoticeValue = contentData.ERROR_MESSAGES.SHOW_NOTICE;
				   if(getJsonAlertValue === "true")
				   {
							$( document ).ready(function() {
								$(".showAlert").css("display", "block");
								$('.showAlert .alert-prefix').html(contentData.ERROR_MESSAGES.ALERT_PREFIX_LABEL);
								$('.showAlert .alert-suffix').html(contentData.ERROR_MESSAGES.ALERT_MESSAGE);
							});
					}
					if(getJsonNoticeValue === "true"){
						$( document ).ready(function() {
								$(".showNotice").css("display", "block");
								$('.showNotice .alert-prefix').html(contentData.ERROR_MESSAGES.NOTICE_PREFIX_LABEL);
								$('.showNotice .alert-suffix').html(contentData.ERROR_MESSAGES.NOTICE_MESSAGE);
							});
					}
			}
		},
		error:function(jqXhr, textStatus, errorMessage){
			console.log(errorMessage);
		}
	}); 
	return staticContent;
	
getStaticContent();
}
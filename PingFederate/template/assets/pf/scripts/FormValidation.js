"use strict";
var isCancelClicked = false;
function required(data) {
    if (data.length === 0) {
        return true;
    } else {
        return false;
    }
}
function alphaNumeric(data) {
    var textRegx = /[^a-zA-Z\-\ 0-9]/;  // With alpha numric we also checked spl character(-) and space.....
    if (textRegx.test(data)) {
        return true;
    } else {
        return false;
    }
}
function validateEmail(data) {
    var emailRegx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!emailRegx.test(data)) {
        return true;
    } else {
        return false;
    }
}
function confirmEmailCheck(email, confirmEmail) {
    if (email.toLowerCase() !== confirmEmail.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}
function validatePassword(data) {
    var passwordRegx = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^a-zA-Z0-9]).{8,}/;
    if (!passwordRegx.test(data)) {
        return true;
    } else {
        return false;
    }
}
function confirmPasswordCheck(password, confirmPassword) {
    if (password !== confirmPassword) {
        return true;
    } else {
        return false;
    }
}
function addError(ele, content) {
    var span = $("<span class=\"alert-inline\" role=\"alert\"></span>");
    span.text(content);
    $(ele).after(span);
}

function removeError(ele) {
    $(".alert-inline", ele).remove();
}

function validateInput(data, validateType, isRequired, isValidate) {
    if (isCancelClicked) { isCancelClicked = false; return; }
    if (typeof (data.value) !== "undefined") { data.value = (data.value).trim(); }

    var submitButton = document.getElementById("submitButton");
    var errorFlag = false;
    switch (validateType) {
        case "password":
            if (isRequired) { errorFlag = required(data.value); }
            if(errorFlag) { break; }
            if (isValidate) { errorFlag = validatePassword(data.value); }
            break;
        case "newPassword":
            var confirmPassword = document.getElementById("confirmPassword");
            if (isRequired) { errorFlag = required(data.value); }
            if(errorFlag) { break; }
            if (isValidate) {
                if (validatePassword(data.value)) {
                    errorFlag = true;
                } else if (confirmPassword !== null && confirmPassword.value !== "") {
                    if (confirmPassword.value !== data.value) {
                        if (confirmPassword.className.indexOf("invalid") === -1) {
                            $(confirmPassword).addClass("invalid");
                            errorFlag = true;
                            data = confirmPassword;
                        }
                    }
                    else {
                        errorFlag = false;
                        data = confirmPassword;
                    }

                }
            }
            break;
        case "confirmPassword":
            var newPassword = document.getElementById("newPassword");
            if (isRequired) { errorFlag = required(data.value); }
            if(errorFlag) { break; }
            if (isValidate) { errorFlag = confirmPasswordCheck(newPassword.value, data.value); }
            break;
        case "inputName":
            if (isRequired) { errorFlag = required(data.value); }
            if(errorFlag) { break; }
            if (isValidate) { errorFlag = alphaNumeric(data.value); }
            break;
        case "email":
            var confirmEmail = document.getElementById("confirmEmail");
            if (isRequired) { errorFlag = required(data.value); }
            if(errorFlag) { break; }
            if (isValidate) {
                if (validateEmail(data.value)) {
                    errorFlag = true;
                } else {
                    var stringToTest = data.value.substring(data.value.lastIndexOf("@") + 1, data.value.lastIndexOf("."));
                    var subStr = stringToTest.split(".");
                    for (var i = 0; i < subStr.length; i++) {
                        if (subStr[i].charAt(0) == "-" || subStr[i].charAt(subStr[i].length - 1) == "-") {
                            errorFlag = true;
                        }
                    }
                }
                if (confirmEmail !== null && confirmEmail.value !== "" && errorFlag === false) {
                    if (confirmEmail.value.toLowerCase() !== data.value.toLowerCase()) {
                        if (confirmEmail.className.indexOf("invalid") === -1) {
                            $(confirmEmail).addClass("invalid");
                            errorFlag = true;
                            data = confirmEmail;
                        }
                    }
                    else {
                        data = confirmEmail;
                        errorFlag = false;
                    }
                }
            }
            break;
        case "confirmEmail":
            var email = document.getElementById("email");
            if (isRequired) { errorFlag = required(data.value); }
            if(errorFlag) { break; }
            if (isValidate) { errorFlag = confirmEmailCheck(email.value, data.value); }
            break;
        case "checkbox":
            if (isRequired) {
                if (!data.checked) { errorFlag = true; }
            }
            break;
    }
    if (errorFlag) {
        if (data.className.indexOf("invalid") === -1) {
            $(data).addClass("invalid");
        }
        var errMsg = $(data).parent().children(".err-content").text();
        addError(data, errMsg);
        if ($(submitButton).is(":focus")) {
            if ($("#cancelButton").length) {
                $("#cancelButton").focus();
            } else {
                $(".footer-links>li:first>a").focus();
            }
        }
        submitButton.disabled = true;
        if ($("#cancelButton").length) {
            $("#cancelButton").focus();
        } 
    } else {
        if (data.className.indexOf("invalid") !== -1) {
            $(data).removeClass("invalid");
            removeError($(data).parent());
            validateFormRequired(data.form.id, "blur");
        } else if (document.getElementsByClassName("invalid").length > 0) {
            if ($(submitButton).is(":focus")) {
                $("#cancelButton").focus();
            }
            submitButton.disabled = true;
        }
    }
}

function validateFormRequired(formId, eventType, currentElementId) {
    if (eventType === "keydown") {
        if (window.event.keyCode !== 13) {
            return;
        }
        if (event.target.type == "checkbox" || event.target.nodeName == "A") {
            return;
        }
    }
    var elements = document.forms[formId].elements;
    var submitButtonEnableFlag = true;
    for (var i = 0; i < elements.length; i++) {
        if (currentElementId === elements[i].id && elements[i].type !== "checkbox") {
            continue;
        } else if (elements[i].type === "text" || elements[i].type === "email" || elements[i].type === "password") {
            submitButtonEnableFlag = formElementsCheck(elements[i], "onblur");
        } else if (elements[i].type === "checkbox") {
            submitButtonEnableFlag = formElementsCheck(elements[i], "onchange");
        }
        if(!submitButtonEnableFlag){break;}
    }
    var submitButton = document.getElementById("submitButton");
    if (submitButtonEnableFlag) {
        submitButton.disabled = false;
        if (!currentElementId) {
            if (window.event.keyCode === 13 && $(window.event.target).attr("id") !== "cancelButton") {
                document.getElementById("submitButton").click();
            }
        }
    } else {
        submitButton.disabled = true;
        if ($(submitButton).is(":focus")) {
            $("#cancelButton").focus();
        }
        if (!currentElementId) {
            if (window.event.keyCode === 13) {
                window.event.preventDefault();
            }
        }
    }

}

function formElementsCheck(elements, eventType) {
    var str = "", delimiter = "", start = 0, end = 0, tokens = [], result = "";
    str = elements.getAttribute(eventType);
    if (str !== null) {
        delimiter = ",";
        start = 2;
        end = 3;
        tokens = str.split(delimiter).slice(start, end);
        result = tokens.join(delimiter);
        if ((result.indexOf("true") !== -1)) { 
            if ((elements.value !== "" || elements.checked) && document.getElementsByClassName("invalid").length === 0) {
                return true;
            } else {
                return false;
            }    
        } else {
            return true;
        }
    }
}

function focusInput(data) {
    if (data.className.indexOf("invalid") !== -1) {
        $(data).removeClass("invalid");
        removeError($(data).parent());
    }
    validateFormRequired(data.form.id, "focus", data.id);
}

function checkKeyDown(data) {
    if (window.event.keyCode === 13) {
        data.blur();
    }
}
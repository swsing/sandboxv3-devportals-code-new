package com.capgemini.tppportal.muleapi.schema;

public class Portal {

	private boolean isPublic;

	public boolean getIsPublic() {
		return this.isPublic;
	}

	public void setIsPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
}

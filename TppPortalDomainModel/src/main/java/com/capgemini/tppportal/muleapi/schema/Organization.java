
package com.capgemini.tppportal.muleapi.schema;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "id",
    "createdAt",
    "updatedAt",
    "ownerId",
    "clientId",
    "domain",
    "idprovider_id",
    "isFederated",
    "parentOrganizationIds",
    "subOrganizationIds",
    "tenantOrganizationIds",
    "isMaster",
    "subscription",
    "properties",
    "entitlements"
})
public class Organization {

    @JsonProperty("name")
    private String name;
    @JsonProperty("id")
    private String id;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("updatedAt")
    private String updatedAt;
    @JsonProperty("ownerId")
    private String ownerId;
    @JsonProperty("clientId")
    private String clientId;
    @JsonProperty("domain")
    private String domain;
    @JsonProperty("idprovider_id")
    private String idproviderId;
    @JsonProperty("isFederated")
    private Boolean isFederated;
    @JsonProperty("parentOrganizationIds")
    private List<Object> parentOrganizationIds = null;
    @JsonProperty("subOrganizationIds")
    private List<String> subOrganizationIds = null;
    @JsonProperty("tenantOrganizationIds")
    private List<Object> tenantOrganizationIds = null;
    @JsonProperty("isMaster")
    private Boolean isMaster;
    @JsonProperty("subscription")
    private Subscription subscription;
    @JsonProperty("properties")
    private Properties properties;
    @JsonProperty("entitlements")
    private Entitlements entitlements;
    private static final long serialVersionUID = -3714047224595273542L;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("createdAt")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("createdAt")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updatedAt")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updatedAt")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("ownerId")
    public String getOwnerId() {
        return ownerId;
    }

    @JsonProperty("ownerId")
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    @JsonProperty("clientId")
    public String getClientId() {
        return clientId;
    }

    @JsonProperty("clientId")
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @JsonProperty("domain")
    public String getDomain() {
        return domain;
    }

    @JsonProperty("domain")
    public void setDomain(String domain) {
        this.domain = domain;
    }

    @JsonProperty("idprovider_id")
    public String getIdproviderId() {
        return idproviderId;
    }

    @JsonProperty("idprovider_id")
    public void setIdproviderId(String idproviderId) {
        this.idproviderId = idproviderId;
    }

    @JsonProperty("isFederated")
    public Boolean getIsFederated() {
        return isFederated;
    }

    @JsonProperty("isFederated")
    public void setIsFederated(Boolean isFederated) {
        this.isFederated = isFederated;
    }

    @JsonProperty("parentOrganizationIds")
    public List<Object> getParentOrganizationIds() {
        return parentOrganizationIds;
    }

    @JsonProperty("parentOrganizationIds")
    public void setParentOrganizationIds(List<Object> parentOrganizationIds) {
        this.parentOrganizationIds = parentOrganizationIds;
    }

    @JsonProperty("subOrganizationIds")
    public List<String> getSubOrganizationIds() {
        return subOrganizationIds;
    }

    @JsonProperty("subOrganizationIds")
    public void setSubOrganizationIds(List<String> subOrganizationIds) {
        this.subOrganizationIds = subOrganizationIds;
    }

    @JsonProperty("tenantOrganizationIds")
    public List<Object> getTenantOrganizationIds() {
        return tenantOrganizationIds;
    }

    @JsonProperty("tenantOrganizationIds")
    public void setTenantOrganizationIds(List<Object> tenantOrganizationIds) {
        this.tenantOrganizationIds = tenantOrganizationIds;
    }

    @JsonProperty("isMaster")
    public Boolean getIsMaster() {
        return isMaster;
    }

    @JsonProperty("isMaster")
    public void setIsMaster(Boolean isMaster) {
        this.isMaster = isMaster;
    }

    @JsonProperty("subscription")
    public Subscription getSubscription() {
        return subscription;
    }

    @JsonProperty("subscription")
    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    @JsonProperty("properties")
    public Properties getProperties() {
        return properties;
    }

    @JsonProperty("properties")
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @JsonProperty("entitlements")
    public Entitlements getEntitlements() {
        return entitlements;
    }

    @JsonProperty("entitlements")
    public void setEntitlements(Entitlements entitlements) {
        this.entitlements = entitlements;
    }

}

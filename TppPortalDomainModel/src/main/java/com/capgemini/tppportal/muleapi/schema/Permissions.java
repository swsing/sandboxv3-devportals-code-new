package com.capgemini.tppportal.muleapi.schema;

public class Permissions {
	private boolean addApiVersion;

	public boolean getAddApiVersion() {
		return this.addApiVersion;
	}

	public void setAddApiVersion(boolean addApiVersion) {
		this.addApiVersion = addApiVersion;
	}
}
package com.capgemini.tppportal.muleapi.schema;

public class ShowSecretResponse {
	
	private String secret;

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}
	
	

}

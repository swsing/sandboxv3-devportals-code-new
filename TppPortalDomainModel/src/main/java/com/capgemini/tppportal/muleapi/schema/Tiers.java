package com.capgemini.tppportal.muleapi.schema;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "total", "tiers" })
public class Tiers {

	@JsonProperty("total")
	private int total;
	@JsonProperty("tiers")
	private List<Tier> tiers;
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<Tier> getTiers() {
		return tiers;
	}
	public void setTiers(List<Tier> tiers) {
		this.tiers = tiers;
	}
	
}

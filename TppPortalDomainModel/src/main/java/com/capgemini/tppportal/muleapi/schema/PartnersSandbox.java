
package com.capgemini.tppportal.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "assigned"
})
public class PartnersSandbox implements Serializable
{

    @JsonProperty("assigned")
    private Integer assigned;
    private static final long serialVersionUID = -6115620043321484946L;

    @JsonProperty("assigned")
    public Integer getAssigned() {
        return assigned;
    }

    @JsonProperty("assigned")
    public void setAssigned(Integer assigned) {
        this.assigned = assigned;
    }

	public PartnersSandbox withAssigned(int assigned) {
		this.assigned = assigned;
		return this;
	}

}
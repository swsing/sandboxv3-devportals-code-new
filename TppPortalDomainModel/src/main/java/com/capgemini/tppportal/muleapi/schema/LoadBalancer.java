
package com.capgemini.tppportal.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "assigned",
    "reassigned"
})
public class LoadBalancer implements Serializable
{

    @JsonProperty("assigned")
    private Integer assigned;
    @JsonProperty("reassigned")
    private Integer reassigned;
    private static final long serialVersionUID = 8781031605565959825L;

    @JsonProperty("assigned")
    public Integer getAssigned() {
        return assigned;
    }

    @JsonProperty("assigned")
    public void setAssigned(Integer assigned) {
        this.assigned = assigned;
    }

    @JsonProperty("reassigned")
    public Integer getReassigned() {
        return reassigned;
    }

    @JsonProperty("reassigned")
    public void setReassigned(Integer reassigned) {
        this.reassigned = reassigned;
    }

}


package com.capgemini.tppportal.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "expiration"
})
public class Subscription implements Serializable
{

    @JsonProperty("type")
    private String type;
    @JsonProperty("expiration")
    private String expiration;
    private static final long serialVersionUID = 5543429593624534013L;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("expiration")
    public String getExpiration() {
        return expiration;
    }

    @JsonProperty("expiration")
    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

	public Subscription withExpiration(String expiration) {
		this.expiration = expiration;
		return this;
	}

}

package com.capgemini.tppportal.muleapi.schema;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "created", "updated" })
public class Audit implements Serializable {

	@JsonProperty("created")
	private Created created;
	@JsonProperty("updated")
	private Updated updated;

	private static final long serialVersionUID = -206705214095758931L;

	@JsonProperty("created")
	public Created getCreated() {
		return created;
	}

	@JsonProperty("created")
	public void setCreated(Created created) {
		this.created = created;
	}

	@JsonProperty("updated")
	public Updated getUpdated() {
		return updated;
	}

	@JsonProperty("updated")
	public void setUpdated(Updated updated) {
		this.updated = updated;
	}

}
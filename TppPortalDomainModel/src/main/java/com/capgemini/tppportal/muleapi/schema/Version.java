package com.capgemini.tppportal.muleapi.schema;

import java.util.ArrayList;
import java.util.Date;

public class Version {

	private Audit audit;
	private String masterOrganizationId;
	private String organizationId;
	private int id;
	private int apiId;
	private Integer portalId;
	private String name;
	private Object instanceLabel;
	private Object groupId;
	private Object assetId;
	private Object assetVersion;
	private Object productVersion;
	private String description;
	private ArrayList<Object> tags;
	private int order;
	private int rootFileId;
	private boolean deprecated;
	private Date lastActiveDate;
	private Object endpointUri;
	private Object environmentId;
	private Object terms;
	private Date termsDate;
	private Portal portal;
	private int lastActiveDelta;
	private boolean pinned;
	

	public Audit getAudit() {
		return this.audit;
	}

	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	public String getMasterOrganizationId() {
		return this.masterOrganizationId;
	}

	public void setMasterOrganizationId(String masterOrganizationId) {
		this.masterOrganizationId = masterOrganizationId;
	}

	public String getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getApiId() {
		return this.apiId;
	}

	public void setApiId(int apiId) {
		this.apiId = apiId;
	}

	public Integer getPortalId() {
		return this.portalId;
	}

	public void setPortalId(Integer portalId) {
		this.portalId = portalId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getInstanceLabel() {
		return this.instanceLabel;
	}

	public void setInstanceLabel(Object instanceLabel) {
		this.instanceLabel = instanceLabel;
	}

	public Object getGroupId() {
		return this.groupId;
	}

	public void setGroupId(Object groupId) {
		this.groupId = groupId;
	}

	public Object getAssetId() {
		return this.assetId;
	}

	public void setAssetId(Object assetId) {
		this.assetId = assetId;
	}

	public Object getAssetVersion() {
		return this.assetVersion;
	}

	public void setAssetVersion(Object assetVersion) {
		this.assetVersion = assetVersion;
	}

	public Object getProductVersion() {
		return this.productVersion;
	}

	public void setProductVersion(Object productVersion) {
		this.productVersion = productVersion;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Object> getTags() {
		return this.tags;
	}

	public void setTags(ArrayList<Object> tags) {
		this.tags = tags;
	}

	public int getOrder() {
		return this.order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getRootFileId() {
		return this.rootFileId;
	}

	public void setRootFileId(int rootFileId) {
		this.rootFileId = rootFileId;
	}

	public boolean getDeprecated() {
		return this.deprecated;
	}

	public void setDeprecated(boolean deprecated) {
		this.deprecated = deprecated;
	}

	public Date getLastActiveDate() {
		return this.lastActiveDate;
	}

	public void setLastActiveDate(Date lastActiveDate) {
		this.lastActiveDate = lastActiveDate;
	}

	public Object getEndpointUri() {
		return this.endpointUri;
	}

	public void setEndpointUri(Object endpointUri) {
		this.endpointUri = endpointUri;
	}

	public Object getEnvironmentId() {
		return this.environmentId;
	}

	public void setEnvironmentId(Object environmentId) {
		this.environmentId = environmentId;
	}

	public Object getTerms() {
		return this.terms;
	}

	public void setTerms(Object terms) {
		this.terms = terms;
	}

	public Date getTermsDate() {
		return this.termsDate;
	}

	public void setTermsDate(Date termsDate) {
		this.termsDate = termsDate;
	}

	public Portal getPortal() {
		return this.portal;
	}

	public void setPortal(Portal portal) {
		this.portal = portal;
	}

	public int getLastActiveDelta() {
		return this.lastActiveDelta;
	}

	public void setLastActiveDelta(int lastActiveDelta) {
		this.lastActiveDelta = lastActiveDelta;
	}

	public boolean getPinned() {
		return this.pinned;
	}

	public void setPinned(boolean pinned) {
		this.pinned = pinned;
	}
}

package com.capgemini.tppportal.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "hybridInsight",
    "objectStoreRequestUnits",
    "createEnvironments",
    "mqMessages",
    "autoscaling",
    "globalDeployment",
    "createSubOrgs",
    "mqRequests",
    "hybrid",
    "partnersProduction",
    "objectStoreKeys",
    "gateways",
    "externalIdentity",
    "workerLoggingOverride",
    "partnersSandbox",
    "armAlerts",
    "apis",
    "cam",
    "mqAdvancedFeatures",
    "exchange2",
    "hybridAutoDiscoverProperties",
    "vpcs",
    "staticIps",
    "loadBalancer",
    "vCoresDesign",
    "vCoresProduction",
    "vCoresSandbox",
    "kpiDashboard",
    "designCenter",
    "crowd",
    "messaging"
})
public class Entitlements implements Serializable
{

    @JsonProperty("hybridInsight")
    private Boolean hybridInsight;
    @JsonProperty("objectStoreRequestUnits")
    private ObjectStoreRequestUnits objectStoreRequestUnits;
    @JsonProperty("createEnvironments")
    private Boolean createEnvironments;
    @JsonProperty("mqMessages")
    private MqMessages mqMessages;
    @JsonProperty("autoscaling")
    private Boolean autoscaling;
    @JsonProperty("globalDeployment")
    private Boolean globalDeployment;
    @JsonProperty("createSubOrgs")
    private Boolean createSubOrgs;
    @JsonProperty("mqRequests")
    private MqRequests mqRequests;
    @JsonProperty("hybrid")
    private Hybrid hybrid;
    @JsonProperty("partnersProduction")
    private PartnersProduction partnersProduction;
    @JsonProperty("objectStoreKeys")
    private ObjectStoreKeys objectStoreKeys;
    @JsonProperty("gateways")
    private Gateways gateways;
    @JsonProperty("externalIdentity")
    private Boolean externalIdentity;
    @JsonProperty("workerLoggingOverride")
    private WorkerLoggingOverride workerLoggingOverride;
    @JsonProperty("partnersSandbox")
    private PartnersSandbox partnersSandbox;
    @JsonProperty("armAlerts")
    private Boolean armAlerts;
    @JsonProperty("apis")
    private Apis apis;
    @JsonProperty("cam")
    private Cam cam;
    @JsonProperty("mqAdvancedFeatures")
    private MqAdvancedFeatures mqAdvancedFeatures;
    @JsonProperty("exchange2")
    private Exchange2 exchange2;
    @JsonProperty("hybridAutoDiscoverProperties")
    private Boolean hybridAutoDiscoverProperties;
    @JsonProperty("vpcs")
    private Vpcs vpcs;
    @JsonProperty("staticIps")
    private StaticIps staticIps;
    @JsonProperty("loadBalancer")
    private LoadBalancer loadBalancer;
    @JsonProperty("vCoresDesign")
    private VCoresDesign vCoresDesign;
    @JsonProperty("vCoresProduction")
    private VCoresProduction vCoresProduction;
    @JsonProperty("vCoresSandbox")
    private VCoresSandbox vCoresSandbox;
    @JsonProperty("kpiDashboard")
    private KpiDashboard kpiDashboard;
    @JsonProperty("designCenter")
    private DesignCenter designCenter;
    @JsonProperty("crowd")
    private Crowd crowd;
    @JsonProperty("messaging")
    private Messaging messaging;
    private static final long serialVersionUID = -5392537806230673045L;

    @JsonProperty("hybridInsight")
    public Boolean getHybridInsight() {
        return hybridInsight;
    }

    @JsonProperty("hybridInsight")
    public void setHybridInsight(Boolean hybridInsight) {
        this.hybridInsight = hybridInsight;
    }

    @JsonProperty("objectStoreRequestUnits")
    public ObjectStoreRequestUnits getObjectStoreRequestUnits() {
        return objectStoreRequestUnits;
    }

    @JsonProperty("objectStoreRequestUnits")
    public void setObjectStoreRequestUnits(ObjectStoreRequestUnits objectStoreRequestUnits) {
        this.objectStoreRequestUnits = objectStoreRequestUnits;
    }

    @JsonProperty("createEnvironments")
    public Boolean getCreateEnvironments() {
        return createEnvironments;
    }

    @JsonProperty("createEnvironments")
    public void setCreateEnvironments(Boolean createEnvironments) {
        this.createEnvironments = createEnvironments;
    }

    @JsonProperty("mqMessages")
    public MqMessages getMqMessages() {
        return mqMessages;
    }

    @JsonProperty("mqMessages")
    public void setMqMessages(MqMessages mqMessages) {
        this.mqMessages = mqMessages;
    }

    @JsonProperty("autoscaling")
    public Boolean getAutoscaling() {
        return autoscaling;
    }

    @JsonProperty("autoscaling")
    public void setAutoscaling(Boolean autoscaling) {
        this.autoscaling = autoscaling;
    }

    @JsonProperty("globalDeployment")
    public Boolean getGlobalDeployment() {
        return globalDeployment;
    }

    @JsonProperty("globalDeployment")
    public void setGlobalDeployment(Boolean globalDeployment) {
        this.globalDeployment = globalDeployment;
    }

    @JsonProperty("createSubOrgs")
    public Boolean getCreateSubOrgs() {
        return createSubOrgs;
    }

    @JsonProperty("createSubOrgs")
    public void setCreateSubOrgs(Boolean createSubOrgs) {
        this.createSubOrgs = createSubOrgs;
    }

    @JsonProperty("mqRequests")
    public MqRequests getMqRequests() {
        return mqRequests;
    }

    @JsonProperty("mqRequests")
    public void setMqRequests(MqRequests mqRequests) {
        this.mqRequests = mqRequests;
    }

    @JsonProperty("hybrid")
    public Hybrid getHybrid() {
        return hybrid;
    }

    @JsonProperty("hybrid")
    public void setHybrid(Hybrid hybrid) {
        this.hybrid = hybrid;
    }

    @JsonProperty("partnersProduction")
    public PartnersProduction getPartnersProduction() {
        return partnersProduction;
    }

    @JsonProperty("partnersProduction")
    public void setPartnersProduction(PartnersProduction partnersProduction) {
        this.partnersProduction = partnersProduction;
    }

    @JsonProperty("objectStoreKeys")
    public ObjectStoreKeys getObjectStoreKeys() {
        return objectStoreKeys;
    }

    @JsonProperty("objectStoreKeys")
    public void setObjectStoreKeys(ObjectStoreKeys objectStoreKeys) {
        this.objectStoreKeys = objectStoreKeys;
    }

    @JsonProperty("gateways")
    public Gateways getGateways() {
        return gateways;
    }

    @JsonProperty("gateways")
    public void setGateways(Gateways gateways) {
        this.gateways = gateways;
    }

    @JsonProperty("externalIdentity")
    public Boolean getExternalIdentity() {
        return externalIdentity;
    }

    @JsonProperty("externalIdentity")
    public void setExternalIdentity(Boolean externalIdentity) {
        this.externalIdentity = externalIdentity;
    }

    @JsonProperty("workerLoggingOverride")
    public WorkerLoggingOverride getWorkerLoggingOverride() {
        return workerLoggingOverride;
    }

    @JsonProperty("workerLoggingOverride")
    public void setWorkerLoggingOverride(WorkerLoggingOverride workerLoggingOverride) {
        this.workerLoggingOverride = workerLoggingOverride;
    }

    @JsonProperty("partnersSandbox")
    public PartnersSandbox getPartnersSandbox() {
        return partnersSandbox;
    }

    @JsonProperty("partnersSandbox")
    public void setPartnersSandbox(PartnersSandbox partnersSandbox) {
        this.partnersSandbox = partnersSandbox;
    }

    @JsonProperty("armAlerts")
    public Boolean getArmAlerts() {
        return armAlerts;
    }

    @JsonProperty("armAlerts")
    public void setArmAlerts(Boolean armAlerts) {
        this.armAlerts = armAlerts;
    }

    @JsonProperty("apis")
    public Apis getApis() {
        return apis;
    }

    @JsonProperty("apis")
    public void setApis(Apis apis) {
        this.apis = apis;
    }

    @JsonProperty("cam")
    public Cam getCam() {
        return cam;
    }

    @JsonProperty("cam")
    public void setCam(Cam cam) {
        this.cam = cam;
    }

    @JsonProperty("mqAdvancedFeatures")
    public MqAdvancedFeatures getMqAdvancedFeatures() {
        return mqAdvancedFeatures;
    }

    @JsonProperty("mqAdvancedFeatures")
    public void setMqAdvancedFeatures(MqAdvancedFeatures mqAdvancedFeatures) {
        this.mqAdvancedFeatures = mqAdvancedFeatures;
    }

    @JsonProperty("exchange2")
    public Exchange2 getExchange2() {
        return exchange2;
    }

    @JsonProperty("exchange2")
    public void setExchange2(Exchange2 exchange2) {
        this.exchange2 = exchange2;
    }

    @JsonProperty("hybridAutoDiscoverProperties")
    public Boolean getHybridAutoDiscoverProperties() {
        return hybridAutoDiscoverProperties;
    }

    @JsonProperty("hybridAutoDiscoverProperties")
    public void setHybridAutoDiscoverProperties(Boolean hybridAutoDiscoverProperties) {
        this.hybridAutoDiscoverProperties = hybridAutoDiscoverProperties;
    }

    @JsonProperty("vpcs")
    public Vpcs getVpcs() {
        return vpcs;
    }

    @JsonProperty("vpcs")
    public void setVpcs(Vpcs vpcs) {
        this.vpcs = vpcs;
    }

    @JsonProperty("staticIps")
    public StaticIps getStaticIps() {
        return staticIps;
    }

    @JsonProperty("staticIps")
    public void setStaticIps(StaticIps staticIps) {
        this.staticIps = staticIps;
    }

    @JsonProperty("loadBalancer")
    public LoadBalancer getLoadBalancer() {
        return loadBalancer;
    }

    @JsonProperty("loadBalancer")
    public void setLoadBalancer(LoadBalancer loadBalancer) {
        this.loadBalancer = loadBalancer;
    }

    @JsonProperty("vCoresDesign")
    public VCoresDesign getVCoresDesign() {
        return vCoresDesign;
    }

    @JsonProperty("vCoresDesign")
    public void setVCoresDesign(VCoresDesign vCoresDesign) {
        this.vCoresDesign = vCoresDesign;
    }

    @JsonProperty("vCoresProduction")
    public VCoresProduction getVCoresProduction() {
        return vCoresProduction;
    }

    @JsonProperty("vCoresProduction")
    public void setVCoresProduction(VCoresProduction vCoresProduction) {
        this.vCoresProduction = vCoresProduction;
    }

    @JsonProperty("vCoresSandbox")
    public VCoresSandbox getVCoresSandbox() {
        return vCoresSandbox;
    }

    @JsonProperty("vCoresSandbox")
    public void setVCoresSandbox(VCoresSandbox vCoresSandbox) {
        this.vCoresSandbox = vCoresSandbox;
    }

    @JsonProperty("kpiDashboard")
    public KpiDashboard getKpiDashboard() {
        return kpiDashboard;
    }

    @JsonProperty("kpiDashboard")
    public void setKpiDashboard(KpiDashboard kpiDashboard) {
        this.kpiDashboard = kpiDashboard;
    }

    @JsonProperty("designCenter")
    public DesignCenter getDesignCenter() {
        return designCenter;
    }

    @JsonProperty("designCenter")
    public void setDesignCenter(DesignCenter designCenter) {
        this.designCenter = designCenter;
    }

    @JsonProperty("crowd")
    public Crowd getCrowd() {
        return crowd;
    }

    @JsonProperty("crowd")
    public void setCrowd(Crowd crowd) {
        this.crowd = crowd;
    }

    @JsonProperty("messaging")
    public Messaging getMessaging() {
        return messaging;
    }

    @JsonProperty("messaging")
    public void setMessaging(Messaging messaging) {
        this.messaging = messaging;
    }

}
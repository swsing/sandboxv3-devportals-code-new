
package com.capgemini.tppportal.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "username_attribute",
    "firstname_attribute",
    "lastname_attribute",
    "email_attribute",
    "group_attribute"
})
public class ClaimsMapping implements Serializable
{

    @JsonProperty("username_attribute")
    private String usernameAttribute;
    @JsonProperty("firstname_attribute")
    private String firstnameAttribute;
    @JsonProperty("lastname_attribute")
    private String lastnameAttribute;
    @JsonProperty("email_attribute")
    private String emailAttribute;
    @JsonProperty("group_attribute")
    private String groupAttribute;
    private static final long serialVersionUID = -4428921499105682075L;

    @JsonProperty("username_attribute")
    public String getUsernameAttribute() {
        return usernameAttribute;
    }

    @JsonProperty("username_attribute")
    public void setUsernameAttribute(String usernameAttribute) {
        this.usernameAttribute = usernameAttribute;
    }

    @JsonProperty("firstname_attribute")
    public String getFirstnameAttribute() {
        return firstnameAttribute;
    }

    @JsonProperty("firstname_attribute")
    public void setFirstnameAttribute(String firstnameAttribute) {
        this.firstnameAttribute = firstnameAttribute;
    }

    @JsonProperty("lastname_attribute")
    public String getLastnameAttribute() {
        return lastnameAttribute;
    }

    @JsonProperty("lastname_attribute")
    public void setLastnameAttribute(String lastnameAttribute) {
        this.lastnameAttribute = lastnameAttribute;
    }

    @JsonProperty("email_attribute")
    public String getEmailAttribute() {
        return emailAttribute;
    }

    @JsonProperty("email_attribute")
    public void setEmailAttribute(String emailAttribute) {
        this.emailAttribute = emailAttribute;
    }

    @JsonProperty("group_attribute")
    public String getGroupAttribute() {
        return groupAttribute;
    }

    @JsonProperty("group_attribute")
    public void setGroupAttribute(String groupAttribute) {
        this.groupAttribute = groupAttribute;
    }

}

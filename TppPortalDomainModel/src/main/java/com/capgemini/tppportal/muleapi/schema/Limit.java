package com.capgemini.tppportal.muleapi.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"visible","maximumRequests","timePeriodInMilliseconds"})
public class Limit {

	@JsonProperty("visible")
	private Boolean visible;
	@JsonProperty("maximumRequests")
	private int maximumRequests;
	@JsonProperty("timePeriodInMilliseconds")
	private int timePeriodInMilliseconds;
	
	public Boolean getVisible() {
		return visible;
	}
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	public int getMaximumRequests() {
		return maximumRequests;
	}
	public void setMaximumRequests(int maximumRequests) {
		this.maximumRequests = maximumRequests;
	}
	public int getTimePeriodInMilliseconds() {
		return timePeriodInMilliseconds;
	}
	public void setTimePeriodInMilliseconds(int timePeriodInMilliseconds) {
		this.timePeriodInMilliseconds = timePeriodInMilliseconds;
	}
		
}

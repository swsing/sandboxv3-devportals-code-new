
package com.capgemini.tppportal.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "createdAt",
    "updatedAt",
    "organizationId",
    "firstName",
    "lastName",
    "email",
    "phoneNumber",
    "idprovider_id",
    "username",
    "enabled",
    "deleted",
    "lastLogin",
    "type",
    "organizationPreferences",
    "organization",
    "properties"
})
public class Owner
{

    @JsonProperty("id")
    private String id;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("updatedAt")
    private String updatedAt;
    @JsonProperty("organizationId")
    private String organizationId;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("email")
    private String email;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("idprovider_id")
    private String idproviderId;
    @JsonProperty("username")
    private String username;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("lastLogin")
    private String lastLogin;
    @JsonProperty("type")
    private String type;
    @JsonProperty("organizationPreferences")
    private OrganizationPreferences organizationPreferences;
    @JsonProperty("organization")
    private Organization organization;
    @JsonProperty("properties")
    private Properties_ properties;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("createdAt")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("createdAt")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updatedAt")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updatedAt")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("organizationId")
    public String getOrganizationId() {
        return organizationId;
    }

    @JsonProperty("organizationId")
    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

    @JsonProperty("phoneNumber")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @JsonProperty("phoneNumber")
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @JsonProperty("idprovider_id")
    public String getIdproviderId() {
        return idproviderId;
    }

    @JsonProperty("idprovider_id")
    public void setIdproviderId(String idproviderId) {
        this.idproviderId = idproviderId;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

	@JsonProperty("enabled")
	public Boolean isEnabled() {
		return enabled;
	}

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("deleted")
    public Boolean getDeleted() {
        return deleted;
    }

    @JsonProperty("deleted")
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @JsonProperty("lastLogin")
    public String getLastLogin() {
        return lastLogin;
    }

    @JsonProperty("lastLogin")
    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("organizationPreferences")
    public OrganizationPreferences getOrganizationPreferences() {
        return organizationPreferences;
    }

    @JsonProperty("organizationPreferences")
    public void setOrganizationPreferences(OrganizationPreferences organizationPreferences) {
        this.organizationPreferences = organizationPreferences;
    }

    @JsonProperty("organization")
    public Organization getOrganization() {
        return organization;
    }

    @JsonProperty("organization")
    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @JsonProperty("properties")
    public Properties_ getProperties() {
        return properties;
    }

    @JsonProperty("properties")
    public void setProperties(Properties_ properties) {
        this.properties = properties;
    }

	public Owner withProperties(Properties_ properties) {
		this.properties = properties;
		return this;
	}

}

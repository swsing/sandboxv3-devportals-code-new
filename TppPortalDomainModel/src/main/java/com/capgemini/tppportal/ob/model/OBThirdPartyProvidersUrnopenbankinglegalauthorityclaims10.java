package com.capgemini.tppportal.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Claims sourced from a legal authority - can be applied to either TPPs to ASPSP
 */
@ApiModel(description = "Claims sourced from a legal authority - can be applied to either TPPs to ASPSP")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10   {
  @JsonProperty("LegalAuthorityClaims")
  private List<OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims> legalAuthorityClaims = null;

  public OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10 legalAuthorityClaims(List<OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims> legalAuthorityClaims) {
    this.legalAuthorityClaims = legalAuthorityClaims;
    return this;
  }

  public OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10 addLegalAuthorityClaimsItem(OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims legalAuthorityClaimsItem) {
    if (this.legalAuthorityClaims == null) {
      this.legalAuthorityClaims = new ArrayList<OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims>();
    }
    this.legalAuthorityClaims.add(legalAuthorityClaimsItem);
    return this;
  }

   /**
   * Get legalAuthorityClaims
   * @return legalAuthorityClaims
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims> getLegalAuthorityClaims() {
    return legalAuthorityClaims;
  }

  public void setLegalAuthorityClaims(List<OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10LegalAuthorityClaims> legalAuthorityClaims) {
    this.legalAuthorityClaims = legalAuthorityClaims;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10 obThirdPartyProvidersUrnopenbankinglegalauthorityclaims10 = (OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10) o;
    return Objects.equals(this.legalAuthorityClaims, obThirdPartyProvidersUrnopenbankinglegalauthorityclaims10.legalAuthorityClaims);
  }

  @Override
  public int hashCode() {
    return Objects.hash(legalAuthorityClaims);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBThirdPartyProvidersUrnopenbankinglegalauthorityclaims10 {\n");
    
    sb.append("    legalAuthorityClaims: ").append(toIndentedString(legalAuthorityClaims)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


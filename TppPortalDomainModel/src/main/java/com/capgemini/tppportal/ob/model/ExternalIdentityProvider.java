package com.capgemini.tppportal.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * The external identity provider
 */
@ApiModel(description = "The external identity provider")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class ExternalIdentityProvider   {
  @JsonProperty("description")
  private String description = null;

  @JsonProperty("iconUrl")
  private String iconUrl = null;

  @JsonProperty("name")
  private String name = null;

  /**
   * The type of external identity provider
   */
  public enum TypeEnum {
    FACEBOOK("Facebook"),
    
    GOOGLEPLUS("GooglePlus"),
    
    OIDC("Oidc");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("type")
  private TypeEnum type = null;

  public ExternalIdentityProvider description(String description) {
    this.description = description;
    return this;
  }

   /**
   * The description of the External Identity Provider
   * @return description
  **/
  @ApiModelProperty(value = "The description of the External Identity Provider")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ExternalIdentityProvider iconUrl(String iconUrl) {
    this.iconUrl = iconUrl;
    return this;
  }

   /**
   * The icon URL for this External Identity Provider
   * @return iconUrl
  **/
  @ApiModelProperty(value = "The icon URL for this External Identity Provider")


  public String getIconUrl() {
    return iconUrl;
  }

  public void setIconUrl(String iconUrl) {
    this.iconUrl = iconUrl;
  }

  public ExternalIdentityProvider name(String name) {
    this.name = name;
    return this;
  }

   /**
   * The name of the External Identity Provider
   * @return name
  **/
  @ApiModelProperty(value = "The name of the External Identity Provider")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ExternalIdentityProvider type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
   * The type of external identity provider
   * @return type
  **/
  @ApiModelProperty(value = "The type of external identity provider")


  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExternalIdentityProvider externalIdentityProvider = (ExternalIdentityProvider) o;
    return Objects.equals(this.description, externalIdentityProvider.description) &&
        Objects.equals(this.iconUrl, externalIdentityProvider.iconUrl) &&
        Objects.equals(this.name, externalIdentityProvider.name) &&
        Objects.equals(this.type, externalIdentityProvider.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(description, iconUrl, name, type);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExternalIdentityProvider {\n");
    
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    iconUrl: ").append(toIndentedString(iconUrl)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package com.capgemini.tppportal.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A user&#39;s history of consent events for an OAuth2 client
 */
@ApiModel(description = "A user's history of consent events for an OAuth2 client")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OAuth2ConsentHistory   {
  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    CONSENTHISTORY("urn:pingidentity:scim:api:messages:2.0:consentHistory");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("client")
  private OAuth2Client client = null;

  @JsonProperty("scopes")
  private List<OAuth2ConsentHistoryScopes> scopes = null;

  public OAuth2ConsentHistory schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public OAuth2ConsentHistory addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public OAuth2ConsentHistory id(String id) {
    this.id = id;
    return this;
  }

   /**
   * The event ID
   * @return id
  **/
  @ApiModelProperty(value = "The event ID")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public OAuth2ConsentHistory client(OAuth2Client client) {
    this.client = client;
    return this;
  }

   /**
   * The target OAuth2 client.
   * @return client
  **/
  @ApiModelProperty(value = "The target OAuth2 client.")

  @Valid

  public OAuth2Client getClient() {
    return client;
  }

  public void setClient(OAuth2Client client) {
    this.client = client;
  }

  public OAuth2ConsentHistory scopes(List<OAuth2ConsentHistoryScopes> scopes) {
    this.scopes = scopes;
    return this;
  }

  public OAuth2ConsentHistory addScopesItem(OAuth2ConsentHistoryScopes scopesItem) {
    if (this.scopes == null) {
      this.scopes = new ArrayList<OAuth2ConsentHistoryScopes>();
    }
    this.scopes.add(scopesItem);
    return this;
  }

   /**
   * The consent decision for OAuth2 scopes.
   * @return scopes
  **/
  @ApiModelProperty(value = "The consent decision for OAuth2 scopes.")

  @Valid

  public List<OAuth2ConsentHistoryScopes> getScopes() {
    return scopes;
  }

  public void setScopes(List<OAuth2ConsentHistoryScopes> scopes) {
    this.scopes = scopes;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OAuth2ConsentHistory oauth2ConsentHistory = (OAuth2ConsentHistory) o;
    return Objects.equals(this.schemas, oauth2ConsentHistory.schemas) &&
        Objects.equals(this.id, oauth2ConsentHistory.id) &&
        Objects.equals(this.client, oauth2ConsentHistory.client) &&
        Objects.equals(this.scopes, oauth2ConsentHistory.scopes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schemas, id, client, scopes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OAuth2ConsentHistory {\n");
    
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    client: ").append(toIndentedString(client)).append("\n");
    sb.append("    scopes: ").append(toIndentedString(scopes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


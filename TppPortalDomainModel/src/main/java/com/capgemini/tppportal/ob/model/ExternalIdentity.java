package com.capgemini.tppportal.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A link to an external identity provider
 */
@ApiModel(description = "A link to an external identity provider")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class ExternalIdentity   {
  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    EXTERNALIDENTITY("urn:pingidentity:scim:api:messages:2.0:ExternalIdentity");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("provider")
  private ExternalIdentityProvider provider = null;

  @JsonProperty("providerUserId")
  private String providerUserId = null;

  @JsonProperty("accessToken")
  private String accessToken = null;

  @JsonProperty("refreshToken")
  private String refreshToken = null;

  public ExternalIdentity schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public ExternalIdentity addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public ExternalIdentity provider(ExternalIdentityProvider provider) {
    this.provider = provider;
    return this;
  }

   /**
   * Get provider
   * @return provider
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ExternalIdentityProvider getProvider() {
    return provider;
  }

  public void setProvider(ExternalIdentityProvider provider) {
    this.provider = provider;
  }

  public ExternalIdentity providerUserId(String providerUserId) {
    this.providerUserId = providerUserId;
    return this;
  }

   /**
   * The user ID at the provider. If not available, the user is not linked to any external identities at this provider.
   * @return providerUserId
  **/
  @ApiModelProperty(readOnly = true, value = "The user ID at the provider. If not available, the user is not linked to any external identities at this provider.")


  public String getProviderUserId() {
    return providerUserId;
  }

  public void setProviderUserId(String providerUserId) {
    this.providerUserId = providerUserId;
  }

  public ExternalIdentity accessToken(String accessToken) {
    this.accessToken = accessToken;
    return this;
  }

   /**
   * The access token issued by the provider that may be used to retrieve additional data.
   * @return accessToken
  **/
  @ApiModelProperty(readOnly = true, value = "The access token issued by the provider that may be used to retrieve additional data.")


  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public ExternalIdentity refreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
    return this;
  }

   /**
   * The refresh token issued by the provider that may be used to retrieve a new access token.
   * @return refreshToken
  **/
  @ApiModelProperty(readOnly = true, value = "The refresh token issued by the provider that may be used to retrieve a new access token.")


  public String getRefreshToken() {
    return refreshToken;
  }

  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExternalIdentity externalIdentity = (ExternalIdentity) o;
    return Objects.equals(this.schemas, externalIdentity.schemas) &&
        Objects.equals(this.provider, externalIdentity.provider) &&
        Objects.equals(this.providerUserId, externalIdentity.providerUserId) &&
        Objects.equals(this.accessToken, externalIdentity.accessToken) &&
        Objects.equals(this.refreshToken, externalIdentity.refreshToken);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schemas, provider, providerUserId, accessToken, refreshToken);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExternalIdentity {\n");
    
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    provider: ").append(toIndentedString(provider)).append("\n");
    sb.append("    providerUserId: ").append(toIndentedString(providerUserId)).append("\n");
    sb.append("    accessToken: ").append(toIndentedString(accessToken)).append("\n");
    sb.append("    refreshToken: ").append(toIndentedString(refreshToken)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package com.capgemini.tppportal.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError   {
  @JsonProperty("passwordRequirements")
  private List<PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements> passwordRequirements = null;

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError passwordRequirements(List<PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements> passwordRequirements) {
    this.passwordRequirements = passwordRequirements;
    return this;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError addPasswordRequirementsItem(PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements passwordRequirementsItem) {
    if (this.passwordRequirements == null) {
      this.passwordRequirements = new ArrayList<PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements>();
    }
    this.passwordRequirements.add(passwordRequirementsItem);
    return this;
  }

   /**
   * If present, the new password did not meet one or more quality requirements in this list
   * @return passwordRequirements
  **/
  @ApiModelProperty(value = "If present, the new password did not meet one or more quality requirements in this list")

  @Valid

  public List<PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements> getPasswordRequirements() {
    return passwordRequirements;
  }

  public void setPasswordRequirements(List<PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorPasswordRequirements> passwordRequirements) {
    this.passwordRequirements = passwordRequirements;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError = (PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError) o;
    return Objects.equals(this.passwordRequirements, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError.passwordRequirements);
  }

  @Override
  public int hashCode() {
    return Objects.hash(passwordRequirements);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateError {\n");
    
    sb.append("    passwordRequirements: ").append(toIndentedString(passwordRequirements)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


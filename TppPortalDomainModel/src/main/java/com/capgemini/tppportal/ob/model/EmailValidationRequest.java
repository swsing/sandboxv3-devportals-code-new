package com.capgemini.tppportal.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Request for validating an email address
 */
@ApiModel(description = "Request for validating an email address")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class EmailValidationRequest   {
  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    EMAILVALIDATIONREQUEST("urn:pingidentity:scim:api:messages:2.0:EmailValidationRequest");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("attributePath")
  private String attributePath = null;

  @JsonProperty("attributeValue")
  private String attributeValue = null;

  @JsonProperty("messageSubject")
  private String messageSubject = null;

  @JsonProperty("messageText")
  private String messageText = null;

  @JsonProperty("verifyCode")
  private String verifyCode = null;

  @JsonProperty("validated")
  private Boolean validated = null;

  @JsonProperty("validatedAt")
  private String validatedAt = null;

  @JsonProperty("codeSent")
  private Boolean codeSent = null;

  public EmailValidationRequest schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public EmailValidationRequest addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public EmailValidationRequest attributePath(String attributePath) {
    this.attributePath = attributePath;
    return this;
  }

   /**
   * The attribute path containing the email address
   * @return attributePath
  **/
  @ApiModelProperty(value = "The attribute path containing the email address")


  public String getAttributePath() {
    return attributePath;
  }

  public void setAttributePath(String attributePath) {
    this.attributePath = attributePath;
  }

  public EmailValidationRequest attributeValue(String attributeValue) {
    this.attributeValue = attributeValue;
    return this;
  }

   /**
   * The email address
   * @return attributeValue
  **/
  @ApiModelProperty(value = "The email address")


  public String getAttributeValue() {
    return attributeValue;
  }

  public void setAttributeValue(String attributeValue) {
    this.attributeValue = attributeValue;
  }

  public EmailValidationRequest messageSubject(String messageSubject) {
    this.messageSubject = messageSubject;
    return this;
  }

   /**
   * The subject of the email message when sending the verification code
   * @return messageSubject
  **/
  @ApiModelProperty(value = "The subject of the email message when sending the verification code")


  public String getMessageSubject() {
    return messageSubject;
  }

  public void setMessageSubject(String messageSubject) {
    this.messageSubject = messageSubject;
  }

  public EmailValidationRequest messageText(String messageText) {
    this.messageText = messageText;
    return this;
  }

   /**
   * The body of the email message when sending the verification code
   * @return messageText
  **/
  @ApiModelProperty(value = "The body of the email message when sending the verification code")


  public String getMessageText() {
    return messageText;
  }

  public void setMessageText(String messageText) {
    this.messageText = messageText;
  }

  public EmailValidationRequest verifyCode(String verifyCode) {
    this.verifyCode = verifyCode;
    return this;
  }

   /**
   * The code to verify
   * @return verifyCode
  **/
  @ApiModelProperty(value = "The code to verify")


  public String getVerifyCode() {
    return verifyCode;
  }

  public void setVerifyCode(String verifyCode) {
    this.verifyCode = verifyCode;
  }

  public EmailValidationRequest validated(Boolean validated) {
    this.validated = validated;
    return this;
  }

   /**
   * Whether the current email address was successfully validated
   * @return validated
  **/
  @ApiModelProperty(value = "Whether the current email address was successfully validated")


  public Boolean getValidated() {
    return validated;
  }

  public void setValidated(Boolean validated) {
    this.validated = validated;
  }

  public EmailValidationRequest validatedAt(String validatedAt) {
    this.validatedAt = validatedAt;
    return this;
  }

   /**
   * The last time the current email address was successfully validated
   * @return validatedAt
  **/
  @ApiModelProperty(value = "The last time the current email address was successfully validated")


  public String getValidatedAt() {
    return validatedAt;
  }

  public void setValidatedAt(String validatedAt) {
    this.validatedAt = validatedAt;
  }

  public EmailValidationRequest codeSent(Boolean codeSent) {
    this.codeSent = codeSent;
    return this;
  }

   /**
   * Whether a verification code was sent and is pending validation
   * @return codeSent
  **/
  @ApiModelProperty(value = "Whether a verification code was sent and is pending validation")


  public Boolean getCodeSent() {
    return codeSent;
  }

  public void setCodeSent(Boolean codeSent) {
    this.codeSent = codeSent;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EmailValidationRequest emailValidationRequest = (EmailValidationRequest) o;
    return Objects.equals(this.schemas, emailValidationRequest.schemas) &&
        Objects.equals(this.attributePath, emailValidationRequest.attributePath) &&
        Objects.equals(this.attributeValue, emailValidationRequest.attributeValue) &&
        Objects.equals(this.messageSubject, emailValidationRequest.messageSubject) &&
        Objects.equals(this.messageText, emailValidationRequest.messageText) &&
        Objects.equals(this.verifyCode, emailValidationRequest.verifyCode) &&
        Objects.equals(this.validated, emailValidationRequest.validated) &&
        Objects.equals(this.validatedAt, emailValidationRequest.validatedAt) &&
        Objects.equals(this.codeSent, emailValidationRequest.codeSent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schemas, attributePath, attributeValue, messageSubject, messageText, verifyCode, validated, validatedAt, codeSent);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EmailValidationRequest {\n");
    
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    attributePath: ").append(toIndentedString(attributePath)).append("\n");
    sb.append("    attributeValue: ").append(toIndentedString(attributeValue)).append("\n");
    sb.append("    messageSubject: ").append(toIndentedString(messageSubject)).append("\n");
    sb.append("    messageText: ").append(toIndentedString(messageText)).append("\n");
    sb.append("    verifyCode: ").append(toIndentedString(verifyCode)).append("\n");
    sb.append("    validated: ").append(toIndentedString(validated)).append("\n");
    sb.append("    validatedAt: ").append(toIndentedString(validatedAt)).append("\n");
    sb.append("    codeSent: ").append(toIndentedString(codeSent)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


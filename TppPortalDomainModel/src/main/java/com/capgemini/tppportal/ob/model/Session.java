package com.capgemini.tppportal.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Session objects
 */
@ApiModel(description = "Session objects")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class Session   {
  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    SESSION("urn:pingidentity:scim:api:messages:2.0:session");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("lastLoginMethods")
  private List<String> lastLoginMethods = null;

  @JsonProperty("lastSecondFactorMethods")
  private List<String> lastSecondFactorMethods = null;

  @JsonProperty("lastLogin")
  private String lastLogin = null;

  @JsonProperty("lastSecondFactor")
  private String lastSecondFactor = null;

  @JsonProperty("ipAddress")
  private String ipAddress = null;

  @JsonProperty("userAgentString")
  private String userAgentString = null;

  public Session schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public Session addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public Session lastLoginMethods(List<String> lastLoginMethods) {
    this.lastLoginMethods = lastLoginMethods;
    return this;
  }

  public Session addLastLoginMethodsItem(String lastLoginMethodsItem) {
    if (this.lastLoginMethods == null) {
      this.lastLoginMethods = new ArrayList<String>();
    }
    this.lastLoginMethods.add(lastLoginMethodsItem);
    return this;
  }

   /**
   * Details about the authentication methods successfully used during the last login event
   * @return lastLoginMethods
  **/
  @ApiModelProperty(value = "Details about the authentication methods successfully used during the last login event")


  public List<String> getLastLoginMethods() {
    return lastLoginMethods;
  }

  public void setLastLoginMethods(List<String> lastLoginMethods) {
    this.lastLoginMethods = lastLoginMethods;
  }

  public Session lastSecondFactorMethods(List<String> lastSecondFactorMethods) {
    this.lastSecondFactorMethods = lastSecondFactorMethods;
    return this;
  }

  public Session addLastSecondFactorMethodsItem(String lastSecondFactorMethodsItem) {
    if (this.lastSecondFactorMethods == null) {
      this.lastSecondFactorMethods = new ArrayList<String>();
    }
    this.lastSecondFactorMethods.add(lastSecondFactorMethodsItem);
    return this;
  }

   /**
   * Details about the authentication methods successfully used during the last second factor event
   * @return lastSecondFactorMethods
  **/
  @ApiModelProperty(value = "Details about the authentication methods successfully used during the last second factor event")


  public List<String> getLastSecondFactorMethods() {
    return lastSecondFactorMethods;
  }

  public void setLastSecondFactorMethods(List<String> lastSecondFactorMethods) {
    this.lastSecondFactorMethods = lastSecondFactorMethods;
  }

  public Session lastLogin(String lastLogin) {
    this.lastLogin = lastLogin;
    return this;
  }

   /**
   * The last time of a successful login event
   * @return lastLogin
  **/
  @ApiModelProperty(value = "The last time of a successful login event")


  public String getLastLogin() {
    return lastLogin;
  }

  public void setLastLogin(String lastLogin) {
    this.lastLogin = lastLogin;
  }

  public Session lastSecondFactor(String lastSecondFactor) {
    this.lastSecondFactor = lastSecondFactor;
    return this;
  }

   /**
   * The last time of a successful second factor event
   * @return lastSecondFactor
  **/
  @ApiModelProperty(value = "The last time of a successful second factor event")


  public String getLastSecondFactor() {
    return lastSecondFactor;
  }

  public void setLastSecondFactor(String lastSecondFactor) {
    this.lastSecondFactor = lastSecondFactor;
  }

  public Session ipAddress(String ipAddress) {
    this.ipAddress = ipAddress;
    return this;
  }

   /**
   * The IP address of the user agent that was used to perform the authentication
   * @return ipAddress
  **/
  @ApiModelProperty(value = "The IP address of the user agent that was used to perform the authentication")


  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public Session userAgentString(String userAgentString) {
    this.userAgentString = userAgentString;
    return this;
  }

   /**
   * The user agent string presented by the user agent that was used to perform the authentication
   * @return userAgentString
  **/
  @ApiModelProperty(value = "The user agent string presented by the user agent that was used to perform the authentication")


  public String getUserAgentString() {
    return userAgentString;
  }

  public void setUserAgentString(String userAgentString) {
    this.userAgentString = userAgentString;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Session session = (Session) o;
    return Objects.equals(this.schemas, session.schemas) &&
        Objects.equals(this.lastLoginMethods, session.lastLoginMethods) &&
        Objects.equals(this.lastSecondFactorMethods, session.lastSecondFactorMethods) &&
        Objects.equals(this.lastLogin, session.lastLogin) &&
        Objects.equals(this.lastSecondFactor, session.lastSecondFactor) &&
        Objects.equals(this.ipAddress, session.ipAddress) &&
        Objects.equals(this.userAgentString, session.userAgentString);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schemas, lastLoginMethods, lastSecondFactorMethods, lastLogin, lastSecondFactor, ipAddress, userAgentString);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Session {\n");
    
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    lastLoginMethods: ").append(toIndentedString(lastLoginMethods)).append("\n");
    sb.append("    lastSecondFactorMethods: ").append(toIndentedString(lastSecondFactorMethods)).append("\n");
    sb.append("    lastLogin: ").append(toIndentedString(lastLogin)).append("\n");
    sb.append("    lastSecondFactor: ").append(toIndentedString(lastSecondFactor)).append("\n");
    sb.append("    ipAddress: ").append(toIndentedString(ipAddress)).append("\n");
    sb.append("    userAgentString: ").append(toIndentedString(userAgentString)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package com.capgemini.tppportal.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OrganisationPostalAddress
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OrganisationPostalAddress   {
  @JsonProperty("AddressLine2")
  private String addressLine2 = null;

  @JsonProperty("Country")
  private String country = null;

  @JsonProperty("County")
  private String county = null;

  @JsonProperty("Name")
  private String name = null;

  @JsonProperty("POBox")
  private String poBox = null;

  @JsonProperty("PostCode")
  private String postCode = null;

  @JsonProperty("Primary")
  private Boolean primary = null;

  @JsonProperty("StreetAddress")
  private String streetAddress = null;

  @JsonProperty("Town")
  private String town = null;

  @JsonProperty("Type")
  private String type = null;

  public OrganisationPostalAddress addressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
    return this;
  }

   /**
   * Locality name
   * @return addressLine2
  **/
  @ApiModelProperty(value = "Locality name")


  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  public OrganisationPostalAddress country(String country) {
    this.country = country;
    return this;
  }

   /**
   * 2 disigt country code
   * @return country
  **/
  @ApiModelProperty(value = "2 disigt country code")


  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public OrganisationPostalAddress county(String county) {
    this.county = county;
    return this;
  }

   /**
   * County
   * @return county
  **/
  @ApiModelProperty(value = "County")


  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  public OrganisationPostalAddress name(String name) {
    this.name = name;
    return this;
  }

   /**
   * A name of a person or an office to which this address belongs.
   * @return name
  **/
  @ApiModelProperty(value = "A name of a person or an office to which this address belongs.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OrganisationPostalAddress poBox(String poBox) {
    this.poBox = poBox;
    return this;
  }

   /**
   * PO Box for address
   * @return poBox
  **/
  @ApiModelProperty(value = "PO Box for address")


  public String getPoBox() {
    return poBox;
  }

  public void setPoBox(String poBox) {
    this.poBox = poBox;
  }

  public OrganisationPostalAddress postCode(String postCode) {
    this.postCode = postCode;
    return this;
  }

   /**
   * Post Code of address
   * @return postCode
  **/
  @ApiModelProperty(value = "Post Code of address")


  public String getPostCode() {
    return postCode;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public OrganisationPostalAddress primary(Boolean primary) {
    this.primary = primary;
    return this;
  }

   /**
   * Inidcator to show if this is the primary address of the organisation
   * @return primary
  **/
  @ApiModelProperty(value = "Inidcator to show if this is the primary address of the organisation")


  public Boolean getPrimary() {
    return primary;
  }

  public void setPrimary(Boolean primary) {
    this.primary = primary;
  }

  public OrganisationPostalAddress streetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
    return this;
  }

   /**
   * Street Address of the organisation
   * @return streetAddress
  **/
  @ApiModelProperty(value = "Street Address of the organisation")


  public String getStreetAddress() {
    return streetAddress;
  }

  public void setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
  }

  public OrganisationPostalAddress town(String town) {
    this.town = town;
    return this;
  }

   /**
   * Postal Town
   * @return town
  **/
  @ApiModelProperty(value = "Postal Town")


  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public OrganisationPostalAddress type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Type of contact
   * @return type
  **/
  @ApiModelProperty(value = "Type of contact")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OrganisationPostalAddress organisationPostalAddress = (OrganisationPostalAddress) o;
    return Objects.equals(this.addressLine2, organisationPostalAddress.addressLine2) &&
        Objects.equals(this.country, organisationPostalAddress.country) &&
        Objects.equals(this.county, organisationPostalAddress.county) &&
        Objects.equals(this.name, organisationPostalAddress.name) &&
        Objects.equals(this.poBox, organisationPostalAddress.poBox) &&
        Objects.equals(this.postCode, organisationPostalAddress.postCode) &&
        Objects.equals(this.primary, organisationPostalAddress.primary) &&
        Objects.equals(this.streetAddress, organisationPostalAddress.streetAddress) &&
        Objects.equals(this.town, organisationPostalAddress.town) &&
        Objects.equals(this.type, organisationPostalAddress.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(addressLine2, country, county, name, poBox, postCode, primary, streetAddress, town, type);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OrganisationPostalAddress {\n");
    
    sb.append("    addressLine2: ").append(toIndentedString(addressLine2)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    county: ").append(toIndentedString(county)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    poBox: ").append(toIndentedString(poBox)).append("\n");
    sb.append("    postCode: ").append(toIndentedString(postCode)).append("\n");
    sb.append("    primary: ").append(toIndentedString(primary)).append("\n");
    sb.append("    streetAddress: ").append(toIndentedString(streetAddress)).append("\n");
    sb.append("    town: ").append(toIndentedString(town)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


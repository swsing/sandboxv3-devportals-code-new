package com.capgemini.tppportal.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Account details for a Primary Technical Contact, or similar, which are used for SCIM access to the Directory on behalf of an accredited organisation
 */
@ApiModel(description = "Account details for a Primary Technical Contact, or similar, which are used for SCIM access to the Directory on behalf of an accredited organisation")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OBAccounts   {
  @JsonProperty("AccountVerified")
  private Boolean accountVerified = null;

  @JsonProperty("CommonName")
  private String commonName = null;

  @JsonProperty("CreateTimestamp")
  private String createTimestamp = null;

  @JsonProperty("EMailAddresses")
  private List<OBAccountsEMailAddresses> emailAddresses = null;

  @JsonProperty("HonourificPrefix")
  private String honourificPrefix = null;

  @JsonProperty("HonourificSuffix")
  private String honourificSuffix = null;

  @JsonProperty("MiddleInitial")
  private String middleInitial = null;

  @JsonProperty("MiddleName")
  private String middleName = null;

  @JsonProperty("ModifyTimestamp")
  private String modifyTimestamp = null;

  @JsonProperty("PhoneNumbers")
  private List<OBAccountsPhoneNumbers> phoneNumbers = null;

  @JsonProperty("PostalAddresses")
  private List<OBAccountsPostalAddresses> postalAddresses = null;

  @JsonProperty("SecondFactorEmail")
  private String secondFactorEmail = null;

  @JsonProperty("SecondFactorEnabled")
  private Boolean secondFactorEnabled = null;

  @JsonProperty("SecondFactorPhoneNumber")
  private String secondFactorPhoneNumber = null;

  @JsonProperty("Status")
  private List<String> status = new ArrayList<String>();

  @JsonProperty("Surname")
  private String surname = null;

  @JsonProperty("Title")
  private String title = null;

  @JsonProperty("Username")
  private String username = null;

  @JsonProperty("externalId")
  private String externalId = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("meta")
  private Meta meta = null;

  @JsonProperty("schemas")
  private List<String> schemas = new ArrayList<String>();

  public OBAccounts accountVerified(Boolean accountVerified) {
    this.accountVerified = accountVerified;
    return this;
  }

   /**
   * Flag showing if account information has been verified
   * @return accountVerified
  **/
  @ApiModelProperty(required = true, value = "Flag showing if account information has been verified")
  @NotNull


  public Boolean getAccountVerified() {
    return accountVerified;
  }

  public void setAccountVerified(Boolean accountVerified) {
    this.accountVerified = accountVerified;
  }

  public OBAccounts commonName(String commonName) {
    this.commonName = commonName;
    return this;
  }

   /**
   * The Forename or Christian Name of the Account Holder
   * @return commonName
  **/
  @ApiModelProperty(value = "The Forename or Christian Name of the Account Holder")


  public String getCommonName() {
    return commonName;
  }

  public void setCommonName(String commonName) {
    this.commonName = commonName;
  }

  public OBAccounts createTimestamp(String createTimestamp) {
    this.createTimestamp = createTimestamp;
    return this;
  }

   /**
   * Creation timestamp
   * @return createTimestamp
  **/
  @ApiModelProperty(readOnly = true, value = "Creation timestamp")


  public String getCreateTimestamp() {
    return createTimestamp;
  }

  public void setCreateTimestamp(String createTimestamp) {
    this.createTimestamp = createTimestamp;
  }

  public OBAccounts emailAddresses(List<OBAccountsEMailAddresses> emailAddresses) {
    this.emailAddresses = emailAddresses;
    return this;
  }

  public OBAccounts addEmailAddressesItem(OBAccountsEMailAddresses emailAddressesItem) {
    if (this.emailAddresses == null) {
      this.emailAddresses = new ArrayList<OBAccountsEMailAddresses>();
    }
    this.emailAddresses.add(emailAddressesItem);
    return this;
  }

   /**
   * Get emailAddresses
   * @return emailAddresses
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<OBAccountsEMailAddresses> getEmailAddresses() {
    return emailAddresses;
  }

  public void setEmailAddresses(List<OBAccountsEMailAddresses> emailAddresses) {
    this.emailAddresses = emailAddresses;
  }

  public OBAccounts honourificPrefix(String honourificPrefix) {
    this.honourificPrefix = honourificPrefix;
    return this;
  }

   /**
   * Honourific Prefix
   * @return honourificPrefix
  **/
  @ApiModelProperty(value = "Honourific Prefix")


  public String getHonourificPrefix() {
    return honourificPrefix;
  }

  public void setHonourificPrefix(String honourificPrefix) {
    this.honourificPrefix = honourificPrefix;
  }

  public OBAccounts honourificSuffix(String honourificSuffix) {
    this.honourificSuffix = honourificSuffix;
    return this;
  }

   /**
   * Honourific Name Suffix i.e. K.G.
   * @return honourificSuffix
  **/
  @ApiModelProperty(value = "Honourific Name Suffix i.e. K.G.")


  public String getHonourificSuffix() {
    return honourificSuffix;
  }

  public void setHonourificSuffix(String honourificSuffix) {
    this.honourificSuffix = honourificSuffix;
  }

  public OBAccounts middleInitial(String middleInitial) {
    this.middleInitial = middleInitial;
    return this;
  }

   /**
   * Middle Initial
   * @return middleInitial
  **/
  @ApiModelProperty(value = "Middle Initial")


  public String getMiddleInitial() {
    return middleInitial;
  }

  public void setMiddleInitial(String middleInitial) {
    this.middleInitial = middleInitial;
  }

  public OBAccounts middleName(String middleName) {
    this.middleName = middleName;
    return this;
  }

   /**
   * Middle Name
   * @return middleName
  **/
  @ApiModelProperty(value = "Middle Name")


  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public OBAccounts modifyTimestamp(String modifyTimestamp) {
    this.modifyTimestamp = modifyTimestamp;
    return this;
  }

   /**
   * Modification Timestamp
   * @return modifyTimestamp
  **/
  @ApiModelProperty(readOnly = true, value = "Modification Timestamp")


  public String getModifyTimestamp() {
    return modifyTimestamp;
  }

  public void setModifyTimestamp(String modifyTimestamp) {
    this.modifyTimestamp = modifyTimestamp;
  }

  public OBAccounts phoneNumbers(List<OBAccountsPhoneNumbers> phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
    return this;
  }

  public OBAccounts addPhoneNumbersItem(OBAccountsPhoneNumbers phoneNumbersItem) {
    if (this.phoneNumbers == null) {
      this.phoneNumbers = new ArrayList<OBAccountsPhoneNumbers>();
    }
    this.phoneNumbers.add(phoneNumbersItem);
    return this;
  }

   /**
   * Phone numbers held on the account
   * @return phoneNumbers
  **/
  @ApiModelProperty(value = "Phone numbers held on the account")

  @Valid

  public List<OBAccountsPhoneNumbers> getPhoneNumbers() {
    return phoneNumbers;
  }

  public void setPhoneNumbers(List<OBAccountsPhoneNumbers> phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
  }

  public OBAccounts postalAddresses(List<OBAccountsPostalAddresses> postalAddresses) {
    this.postalAddresses = postalAddresses;
    return this;
  }

  public OBAccounts addPostalAddressesItem(OBAccountsPostalAddresses postalAddressesItem) {
    if (this.postalAddresses == null) {
      this.postalAddresses = new ArrayList<OBAccountsPostalAddresses>();
    }
    this.postalAddresses.add(postalAddressesItem);
    return this;
  }

   /**
   * Account Holder Postal Addresses
   * @return postalAddresses
  **/
  @ApiModelProperty(value = "Account Holder Postal Addresses")

  @Valid

  public List<OBAccountsPostalAddresses> getPostalAddresses() {
    return postalAddresses;
  }

  public void setPostalAddresses(List<OBAccountsPostalAddresses> postalAddresses) {
    this.postalAddresses = postalAddresses;
  }

  public OBAccounts secondFactorEmail(String secondFactorEmail) {
    this.secondFactorEmail = secondFactorEmail;
    return this;
  }

   /**
   * Second factor Email address
   * @return secondFactorEmail
  **/
  @ApiModelProperty(value = "Second factor Email address")


  public String getSecondFactorEmail() {
    return secondFactorEmail;
  }

  public void setSecondFactorEmail(String secondFactorEmail) {
    this.secondFactorEmail = secondFactorEmail;
  }

  public OBAccounts secondFactorEnabled(Boolean secondFactorEnabled) {
    this.secondFactorEnabled = secondFactorEnabled;
    return this;
  }

   /**
   * Flag to show is second factor details have been enabled
   * @return secondFactorEnabled
  **/
  @ApiModelProperty(required = true, value = "Flag to show is second factor details have been enabled")
  @NotNull


  public Boolean getSecondFactorEnabled() {
    return secondFactorEnabled;
  }

  public void setSecondFactorEnabled(Boolean secondFactorEnabled) {
    this.secondFactorEnabled = secondFactorEnabled;
  }

  public OBAccounts secondFactorPhoneNumber(String secondFactorPhoneNumber) {
    this.secondFactorPhoneNumber = secondFactorPhoneNumber;
    return this;
  }

   /**
   * Second Factor Phone Number
   * @return secondFactorPhoneNumber
  **/
  @ApiModelProperty(value = "Second Factor Phone Number")


  public String getSecondFactorPhoneNumber() {
    return secondFactorPhoneNumber;
  }

  public void setSecondFactorPhoneNumber(String secondFactorPhoneNumber) {
    this.secondFactorPhoneNumber = secondFactorPhoneNumber;
  }

  public OBAccounts status(List<String> status) {
    this.status = status;
    return this;
  }

  public OBAccounts addStatusItem(String statusItem) {
    this.status.add(statusItem);
    return this;
  }

   /**
   * User Status
   * @return status
  **/
  @ApiModelProperty(required = true, value = "User Status")
  @NotNull


  public List<String> getStatus() {
    return status;
  }

  public void setStatus(List<String> status) {
    this.status = status;
  }

  public OBAccounts surname(String surname) {
    this.surname = surname;
    return this;
  }

   /**
   * The Last or surname of the person to whom the account belongs
   * @return surname
  **/
  @ApiModelProperty(value = "The Last or surname of the person to whom the account belongs")


  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public OBAccounts title(String title) {
    this.title = title;
    return this;
  }

   /**
   * Title of person to whom the account belongs
   * @return title
  **/
  @ApiModelProperty(value = "Title of person to whom the account belongs")


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public OBAccounts username(String username) {
    this.username = username;
    return this;
  }

   /**
   * Unique username for this personal account - usually email address
   * @return username
  **/
  @ApiModelProperty(required = true, value = "Unique username for this personal account - usually email address")
  @NotNull


  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public OBAccounts externalId(String externalId) {
    this.externalId = externalId;
    return this;
  }

   /**
   * Get externalId
   * @return externalId
  **/
  @ApiModelProperty(value = "")


  public String getExternalId() {
    return externalId;
  }

  public void setExternalId(String externalId) {
    this.externalId = externalId;
  }

  public OBAccounts id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(readOnly = true, value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public OBAccounts meta(Meta meta) {
    this.meta = meta;
    return this;
  }

   /**
   * Get meta
   * @return meta
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Meta getMeta() {
    return meta;
  }

  public void setMeta(Meta meta) {
    this.meta = meta;
  }

  public OBAccounts schemas(List<String> schemas) {
    this.schemas = schemas;
    return this;
  }

  public OBAccounts addSchemasItem(String schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Get schemas
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public List<String> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<String> schemas) {
    this.schemas = schemas;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBAccounts obAccounts = (OBAccounts) o;
    return Objects.equals(this.accountVerified, obAccounts.accountVerified) &&
        Objects.equals(this.commonName, obAccounts.commonName) &&
        Objects.equals(this.createTimestamp, obAccounts.createTimestamp) &&
        Objects.equals(this.emailAddresses, obAccounts.emailAddresses) &&
        Objects.equals(this.honourificPrefix, obAccounts.honourificPrefix) &&
        Objects.equals(this.honourificSuffix, obAccounts.honourificSuffix) &&
        Objects.equals(this.middleInitial, obAccounts.middleInitial) &&
        Objects.equals(this.middleName, obAccounts.middleName) &&
        Objects.equals(this.modifyTimestamp, obAccounts.modifyTimestamp) &&
        Objects.equals(this.phoneNumbers, obAccounts.phoneNumbers) &&
        Objects.equals(this.postalAddresses, obAccounts.postalAddresses) &&
        Objects.equals(this.secondFactorEmail, obAccounts.secondFactorEmail) &&
        Objects.equals(this.secondFactorEnabled, obAccounts.secondFactorEnabled) &&
        Objects.equals(this.secondFactorPhoneNumber, obAccounts.secondFactorPhoneNumber) &&
        Objects.equals(this.status, obAccounts.status) &&
        Objects.equals(this.surname, obAccounts.surname) &&
        Objects.equals(this.title, obAccounts.title) &&
        Objects.equals(this.username, obAccounts.username) &&
        Objects.equals(this.externalId, obAccounts.externalId) &&
        Objects.equals(this.id, obAccounts.id) &&
        Objects.equals(this.meta, obAccounts.meta) &&
        Objects.equals(this.schemas, obAccounts.schemas);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountVerified, commonName, createTimestamp, emailAddresses, honourificPrefix, honourificSuffix, middleInitial, middleName, modifyTimestamp, phoneNumbers, postalAddresses, secondFactorEmail, secondFactorEnabled, secondFactorPhoneNumber, status, surname, title, username, externalId, id, meta, schemas);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBAccounts {\n");
    
    sb.append("    accountVerified: ").append(toIndentedString(accountVerified)).append("\n");
    sb.append("    commonName: ").append(toIndentedString(commonName)).append("\n");
    sb.append("    createTimestamp: ").append(toIndentedString(createTimestamp)).append("\n");
    sb.append("    emailAddresses: ").append(toIndentedString(emailAddresses)).append("\n");
    sb.append("    honourificPrefix: ").append(toIndentedString(honourificPrefix)).append("\n");
    sb.append("    honourificSuffix: ").append(toIndentedString(honourificSuffix)).append("\n");
    sb.append("    middleInitial: ").append(toIndentedString(middleInitial)).append("\n");
    sb.append("    middleName: ").append(toIndentedString(middleName)).append("\n");
    sb.append("    modifyTimestamp: ").append(toIndentedString(modifyTimestamp)).append("\n");
    sb.append("    phoneNumbers: ").append(toIndentedString(phoneNumbers)).append("\n");
    sb.append("    postalAddresses: ").append(toIndentedString(postalAddresses)).append("\n");
    sb.append("    secondFactorEmail: ").append(toIndentedString(secondFactorEmail)).append("\n");
    sb.append("    secondFactorEnabled: ").append(toIndentedString(secondFactorEnabled)).append("\n");
    sb.append("    secondFactorPhoneNumber: ").append(toIndentedString(secondFactorPhoneNumber)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    surname: ").append(toIndentedString(surname)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("    externalId: ").append(toIndentedString(externalId)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


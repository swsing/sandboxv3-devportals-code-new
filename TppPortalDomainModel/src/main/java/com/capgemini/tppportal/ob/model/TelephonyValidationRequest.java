package com.capgemini.tppportal.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Request for validating a phone number
 */
@ApiModel(description = "Request for validating a phone number")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class TelephonyValidationRequest   {
  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    TELEPHONYVALIDATIONREQUEST("urn:pingidentity:scim:api:messages:2.0:TelephonyValidationRequest");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("attributePath")
  private String attributePath = null;

  @JsonProperty("attributeValue")
  private String attributeValue = null;

  @JsonProperty("message")
  private TelephonyValidationRequestMessage message = null;

  @JsonProperty("messagingProvider")
  private String messagingProvider = null;

  @JsonProperty("verifyCode")
  private String verifyCode = null;

  @JsonProperty("validated")
  private Boolean validated = null;

  @JsonProperty("validatedAt")
  private String validatedAt = null;

  @JsonProperty("codeSent")
  private Boolean codeSent = null;

  public TelephonyValidationRequest schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public TelephonyValidationRequest addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public TelephonyValidationRequest attributePath(String attributePath) {
    this.attributePath = attributePath;
    return this;
  }

   /**
   * The attribute path containing the telephone number
   * @return attributePath
  **/
  @ApiModelProperty(value = "The attribute path containing the telephone number")


  public String getAttributePath() {
    return attributePath;
  }

  public void setAttributePath(String attributePath) {
    this.attributePath = attributePath;
  }

  public TelephonyValidationRequest attributeValue(String attributeValue) {
    this.attributeValue = attributeValue;
    return this;
  }

   /**
   * The telephone number
   * @return attributeValue
  **/
  @ApiModelProperty(value = "The telephone number")


  public String getAttributeValue() {
    return attributeValue;
  }

  public void setAttributeValue(String attributeValue) {
    this.attributeValue = attributeValue;
  }

  public TelephonyValidationRequest message(TelephonyValidationRequestMessage message) {
    this.message = message;
    return this;
  }

   /**
   * Get message
   * @return message
  **/
  @ApiModelProperty(value = "")

  @Valid

  public TelephonyValidationRequestMessage getMessage() {
    return message;
  }

  public void setMessage(TelephonyValidationRequestMessage message) {
    this.message = message;
  }

  public TelephonyValidationRequest messagingProvider(String messagingProvider) {
    this.messagingProvider = messagingProvider;
    return this;
  }

   /**
   * The messaging provider to use when sending the verification code
   * @return messagingProvider
  **/
  @ApiModelProperty(value = "The messaging provider to use when sending the verification code")


  public String getMessagingProvider() {
    return messagingProvider;
  }

  public void setMessagingProvider(String messagingProvider) {
    this.messagingProvider = messagingProvider;
  }

  public TelephonyValidationRequest verifyCode(String verifyCode) {
    this.verifyCode = verifyCode;
    return this;
  }

   /**
   * The code to verify
   * @return verifyCode
  **/
  @ApiModelProperty(value = "The code to verify")


  public String getVerifyCode() {
    return verifyCode;
  }

  public void setVerifyCode(String verifyCode) {
    this.verifyCode = verifyCode;
  }

  public TelephonyValidationRequest validated(Boolean validated) {
    this.validated = validated;
    return this;
  }

   /**
   * Whether the current telephone number was successfully validated
   * @return validated
  **/
  @ApiModelProperty(value = "Whether the current telephone number was successfully validated")


  public Boolean getValidated() {
    return validated;
  }

  public void setValidated(Boolean validated) {
    this.validated = validated;
  }

  public TelephonyValidationRequest validatedAt(String validatedAt) {
    this.validatedAt = validatedAt;
    return this;
  }

   /**
   * The last time the current telephone number was successfully validated
   * @return validatedAt
  **/
  @ApiModelProperty(value = "The last time the current telephone number was successfully validated")


  public String getValidatedAt() {
    return validatedAt;
  }

  public void setValidatedAt(String validatedAt) {
    this.validatedAt = validatedAt;
  }

  public TelephonyValidationRequest codeSent(Boolean codeSent) {
    this.codeSent = codeSent;
    return this;
  }

   /**
   * Whether a verification code was sent and is pending validation
   * @return codeSent
  **/
  @ApiModelProperty(value = "Whether a verification code was sent and is pending validation")


  public Boolean getCodeSent() {
    return codeSent;
  }

  public void setCodeSent(Boolean codeSent) {
    this.codeSent = codeSent;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TelephonyValidationRequest telephonyValidationRequest = (TelephonyValidationRequest) o;
    return Objects.equals(this.schemas, telephonyValidationRequest.schemas) &&
        Objects.equals(this.attributePath, telephonyValidationRequest.attributePath) &&
        Objects.equals(this.attributeValue, telephonyValidationRequest.attributeValue) &&
        Objects.equals(this.message, telephonyValidationRequest.message) &&
        Objects.equals(this.messagingProvider, telephonyValidationRequest.messagingProvider) &&
        Objects.equals(this.verifyCode, telephonyValidationRequest.verifyCode) &&
        Objects.equals(this.validated, telephonyValidationRequest.validated) &&
        Objects.equals(this.validatedAt, telephonyValidationRequest.validatedAt) &&
        Objects.equals(this.codeSent, telephonyValidationRequest.codeSent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schemas, attributePath, attributeValue, message, messagingProvider, verifyCode, validated, validatedAt, codeSent);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TelephonyValidationRequest {\n");
    
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    attributePath: ").append(toIndentedString(attributePath)).append("\n");
    sb.append("    attributeValue: ").append(toIndentedString(attributeValue)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    messagingProvider: ").append(toIndentedString(messagingProvider)).append("\n");
    sb.append("    verifyCode: ").append(toIndentedString(verifyCode)).append("\n");
    sb.append("    validated: ").append(toIndentedString(validated)).append("\n");
    sb.append("    validatedAt: ").append(toIndentedString(validatedAt)).append("\n");
    sb.append("    codeSent: ").append(toIndentedString(codeSent)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


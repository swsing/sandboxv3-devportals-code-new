package com.capgemini.tppportal.dtos;

import java.util.List;

import com.capgemini.tppportal.ldap.client.model.TPPApplication;

public class TPPApplicationResponse{

	private List<TPPApplication> tppAppList;

	public List<TPPApplication> getTppAppList() {
		return tppAppList;
	}

	public void setTppAppList(List<TPPApplication> tppAppList) {
		this.tppAppList = tppAppList;
	}

}

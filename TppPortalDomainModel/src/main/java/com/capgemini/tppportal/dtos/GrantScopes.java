package com.capgemini.tppportal.dtos;

import java.util.ArrayList;
import java.util.List;

public class GrantScopes {

	private List<String> granttypes = new ArrayList<>();	
	private List<String> scopes = new ArrayList<>(); 
	
	public List<String> getGranttypes() {
		return granttypes;
	}
	public List<String> getScopes() {
		return scopes;
	}
}

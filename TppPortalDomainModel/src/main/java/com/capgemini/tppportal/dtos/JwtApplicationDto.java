package com.capgemini.tppportal.dtos;

import java.util.List;

public class JwtApplicationDto {

	private String softwareId;
	private String softwareClientId;
	private String softwareClientName;
	private String softwareClientDescription;
	private String softwareClientURI;
	private List<String> softwareRedirectURIs;
	private List<String> softwareRoles;
	private String softwareEnvironment;
	private String softwareMode;
	private String softwareVersion;

	private String orgId;
	private String orgStatus;
	private String orgName;
	private String orgJWKSEndpoint;
	private String orgJWKSRevokedEndpoint;
	private String orgCompetentAuthorityId;
	
	

	private static final String ATTR_MISSING = "ATTR_MISSING_IN_SSA";

	public String getSoftwareId() {
		if (softwareId == null) {
			return ATTR_MISSING;
		}
		return softwareId;
	}

	public void setSoftwareId(String softwareId) {
		this.softwareId = softwareId;
	}

	public String getSoftwareClientId() {
		if (softwareClientId == null) {
			return ATTR_MISSING;
		}
		return softwareClientId;
	}

	public void setSoftwareClientId(String softwareClientId) {
		this.softwareClientId = softwareClientId;
	}

	public String getSoftwareClientName() {
		if (softwareClientName == null) {
			return ATTR_MISSING;
		}
		return softwareClientName;
	}

	public void setSoftwareClientName(String softwareClientName) {
		this.softwareClientName = softwareClientName;
	}

	public String getSoftwareClientDescription() {
		if (softwareClientDescription == null) {
			return ATTR_MISSING;
		}
		return softwareClientDescription;
	}

	public void setSoftwareClientDescription(String softwareClientDescription) {
		this.softwareClientDescription = softwareClientDescription;
	}

	public String getSoftwareClientURI() {
		if (softwareClientURI == null) {
			return ATTR_MISSING;
		}
		return softwareClientURI;
	}

	public void setSoftwareClientURI(String softwareClientURI) {
		this.softwareClientURI = softwareClientURI;
	}

	public List<String> getSoftwareRedirectURIs() {
		return softwareRedirectURIs;
	}

	public void setSoftwareRedirectURIs(List<String> softwareRedirectURIs) {
		this.softwareRedirectURIs = softwareRedirectURIs;
	}

	public List<String> getSoftwareRoles() {
		return softwareRoles;
	}

	public void setSoftwareRoles(List<String> softwareRoles) {
		this.softwareRoles = softwareRoles;
	}

	/**
	 * @return the softwareEnvironment
	 */
	public String getSoftwareEnvironment() {
		if (softwareEnvironment == null) {
			return ATTR_MISSING;
		}
		return softwareEnvironment;
	}

	/**
	 * @param softwareEnvironment
	 *            the softwareEnvironment to set
	 */
	public void setSoftwareEnvironment(String softwareEnvironment) {
		this.softwareEnvironment = softwareEnvironment;
	}

	/**
	 * @return the softwareMode
	 */
	public String getSoftwareMode() {
		if (softwareMode == null) {
			return ATTR_MISSING;
		}
		return softwareMode;
	}

	/**
	 * @param softwareMode
	 *            the softwareMode to set
	 */
	public void setSoftwareMode(String softwareMode) {
		this.softwareMode = softwareMode;
	}

	/**
	 * @return the softwareVersion
	 */
	public String getSoftwareVersion() {
		if (softwareVersion == null) {
			return ATTR_MISSING;
		}
		return softwareVersion;
	}

	/**
	 * @param softwareVersion
	 *            the softwareVersion to set
	 */
	public void setSoftwareVersion(String softwareVersion) {
		this.softwareVersion = softwareVersion;
	}

	/**
	 * @return the orgId
	 */
	public String getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId
	 *            the orgId to set
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return the orgStatus
	 */
	public String getOrgStatus() {
		if (orgStatus == null) {
			return ATTR_MISSING;
		}
		return orgStatus;
	}

	/**
	 * @param orgStatus
	 *            the orgStatus to set
	 */
	public void setOrgStatus(String orgStatus) {
		this.orgStatus = orgStatus;
	}

	/**
	 * @return the orgName
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * @param orgName
	 *            the orgName to set
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	/**
	 * @return the orgJWKSEndpoint
	 */
	public String getOrgJWKSEndpoint() {
		return orgJWKSEndpoint;
	}

	/**
	 * @param orgJWKSEndpoint
	 *            the orgJWKSEndpoint to set
	 */
	public void setOrgJWKSEndpoint(String orgJWKSEndpoint) {
		this.orgJWKSEndpoint = orgJWKSEndpoint;
	}

	/**
	 * @return the orgJWKSRevokedEndpoint
	 */
	public String getOrgJWKSRevokedEndpoint() {
		return orgJWKSRevokedEndpoint;
	}

	/**
	 * @param orgJWKSRevokedEndpoint
	 *            the orgJWKSRevokedEndpoint to set
	 */
	public void setOrgJWKSRevokedEndpoint(String orgJWKSRevokedEndpoint) {
		this.orgJWKSRevokedEndpoint = orgJWKSRevokedEndpoint;
	}

	/**
	 * @return the orgCompetentAuthorityId
	 */
	public String getOrgCompetentAuthorityId() {
		if (orgCompetentAuthorityId == null) {
			return ATTR_MISSING;
		}
		return orgCompetentAuthorityId;
	}

	/**
	 * @param orgCompetentAuthorityId
	 *            the orgCompetentAuthorityId to set
	 */
	public void setOrgCompetentAuthorityId(String orgCompetentAuthorityId) {
		this.orgCompetentAuthorityId = orgCompetentAuthorityId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "JwtApplicationDto [softwareId=" + softwareId + ", softwareClientId=" + softwareClientId
				+ ", softwareClientName=" + softwareClientName + ", softwareClientDescription="
				+ softwareClientDescription + ", softwareClientURI=" + softwareClientURI + ", softwareRedirectURIs="
				+ softwareRedirectURIs + ", softwareRoles=" + softwareRoles + ", softwareEnvironment="
				+ softwareEnvironment + ", softwareMode=" + softwareMode + ", softwareVersion=" + softwareVersion
				+ ", orgId=" + orgId + ", orgStatus=" + orgStatus + ", orgName=" + orgName + ", orgJWKSEndpoint="
				+ orgJWKSEndpoint + ", orgJWKSRevokedEndpoint=" + orgJWKSRevokedEndpoint + ", orgCompetentAuthorityId="
				+ orgCompetentAuthorityId + "]";
	}
}
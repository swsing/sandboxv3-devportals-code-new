package com.capgemini.tppportal.dtos;

public enum DeleteApplicationStageEnum {
	PF_APP_DELETE_FAILURE(1,"Ping Federate Application Deletion Call Failed to Execute"),
	PD_CLIENT_ASS_DELETE_FAILURE(2,"Ping Directory TPP Application Deletion from TPP Organisation from ou=tpp Failed to Execute"),
	PD_CLIENT_APP_MAPPING_DELETE_FAILURE(3,"Ping Directory TPP Application details Deletion from ou=clientAppMapping Failed to Execute"),
	MULE_APP_DELETE_FAILURE(4,"Mule TPP Application Deletion Call Failed to Execute");
	
	private int id;
	private String description;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	private DeleteApplicationStageEnum(int id, String description) {
		this.id = id;
		this.description = description;
	}
}

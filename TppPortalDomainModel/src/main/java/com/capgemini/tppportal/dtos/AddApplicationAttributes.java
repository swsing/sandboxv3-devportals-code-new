package com.capgemini.tppportal.dtos;

import com.capgemini.tppportal.muleapi.schema.LoginResponse;

public class AddApplicationAttributes {

	private AddApplicationStageEnum stage;
	private String clientId;
	private String applicationId;
	private LoginResponse loginResponse;
	private String tppOrgId;
	private Boolean isTppExist;
	
	
	public Boolean getIsTppExist() {
		return isTppExist;
	}
	public void setIsTppExist(Boolean isTppExist) {
		this.isTppExist = isTppExist;
	}
	public String getTppOrgId() {
		return tppOrgId;
	}
	public void setTppOrgId(String tppOrgId) {
		this.tppOrgId = tppOrgId;
	}
	
	public AddApplicationStageEnum getStage() {
		return stage;
	}
	public void setStage(AddApplicationStageEnum stage) {
		this.stage = stage;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public LoginResponse getLoginResponse() {
		return loginResponse;
	}
	public void setLoginResponse(LoginResponse loginResponse) {
		this.loginResponse = loginResponse;
	}
}

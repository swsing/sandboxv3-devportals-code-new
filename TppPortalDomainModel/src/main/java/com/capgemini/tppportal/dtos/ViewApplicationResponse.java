/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL

 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.tppportal.dtos;

import java.util.List;

import com.capgemini.tppportal.ldap.client.model.TPPApplication;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Class ViewApplicationResponse.
 */


@JsonPropertyOrder({
"tppAppList","tppOrgId","isTPPBlocked"
})


public class ViewApplicationResponse  {

	private String tppOrgId;
	

	private List<TPPApplication> tppAppList;
	
	private boolean isTPPBlocked;

	public List<TPPApplication> getTppAppList() {
		return tppAppList;
	}

	public void setTppAppList(List<TPPApplication> tppAppList) {
		this.tppAppList = tppAppList;
	}

	public String getTppOrgId() {
		return tppOrgId;
	}

	public void setTppOrgId(String tppOrgId) {
		this.tppOrgId = tppOrgId;
	}

	
	public boolean isTPPBlocked() {
		return isTPPBlocked;
	}

	public void setTPPBlocked(boolean isTPPBlocked) {
		this.isTPPBlocked = isTPPBlocked;
	}

	

	

	}
package com.capgemini.tppportal.ldap.client.model;

import java.io.Serializable;

public class TppGroup implements Serializable {
	

	private static final long serialVersionUID = 1L;

	private String cn;
	private String o;
	private String blacklist;
	private String block;
	private String cert;
	private String competentAuth;
	private String domainNames;
	private String roles;
	private String source;
	private String orgStatus;
	private String orgId;
	private String orgName;
	private String orgJwksEndpoint;
	private String orgJwksRevokedEndpoint;
	private String orgCompetentAuthId;
	
	/**
	 * @return the cn
	 */
	public String getCn() {
		return cn;
	}
	/**
	 * @param cn the cn to set
	 */
	public void setCn(String cn) {
		this.cn = cn;
	}
	/**
	 * @return the o
	 */
	public String getO() {
		return o;
	}
	/**
	 * @param o the o to set
	 */
	public void setO(String o) {
		this.o = o;
	}
	/**
	 * @return the blacklist
	 */
	public String getBlacklist() {
		return blacklist;
	}
	/**
	 * @param blacklist the blacklist to set
	 */
	public void setBlacklist(String blacklist) {
		this.blacklist = blacklist;
	}
	/**
	 * @return the block
	 */
	public String getBlock() {
		return block;
	}
	/**
	 * @param block the block to set
	 */
	public void setBlock(String block) {
		this.block = block;
	}
	/**
	 * @return the cert
	 */
	public String getCert() {
		return cert;
	}
	/**
	 * @param cert the cert to set
	 */
	public void setCert(String cert) {
		this.cert = cert;
	}
	/**
	 * @return the competentAuth
	 */
	public String getCompetentAuth() {
		return competentAuth;
	}
	/**
	 * @param competentAuth the competentAuth to set
	 */
	public void setCompetentAuth(String competentAuth) {
		this.competentAuth = competentAuth;
	}
	/**
	 * @return the domainNames
	 */
	public String getDomainNames() {
		return domainNames;
	}
	/**
	 * @param domainNames the domainNames to set
	 */
	public void setDomainNames(String domainNames) {
		this.domainNames = domainNames;
	}
	/**
	 * @return the roles
	 */
	public String getRoles() {
		return roles;
	}
	/**
	 * @param roles the roles to set
	 */
	public void setRoles(String roles) {
		this.roles = roles;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the orgStatus
	 */
	public String getOrgStatus() {
		return orgStatus;
	}
	/**
	 * @param orgStatus the orgStatus to set
	 */
	public void setOrgStatus(String orgStatus) {
		this.orgStatus = orgStatus;
	}
	/**
	 * @return the orgId
	 */
	public String getOrgId() {
		return orgId;
	}
	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	/**
	 * @return the orgName
	 */
	public String getOrgName() {
		return orgName;
	}
	/**
	 * @param orgName the orgName to set
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	/**
	 * @return the orgJwksEndpoint
	 */
	public String getOrgJwksEndpoint() {
		return orgJwksEndpoint;
	}
	/**
	 * @param orgJwksEndpoint the orgJwksEndpoint to set
	 */
	public void setOrgJwksEndpoint(String orgJwksEndpoint) {
		this.orgJwksEndpoint = orgJwksEndpoint;
	}
	/**
	 * @return the orgJwksRevokedEndpoint
	 */
	public String getOrgJwksRevokedEndpoint() {
		return orgJwksRevokedEndpoint;
	}
	/**
	 * @param orgJwksRevokedEndpoint the orgJwksRevokedEndpoint to set
	 */
	public void setOrgJwksRevokedEndpoint(String orgJwksRevokedEndpoint) {
		this.orgJwksRevokedEndpoint = orgJwksRevokedEndpoint;
	}
	/**
	 * @return the orgCompetentAuthId
	 */
	public String getOrgCompetentAuthId() {
		return orgCompetentAuthId;
	}
	/**
	 * @param orgCompetentAuthId the orgCompetentAuthId to set
	 */
	public void setOrgCompetentAuthId(String orgCompetentAuthId) {
		this.orgCompetentAuthId = orgCompetentAuthId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TppGroup [getCn()=");
		builder.append(getCn());
		builder.append(", getO()=");
		builder.append(getO());
		builder.append(", getBlacklist()=");
		builder.append(getBlacklist());
		builder.append(", getBlock()=");
		builder.append(getBlock());
		builder.append(", getCert()=");
		builder.append(getCert());
		builder.append(", getCompetentAuth()=");
		builder.append(getCompetentAuth());
		builder.append(", getDomainNames()=");
		builder.append(getDomainNames());
		builder.append(", getRoles()=");
		builder.append(getRoles());
		builder.append(", getSource()=");
		builder.append(getSource());
		builder.append(", getOrgStatus()=");
		builder.append(getOrgStatus());
		builder.append(", getOrgId()=");
		builder.append(getOrgId());
		builder.append(", getOrgName()=");
		builder.append(getOrgName());
		builder.append(", getOrgJwksEndpoint()=");
		builder.append(getOrgJwksEndpoint());
		builder.append(", getOrgJwksRevokedEndpoint()=");
		builder.append(getOrgJwksRevokedEndpoint());
		builder.append(", getOrgCompetentAuthId()=");
		builder.append(getOrgCompetentAuthId());
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
	
}

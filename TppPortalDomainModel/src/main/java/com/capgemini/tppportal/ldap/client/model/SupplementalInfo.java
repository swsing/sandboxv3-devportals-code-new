package com.capgemini.tppportal.ldap.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SupplementalInfo {
	
	private String ACCESS_SESSION_REVOCATION_API;
	private String VALIDATE_USING_ALL_ELIGIBLE_ATMS;
	private String DEFAULT_ATM_ID;
	private String PING_ACCESS_LOGOUT_CAPABLE;
	private String id_token_signed_response_alg;

	public String getACCESSSESSIONREVOCATIONAPI() {
		return this.ACCESS_SESSION_REVOCATION_API;
	}

	public void setACCESSSESSIONREVOCATIONAPI(String ACCESS_SESSION_REVOCATION_API) {
		this.ACCESS_SESSION_REVOCATION_API = ACCESS_SESSION_REVOCATION_API;
	}

	public String getVALIDATEUSINGALLELIGIBLEATMS() {
		return this.VALIDATE_USING_ALL_ELIGIBLE_ATMS;
	}

	public void setVALIDATEUSINGALLELIGIBLEATMS(String VALIDATE_USING_ALL_ELIGIBLE_ATMS) {
		this.VALIDATE_USING_ALL_ELIGIBLE_ATMS = VALIDATE_USING_ALL_ELIGIBLE_ATMS;
	}

	public String getDEFAULTATMID() {
		return this.DEFAULT_ATM_ID;
	}

	public void setDEFAULTATMID(String DEFAULT_ATM_ID) {
		this.DEFAULT_ATM_ID = DEFAULT_ATM_ID;
	}

	public String getPINGACCESSLOGOUTCAPABLE() {
		return this.PING_ACCESS_LOGOUT_CAPABLE;
	}

	public void setPINGACCESSLOGOUTCAPABLE(String PING_ACCESS_LOGOUT_CAPABLE) {
		this.PING_ACCESS_LOGOUT_CAPABLE = PING_ACCESS_LOGOUT_CAPABLE;
	}

	public String getIdTokenSignedResponseAlg() {
		return this.id_token_signed_response_alg;
	}

	public void setIdTokenSignedResponseAlg(String id_token_signed_response_alg) {
		this.id_token_signed_response_alg = id_token_signed_response_alg;
	}
}

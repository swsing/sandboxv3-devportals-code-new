package com.capgemini.tppportal.ssa.model;

import java.util.ArrayList;

public class SSAModel {
	
	private int iat;

	public int getIat() {
		return this.iat;
	}

	public void setIat(int iat) {
		this.iat = iat;
	}

	private String iss;

	public String getIss() {
		return this.iss;
	}

	public void setIss(String iss) {
		this.iss = iss;
	}

	private String jti;

	public String getJti() {
		return this.jti;
	}

	public void setJti(String jti) {
		this.jti = jti;
	}

	private String ob_registry_tos;

	public String getOb_registry_tos() {
		return this.ob_registry_tos;
	}

	public void setOb_registry_tos(String ob_registry_tos) {
		this.ob_registry_tos = ob_registry_tos;
	}

	private Object org_created_date;

	public Object getOrg_created_date() {
		return this.org_created_date;
	}

	public void setOrg_created_date(Object org_created_date) {
		this.org_created_date = org_created_date;
	}

	private String org_id;

	public String getOrg_id() {
		return this.org_id;
	}

	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}

	private String org_jwks_endpoint;

	public String getOrg_jwks_endpoint() {
		return this.org_jwks_endpoint;
	}

	public void setOrg_jwks_endpoint(String org_jwks_endpoint) {
		this.org_jwks_endpoint = org_jwks_endpoint;
	}

	private String org_jwks_revoked_endpoint;

	public String getOrg_jwks_revoked_endpoint() {
		return this.org_jwks_revoked_endpoint;
	}

	public void setOrg_jwks_revoked_endpoint(String org_jwks_revoked_endpoint) {
		this.org_jwks_revoked_endpoint = org_jwks_revoked_endpoint;
	}

	private Object org_last_modified_date;

	public Object getOrg_last_modified_date() {
		return this.org_last_modified_date;
	}

	public void setOrg_last_modified_date(Object org_last_modified_date) {
		this.org_last_modified_date = org_last_modified_date;
	}

	private String org_name;

	public String getOrg_name() {
		return this.org_name;
	}

	public void setOrg_name(String org_name) {
		this.org_name = org_name;
	}

	private String org_status;

	public String getOrg_status() {
		return this.org_status;
	}

	public void setOrg_status(String org_status) {
		this.org_status = org_status;
	}

	private OrganisationCompetentAuthorityClaims organisation_competent_authority_claims;

	public OrganisationCompetentAuthorityClaims getOrganisation_competent_authority_claims() {
		return this.organisation_competent_authority_claims;
	}

	public void setOrganisation_competent_authority_claims(
			OrganisationCompetentAuthorityClaims organisation_competent_authority_claims) {
		this.organisation_competent_authority_claims = organisation_competent_authority_claims;
	}

	private String software_client_description;

	public String getSoftware_client_description() {
		return this.software_client_description;
	}

	public void setSoftware_client_description(String software_client_description) {
		this.software_client_description = software_client_description;
	}

	private String software_client_id;

	public String getSoftware_client_id() {
		return this.software_client_id;
	}

	public void setSoftware_client_id(String software_client_id) {
		this.software_client_id = software_client_id;
	}

	private String software_client_name;

	public String getSoftware_client_name() {
		return this.software_client_name;
	}

	public void setSoftware_client_name(String software_client_name) {
		this.software_client_name = software_client_name;
	}

	private String software_client_uri;

	public String getSoftware_client_uri() {
		return this.software_client_uri;
	}

	public void setSoftware_client_uri(String software_client_uri) {
		this.software_client_uri = software_client_uri;
	}

	private ArrayList<Object> org_contacts;

	public ArrayList<Object> getOrg_contacts() {
		return org_contacts;
	}

	public void setOrg_contacts(ArrayList<Object> org_contacts) {
		this.org_contacts = org_contacts;
	}

	private String software_environment;

	public String getSoftware_environment() {
		return this.software_environment;
	}

	public void setSoftware_environment(String software_environment) {
		this.software_environment = software_environment;
	}

	private String software_id;

	public String getSoftware_id() {
		return this.software_id;
	}

	public void setSoftware_id(String software_id) {
		this.software_id = software_id;
	}

	private String software_jwks_endpoint;

	public String getSoftware_jwks_endpoint() {
		return this.software_jwks_endpoint;
	}

	public void setSoftware_jwks_endpoint(String software_jwks_endpoint) {
		this.software_jwks_endpoint = software_jwks_endpoint;
	}

	private String software_jwks_revoked_endpoint;

	public String getSoftware_jwks_revoked_endpoint() {
		return this.software_jwks_revoked_endpoint;
	}

	public void setSoftware_jwks_revoked_endpoint(String software_jwks_revoked_endpoint) {
		this.software_jwks_revoked_endpoint = software_jwks_revoked_endpoint;
	}

	private String software_logo_uri;

	public String getSoftware_logo_uri() {
		return this.software_logo_uri;
	}

	public void setSoftware_logo_uri(String software_logo_uri) {
		this.software_logo_uri = software_logo_uri;
	}

	private String software_mode;

	public String getSoftware_mode() {
		return this.software_mode;
	}

	public void setSoftware_mode(String software_mode) {
		this.software_mode = software_mode;
	}

	private String software_on_behalf_of_org;

	public String getSoftware_on_behalf_of_org() {
		return this.software_on_behalf_of_org;
	}

	public void setSoftware_on_behalf_of_org(String software_on_behalf_of_org) {
		this.software_on_behalf_of_org = software_on_behalf_of_org;
	}

	private String software_policy_uri;

	public String getSoftware_policy_uri() {
		return this.software_policy_uri;
	}

	public void setSoftware_policy_uri(String software_policy_uri) {
		this.software_policy_uri = software_policy_uri;
	}

	private ArrayList<String> software_redirect_uris;

	public ArrayList<String> getSoftware_redirect_uris() {
		return this.software_redirect_uris;
	}

	public void setSoftware_redirect_uris(ArrayList<String> software_redirect_uris) {
		this.software_redirect_uris = software_redirect_uris;
	}

	private ArrayList<String> software_roles;

	public ArrayList<String> getSoftware_roles() {
		return this.software_roles;
	}

	public void setSoftware_roles(ArrayList<String> software_roles) {
		this.software_roles = software_roles;
	}

	private String software_tos_uri;

	public String getSoftware_tos_uri() {
		return this.software_tos_uri;
	}

	public void setSoftware_tos_uri(String software_tos_uri) {
		this.software_tos_uri = software_tos_uri;
	}

	private double software_version;

	public double getSoftware_version() {
		return this.software_version;
	}

	public void setSoftware_version(double software_version) {
		this.software_version = software_version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SSAModel [iat=");
		builder.append(iat);
		builder.append(", iss=");
		builder.append(iss);
		builder.append(", jti=");
		builder.append(jti);
		builder.append(", ob_registry_tos=");
		builder.append(ob_registry_tos);
		builder.append(", org_created_date=");
		builder.append(org_created_date);
		builder.append(", org_id=");
		builder.append(org_id);
		builder.append(", org_jwks_endpoint=");
		builder.append(org_jwks_endpoint);
		builder.append(", org_jwks_revoked_endpoint=");
		builder.append(org_jwks_revoked_endpoint);
		builder.append(", org_last_modified_date=");
		builder.append(org_last_modified_date);
		builder.append(", org_name=");
		builder.append(org_name);
		builder.append(", org_status=");
		builder.append(org_status);
		builder.append(", organisation_competent_authority_claims=");
		builder.append(organisation_competent_authority_claims);
		builder.append(", software_client_description=");
		builder.append(software_client_description);
		builder.append(", software_client_id=");
		builder.append(software_client_id);
		builder.append(", software_client_name=");
		builder.append(software_client_name);
		builder.append(", software_client_uri=");
		builder.append(software_client_uri);
		builder.append(", org_contacts=");
		builder.append(org_contacts);
		builder.append(", software_environment=");
		builder.append(software_environment);
		builder.append(", software_id=");
		builder.append(software_id);
		builder.append(", software_jwks_endpoint=");
		builder.append(software_jwks_endpoint);
		builder.append(", software_jwks_revoked_endpoint=");
		builder.append(software_jwks_revoked_endpoint);
		builder.append(", software_logo_uri=");
		builder.append(software_logo_uri);
		builder.append(", software_mode=");
		builder.append(software_mode);
		builder.append(", software_on_behalf_of_org=");
		builder.append(software_on_behalf_of_org);
		builder.append(", software_policy_uri=");
		builder.append(software_policy_uri);
		builder.append(", software_redirect_uris=");
		builder.append(software_redirect_uris);
		builder.append(", software_roles=");
		builder.append(software_roles);
		builder.append(", software_tos_uri=");
		builder.append(software_tos_uri);
		builder.append(", software_version=");
		builder.append(software_version);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}

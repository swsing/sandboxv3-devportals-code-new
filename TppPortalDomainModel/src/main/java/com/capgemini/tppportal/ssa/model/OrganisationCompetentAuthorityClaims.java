package com.capgemini.tppportal.ssa.model;

import java.util.ArrayList;

public class OrganisationCompetentAuthorityClaims {
	
	private ArrayList<Authorisation> authorisations;

	public ArrayList<Authorisation> getAuthorisations() {
		return this.authorisations;
	}

	public void setAuthorisations(ArrayList<Authorisation> authorisations) {
		this.authorisations = authorisations;
	}

	private String authority_id;

	public String getAuthority_id() {
		return this.authority_id;
	}

	public void setAuthority_id(String authority_id) {
		this.authority_id = authority_id;
	}

	private String registration_id;

	public String getRegistration_id() {
		return this.registration_id;
	}

	public void setRegistration_id(String registration_id) {
		this.registration_id = registration_id;
	}

	private String status;

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}

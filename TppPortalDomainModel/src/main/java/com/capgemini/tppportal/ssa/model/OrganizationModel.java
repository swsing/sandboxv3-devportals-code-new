package com.capgemini.tppportal.ssa.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrganizationModel {
	
	private String ob_registry_tos;

	public String getOb_registry_tos() {
		return this.ob_registry_tos;
	}

	public void setOb_registry_tos(String ob_registry_tos) {
		this.ob_registry_tos = ob_registry_tos;
	}

	private Object org_created_date;

	public Object getOrg_created_date() {
		return this.org_created_date;
	}

	public void setOrg_created_date(Object org_created_date) {
		this.org_created_date = org_created_date;
	}

	private String org_id;

	public String getOrg_id() {
		return this.org_id;
	}

	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}

	private String org_jwks_endpoint;

	public String getOrg_jwks_endpoint() {
		return this.org_jwks_endpoint;
	}

	public void setOrg_jwks_endpoint(String org_jwks_endpoint) {
		this.org_jwks_endpoint = org_jwks_endpoint;
	}

	private String org_jwks_revoked_endpoint;

	public String getOrg_jwks_revoked_endpoint() {
		return this.org_jwks_revoked_endpoint;
	}

	public void setOrg_jwks_revoked_endpoint(String org_jwks_revoked_endpoint) {
		this.org_jwks_revoked_endpoint = org_jwks_revoked_endpoint;
	}

	private Object org_last_modified_date;

	public Object getOrg_last_modified_date() {
		return this.org_last_modified_date;
	}

	public void setOrg_last_modified_date(Object org_last_modified_date) {
		this.org_last_modified_date = org_last_modified_date;
	}

	private String org_name;

	public String getOrg_name() {
		return this.org_name;
	}

	public void setOrg_name(String org_name) {
		this.org_name = org_name;
	}

	private String org_status;

	public String getOrg_status() {
		return this.org_status;
	}

	public void setOrg_status(String org_status) {
		this.org_status = org_status;
	}

	private OrganisationCompetentAuthorityClaims organisation_competent_authority_claims;

	public OrganisationCompetentAuthorityClaims getOrganisation_competent_authority_claims() {
		return this.organisation_competent_authority_claims;
	}

	public void setOrganisation_competent_authority_claims(
			OrganisationCompetentAuthorityClaims organisation_competent_authority_claims) {
		this.organisation_competent_authority_claims = organisation_competent_authority_claims;
	}	

	
}

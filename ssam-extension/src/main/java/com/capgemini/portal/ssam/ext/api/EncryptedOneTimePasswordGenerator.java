package com.capgemini.portal.ssam.ext.api;

import static com.unboundid.ldap.sdk.unboundidds.OneTimePassword.DEFAULT_TOTP_INTERVAL_DURATION_SECONDS;
import static com.unboundid.ldap.sdk.unboundidds.OneTimePassword.DEFAULT_TOTP_NUM_DIGITS;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.portal.ssam.ext.util.AESEncDecUtil;
import com.unboundid.directory.sdk.common.types.Entry;
import com.unboundid.directory.sdk.ds.api.PasswordGenerator;
import com.unboundid.directory.sdk.ds.config.PasswordGeneratorConfig;
import com.unboundid.directory.sdk.ds.internal.DirectoryServerExtension;
import com.unboundid.directory.sdk.ds.types.DirectoryServerContext;
import com.unboundid.directory.sdk.proxy.internal.DirectoryProxyServerExtension;
import com.unboundid.directory.sdk.sync.internal.SynchronizationServerExtension;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.unboundidds.OneTimePassword;
import com.unboundid.util.ByteString;
import com.unboundid.util.ByteStringFactory;
import com.unboundid.util.Extensible;
import com.unboundid.util.ThreadSafety;
import com.unboundid.util.ThreadSafetyLevel;
import com.unboundid.util.args.ArgumentException;
import com.unboundid.util.args.ArgumentParser;
import com.unboundid.util.args.IntegerArgument;

/**
 * {@inheritDoc}
 */
@Extensible()
@DirectoryServerExtension()
@DirectoryProxyServerExtension(appliesToLocalContent = true, appliesToRemoteContent = false)
@SynchronizationServerExtension(appliesToLocalContent = true, appliesToSynchronizedContent = false)
@ThreadSafety(level = ThreadSafetyLevel.INTERFACE_THREADSAFE)
public class EncryptedOneTimePasswordGenerator extends PasswordGenerator {
	private static final Logger LOG = LoggerFactory.getLogger(EncryptedOneTimePasswordGenerator.class);
	private static final String INTERVAL_TIME = "intervalTime";
	private static final byte[] SECRETKEY = new byte[] { 
			'$', 'a', 'a', 'm', 
			'D', 'e', 'v', 
			'P', '0', 'r', 't', 'a','l',
			'#', 'V', '2' 
	};
	private volatile int intervalTime = 0;

	/**
	 * {@inheritDoc}
	 */

	public EncryptedOneTimePasswordGenerator() {
		// No implementation is required.
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public String getExtensionName() {
		return this.getClass().getSimpleName();
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public String[] getExtensionDescription() {
		return new String[] { "This password generator is used to encrypt the code along with email for sending OTP." };
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public ByteString generatePassword(Entry userEntry) throws LDAPException {
		LOG.debug("START: generatePassword() method");
		String oneTimePassword = OneTimePassword.totp(SECRETKEY, System.currentTimeMillis(), (intervalTime == 0) ? DEFAULT_TOTP_INTERVAL_DURATION_SECONDS : intervalTime,
				DEFAULT_TOTP_NUM_DIGITS);
		StringBuilder builder = new StringBuilder()
				.append("email=")
				.append(userEntry.getAttribute("uid").get(0).getValue())
				.append("&code=")
				.append(oneTimePassword);
		String encryptedText = null;
		try {
			encryptedText = AESEncDecUtil.encrypt(builder.toString());
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
			LOG.error("Error during encryption process: " + e.getMessage());
			throw new LDAPException(ResultCode.TOKEN_DELIVERY_MECHANISM_UNAVAILABLE, e);
		}
		LOG.debug("END: generatePassword() method");
		return ByteStringFactory.create(encryptedText);
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public void defineConfigArguments(final ArgumentParser parser) throws ArgumentException {
		parser.addArgument(new IntegerArgument(null, INTERVAL_TIME, true, 1, INTERVAL_TIME,
				"The interval time in seconds to be used when constructing the OTP.", 5 * 60));
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public void initializePasswordGenerator(final DirectoryServerContext serverContext,
			final PasswordGeneratorConfig config, final ArgumentParser parser) throws LDAPException {
		LOG.debug("START: initializePasswordGenerator() method");
		final IntegerArgument intervalTimeArgument = parser.getIntegerArgument(INTERVAL_TIME);
		this.intervalTime = intervalTimeArgument.getValue();
		LOG.debug("END: initializePasswordGenerator() method");
	}

}

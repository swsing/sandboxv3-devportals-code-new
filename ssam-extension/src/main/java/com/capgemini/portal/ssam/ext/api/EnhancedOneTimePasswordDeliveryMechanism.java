package com.capgemini.portal.ssam.ext.api;

import static com.capgemini.portal.ssam.ext.util.Constants.APP_PROPS_FILE_NAME;
import static com.capgemini.portal.ssam.ext.util.Constants.APP_PROPS_RELATIVE_PATH;
import static com.capgemini.portal.ssam.ext.util.Constants.COMMA;
import static com.capgemini.portal.ssam.ext.util.Constants.EMAIL_TEMPLATE_FILE_NAME;
import static com.capgemini.portal.ssam.ext.util.Constants.EQUAL_SIGN;
import static com.capgemini.portal.ssam.ext.util.Constants.INSTANCE_ROOT;
import static com.capgemini.portal.ssam.ext.util.Constants.POUND_SIGN;
import static com.capgemini.portal.ssam.ext.util.Constants.SLASH;
import static com.capgemini.portal.ssam.ext.util.Constants.TEMPLATE_RELATIVE_PATH;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.portal.ssam.ext.config.EnhancedOneTimePasswordDeliveryMechanismConfig;
import com.unboundid.directory.sdk.common.types.Entry;
import com.unboundid.directory.sdk.ds.api.OneTimePasswordDeliveryMechanism;
import com.unboundid.directory.sdk.ds.internal.DirectoryServerExtension;
import com.unboundid.directory.sdk.proxy.internal.DirectoryProxyServerExtension;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.util.Extensible;
import com.unboundid.util.ThreadSafety;
import com.unboundid.util.ThreadSafetyLevel;

@Extensible()
@DirectoryServerExtension()
@DirectoryProxyServerExtension(appliesToLocalContent=true,
     appliesToRemoteContent=true)
@ThreadSafety(level=ThreadSafetyLevel.INTERFACE_THREADSAFE)
public class EnhancedOneTimePasswordDeliveryMechanism extends OneTimePasswordDeliveryMechanism {
	private static final Logger LOG = LoggerFactory.getLogger(EnhancedOneTimePasswordDeliveryMechanism.class);

	private volatile EnhancedOneTimePasswordDeliveryMechanismConfig cfg;

	private static final String MECHANISM_UNAVAILABLE_ERROR_MSG = "Error sending registration code with delivery mechanism: " + ResultCode.TOKEN_DELIVERY_MECHANISM_UNAVAILABLE.intValue();

	private static final String INVALID_RECIPIENT_ERROR_MSG = "Error sending registration code due to invalid recipient: " + ResultCode.TOKEN_DELIVERY_INVALID_RECIPIENT_ID.intValue();

	private static StringBuilder templateMessage;

	private static volatile Properties props;
	
	private static boolean isPropertiesSet;
	
	private Path propsPath;
	
	private Path templatePath;

	static {
		isPropertiesSet = false;
		templateMessage = new StringBuilder();
		props = new Properties();
	}

	public EnhancedOneTimePasswordDeliveryMechanism() {
		this.cfg = null;
	}

	public EnhancedOneTimePasswordDeliveryMechanismConfig getConfiguration() {
		return this.cfg;
	}

	private String processTemplate(String tokenValue, String targetRecipientID, Entry userEntry, boolean isMock, Path... paths) throws LDAPException {
		LOG.debug("START: processTemplate() method");
		String recipientAddress = null;
		String firstName = "User";
		
		while(!isPropertiesSet) {
			// Read properties and template file
			loadPropertiesAndTemplate(isMock, paths);
			isPropertiesSet =  true;
		}
		
		String verifylinkIntervalTime = props.getProperty("verifylinkTimetoLive");
		// current time in UTC time zone
		ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC).plusSeconds(Integer.valueOf(verifylinkIntervalTime));
		String datePatternString = " d'" + getOrdinalForNumber(utc.getDayOfMonth()) + "' MMMM yyyy";
		
		List<String> emailAddresses = new ArrayList<>();
		// Extract email address from userDN
		if (userEntry.getDN() != null) {
			final String email = userEntry.getDN().split(COMMA, 2)[0].split(EQUAL_SIGN, 2)[1];
			emailAddresses.add(email);
			if(userEntry.hasAttribute("cn")){
				firstName =  userEntry.getAttribute("cn").get(0).getValue();
			}
		}

		if (!emailAddresses.isEmpty() && (targetRecipientID == null)) {
			recipientAddress = emailAddresses.get(0);
		}

		// If recipientAddress is absent, throw LDAP exception
		if (recipientAddress == null) {
			if (targetRecipientID == null) {
				LOG.error(MECHANISM_UNAVAILABLE_ERROR_MSG);
				throw new LDAPException(ResultCode.TOKEN_DELIVERY_MECHANISM_UNAVAILABLE, MECHANISM_UNAVAILABLE_ERROR_MSG);
			}
			LOG.error(INVALID_RECIPIENT_ERROR_MSG);
			throw new LDAPException(ResultCode.TOKEN_DELIVERY_INVALID_RECIPIENT_ID, INVALID_RECIPIENT_ERROR_MSG);
		}

		// Initialize mail session
		Session session = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(props.getProperty("mail.username"), props.getProperty("mail.password"));
			}
		});
		
		
		
				
		// Construct the actual message from template
		final String validateHTML = templateMessage.toString()
				.replace("{firstName}", firstName)
				.replace("{federationHost}", props.getProperty("federationHost"))
				.replace("{code}", tokenValue)
				.replace("{utcTimeString}", String.format("%1$tH:%1$tM %1$tp", utc))
				.replace("{utcDateString}", utc.format(DateTimeFormatter.ofPattern(datePatternString)));
		try {
			Message emailMessage = new MimeMessage(session);
			emailMessage.setFrom(new InternetAddress(props.getProperty("senderAddress")));
			emailMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipientAddress));
			emailMessage.setSubject(props.getProperty("messageSubject"));
			emailMessage.setContent(validateHTML, props.getProperty("mail.contentType"));
			Transport.send(emailMessage);
		} catch (MessagingException e) {
			LOG.error(MECHANISM_UNAVAILABLE_ERROR_MSG);
			throw new LDAPException(ResultCode.TOKEN_DELIVERY_MECHANISM_UNAVAILABLE, recipientAddress, e);
		}

		LOG.debug("END: processTemplate() method");
		return recipientAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getExtensionName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public String[] getExtensionDescription() {
		return new String[] { "This mechanism is used to deliver HTML Email" };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String deliverOneTimePassword(String oneTimePassword, Entry userEntry, String targetRecipientID,
			StringBuilder resultMessage) throws LDAPException {
		LOG.debug("Inside deliverOneTimePassword() method...");
		
		// Set file locations
		this.setPropsPath(Paths.get(getConfigurationPath(APP_PROPS_RELATIVE_PATH, APP_PROPS_FILE_NAME)));
		this.setTemplatePath(Paths.get(getConfigurationPath(TEMPLATE_RELATIVE_PATH, EMAIL_TEMPLATE_FILE_NAME)));

		return processTemplate(oneTimePassword, targetRecipientID, userEntry, false);
	}
	
	String getConfigurationPath(final String relativePath, final String fileName) {
		String rootPath = System.getenv(INSTANCE_ROOT); 
		if(rootPath == null) {
			rootPath = File.separator;
		}
		return new StringBuilder()
				.append(rootPath.replace(SLASH, File.separator))
				.append(File.separator)
				.append(relativePath.replace(SLASH, File.separator))
				.append(File.separator)
				.append(fileName)
				.toString();
	}

	void loadPropertiesAndTemplate(boolean isMock, Path... paths) throws LDAPException {
		Path propertiesPath = isMock ? paths[0] : this.getPropsPath();
		try (Stream<String> stream = Files.lines(propertiesPath)) {
			// load a properties file
			stream
			.filter(s -> s.contains(EQUAL_SIGN) && !s.startsWith(POUND_SIGN))
			.collect(Collectors.toMap(
					k -> k.split(EQUAL_SIGN, 2)[0], 
					v -> v.split(EQUAL_SIGN, 2)[1]))
			.forEach(props::put);
		} catch (IOException ex) {
			LOG.error("Error reading application.properties at location : {}", ex.getMessage());
			throw new LDAPException(ResultCode.TOKEN_DELIVERY_MECHANISM_UNAVAILABLE, MECHANISM_UNAVAILABLE_ERROR_MSG);
		}
		Path templatingPath = isMock ? paths[1] : this.getTemplatePath();
		try (Stream<String> stream = Files.lines(templatingPath)) {
			stream.forEachOrdered(templateMessage::append);
		} catch (IOException e) {
			LOG.error("Error reading template file at location : {}", e.getMessage());
			throw new LDAPException(ResultCode.TOKEN_DELIVERY_MECHANISM_UNAVAILABLE, MECHANISM_UNAVAILABLE_ERROR_MSG);
		}
	}

	public Path getPropsPath() {
		return propsPath;
	}

	public void setPropsPath(Path propsPath) {
		this.propsPath = propsPath;
	}

	public Path getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(Path templatePath) {
		this.templatePath = templatePath;
	}
	
	/**
	 * return Ordinal for given number
	 * @param value
	 * @return
	 */
	private String getOrdinalForNumber(int value) {
		 int hundredRemainder = value % 100;
		 int tenRemainder = value % 10;
		 if(hundredRemainder - tenRemainder == 10) {
		  return "th";
		 }
		 
		 switch (tenRemainder) {
		  case 1:
		   return "st";
		  case 2:
		   return "nd";
		  case 3:
		   return "rd";
		  default:
		   return "th";
		 }
		}


}

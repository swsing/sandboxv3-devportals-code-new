package com.capgemini.portal.ssam.ext.config;

import com.unboundid.directory.sdk.common.schema.AttributeType;
import com.unboundid.directory.sdk.ds.config.OneTimePasswordDeliveryMechanismConfig;
import com.unboundid.util.NotExtensible;
import com.unboundid.util.ThreadSafety;
import com.unboundid.util.ThreadSafetyLevel;

/**
 * This interface defines a set of methods that may be used to obtain
 * information about the general configuration for extensions used by the
 * Directory Server.
 */
@NotExtensible()
@ThreadSafety(level = ThreadSafetyLevel.INTERFACE_NOT_THREADSAFE)
public interface EnhancedOneTimePasswordDeliveryMechanismConfig extends OneTimePasswordDeliveryMechanismConfig {
	public abstract AttributeType getEmailAddressAttributeType();

	public abstract String getEmailAddressJSONField();

	public abstract String getJavaClass();

	public abstract String getMessageSubject();

	public abstract String getMessageTextAfterOTP();

	public abstract String getMessageTextBeforeOTP();

	public abstract String getSenderAddress();

	public abstract String getValidateURL();
}

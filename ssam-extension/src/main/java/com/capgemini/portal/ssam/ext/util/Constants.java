package com.capgemini.portal.ssam.ext.util;

public final class Constants {
	public static final String ALGORITHM = "AES";
	public static final String AMPERSAND = "&";
	public static final String COMMA = ",";
	public static final String ENCODING = "UTF-8";
	public static final String EQUAL_SIGN = "=";
	public static final String INSTANCE_ROOT = "INSTANCE_ROOT";
	public static final String POUND_SIGN = "#";
	public static final String SHA256 = "SHA-256";
	public static final String SLASH = "/";
	public static final String TEMPLATE_RELATIVE_PATH = "extensions/com.capgemini.portal.ssam-extension/config";
	public static final String APP_PROPS_RELATIVE_PATH = "webapps/ssam-config";
	public static final String APP_PROPS_FILE_NAME = "application.properties";
	public static final String EMAIL_TEMPLATE_FILE_NAME = "emailMessage.html";
	
	/**
	 * Private constructor as only static members present
	 */
	private Constants() {
		// Private constructor
	}
	
	public static final Constants getInstance() {
		return new Constants();
	}
}

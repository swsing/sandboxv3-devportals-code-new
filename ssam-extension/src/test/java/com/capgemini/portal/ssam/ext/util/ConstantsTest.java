package com.capgemini.portal.ssam.ext.util;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.portal.ssam.ext.util.Constants;

public class ConstantsTest {
	static final Logger LOG = LoggerFactory.getLogger(ConstantsTest.class);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// Empty implementation
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		// Empty implementation
	}

	@Before
	public void setUp() throws Exception {
		// Empty implementation
	}

	@After
	public void tearDown() throws Exception {
		// Empty implementation
	}

	@Test
	public void testConstructorIsPrivate()
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		LOG.debug("START: testConstructorIsPrivate() method");
		Constructor<Constants> constructor = Constants.class.getDeclaredConstructor();
		assertTrue(Modifier.isPrivate(constructor.getModifiers()));
		constructor.setAccessible(true);
		assertNotSame(constructor.newInstance(), Constants.getInstance());
		LOG.debug("END: testConstructorIsPrivate() method");
	}
}

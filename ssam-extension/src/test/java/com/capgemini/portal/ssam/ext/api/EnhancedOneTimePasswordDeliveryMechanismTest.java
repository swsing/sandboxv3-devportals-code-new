package com.capgemini.portal.ssam.ext.api;

import static com.capgemini.portal.ssam.ext.util.Constants.APP_PROPS_FILE_NAME;
import static com.capgemini.portal.ssam.ext.util.Constants.APP_PROPS_RELATIVE_PATH;
import static com.capgemini.portal.ssam.ext.util.Constants.EMAIL_TEMPLATE_FILE_NAME;
import static com.capgemini.portal.ssam.ext.util.Constants.TEMPLATE_RELATIVE_PATH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.powermock.api.mockito.PowerMockito.doCallRealMethod;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.portal.ssam.ext.api.EnhancedOneTimePasswordDeliveryMechanism;
import com.capgemini.portal.ssam.ext.util.Constants;
import com.unboundid.directory.sdk.common.types.Entry;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.util.args.ArgumentException;
import com.unboundid.util.args.ArgumentParser;

@RunWith(PowerMockRunner.class)
public class EnhancedOneTimePasswordDeliveryMechanismTest {
	static final Logger LOG = LoggerFactory.getLogger(EnhancedOneTimePasswordDeliveryMechanismTest.class);

	private Path propsPath;
	
	private Path templatePath;
	// create mock
	@Mock
	private volatile EnhancedOneTimePasswordDeliveryMechanism testMechanism;
	
	@Mock
	private volatile Entry userEntry;
	
	@Mock
	private volatile Message message;
	
	@Mock
	private volatile Path path;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// Empty implementation
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		// Empty implementation
	}

	@Before
	public void setUp() throws Exception {
		LOG.debug("START: setUp() method");
		MockitoAnnotations.initMocks(this);
		final URL propsResource = this.getClass().getClassLoader().getParent().getResource(Constants.APP_PROPS_FILE_NAME);
		final URI propsURI = propsResource.toURI();
		propsPath = Paths.get(propsURI);
		final URL templateResource = this.getClass().getClassLoader().getParent().getResource(Constants.EMAIL_TEMPLATE_FILE_NAME);
		final URI templateURI = templateResource.toURI();
		templatePath = Paths.get(templateURI);

		final String tokenValue = "email=sales@capgemini.com&code=187212";
		Properties props = new Properties();
		props.put("federationHost", "https://federation.capgeminiwebservices.com");
		props.put("code", tokenValue);
		props.put("senderAddress", "no-reply@capgeminiwebservices.com");
		props.put("verifylinkTimetoLive", "30");

		Whitebox.setInternalState(EnhancedOneTimePasswordDeliveryMechanism.class, "templateMessage", new StringBuilder());
		Whitebox.setInternalState(EnhancedOneTimePasswordDeliveryMechanism.class, "props", props);

		LOG.debug("END: setUp() method");
	}

	@After
	public void tearDown() throws Exception {
		// Empty implementation
	}

	@Test(expected = LDAPException.class)
	@PrepareForTest({Transport.class, Message.class, Entry.class})
	public void testFailNotNullRecpIDDeliverOneTimePassword() throws Exception {
		LOG.debug("START: testFailNotNullRecpIDDeliverOneTimePassword() method");
		String tokenValue = "email=sales@capgemini.com&code=187212";
		// create mock
		when(userEntry.getDN()).thenReturn("uid=sales@capgemini.com,ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");

		// use mock in test....
		String actual = Whitebox.<String> invokeMethod(testMechanism, "processTemplate", tokenValue, "sales@capgemini.com", userEntry, false, propsPath, templatePath);
		assertNull(actual);
		LOG.debug("END: testFailNotNullRecpIDDeliverOneTimePassword() method");
	}
	
	@Test(expected = LDAPException.class)
	@PrepareForTest({Transport.class, Message.class, Entry.class})
	public void testFailNullRecpIDDeliverOneTimePassword() throws Exception {
		LOG.debug("START: testFailNullRecpIDDeliverOneTimePassword() method");
		String tokenValue = "email=sales@capgemini.com&code=187212";
		// create mock
		when(userEntry.getDN()).thenReturn(null);

		// use mock in test....
		String actual = Whitebox.<String> invokeMethod(testMechanism, "processTemplate", tokenValue, null, userEntry, false, propsPath, templatePath);
		assertNull(actual);
		LOG.debug("END: testFailNullRecpIDDeliverOneTimePassword() method");
	}

	@Test
	@PrepareForTest({Transport.class, Message.class, Entry.class})
	public void testDeliverOneTimePassword() throws Exception {
		LOG.debug("START: testDeliverOneTimePassword() method");
		String tokenValue = "email=sales@capgemini.com&code=187212";
		// create mock
		when(userEntry.getDN()).thenReturn("uid=sales@capgemini.com,ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");

		mockStatic(Transport.class);
		doNothing().when(Transport.class, "send", message);
		
		spy(testMechanism);
		doNothing().when(testMechanism).loadPropertiesAndTemplate(false, propsPath, templatePath);
		
		// define return value for method
		String expected = "sales@capgemini.com";
		when(testMechanism.deliverOneTimePassword(tokenValue, userEntry, null, new StringBuilder())).thenCallRealMethod();
		String actual = Whitebox.<String> invokeMethod(testMechanism, "processTemplate", tokenValue, null, userEntry, false, propsPath, templatePath);
		// use mock in test....
		assertEquals(expected, actual);
		LOG.debug("END: testDeliverOneTimePassword() method");
	}

	@Test
	@PrepareForTest({Transport.class, Message.class, Entry.class})
	public void testDeliverOneTimePasswordMocked() throws Exception {
		LOG.debug("START: testDeliverOneTimePasswordMocked() method");
		String tokenValue = "email=sales@capgemini.com&code=187212";
		// create mock
		when(userEntry.getDN()).thenReturn("uid=sales@capgemini.com,ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");

		mockStatic(Transport.class);
		doNothing().when(Transport.class, "send", message);
		
		spy(testMechanism);
		Properties props = new Properties();
		props.put("federationHost", "https://federation.capgeminiwebservices.com");
		props.put("code", tokenValue);
		props.put("senderAddress", "no-reply@capgeminiwebservices.com");
		props.put("verifylinkTimetoLive", "30");

		doCallRealMethod().when(testMechanism).loadPropertiesAndTemplate(true, propsPath, templatePath);
		
		Whitebox.setInternalState(EnhancedOneTimePasswordDeliveryMechanism.class, "templateMessage", new StringBuilder());
		Whitebox.setInternalState(EnhancedOneTimePasswordDeliveryMechanism.class, "props", props);
		
		// define return value for method
		String expected = "sales@capgemini.com";
		String actual = Whitebox.<String> invokeMethod(testMechanism, "processTemplate", tokenValue, null, userEntry, true, propsPath, templatePath);
		// use mock in test....
		assertEquals(expected, actual);
		LOG.debug("END: testDeliverOneTimePasswordMocked() method");
	}

	@Test
	public void testInitializeOTPDeliveryMechanism() throws ArgumentException, LDAPException {
		LOG.debug("START: testInitializeOTPDeliveryMechanism() method");
		// define return value for method
		EnhancedOneTimePasswordDeliveryMechanism deliveryMechanism = new EnhancedOneTimePasswordDeliveryMechanism();
		ArgumentParser parser = new ArgumentParser(deliveryMechanism.getExtensionName(), deliveryMechanism.getExtensionDescription()[0]);
		deliveryMechanism.defineConfigArguments(parser);

		// use mock in test....
		deliveryMechanism.initializeOTPDeliveryMechanism(null, deliveryMechanism.getConfiguration(), parser);
		LOG.debug("END: testInitializeOTPDeliveryMechanism() method");
	}

	@Test
	@PrepareForTest({Transport.class, Message.class, Entry.class})
	public void testGetConfiguration() throws LDAPException {
		LOG.debug("START: testGetConfiguration() method");
		// use mock in test....
		when(testMechanism.getConfiguration()).thenCallRealMethod();
		assertNull(testMechanism.getConfiguration());
		LOG.debug("END: testGetConfiguration() method");
	}
	
	@Test
	@PrepareForTest({Transport.class, Message.class, Entry.class})
	public void testVerifyInvocation() throws Exception {
		LOG.debug("START: testVerifyInvocation() method");
		String tokenValue = "email=sales@capgemini.com&code=187212";

		// use mock in test....
		// create mock
		when(userEntry.getDN()).thenReturn("uid=sales@capgemini.com,ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");

		mockStatic(Transport.class);
		doNothing().when(Transport.class, "send", message);
		
		Properties props = new Properties();
		props.put("federationHost", "https://federation.capgeminiwebservices.com");
		props.put("code", tokenValue);
		props.put("senderAddress", "no-reply@capgeminiwebservices.com");
		props.put("verifylinkTimetoLive", "30");
		Whitebox.setInternalState(EnhancedOneTimePasswordDeliveryMechanism.class, "templateMessage", new StringBuilder());
		Whitebox.setInternalState(EnhancedOneTimePasswordDeliveryMechanism.class, "props", props);

		when(testMechanism, "processTemplate", tokenValue, null, userEntry, true, propsPath, templatePath).thenCallRealMethod();

		// define return value for method
		String actual = Whitebox.<String> invokeMethod(testMechanism, "processTemplate", tokenValue, null, userEntry, true, propsPath, templatePath);
		assertNotNull(actual);
		LOG.debug("END: testVerifyInvocation() method");
	}

	@Test(expected=LDAPException.class)
	@PrepareForTest({Transport.class, Message.class, Entry.class})
	public void testVerifyInvocationError() throws Exception {
		LOG.debug("START: testVerifyInvocationError() method");
		String tokenValue = "email=sales@capgemini.com&code=187212";

		// use mock in test....
		// create mock
		when(userEntry.getDN()).thenReturn("uid=sales@capgemini.com,ou=dev,ou=people,dc=boi,dc=co.uk,dc=bank,dc=com");

		mockStatic(Transport.class);
		ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
		doThrow(new MessagingException()).when(Transport.class, "send", captor.capture());

		when(testMechanism, "processTemplate", tokenValue, null, userEntry, true, propsPath, templatePath).thenCallRealMethod();

		// define return value for method
		String actual = Whitebox.<String> invokeMethod(testMechanism, "processTemplate", tokenValue, null, userEntry, true, propsPath, templatePath);
		assertNotNull(actual);
		LOG.debug("END: testVerifyInvocationError() method");
	}

	@Test
	public void testGetPutInstanceMembers() throws LDAPException {
		LOG.debug("START: testGetPutInstanceMembers() method");
		EnhancedOneTimePasswordDeliveryMechanism deliveryMechanism = new EnhancedOneTimePasswordDeliveryMechanism();
		deliveryMechanism.setPropsPath(propsPath);
		deliveryMechanism.setTemplatePath(templatePath);
		
		assertEquals(propsPath, deliveryMechanism.getPropsPath());
		assertEquals(templatePath, deliveryMechanism.getTemplatePath());
		
		deliveryMechanism.loadPropertiesAndTemplate(true, propsPath, templatePath);
		LOG.debug("END: testGetPutInstanceMembers() method");
	}
	
	/**
	 * @throws LDAPException 
	 * 
	 */
	@Test(expected=LDAPException.class)
	public void testLoadPropertiesError() throws LDAPException {
		LOG.debug("START: testLoadPropertiesError() method");
		EnhancedOneTimePasswordDeliveryMechanism deliveryMechanism = new EnhancedOneTimePasswordDeliveryMechanism();
		deliveryMechanism.setPropsPath(Paths.get(deliveryMechanism.getConfigurationPath(APP_PROPS_RELATIVE_PATH, APP_PROPS_FILE_NAME)));
		deliveryMechanism.setTemplatePath(templatePath);
		deliveryMechanism.loadPropertiesAndTemplate(false);
		LOG.debug("END: testLoadPropertiesError() method");
	}
	
	@Test(expected=LDAPException.class)
	public void testLoadTemplateError() throws LDAPException {
		LOG.debug("START: testLoadTemplateError() method");
		EnhancedOneTimePasswordDeliveryMechanism deliveryMechanism = new EnhancedOneTimePasswordDeliveryMechanism();
		deliveryMechanism.setPropsPath(propsPath);
		deliveryMechanism.setTemplatePath(Paths.get(deliveryMechanism.getConfigurationPath(TEMPLATE_RELATIVE_PATH, EMAIL_TEMPLATE_FILE_NAME)));
		deliveryMechanism.loadPropertiesAndTemplate(false);
		LOG.debug("END: testLoadTemplateError() method");	
	}
}

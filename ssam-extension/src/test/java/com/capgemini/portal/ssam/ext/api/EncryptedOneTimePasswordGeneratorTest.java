package com.capgemini.portal.ssam.ext.api;

import static com.unboundid.ldap.sdk.unboundidds.OneTimePassword.DEFAULT_TOTP_INTERVAL_DURATION_SECONDS;
import static com.unboundid.ldap.sdk.unboundidds.OneTimePassword.DEFAULT_TOTP_NUM_DIGITS;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.portal.ssam.ext.api.EncryptedOneTimePasswordGenerator;
import com.capgemini.portal.ssam.ext.util.AESEncDecUtil;
import com.unboundid.directory.sdk.common.types.Entry;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.unboundidds.OneTimePassword;
import com.unboundid.util.ByteStringFactory;
import com.unboundid.util.args.ArgumentException;
import com.unboundid.util.args.ArgumentParser;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ OneTimePassword.class, AESEncDecUtil.class })
@PowerMockIgnore({"javax.crypto.*" })
public class EncryptedOneTimePasswordGeneratorTest {
	static final Logger LOG = LoggerFactory.getLogger(EncryptedOneTimePasswordGeneratorTest.class);
	static final byte[] SECRETKEY = new byte[] { 
			'$', 'a', 'a', 'm',
			'D', 'e', 'v', 'P', '0', 'r', 't', 'a',	'l',
			'#', 'V', '2' 
	};
	//private static final String INTERVAL_TIME = "intervalTime";
	// create mock
	@Mock
	private volatile EncryptedOneTimePasswordGenerator testGenerator;

	private volatile String code = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// Empty implementation
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		// Empty implementation
	}

	@Before
	public void setUp() throws Exception {
		LOG.debug("START: setUp() method");
		MockitoAnnotations.initMocks(this);
		code = "LlOiWynLAirwsORiDrMt-G_AQMTExq_5rk3mo08o4cu89D6c6K1fS0CtzRLATHqq";
		LOG.debug("END: setUp() method");
	}

	@After
	public void tearDown() throws Exception {
		// Empty implementation
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGeneratePassword() throws LDAPException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {
		LOG.debug("START: testGeneratePassword() method");
		final String prefix = "email=sales@capgemini.com";

		// define return value for method generatePassword()
		when(testGenerator.generatePassword(null)).thenReturn(ByteStringFactory.create(code));
		mockStatic(AESEncDecUtil.class);
		when(AESEncDecUtil.getInstance().decrypt(code))
		.thenReturn(
			new StringBuilder()
			.append(prefix)
			.append("&code=187212")
			.toString()
		);

		// use mock in test....
		assertEquals(AESEncDecUtil.getInstance()
				.decrypt(testGenerator
						.generatePassword(null)
						.stringValue())
				.startsWith(prefix), true);
		LOG.debug("END: testGeneratePassword() method");
	}

	@Test(expected = LDAPException.class)
	public void testGeneratePasswordException() throws Exception {
		LOG.debug("START: testGeneratePasswordWithEntry() method");
		String prefix = "sales@capgemini.com";
		String codeLocal= "187212";

		// create mock
		Entry userEntry = mock(Entry.class);
		spy(OneTimePassword.class);
	    doReturn(codeLocal).when(OneTimePassword.class, "hotp", SECRETKEY, System.currentTimeMillis() / 1000 / DEFAULT_TOTP_INTERVAL_DURATION_SECONDS, DEFAULT_TOTP_NUM_DIGITS);
	    
		List<Attribute> list = new ArrayList<>();
		list.add(new Attribute("uid", prefix));
		when(userEntry.getAttribute("uid")).thenReturn(list);
		spy(AESEncDecUtil.class);
		String text = new StringBuilder()
				.append("email=")
				.append(prefix)
				.append("&code=")
				.append(codeLocal)
				.toString();
		doThrow(new InvalidKeyException()).when(AESEncDecUtil.class, "encrypt", text);
		doReturn(text).when(AESEncDecUtil.class, "decrypt", code);
		// define return value for method
		when(testGenerator.generatePassword(userEntry)).thenCallRealMethod();

		// use mock in test....
		assertEquals(AESEncDecUtil.decrypt(testGenerator.generatePassword(userEntry).stringValue()).startsWith("email="+prefix),
				true);
		LOG.debug("END: testGeneratePasswordWithEntry() method");
	}
	
	@Test
	public void testGeneratePasswordWithEntry() throws Exception {
		LOG.debug("START: testGeneratePasswordWithEntry() method");
		String prefix = "sales@capgemini.com";
		String codeLocal= "187212";

		// create mock
		Entry userEntry = mock(Entry.class);
		spy(OneTimePassword.class);
	    doReturn(codeLocal).when(OneTimePassword.class, "hotp", SECRETKEY, System.currentTimeMillis() / 1000 / DEFAULT_TOTP_INTERVAL_DURATION_SECONDS, DEFAULT_TOTP_NUM_DIGITS);
	    
		List<Attribute> list = new ArrayList<>();
		list.add(new Attribute("uid", prefix));
		when(userEntry.getAttribute("uid")).thenReturn(list);
		spy(AESEncDecUtil.class);
		String text = new StringBuilder()
				.append("email=")
				.append(prefix)
				.append("&code=")
				.append(codeLocal)
				.toString();
		doReturn(code).when(AESEncDecUtil.class, "encrypt", text);
		doReturn(text).when(AESEncDecUtil.class, "decrypt", code);
		// define return value for method
		when(testGenerator.generatePassword(userEntry)).thenCallRealMethod();

		// use mock in test....
		assertEquals(AESEncDecUtil.decrypt(testGenerator.generatePassword(userEntry).stringValue()).startsWith("email="+prefix),
				true);
		LOG.debug("END: testGeneratePasswordWithEntry() method");
	}

	@Test
	public void testInitializePasswordGenerator() throws ArgumentException, LDAPException {
		LOG.debug("START: testInitializePasswordGenerator() method");
		// define return value for method
		EncryptedOneTimePasswordGenerator generator = new EncryptedOneTimePasswordGenerator();
		ArgumentParser parser = new ArgumentParser(generator.getExtensionName(), generator.getExtensionDescription()[0]);
		generator.defineConfigArguments(parser);
		generator.initializePasswordGenerator(null, null, parser);
		LOG.debug("END: testInitializePasswordGenerator() method");
	}
}
